<?php
/**
* Created by PhpStorm.
* User: bmcree
* Date: 7/19/2016
* Time: 12:45 PM
*/
// Example adldap.php file.
return [
    'account_suffix' => "@sgmc.org",

    'domain_controllers' => array("172.30.23.80"), // An array of domains may be provided for load balancing.

    'base_dn' => 'DC=sgmc,DC=org',

    'admin_username' => 'aimtechweb',

    'admin_password' => 'd1n0s@ur',
    //'admin_username' => NULL,

    //'admin_password' => NULL,

    'real_primary_group' => true, // Returns the primary group (an educated guess).

    'use_ssl' => false, // If TLS is true this MUST be false.

    'use_tls' => false, // If SSL is true this MUST be false.

    'recursive_groups' => true,
];