<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeHealthTST extends Model
{
    use SoftDeletes;
    protected $table = 'employee_health_TST_vac';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $dates = ['date_given', 'exp_date', 'date_read'];
}
