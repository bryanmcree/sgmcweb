<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketShareIP extends Model
{
    protected $table = 'market_share_IP';
    protected $guarded = [];
}
