<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatsReportAxiomView extends Model
{
    protected $table = 'stats_axiom_export';
}
