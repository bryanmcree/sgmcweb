<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysaidDashboard extends Model
{
    protected $table = 'sysaid';
    protected $dateFormat = 'Y-m-d H:i:s';
}
