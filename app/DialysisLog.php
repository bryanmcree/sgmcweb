<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DialysisLog extends Model
{
    use SoftDeletes;
    protected $table = 'dialysis_log';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    public function createdBy()
    {
        return $this->hasOne('App\User', 'employee_number', 'created_by');
    }
}
