<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HandwashingCostCenters extends Model
{
    use SoftDeletes;
    protected $table = 'handwashing_cost_centers';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    public function manager()
    {
        return $this->hasOne('App\User', 'employee_number', 'employee_number');
    }

    public function completed(){

        return $this->hasOne('App\Handwashing', 'cost_center', 'cost_center');
    }

    public function costCenters(){

        return $this->hasOne('App\CostCenters', 'cost_center', 'cost_center');
    }

    public function surveyCount()
    {
        return $this->completed()
            ->selectRaw('cost_center, count(*) as completedtotal')
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->groupBy('cost_center');
    }

    public function getCommentsCountAttribute()
    {
        // if relation is not loaded already, let's do it first
        if ( ! array_key_exists('surveyCount', $this->relations))
            $this->load('surveyCount');

        $related = $this->getRelationValue('surveyCount');

        // then return the count directly
        return ($related) ? (int) $related->completedtotal : 0;
    }
}
