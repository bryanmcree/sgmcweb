<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EducationPresenter extends Model
{
    protected $table = 'educational_presenters';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';
    public $timestamps = false;
}
