<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SSIS extends Model
{
    protected $table = 'SSIS_History';
    protected $dateFormat = 'Y-m-d H:i:s';
}
