<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EducationMethod extends Model
{
    protected $table = 'educational_methodology';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';
    public $timestamps = false;
}
