<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AxiomJobCodes extends Model
{
    protected $table = 'Axiom_Job_Codes';
}
