<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TimeOffRequest extends Model
{
    use SoftDeletes;
    protected $table = 'time_off_request';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $dates = [];

    public function requested_by()
    {
        return $this->hasOne('App\User', 'employee_number', 'employee_number');
    }

}
