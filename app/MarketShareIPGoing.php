<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketShareIPGoing extends Model
{
    protected $table = 'market_share_IP_going_to';
    protected $guarded = [];
}
