<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SecurityDispatch extends Model
{
    use SoftDeletes;
    protected $table = 'security_dispatches';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    public static function ongoing()
    {
        return static::where('completed', '=', '00:00:00.000000')
            ->get();
    }
}
