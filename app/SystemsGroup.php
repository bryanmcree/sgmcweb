<?php

namespace App;
use Carbon\Carbon;
use \Auth;

use Illuminate\Database\Eloquent\Model;

class SystemsGroup extends Model
{
    protected $table = 'systems_group';
    protected $guarded = [];
    public $incrementing = false;

    public function groupSystems()
    {
        return $this->belongsTo('App\SystemList', 'systems_id', 'id');
       // return $this->hasMany('App\SystemList', 'id')->orderBy('sys_name');
    }

    public function systemContacts()
    {
        return $this->hasMany('App\SystemsContacts', 'systems_id', 'systems_id')->orderBy('Pri');
    }

    public function getCreatedAtAttribute($date)
    {
        if(Auth::check())
            return Carbon::createFromFormat('Y-m-d H:i:s', $date)->copy()->tz(Auth::user()->timezone)->format('F j, Y @ g:i A');
        else
            return Carbon::createFromFormat('Y-m-d H:i:s', $date)->copy()->tz('America/Toronto')->format('F j, Y @ g:i A');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('F j, Y @ g:i A');
    }
}
