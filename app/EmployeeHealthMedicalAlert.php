<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeHealthMedicalAlert extends Model
{
    use SoftDeletes;
    protected $table = 'employee_health_medical_alerts';
    protected $guarded = [];
    public $incrementing = false;

    public function createdBy()
    {
        return $this->hasOne('App\User', 'employee_number', 'created_by');
    }
    
}
