<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider

{
    //public $query;
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        //not working
        'Illuminate\Auth\Events\Login' => ['App\Listeners\UpdateLastLoginOnLogin@login',],
        'Illuminate\Auth\Events\Logout' => ['App\Listeners\UpdateLastLoginOnLogin@logout',],
       // 'App\Listeners\UpdateLastLoginOnLogin' => ['App\Listeners\UpdateLastLoginOnLogin@searchEmployee',]
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
