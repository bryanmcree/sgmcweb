<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PRTwo extends Model
{
    use SoftDeletes;
    protected $table = 'patient_relations_part_two';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    public function completedBy()
    {
        return $this->hasOne('App\User', 'employee_number', 'part_two_completed_by');
    }

    public function patient()
    {
        return $this->hasOne('App\PublicRelationsPatients', 'pat_enc_csn_id', 'encounter_id');
    }
}
