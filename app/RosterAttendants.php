<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RosterAttendants extends Model
{
    use SoftDeletes;
    protected $table = 'roster_attendants';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    public function attendantInfo()
    {
        return $this->hasOne('App\User', 'employee_number', 'employee_number');
    }
}
