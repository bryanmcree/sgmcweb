<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EventScheduleTime extends Model
{
    use SoftDeletes;
    protected $table = 'event_schedule_time';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    public function reservedBy()
    {
        return $this->hasOne('App\User', 'employee_number', 'reserved_by');
    }
}
