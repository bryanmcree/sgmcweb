<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductivityUnitTotals extends Model
{
    protected $table = 'ProductivityUnitTotals';
}
