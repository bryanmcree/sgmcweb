<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SGMCCensusPatients extends Model
{
    protected $table = 'SGMC_census_temp';
    protected $dateFormat = 'Y-m-d H:i:s';
}
