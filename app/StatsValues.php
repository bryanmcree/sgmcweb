<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StatsValues extends Model
{
    use SoftDeletes;
    protected $table = 'SGMC_Stats_Values';
    protected $guarded = [];
    //public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $dates = ['start_date'];


    public function statsMain()
    {
        return $this->hasOne('App\Stats', 'stat_link', 'stat_link');
    }
}
