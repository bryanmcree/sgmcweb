<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SecurityViolation extends Model
{
    protected $table = 'security_violations';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';
}
