<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AuditAnswer extends Model
{
    use SoftDeletes;
    public $incrementing = FALSE;
    protected $table = 'audit_answers';
    protected $guarded = [];
    protected $dateFormat = 'Y-m-d H:i:s';

    public function submittedBy()
    {
        return $this->hasOne('App\User', 'employee_number', 'submitted_by');
    }

    public function question_title()
    {
        return $this->belongsTo('App\AuditQuestion', 'question_id', 'id');
    }
}
