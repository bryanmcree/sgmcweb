<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DuplicateUsers extends Model
{
    protected $table = 'DuplicateUsers';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';
}
