<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PRReportSurveybyEmployee extends Model
{
    protected $table = 'PR_report_survey_by_emp';
    protected $guarded = [];

    public function completedBy()
    {
        return $this->hasOne('App\User', 'employee_number', 'emp');
    }
}
