<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CafeteriaMenu extends Model
{
    use SoftDeletes;
    protected $table = 'cafeteria_menus';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    public function createdBy()
    {
        return $this->hasOne('App\User', 'employee_number', 'created_by');
    }

    public function menuItems()
    {
        return $this->belongsToMany('App\CafeteriaItem', 'cafeteria_menu_items', 'menu_id', 'item_id');
    }

}

    