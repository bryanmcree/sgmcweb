<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CapitalRequestEmailHistory extends Model
{
    use SoftDeletes;
    protected $table = 'capital_request_email_history';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';
    //protected $dates = ['approved', 'purchased','inservice_date'];
}
