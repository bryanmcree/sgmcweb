<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestNew extends Model
{
    protected $table = 'Request';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';
}
