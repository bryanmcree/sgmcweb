<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EMSZero extends Model
{
    protected $table = 'ems_level_zero';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $dates = ['date'];
    public $timestamps = false;
}
