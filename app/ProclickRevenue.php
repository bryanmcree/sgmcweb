<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProclickRevenue extends Model
{
    protected $table = 'proclick_revenue';
    protected $primaryKey = null;
    public $incrementing = false;
}
