<?php

namespace App;
use Carbon\Carbon;
use \Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SystemList extends Model
{
    protected $table = 'systems';
    protected $guarded = [];
    public $incrementing = false;
    public $primaryKey  = 'id';
    protected $dateFormat = 'Y-m-d H:i:s';
    //protected $dateFormat = 'Y-m-d H:i:s';
    use SoftDeletes;
    

    public function systemContacts()
    {
        return $this->hasMany('App\SystemsContacts', 'systems_id')->orderby('Pri');
    }
    
    public function systemVendors()
    {
        return $this->hasMany('App\SystemsVendors', 'systems_id');
    }

    public function mailContacts()
    {
        return $this->systemContacts->lists('email_address');
    }

    public function serverList()
    {
        return $this->systemServers->lists('srv_name');
    }

    public function systemVerification()
    {
        return $this->hasMany('App\Verify', 'systems_id')->orderBy('created_at','DESC');
    }

    public function systemContacts2()
    {
        return $this->hasMany('App\SystemsContacts', 'systems_id');
    }


    public function groupList()
    {
        return $this->belongsTo('App\SystemsGroup', 'systems_id', 'id')->orderBy('sys_name');
    }

    public function systemServers()
    {
        return $this->hasMany('App\Servers', 'sys_id');
    }

    protected $appends = array('Server_Count','Contacts_Count');

    public function getServerCountAttribute()
    {
        return $this->systemServers->count();
    }

    //protected $appends = array('Contacts_Count');

    public function getContactsCountAttribute()
    {
        return $this->systemContacts2->count();
    }

}
