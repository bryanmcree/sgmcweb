<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NursingCompCategory extends Model
{
    use SoftDeletes;
    protected $table = 'nursing_comp_categories';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    public function submittedBy()
    {
        return $this->hasOne('App\User', 'employee_number', 'created_by');
    }

    public function comp_title()
    {
        return $this->belongsTo('App\NursingCompetency', 'comp_id', 'id');
    }

}
