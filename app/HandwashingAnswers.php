<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HandwashingAnswers extends Model
{
    use SoftDeletes;
    protected $table = 'handwashing_answers';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    public function categoryType()
    {
        return $this->belongsTo('App\HandwashingQuestions', 'question_id', 'id');
    }
}
