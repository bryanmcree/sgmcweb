<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use app\Events\UserLoggedIn;
use Illuminate\Http\Request;
use App\History;
use Webpatser\Uuid;

class UpdateUserMetaData extends Event
{
    use SerializesModels;
    protected $request;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function handle(UserLoggedIn $event)
    {
        $user = User::find($event->userId); // find the user associated with this event
        if(!isEmpty($user)){
            $user->last_login = new DateTime;
            $user->last_login_ip = $this->request->getClientIp();
            $user->save();
        }

    }
}
