<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EOCSafety extends Model
{
    use SoftDeletes;
    protected $table = 'EOC_safety_nomination';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    public function submittedBy()
    {
        return $this->hasOne('App\User', 'employee_number', 'submitted_by');
    }

}
