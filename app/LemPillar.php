<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LemPillar extends Model
{
    use SoftDeletes;
    protected $table = 'lem_pillar';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    //Links the User table to the ID number of the created by ID
    public function createdBy()
    {
        return $this->hasOne('App\User', 'id', 'created_by');
    }

    //Links the LemName back to the correct pillars
    public function lemName()
    {
        return $this->hasOne('App\LemName', 'id', 'lem_id');
    }

    //Links the LemName back to the correct pillars
    public function lemGoals()
    {
        return $this->hasMany('App\LemGoal', 'pillar_id', 'id');
    }

    //Links the LemName back to the correct pillars
    public function pillarSum()
    {
        return $this->sum('pillar_percent');
    }
}

