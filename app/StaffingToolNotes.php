<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StaffingToolNotes extends Model
{
    use SoftDeletes;
    protected $table = 'staffing_tool_notes';
    protected $guarded = [];
    public $incrementing = false;
    //public $primaryKey  = 'srv_id';
    //Needed to format SQL server dates
    protected $dateFormat = 'Y-m-d H:i:s';

    public function createdBy()
    {
        return $this->hasOne('App\User', 'employee_number', 'created_by');
    }
}


