<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EOCAnswers extends Model
{
    use SoftDeletes;
    protected $table = 'EOC_answers';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';


    public function eocQuestion()
    {
        return $this->hasOne('App\EOCQuestions', 'id', 'question_id');
    }

    public function eocCategory()
    {
        return $this->hasOne('App\EOCCategory', 'id', 'category_id');
    }
}
