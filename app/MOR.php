<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MOR extends Model
{
    protected $table = 'MOR_Combined';
    protected $dateFormat = 'Y-m-d H:i:s';
}
