<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemMenuPivot extends Model
{
    public $timestamps = false;
    protected $table = 'Item_Menu_Pivot';
}
