<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ScorecardKpi extends Model
{
    use SoftDeletes;
    protected $table = 'scorecard_kpi_values';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $dates = ['report_date'];

    public function customQuestions()
    {
        return $this->hasOne('App\ScorecardCustomQuestions', 'id', 'question_id');
    }

    public function customQuestions2()
    {
        return $this->hasMany('App\ScorecardCustomQuestions', 'cost_center', 'cost_center')->orderby('auto_generate');
    }


    public function kpi_answers(){

        return $this->hasMany('App\ScorecardKpi','cost_center','cost_center');
            //->where('cost_center','=',$this->cost_center)
            //->where('report_year','=',$this->report_year);
    }

    public function kpi_answers2(){

        return $this->kpi_answers()
            ->where('report_month','=','10')
            ->where('cost_center','=','7009')
            ->where('report_year','=','2017');
    }

    public function costcenters()
    {
        return $this->hasOne('App\CostCenters', 'cost_center', 'cost_center');
    }


    public function questionCount()
    {
        return $this->customQuestions()
            ->selectRaw('id, count(*) as completedtotal')
            //->where('report_date', '>=', Carbon::now()->startOfMonth())
            ->groupBy('id');
    }


    public function getCommentsCountAttribute()
    {
        // if relation is not loaded already, let's do it first
        if ( ! array_key_exists('questionCount', $this->relations))
            $this->load('questionCount');

        $related = $this->getRelationValue('questionCount');

        // then return the count directly
        return ($related) ? (int) $related->completedtotal : 0;
    }
}
