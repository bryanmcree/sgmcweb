<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserNames extends Model
{
    protected $table = 'UserNames';
    public $timestamps = false;
}
