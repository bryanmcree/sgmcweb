<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FormRequestStatus extends Model
{
    use SoftDeletes;
    protected $table = 'FormRequestStatus';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';


    public function updatedBy()
    {
        return $this->hasOne('App\User', 'employee_number', 'updated_by');
    }
}
