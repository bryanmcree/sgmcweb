<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketShareOP extends Model
{
    protected $table = 'market_share_OP';
    protected $guarded = [];
}
