<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chaplain extends Model
{
    protected $table = 'chaplain';
    protected $guarded = [];
    //public $incrementing = false;
    public $primaryKey  = 'id';
    protected $dateFormat = 'Y-m-d H:i:s';
}
