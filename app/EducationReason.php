<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EducationReason extends Model
{
    protected $table = 'educational_reasons';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';
    public $timestamps = false;
}
