<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HandwashingReportAudit extends Model
{
    public $timestamps = FALSE;
    protected $table = 'handwashing_report_audit';
    protected $dateFormat = 'Y-m-d H:i:s';
}
