<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CovidScreening extends Model
{
    use SoftDeletes;

    protected $table = 'covid_screening';
    protected $guarded = [];
    public $incrementing = FALSE;

    public function employeeName()
    {
        return $this->hasOne('App\User', 'employee_number', 'emp_number');
    }

}
