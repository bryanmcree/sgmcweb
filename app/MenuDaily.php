<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon;

class MenuDaily extends Model
{
    use SoftDeletes;
    protected $table = 'menu_daily';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $dates = ['start_date'];

    public function menuItems()
    {
        return $this->belongsToMany('App\MenuItems', 'Item_Menu_Pivot', 'menu_daily_id', 'menu_item_id');
    }

    public function createdBy()
    {
        return $this->hasOne('App\User', 'employee_number', 'created_by');
    }

    public function getCreatedAtAttribute($timestamp) {
        return Carbon::parse($timestamp)->format('M d, Y');
    }

}
