<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PIDashboardCensus extends Model
{
    protected $table = 'PI_Dashboard_census';

    public function costCenter()
    {
        return $this->hasOne('App\CostCenters', 'cost_center', 'cost_center');
    }

}
