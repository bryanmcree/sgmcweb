<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PolicyFacilities extends Model
{
    protected $table = 'policy_facilities';
    protected $guarded = [];
    public $incrementing = false;
    public $timestamps = false;
}
