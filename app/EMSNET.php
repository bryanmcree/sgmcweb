<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EMSNET extends Model
{
    protected $table = 'ems_NET';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $dates = ['date'];
    public $timestamps = false;
}
