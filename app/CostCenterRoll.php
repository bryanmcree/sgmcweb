<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CostCenterRoll extends Model
{
    protected $table = 'CostCenter_rollup';
    protected $guarded = [];
    public $timestamps = false;
}
