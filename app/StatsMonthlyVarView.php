<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatsMonthlyVarView extends Model
{
    protected $table = 'monthly_stats_out_of_variance';
}
