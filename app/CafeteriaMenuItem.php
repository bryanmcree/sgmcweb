<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CafeteriaMenuItem extends Model
{
    protected $table = 'cafeteria_menu_items';
    protected $guarded = [];
    public $incrementing = false;
    public $timestamps = false;
}
