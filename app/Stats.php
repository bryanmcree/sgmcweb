<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Stats extends Model
{
    use SoftDeletes;
    protected $table = 'SGMC_stats';
    protected $guarded = [];
    //public $incrementing = false;
    protected $dates = ['validated_date'];

    protected $dateFormat = 'Y-m-d H:i:s';


    public function statsValues()
    {
        return $this->hasMany('App\StatsValues', 'stat_link', 'stat_link')->orderby('start_date');
    }

    public function createdBy()
    {
        return $this->hasOne('App\User', 'employee_number', 'created_by');
    }

    public function validatedBy()
    {
        return $this->hasOne('App\User', 'employee_number', 'validated_by');
    }
}
