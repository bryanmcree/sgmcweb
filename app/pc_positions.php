<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class pc_positions extends Model
{
    use SoftDeletes;
    protected $table = 'pc_positions';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    //Lets link the cost center number in positions to a description.
    public function description()
    {
        return $this->hasOne('App\PC_Cost_Centers', 'cost_center', 'cost_center');
    }

    //Lets link the cost center number in positions to a description.
    public function employee()
    {
        return $this->hasOne('App\User', 'employee_number', 'employee_number');
    }



}
