<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SurveyAnswers extends Model
{
    use SoftDeletes;
    protected $table = 'survey_answers';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    public function submittedBy()
    {
        return $this->hasOne('App\User', 'employee_number', 'submitted_by');
    }

    public function employeeSelect()
    {
        return $this->hasOne('App\User', 'employee_number', 'answer');
    }

    public function costCenter()
    {
        return $this->hasOne('App\CostCenters', 'cost_center', 'answer');
    }

    public function surveyQuestion()
    {
        return $this->hasOne('App\Questions', 'id', 'question_id');
    }

    public function dropDown()
    {
        return $this->hasOne('App\SurveyDropdown', 'id', 'answer_string');
    }
}
