<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NursingCompetency extends Model
{
    use SoftDeletes;
    protected $table = 'nursing_competencies';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    public function users()
    {
        return $this->belongsToMany('App\User', 'nurse_competencies', 'comp_id', 'nurse_id');
    }
}
