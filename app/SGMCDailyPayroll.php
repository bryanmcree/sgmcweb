<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SGMCDailyPayroll extends Model
{

    protected $table = 'SGMC_Daily_Payroll';
    protected $dateFormat = 'Y-m-d H:i:s';
}
