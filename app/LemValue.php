<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LemValue extends Model
{
    use SoftDeletes;
    protected $table = 'lem_values';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';


    //Links the User table to the ID number of the created by ID
    public function evaluated()
    {
        return $this->hasOne('App\User', 'id', 'emp_id');
    }

    //Links the LemName back to the correct pillars
    public function lemName()
    {
        return $this->hasOne('App\LemName', 'id', 'lem_id');
    }

    //Links the User table to the ID number of the created by ID
    public function evalBy()
    {
        return $this->hasOne('App\User', 'id', 'evaluator');
    }
}
