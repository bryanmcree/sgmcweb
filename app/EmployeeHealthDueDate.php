<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeHealthDueDate extends Model
{
    use SoftDeletes;
    protected $table = 'employee_health_next_due_date';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $dates = ['next_due_date'];

    public function createdBy()
    {
        return $this->hasOne('App\User', 'employee_number', 'next_due_date');
    }
}
