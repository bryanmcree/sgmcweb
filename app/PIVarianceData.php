<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PIVarianceData extends Model
{
    protected $table = 'pi_variance_form_data';
}
