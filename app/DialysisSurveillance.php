<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DialysisSurveillance extends Model
{
    use SoftDeletes;
    protected $table = 'dialysis_surveillance';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    public function createdBy()
    {
        return $this->hasOne('App\User', 'employee_number', 'created_by');
    }

}
