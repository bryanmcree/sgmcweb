<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditStandardList extends Model
{
    protected $table = 'audit_standards';
}
