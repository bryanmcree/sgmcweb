<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatsReportView extends Model
{
    protected $table = 'stats_all_export';
}
