<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CurrentlyClockedIn extends Model
{
    protected $table = 'Trend_currently_clocked_in';


    public function getCreatedAtAttribute($date)
    {
        if(Auth::check())
            return Carbon::createFromFormat('Y-m-d H:i:s', $date)->copy()->tz(Auth::user()->timezone)->format('F j, Y @ g:i A');
        else
            return Carbon::createFromFormat('Y-m-d H:i:s', $date)->copy()->tz('America/Toronto')->format('F j, Y @ g:i A');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('F j, Y @ g:i A');
    }
}
