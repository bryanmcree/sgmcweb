<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HandwashingComplete extends Model
{
    protected $table = 'handwashing_complete';

    public function costCenters(){

        return $this->hasOne('App\CostCenters', 'cost_center', 'cost_center');
    }

    public function manager()
    {
        return $this->hasOne('App\User', 'employee_number', 'employee_number');
    }

    public function handwashingCC(){

        return $this->hasOne('App\Handwashing', 'cost_center', 'cost_center');
    }

}
