<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tardy extends Model
{
    protected $table = 'tardyreport';
    protected $dateFormat = 'M j Y h:i:s:000A';
    
}
