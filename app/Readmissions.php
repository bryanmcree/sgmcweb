<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Readmissions extends Model
{

    protected $table = 'readmission';
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $dates = ['index_disch_date','readmit_adm_date'];

}
