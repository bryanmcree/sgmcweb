<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EOC extends Model
{
    use SoftDeletes;
    protected $table = 'EOC_survey';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    public function eocSurveyor()
    {
        return $this->hasOne('App\User', 'employee_number', 'surveyor');
    }

    public function eocCostCenter()
    {
        return $this->hasOne('App\CostCenters', 'cost_center', 'cost_center');
    }
}
