<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventScheduleReserve extends Model
{
    protected $table = 'event_schedule_cc';
    protected $guarded = [];
    public $incrementing = false;
    public $timestamps = false;

    public function reservedBy()
    {
        return $this->hasOne('App\User', 'employee_number', 'reserved_by');
    }

}
