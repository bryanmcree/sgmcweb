<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PIDashboardClockedIn extends Model
{
    protected $table = 'PI_Dashboard_clockedIn';
    protected $dateFormat = 'Y-m-d H:i:s';

    public function costCenter()
    {
        return $this->hasOne('App\CostCenter', 'cost_center', 'cost_center');
    }

    public function empoyee()
    {
        return $this->hasOne('App\User', 'employee_number', 'employee_number');
    }
}
