<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PCCRTCurrent extends Model
{
    protected $table = 'pcc_rt_current_patients';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';
}
