<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClockedIn extends Model
{
    protected $table = 'clocked_in';

    public function costCenter()
    {
        return $this->hasOne('App\CostCenter', 'cost_center', 'cost_center');
    }

    public function employee()
    {
        return $this->hasOne('App\User', 'employee_number', 'employee_number');
    }
}
