<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditQuestionStandard extends Model
{
    protected $table = 'audit_question_standard';
    protected $guarded = [];
    public $timestamps = FALSE;
    public $incrementing = FALSE;
}
