<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AxiomEarningDetail extends Model
{
    protected $table = 'Axiom_Pay_Codes';
}
