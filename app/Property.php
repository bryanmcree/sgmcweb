<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $table = 'sgmc_properties';
    protected $guarded = [];
}
