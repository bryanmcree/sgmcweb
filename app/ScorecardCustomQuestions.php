<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ScorecardCustomQuestions extends Model
{
    use SoftDeletes;
    protected $table = 'scorecard_custom_questions';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    public function costcenters()
    {
        return $this->hasOne('App\CostCenters', 'cost_center', 'cost_center');
    }
}
