<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExecSummary extends Model
{
    protected $table = 'exec_summary';
    protected $guarded = [];
    public $incrementing = true;
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $dates = ['report_date'];
}
