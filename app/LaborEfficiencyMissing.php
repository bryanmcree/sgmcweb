<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LaborEfficiencyMissing extends Model
{
    protected $table = 'LE_missing_entry';
}
