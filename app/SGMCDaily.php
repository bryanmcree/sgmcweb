<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SGMCDaily extends Model
{
    use SoftDeletes;
    protected $table = 'SGMC_Daily_Dashboard';
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $dates = ['report_date'];

}
