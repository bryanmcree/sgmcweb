<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScorecardCostCenters extends Model
{
    protected $table = 'scorecard_cost_centers';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $primaryKey = "cost_center";

    public function costCenter()
    {
        return $this->hasOne('App\CostCenters', 'cost_center', 'cost_center');
    }

    public function customQuestions()
    {
        return $this->hasMany('App\ScorecardCustomQuestions', 'cost_center', 'cost_center');
    }


}
