<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CapitalRequestFile extends Model
{
    use SoftDeletes;
    protected $table = 'capital_request_files';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';
    //protected $dates = ['approved', 'purchased','inservice_date'];

    public function added_by()
    {
        return $this->hasOne('App\User', 'employee_number', 'created_by');
    }
}
