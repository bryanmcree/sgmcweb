<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AuditQuestion extends Model
{
    use SoftDeletes;
    public $incrementing = FALSE;
    protected $table = 'audit_questions';
    protected $guarded = [];
    protected $dateFormat = 'Y-m-d H:i:s';

    public function createdBy()
    {
        return $this->hasOne('App\User', 'employee_number', 'created_by');
    }

    public function question_answers()
    {
        return $this->hasMany('App\SurveyAnswers', 'question_id', 'id');
    }
}
