<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SecurityCitation extends Model
{
    use SoftDeletes;
    protected $table = 'security_citations';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    public function violations()
    {
        return $this->hasMany('App\SecurityViolation', 'citation_id');
    }

}
