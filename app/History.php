<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use \Auth;

class History extends Model
{
    protected $table = 'history';
    //protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    public function user_id()
    {
        return $this->hasOne('App\User', 'username', 'userid');
    }
}
