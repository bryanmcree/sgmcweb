<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ScorecardComments extends Model
{
    use SoftDeletes;
    protected $table = 'scorecard_comments';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    public function costCenter()
    {
        return $this->hasOne('App\CostCenters', 'cost_center', 'cost_center');
    }

}
