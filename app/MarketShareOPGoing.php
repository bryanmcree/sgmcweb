<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketShareOPGoing extends Model
{
    protected $table = 'market_share_OP_going_to';
    protected $guarded = [];
}
