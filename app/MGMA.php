<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MGMA extends Model
{
    protected $table = 'mgma';
    protected $guarded = [];
    public $incrementing = false;
    public $timestamps = false;
    public $primaryKey  = 'Specialty';
}
