<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReportName extends Model
{
    use SoftDeletes;
    protected $table = 'report_name';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    public function requestedBy()
    {
        return $this->hasOne('App\User', 'employee_number', 'requested_by');
    }


    public function reportDocs()
    {
        return $this->hasMany('\App\ReportDocs','report_id','id');
    }

    public function reportDetail()
    {
        return $this->hasOne('\App\ReportDetails','report_id','id');
    }

}
