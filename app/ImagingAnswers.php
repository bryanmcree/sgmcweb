<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ImagingAnswers extends Model
{
    use SoftDeletes;
    protected $table = 'imaging_answers';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    public function categoryType()
    {
        return $this->belongsTo('App\ImagingQuestions', 'question_id', 'id');
    }
}
