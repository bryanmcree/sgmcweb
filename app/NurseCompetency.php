<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NurseCompetency extends Model
{
    use SoftDeletes;
    protected $table = 'nurse_competencies';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    public function compTitle()
    {
        return $this->hasOne('App\NursingCompetency', 'id', 'comp_id');
    }

    public function nurseName()
    {
        return $this->hasOne('App\User', 'id', 'nurse_id');
    }

}
