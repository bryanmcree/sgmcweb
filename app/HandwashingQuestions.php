<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HandwashingQuestions extends Model
{
    use SoftDeletes;
    protected $table = 'handwashing_questions';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';


    public function handwashing_answers(){

        return $this->hasMany('App\HandwashingAnswers', 'question_id', 'id');
    }
}
