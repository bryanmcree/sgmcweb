<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataSubmit extends Model
{
    protected $table = 'DataSubmission';
    protected $guarded = [];
    public $incrementing = false;
}
