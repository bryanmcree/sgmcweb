<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceRecovery extends Model
{
    use SoftDeletes;
    protected $table = 'service_recovery';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';


    public function cost_centers(){

        return $this->hasOne('App\CostCenters', 'cost_center', 'cost_center');
    }
}
