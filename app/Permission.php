<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use \Auth;
class Permission extends Model
{
    public $timestamps = true;
    protected $guarded = [];
    public $incrementing = false;

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function right()
    {
        return $this->belongsToMany(Right::class);
    }

    public function getCreatedAtAttribute($date)
    {
        if(Auth::check())
            return Carbon::createFromFormat('Y-m-d H:i:s', $date)->copy()->tz(Auth::user()->timezone)->format('F j, Y @ g:i A');
        else
            return Carbon::createFromFormat('Y-m-d H:i:s', $date)->copy()->tz('America/Toronto')->format('F j, Y @ g:i A');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('F j, Y @ g:i A');
    }
}
