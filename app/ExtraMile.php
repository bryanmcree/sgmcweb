<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExtraMile extends Model
{
    protected $table = 'ExtraMile';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    public function receiverInfo()
    {
        return $this->hasOne('App\User', 'employee_number', 'receiver_id');
    }

    public function presenterInfo()
    {
        return $this->hasOne('App\User', 'employee_number', 'presenter_id');
    }
}
