<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeHealthGrits extends Model
{
    use SoftDeletes;
    protected $table = 'employee_health_grits';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $dates = ['followup_date'];

    public function createdBy()
    {
        return $this->hasOne('App\User', 'employee_number', 'created_by');
    }
}
