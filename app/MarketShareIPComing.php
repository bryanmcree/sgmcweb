<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketShareIPComing extends Model
{
    protected $table = 'market_share_IP_coming_from';
    protected $guarded = [];
}
