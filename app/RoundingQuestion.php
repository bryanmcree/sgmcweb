<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoundingQuestion extends Model
{
    protected $table = 'rounding_questions';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';
}
