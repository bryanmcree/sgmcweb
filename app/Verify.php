<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Verify extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    public $incrementing = false;
    protected $table = 'systems_verify';
    protected $dateFormat = 'Y-m-d H:i:s';


}
