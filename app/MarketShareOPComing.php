<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketShareOPComing extends Model
{
    protected $table = 'market_share_OP_coming_from';
    protected $guarded = [];
}
