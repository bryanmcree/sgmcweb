<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SGMCCensusEmp extends Model
{
    protected $table = 'SGMC_census_emp';
    protected $dateFormat = 'Y-m-d H:i:s';
}
