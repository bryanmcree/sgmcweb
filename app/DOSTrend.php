<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DOSTrend extends Model
{
    protected $table = 'DOS_trend';
    protected $dateFormat = 'Y-m-d';
}
