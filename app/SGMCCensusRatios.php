<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SGMCCensusRatios extends Model
{
    protected $table = 'SGMC_census_ratios';
}
