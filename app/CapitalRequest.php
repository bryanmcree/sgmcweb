<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CapitalRequest extends Model
{
    use SoftDeletes;
    protected $table = 'capital_request';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $dates = ['approved', 'purchased','inservice_date','item_received','denied_date',
        'forwarded_legal'];

    public function createdBy()
    {
        return $this->hasOne('App\User', 'employee_number', 'created_by');
    }

    public function projectManager()
    {
        return $this->hasOne('App\User', 'employee_number', 'project_manager');
    }

}
