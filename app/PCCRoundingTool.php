<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PCCRoundingTool extends Model
{
    use SoftDeletes;
    protected $table = 'pcc_rounding_tool';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    public function foleys()
    {
        return $this->hasMany('App\PCCFoley', 'pcc_rt_id');
    }

    public function centrals()
    {
        return $this->hasMany('App\PCCCentralLine', 'pcc_rt_id');
    }
}
