<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExecSummaryBudget extends Model
{
    protected $table = 'exec_summary_budget';
    protected $guarded = [];
    public $incrementing = true;
    protected $dateFormat = 'Y-m-d H:i:s';

}
