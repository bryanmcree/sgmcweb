<?php
//Unsecured Routes
Route::get('/', function () {return view('hpf');});

Route::get('/loading/test', function () {return view('loadTest');});

// public files

// Route::get('/')

//Menu Show
//Route::get('/menu/admin/show/{id}', 'MenuController@show'); //RESTORE LATER
Route::get('/menu/admin/show/Grill', 'MenuController@showGrill');
Route::get('/menu/admin/show/Deli', 'MenuController@showDeli');
Route::get('/menu/admin/show/Salad', 'MenuController@showSalad');

// Cafeteria Show
Route::get('/cafeteria/show/{menu}', 'CafeteriaController@menu');

//Cutural Survey
Route::get('/SGMCCultureSurvey', 'SGMCCultureSurveyController@index');
Route::POST('/SGMCCultureSurvey/submit', 'SGMCCultureSurveyController@submit');

//Route::get('/ie', function () {return view('index');});
//Route::get('emp/test', 'EmployeeController@test');
Route::POST('/extramileTemp/request', 'ExtraMileController@sendRequest');
Route::get('extramileTemp/request', 'ExtraMileController@requestAccess');
Route::POST('/user/new', 'UserController@storeNew' );
Route::resource('data', 'DataSubmitController');
Route::get('/ie', 'HomeController@ie');

Route::get('/hw', 'HandwashingController@index');
Route::POST('/hw/submit', 'HandwashingController@submit_survey_Anonymous');

//Menu Boards
Route::get('/menu', 'MenuController@display');

// dashboard blank
Route::get('/analytics/dashboard', function () {return view('blank_dashboard');});


//sysaid dashboard
Route::get('/sysaid', 'SysaidDashboardController@index');
//sysaid data dumps
Route::get('/sysaid/category', 'SysaidDashboardController@sysaid_category');
Route::get('/sysaid/isepic', 'SysaidDashboardController@is_epic');
Route::get('/sysaid/byday', 'SysaidDashboardController@by_day');
Route::get('/sysaid/byshift', 'SysaidDashboardController@by_shift');
Route::get('/sysaid/byhour', 'SysaidDashboardController@by_hour');
Route::get('/sysaid/byuser', 'SysaidDashboardController@by_user');
Route::get('/sysaid/bymanager', 'SysaidDashboardController@by_manager');
Route::get('/sysaid/byyear', 'SysaidDashboardController@by_year');
Route::get('/sysaid/byesclation', 'SysaidDashboardController@by_esclation');
Route::get('/sysaid/is_breakdown', 'SysaidDashboardController@is_breakdown');
Route::get('/sysaid/epic_breakdown', 'SysaidDashboardController@epic_breakdown');

//Compliance Survey - public routes...  Secure routs below
Route::get('/compliance/survey', 'ComplianceController@index');
Route::post('/compliance/survey/complete', 'ComplianceController@AddNewSurvey');
Route::get('/compliance/survey/certificate', 'ComplianceController@certificate');

//Guest Group
Route::group(['middleware' => ['guest']], function () {
});


//Auth included files (login, logout etc)
Route::auth();
//Secure Routes - Will require login
Route::group(['middleware' => ['auth']], function () {

//Labor Efficiency
    Route::get('/labor-efficiency', 'LaborEfficiencyController@index');
    Route::get('/labor-efficiency/show/{cc}', 'LaborEfficiencyController@show');
    Route::get('/labor-efficiency/division/{division}', 'LaborEfficiencyController@division');
    Route::get('/labor-efficiency/cc-report', 'LaborEfficiencyController@ccReport');
    Route::POST('/labor-efficiency/store', 'LaborEfficiencyController@store');
    Route::POST('/labor-efficiency/update', 'LaborEfficiencyController@update');
    Route::POST('/labor-efficiency/report', 'LaborEfficiencyController@report');
    Route::get('/labor-efficiency/report/extract', 'LaborEfficiencyController@reportExtract');
    Route::get('/labor-efficiency/report/extract/division', 'LaborEfficiencyController@reportExtractDivision');

//COVID-19
    // Route::get('/covid/dashboard', 'CovidController@index');
    // Route::get('/covid/create', 'CovidController@create');
    // Route::POST('/covid/store', 'CovidController@store');
    // Route::get('/covid/destroy/{covid}', 'CovidController@destroy');

//Palliative Care
    Route::get('/palliative', 'PalliativeController@index');
    Route::POST('/palliative/patient/add', 'PalliativeController@addPatient');
    Route::POST('/palliative/patient/visit/add', 'PalliativeController@addVisit');
    Route::get('/palliative/patient/{id}', 'PalliativeController@detail');
    Route::get('/palliative/patient/delete/{id}', 'PalliativeController@deletePatient');
    Route::get('/palliative/patient/visit/delete/{id}', 'PalliativeController@deleteVisit');

//Axiom Reports
    Route::get('/axiom', 'AxiomController@index');
    Route::get('/axiom/paycodes', 'AxiomEarningDetailController@pay_codes');
    Route::get('/axiom/jobcodes', 'AxiomController@job_codes');

//Market Share Maps
    Route::get('/market-share', 'MarketShareController@index');
    Route::get('/market-share/print-all', 'MarketShareController@printAll');
    // Route::get('/market-share/inpatient-map/{category}', 'MarketShareController@inpatient');
    Route::get('/market-share/inpatient-going/{category}', 'MarketShareController@inpatientGoing');
    Route::get('/market-share/inpatient-coming/{category}', 'MarketShareController@inpatientComing');
    Route::get('/market-share/outpatient-going/{category}', 'MarketShareController@outpatientGoing');
    Route::get('/market-share/outpatient-coming/{category}', 'MarketShareController@outpatientComing');
    Route::get('/market-share/ip-going/print/{drg}', 'MarketShareController@ipGoingPrint');
    Route::get('/market-share/ip-coming/print/{drg}', 'MarketShareController@ipComingPrint');
    Route::get('/market-share/op-going/print/{county}', 'MarketShareController@opGoingPrint');
    Route::get('/market-share/op-coming/print/{facility}', 'MarketShareController@opComingPrint');

// SGMC Properties
    Route::get('/properties', 'PropertiesController@index');

//SGMC Daily PI
    Route::get('/daily', 'SGMCDailyController@index');
    Route::get('/daily/download/overtime', 'SGMCDailyController@download_overtime');
    Route::get('/daily/download/epic_stats', 'SGMCDailyController@download_epic_stats');
    Route::get('/daily/download/history', 'SGMCDailyController@download_history');

//SGMC Readmission
    Route::get('/readmissions', 'ReadmissionsController@index');
    Route::get('/readmissions/survey/{id}', 'ReadmissionsController@survey');

//SGMC Census
    Route::get('/census', 'SGMCCensusController@index');
    Route::POST('/census/report', 'SGMCCensusController@censusReport');

//SGMC Stats
    Route::get('/stats', 'StatsController@index');
    Route::get('/stats/detail/{id}', ['as' => 'statDetail', 'uses' => 'StatsController@detail']); // Added detail in URL because it was reading 'stats/anything_here' as 'stats/{id}'
    Route::get('/stats/create', 'StatsController@create');
    Route::POST('/stats/store', 'StatsController@store');
    Route::get('/stats/edit/{stat}', 'StatsController@edit');
    Route::POST('/stats/update/{stat}', 'StatsController@update');
    Route::get('/stats/destroy/{stat}', 'StatsController@destroy');
    Route::get('/stats/flagged', 'StatsController@flagged');
    Route::get('/stats/trashed', 'StatsController@trashed');
    Route::get('/stats/restore/{id}', 'StatsController@restore');
    Route::get('/stats/search', 'StatsController@search');
    Route::get('/stats/reports/unlinked_auto', 'StatsController@unlinked_stats_auto');
    Route::get('/stats/reports/variance', 'StatsController@variance');
    Route::get('/stats/auto', 'StatsController@auto');
    Route::get('/stats/calculated', 'StatsController@calculated');
    Route::get('/stats/download', 'StatsController@download_stats');
    Route::get('/stats/download/axiom', 'StatsController@download_stats_axiom');
    Route::get('/stats/stat-zero/{stat}', 'StatsController@zeroStat');
    Route::get('/stats/empty', 'StatsController@empty');
    Route::get('/stats/flex', 'StatsController@flexStats');
    Route::get('/stats/finance', 'StatsController@financeStats');
    Route::get('/stats/primary', 'StatsController@primaryStats');
    // Stat Values
    Route::POST('/stats/values', 'StatValuesController@store');
    Route::POST('/stats/values/edit', 'StatValuesController@edit');
    Route::POST('/stats/values/update', 'StatValuesController@update');
    Route::get('/stats/values/destroy/{value}', 'StatValuesController@destroy');
    Route::get('/stats/manual', 'StatValuesController@create');
    Route::POST('/stats/manual/update', 'StatValuesController@updateManual');
    Route::get('/stats/excel', 'StatValuesController@excel');
    Route::POST('/stats/variance-explanation', 'StatValuesController@varianceExplanation');
    Route::get('/stats/endUser/create', 'StatValuesController@userCreate');
    Route::POST('/stats/endUser/store', 'StatValuesController@userStore');
    Route::POST('/stats/recalculate', 'StatValuesController@recalculate');

    // MOR View
    Route::get('/mor', 'MORController@index');

// Productivity Index Variance
    Route::get('/variance', 'VarianceController@index');
    Route::get('/variance/create', 'VarianceController@create');
    Route::POST('/variance/store', 'VarianceController@store');
    Route::get('/variance/show/{var}', 'VarianceController@show');
    Route::get('/variance/print/{var}', 'VarianceController@print');
    Route::get('/variance/destroy/{var}', 'VarianceController@destroy');

// Event Scheduling Application
    Route::get('/events', 'EventController@index');
    Route::get('/events/detail/{event}', 'EventController@detail');
    Route::POST('/events/store', 'EventController@store');
    Route::POST('/events/update', 'EventController@update');
    Route::get('/events/destroy/{event}', 'EventController@destroy');
    Route::POST('/events/activate/{event}', 'EventController@active');
    Route::POST('/events/roster/{event}', 'EventController@roster');
    //Schedule
    Route::POST('/events/schedule/store', 'EventScheduleController@store');
    Route::POST('/events/schedule/{reserve}', 'EventScheduleController@reserve');
    Route::get('/events/cancel/{reserve}', 'EventScheduleController@cancel');
    Route::get('/events/complete/{reserve}', 'EventScheduleController@complete');
    Route::POST('/events/notes', 'EventScheduleController@notes');

// Educational Attendance Roster
    Route::get('/education', 'EducationalController@index');
    Route::get('/education/show/{edu}', 'EducationalController@show');
    Route::get('/education/print/{edu}', 'EducationalController@print');
    Route::get('/education/create', 'EducationalController@create');
    Route::POST('/education/store', 'EducationalController@store');
    Route::get('/education/edit/{edu}', 'EducationalController@edit');
    Route::POST('/education/update/{edu}', 'EducationalController@update');
    Route::get('/education/destroy/{edu}', 'EducationalController@destroy');

// Policies and Procedures
    Route::get('/policies', 'PolicyController@index');
    Route::get('/policies/create', 'PolicyController@create');
    Route::POST('/policies/store', 'PolicyController@store');
    Route::get('/policies/download/{policy}', 'PolicyController@download');
    Route::POST('/policies/active/{policy}', 'PolicyController@active');
    Route::get('/policies/edit/{policy}', 'PolicyController@edit');
    Route::POST('/policies/update/{policy}', 'PolicyController@update');

// PCC RT Rounding Tool
    Route::get('/pcc', 'PCCRoundingController@index');
    Route::get('/pcc/create/{id}', 'PCCRoundingController@create');
    Route::POST('/pcc/store', 'PCCRoundingController@store');
    Route::get('/pcc/edit/{survey}', 'PCCRoundingController@edit');
    Route::get('/pcc/show/{survey}', 'PCCRoundingController@show');
    Route::POST('/pcc/report', 'PCCRoundingController@report');
    Route::POST('/pcc/update/{survey}', 'PCCRoundingController@update');
    Route::get('/pcc/destroy/{survey}', 'PCCRoundingController@destroy');
    Route::get('/pcc/unit/{unit}', ['as' => 'unitDetail', 'uses' => 'PCCRoundingController@unit']);

// Security & Safety
    Route::get('/security', 'SGMCSecurityController@index'); //Security Dashboard
    Route::get('/security/location_activity', 'SGMCSecurityController@DBLists'); // Show form for adding new locations, activities, and incidents
    Route::POST('/security/report', 'SGMCSecurityController@report'); // Generate Report
    Route::POST('/security/matrix', 'SGMCSecurityController@matrix'); // Generate Security Matrix
    Route::POST('/security/specific', 'SGMCSecurityController@specific'); // Generate Report for specific activities
    Route::get('/security/specific/export/{startDate}/{endDate}/{activity}', 'SGMCSecurityController@specificExport'); // Export Report for specific activities
    Route::POST('/security/activity/create', 'SGMCSecurityController@createActivity'); // Adds new activity to DB list
    Route::POST('/security/location/create', 'SGMCSecurityController@createLocation'); // Adds new location to DB list
    Route::POST('/security/incident/create', 'SGMCSecurityController@newIncident'); // Adds new incident to DB list
    Route::get('/security/activity/destroy/{activity}', 'SGMCSecurityController@activityDestroy'); // Remove Activity
    Route::get('/security/location/destroy/{location}', 'SGMCSecurityController@locationDestroy'); // Remove Location
    Route::get('/security/incident/destroy/{incident}', 'SGMCSecurityController@incidentDestroy'); // Remove Incident
// Badge
    Route::get('/security/badge', 'SGMCSecurityBadgeController@index'); // Shows form for badge issuing
    Route::POST('/security/badge/issue', 'SGMCSecurityBadgeController@store'); // Submit form for how many badges were issued
    Route::POST('/security/badge/update', 'SGMCSecurityBadgeController@update'); // Update badge issue information
    Route::get('/security/badge/destroy/{id}', 'SGMCSecurityBadgeController@destroy'); // Delete badge
// Citation
    Route::get('/security/citation', 'SGMCSecurityCitationController@index'); // Shows form for citation issuing
    Route::POST('/security/citation/issue', 'SGMCSecurityCitationController@store'); // Submit form for how many citations were issued
    Route::get('/security/citation/edit/{id}', 'SGMCSecurityCitationController@edit'); // Update badge issue information
    Route::POST('/security/citation/update/{id}', 'SGMCSecurityCitationController@update'); // Update badge issue information
    Route::get('/security/citation/destroy/{id}', 'SGMCSecurityCitationController@destroy'); // Destroy badge issue information
// Dispatch
    Route::get('/security/dispatch', 'SGMCSecurityDispatchController@index'); // Shows dispatch form
    Route::POST('/security/dispatch/create', 'SGMCSecurityDispatchController@store'); //Submit form to dispatch
    Route::get('/security/dispatch/edit/{id}', 'SGMCSecurityDispatchController@edit'); // Edit dispatches
    Route::POST('/security/dispatch/update/{id}', 'SGMCSecurityDispatchController@update'); // Update Dispatch
    Route::get('/security/dispatch/show/{id}', 'SGMCSecurityDispatchController@show'); // Show details of a dispatch
    Route::get('/security/dispatch/destroy/{dispatch}', 'SGMCSecurityDispatchController@destroy'); // Delete Dispatch
    Route::get('/security/ongoing', 'SGMCSecurityDispatchController@ongoing'); // Show list of open dispatches that need to be completed
// Rounds
    Route::get('/security/rounds', 'SGMCSecurityRoundsController@index'); // Show form for checking off an area while making rounds
    Route::get('/security/rounds/edit/{round}', 'SGMCSecurityRoundsController@edit'); // Show form for editing rounds
    Route::POST('/security/rounds/create', 'SGMCSecurityRoundsController@store'); // Submit form for rounds
    Route::POST('/security/rounds/update/{round}', 'SGMCSecurityRoundsController@update'); // Update Security Round
    Route::get('/security/rounds/destroy/{round}', 'SGMCSecurityRoundsController@destroy'); // Delete Rounds*
// Incident
    Route::get('security/incident', 'SGMCSecurityIncidentController@index'); // Shows form for incident
    Route::POST('/security/incident/store', 'SGMCSecurityIncidentController@store'); // Create new Incident
    Route::get('/security/incident/edit/{incident}', 'SGMCSecurityIncidentController@edit'); // show form for editing incident
    Route::POST('/security/incident/update/{incident}', 'SGMCSecurityIncidentController@update'); // update incident
    Route::get('/security/incident/view/{incident}', 'SGMCSecurityIncidentController@show'); // view incident report
    Route::get('/security/incidentReport/destroy/{incident}', 'SGMCSecurityIncidentController@destroy'); // delete an incident report
// Trespass
    Route::get('/security/trespass', 'SGMCSecurityTrespassController@index');
    Route::POST('/security/trespass/store', 'SGMCSecurityTrespassController@store');
    Route::get('/security/trespass/destroy/{trespass}', 'SGMCSecurityTrespassController@destroy');
    Route::get('/security/trespass/show/{trespass}', 'SGMCSecurityTrespassController@show');
    Route::get('/security/trespass/edit/{trespass}', 'SGMCSecurityTrespassController@edit');
    Route::POST('/security/trespass/update/{trespass}', 'SGMCSecurityTrespassController@update');
// Forensic Officer Orientated
    Route::get('/security/orientation', 'SGMCSecurityOrientationController@index');
    Route::POST('/security/orientation/store', 'SGMCSecurityOrientationController@store');
    Route::get('/security/orientation/destroy/{orientation}', 'SGMCSecurityOrientationController@destroy');
    Route::get('/security/orientation/edit/{orientation}', 'SGMCSecurityOrientationController@edit');
    Route::POST('/security/orientation/update/{orientation}', 'SGMCSecurityOrientationController@update');


//Employee Health
    Route::get('/employee_health/{id}', 'EmployeeHealthController@index');
    Route::POST('/employee_health/new_screen', 'EmployeeHealthController@new_screen');
    Route::POST('/employee_health/enter_bp', 'EmployeeHealthController@enter_bp'); // enter bp
    Route::POST('/employee_health/enter_weight', 'EmployeeHealthController@enter_weight'); //enter weight
    Route::get('/employee_health/hs/{id}', 'EmployeeHealthController@health_screen'); //Healths Screen Detail
    Route::POST('/employee_health/hs/update', 'EmployeeHealthController@update_hs'); // Update hs main
    Route::POST('/employee_health/enter_note', 'EmployeeHealthNotesController@enter_note'); // enter bp
    Route::POST('/employee_health/edit_note', 'EmployeeHealthNotesController@edit_note'); // enter bp
    Route::POST('/employee_health/edit_allergies', 'EmployeeHealthController@edit_allergies'); // enter bp
    Route::POST('/employee_health/edit_grits', 'EmployeeHealthGritsController@edit_grits'); // edit grits number
    Route::POST('/employee_health/next_due_date', 'EmployeeHealthDueDateController@next_due'); // custom next due date
    Route::POST('/employee_health/medicalAlert/create', 'EmployeeHealthMedAlertsController@create'); // create new medical alert
    Route::POST('/employee_health/update_medAlert', 'EmployeeHealthMedAlertsController@updateAlert'); // update medical alerts
    Route::get('/employee_health/remove/{id}', 'EmployeeHealthMedAlertsController@deleteAlert');  // Delete medical alert
    Route::POST('/employee_health/TST/create', 'EmployeeHealthVaccinationController@createTST'); // Submit result for TST form
    Route::get('/employee_health/tst/{id}', 'EmployeeHealthVaccinationController@viewTST'); // View results of TST form
    Route::POST('/employee_health/TST/edit', 'EmployeeHealthVaccinationController@editTST'); // Edit TST form


//security
    Route::get('/securityBulk', 'SecurityController@index');
    Route::post('/security/assign', 'SecurityController@assign_roles');

//AllSpice Menu Application
    Route::get('/menu/admin', 'MenuController@index');
    Route::get('/menu/admin/items', 'MenuController@items');
    Route::POST('/menu/admin/additem', 'MenuController@addItem');
    Route::get('/menu/admin/menus', 'MenuController@menus');
    Route::POST('menu/admin/addmenu', 'MenuController@addMenu');
    // ITEMS //
    Route::get('/menu/admin/editItem/{id}', 'MenuController@editItem');
    Route::POST('/menu/admin/updateItem/{id}', 'MenuController@updateItem');
    Route::get('/menu/admin/destroyItem/{id}', 'MenuController@destroyItem');
    // MENUS //
    //Route::get('/menu/admin/show/{id}', 'MenuController@show');  Moved to unsecure
    Route::get('/menu/admin/edit/{id}', 'MenuController@edit');
    Route::POST('/menu/admin/update/{id}', 'MenuController@update');
    Route::get('/menu/admin/destroy/{id}', 'MenuController@destroy');

// Cafeteria Menu Application
    Route::get('/cafeteria', 'CafeteriaController@index');
    Route::POST('/cafeteria/menu/store', 'CafeteriaController@store');
    Route::get('/cafeteria/menu/items/{menu}', 'CafeteriaController@items'); // Form for Adding Items to Menu
    Route::POST('/cafeteria/menu/items/store/{menu}', 'CafeteriaController@storeItems'); // Store items to menu
    // View Route is outside of auth group

    //Items
    Route::POST('/cafeteria/item/store', 'CafeteriaItemsController@store');
    Route::get('/cafeteria/items', 'CafeteriaItemsController@index');
    Route::POST('/cafeteria/item/update', 'CafeteriaItemsController@update');
    Route::get('/cafeteria/item/destroy/{item}', 'CafeteriaItemsController@destroy');



//PI dashboard
    Route::get('/pi', 'PIController@index');

//Exec Summary
    Route::get('/exec_sum', 'ExecSummaryController@index');

//Epic report request
    Route::get('/epic_report', 'EpicReportController@index');
    Route::POST('/epic_report/submit', 'EpicReportController@submit_request');

//Parking lot DB 08012018
    Route::get('/parking', 'ParkingController@index');
    Route::POST('/parking/add', 'ParkingController@SubmitTicket');

//Secure Compliance Routes
    Route::get('/compliance/survey/admin', 'ComplianceController@dashboard');

//Secure IS Dashboard routes
    Route::get('/sysaid/licenses/microsoft', 'LicenseMicrosoftController@index');
    Route::POST('/sysaid/licenses/microsoft/add', 'LicenseMicrosoftController@add_microsoft');
    Route::get('/sysaid/licenses/imprivata', 'LicenseImprivataController@index');
    Route::POST('/sysaid/licenses/imprivata/add', 'LicenseImprivataController@add_imprivata');
    Route::get('/sysaid/licenses/citrix', 'LicenseCitrixController@index');
    Route::POST('/sysaid/licenses/citrix/add', 'LicenseCitrixController@add_citrix');
    Route::get('/sysaid/licenses/airwatch', 'LicenseAirwatchController@index');
    Route::POST('/sysaid/licenses/airwatch/add', 'LicenseAirwatchController@add_airwatch');
    Route::get('/sysaid/licenses/adobe', 'LicenseAdobeController@index');
    Route::POST('/sysaid/licenses/adobe/add', 'LicenseAdobeController@add_adobe');

//Time off request
    Route::get('/time-off', 'TimeOffController@index');
    Route::POST('/time-off/add', 'TimeOffController@add');

//Select/Update Manager Page
    Route::get('/manager', 'UserController@find_manager');
    Route::POST('/manager/update', 'UserController@manager');

//Print Routes
    Route::get('user/print', 'UserController@printList');
    Route::get('systemlist/print', 'SystemListController@printList');

//Delete Routes
    Route::get('/user/del/{id}', 'UserController@delete');
    Route::get('/role/del/{id}', 'RoleController@delete'); // Maybe Not in use ??
    Route::get('/role/destroy/{role}', 'RoleController@destroy'); // Delete Security Role from roles Table
    Route::get('/permission/del/{id}', 'PermissionController@delete');
    Route::get('/right/del/{id}', 'RightController@delete');
    Route::get('/systemlist/systemcontact/del/{id}', 'SystemListController@DeleteContact');
    Route::get('/systemlist/systemvendor/del/{id}', 'SystemListController@DeleteVendor');
    Route::get('/systemlist/inventory/del/{id}', 'SystemListController@deletefromGroup');
    Route::get('/systemlist/del/{id}', 'SystemListController@delete');
    Route::get('/systemlist/server/del/{id}', 'ServersController@DeleteServer');
    Route::get('/chaplain/del/{id}', 'ChaplainController@DeleteEntry');
    Route::get('/WebForms/delete/{id}', 'WebFormsController@Delete');
    Route::get('/lem/pillar/del/{id}', 'LemController@deletePillar');
    Route::get('/rounding/question/del/{id}', 'RoundingController@deleteQuestion');
    Route::get('/eoc/question/del/{id}', 'EOCController@deleteQuestion');
    Route::get('/eoc/category/del/{id}', 'EOCController@deleteCategory');

//Custom Routes
    Route::get('/extramile/all', 'ExtraMileController@allExtraMiles');
    Route::POST('/extramile/report', 'ExtraMileController@report');
    Route::get('/extramile/report/export/{start}/{end}', 'ExtraMileController@export');
    Route::get('/right/{id}', 'RightController@index');
    Route::get('/right', 'RightController@search');
    Route::get('/security/{id}', 'UserController@security');
    Route::POST('/payroll/reports', 'PayrollController@payrollReport');
    Route::get('/extramileTemp/create/{id}', 'ExtraMileController@create');
    Route::get('/hr/par', 'HrController@par');
    Route::get('/hr/dashboard', 'HrController@dashboard');
    Route::POST('/systemlist/update/{id}', 'SystemListController@update');
    Route::patch('/systemlist/contact/update/{id}', 'SystemListController@UpdateContact');
    Route::patch('/systemlist/vendor/update/{id}', 'SystemListController@UpdateVendor');
    Route::POST('/systemlist/addcontact', 'SystemListController@AddContact');
    Route::POST('/systemlist/addvendor', 'SystemListController@AddVendor');
    Route::get('/productivity/clockedin', 'ProductivityController@clockedin');
    Route::GET('/systemlist/inventory', 'SystemListController@inventoryIndex');
    Route::GET('/systemlist/inventory/systems', 'SystemListController@inventorySystems');
    Route::POST('/systemlist/inventory/add2Group', 'SystemListController@add2Group');
    Route::POST('/systemlist/inventory/add2system/{cost_center}', 'SystemListController@storeSystem');
    Route::POST('/systemlist/inventory/addcontact/{cost_center}', 'SystemListController@AddContact2');
    Route::GET('/systemlist/inventory/finished', 'SystemListController@finished');
    Route::get('/costcenter', 'SystemListController@searchcostcenter');
    Route::POST('/systemlist/addserver', 'SystemListController@AddServer');
    Route::POST('/systemlist/updateserver', 'SystemListController@UpdateServer');
    Route::POST('/productivity/chart', 'ProductivityController@chart');
    Route::POST('/systemlist/contact/update', 'SystemListController@updateContact');
    Route::POST('/systemlist/vendor/update', 'SystemListController@updateVendor');
    Route::get('/reports/history', 'HistoryController@reportHistory');
    Route::POST('/reports/history', 'HistoryController@reportHistory');
    Route::POST('/help/request', 'HelpController@sendHelp');
    Route::POST('/help/mobile', 'HelpController@mobileHelp');
    Route::POST('/systemlist/verify', 'SystemListController@systemsVerify');
    Route::get('/search',['uses' => 'EmployeeController@employeeSearch','as' => 'search']);
    Route::POST('/chaplain/addentry', 'ChaplainController@AddEntry');
    Route::POST('/chaplain/editentry', 'ChaplainController@EditEntry');
    Route::POST('/systemlist/contact/update/{cost_center}', 'SystemListController@updateContact2');
    Route::POST('/chaplain/search', 'ChaplainController@search');
    Route::get('/chaplain/search',['uses' => 'ChaplainController@search','as' => 'search']);

    Route::get('/chaplain/reports', 'ChaplainController@report_index');
    Route::POST('/chaplain/reports/view', 'ChaplainController@report_view');

    Route::get('/productivity/chart/print', 'ProductivityController@pushprint');
    Route::get('/rounding/show/{id}', 'RoundingController@show');
    Route::get('/productivity/chart/image/{id}', 'ProductivityController@chart2');
    Route::POST('/rounding/show/addquestion', 'RoundingController@storeQuestion');
    Route::get('/rounding/survey/{id}', 'RoundingController@takeSurvey');
    Route::POST('/rounding/complete', 'RoundingController@storingSurvey');
    Route::get('/rounding/survey/edit/{id}', 'RoundingController@modifySurvey');
    Route::POST('/user/roles', 'UserController@newRoles');
    Route::POST('/user_roles/remove', 'UserController@removeRoles');
    Route::get('/user/updatemanager/{name}', 'UserController@updateManager');
    Route::POST('/user/details/update/{id}', 'UserController@updateDetail'); // Update user details through employee search modal

    Route::get('reports/request', 'ReportNameController@index');
    //Route::POST('reports/request','ReportNameController@index');
    Route::get('/reports/request/feed','ReportRequestController@RequestFeed');
    Route::get('/reports/respond/{id}','ReportRequestController@ManagerResponse');
    Route::get('/reports/request/termination/{id}','ReportRequestController@TerminationResponse');
    Route::POST('/reports/request/termination/update','ReportRequestController@clearTerminated');
    Route::get('reports/request/dg', 'ReportNameController@dg_view');


    Route::get('/systemlist/audit','SystemListController@audit');
    Route::POST('/user/ld','UserController@updateLD');
    Route::get('extramile/new/{id}','ExtraMileController@newExtraMile');
    Route::get('extramile/download/{id}','ExtraMileController@printCertificate');
    Route::get('user/pic/{id}', 'UserController@showPicture');
    Route::get('user/detail/{id}','UserController@userDetail');
    Route::get('WebForms/','WebFormsController@index');
    Route::POST('WebForms/search','WebFormsController@search');
    Route::get('WebForms/select/{id}','WebFormsController@userSelect');
    Route::POST('WebForms/addTerm','WebFormsController@addTerm');
    Route::POST('WebForms/updateld','WebFormsController@updateLastDay');
    Route::POST('user/update','UserController@updateDetail');
    Route::get('widgets/terminations', 'WidgetController@terminations');
    Route::get('widgets/extramile', 'WidgetController@extramile');
    Route::POST('lem/addeval','LemController@addEval');
    Route::get('lem/eval/{id}','LemController@showEval');
    Route::POST('lem/pillar','LemController@editPillar');
    Route::POST('lem/pillar/add','LemController@addPillar');
    Route::POST('lem/goal/add','LemController@addGoal');
    Route::POST('lem/new','LemController@newEval');
    Route::get('lem/create/{id}','LemController@create');
    Route::POST('lem/store','LemController@storeEval');
    Route::get('WebForms/physician','WebFormsController@physicianIndex');
    Route::get('widgets/SGMCbar', 'WidgetController@SGMCbar');
    Route::get('widgets/dupes', 'WidgetController@duplicate_users');
    Route::get('widgets/dupes/del/{id}', 'WidgetController@remove_dups');
    Route::get('hr/exit', 'HrController@exitInterview');
    Route::POST('hr/exit/add','HrController@getexitInterview');
    Route::get('hr/exit/complete', 'HrController@complete');
    Route::get('hr/exit/view/{id}/edit', 'HrController@surveyView');
    Route::POST('transport/add','TransportController@newRequest');
    Route::POST('transport/assign','TransportController@assign');
    Route::get('transport/dashboard', 'TransportController@dashboard');
    Route::get('quality/imaging', 'QualityController@index');
    Route::get('transport/completed', 'TransportController@completed');
    Route::POST('reports/request/manager','ReportRequestController@managerApproval');
    Route::POST('quality/imaging/add', 'QualityController@newMeasure');
    Route::POST('hr/exit/report', 'HrController@report');
    Route::get('hr/exit/detail/{fieldname}', 'HrController@detail');
    Route::POST('rounding/show/edit/question', 'RoundingController@editQuestion');
    Route::POST('roster/Create/', 'RosterController@createRoster');
    Route::POST('/roster/update', 'RosterController@update');
    Route::get('/roster/load/{id}', 'RosterController@loadRoster');
    Route::get('/roster/download/{roster}', 'RosterController@download');
    Route::POST('/roster/attendant', 'RosterController@submitAttendant');
    Route::get('/roster/attendant/remove/{attendant}', 'RosterController@removeAttendant');
    Route::get('/roster/destroy/{roster}', 'RosterController@destroy');

    Route::get('/quality/imaging/report', 'QualityController@report');
    Route::POST('/quality/imaging/report/run', 'QualityController@notCompleted');
    Route::POST('/mgma/specialty', 'mgmaController@index');
    Route::POST('/mgma/formula', 'mgmaController@formula');

    //Patient Relations (PR) Routes
    Route::get('/pr', 'PRController@index');
    Route::POST('/pr/question/add', 'PRController@add_question');
    Route::get('/pr/survey/{encounter}', 'PRController@survey');
    Route::get('/pr/service_recovery', 'PRController@ServiceRecovery');
    Route::POST('/pr/service_recovery/add', 'PRController@AddServiceRecovery');
    Route::get('/pr/service_recovery/certificate/{id}', 'PRController@PrintCertificate');
    Route::get('/pr/floor/{floor}', 'PRController@SelectFloor');
    Route::POST('/pr/survey/add', 'PRController@NewCheck');
    Route::POST('/pr/survey/two', 'PRController@two');
    Route::POST('/pr/survey/file', 'PRController@file_upload');
    Route::get('/pr/survey/nir/{floor}', 'PRController@nir');
    Route::get('/pr/survey/ir/{floor}', 'PRController@ir');
    Route::get('/pr/survey/swing/{floor}', 'PRController@swing');
    Route::get('/pr/dashboard', 'PRController@dashboard');
    Route::POST('/pr/reports/surveysbyemployeebyfloor', 'PRController@surveysbyemployeebyfloor');
    Route::POST('/pr/filtered', 'PRController@filter');


    //EMS Routes
    Route::get('/ems', 'EMSController@index');
    Route::POST('/ems/zero', 'EMSController@zero');
    Route::get('/ems/zero/show', 'EMSController@showZero');
    Route::get('/ems/zero/destroy/{zero}', 'EMSController@zeroDestroy');
    Route::POST('/ems/editUnclassified', 'EMSController@editUnclassified');
    Route::POST('/ems/report', 'EMSController@report');
    Route::get('/ems/NET/show', 'EMSController@showNET');
    Route::get('/ems/NET/destroy/{NET}', 'EMSController@destroyNET');
    Route::POST('/ems/NET/store', 'EMSController@storeNET');
    //EMS Charts
    Route::get('/ems/charts/by_month', 'EMSController@chart_by_month');
    Route::get('/ems/scripting/map', 'EMSController@scriptMap'); // Script to map new coords
    Route::get('/ems/scripting/{ems}/{coordinates}', 'EMSController@coordinates'); // Script to map new coords
    Route::get('/ems/map', 'EMSController@map');

    //Epic Security - Mark's Report
    Route::get('/epicsecurity', 'EpicSecurityController@index');
    Route::get('/epicsecurity/download', 'EpicSecurityController@download');

    //DOS Routes
    Route::get('/dos', 'DOSController@index');
    Route::get('/dos/ACUTE_ALOS_BER_Parent_Location', 'DOSController@ACUTE_ALOS_BER_Parent_Location');

    //Chaplain Routes
    Route::POST('/chaplain/reports/print', 'ChaplainController@print_report');

    //History Routes
    Route::get('/history/daily','HistoryController@daily');

    //Employee Widget chart route
    Route::get('/employee/chart/emp', 'EmployeeController@widget_chart_json');
    Route::get('/employee/chart/prn', 'EmployeeController@prn');
    Route::get('/employee/chart/rn', 'EmployeeController@rn');
    Route::get('/employee/chart', 'EmployeeController@emp_chart');

    //EOC Routes
    Route::POST('eoc/survey/new', 'EOCController@newSurvey');
    Route::get('/eoc/survey/{id}', 'EOCController@survey');
    Route::get('/eoc/survey/category/{id}', 'EOCController@surveyCategory');
    Route::POST('eoc/survey/submit', 'EOCController@submitAnswers');
    Route::POST('/eoc/survey/documents/add', 'EOCDocsController@addFile');
    Route::get('/eoc/survey/category/edit/{id}', 'EOCController@surveyCategoryEdit');
    Route::POST('eoc/category/', 'EOCController@newCategory');
    Route::POST('eoc/question/', 'EOCController@newQuestion');
    Route::POST('eoc/question/edit', 'EOCController@editQuestion');
    Route::POST('eoc/category/edit', 'EOCController@editCategory');
    Route::GET('/eoc/survey/documents/download/{id}', 'EOCDocsController@downloadFile');
    Route::get('/eoc/report/{eoc}', 'EOCController@report');
    Route::get('/eoc/answer/edit/{answer}', 'EOCController@editAnswer');
    Route::POST('/eoc/answer/update/{answer}', 'EOCController@updateAnswer');
    Route::get('/eoc/safety/create', 'EOCSafetyController@create');
    Route::POST('/eoc/safety/store', 'EOCSafetyController@store');

    //Report Request Routes
    Route::POST('/reports/request/approval/{id}', 'ReportNameController@approveReport');
    Route::POST('/reports/request/denial/{id}', 'ReportNameController@deny_request');
    Route::POST('/report/request/new', 'ReportNameController@newReport');
    Route::get('/report/request/detail/{id}', 'ReportNameController@detailReport');
    Route::POST('/report/request/documents/add', 'ReportDocsController@addFile');
    Route::GET('/report/request/documents/download/{id}', 'ReportDocsController@downloadFile');
    Route::GET('/report/request/documents/delete/{id}', 'ReportDocsController@deleteFile');
    Route::POST('/report/request/detail/add', 'ReportNameController@detailSubmit');
    Route::get('/report/request/detail/view/{id}', 'ReportNameController@viewReport');
    Route::POST('/report/request/detail/update', 'ReportNameController@updateDetails');


    //Housekeeping Routes
    Route::get('/housekeeping', 'HousekeepingController@index');

    //UserNames Search Application
    Route::get('/usernames', 'UserNamesController@index');
    Route::post('/usernames/search', 'UserNamesController@search');

    //Schedule Application Routes
    Route::get('/schedule', 'ScheduleController@index');

    //Cost Centers Application
    Route::get('/costcenters/analyst', 'CostCentersController@analyst');
    Route::get('/costcenters/download', 'CostCentersController@download');
    Route::get('/costcenters', 'CostCentersController@index');
    Route::get('/costcenters/add', 'CostCentersController@add');
    Route::get('/costcenter/create', 'CostCentersController@create'); // Form to Create new costcenter
    Route::POST('/costcenter/store', 'CostCentersController@store'); // Store new costcenter
    Route::POST('/costcenters/update', 'CostCentersController@update');
    Route::get('/costcenters/report', 'CostCentersController@report');
    Route::get('/costcenters/report_dir/{dir}', 'CostCentersController@report_dir'); //director drill
    Route::get('/costcenters/report_cc/{dir}', 'CostCentersController@report_cc'); //cost center drill
    Route::get('/costcenters/{id}', 'CostCentersController@detail');
    Route::POST('/costcenters/edit', 'CostCentersController@edit'); // edit cost center
    Route::POST('/costcenters/notes', 'CostCentersController@notes'); // Add Notes
    Route::POST('/costcenters/edit/pi/{costcenter}', 'CostCentersController@editPI'); // Update ActionOI and PI
    Route::get('/costcenters/edit/LE', 'CostCentersController@LE');
    Route::POST('/costcenters/update/LE/{cc}', 'CostCentersController@updateLE');


    //Handwashing Routes
    Route::get('/handwashing', 'HandwashingController@dashboard');
    Route::get('/handwashing/survey', 'HandwashingController@index');
    Route::get('/handwashing/admin', 'HandwashingController@admin_index');
    Route::POST('/handwashing/admin/question/add', 'HandwashingController@add_question');
    Route::POST('/handwashing/submit', 'HandwashingController@submit_survey');
    Route::POST('/handwashing/admin/report', 'HandwashingController@admin_report');
    Route::get('/handwashing/admin/view/{id}', 'HandwashingController@view_survey');
    Route::get('/handwashing/widget', 'HandwashingController@handwashing_widget');
    Route::POST('/handwashing/update', 'HandwashingController@update');

    //NEW Handwashing Refactor

    Route::get('/handwashing2', 'HandwashingController@new_index');
    Route::get('/handwashing2/admin', 'HandwashingController@new_admin');


    //System Access Pages
    Route::get('/access', 'AccessController@index');
    Route::get('/access/employee', 'AccessController@employee');
    Route::get('/access/physician', 'AccessController@physician');

    // **** NEW SURVEY APPLICATION **** //
    Route::get('/audit', 'AuditController@index');
    Route::POST('/audit/store', 'AuditController@store');
    Route::get('/audit/details/{audit}', 'AuditController@detail');
    Route::POST('/audit/update/{audit}', 'AuditController@update');

    //Questions
    Route::POST('/audit/question/store', 'AuditQuestionsController@store');
    Route::get('/audit/short/{audit}/{question}', 'AuditQuestionsController@shortAnswer');
    Route::POST('/audit/order', 'AuditQuestionsController@updateOrder');
    Route::GET('/audit/question/destroy/{question}', 'AuditQuestionsController@destroy');

    //Answers
    Route::get('/audit/answers/create/{audit}', 'AuditAnswersController@create');
    Route::POST('/audit/answers/store', 'AuditAnswersController@store');
    Route::get('/audit/report/{audit}', 'AuditAnswersController@report');
    Route::get('/audit/report/print/{audit}', 'AuditAnswersController@print');
    Route::get('/audit/report/excel/{audit}', 'AuditAnswersController@excel');
    Route::get('/audit/shortAnswer/excel/{audit}', 'AuditAnswersController@shortExcel');

    //Survey Application
    // Route::get('/survey/employeesearch', 'SurveyController@employee_search'); //Auto complete for employees
    Route::get('/survey', 'SurveyController@inactive'); //main landing page
    Route::post('/survey/new/create', 'SurveyController@inactive'); //create new survey
    Route::get('/survey/details/{id}', ['as' => 'SurveyDetail', 'uses' => 'SurveyController@inactive']); //main landing page
    // Route::post('/survey/question/create', 'QuestionsController@add_question'); //Add question to survey
    // Route::post('/survey/submit', 'SurveyAnswersController@submit_survey'); //submit survey answers
    // Route::post('/survey/options/{id}', 'SurveyController@survey_options'); //submit survey options
    // Route::POST('/survey/report/{survey}', 'SurveyController@report'); // Detailed report view by date range

    // Route::get('/survey/question/edit/{survey}/{question}', 'QuestionsController@edit'); // Edit Question
    // Route::POST('/survey/question/update/{question}', 'QuestionsController@update'); //update question
    // Route::get('/survey/shortresults/{surveyid}/{questionid}', 'SurveyController@short_results');
    // Route::get('/survey/empresults/{surveyid}/{questionid}', 'SurveyController@emp_results');
    // Route::get('/survey/ccresults/{surveyid}/{questionid}', 'SurveyController@cc_results');
    // Route::get('/survey/datetime_results/{surveyid}/{questionid}', 'SurveyController@datetime_results');
    // Route::get('/survey/pie/{surveyid}/{questionid}', 'SurveyController@pie');

    Route::get('/survey/{id}', 'SurveyController@inactive'); //outside users survey page
    // Route::get('/survey/thankyou/{id}', 'SurveyController@thank_you'); //main landing page
    // Route::post('/survey/order', 'SurveyController@update_order'); //submit survey options
    // Route::get('/survey/deletequestion/{id}', 'SurveyController@delete_question'); //delete question
    // Route::get('/survey/deletesurvey/{id}', 'SurveyController@delete_survey'); //delete survey
    // Route::get('/survey/print/{id}', 'SurveyController@printSurvey'); //delete survey

    // Anesthesia Survey Routes Application
    Route::get('/anesthesia', 'AnesthesiaController@index');
    Route::get('/anesthesia/create', 'AnesthesiaController@create');
    Route::POST('/anesthesia/store', 'AnesthesiaController@store');
    Route::get('/anesthesia/edit/{anesthesia}', 'AnesthesiaController@edit');
    Route::POST('/anesthesia/update/{a}', 'AnesthesiaController@update');
    Route::POST('/anesthesia/report', 'AnesthesiaController@report');
    Route::get('/anesthesia/show/{anesthesia}', 'AnesthesiaController@show');

    // Dialysis log
    Route::get('/dialysis', 'DialysisController@index');
    Route::get('/dialysis/log/create', 'DialysisController@create');
    Route::POST('/dialysis/log/store', 'DialysisController@store');
    Route::get('/dialysis/log/details', 'DialysisController@show');
    Route::get('/dialysis/log/edit/{log}', 'DialysisController@edit');
    Route::POST('/dialysis/log/update/{log}', 'DialysisController@update');
    Route::get('/dialysis/log/destroy/{log}', 'DialysisController@destroy');
    Route::POST('/dialysis/report', 'DialysisController@report');
    // Dialysis surveillance
    Route::get('/dialysis/surveillance/create', 'DialysisSurveillanceController@create');
    Route::POST('/dialysis/surveillance/store', 'DialysisSurveillanceController@store');
    Route::get('/dialysis/surveillance/details', 'DialysisSurveillanceController@show');
    Route::get('/dialysis/surveillance/edit/{surveillance}', 'DialysisSurveillanceController@edit');
    Route::POST('/dialysis/surveillance/update/{surveillance}', 'DialysisSurveillanceController@update');
    Route::get('/dialysis/surveillance/destroy/{surveillance}', 'DialysisSurveillanceController@destroy');
    Route::POST('/dialysis/nurse/create', 'DialysisSurveillanceController@nurse');

    // Nursing Competency Application
    Route::get('/competencies', 'CompetencyController@index');
    Route::POST('/competencies/store', 'CompetencyController@store');
    Route::get('/competencies/show/{competency}', 'CompetencyController@show');
    Route::POST('/competencies/assign/store', 'CompetencyController@assign');
    Route::get('/competencies/destroy/{comp}', 'CompetencyController@destroy');
    Route::get('/competencies/not-complete', 'CompetencyController@notComplete');
    Route::get('/competencies/dashboard/nurse-view/{id}', 'CompetencyController@nurseDashboard');
    Route::get('/competencies/dashboard/comp-view/{id}', 'CompetencyController@compDashboard');
    //Category
    Route::POST('/competencies/category/store', 'CompetencyCategoryController@store');
    //Question
    Route::POST('/competencies/question/store', 'CompetencyQuestionController@store');
    Route::get('/competencies/question/destroy/{question}', 'CompetencyQuestionController@destroy');
    //Answers
    Route::get('/competencies/complete/{comp}', 'CompetencyAnswerController@create');
    Route::POST('/competencies/answers/update', 'CompetencyAnswerController@update');
    Route::get('/competencies/approve/view/{nurse}/{comp}/{pivot}', 'CompetencyAnswerController@approvalView');
    Route::POST('/competencies/approve/{comp}', 'CompetencyAnswerController@approve');
    Route::get('/competencies/detail/{pivot}', 'CompetencyAnswerController@detail');
    Route::POST('/competencies/notes/{pivot}', 'CompetencyAnswerController@updateNotes');

    // Preventive Maintenance App
    Route::get('/maintenance', 'PreventiveMaintController@index');
    Route::get('/maintenance/create', 'PreventiveMaintController@create');
    Route::POST('/maintenance/store', 'PreventiveMaintController@store');

    //Imaging Routes
    Route::get('/imaging', 'ImagingController@index');
    Route::get('/imaging/admin', 'ImagingController@admin_index');
    Route::POST('/imaging/survey', 'ImagingController@survey');
    Route::POST('/imaging/admin/question/add', 'ImagingController@add_question');
    Route::POST('/imaging/submit', 'ImagingController@submit_survey');
    Route::POST('/imaging/admin/report', 'ImagingController@admin_report');

    //Scorecard Routes
    Route::get('/scorecard', 'ScorecardController@index');
    Route::get('/scorecard/dashboard', 'ScorecardController@dashboard');
    Route::post('/scorecard/dashboard/view', 'ScorecardController@view');
    Route::get('/scorecard/dashboard/view/{id}', 'ScorecardController@viewMetric');
    Route::post('/scorecard/dashboard/addmetric', 'ScorecardController@addMetric');
    Route::post('/scorecard/dashboard/editmetric', 'ScorecardController@editMetric');
    Route::get('/scorecard/dashboard/deletemetric/{id}', 'ScorecardController@deleteMetric');
    Route::get('/scorecard/admin/view', 'ScorecardController@viewcustom');
    Route::get('/scorecard/admin/view/{id}', 'ScorecardController@viewcustomdetail');
    Route::post('/scorecard/admin/approval', 'ScorecardController@approval');
    Route::post('/scorecard/kpi', 'ScorecardController@enter_values');
    Route::get('/scorecard/kpi/chart/{id}', 'ScorecardController@chart');
    Route::get('/scorecard/kpi/chart/json/{id}', 'ScorecardController@chart_json');
    Route::get('/scorecard/report/report1', 'ScorecardController@report1');
    Route::get('/scorecard/report/report2', 'ScorecardController@report2');
    Route::get('/scorecard/report/report3/{id}', 'ScorecardController@report3');
    Route::get('/scorecard/dashboard/deletevalue/{id}', 'ScorecardController@deleteValue');
    Route::get('/scorecard/dashboard/deletemonthvalue/{id}', 'ScorecardController@deleteMonthvalue');
    Route::get('/scorecard/dashboard/manager/{id}', 'ScorecardController@manager');
    Route::get('/scorecard/dashboard/managerclear/{id}', 'ScorecardController@manager_clear');
    Route::post('/scorecard/dashboard/editmetricvalue', 'ScorecardController@editmetricvalue');
    Route::get('/scorecard/report/report4/{id}', 'ScorecardController@report4');
    Route::get('/scorecard/report/report5/{id}', 'ScorecardController@report5');
    Route::get('/scorecard/report', 'ScorecardController@reportmenu');
    Route::get('/scorecard/report/report6', 'ScorecardController@report6');
    Route::get('/scorecard/employeesearch', 'ScorecardController@employeesearch');
    Route::POST('/scorecard/dashboard/manager_admin', 'ScorecardController@manager_admin');
    Route::POST('/scorecard/dashboard/administrator', 'ScorecardController@administrator');
    Route::POST('/scorecard/dashboard/addcomment', 'ScorecardController@email_test');
    Route::get('/scorecard/dashboard/gotit/{id}', 'ScorecardController@got_it');

    //Report Routes
    Route::get('/reports/tardy', 'ReportsController@tardyReport');
    Route::POST('/reports/tardy', 'ReportsController@tardyReport');



    //Position Control Routes
    Route::get('/pc', 'PCController@index');
    Route::post('/pc/addposition', 'pc_positionsController@create_position');
    Route::post('/pc/addcostcenter', 'PC_Cost_CentersController@add_cost_center');
    Route::get('/pc/assign/{id}', 'PCController@assign');
    Route::get('/pc/{id}', 'PCController@view');
    Route::post('/pc/addshift', 'PC_ShiftsController@add_shift');
    Route::post('/pc/editshift', 'PC_ShiftsController@edit_shift');
    Route::post('/pc/assignshift', 'PC_ShiftsController@assign_shift');
    Route::post('/pc/assign/emp', 'PCController@find_employee');
    Route::post('/pc/assign/emp/assign', 'PCController@assign_employee');
    Route::get('/pc/pm/{id}', 'PCController@position_manager');
    Route::post('/pc/pm/update', 'PCController@update_position');
    Route::get('/pc/shift/remove/{id}', 'PC_ShiftsController@remove_from_shift');
    Route::get('/pc/shift/delete/{id}', 'PC_ShiftsController@delete_shift');
    Route::get('/pc/remove/{id}', 'PCController@unassign_employee');
    Route::post('/pc/editcostcenter', 'PC_Cost_CentersController@edit_cost_center');
    Route::get('/pc/pm/delete/{id}', 'PCController@delete_position'); // Delete position

    //Capital Request
    Route::get('/cr', 'CapitalRequestController@index');
    Route::get('/cr/detail/{id}', 'CapitalRequestController@detail');
    Route::post('/cr/detail/add', 'CapitalRequestController@add_item');
    Route::get('/cr/detail/item/{id}', 'CapitalRequestController@item_detail');
    Route::post('/cr/detail/edit', 'CapitalRequestController@edit_item');
    Route::post('/cr/detail/edit_settings', 'CapitalRequestController@edit_settings');
    Route::post('/cr/detail/file_upload', 'CapitalRequestController@file_upload');
    Route::get('/cr/employeesearch', 'CapitalRequestController@employeesearch');
    Route::post('/cr/detail/edit_sbar', 'CapitalRequestController@edit_sbar');
    Route::get('/cr/detail/item/destroy/{item}', 'CapitalRequestController@destroy');


    //ED TRend
    Route::get('/ed', 'ED_Trend_Controller@index');

    //Staffing Tool
    Route::get('/staffing_tool', 'StaffingToolController@index');
    Route::POST('/staffing_tool/results', 'StaffingToolController@results');
    Route::POST('/staffing_tool/addnote', 'StaffingToolController@add_note');
    Route::get('/staffing_tool/history', 'StaffingToolController@history');


    //SSIS History
    Route::get('/ssis', 'SSISController@index');

    //Upload File to Storage
    Route::get('/upload', 'UploadController@index');
    Route::post('/upload/newfile', 'UploadController@upload');

    //Project Management
    Route::get('/project', 'ProjectController@index');
    Route::post('/project/new', 'ProjectController@newProject');
    Route::get('/project/{id}', 'ProjectController@viewProject');
    Route::post('/project/task', 'ProjectTaskController@newTask');
    Route::get('/project/share/{id}', 'ProjectController@shareProject');


    //Json Dumps
    Route::get('/json/searchSystemlistContacts','SystemListController@searchSystemlistContacts');
    Route::get('/history/feed','HistoryController@historyFeed');
    Route::get('/json/currentemployees','ReportsController@currentEmployees');
    Route::get('/json/productivity/{unit}','ProductivityController@ChartsNew');
    Route::get('/json/systemaudit','SystemListController@systemSearch');
    Route::get('/json/history','HistoryController@dashboard');


//Restful Routes
    Route::get('/home', 'HomeController@index');
    Route::resource('user', 'UserController');
    Route::resource('role', 'RoleController');
    Route::resource('permission', 'PermissionController');
    Route::resource('right', 'RightController');
    Route::resource('employees', 'EmployeeController');
    Route::resource('terminations', 'TerminationController');
    Route::resource('productivity', 'ProductivityController');
    Route::resource('payroll', 'PayrollController');
    Route::resource('clockedin', 'ClockedInController');
    Route::resource('systemlist', 'SystemListController');
    Route::resource('hr', 'HrController');
    Route::resource('reports', 'ReportsController');
    Route::resource('chaplain', 'ChaplainController');
    Route::resource('rounding', 'RoundingController');
    Route::resource('extramile', 'ExtraMileController');
    Route::resource('lem', 'LemController');
    Route::resource('transport', 'TransportController');
    Route::resource('quality', 'QualityController');
    Route::resource('eoc', 'EOCController');
    Route::resource('roster', 'RosterController');
    Route::resource('mgma', 'mgmaController');


//Meal Charge Exclude
    Route::get('/meal_charge', 'MealChargeController@index');
    Route::get('/meal_charge/employeesearch', 'MealChargeController@employeesearch');
    Route::POST('/meal_charge/add', 'MealChargeController@AddEmployee');




});

//TESTING Script
//Event::listen('Illuminate\Database\Events\QueryExecuted', function ($query) {
//    var_dump($query->sql);
//    var_dump($query->bindings);
//    var_dump($query->time);
//});