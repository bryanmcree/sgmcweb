<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Permission;
use App\Http\Requests;
use Webpatser\Uuid\Uuid;
use DB;


class PermissionController extends Controller
{
    public function index()
    {
        //$users = User::paginate(10);
        $permissions = Permission::orderBy('name', 'DESC')->get();
        return View('permission.index', compact('permissions'));
    }

    public function create()
    {
        return view('permission.create');
    }

    public function store(Requests\PermissionRequest $request)
    {
        //dd($request);
        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();
        Permission::create($input);
        return redirect('/permission')->with('msg', 'Permission has been added.');
    }

    public function edit($id)
    {
        $permissions = Permission::find($id);
        return View('permission.edit', compact('permissions'));
    }

    public function update(Requests\PermissionRequest $request, $id)
    {
        $permissions = Permission::find($id);
        $permissions->update($request->all());
        return redirect('/permission')->with('msg', 'Permission has been updated successfully');
    }

    public function delete($id)
    {
        $permissions = Permission::find( $id );
        $permissions->delete();
        return redirect('/permission')->with('msg', 'Permission has been deleted.');
    }
}
