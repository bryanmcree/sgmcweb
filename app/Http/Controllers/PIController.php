<?php

namespace App\Http\Controllers;

use App\PIDashboardCensus;
use App\PIDashboardClockedIn;
use Illuminate\Http\Request;

use App\Http\Requests;

class PIController extends Controller
{
    public function index(){


        $clockedin_time_id = PIDashboardClockedIn::select('time_id')
            //->take(1)
            ->orderby('time_id', 'asc')
            ->first();

        $clockedin = PIDashboardClockedIn::selectRaw('cost_center as cost_center, count(employee_number) as total')
            ->where('time_id','=',$clockedin_time_id->time_id)
            ->groupby('cost_center')
            //->take(5)
            ->get();

        $census = PIDashboardCensus::where('time_id','=',$clockedin_time_id->time_id)
            ->orderby('cost_center')
            ->get();


        //dd($clockedin_time_id);

        return view('pi.index', compact('clockedin','census'));

    }
}
