<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Carbon;
use App\CostCenters;
use App\Event;
use App\Roster;
use App\EventSchedule;
use App\EventScheduleReserve;
use App\EventScheduleTime;
use App\History;
use App\Http\Requests;
use Webpatser\Uuid\Uuid;


class EventController extends Controller
{
    public function index()
    {
        $upEvents = Event::where('active', '=', '1')
            ->where('start_date', '>=', Carbon::now()->subDays(1))
            ->orderby('start_date')
            ->get();
        
        $myEvents = Event::where('created_by', '=', Auth::user()->employee_number)
            ->orderby('start_date', 'desc')
            ->get()
            ->take(5);

        return view('events.index', compact('upEvents', 'myEvents'));
    }

    public function detail(Event $event)
    {
        $schedule = EventSchedule::where('event_id', '=', $event->id)
            ->first();

        $scheduleData = '';
        $reserved = '';
        $reserved_names = '';

        if(!empty($schedule))
        {
            $scheduleData = EventScheduleTime::where('schedule_id', '=', $schedule->id)
                // ->orderby('reserved_by')
                ->get();

            $reserved = EventScheduleReserve::where('schedule_id', '=', $schedule->id)
                ->get();

            $reserved_names = EventScheduleReserve::select('reserved_by')
                ->where('schedule_id', '=', $schedule->id)
                ->where('reserved_by', '<>', 'NULL')
                ->orderby('reserved_by')
                ->distinct()
                ->get();
        }
        
        $flag = 0;

        $costcenters = CostCenters::wherenotnull('id')
            ->where('active_status', '=', 1)
            ->orderby('cost_center', 'asc')
            ->get();

        $rosters = Roster::where('created_by','=', \Auth::user()->id)
            ->orderby('created_at','desc')
            ->get();

        return view('events.detail', compact('event', 'schedule', 'scheduleData', 'costcenters', 'reserved', 'reserved_names', 'flag', 'rosters'));
    }

    public function store(Request $request)
    {
        $request->offsetSet('id', Uuid::generate(4));

        Event::create($request->all());

        alert()->success('Event Created', 'Success');
        return redirect()->back();
    }

    public function update(Request $request)
    {
        $event = Event::find($request->id);

        $event->update($request->all());

        alert()->success('Event Updated', 'Success');
        return redirect()->back();
    }

    public function destroy(Event $event)
    {
        $event->delete();
    }

    public function active(Request $request, Event $event)
    {
        if($request->active == 0)
        {
            $event->update([
                'active' => 1
            ]);

            alert()->success('Event Activated', 'Success!');
        }
        elseif($request->active == 1)
        {
            $event->update([
                'active' => 0
            ]);

            alert()->success('Event Deactivated', 'Success!');
        }

        return redirect()->back();
    }

    public function roster(Request $request, Event $event)
    {
        $event->update([
            'roster_id' => $request->roster
        ]);

        alert()->success('Roster Linked', 'Success!');
        return redirect()->back();
    }

}
