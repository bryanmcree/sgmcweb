<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\RoleUser;
use App\User;
use Webpatser\Uuid\Uuid;
use App\Http\Requests;
use Image;
use DB;
use App\State;
use App\Role;
use App\Http\Controllers\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use App\History;
use Maatwebsite\Excel\Facades\Excel;


class UserController extends Controller
{
    public function index(Role $assignedroles)
    {
        //$assignedRoles = $roles->users()->get();
        $roles = Role::orderBy('name')->get();
        //$users = User::paginate(10);
        $users = User::orderBy('last_login','DESC')->get();
        return View('user.index', compact('roles','assignedroles','users'));
    }

    public function show($id)
    {
        $users = User::find($id);
        return view('user.show', compact('users'));
    }

    public function create()
    {
        $states = State::orderBy('name')->pluck('name', 'id');
        $states[''] = 'Select a State';
        return view('user.create', compact('states'));
    }

    public function store(Requests\UserRequest $request)
    {

        //$request->offsetSet('id', Uuid::generate(4));
        //var_dump($request['id']);
        //dd($request);
        $input = $request->all();
        User::create($input);

        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'User Created';
        $history->userid = \Auth::user()->username;
        $history->search_string = $input;
        $history->user_ip = \Request::ip();
        $history->save();


        return redirect('/user')->with('msg', 'User has been added.');
    }

    public function edit($id)
    {
        $states = State::orderBy('name')->pluck('name', 'id');
        $states[''] = 'Select a State';
        $users = User::find($id);
        return View('user.edit', compact('users', 'states'));
    }

    public function update(Requests\UserRequest $request, $id)
    {
        $users = User::find($id);
        $users->update($request->all());
        return redirect('/user')->with('msg', 'User has been updated successfully');
    }

    public function security($id)
    {



        if(Input::has('role_id')) {
            $userName = User::where('id', $id)->first();
            $roleUser = RoleUser::where('user_id', $id)->first();
            $roles = Role::orderBy('name')->pluck('name', 'id');
            $roles[''] = 'Select a Role';
            $input = Input::all();
            RoleUser::create($input);
            return redirect('/security/'.$id)->with('msg', 'Role has been assigned to user.');
        }Else{
            $userName = User::where('id', $id)->first();
            $roleUser = RoleUser::where('user_id', $id)->first();
            $roles = Role::orderBy('name')->pluck('name', 'id');
            $roles[''] = 'Select a Role';
            return View('user.security', compact('id','roles', 'userName', 'roleUser'));
        }


    }

    public function delete($id)
    {
        $users = User::find( $id );
        $users->delete();

        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'User Deleted';
        $history->userid = \Auth::user()->username;
        $history->search_string = $id;
        $history->user_ip = \Request::ip();
        $history->save();

        return redirect('/user')->with('msg', 'User has been deleted.');
    }


    public function storeNew(Requests\UserRequest $request)
    {
        //dd('you got this far!');
        $request->offsetSet('id', Uuid::generate(4));
        //var_dump($request['id']);
        //dd($request);
        $input = $request->all();
        User::create($input);
        alert()->success('Your SGMC user has been added.  Please remember to only use Google Chrome!  You can now login.', 'Welcome!')->persistent("Got it?");
        return redirect('/login');
    }

    public function newRoles(Requests\SystemListRequest $request)
    {
        // remove the token
        $roles = $request->except('_token','user_id');
       // $instance_id = Uuid::generate(4);
        //dd($request->user_id);
        foreach ($roles as $key => $value) {
            $newRoles = new RoleUser();
            if (! is_array( $value )) {
                $newValue = $value['answer'];
            } else {
                $newValue = json_encode($value['answer']);
            }

            $newRoles->role_id = str_replace('"','',$newValue);
            $newRoles->user_id = $request->user_id;
            $newRoles->save();
            //dd($newRoles);
            $newRoles1[] = $newRoles;
        };
        alert()->success('Roles Updated!', 'COMPLETE!');
        return redirect('/search');
    }

    public function removeRoles(Requests\SystemListRequest $request)
    {
        $user = User::find($request->user_id);
        $newUser = $user->id;
        $roles = $request->role_id;
        //dd($newUser, $roles);

        foreach($roles as $role)
        {
            $delete = RoleUser::where('user_id', '=', $newUser)
            ->where('role_id', '=', $role)
            ->delete();            
        }

        alert('Roles Updated', 'COMPLETE!');
        return redirect('/search');

    }

    public function updateManager($name)
    {
        //dd(Input::get('returnTo'));
        $users = User::find(\Auth::user()->id);
        //$users->manageremail = Input::get('mail');
        $users->manager_emp_number = $name;
        $users->save();

        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'Updated Supervisor';
        $history->userid = \Auth::user()->username;
        $history->search_string = $name;
        $history->user_ip = \Request::ip();
        $history->save();
        alert()->success('Account Updated!', 'Thank You!');

        if(Input::get('returnTo') == 1){
            $returnTo = '/reports/request';
        }Else{
            $returnTo = '/home';
        }

        return redirect($returnTo);

    }

    public function updateLD()
    {
        //dd(Input::get('ldac'));
        $ldac = User::find( Input::get('id') );

        $ldac->update(Input::only(['ldac']));
        //dd($servers);
        alert()->success('LDAC has been Updated!', 'Success!');

        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'LDAC Updated on User';
        $history->userid = \Auth::user()->username;
        $history->search_string = Input::get('ldac');
        $history->user_ip = \Request::ip();
        $history->save();

        return redirect('/user/');
    }

    public function showPicture($id)
    {
        $picture = User::findOrFail($id);
        dd($picture);
        $pic = Image::make($picture->photo);

        $response = Response::make($pic->encode('jpeg'));

        //setting content-type
        $response->header('Content-Type', 'image/jpeg');

        return $response;
    }

    public function userDetail($id){
        $user = User::findOrFail($id);
        $assignedRoles = RoleUser::select('role_id')->where('user_id','=',$user->id)->get();
        //dd($assignedRoles);

        $roles = Role::whereNotIn('id',$assignedRoles)
        ->orderBy('name')->get();
        //dd($roles, $assignedRoles);

        $assignedNew = Role::whereIn('id', $assignedRoles)
        ->orderBy('name')->get();
        //dd($assignedNew);

        return  View('employees.modal.detail', compact('user','roles', 'assignedNew'));
    }

    public function updateDetail(Request $request, $id){
        //dd(Input::get('ldac'));
        $user = User::find($id);

        $user->update($request->only(['username', 'access','emp_status', 'title', 'employee_number', 'job_code', 'unit_code', 'address', 'city', 'state', 'zip']));

        //dd($servers);
        alert()->success('User has been Updated!', 'Success!');

        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'Updated on User';
        $history->userid = \Auth::user()->username;
        $history->search_string = 'N/A';
        $history->user_ip = \Request::ip();
        $history->save();

        return redirect('/search');
    }

    public static function printList()
    {
        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'Employee Print List';
        $history->userid = \Auth::user()->username;
        $history->search_string = 'None';
        $history->user_ip = \Request::ip();
        $history->save();

        $tests = User::select('employee_number','last_name', 'first_name','title','unit_code', 'unit_code_description','mail','location')
            ->where('emp_status', '<>', 'Terminated')
            ->wherenotnull('employee_number')
            ->get()
            ->toArray();
        Excel::create('employees '.date('m-d-Y_hia'), function($excel) use($tests) {
            $excel->sheet('New sheet', function($sheet) use($tests) {
                $sheet->fromArray($tests);
            })->download('xlsx');
        });
    }

    public function find_manager(){

        return  View('updatemanager', compact('user','roles'));

    }


    public function manager()
    {


        $manager = User::where('name','=', Input::get('director' ))->first();
        //dd($manager);
        $users = User::find(\Auth::user()->id);
        //dd($users);
        //dd($manager->employee_number);
        $users->manager_emp_number = $manager->employee_number;
        $users->save();

        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'Updated Supervisor';
        $history->userid = \Auth::user()->username;
        $history->search_string = 'N/A';
        $history->user_ip = \Request::ip();
        $history->save();

        //dd(Input::get('redirect' ));
        alert()->success('Sending you where you want to go.', 'Much better! Thank You!');


        IF(Input::get('redirect')=='ReportRequest'){
            return redirect('/reports/request/');
        }else{
            return back();
        }



    }
}
