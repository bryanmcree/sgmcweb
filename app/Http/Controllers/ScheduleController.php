<?php

namespace App\Http\Controllers;

use App\Schedule;
use Illuminate\Http\Request;

use App\Http\Requests;

class ScheduleController extends Controller
{
    public function index(){

        //Select all the meeting dates
        $ava_dates = Schedule::wherenotnull('id')
            ->orderby('meeting_date')
            ->orderby('start_time')
            ->get();
        //dd($ava_dates);

        return view('schedule.index', compact('ava_dates','total_patients','total_part_one','total_part_two'));
    }
}
