<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;

use App\Http\Requests;
use App\SecurityBadges;
use App\SecurityCitation;
use App\SecurityActivityList;
use App\SecurityLocationList;
use App\SecurityDispatch;
use App\SecurityRounds;
use App\SecurityViolation;
use App\SecurityIncident;
use App\SecurityIncidentList;
use App\User;
use App\History;
use Auth;
use Carbon;
use DB;

class SGMCSecurityCitationController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $offenders = SecurityCitation::wherenotnull('id')
            ->orderBy('date', 'desc')
            ->get();
        
        return view('sgmcsecurity.citation', compact('user', 'offenders'));
    }

    public function store(Request $request)
    {
        $request->offsetSet('id', Uuid::generate(4));

        $citation = $request->except('violation');
        $violation = $request->violation;

        // dd($citation, $violation);

        SecurityCitation::create($citation);

        foreach($violation as $vio)
        {
            SecurityViolation::create([
                'id' => Uuid::generate(4),
                'citation_id' => $request->id,
                'violation' => $vio,
            ]);
        }

        alert()->success('Citation Recorded!', 'Success!');
        return redirect()->back();
    }

    public function edit($id)
    {
        $citation = SecurityCitation::find($id);

        $violations = $citation->violations()->get();

        return view('sgmcsecurity.edit.citation', compact('citation', 'violations'));
    }

    public function update(Request $request, $id)
    {
        $citation = SecurityCitation::find($id);
        $violations = SecurityViolation::where('citation_id', '=', $id)->get();

        foreach($violations as $v)
        {
            $v->delete();
        }

        foreach($request->violation as $vio)
        {
            SecurityViolation::create([
                'id' => Uuid::generate(4),
                'citation_id' => $request->id,
                'violation' => $vio,
            ]);
        }

        $citation->update($request->except('violation'));

        alert()->success('Citation Updated!', 'Success!');
        return redirect('/security');
    }

    public function destroy($id)
    {
        $citation = SecurityCitation::find($id);

        $citation->delete();
    }
}
