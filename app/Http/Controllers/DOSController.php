<?php

namespace App\Http\Controllers;

use App\DOSTrend;
use App\History;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;

class DOSController extends Controller
{
    public function index(){

        $chart_lables = DOSTrend::select('rpt_date')
            ->where('rpt_date', '<=', Carbon::today())
            ->distinct()
            ->take(30)
            ->orderby('rpt_date')
            ->get();

        //Acute ALOS

        $ber = DOSTrend::select('value')
            ->where('category', '=', 'ACUTE_ALOS')
            ->where('dept_loc','BER Parent Location')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();

        $lan = DOSTrend::select('value')
            ->where('category', '=', 'ACUTE_ALOS')
            ->where('dept_loc','LAN Parent Location')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();

        $sga = DOSTrend::select('value')
            ->where('category', '=', 'ACUTE_ALOS')
            ->where('dept_loc','SGA Parent Location')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();


        //ALOS
        $ALOS_ber = DOSTrend::select('value')
            ->where('category', '=', 'ALOS')
            ->where('dept_loc','BER Parent Location')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();

        $ALOS_lan = DOSTrend::select('value')
            ->where('category', '=', 'ALOS')
            ->where('dept_loc','LAN Parent Location')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();

        $ALOS_sga = DOSTrend::select('value')
            ->where('category', '=', 'ALOS')
            ->where('dept_loc','SGA Parent Location')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();

        $ALOS_vil = DOSTrend::select('value')
            ->where('category', '=', 'ALOS')
            ->where('dept_loc','VIL Parent Location')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();


        //Acute Discharges
        $AcuteDis_sga = DOSTrend::select('value')
            ->where('category', '=', 'ACUTE_DISCHARGES')
            ->where('dept_loc','SGA Revenue Location')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();

        $AcuteDis_ber = DOSTrend::select('value')
            ->where('category', '=', 'ACUTE_DISCHARGES')
            ->where('dept_loc','BER Revenue Location')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();

        //Adult ped discharges
        $adultDis_sga = DOSTrend::select('value')
            ->where('category', '=', 'ADLT/PED Discharges')
            ->where('dept_loc','SGA Revenue Location')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();

        $adultDis_ber = DOSTrend::select('value')
            ->where('category', '=', 'ADLT/PED Discharges')
            ->where('dept_loc','BER Revenue Location')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();

        $adultDis_vil = DOSTrend::select('value')
            ->where('category', '=', 'ADLT/PED Discharges')
            ->where('dept_loc','VIL Revenue Location')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();

        $adultDis_lan = DOSTrend::select('value')
            ->where('category', '=', 'ADLT/PED Discharges')
            ->where('dept_loc','LAN Revenue Location')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();


        //ADULT/PED PATIENT_DAYS
        $adult_days_sga = DOSTrend::select('value')
            ->where('category', '=', 'ADULT/PED PATIENT_DAYS')
            ->where('dept_loc','SGA Parent Location')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();

        $adult_days_ber = DOSTrend::select('value')
            ->where('category', '=', 'ADULT/PED PATIENT_DAYS')
            ->where('dept_loc','BER Parent Location')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();

        $adult_days_vil = DOSTrend::select('value')
            ->where('category', '=', 'ADULT/PED PATIENT_DAYS')
            ->where('dept_loc','VIL Parent Location')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();

        $adult_days_lan = DOSTrend::select('value')
            ->where('category', '=', 'ADULT/PED PATIENT_DAYS')
            ->where('dept_loc','LAN Parent Location')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();

        //ED_ADMITS_TO_BED
        $ed_bed_sga = DOSTrend::select('value')
            ->where('category', '=', 'ED_ADMITS_TO_BED')
            ->where('dept_loc','SGA Revenue Location')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();

        $ed_bed_ber = DOSTrend::select('value')
            ->where('category', '=', 'ED_ADMITS_TO_BED')
            ->where('dept_loc','BER Revenue Location')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();

        $ed_bed_vil = DOSTrend::select('value')
            ->where('category', '=', 'ED_ADMITS_TO_BED')
            ->where('dept_loc','VIL Revenue Location')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();

        $ed_bed_lan = DOSTrend::select('value')
            ->where('category', '=', 'ED_ADMITS_TO_BED')
            ->where('dept_loc','LAN Revenue Location')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();


        //Patient REV
        $patient_rev_sga = DOSTrend::select('value')
            ->where('category', '=', 'PATIENT_REV')
            ->where('dept_loc','SGA Parent Location')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();

        $patient_rev_ber = DOSTrend::select('value')
            ->where('category', '=', 'PATIENT_REV')
            ->where('dept_loc','BER Parent Location')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();

        $patient_rev_vil = DOSTrend::select('value')
            ->where('category', '=', 'PATIENT_REV')
            ->where('dept_loc','VIL Parent Location')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();

        $patient_rev_lan = DOSTrend::select('value')
            ->where('category', '=', 'PATIENT_REV')
            ->where('dept_loc','LAN Parent Location')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();

        //PB Revenue
        $pb_rev_ber = DOSTrend::select('value')
            ->where('category', '=', 'PB_REVENUE')
            ->where('dept_loc','BER Revenue Location')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();

        $pb_rev_lucas = DOSTrend::select('value')
            ->where('category', '=', 'PB_REVENUE')
            ->where('dept_loc','LAN Dr Lucas')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();

        $pb_rev_lan = DOSTrend::select('value')
            ->where('category', '=', 'PB_REVENUE')
            ->where('dept_loc','LAN Revenue Location')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();

        $pb_rev_asc = DOSTrend::select('value')
            ->where('category', '=', 'PB_REVENUE')
            ->where('dept_loc','SGA ASC')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();

        $pb_rev_ashley = DOSTrend::select('value')
            ->where('category', '=', 'PB_REVENUE')
            ->where('dept_loc','SGA Ashley Street OPC')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();

        $pb_rev_cancer = DOSTrend::select('value')
            ->where('category', '=', 'PB_REVENUE')
            ->where('dept_loc','SGA CANCER CENTER')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();

        $pb_rev_cardio = DOSTrend::select('value')
            ->where('category', '=', 'PB_REVENUE')
            ->where('dept_loc','SGA CARDIOLOGY CLINIC')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();

        $pb_rev_cvi = DOSTrend::select('value')
            ->where('category', '=', 'PB_REVENUE')
            ->where('dept_loc','SGA CVI')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();

        $pb_rev_diabetes = DOSTrend::select('value')
            ->where('category', '=', 'PB_REVENUE')
            ->where('dept_loc','SGA Diabetes Mgmt')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();

        $pb_rev_family = DOSTrend::select('value')
            ->where('category', '=', 'PB_REVENUE')
            ->where('dept_loc','SGA FAMILY PRACTICE')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();

        $pb_rev_surgery = DOSTrend::select('value')
            ->where('category', '=', 'PB_REVENUE')
            ->where('dept_loc','SGA General Surgery')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();

        $pb_rev_south = DOSTrend::select('value')
            ->where('category', '=', 'PB_REVENUE')
            ->where('dept_loc','SGA Healthcare South')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();

        $pb_rev_img = DOSTrend::select('value')
            ->where('category', '=', 'PB_REVENUE')
            ->where('dept_loc','SGA Imaging Center')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();

        $pb_rev_sga = DOSTrend::select('value')
            ->where('category', '=', 'PB_REVENUE')
            ->where('dept_loc','SGA Revenue Location')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();

        $pb_rev_snh1 = DOSTrend::select('value')
            ->where('category', '=', 'PB_REVENUE')
            ->where('dept_loc','SNH Parent Location')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();

        $pb_rev_snh2 = DOSTrend::select('value')
            ->where('category', '=', 'PB_REVENUE')
            ->where('dept_loc','SNH Revenue Location')
            ->where('rpt_date', '<=', Carbon::today())
            ->take(30)
            ->orderby('rpt_date')
            ->get();


        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'Viewed DOS';
        $history->userid = \Auth::user()->username;
        $history->search_string = 'N/A';
        $history->user_ip = \Request::ip();
        $history->save();

        //dd($AcuteDis_sga);
        return View('dos.index', compact('chart_lables',
            'ber','lan','sga',
            'ALOS_ber','ALOS_lan','ALOS_sga','ALOS_vil',
            'AcuteDis_ber','AcuteDis_sga',
            'adultDis_ber','adultDis_lan','adultDis_sga','adultDis_vil',
            'adult_days_ber','adult_days_lan','adult_days_sga','adult_days_vil',
            'ed_bed_ber','ed_bed_lan','ed_bed_sga','ed_bed_vil',
            'patient_rev_ber', 'patient_rev_lan','patient_rev_sga','patient_rev_vil',
            'pb_rev_asc','pb_rev_ashley','pb_rev_ber','pb_rev_cancer','pb_rev_cardio','pb_rev_cvi','pb_rev_diabetes','pb_rev_family','pb_rev_img','pb_rev_lan','pb_rev_lucas','pb_rev_sga','pb_rev_snh1','pb_rev_snh2','pb_rev_south','pb_rev_surgery'));
    }

}
