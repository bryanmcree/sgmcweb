<?php

namespace App\Http\Controllers;

use App\Quality;
use App\QualityImage;
use Illuminate\Http\Request;

use App\Http\Requests;
use Webpatser\Uuid\Uuid;
use Image;
use DB;
use App\State;
use App\Role;
use App\Http\Controllers\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;

class QualityController extends Controller
{
    public function index(){
        return view('imaging.index');

        $monthlyMRI = Quality::select('measure','cost_center',DB::raw('COUNT(cost_center) as total'))
            ->where('measure','=','MRI')
            ->with('perMonth')
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->groupby('measure','cost_center')
            ->get();
        //dd($monthlyMRI);

        $monthlyCT = Quality::select('measure','cost_center',DB::raw('COUNT(cost_center) as total'))
            ->where('measure','=','CT')
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->groupby('measure','cost_center')
            ->get();

        $monthlyIR = Quality::select('measure','cost_center',DB::raw('COUNT(cost_center) as total'))
            ->where('measure','=','IR')
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->groupby('measure','cost_center')
            ->get();

        $monthlyNuc = Quality::select('measure','cost_center',DB::raw('COUNT(cost_center) as total'))
            ->where('measure','=','Nuc Med')
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->groupby('measure','cost_center')
            ->get();

        $monthlyXray = Quality::select('measure','cost_center',DB::raw('COUNT(cost_center) as total'))
            ->where('measure','=','X-Ray')
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->groupby('measure','cost_center')
            ->get();

        $monthlyUltra = Quality::select('measure','cost_center',DB::raw('COUNT(cost_center) as total'))
            ->where('measure','=','Ultrasound')
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->groupby('measure','cost_center')
            ->get();

        return view('quality.imaging.index', compact('monthlyMRI','monthlyCT','monthlyIR','monthlyNuc','monthlyUltra',
            'monthlyXray'));
    }
    
    public function newMeasure(Requests\SystemListRequest $request){
        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();
        Quality::create($input);

        alert()->success('Measure has been added!', 'Success!');

        return redirect('/quality/imaging');
    }

    public function report(){
        return view('quality.imaging.report');
    }

    public function notCompleted(){

        //Select Month and Year
        $start_date  = Input::get('start_date');
        $end_date  = Input::get('end_date');

        $monthly = QualityImage::with('perMonth')
            //->select('measure','quality_control_cost_centers.cost_center')
            //->distinct()
            //->select('measure','qty','quality_control_cost_centers.cost_center',DB::raw('COUNT(quality_imaging.cost_center) as total'))
           // ->LeftJoin('quality_imaging','quality_imaging.cost_center','=','quality_control_cost_centers.cost_center')
            //->whereYear('created_at','=', $year)
            //->whereMonth('created_at','=', $month)
           // ->whereBetween('perMonth.created_at', array($start_date, $end_date))
            //->groupby('measure','quality_control_cost_centers.cost_center')
           // ->orderby('measure')

            //->distinct()
            ->get();
        //dd($monthly);



        //dd($monthly);

        $report = Quality::select('measure','cost_center',DB::raw('COUNT(cost_center) as total'))
            ->whereBetween('created_at', array($start_date, $end_date))
            ->groupby('measure','cost_center')
            ->orderby('measure')
            ->get();

        $MRI = Quality::distinct()
            ->select('two',DB::raw('MAX(created_at) as created_at'),DB::raw('COUNT(two) as totals'))
            //->whereYear('created_at','=', $year)
            //->whereMonth('created_at','=', $month)
            ->where('measure','MRI')
            ->groupby('two')
            ->get();
            //->select('one')
            //select(DB::raw('DISTINCT(two)'),DB::raw('COUNT(two) as totals'))
           // ->whereYear('created_at','=', $year)
           // ->whereMonth('created_at','=', $month)
          //  ->where('measure','MRI')
           // ->groupby('two')
          //  ->get();
           // ->where('measure','MRI')
          //  ->count(['one'])
            //->count(DB::raw('DISTINCT one'))
            //->count('one')
          //  ->groupby('one')
            //->count('one');
          //  ->get();

        //dd($MRI);
        return view('quality.imaging.view', compact('report','MRI','monthly'));
    }
}
