<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use Maatwebsite\Excel\Facades\Excel;

use App\Http\Requests;
use App\SecurityBadges;
use App\SecurityCitation;
use App\SecurityActivityList;
use App\SecurityLocationList;
use App\SecurityDispatch;
use App\SecurityRounds;
use App\SecurityViolation;
use App\SecurityIncident;
use App\SecurityIncidentList;
use App\SecurityTrespass;
use App\SecurityOrientation;
use App\User;
use App\History;
use Auth;
use Carbon;
use DB;

class SGMCSecurityController extends Controller
{

    public function index()
    {
        $history = new History();
        $history->id = \Uuid::generate(4);
        $history->action = 'Viewed Security Dashboard';
        $history->userid = \Auth::user()->username;
        $history->user_ip = \Request::ip();
        $history->search_string = 'None';
        $history->save();

        $badges = SecurityBadges::where('date', '=', Carbon::yesterday())
            ->first();

        $citations = SecurityCitation::where('date', '>', Carbon::now()->subDays(30))
            ->get();

        $dispatches = SecurityDispatch::where('date', '>', Carbon::now()->subDays(7))
            ->get();

        $rounds = SecurityRounds::where('date_time', '>', Carbon::now()->subDays(5))
            ->get();

        $incidents = SecurityIncident::where('datetime', '>=', Carbon::now()->subDays(30))->get();

        $locations = SecurityLocationList::wherenotnull('id')->get();

        $rounds5 = SecurityRounds::where('date_time', '<', Carbon::now()->subDays(4))
            ->where('date_time', '>', Carbon::now()->subDays(5))
            ->count();

        $rounds4 = SecurityRounds::where('date_time', '<', Carbon::now()->subDays(3))
            ->where('date_time', '>', Carbon::now()->subDays(4))
            ->count();        

        $rounds3 = SecurityRounds::where('date_time', '<', Carbon::now()->subDays(2))
            ->where('date_time', '>', Carbon::now()->subDays(3))
            ->count();

        $rounds2 = SecurityRounds::where('date_time', '<', Carbon::now()->subDays(1))
            ->where('date_time', '>', Carbon::now()->subDays(2))
            ->count();

        $rounds1 = SecurityRounds::where('date_time', '<', Carbon::now())
            ->where('date_time', '>', Carbon::now()->subDays(1))
            ->count();

        $activities = SecurityActivityList::wherenotnull('id')->orderby('activity')->get();

        $chartBadges = SecurityBadges::where('date', '<', Carbon::now())
            ->where('date', '>', Carbon::now()->subDays(5))
            ->orderBy('date', 'asc')
            ->get();

        $chartCitations6 = SecurityCitation::where('date', '<', Carbon::now()->subWeeks(5))
            ->where('date', '>', Carbon::now()->subWeeks(6))
            ->count();

        $chartCitations5 = SecurityCitation::where('date', '<', Carbon::now()->subWeeks(4))
            ->where('date', '>', Carbon::now()->subWeeks(5))
            ->count();

        $chartCitations4 = SecurityCitation::where('date', '<', Carbon::now()->subWeeks(3))
            ->where('date', '>', Carbon::now()->subWeeks(4))
            ->count();

        $chartCitations3 = SecurityCitation::where('date', '<', Carbon::now()->subWeeks(2))
            ->where('date', '>', Carbon::now()->subWeeks(3))
            ->count();

        $chartCitations2 = SecurityCitation::where('date', '<', Carbon::now()->subWeeks(1))
            ->where('date', '>', Carbon::now()->subWeeks(2))
            ->count();

        $chartCitations1 = SecurityCitation::where('date', '<', Carbon::now())
            ->where('date', '>', Carbon::now()->subWeeks(1))
            ->count();

        //

        $chartDispatches6 = SecurityDispatch::where('date', '<', Carbon::now()->subWeeks(5))
            ->where('date', '>', Carbon::now()->subWeeks(6))
            ->count();

        $chartDispatches5 = SecurityDispatch::where('date', '<', Carbon::now()->subWeeks(4))
            ->where('date', '>', Carbon::now()->subWeeks(5))
            ->count();

        $chartDispatches4 = SecurityDispatch::where('date', '<', Carbon::now()->subWeeks(3))
            ->where('date', '>', Carbon::now()->subWeeks(4))
            ->count();

        $chartDispatches3 = SecurityDispatch::where('date', '<', Carbon::now()->subWeeks(2))
            ->where('date', '>', Carbon::now()->subWeeks(3))
            ->count();

        $chartDispatches2 = SecurityDispatch::where('date', '<', Carbon::now()->subWeeks(1))
            ->where('date', '>', Carbon::now()->subWeeks(2))
            ->count();

        $chartDispatches1 = SecurityDispatch::where('date', '<', Carbon::now())
            ->where('date', '>', Carbon::now()->subWeeks(1))
            ->count();

        $chartIncidents6 = SecurityIncident::where('datetime', '<', Carbon::now()->subWeeks(5))
            ->where('datetime', '>', Carbon::now()->subWeeks(6))
            ->count();

        $chartIncidents5 = SecurityIncident::where('datetime', '<', Carbon::now()->subWeeks(4))
            ->where('datetime', '>', Carbon::now()->subWeeks(5))
            ->count();

        $chartIncidents4 = SecurityIncident::where('datetime', '<', Carbon::now()->subWeeks(3))
            ->where('datetime', '>', Carbon::now()->subWeeks(4))
            ->count();

        $chartIncidents3 = SecurityIncident::where('datetime', '<', Carbon::now()->subWeeks(2))
            ->where('datetime', '>', Carbon::now()->subWeeks(3))
            ->count();

        $chartIncidents2 = SecurityIncident::where('datetime', '<', Carbon::now()->subWeeks(1))
            ->where('datetime', '>', Carbon::now()->subWeeks(2))
            ->count();

        $chartIncidents1 = SecurityIncident::where('datetime', '<', Carbon::now())
            ->where('datetime', '>', Carbon::now()->subWeeks(1))
            ->count();

        return view('sgmcsecurity.index', compact('badges', 'citations', 'dispatches', 'rounds', 'incidents', 'chartBadges', 'activities', 'rounds1', 'rounds2', 'rounds3', 'rounds4', 'rounds5',
                                                  'chartCitations6', 'chartCitations5', 'chartCitations4', 'chartCitations3', 'chartCitations2', 'chartCitations1',
                                                  'chartDispatches6', 'chartDispatches5', 'chartDispatches4', 'chartDispatches3', 'chartDispatches2', 'chartDispatches1',
                                                  'chartIncidents6', 'chartIncidents5', 'chartIncidents4', 'chartIncidents3', 'chartIncidents2', 'chartIncidents1', 'locations' ));
    }


    public function DBLists()
    {
        $activities = SecurityActivityList::wherenotnull('id')->get();
        $locations = SecurityLocationList::wherenotnull('id')->get();
        $incidents = SecurityIncidentList::wherenotnull('id')->get();

        return view('sgmcsecurity.location_activity', compact('activities', 'locations', 'incidents'));
    }

    public function newIncident(Request $request)
    {
        $request->offsetSet('id', Uuid::generate(4));

        $incident = $request->all();

        SecurityIncidentList::create($incident);

        alert()->success('Incident Added!', 'Success!');
        return redirect()->back();
    }

    public function createActivity(Request $request)
    {   
        $request->offsetSet('id', Uuid::generate(4));

        $activity = $request->all();

        SecurityActivityList::create($activity);

        alert()->success('Activity Added!', 'Success!');
        return redirect()->back();
    }

    public function createLocation(Request $request)
    {   
        $request->offsetSet('id', Uuid::generate(4));

        $location = $request->all();

        SecurityLocationList::create($location);

        alert()->success('Location Added!', 'Success!');
        return redirect()->back();
    }

    public function activityDestroy(SecurityActivityList $activity)
    {
        $activity->delete();
    }

    public function locationDestroy(SecurityLocationList $location)
    {
        $location->delete();
    }

    public function incidentDestroy(SecurityIncidentList $incident)
    {
        $incident->delete();
    }
    
    
    // REPORTS 

    public function report(Request $request)
    {
        $startDate = Carbon::parse($request->startDate)->format('Y-m-d H:i:s');
        $endDate = Carbon::parse($request->endDate)->format('Y-m-d H:i:s');

        if($request->report == 'badge')
        {
            $reportData = SecurityBadges::whereBetween('date', [$startDate, $endDate])
                ->orderBy('date', 'asc')
                ->get();
                
            $total = SecurityBadges::whereBetween('date', [$startDate, $endDate])
                ->sum('total');

            return view('sgmcsecurity.reports.badgeReport', compact('reportData', 'startDate', 'endDate', 'total'));
        }
        else if($request->report == 'citation')
        {
            $reportData = SecurityCitation::whereBetween('date', [$startDate, $endDate])
                ->orderBy('date', 'desc')
                ->get();

            $sgmcData = SecurityCitation::whereBetween('date', [$startDate, $endDate])
                ->where('location', '=', 'sgmc')
                ->orderBy('date', 'desc')
                ->get();

            $vsuData = SecurityCitation::whereBetween('date', [$startDate, $endDate])
                ->where('location', '=', 'vsu')
                ->orderBy('date', 'desc')
                ->get();

            // $noDecal = SecurityViolation::whereBetween('date', [$startDate, $endDate])
            //     ->where('violation', '=', 'no parking decal')
            //     ->orderBy('date', 'desc')
            //     ->get();

            // $timeLimit = SecurityViolation::whereBetween('date', [$startDate, $endDate])
            //     ->where('violation', '=', 'exceeded authorized time limit')
            //     ->orderBy('date', 'desc')
            //     ->get();

            // $unauthorized = SecurityViolation::whereBetween('date', [$startDate, $endDate])
            //     ->where('violation', '=', 'parking in unauthorized area')
            //     ->orderBy('date', 'desc')
            //     ->get();

            // $speeding = SecurityViolation::whereBetween('date', [$startDate, $endDate])
            //     ->where('violation', '=', 'excessive speed for conditions')
            //     ->orderBy('date', 'desc')
            //     ->get();

            // $badParking = SecurityViolation::whereBetween('date', [$startDate, $endDate])
            //     ->where('violation', '=', 'incorrect parking')
            //     ->orderBy('date', 'desc')
            //     ->get();

            // $handicappedParking = SecurityViolation::whereBetween('date', [$startDate, $endDate])
            //     ->where('violation', '=', 'handicapped parking')
            //     ->orderBy('date', 'desc')
            //     ->get();

            // $tow = SecurityViolation::whereBetween('date', [$startDate, $endDate])
            //     ->where('violation', '=', 'vehicle subject to be towed')
            //     ->orderBy('date', 'desc')
            //     ->get();

            return view('sgmcsecurity.reports.citationReport', compact('reportData', 'startDate', 'endDate', 'sgmcData', 'vsuData'));
        }
        else if($request->report == 'dispatch')
        {
            if($request->location <> NULL)
            {
                $reportData = SecurityDispatch::whereBetween('date', [$startDate, $endDate])
                    ->where('activity', '<>', 'SHIFT CHANGE')
                    ->where('location', $request->location)
                    ->orderBy('date', 'desc')
                    ->get();
            }
            else
            {
                $reportData = SecurityDispatch::whereBetween('date', [$startDate, $endDate])
                    ->where('activity', '<>', 'SHIFT CHANGE')
                    ->orderBy('date', 'desc')
                    ->get();
            }
            

            return view('sgmcsecurity.reports.dispatchReport', compact('reportData', 'startDate', 'endDate'));
        }
        else if($request->report == 'rounds')
        {
            $startDate = Carbon::parse($request->startDate)->format('Y-m-d');
            $endDate = Carbon::parse($request->endDate)->addDay()->format('Y-m-d'); // add day because rounds use date_time field, this will show ALL data between date range

            if($request->location <> NULL)
            {
                $reportData = SecurityRounds::whereBetween('date_time', [$startDate, $endDate])
                    ->where('location', $request->location)
                    ->orderBy('date_time', 'desc')
                    ->get();
            }
            else
            {
                $reportData = SecurityRounds::whereBetween('date_time', [$startDate, $endDate])
                    ->orderBy('date_time', 'desc')
                    ->get();
            }
            

            return view('sgmcsecurity.reports.roundsReport', compact('reportData', 'startDate', 'endDate'));
        }
        else if($request->report == 'incident')
        {
            if($request->location <> NULL)
            {
                $reportData = SecurityIncident::whereBetween('datetime', [$startDate, Carbon::parse($endDate)->endOfDay()])
                    ->where('location', $request->location)
                    ->orderBy('datetime', 'desc')
                    ->get();
            }
            else
            {
                $reportData = SecurityIncident::whereBetween('datetime', [$startDate, Carbon::parse($endDate)->endOfDay()])
                    ->orderBy('datetime', 'desc')
                    ->get();
            }

            return view('sgmcsecurity.reports.incidentReport', compact('reportData', 'startDate', 'endDate'));
        }
        else if($request->report == 'trespass')
        {
            $reportData = SecurityTrespass::whereBetween('date_time', [$startDate, Carbon::parse($endDate)->endOfDay()])
                ->orderBy('date_time', 'desc')
                ->get();

            return view('sgmcsecurity.reports.trespassReport', compact('reportData', 'startDate', 'endDate'));
        }
        else if($request->report == 'orientation')
        {
            $reportData = SecurityOrientation::whereBetween('date', [$startDate, Carbon::parse($endDate)->endOfDay()])
                ->orderBy('date', 'desc')
                ->get();

            return view('sgmcsecurity.reports.orientationReport', compact('reportData', 'startDate', 'endDate'));
        }
        else if($request->report == 'wpv')
        {
            $dispatchData = SecurityDispatch::whereBetween('date', [$startDate, Carbon::parse($endDate)->endOfDay()])
                ->where('wpv', 'yes')
                ->orderBy('date', 'desc')
                ->get();
            
            $incidentData = SecurityIncident::whereBetween('datetime', [$startDate, Carbon::parse($endDate)->endOfDay()])
                ->where('wpv', 'yes')
                ->orderBy('datetime', 'desc')
                ->get();

            return view('sgmcsecurity.reports.WPVReport', compact('dispatchData', 'incidentData', 'startDate', 'endDate'));
        }

    }

    public function specific(Request $request)
    {
        $startDate = Carbon::parse($request->startDate)->format('Y-m-d H:i:s');
        $endDate = Carbon::parse($request->endDate)->format('Y-m-d H:i:s');
        $activity = $request->activity;

        $reportData = SecurityDispatch::whereBetween('date', [$startDate, $endDate])
            ->where('activity', '=', $activity)
            ->orderBy('date', 'desc')
            ->get();

        $totalhours = SecurityDispatch::selectRaw('SUM(DATEPART(HH,response)) as hours, SUM(DATEPART(n,response)) as minutes')
        ->whereBetween('date', [$startDate, $endDate])
        ->where('activity', '=', $activity)
        ->first();


        $hour = $totalhours->hours;
        $min = $totalhours->minutes;
        $counter = 0;

        for($c = 0; $c <= $counter; $c++)
        { 
            if($min >= 60)
            {
                $min -= 60;
                $hour += 1;
                $counter += 1;
            }
        }

        // dd($min, 'min', $hour, 'hour');

        return view('sgmcsecurity.reports.specificReport', compact('reportData', 'startDate', 'endDate', 'activity', 'hour', 'min'));
    } 

    public function specificExport($startDate, $endDate, $activity)
    {
        $data = SecurityDispatch::whereBetween('date', [$startDate, $endDate])
            ->where('activity', '=', $activity)
            ->orderBy('date', 'desc')
            ->get();

        Excel::create('SGMC_Security '.date('m-d-Y_hia'), function($excel) use($data) {
            $excel->sheet('New sheet', function($sheet) use($data) {

                $sheet->setColumnFormat(array(
                    'C' => 'h:mm:ss AM/PM',
                    'D' => 'h:mm:ss AM/PM',
                    'H' => 'h:mm:ss AM/PM',
                    'I' => 'h:mm:ss AM/PM',
                    'J' => 'h:mm:ss AM/PM',
                ));

                //Adds Zeros back in null fields
                $sheet->fromArray($data, null, 'A1', true);

            })->download('xlsx');
        });
    }

    public function matrix(Request $request)
    {
        $startDate = Carbon::parse($request->startDate)->format('Y-m-d H:i:s');
        $endDate = Carbon::parse($request->endDate)->format('Y-m-d H:i:s');

        $doors = SecurityDispatch::where('activity', '=', 'REQUEST DOOR UNLOCK')
            ->whereBetween('date', [$startDate, $endDate])
            ->get();

        $weapons = DB::table('security_dispatches')
            ->selectRaw('activity, COUNT(activity) as total')
            ->whereBetween('date', [$startDate, $endDate])
            ->where('activity', '=', 'VAL PICK UP/WEAPON')
            ->orWhereBetween('date', [$startDate, $endDate])
            ->where('activity', '=', 'PT VAL PICK UP/WEAPON')
            ->groupBy('activity')
            ->orderBy('total', 'desc')
            ->get();
    
        $weapon = 0;

        foreach($weapons as $t)
        {
            $weapon += $t->total;
        }

        $riskPT = SecurityDispatch::where('activity', '=', 'AT RISK PATIENT')
            ->whereBetween('date', [$startDate, $endDate])
            ->get();

        $riskHours = DB::table('security_dispatches')
            ->selectRaw('SUM(DATEPART(HH,total)) as hours')
            ->whereBetween('date', [$startDate, $endDate])
            ->where('activity', '=', 'AT RISK PATIENT')
            ->first();

        $riskMin = DB::table('security_dispatches')
            ->selectRaw('SUM(DATEPART(n,total)) as minutes')
            ->whereBetween('date', [$startDate, $endDate])
            ->where('activity', '=', 'AT RISK PATIENT')
            ->first();

        $risk = $riskHours->hours;
        $min = $riskMin->minutes;
        $counter = 0;

        //dd($min);


        for($c = 0; $c <= $counter; $c++)
        { 
            if($min >= 60)
            {
                $min -= 60;
                $risk += 1;
                $counter += 1;
            }
        }

        // dd($min, 'min', $risk, 'hour', 'Totals', $riskHours, $riskMin);

        $ccs = DB::table('security_dispatches')
            ->selectRaw('activity, COUNT(activity) as total')
            ->whereBetween('date', [$startDate, $endDate])
            ->where('activity', '=', 'CODE BLUE')
            ->orWhereBetween('date', [$startDate, $endDate])
            ->where('activity', '=', 'RAPID RESPONSE')
            ->groupBy('activity')
            ->orderBy('total', 'desc')
            ->get();
    
        $cc = 0;

        foreach($ccs as $t)
        {
            $cc += $t->total;
        } 

        $safety = SecurityDispatch::where('activity', '=', 'SAFETY VIOLATION')
            ->whereBetween('date', [$startDate, $endDate])
            ->get(); 

        $parking = SecurityCitation::whereBetween('date', [$startDate, $endDate])
            ->get(); 

        $escorts = DB::table('security_dispatches')
            ->selectRaw('activity, COUNT(activity) as total')
            ->whereBetween('date', [$startDate, $endDate])
            ->where('activity', '=', 'ESCORT')
            ->orWhereBetween('date', [$startDate, $endDate])
            ->where('activity', '=', 'CODE F ESCORT')
            ->groupBy('activity')
            ->orderBy('total', 'desc')
            ->get(); // remove code 

        $escort = 0;

        foreach($escorts as $t)
        {
            $escort += $t->total;
        }

        $codeGrey = DB::table('security_dispatches')
            ->selectRaw('activity, COUNT(activity) as total')
            ->whereBetween('date', [$startDate, $endDate])
            ->where('activity', '=', 'CODE GREY - LEVEL 1')
            ->orWhereBetween('date', [$startDate, $endDate])
            ->where('activity', '=', 'CODE GREY - LEVEL 2')
            ->groupBy('activity')
            ->orderBy('total', 'desc')
            ->get(); 

        $grey = 0;

        foreach($codeGrey as $t)
        {
            $grey += $t->total;
        }

        $missing_item = SecurityDispatch::where('activity', '=', 'MISSING ITEM')
            ->whereBetween('date', [$startDate, $endDate])
            ->get(); 

        $screened = SecurityBadges::whereBetween('date', [$startDate, $endDate])
            ->sum('total');

        $arrests = SecurityDispatch::where('activity', '=', 'ASSIST WITH ARREST')
            ->whereBetween('date', [$startDate, $endDate])
            ->get(); 

        $breakIn = SecurityDispatch::where('activity', '=', 'BREAK IN')
            ->whereBetween('date', [$startDate, $endDate])
            ->get(); 

        $forenPT = SecurityDispatch::where('activity', '=', 'FORENSIC PATIENT')
            ->whereBetween('date', [$startDate, $endDate])
            ->get(); 

        $forenOFF = SecurityDispatch::where('activity', '=', 'FORENSIC OFFICER ORIENTATED')
            ->whereBetween('date', [$startDate, $endDate])
            ->get(); 

        $ptValUp = SecurityDispatch::where('activity', '=', 'PT VALUABLE PICK UP')
            ->whereBetween('date', [$startDate, $endDate])
            ->get(); 

        $ptValReturn = SecurityDispatch::where('activity', '=', 'PT VALUABLE RETURN')
            ->whereBetween('date', [$startDate, $endDate])
            ->get();

        $harass = SecurityDispatch::where('activity', '=', 'HARASSMENT (WPV)')
            ->whereBetween('date', [$startDate, $endDate])
            ->get(); 

        $threat = SecurityDispatch::where('activity', '=', 'THREAT (WPV)')
            ->whereBetween('date', [$startDate, $endDate])
            ->get();

        $disorderFight = SecurityDispatch::where('activity', '=', 'DISORDER/FIGHT (WPV)')
            ->whereBetween('date', [$startDate, $endDate])
            ->get(); 

        $unauthorized = SecurityDispatch::where('activity', '=', 'UNAUTHORIZED PERSON')
            ->whereBetween('date', [$startDate, $endDate])
            ->get();

        $incidents = SecurityIncident::whereBetween('datetime', [$startDate, $endDate])
            ->count();

        $smoking = SecurityDispatch::where('activity', '=', 'SMOKING VIOLATION')
            ->whereBetween('date', [$startDate, $endDate])
            ->get();

        return view('sgmcsecurity.reports.matrixReport', compact('startDate', 'endDate', 'doors', 'weapon', 'riskPT', 'risk', 'min', 'cc', 'grey', 'safety', 'parking',
                                                                 'escort', 'missing_item', 'screened', 'arrests', 'breakIn', 'forenPT', 'forenOFF', 'ptValUp', 'ptValReturn', 'harass',
                                                                 'threat', 'disorderFight', 'unauthorized', 'incidents', 'smoking'));
    }


}
