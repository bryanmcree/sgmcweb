<?php

namespace App\Http\Controllers;

use App\CostCenters;
use App\Questions;
use App\Survey;
use App\SurveyAnswers;
use App\SurveyDropdown;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use Webpatser\Uuid\Uuid;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;

class SurveyController extends Controller
{
    public function inactive()
    {
        return view('survey.inactive');
    }
    public function index()
    {
        //Pull all users surveys
        $user_survey = Survey::where('created_by','=', \Auth::user()->employee_number)
            ->get();
        //Pull public surveys
        $public_survey = Survey::where('visibility','=',1)
            ->where('active','=',1)
            ->get();

        $taken = SurveyAnswers::select('incident_id','survey_id')
            ->wherenotnull('incident_id')
            ->groupby(['incident_id','survey_id'])
            ->get();
        //dd($taken);


        return view('survey.index', compact('user_survey','taken','public_survey'));
    }

    //We got the first two fields lets build the rest.
    public function create_survey(Requests\SurveyRequest $request){

        $request->offsetSet('id', Uuid::generate(4));
        $survey = $request->all();

        Survey::create($survey);

        //Take the user to the detail view of the new survey
        return redirect('/survey/details/'. $request->id);
    }

    public function survey_detail($id){
        //This is where all the functions of the survey will live

        //Pulls the main survey
        $survey = Survey::find($id);
        //Pulls all the questions for the above survey
        $questions = Questions::with('question_answers')
            ->where('survey_id','=',$id)
            ->orderby('question_order')
            ->get();
        //dd($questions);

        // $who_took = SurveyAnswers::select('submitted_by','created_at','incident_id')
        //     //->with('submittedBy')
        //     ->where('survey_id','=',$id)
        //     //->groupby(['submitted_by','created_at'])
        //     ->orderby('created_at')
        //     ->get();
        //dd($who_took);

        $yesno = SurveyAnswers::selectRaw('question_id, answer, count(*) as total')
            ->where('question_type','=','YesNo')
            ->groupby(['question_id','answer'])
            ->orderby('answer','DESC')
            ->get();
        //dd($yesno);

        $yesnoNA = SurveyAnswers::selectRaw('question_id, answer, count(*) as total')
            ->where('question_type','=','YesNoNA')
            ->groupby(['question_id','answer'])
            ->orderby('answer','DESC')
            ->get();
        // dd($yesnoNA);

        $scale5 = SurveyAnswers::selectRaw('question_id, answer, count(*) as total')
            ->where('question_type','=','Scale_5')
            ->groupby(['question_id','answer'])
            ->get();
        //dd($scale5);

        $scale10 = SurveyAnswers::selectRaw('question_id, answer, count(*) as total')
            ->where('question_type','=','Scale_10')
            ->groupby(['question_id','answer'])
            ->get();
        //dd($scale10);

        $cost_centers = CostCenters::where('active_status','=',1)
            ->orderby('style1')
            ->get();

        $dropdown = SurveyDropdown::where('survey_id','=',$id)
            ->get();

        $joinDrop = SurveyAnswers::selectRaw('survey_answers.question_id, survey_dropdown.dropdown_choice, count(*) as total_count')
            ->leftJoin('survey_dropdown', 'survey_dropdown.id', '=', 'survey_answers.answer_string')
            ->where('survey_answers.survey_id','=', $id)
            ->wherenotnull('survey_dropdown.dropdown_choice')
            ->groupBy(['survey_dropdown.dropdown_choice', 'survey_answers.question_id'])
            ->get();

        // dd($joinDrop);
        
        return view('survey.details', compact('survey','questions', 'joinDrop','yesno','scale5','scale10','cost_centers','dropdown', 'yesnoNA'));
    }

    public function report(Survey $survey, Request $request)
    {
        $startDate = Carbon::parse($request->startDate)->format('Y-m-d H:i:s');
        $endDate = Carbon::parse($request->endDate)->addDay()->format('Y-m-d H:i:s');

        $questions = Questions::with('question_answers')
            ->where('survey_id','=',$survey->id)
            ->orderby('question_order')
            ->get();
        //dd($questions);

        $short = SurveyAnswers::where('survey_id','=', $survey->id)
            ->whereBetween('created_at', [$startDate, $endDate])
            ->where('question_type','=','Short Answer')
            ->orderby('created_at', 'desc')
            ->get();

        $yesno = SurveyAnswers::selectRaw('question_id, answer, count(*) as total')
            ->whereBetween('created_at', [$startDate, $endDate])
            ->where('question_type','=','YesNo')
            ->groupby(['question_id','answer'])
            ->orderby('answer','DESC')
            ->get();
        //dd($yesno);

        $yesnoNA = SurveyAnswers::selectRaw('question_id, answer, count(*) as total')
            ->whereBetween('created_at', [$startDate, $endDate])
            ->where('question_type','=','YesNoNA')
            ->groupby(['question_id','answer'])
            ->orderby('answer','DESC')
            ->get();
        // dd($yesnoNA);

        $scale5 = SurveyAnswers::selectRaw('question_id, answer, count(*) as total')
            ->whereBetween('created_at', [$startDate, $endDate])
            ->where('question_type','=','Scale_5')
            ->groupby(['question_id','answer'])
            ->get();
        //dd($scale5);

        $scale10 = SurveyAnswers::selectRaw('question_id, answer, count(*) as total')
            ->whereBetween('created_at', [$startDate, $endDate])
            ->where('question_type','=','Scale_10')
            ->groupby(['question_id','answer'])
            ->get();
        //dd($scale10);

        $dropdown = SurveyDropdown::where('survey_id','=',$survey->id)
            ->get();

        $joinDrop = SurveyAnswers::selectRaw('survey_answers.question_id, survey_dropdown.dropdown_choice, count(*) as total_count')
            ->whereBetween('survey_answers.created_at', [$startDate, $endDate])
            ->leftJoin('survey_dropdown', 'survey_dropdown.id', '=', 'survey_answers.answer_string')
            ->where('survey_answers.survey_id','=', $survey->id)
            ->wherenotnull('survey_dropdown.dropdown_choice')
            ->groupBy(['survey_dropdown.dropdown_choice', 'survey_answers.question_id'])
            ->get();

        return view('survey.report', compact('survey','questions', 'joinDrop','yesno','scale5','scale10','dropdown', 'yesnoNA', 'short'));
    }

    public function short_results($survey_id, $question_id)
    {
        $survey = Survey::find($survey_id);

        $results = SurveyAnswers::where('question_id', '=', $question_id)
            ->orderby('created_at', 'desc')
            ->get();

        //dd($results);

        return view('survey.shortresults', compact('survey', 'results'));
    }

    public function emp_results($survey_id, $question_id)
    {
        $survey = Survey::find($survey_id);


        $results = SurveyAnswers::where('question_id', '=', $question_id)
            ->orderby('created_at', 'desc')
            ->get();

        //dd($results);

        return view('survey.empresults', compact('survey', 'results'));
    }

    public function cc_results($survey_id, $question_id)
    {
        $survey = Survey::find($survey_id);


        $results = SurveyAnswers::where('question_id', '=', $question_id)  //->wherenotnull('answers')  if you want to remove empty rows from view
            ->orderby('created_at', 'desc')
            ->get();

        //dd($results);

        return view('survey.ccresults', compact('survey', 'results'));
    }

    public function datetime_results($survey_id, $question_id)
    {
        $survey = Survey::find($survey_id);

        $results = SurveyAnswers::where('question_id', '=', $question_id)  //->wherenotnull('answers')  if you want to remove empty rows from view
            ->orderby('created_at', 'desc')
            ->get();

        // dd($results);

        return view('survey.datetime_results', compact('survey', 'results'));
    }

    public function pie($survey_id, $question_id)
    {
        $survey = Survey::find($survey_id);

        $results = SurveyAnswers::where('question_id', '=', $question_id)  //->wherenotnull('answers')  if you want to remove empty rows from view
            ->orderby('created_at', 'desc')
            ->get();
            
        return view('survey.pie', compact('survey', 'results'));
    }

    public function survey_options($id){

        $survey = Survey::findOrFail($id);

        $survey->update(Input::all());

        alert()->success('Survey options have been saved!', 'Survey Options');

        //Take the user to the detail view of the new survey
        return redirect('/survey/details/'. $id);
    }

    public function view_survey($id){
        //Pulls the main survey
        $survey = Survey::find($id);

        $questions = Questions::with('question_answers')
            ->where('survey_id','=',$id)
            ->orderby('question_order')
            ->get();

        $dropdown = SurveyDropdown::where('survey_id','=',$id)
            ->get();

        $cost_centers = CostCenters::where('active_status','=',1)
            ->orderby('style1')
            ->get();
        //dd($questions);

        return view('survey.survey', compact('survey','questions','cost_centers','dropdown'));

    }

    public function thank_you($id){

        $user_check = SurveyAnswers::where('survey_id', '=', $id)
            ->where('submitted_by','=', \Auth::user()->employee_number)
            ->first();

        return view('survey.thankyou', compact('user_check','questions'));
    }

    public function update_order(Requests\SurveyRequest $request)
    {
        // allows users to drag and drop the order of the questions.

        $questions = Questions::where('survey_id', '=', $request->survey_id)->get();
        //dd($questions);

        foreach ($questions as $question) {
            $question->timestamps = false; // To disable update_at field updation
            $id = $question->id;

            foreach ($request->order as $order) {
                if ($order['id'] == $id) {
                    $question->update(['question_order' => $order['position']]);
                }
            }
        }

        return response('Update Successfully.', 200);
    }

    public function employee_search()
    {
        //This is the auto complete function for the employee select
        $data = User::select("name",'employee_number')
            ->where('name', 'LIKE','%' . Input::get('query') . '%')
            ->whereIn('emp_status',['Active','Contract','Leave of Absence','Intermittent FMLA'])
            ->wherenull('deleted_at')
            ->orderby('name')
            ->get();
        //var_dump($data);

        foreach ($data as $key => $row) {
            $results['suggestions'][] = array('value' => $row['name'],'data' => $row['employee_number']);
        }

        return \Response::json($results);
    }

    public function delete_survey($id){
        //find the question to delete
        $survey = Survey::where('id','=',$id)
            ->first();
        //dd($question);
        $survey->delete();
        //find the answers to the question to delete

        alert()->success('Question Deleted!', 'Success!');
        return redirect('/survey');

    }

    public function delete_question($id){
        //find the question to delete
        $question = Questions::where('id','=',$id)
            ->first();
        //dd($question);

        //If question is a dropdown we need to delete the choices also.
        if($question->question_type == 'dropdown'){
            $dropdown = SurveyDropdown::where('survey_id','=', Input::get('survey_id'))
                ->delete();
            //dd($dropdown);
            //$dropdown->delete();
        }

        //dd($question);
        $question->delete();
        //find the answers to the question to delete

        alert()->success('Question Deleted!', 'Success!');
        return redirect('/survey/details/' . Input::get('survey_id'));

    }

    public function printSurvey($id) {

        $survey = Survey::where('id','=',$id)
            ->first();

        //Pulls all the questions for the above survey
        $questions = Questions::with('question_answers')
            ->where('survey_id','=',$id)
            ->orderby('question_order')
            ->get();

        $answers = SurveyAnswers::where('incident_id','=', Input::get('incident_id'))
            ->with('surveyQuestion')
            ->where('survey_id','=', $id)
            ->get();

        //dd($answers);

        //dd($suvey);
        return view('survey.printview', compact('survey','questions','answers'));

    }

}
