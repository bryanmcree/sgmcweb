<?php

namespace App\Http\Controllers;

use App\Termination;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Employee;
use Illuminate\Support\Facades\Input;
use DB;
use PDF;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Listeners\UpdateLastLoginOnLogin;
use App\History;
use App\User;
use App\SystemList;
use App\State;
class WebFormsController extends Controller
{
    public function index(){
        $term = Termination::where('notice_by','=',\Auth::user()->employee_number)->get();
        //dd($term);
        return view('WebForms.index', compact('term'));
    }

    public function search(){

        $search = Input::get('search');

        $tests = User::
        where('emp_status', '=', 'Active')
            ->where(function($query) use ($search){
                $query->Where('first_name', 'LIKE', '%' . $search . '%');
                $query->orWhere('last_name', 'LIKE', '%' . $search . '%');
                $query->orWhere('employee_number', 'LIKE', '%' . $search . '%');
                $query->orWhere('username', 'LIKE', '%' . $search . '%');
            })
        ->orderBy('last_name')
        ->get();



        //dd($query);
        //event(new UpdateLastLoginOnLogin('test'));

        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'WebForm Search';
        $history->userid = \Auth::user()->username;
        $history->search_string = $search;
        $history->user_ip = \Request::ip();
        $history->save();

        return View('WebForms.search', compact('tests'));
    }

    public function userSelect($id){
        $user = User::findOrFail($id);
        //dd($user);
        return  View('WebForms.selected', compact('user','roles'));
    }
    
    public function addTerm(){
        //dd(Input::all());
        $term = Input::except('_token');
        Termination::create($term);

        alert()->success('New system has been created!', 'Success!');


        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'WebForm - Termination';
        $history->userid = \Auth::user()->username;
        $history->search_string = 'N/A';
        $history->user_ip = \Request::ip();
        $history->save();
        return redirect('/WebForms/');

    }

    public function updateLastDay(){
        //dd(Input::get('ldac'));
        $lastday = Termination::find( Input::get('id') );

        $lastday->update(Input::only(['last_day']));
        //dd($servers);
        alert()->success('Last Day has been updated!', 'Success!');

        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'WebForms Update Term';
        $history->userid = \Auth::user()->username;
        $history->search_string = 'N/A';
        $history->user_ip = \Request::ip();
        $history->save();

        return redirect('/WebForms');
    }

    public function delete($id)
    {
        $term = Termination::find( $id );
        $term->delete();

        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'WebForms Term Deleted';
        $history->userid = \Auth::user()->username;
        $history->search_string = $id;
        $history->user_ip = \Request::ip();
        $history->save();

        return redirect('/WebForms');
    }

    public function physicianIndex(){
        $state = State::select('name', 'code')->get();

        $SystemList = SystemList::with('systemContacts', 'systemServers')
            ->where('inv_visiable','=', Null)
            ->orderBy('sys_name')
            ->get();

        return  View('WebForms.physician.index', compact('SystemList','state'));
    }
}
