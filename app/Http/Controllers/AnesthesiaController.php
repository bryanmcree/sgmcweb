<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon;
use App\Anesthesia;
use Webpatser\Uuid\Uuid;
use App\Http\Requests;

class AnesthesiaController extends Controller
{
    public function index()
    {
        $data = Anesthesia::selectRaw('id, date, MRN, anesthesiologist, type, emergency_procedure')
            ->wherenotnull('id')
            ->orderBy('date', 'desc')
            ->get();

        $yes = Anesthesia::wherenotnull('id')->sum('comp_yes');
        $no = Anesthesia::wherenotnull('id')->sum('comp_no');

        $total = $yes + $no;

        if($total > 0)
        {
            $actual = ($yes / $total) * 100;
        }
        else 
        {
            $actual = 0; $total = 0;
        }
        
        // dd($yes, $actual, 'total = ' . $total);

        $doctors = Anesthesia::selectRaw('anesthesiologist, SUM(comp_yes) as yes, SUM(comp_no) as no')->wherenotnull('id')->groupby('anesthesiologist')->get();

        $preop = Anesthesia::where('preop', '=', 'Yes')->count();
        $preop_30 = Anesthesia::where('preop_30', '=', 'Yes')->count();
        $ASA = Anesthesia::where('ASA', '=', 'Yes')->count();
        $providers = Anesthesia::where('providers', '=', 'Yes')->count();
        $review_48 = Anesthesia::where('review_48', '=', 'Yes')->count();
        $airway = Anesthesia::where('airway', '=', 'Yes')->count();
        $machine_monitors = Anesthesia::where('machine_monitors', '=', 'Yes')->count();
        $doc_CRNA = Anesthesia::where('doc_CRNA', '=', 'Yes')->count();
        $doc_MDstaff = Anesthesia::where('doc_MDstaff', '=', 'Yes')->count();
        $induction_time = Anesthesia::where('induction_time', '=', 'Yes')->count();
        $doc_position = Anesthesia::where('doc_position', '=', 'Yes')->count();
        $doc_intubation = Anesthesia::where('doc_intubation', '=', 'Yes')->count();
        $doc_flowrate = Anesthesia::where('doc_flowrate', '=', 'Yes')->count();
        $doc_vital_5 = Anesthesia::where('doc_vital_5', '=', 'Yes')->count();
        $doc_name_time = Anesthesia::where('doc_name_time', '=', 'Yes')->count();
        $doc_IV_amount = Anesthesia::where('doc_IV_amount', '=', 'Yes')->count();
        $doc_blood_amount = Anesthesia::where('doc_blood_amount', '=', 'Yes')->count();
        $doc_blood_loss = Anesthesia::where('doc_blood_loss', '=', 'Yes')->count();
        $urine_output = Anesthesia::where('urine_output', '=', 'Yes')->count();
        $doc_PACU_transfer = Anesthesia::where('doc_PACU_transfer', '=', 'Yes')->count();
        $doc_normothermia = Anesthesia::where('doc_normothermia', '=', 'Yes')->count();
        $doc_PACU_ICU = Anesthesia::where('doc_PACU_ICU', '=', 'Yes')->count();
        $post_anesthesia = Anesthesia::where('post_anesthesia', '=', 'Yes')->count();
        $narcan = Anesthesia::where('narcan', '=', 'Yes')->count();
        $reversal_agent = Anesthesia::where('reversal_agent', '=', 'Yes')->count();
        $postop_nausea = Anesthesia::where('postop_nausea', '=', 'Yes')->count();
        $cardiac_arrest = Anesthesia::where('cardiac_arrest', '=', 'Yes')->count();
        $anaphylaxis = Anesthesia::where('anaphylaxis', '=', 'Yes')->count();
        $malignant_hypo = Anesthesia::where('malignant_hypo', '=', 'Yes')->count();
        $incorrect_site = Anesthesia::where('incorrect_site', '=', 'Yes')->count();
        $med_error = Anesthesia::where('med_error', '=', 'Yes')->count();
        $intraoperative = Anesthesia::where('intraoperative', '=', 'Yes')->count();
        $difficult_air = Anesthesia::where('difficult_air', '=', 'Yes')->count();
        $unplanned_reintubation = Anesthesia::where('unplanned_reintubation', '=', 'Yes')->count();
        $dental = Anesthesia::where('dental', '=', 'Yes')->count();
        $spinal = Anesthesia::where('spinal', '=', 'Yes')->count();
        $flash_edema = Anesthesia::where('flash_edema', '=', 'Yes')->count();
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $preop_rows = Anesthesia::where('preop', '<>', 'NA')->count();
        $preop_30_rows = Anesthesia::where('preop_30', '<>', 'NA')->count();
        $ASA_rows = Anesthesia::where('ASA', '<>', 'NA')->count();
        $providers_rows = Anesthesia::where('providers', '<>', 'NA')->count();
        $review_48_rows = Anesthesia::where('review_48', '<>', 'NA')->count();
        $airway_rows = Anesthesia::where('airway', '<>', 'NA')->count();
        $machine_monitors_rows = Anesthesia::where('machine_monitors', '<>', 'NA')->count();
        $doc_CRNA_rows = Anesthesia::where('doc_CRNA', '<>', 'NA')->count();
        $doc_MDstaff_rows = Anesthesia::where('doc_MDstaff', '<>', 'NA')->count();
        $induction_time_rows = Anesthesia::where('induction_time', '<>', 'NA')->count();
        $doc_position_rows = Anesthesia::where('doc_position', '<>', 'NA')->count();
        $doc_intubation_rows = Anesthesia::where('doc_intubation', '<>', 'NA')->count();
        $doc_flowrate_rows = Anesthesia::where('doc_flowrate', '<>', 'NA')->count();
        $doc_vital_5_rows = Anesthesia::where('doc_vital_5', '<>', 'NA')->count();
        $doc_name_time_rows = Anesthesia::where('doc_name_time', '<>', 'NA')->count();
        $doc_IV_amount_rows = Anesthesia::where('doc_IV_amount', '<>', 'NA')->count();
        $doc_blood_amount_rows = Anesthesia::where('doc_blood_amount', '<>', 'NA')->count();
        $doc_blood_loss_rows = Anesthesia::where('doc_blood_loss', '<>', 'NA')->count();
        $urine_output_rows = Anesthesia::where('urine_output', '<>', 'NA')->count();
        $doc_PACU_transfer_rows = Anesthesia::where('doc_PACU_transfer', '<>', 'NA')->count();
        $doc_normothermia_rows = Anesthesia::where('doc_normothermia', '<>', 'NA')->count();
        $doc_PACU_ICU_rows = Anesthesia::where('doc_PACU_ICU', '<>', 'NA')->count();
        $post_anesthesia_rows = Anesthesia::where('post_anesthesia', '<>', 'NA')->count();

        $rows = Anesthesia::wherenotnull('id')->count();


        return view('anesthesia.index', compact('data', 'yes', 'actual', 'total', 'doctors', 'preop', 'preop_30', 'ASA', 'providers', 'review_48', 'airway', 'machine_monitors'
                                                , 'doc_CRNA', 'doc_MDstaff', 'induction_time', 'doc_position', 'doc_intubation', 'doc_flowrate', 'doc_vital_5', 'doc_name_time', 'doc_IV_amount', 'doc_blood_amount'
                                                , 'doc_blood_loss', 'urine_output', 'doc_PACU_transfer', 'doc_normothermia', 'doc_PACU_ICU', 'post_anesthesia', 'narcan', 'reversal_agent'
                                                , 'postop_nausea', 'cardiac_arrest', 'anaphylaxis', 'malignant_hypo', 'incorrect_site', 'med_error', 'intraoperative', 'difficult_air'
                                                , 'unplanned_reintubation', 'dental', 'spinal', 'flash_edema', 'rows', 'preop_rows', 'preop_30_rows', 'ASA_rows', 'providers_rows'
                                                , 'review_48_rows', 'airway_rows', 'machine_monitors_rows', 'doc_CRNA_rows', 'doc_MDstaff_rows', 'induction_time_rows', 'doc_position_rows'
                                                , 'doc_intubation_rows', 'doc_flowrate_rows', 'doc_vital_5_rows', 'doc_name_time_rows', 'doc_IV_amount_rows', 'doc_blood_amount_rows'
                                                , 'doc_blood_loss_rows', 'urine_output_rows', 'doc_PACU_transfer_rows', 'doc_normothermia_rows', 'doc_PACU_ICU_rows', 'post_anesthesia_rows'));
    }

    public function create()
    {
        return view('anesthesia.create');
    }

    public function show(Anesthesia $anesthesia)
    {
        return view('anesthesia.show', compact('anesthesia'));
    }

    public function edit(Anesthesia $anesthesia)
    {
        return view('anesthesia.edit', compact('anesthesia'));
    }

    public function update(Request $request, Anesthesia $a)
    {
        $request->offsetSet('date', Carbon::parse($request->date)->format('Y-m-d H:i:s'));

        $yes = 0;
        $no = 0;
        $na = 0;

        // Ugly but works
            if($request->preop == 'Yes')
            {
                $yes += 1;
            }
            elseif($request->preop == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($request->preop_30 == 'Yes')
            {
                $yes += 1;
            }
            elseif($request->preop_30 == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($request->ASA == 'Yes')
            {
                $yes += 1;
            }
            elseif($request->ASA == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($request->providers == 'Yes')
            {
                $yes += 1;
            }
            elseif($request->providers == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($request->review_48 == 'Yes')
            {
                $yes += 1;
            }
            elseif($request->review_48 == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($request->airway == 'Yes')
            {
                $yes += 1;
            }
            elseif($request->airway == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($request->machine_monitors == 'Yes')
            {
                $yes += 1;
            }
            elseif($request->machine_monitors == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }


            if($request->doc_CRNA == 'Yes')
            {
                $yes += 1;
            }
            elseif($request->doc_CRNA == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($request->doc_MDstaff == 'Yes')
            {
                $yes += 1;
            }
            elseif($request->doc_MDstaff == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($request->induction_time == 'Yes')
            {
                $yes += 1;
            }
            elseif($request->induction_time == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($request->doc_position == 'Yes')
            {
                $yes += 1;
            }
            elseif($request->doc_position == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($request->doc_intubation == 'Yes')
            {
                $yes += 1;
            }
            elseif($request->doc_intubation == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($request->doc_flowrate == 'Yes')
            {
                $yes += 1;
            }
            elseif($request->doc_flowrate == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($request->doc_vital_5 == 'Yes')
            {
                $yes += 1;
            }
            elseif($request->doc_vital_5 == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($request->doc_name_time == 'Yes')
            {
                $yes += 1;
            }
            elseif($request->doc_name_time == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($request->doc_IV_amount == 'Yes')
            {
                $yes += 1;
            }
            elseif($request->doc_IV_amount == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($request->doc_blood_amount == 'Yes')
            {
                $yes += 1;
            }
            elseif($request->doc_blood_amount == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($request->doc_blood_loss == 'Yes')
            {
                $yes += 1;
            }
            elseif($request->doc_blood_loss == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($request->urine_output == 'Yes')
            {
                $yes += 1;
            }
            elseif($request->urine_output == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($request->doc_PACU_transfer == 'Yes')
            {
                $yes += 1;
            }
            elseif($request->doc_PACU_transfer == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($request->doc_normothermia == 'Yes')
            {
                $yes += 1;
            }
            elseif($request->doc_normothermia == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($request->doc_PACU_ICU == 'Yes')
            {
                $yes += 1;
            }
            elseif($request->doc_PACU_ICU == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($request->post_anesthesia == 'Yes')
            {
                $yes += 1;
            }
            elseif($request->post_anesthesia == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($request->narcan == 'No')
            {
                $yes += 1;
            }
            else
            {
                $no += 1;
            }

            if($request->reversal_agent == 'No')
            {
                $yes += 1;
            }
            else
            {
                $no += 1;
            }

            if($request->postop_nausea == 'No')
            {
                $yes += 1;
            }
            else
            {
                $no += 1;
            }

            if($request->cardiac_arrest == 'No')
            {
                $yes += 1;
            }
            else
            {
                $no += 1;
            }

            if($request->anaphylaxis == 'No')
            {
                $yes += 1;
            }
            else
            {
                $no += 1;
            }

            if($request->malignant_hypo == 'No')
            {
                $yes += 1;
            }
            else
            {
                $no += 1;
            }

            if($request->incorrect_site == 'No')
            {
                $yes += 1;
            }
            else
            {
                $no += 1;
            }

            if($request->med_error == 'No')
            {
                $yes += 1;
            }
            else
            {
                $no += 1;
            }

            if($request->intraoperative == 'No')
            {
                $yes += 1;
            }
            else
            {
                $no += 1;
            }

            if($request->difficult_air == 'No')
            {
                $yes += 1;
            }
            else
            {
                $no += 1;
            }

            if($request->unplanned_reintubation == 'No')
            {
                $yes += 1;
            }
            else
            {
                $no += 1;
            }

            if($request->dental == 'No')
            {
                $yes += 1;
            }
            else
            {
                $no += 1;
            }

            if($request->spinal == 'No')
            {
                $yes += 1;
            }
            else
            {
                $no += 1;
            }

            if($request->flash_edema == 'No')
            {
                $yes += 1;
            }
            else
            {
                $no += 1;
            }

        $request->offsetSet('comp_yes', $yes);
        $request->offsetSet('comp_no', $no);
        $request->offsetSet('comp_NA', $na);

        // dd($request->all());

        $a->update($request->all());


        alert()->success('Success', 'Form Updated!');
        return redirect()->back();
    }

    public function report(Request $request)
    {
        $startDate = Carbon::parse($request->startDate)->format('Y-m-d H:i:s');
        $endDate = Carbon::parse($request->endDate)->endOfDay()->format('Y-m-d H:i:s');
        
        if(empty($request->doctor))
        {
            $yes = Anesthesia::whereBetween('date', [$startDate, $endDate])->sum('comp_yes');
            $no = Anesthesia::whereBetween('date', [$startDate, $endDate])->sum('comp_no');

            $total = $yes + $no;

            if($total > 0)
            {
                $actual = ($yes / $total) * 100;
            }
            else 
            {
                $actual = 0; $total = 0;
            }

            $doctors = Anesthesia::selectRaw('anesthesiologist, SUM(comp_yes) as yes, SUM(comp_no) as no')->whereBetween('date', [$startDate, $endDate])->groupby('anesthesiologist')->get();

            $doc = FALSE;
            // dd($startDate, $endDate, $yes, $actual, $doctors);

            $preop = Anesthesia::where('preop', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->count();
            $preop_30 = Anesthesia::where('preop_30', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->count();
            $ASA = Anesthesia::where('ASA', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->count();
            $providers = Anesthesia::where('providers', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->count();
            $review_48 = Anesthesia::where('review_48', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->count();
            $airway = Anesthesia::where('airway', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->count();
            $machine_monitors = Anesthesia::where('machine_monitors', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->count();
            $doc_CRNA = Anesthesia::where('doc_CRNA', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->count();
            $doc_MDstaff = Anesthesia::where('doc_MDstaff', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->count();
            $induction_time = Anesthesia::where('induction_time', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->count();
            $doc_position = Anesthesia::where('doc_position', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->count();
            $doc_intubation = Anesthesia::where('doc_intubation', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->count();
            $doc_flowrate = Anesthesia::where('doc_flowrate', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->count();
            $doc_vital_5 = Anesthesia::where('doc_vital_5', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->count();
            $doc_name_time = Anesthesia::where('doc_name_time', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->count();
            $doc_IV_amount = Anesthesia::where('doc_IV_amount', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->count();
            $doc_blood_amount = Anesthesia::where('doc_blood_amount', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->count();
            $doc_blood_loss = Anesthesia::where('doc_blood_loss', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->count();
            $urine_output = Anesthesia::where('urine_output', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->count();
            $doc_PACU_transfer = Anesthesia::where('doc_PACU_transfer', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->count();
            $doc_normothermia = Anesthesia::where('doc_normothermia', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->count();
            $doc_PACU_ICU = Anesthesia::where('doc_PACU_ICU', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->count();
            $post_anesthesia = Anesthesia::where('post_anesthesia', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->count();
            $narcan = Anesthesia::where('narcan', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->count();
            $reversal_agent = Anesthesia::where('reversal_agent', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->count();
            $postop_nausea = Anesthesia::where('postop_nausea', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->count();
            $cardiac_arrest = Anesthesia::where('cardiac_arrest', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->count();
            $anaphylaxis = Anesthesia::where('anaphylaxis', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->count();
            $malignant_hypo = Anesthesia::where('malignant_hypo', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->count();
            $incorrect_site = Anesthesia::where('incorrect_site', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->count();
            $med_error = Anesthesia::where('med_error', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->count();
            $intraoperative = Anesthesia::where('intraoperative', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->count();
            $difficult_air = Anesthesia::where('difficult_air', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->count();
            $unplanned_reintubation = Anesthesia::where('unplanned_reintubation', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->count();
            $dental = Anesthesia::where('dental', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->count();
            $spinal = Anesthesia::where('spinal', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->count();
            $flash_edema = Anesthesia::where('flash_edema', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->count();
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $preop_rows = Anesthesia::where('preop', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->count();
            $preop_30_rows = Anesthesia::where('preop_30', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->count();
            $ASA_rows = Anesthesia::where('ASA', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->count();
            $providers_rows = Anesthesia::where('providers', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->count();
            $review_48_rows = Anesthesia::where('review_48', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->count();
            $airway_rows = Anesthesia::where('airway', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->count();
            $machine_monitors_rows = Anesthesia::where('machine_monitors', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->count();
            $doc_CRNA_rows = Anesthesia::where('doc_CRNA', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->count();
            $doc_MDstaff_rows = Anesthesia::where('doc_MDstaff', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->count();
            $induction_time_rows = Anesthesia::where('induction_time', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->count();
            $doc_position_rows = Anesthesia::where('doc_position', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->count();
            $doc_intubation_rows = Anesthesia::where('doc_intubation', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->count();
            $doc_flowrate_rows = Anesthesia::where('doc_flowrate', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->count();
            $doc_vital_5_rows = Anesthesia::where('doc_vital_5', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->count();
            $doc_name_time_rows = Anesthesia::where('doc_name_time', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->count();
            $doc_IV_amount_rows = Anesthesia::where('doc_IV_amount', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->count();
            $doc_blood_amount_rows = Anesthesia::where('doc_blood_amount', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->count();
            $doc_blood_loss_rows = Anesthesia::where('doc_blood_loss', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->count();
            $urine_output_rows = Anesthesia::where('urine_output', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->count();
            $doc_PACU_transfer_rows = Anesthesia::where('doc_PACU_transfer', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->count();
            $doc_normothermia_rows = Anesthesia::where('doc_normothermia', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->count();
            $doc_PACU_ICU_rows = Anesthesia::where('doc_PACU_ICU', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->count();
            $post_anesthesia_rows = Anesthesia::where('post_anesthesia', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->count();

            $rows = Anesthesia::wherenotnull('id')->whereBetween('date', [$startDate, $endDate])->count();

            return view('anesthesia.report', compact('total', 'yes', 'no', 'actual', 'doctors', 'startDate', 'endDate', 'preop', 'preop_30', 'ASA', 'providers', 'review_48', 'airway', 'machine_monitors'
                        , 'doc_CRNA', 'doc_MDstaff', 'induction_time', 'doc_position', 'doc_intubation', 'doc_flowrate', 'doc_vital_5', 'doc_name_time', 'doc_IV_amount', 'doc_blood_amount'
                        , 'doc_blood_loss', 'urine_output', 'doc_PACU_transfer', 'doc_normothermia', 'doc_PACU_ICU', 'post_anesthesia', 'narcan', 'reversal_agent'
                        , 'postop_nausea', 'cardiac_arrest', 'anaphylaxis', 'malignant_hypo', 'incorrect_site', 'med_error', 'intraoperative', 'difficult_air'
                        , 'unplanned_reintubation', 'dental', 'spinal', 'flash_edema', 'doc', 'rows', 'preop_rows', 'preop_30_rows', 'ASA_rows', 'providers_rows'
                        , 'review_48_rows', 'airway_rows', 'machine_monitors_rows', 'doc_CRNA_rows', 'doc_MDstaff_rows', 'induction_time_rows', 'doc_position_rows'
                        , 'doc_intubation_rows', 'doc_flowrate_rows', 'doc_vital_5_rows', 'doc_name_time_rows', 'doc_IV_amount_rows', 'doc_blood_amount_rows'
                        , 'doc_blood_loss_rows', 'urine_output_rows', 'doc_PACU_transfer_rows', 'doc_normothermia_rows', 'doc_PACU_ICU_rows', 'post_anesthesia_rows'));

        }
        else
        {
            $yes = Anesthesia::whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->sum('comp_yes');
            $no = Anesthesia::whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->sum('comp_no');

            $total = $yes + $no;

            if($total > 0)
            {
                $actual = ($yes / $total) * 100;
            }
            else 
            {
                $actual = 0; $total = 0;
            }

            $doctors = Anesthesia::selectRaw('anesthesiologist, SUM(comp_yes) as yes, SUM(comp_no) as no')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->groupby('anesthesiologist')->get();

            $doc = TRUE;

            $preop = Anesthesia::where('preop', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $preop_30 = Anesthesia::where('preop_30', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $ASA = Anesthesia::where('ASA', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $providers = Anesthesia::where('providers', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $review_48 = Anesthesia::where('review_48', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $airway = Anesthesia::where('airway', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $machine_monitors = Anesthesia::where('machine_monitors', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $doc_CRNA = Anesthesia::where('doc_CRNA', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $doc_MDstaff = Anesthesia::where('doc_MDstaff', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $induction_time = Anesthesia::where('induction_time', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $doc_position = Anesthesia::where('doc_position', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $doc_intubation = Anesthesia::where('doc_intubation', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $doc_flowrate = Anesthesia::where('doc_flowrate', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $doc_vital_5 = Anesthesia::where('doc_vital_5', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $doc_name_time = Anesthesia::where('doc_name_time', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $doc_IV_amount = Anesthesia::where('doc_IV_amount', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $doc_blood_amount = Anesthesia::where('doc_blood_amount', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $doc_blood_loss = Anesthesia::where('doc_blood_loss', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $urine_output = Anesthesia::where('urine_output', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $doc_PACU_transfer = Anesthesia::where('doc_PACU_transfer', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $doc_normothermia = Anesthesia::where('doc_normothermia', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $doc_PACU_ICU = Anesthesia::where('doc_PACU_ICU', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $post_anesthesia = Anesthesia::where('post_anesthesia', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $narcan = Anesthesia::where('narcan', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $reversal_agent = Anesthesia::where('reversal_agent', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $postop_nausea = Anesthesia::where('postop_nausea', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $cardiac_arrest = Anesthesia::where('cardiac_arrest', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $anaphylaxis = Anesthesia::where('anaphylaxis', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $malignant_hypo = Anesthesia::where('malignant_hypo', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $incorrect_site = Anesthesia::where('incorrect_site', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $med_error = Anesthesia::where('med_error', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $intraoperative = Anesthesia::where('intraoperative', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $difficult_air = Anesthesia::where('difficult_air', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $unplanned_reintubation = Anesthesia::where('unplanned_reintubation', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $dental = Anesthesia::where('dental', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $spinal = Anesthesia::where('spinal', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $flash_edema = Anesthesia::where('flash_edema', '=', 'Yes')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $preop_rows = Anesthesia::where('preop', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $preop_30_rows = Anesthesia::where('preop_30', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $ASA_rows = Anesthesia::where('ASA', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $providers_rows = Anesthesia::where('providers', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $review_48_rows = Anesthesia::where('review_48', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $airway_rows = Anesthesia::where('airway', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $machine_monitors_rows = Anesthesia::where('machine_monitors', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $doc_CRNA_rows = Anesthesia::where('doc_CRNA', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $doc_MDstaff_rows = Anesthesia::where('doc_MDstaff', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $induction_time_rows = Anesthesia::where('induction_time', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $doc_position_rows = Anesthesia::where('doc_position', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $doc_intubation_rows = Anesthesia::where('doc_intubation', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $doc_flowrate_rows = Anesthesia::where('doc_flowrate', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $doc_vital_5_rows = Anesthesia::where('doc_vital_5', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $doc_name_time_rows = Anesthesia::where('doc_name_time', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $doc_IV_amount_rows = Anesthesia::where('doc_IV_amount', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $doc_blood_amount_rows = Anesthesia::where('doc_blood_amount', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $doc_blood_loss_rows = Anesthesia::where('doc_blood_loss', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $urine_output_rows = Anesthesia::where('urine_output', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $doc_PACU_transfer_rows = Anesthesia::where('doc_PACU_transfer', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $doc_normothermia_rows = Anesthesia::where('doc_normothermia', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $doc_PACU_ICU_rows = Anesthesia::where('doc_PACU_ICU', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();
            $post_anesthesia_rows = Anesthesia::where('post_anesthesia', '<>', 'NA')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();

            $rows = Anesthesia::wherenotnull('id')->whereBetween('date', [$startDate, $endDate])->where('anesthesiologist', '=', $request->doctor)->count();

            return view('anesthesia.report', compact('total', 'yes', 'no', 'actual', 'doctors', 'startDate', 'endDate', 'preop', 'preop_30', 'ASA', 'providers', 'review_48', 'airway', 'machine_monitors'
            , 'doc_CRNA', 'doc_MDstaff', 'induction_time', 'doc_position', 'doc_intubation', 'doc_flowrate', 'doc_vital_5', 'doc_name_time', 'doc_IV_amount', 'doc_blood_amount'
            , 'doc_blood_loss', 'urine_output', 'doc_PACU_transfer', 'doc_normothermia', 'doc_PACU_ICU', 'post_anesthesia', 'narcan', 'reversal_agent'
            , 'postop_nausea', 'cardiac_arrest', 'anaphylaxis', 'malignant_hypo', 'incorrect_site', 'med_error', 'intraoperative', 'difficult_air'
            , 'unplanned_reintubation', 'dental', 'spinal', 'flash_edema', 'doc', 'rows', 'preop_rows', 'preop_30_rows', 'ASA_rows', 'providers_rows'
            , 'review_48_rows', 'airway_rows', 'machine_monitors_rows', 'doc_CRNA_rows', 'doc_MDstaff_rows', 'induction_time_rows', 'doc_position_rows'
            , 'doc_intubation_rows', 'doc_flowrate_rows', 'doc_vital_5_rows', 'doc_name_time_rows', 'doc_IV_amount_rows', 'doc_blood_amount_rows'
            , 'doc_blood_loss_rows', 'urine_output_rows', 'doc_PACU_transfer_rows', 'doc_normothermia_rows', 'doc_PACU_ICU_rows', 'post_anesthesia_rows'));
        }
        
    }

    public function store(Request $request)
    {
        $request->offsetSet('id', Uuid::generate(4));
        $request->offsetSet('date', Carbon::parse($request->date)->format('Y-m-d H:i:s'));

        $yes = 0;
        $no = 0;
        $na = 0;

        $anesthesia = Anesthesia::create($request->all());

        // Ugly but works
            if($anesthesia->preop == 'Yes')
            {
                $yes += 1;
            }
            elseif($anesthesia->preop == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($anesthesia->preop_30 == 'Yes')
            {
                $yes += 1;
            }
            elseif($anesthesia->preop_30 == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($anesthesia->ASA == 'Yes')
            {
                $yes += 1;
            }
            elseif($anesthesia->ASA == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($anesthesia->providers == 'Yes')
            {
                $yes += 1;
            }
            elseif($anesthesia->providers == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($anesthesia->review_48 == 'Yes')
            {
                $yes += 1;
            }
            elseif($anesthesia->review_48 == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($anesthesia->airway == 'Yes')
            {
                $yes += 1;
            }
            elseif($anesthesia->airway == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($anesthesia->machine_monitors == 'Yes')
            {
                $yes += 1;
            }
            elseif($anesthesia->machine_monitors == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }


            if($anesthesia->doc_CRNA == 'Yes')
            {
                $yes += 1;
            }
            elseif($anesthesia->doc_CRNA == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($anesthesia->doc_MDstaff == 'Yes')
            {
                $yes += 1;
            }
            elseif($anesthesia->doc_MDstaff == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($anesthesia->induction_time == 'Yes')
            {
                $yes += 1;
            }
            elseif($anesthesia->induction_time == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($anesthesia->doc_position == 'Yes')
            {
                $yes += 1;
            }
            elseif($anesthesia->doc_position == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($anesthesia->doc_intubation == 'Yes')
            {
                $yes += 1;
            }
            elseif($anesthesia->doc_intubation == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($anesthesia->doc_flowrate == 'Yes')
            {
                $yes += 1;
            }
            elseif($anesthesia->doc_flowrate == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($anesthesia->doc_vital_5 == 'Yes')
            {
                $yes += 1;
            }
            elseif($anesthesia->doc_vital_5 == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($anesthesia->doc_name_time == 'Yes')
            {
                $yes += 1;
            }
            elseif($anesthesia->doc_name_time == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($anesthesia->doc_IV_amount == 'Yes')
            {
                $yes += 1;
            }
            elseif($anesthesia->doc_IV_amount == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($anesthesia->doc_blood_amount == 'Yes')
            {
                $yes += 1;
            }
            elseif($anesthesia->doc_blood_amount == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($anesthesia->doc_blood_loss == 'Yes')
            {
                $yes += 1;
            }
            elseif($anesthesia->doc_blood_loss == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($anesthesia->urine_output == 'Yes')
            {
                $yes += 1;
            }
            elseif($anesthesia->urine_output == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($anesthesia->doc_PACU_transfer == 'Yes')
            {
                $yes += 1;
            }
            elseif($anesthesia->doc_PACU_transfer == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($anesthesia->doc_normothermia == 'Yes')
            {
                $yes += 1;
            }
            elseif($anesthesia->doc_normothermia == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($anesthesia->doc_PACU_ICU == 'Yes')
            {
                $yes += 1;
            }
            elseif($anesthesia->doc_PACU_ICU == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($anesthesia->post_anesthesia == 'Yes')
            {
                $yes += 1;
            }
            elseif($anesthesia->post_anesthesia == 'No')
            {
                $no += 1;
            }
            else
            {
                $na += 1;
            }

            if($anesthesia->narcan == 'No')
            {
                $yes += 1;
            }
            else
            {
                $no += 1;
            }

            if($anesthesia->reversal_agent == 'No')
            {
                $yes += 1;
            }
            else
            {
                $no += 1;
            }

            if($anesthesia->postop_nausea == 'No')
            {
                $yes += 1;
            }
            else
            {
                $no += 1;
            }

            if($anesthesia->cardiac_arrest == 'No')
            {
                $yes += 1;
            }
            else
            {
                $no += 1;
            }

            if($anesthesia->anaphylaxis == 'No')
            {
                $yes += 1;
            }
            else
            {
                $no += 1;
            }

            if($anesthesia->malignant_hypo == 'No')
            {
                $yes += 1;
            }
            else
            {
                $no += 1;
            }

            if($anesthesia->incorrect_site == 'No')
            {
                $yes += 1;
            }
            else
            {
                $no += 1;
            }

            if($anesthesia->med_error == 'No')
            {
                $yes += 1;
            }
            else
            {
                $no += 1;
            }

            if($anesthesia->intraoperative == 'No')
            {
                $yes += 1;
            }
            else
            {
                $no += 1;
            }

            if($anesthesia->difficult_air == 'No')
            {
                $yes += 1;
            }
            else
            {
                $no += 1;
            }

            if($anesthesia->unplanned_reintubation == 'No')
            {
                $yes += 1;
            }
            else
            {
                $no += 1;
            }

            if($anesthesia->dental == 'No')
            {
                $yes += 1;
            }
            else
            {
                $no += 1;
            }

            if($anesthesia->spinal == 'No')
            {
                $yes += 1;
            }
            else
            {
                $no += 1;
            }

            if($anesthesia->flash_edema == 'No')
            {
                $yes += 1;
            }
            else
            {
                $no += 1;
            }

        $request->offsetSet('comp_yes', $yes);
        $request->offsetSet('comp_no', $no);
        $request->offsetSet('comp_NA', $na);

        // dd($request->all());

        $anesthesia->update($request->all());


        alert()->success('Success', 'Form Submitted!');
        return redirect()->back();
    }
}
