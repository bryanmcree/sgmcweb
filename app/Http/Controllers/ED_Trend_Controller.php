<?php

namespace App\Http\Controllers;

use App\ED_Trend;
use Illuminate\Http\Request;
use App\History;
use Carbon\Carbon;
use App\Http\Requests;

class ED_Trend_Controller extends Controller
{
    public function index(){

        $chart_lables = ED_Trend::select('rpt_date')
            ->where('rpt_date', '<=', Carbon::today())
            ->where('rpt_date', '>=', Carbon::now()->subDays(30))
            ->distinct()
            ->orderby('rpt_date')
            ->get();

        $ed_adv = ED_Trend::select('AVG_Min')
            ->where('rpt_date', '<=', Carbon::today())
            ->where('rpt_date', '>=', Carbon::now()->subDays(30))
            ->orderby('rpt_date')
            ->get();
        //dd($ed_adv);

        $ed_running_adv = ED_Trend::avg('AVG_Min');
            //->orderby('rpt_date')
            //->get();
        //dd($ed_monthly_adv);

        $ed_monthly_adv = ED_Trend::where('rpt_date', '>=', Carbon::now()->startOfMonth())
            ->avg('AVG_Min');
        //dd($ed_monthly_adv);

        $ed_yearly_adv = ED_Trend::where('rpt_date', '>=', Carbon::now()->startOfYear())
            ->avg('AVG_Min');
        //dd($ed_monthly_adv);

        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'Viewed ED';
        $history->userid = \Auth::user()->username;
        $history->search_string = 'N/A';
        $history->user_ip = \Request::ip();
        $history->save();

        return View('ed.index', compact('ed_adv','chart_lables','ed_running_adv','ed_monthly_adv',
            'ed_yearly_adv'));
    }
}
