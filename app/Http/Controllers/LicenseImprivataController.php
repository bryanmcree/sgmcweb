<?php

namespace App\Http\Controllers;

use App\SysaidLicenses;
use Illuminate\Http\Request;

use App\Http\Requests;

class LicenseImprivataController extends Controller
{
    public function index(){
        $imprivata = SysaidLicenses::where('license','=','Imprivata')
            ->orderby('created_at','desc')
            ->get();

        $imprivata_used = SysaidLicenses::where('license','=','Imprivata')
            ->orderby('created_at')
            ->get();

        //dd($imprivata_used);

        return view('sysaid.licenses.imprivata', compact('imprivata','imprivata_used'));
    }

    public function add_imprivata(Requests\SystemListRequest $request){
        //Add new license entry
        //$request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();
        SysaidLicenses::create($input);
        alert()->success('License Updated!', 'Success!');
        return redirect('/sysaid');

    }
}
