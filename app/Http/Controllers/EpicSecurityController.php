<?php

namespace App\Http\Controllers;

use App\EpicSecurity;
use App\History;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Requests;

class EpicSecurityController extends Controller
{
    public function index(){


        return View('epic.index');


    }

    public function download(){

        //Because this uses a view with linked servers.
        $handle = \DB::connection()->getPdo();
        $handle->exec('SET QUOTED_IDENTIFIER ON');
        $handle->exec('SET ANSI_WARNINGS ON');
        $handle->exec('SET ANSI_PADDING ON');
        $handle->exec('SET ANSI_NULLS ON');
        $handle->exec('SET CONCAT_NULL_YIELDS_NULL ON');

        $emps = EpicSecurity::get()->toArray();


        $history = new History();
        $history->id = \Uuid::generate(4);
        $history->action = 'Download Epic Security Report';
        $history->userid = \Auth::user()->username;
        $history->user_ip = \Request::ip();
        $history->search_string = 'User downloaded the epic security report.';
        $history->save();


        Excel::create('Marks_Report '.date('m-d-Y_hia'), function($excel) use($emps) {
            $excel->sheet('New sheet', function($sheet) use($emps) {
                $sheet->fromArray($emps);
            })->download('csv');
        });
    }
}
