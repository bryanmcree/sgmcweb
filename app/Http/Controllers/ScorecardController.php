<?php

namespace App\Http\Controllers;

use App\CostCenters;
use App\History;
use App\Scorecard;
use App\ScorecardComments;
use App\ScorecardCostCenters;
use App\ScorecardCustomQuestions;
use App\ScorecardDefaultQuestions;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use App\ScorecardKpi;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Webpatser\Uuid\Uuid;
use PDF;

class ScorecardController extends Controller
{
    public function index(){

        $monthly_kpi = Scorecard::where('created_at', '>=', Carbon::now()->startOfMonth()->subMonth(1))
            ->first();

        //dd($monthly_kpi);

        return view('scorecard.index', compact('monthly_kpi','evals'));
    }

    public function dashboard(){

        $myCC = ScorecardCostCenters::where('director',Auth::user()->name)
            ->orderby('cost_center')
            ->get();

        $questions = ScorecardCustomQuestions::select('cost_center')
            ->whereNull('auto_generate')
            ->distinct()
            ->get();


        $cc_count = ScorecardCostCenters::wherenotin('cost_center',$questions)
            ->orderby('cost_center')
            ->get();


        $cost_centers = ScorecardCostCenters::
            orderby('cost_center')
            ->get();

        $default = ScorecardDefaultQuestions::get();

        //dd($cost_centers);

        return view('scorecard.dashboard', compact('cost_centers','default','cc_count','myCC'));

    }

    public function view(Requests\SystemListRequest $request){

        $cost_center = $request->cost_center;

        $costcenterQuestions = ScorecardCostCenters::where('cost_center','=', $cost_center)
            ->first();

        $custommetrics = ScorecardCustomQuestions::where('cost_center','=', $cost_center)
            ->get();

        $kpi_values = ScorecardKpi::with('questionCount')->where('cost_center','=', $cost_center)
            ->where('report_date', '>=', Carbon::now()->startOfMonth())
            //->groupby('report_month','report_year')
            ->get();
        //dd($kpi_values);

        $kpi_question_total = ScorecardKpi::get()->groupBy('question_id');

        return view('scorecard.view', compact('costcenterQuestions','custommetrics','kpi_values',
            'kpi_question_total'));
    }

    public function viewMetric($id){

        $cost_center = $id;
        $costcenterQuestions = ScorecardCostCenters::where('cost_center','=', $cost_center)
            ->first();
        $custommetrics = ScorecardCustomQuestions::where('cost_center','=', $cost_center)
            ->orderby('auto_generate')
            ->get();

        $custommetrics2 = ScorecardCustomQuestions::where('cost_center','=', $cost_center)
            ->whereNull('auto_generate')
            ->get();

        $myCC = ScorecardCostCenters::where('director',Auth::user()->name)
            ->orderby('cost_center')
            ->get();

        $comments = ScorecardComments::where('cost_center','=', $cost_center)
            ->orderby('created_at','DESC')
            ->get();

        $comments_user = ScorecardComments::where('show_user','Yes')
            ->where('cost_center','=', $cost_center)
            ->wherenull('show_user_confirmed')
            ->get();

        $cost_centers = ScorecardCostCenters::
        orderby('cost_center')
            ->get();

        //dd($comments_user);

        $kpi_values = ScorecardKpi::with('questionCount')->where('cost_center','=', $cost_center)
            ->orderby('auto_generate')
            ->get();
        //dd($kpi_values);

        //Lets get the month and year for all the data.  This is what you will loop through.
        $kpi_dates = ScorecardKpi::selectRaw("report_month, report_year, COUNT(id) as totalanswers")
            //->with('kpi_answers','kpi_answers2')
            ->where('cost_center','=', $cost_center)
            ->groupby('report_month','report_year')
            ->orderby('report_year','DESC')
            ->orderby('report_month','DESC')

            ->get();
        //dd($kpi_dates);

        $kpi_question_total = ScorecardKpi::get()->groupBy('question_id');

        return view('scorecard.view', compact('costcenterQuestions','custommetrics','kpi_values',
            'kpi_question_total','kpi_dates','custommetrics2','myCC','comments','comments_user','cost_centers'));
    }

    public function addMetric(Requests\SystemListRequest $request){

        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();
        ScorecardCustomQuestions::create($input);

        $history = new History();
        $history->id = Uuid::generate(4);
        $history->action = 'Custom KPI';
        $history->userid = \Auth::user()->username;
        $history->search_string = 'none';
        $history->user_ip = \Request::ip();
        $history->save();

        alert()->success('Metric has been added!', 'Success!');

        return redirect('/scorecard/dashboard/view/'. $request->cost_center);
    }

    public function editMetric(){
        $metric = ScorecardCustomQuestions::find( Input::get('id') );
        $metric->update(Input::except(['id']));
        alert()->success('Metric has been Updated!', 'Success!');
        return redirect('/scorecard/dashboard/view/' . $metric->cost_center);

    }

    public function deleteMetric($id)
    {
        $metric = ScorecardCustomQuestions::find( $id );
        $metric->delete();
        $cost_center = Input::get('cost_center');
        return redirect('/scorecard/dashboard/view/' . $cost_center);
    }

    public function viewcustom(){
        //view custom metrics admin
        $metrics = ScorecardCustomQuestions::orderby('approved')
            ->orderby('cost_center')
            ->get();

        //dd($metrics);
        return view('scorecard.admin.viewcustom', compact('metrics','default'));

    }

    public function viewcustomdetail($id){
        //View custom detail view for custom metric
        $metric_detail = ScorecardCustomQuestions::find($id);
        $metrics = ScorecardCustomQuestions::orderby('approved')
            ->orderby('cost_center')
            ->get();
        //dd($metric);
        return view('scorecard.admin.viewdetail', compact('metric_detail','metrics'));
    }

    public function approval(){
        //Get data from approval form and update record.
        $metric = ScorecardCustomQuestions::find( Input::get('id') );
        $metric->update(Input::only(['approved','division','new_source','comments']));
        alert()->success('Metric has been Updated!', 'Success!');
        return redirect('/scorecard/admin/view/' . Input::get('id'));
    }

    public function enter_values(Requests\SystemListRequest $request){

        $request->offsetSet('id', Uuid::generate(4));
        $request->offsetSet('report_month', Carbon::parse($request->report_date)->format('m'));
        $request->offsetSet('report_year', Carbon::parse($request->report_date)->format('Y'));
        $input = $request->except('response');
        //ScorecardCustomQuestions::create($input);
        //dd($input);

        //First lets make sure they have not already entered values for this month/year.
        $kpi_check = ScorecardKpi::where('report_month',$request->report_month)
            ->where('report_year',$request->report_year)
            ->where('cost_center', $request->cost_center)
            ->whereNull('auto_generate')
            ->get();
        //dd($kpi_check);

        if(!$kpi_check->isEmpty()){
            alert()->error('You have already entered values for '. date("F", mktime(0, 0, 0, $request->report_month, 1)) .','. $request->report_year .'.
            You can edit these values below. ', 'OOOps!')->persistent('Close');
            return redirect('/scorecard/dashboard/view/'. $request->cost_center);
            //echo "noope you can't enter this";
        }ELSE {


            //Submit answers
            $arr = Input::get('response');
            //$main_id = $request->id;


            foreach ($arr as $key => $value) {
                $newAnswer = new ScorecardKpi();
                if (!is_array($value)) {
                    $newValue = $value['answer'];
                } else {
                    $newValue = json_encode($value['answer']);
                }
                //dd($newValue);


                $newAnswer->id = Uuid::generate(4);
                $newAnswer->question_id = $key;
                $newAnswer->cost_center = $request->cost_center;
                $newAnswer->report_value = $request->report_value;
                $newAnswer->report_month = $request->report_month;
                $newAnswer->report_year = $request->report_year;
                $newAnswer->report_date = $request->report_date;
                $newAnswer->report_value = str_replace('"', '', $newValue);

                $newAnswer->save();

                $answerArray[] = $newAnswer;
            };

            $history = new History();
            $history->id = Uuid::generate(4);
            $history->action = 'Entered KPI Values';
            $history->userid = \Auth::user()->username;
            $history->search_string = 'none';
            $history->user_ip = \Request::ip();
            $history->save();

            alert()->success('Values have been stored!', 'Success!');

            return redirect('/scorecard/dashboard/view/' . $request->cost_center);
        }
    }

    public function chart($id){


        $kpi_data = ScorecardKpi::where('question_id','=', $id)
            //->where('report_date', '>=', Carbon::now()->startOfMonth())
            //->groupby('question_id')
            ->orderby('report_date')
            ->first();


        return view('scorecard.chart', compact('kpi_chart','kpi_data'));
    }


    public function chart_json($id){
        $kpi_values = ScorecardKpi::selectRaw('report_date,report_value')
            ->where('question_id','=', $id)
            ->orderby('report_month')
            ->get();

        return $kpi_values;
    }

    public function report1(){

        $cost_centers = ScorecardCostCenters::with('customQuestions')
            //->take(150)
            ->get();

        return view('scorecard.reports.report1', compact('cost_centers','custommetrics','kpi_values',
           'kpi_question_total'));

    }

    public function report2(){

        $kpi_values = ScorecardKpi::with('questionCount')
            //->where('cost_center','=', $cost_center)
            ->orderby('report_date')
            ->get();

        $kpi_cost_center = ScorecardKpi::select('cost_center')
            ->distinct()
            //->with('customQuestions')
            //->where('cost_center','=', $cost_center)
            //->groupby('report_month','report_year', 'report_date')
            ->orderby('cost_center')
            ->get();

        //dd($kpi_cost_center);

        return view('scorecard.reports.report2', compact('kpi_cost_center',
            'kpi_values'));

    }

    public function report3($id){

        $cc = CostCenters::where('cost_center', $id)
            ->first();
        //dd($cc);

        $cost_centers = ScorecardCustomQuestions::where('cost_center', $id)
            ->with('costcenters')
            ->whereNull('auto_generate')
            ->get();


        return view('scorecard.reports.report3', compact('cost_centers','custommetrics','kpi_values',
            'cc'));

    }

    public function deleteValue($id)
    {
        $metric = ScorecardKpi::find( $id );
        $metric->delete();
        $cost_center = Input::get('cost_center');
        return redirect('/scorecard/dashboard/view/' . $cost_center);
    }

    public function deleteMonthvalue($id)
    {
        $cost_center = Input::get('cost_center');
        $report_year = Input::get('report_year');

        //dd($report_year);

        $metric = ScorecardKpi::where('report_month','=', $id)
            ->whereNull('auto_generate')
            ->where('cost_center','=', $cost_center)
            ->where('report_year','=', $report_year);
        //dd($metric);

        $metric->delete();
        return redirect('/scorecard/dashboard/view/' . $cost_center);
    }

    public function manager($id){

        $cost_center = ScorecardCostCenters::where('cost_center',$id)->first();

        $cost_center->director = Auth::user()->name;
        $cost_center->save();

        $history = new History();
        $history->id = Uuid::generate(4);
        $history->action = 'Changed Director/Manager KPI';
        $history->userid = \Auth::user()->username;
        $history->search_string = Auth::user()->name;
        $history->user_ip = \Request::ip();
        $history->save();


        alert()->success('Director / Manager updated!', 'Success!');
        return redirect('/scorecard/dashboard/view/' . $id);

    }

    public function editmetricvalue(){

        $metric = ScorecardKpi::find( Input::get('id') );

        $metric->offsetSet('report_month', Carbon::parse(Input::get('report_date'))->format('m'));
        $metric->offsetSet('report_year', Carbon::parse(Input::get('report_date'))->format('Y'));

        $metric->update(Input::except(['id']));
        alert()->success('Metric value has been updated!', 'Success!');
        return redirect('/scorecard/dashboard/view/' . $metric->cost_center);
    }


    public function report4($id){

        $cc = ScorecardCostCenters::where('cost_center', $id)
            ->first();
        //dd($cc);


        $kpi_values = ScorecardKpi::with('questionCount')
            //->where('cost_center','=', $cost_center)
            ->orderby('report_date')
            ->get();

        //dd($kpi_values);


        $cost_centers = ScorecardCustomQuestions::where('cost_center', $id)
            ->with('costcenters')
            //->whereNull('auto_generate')
            //->take(1)
            ->get();


        return view('scorecard.reports.report4', compact('cost_centers','custommetrics','kpi_values',
            'cc'));

    }


    public function report5($id){

        $kpi_values = ScorecardKpi::with('questionCount')
            ->where('cost_center','=', $id)
            ->orderby('report_date')
            ->get();

        $kpi_cost_center = ScorecardKpi::select('cost_center')
            ->distinct()
            //->with('customQuestions')
            ->where('cost_center','=', $id)
            //->groupby('report_month','report_year', 'report_date')
            //->orderby('report_date','DESC')
            ->get();

        //dd($kpi_cost_center);

        return view('scorecard.reports.report2', compact('kpi_cost_center',
            'kpi_values'));

    }

    public function reportmenu(){
        return view('scorecard.report', compact('cost_centers','custommetrics','kpi_values',
            'cc'));
    }

    public function manager_clear($id){

        $cost_center = ScorecardCostCenters::where('cost_center',$id)->first();

        $cost_center->director = null;
        $cost_center->save();

        $history = new History();
        $history->id = Uuid::generate(4);
        $history->action = 'Cleared Director/Manager KPI';
        $history->userid = \Auth::user()->username;
        $history->search_string = Auth::user()->name;
        $history->user_ip = \Request::ip();
        $history->save();


        alert()->success('Director / Manager cleared!', 'Success!');
        return redirect('/scorecard/dashboard/view/' . $id);

    }

    public function report6(){

        $questions = ScorecardCustomQuestions::select('cost_center')
            ->whereNull('auto_generate')
            ->distinct()
            ->get();


        $cost_centers = ScorecardCostCenters::wherenotin('cost_center',$questions)
            ->wherenull('deactivated')
            ->orderby('cost_center')
            ->get();

        //dd($cost_centers);

        return view('scorecard.reports.report6', compact('cost_centers','custommetrics','kpi_values',
            'cc'));

    }

    public function employeesearch()
    {

        $data = User::select("name")
            ->where('name', 'LIKE','%' . Input::get('query') . '%')
            ->whereIn('emp_status',['Active','Contract','Leave of Absence'])
            ->wherenull('deleted_at')
            ->orderby('name')
            ->get();
        //var_dump($data);

        foreach ($data as $key => $row) {
            $results['suggestions'][] = array('value' => $row['name'],'data' => $row['name']);
        }

        return \Response::json($results);
    }

    public function manager_admin(){

        $cost_center = ScorecardCostCenters::where('cost_center',Input::get('cost_center'))->first();

        $cost_center->director = Input::get('director');
        $cost_center->save();

        $history = new History();
        $history->id = Uuid::generate(4);
        $history->action = 'Changed Director/Manager KPI';
        $history->userid = \Auth::user()->username;
        $history->search_string = Input::get('director');
        $history->user_ip = \Request::ip();
        $history->save();


        alert()->success('Director / Manager updated!', 'Success!');
        return redirect('/scorecard/dashboard/view/' . Input::get('cost_center'));

    }

    public function administrator(){

        $cost_center = ScorecardCostCenters::where('cost_center',Input::get('cost_center'))->first();

        $cost_center->administrator = Input::get('administrator');
        $cost_center->save();

        $history = new History();
        $history->id = Uuid::generate(4);
        $history->action = 'Changed Administrator KPI';
        $history->userid = \Auth::user()->username;
        $history->search_string = Input::get('administrator');
        $history->user_ip = \Request::ip();
        $history->save();


        alert()->success('Administrator updated!', 'Success!');
        return redirect('/scorecard/dashboard/view/' . Input::get('cost_center'));

    }

    //Email test

    public function email_test(Requests\SystemListRequest $request){


        IF(Input::get('reminder_set') != 'Yes'){

            $request->offsetSet('id', Uuid::generate(4));
            $input = $request->all();
            ScorecardComments::create($input);
            return redirect('/scorecard/dashboard/view/'. $request->cost_center);

        }
        ELSE
        {
            $request->offsetSet('id', Uuid::generate(4));
            $input = $request->all();

           // $start_date = Input::get('start_date'). ' 00:00:00';
          // $end_date = Input::get('end_date'). ' 23:59:59';

            $formated_reminder = $request->reminder_date . ' ' .$request->reminder_time .':00';
            $startTime = Carbon::createFromFormat('Y-m-d H:i:s', $formated_reminder)->toDateTimeString();
            $endTime = Carbon::createFromFormat('Y-m-d H:i:s', $formated_reminder)->addMinute($request->meeting_time)->toDateTimeString();
            //"2017-12-19 16:43:31"
            //dd(Carbon::now()->toDateTimeString());
            //dd($endTime);
            //dd($formated_reminder);
            //dd($endTime);

            //$date = Carbon::now()->toDateTimeString();

            //dd($date);
            $date = $startTime;
            $content = 'This is a test';
            $subject = 'DOR Reminder';

            $dateTimeArr = explode(' ', $date);
            $newDate = $dateTimeArr[0];
            $dateArr = explode('-', $newDate);
            $newDate = $dateArr[0] . $dateArr[1] . $dateArr[2] . '-' . $dateTimeArr[1];
            $Startdate = substr($newDate, 0, -9);

            //dd($date);

            $startTime = str_replace(":", "", substr($newDate, 9, -3));


            $dateTimeArr = explode(' ', $endTime);
            $newDate = $dateTimeArr[0];
            $dateArr = explode('-', $newDate);
            $newDate = $dateArr[0] . $dateArr[1] . $dateArr[2] . '-' . $dateTimeArr[1];
            $Startdate = substr($newDate, 0, -9);
            $EndTime = str_replace(":", "", substr($newDate, 9, -3));

            //$endTime = '1530';
             //dd($EndTime);

            $strip = str_replace("\r", " ", strip_tags($request->comments));
            //$desc1 = str_replace("&#10;", "\n", $request->comments);
             $desc = str_replace("\n", "", $strip);
             //$desc = $request->comments;

             //dd($desc);

            //dd($desc);
            //dd("DTSTART;TZID=\"Eastern Standard Time\":" . $Startdate . "T" . $startTime . "00Z");

            // ICS
            $mail[0] = "BEGIN:VCALENDAR";
            $mail[1] = "PRODID:-//Microsoft Corporation//Outlook 16.0 MIMEDIR//EN";
            $mail[2] = "VERSION:2.0";
            $mail[3] = "METHOD:PUBLISH";
            $mail[4] = "X-MS-OLK-FORCEINSPECTOROPEN:TRUE";
            $mail[5] = "BEGIN:VTIMEZONE"; // Start VTimezone
            $mail[6] = "TZID:Eastern Standard Time";
            $mail[7] = "BEGIN:STANDARD";  //BEGIN Standard
            $mail[8] = "DTSTART:" . $Startdate . "T" . $startTime . "00Z";
            $mail[9] = "RRULE:FREQ=YEARLY;BYDAY=1SU;BYMONTH=11";
            $mail[10] = "TZOFFSETFROM:-0400";
            $mail[11] = "TZOFFSETTO:-0000";
            $mail[12] = "END:STANDARD";  //END Standard
            $mail[13] = "BEGIN:DAYLIGHT";  //Begin Daylight
            $mail[14] = "DTSTART:" . $Startdate . "T" . $startTime . "00Z";
            $mail[15] = "RRULE:FREQ=YEARLY;BYDAY=2SU;BYMONTH=3";
            $mail[16] = "TZOFFSETFROM:-0000";
            $mail[17] = "TZOFFSETTO:-0400";
            $mail[18] = "END:DAYLIGHT";  //END daylight
            $mail[19] = "END:VTIMEZONE"; //END VTIMEZone
            $mail[20] = "BEGIN:VEVENT";  //Start the event
            $mail[21] = "ATTENDEE;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;RSVP=TRUE;ORGANIZER;CN=McRee, Bryan E., Test:MAILTO:bryan.mcree@sgmc.org";  //The person who made the reqeust
            $mail[22] = "CLASS:PUBLIC";
            $mail[23] = "DESCRIPTION:". $desc;
            $mail[24] = "DTEND;TZID=\"Eastern Standard Time\":" . $Startdate . "T" . $EndTime . "00Z";
            $mail[25] = "DTSTART;TZID=\"Eastern Standard Time\":" . $Startdate . "T" . $startTime . "00Z";
            $mail[26] = "DTSTAMP:" . gmdate('Ymd') . 'T' . gmdate('His') . "Z";
            $mail[27] = "LOCATION: Not Specified";
            $mail[28] = "ORGANIZER;CN=\"No, Reply\":mailto:no.reply@SGMC.ORG";
            $mail[29] = "PRIORITY:1";
            $mail[30] = "SEQUENCE:0";
            $mail[31] = "SUMMARY;LANGUAGE=en-us:DOR Reminder";
            $mail[32] = "TRANSP:OPAQUE";
            $mail[33] = "BEGIN:VALARM";
            $mail[34] = "TRIGGER:-PT15M";
            $mail[35] = "ACTION:DISPLAY";
            $mail[37] = "END:VALARM";
            $mail[38] = "END:VEVENT";
            $mail[39] = "END:VCALENDAR";
            $mail[36] = "X-MICROSOFT-CDO-BUSYSTATUS:FREE";
            //$mail[37] = "X-MICROSOFT-CDO-INTENDEDSTATUS:FREE";  //Meeting Organizer
            // $mail[38] = "";
            // $mail[39] = "";



            //set correct content-type-header
            $filename = $subject . '.ics';
            $mail = implode("\r\n", $mail);

            header("text/calendar");

            Storage::put('public/events/' . $filename , $mail);

            //Lets an email to the requester notifying them of the approval.
            $data = array('name'=>"Virat Gandhi");

            Mail::raw('See the attached reminder and add it to your calendar', function($message) {
                $message->to(Auth::user()->mail, 'DOR Reminder')->subject
                ('See the attached reminder and add it to your calendar.');
                $message->from('no.reply@sgmc.org','Web.SGMC.org');
                $message->attach(storage_path('app/public/public/events/DOR Reminder.ics'));

            });
            //echo "HTML Email Sent. Check your inbox.";

            ScorecardComments::create($input);
            return redirect('/scorecard/dashboard/view/'. $request->cost_center);

        }

    }

    public function got_it($id){


        $reminder = ScorecardComments::find($id);

        $reminder->show_user_confirmed = Carbon::now();
        $reminder->confirmed_by = Auth::user()->name;
        $reminder->save();

        //alert()->success('Metric has been Updated!', 'Success!');
        return redirect('/scorecard/dashboard/view/' . $reminder->cost_center);

    }
}
