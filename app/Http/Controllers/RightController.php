<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Right;
use App\Permission;
use App\Role;
use Illuminate\Support\Facades\Input;

class RightController extends Controller
{
    public function index($id)
    {
        if(Input::has('permission_id')) {
        $permissions = Permission::orderBy('name')->pluck('name', 'id');
        $permissions[''] = 'Select a Permission';
        $roleName = Role::where('id', $id)->first();
        $rights = Right::with('permissions')->where('permission_id', $id)->get();
        $input = Input::all();
        Right::create($input);
        return redirect('/right/'.$id)->with('msg', 'Permission has been added to Role.');
        }Else{
        $permissions = Permission::orderBy('name')->pluck('name', 'id');
        $permissions[''] = 'Select a Permission';
        $roleName = Role::where('id', $id)->first();
        $rights = Right::where('role_id', $id)->get();
        return View('right.index', compact('rights', 'permissions', 'roleName', 'id'));
        }
    }

    public function create()
    {
        return view('role.create');
    }

    public function store(Requests\RightRequest $request)
    {
        dd($request);
        $input = $request->all();
        Right::create($input);
        return redirect('/right/'. $request->permission_id)->with('msg', 'Permission has been added to Role.');
    }

    public function delete($id)
    {
        $role_id = Input::get('role_id');
        $rights = Right::where('permission_id', $id)->where('role_id', $role_id)->delete();
        return redirect('/right/'.$role_id)->with('msg', 'Permission has been deleted.');
    }
}
