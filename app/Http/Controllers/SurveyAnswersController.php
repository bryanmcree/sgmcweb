<?php

namespace App\Http\Controllers;

use App\History;
use App\Survey;
use App\SurveyAnswers;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Mail;
use Webpatser\Uuid\Uuid;

class SurveyAnswersController extends Controller
{
    public function submit_survey(Requests\SurveyRequest $request){

        //dd($request->all());

        //Lets find this survey and add one to the count.
        $count = Survey::where('id','=', $request->survey_id)->first();
        $count->taken_count = $count->taken_count + 1;
        $count->save();

        //Now lets generate the incident id
        $last_incident_id = SurveyAnswers::select('incident_id')
            ->where('survey_id','=', $request->survey_id)
            ->orderby('incident_id','desc')
            ->first();


        //See if the person already submitted one and is not permitted another.

        //dd($count->limit_to_one);
        if($count->limit_to_one == 1){
            $user_check = SurveyAnswers::where('survey_id', '=', $request->survey_id)
                ->where('submitted_by','=', \Auth::user()->employee_number)
                ->first();
            if(!is_null($user_check)){
                return redirect('/survey/thankyou/'. $request->survey_id)->with('alert', 'Results saved!');
            }
        }


        //lets set the options of the survey for down the road.
        $survey = Survey::where('id','=', $request->survey_id)->first();

        IF($last_incident_id == NULL){
            //First answer
            $new_incident_id = 1;
        }else{
            $new_incident_id = $last_incident_id->incident_id + 1;
        }

        //dd($new_incident_id);


        //Send email if the survey creator wants to
        //Lets send the manager an email for approval.
        if($survey->email_completed ==1){

            //$mailData = $request->toArray();
            $mailData = Survey::with('createdBy')
                ->where('id','=',$request->survey_id)
                ->get()
                ->first()
                ->toArray();

            $completedBy = User::selectraw('name as completed_by')
                ->where('username','=', \Auth::user()->username)
                ->get()
                ->first()
                ->toArray();
            //dd($completedBy);

            Mail::send('survey.emails.surveyComplete', $mailData, function ($m) use ($mailData) {
                $m->from('Surveys@sgmc.org', 'Web.SGMC.org');
                $m->to($mailData['created_by']['mail']);
                $m->subject('Survey Completed');
            });
        }



        // remove the token
        $arr = $request->except('_token','account_id','user_id','survey_id','submitted_by','answer_string','survey_type');
        //$instance_id = Uuid::generate(4);
        //dd($arr);
        foreach ($arr as $key => $value) {
            $newAnswer = new SurveyAnswers();
            if (! is_array( $value )) {
                $newValue = $value['answer'];
                $question_type = $value['question_type'];
            } else {
                $newValue = json_encode($value['answer']);
                $question_type = json_encode($value['question_type']);
            }
            //dd($newValue);
            //$newAnswer->answer = str_replace('"','',$newValue);
            $newAnswer->survey_id = $request->survey_id;
            $newAnswer->question_id = $key;
            $newAnswer->incident_id = $new_incident_id;

            if(is_numeric(str_replace('"','',$newValue))){
                $newAnswer->answer = str_replace('"','',$newValue);
            }else{
                $newAnswer->answer_string = str_replace('"','',$newValue);
            }

            $newAnswer->submitted_by = $request->submitted_by;
            $newAnswer->question_type = str_replace('"','',str_replace('"','',$question_type));
            $newAnswer->id = Uuid::generate(4);
            $newAnswer->save();

            $answerArray[] = $newAnswer;
        };

        //Log History
        //History
        $history = new History();
        $history->id = \Uuid::generate(4);
        $history->action = 'Survey Completed';
        $history->userid = \Auth::user()->username;
        $history->search_string = $survey->survey_name;
        $history->user_ip = \Request::ip();
        $history->save();


        if($request->survey_type == 'user'){
            if($survey->survey_redirect == 1){
                alert()->success('Survey has been submitted!', 'Survey Submitted');
                return redirect('/survey/'. $request->survey_id)->with('alert', 'Results saved!');
            }else{
                alert()->success('Survey has been submitted!', 'Survey Submitted');
                return redirect('/survey/thankyou')->with('alert', 'Results saved!');
            }
        }else{
            alert()->success('Survey has been submitted!', 'Survey Submitted');
            return redirect('/survey/details/'. $request->survey_id)->with('alert', 'Results saved!');
        }

    }
}
