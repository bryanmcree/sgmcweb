<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;

use App\Http\Requests;
use App\SecurityBadges;
use App\SecurityCitation;
use App\SecurityActivityList;
use App\SecurityLocationList;
use App\SecurityDispatch;
use App\SecurityRounds;
use App\SecurityViolation;
use App\SecurityIncident;
use App\SecurityIncidentList;
use App\User;
use App\History;
use Auth;
use Carbon;
use DB;

class SGMCSecurityBadgeController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        
        return view('sgmcsecurity.badge', compact('user'));
    }

    public function store(Request $request)
    {
        $request->offsetSet('id', Uuid::generate(4));

        $badges = $request->all();

        SecurityBadges::create($badges);

        alert()->success('Badge Total Submitted!', 'Success!');
        return redirect()->back();
    }

    public function update(Request $request)
    {
        $badge = SecurityBadges::find($request->id);

        $badge->id = $request->id;
        $badge->date = $request->date;
        $badge->name = $request->name;
        $badge->total = $request->total;

        $badge->save();

        alert()->success('Badge Info Updated!', 'Success!');
        return redirect('/security');
    }

    public function destroy($id)
    {
        $badge = SecurityBadges::find($id);

        $badge->delete();
    }
}
