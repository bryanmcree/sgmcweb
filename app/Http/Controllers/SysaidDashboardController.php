<?php

namespace App\Http\Controllers;

use App\History;
use App\SysaidDashboard;
use App\SysaidLicenses;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use App\Http\Requests;

class SysaidDashboardController extends Controller
{
    public function index()
    {
        //Open Tickets Total

        $total_open = SysaidDashboard::wherenotin('ticket_status',['Ticket - CLOSED','Change - Completed','Merge Closed','Change - Approved and Implement','Change - More Info Required'])
            ->wherenotnull('ticket_status')

        //where('ticket_status','<>','Ticket - CLOSED')
            ->get();

        //dd($total_open->created_at->first());

        $last_updated = SysaidDashboard::first();

        $is_open = SysaidDashboard::selectRaw('count(*) as total')
            //->where('request_time', '>=', Carbon::now()->subDays(30))
            ->where('ticket_epic', '<>', 1)
            ->wherenotin('ticket_status',['Ticket - CLOSED','Change - Completed','Merge Closed','Change - Approved and Implement','Change - More Info Required'])
            ->wherenotnull('ticket_status')
            ->first();
        //dd($is_open);


        $is_closed = SysaidDashboard::selectRaw('count(*) as total')
            ->where('request_time', '>=', Carbon::now()->subDays(30))
            ->where('ticket_is', '=', 1)
            ->wherein('ticket_status',['Ticket - CLOSED','Change - Completed','Merge Closed','Change - Approved and Implement','Change - More Info Required'])
            ->first();

        $epic_open = SysaidDashboard::selectRaw('count(*) as total')
            //->where('request_time', '>=', Carbon::now()->subDays(30))
            ->where('ticket_epic', '=', 1)
            ->wherenotin('ticket_status',['Ticket - CLOSED','Change - Completed','Merge Closed','Change - Approved and Implement','Change - More Info Required'])
            ->first();
        $epic_closed = SysaidDashboard::selectRaw('count(*) as total')
            ->where('request_time', '>=', Carbon::now()->subDays(30))
            ->where('ticket_epic', '=', 1)
            ->wherein('ticket_status',['Ticket - CLOSED','Change - Completed','Merge Closed','Change - Approved and Implement','Change - More Info Required'])
            ->first();

        //dd($epic->total);


        $category_epic = SysaidDashboard::selectRaw('count(*) as total')
            ->where('request_time', '>=', Carbon::now()->subDays(30))
            ->wherenotin('ticket_status',['Ticket - CLOSED'])
            ->where('category','=','System Administration Related Request')
            ->where('ticket_epic','=',1)
            ->first();

        $category_is = SysaidDashboard::selectRaw('count(*) as total')
            ->where('request_time', '>=', Carbon::now()->subDays(30))
            ->wherenotin('ticket_status',['Ticket - CLOSED'])
            ->where('category','=','System Administration Related Request')
            ->where('ticket_epic','=',0)
            ->first();


        //SLA violation breakout via epic/is
        $epic_30 = SysaidDashboard::selectRaw('esclation, count(*) as total')
            //->where('request_time', '>=', Carbon::now()->subDays(30))
            ->wherenotin('ticket_status',['Ticket - CLOSED','Change - Completed'])
            //->wherenotin('esclation',['0'])
            ->groupby('esclation')
            ->orderby('esclation','DESC')
            //->take(5)
            ->get();

        //dd($category_is);

        //History feature removed due to automatic refresh.

        //Pull License Info
        $microsoft = SysaidLicenses::where('license','=','Microsoft')
            ->orderby('created_at','desc')
            ->first();

        $imprivata = SysaidLicenses::where('license','=','Imprivata')
            ->orderby('created_at','desc')
            ->first();

        $airwatch = SysaidLicenses::where('license','=','Airwatch')
            ->orderby('created_at','desc')
            ->first();

        $citrix = SysaidLicenses::where('license','=','Citrix')
            ->orderby('created_at','desc')
            ->first();

        $adobe = SysaidLicenses::where('license','=','Adobe')
            ->orderby('created_at','desc')
            ->first();
        //dd($microsoft);

        return View('sysaid.index', compact('total_open','is_closed','is_open','epic_closed','epic_open',
            'last_updated','category_epic','category_is','microsoft','imprivata','airwatch','citrix','adobe'));
    }



    public function sysaid_category(){

        $category = SysaidDashboard::selectRaw('category,count(*) as total')
            ->where('request_time', '>=', Carbon::now()->subDays(30))
            ->wherenotin('ticket_status',['Ticket - CLOSED'])
            ->wherenotin('category',['none','System Administration Related Request'])
            ->groupBy('category')
            ->orderBy('category')
            ->get();

        return $category;

    }

    public function is_epic(){

        $is = SysaidDashboard::selectRaw('count(*) as total1')
            ->where('request_time', '>=', Carbon::now()->subDays(30))
            ->where('sub_category', 'NOT LIKE', '%EPIC%')
            ->wherenotin('ticket_status',['Ticket - CLOSED'])
            ->get();

        $epic = SysaidDashboard::selectRaw('count(*) as total')
            ->where('request_time', '>=', Carbon::now()->subDays(30))
            ->where('sub_category', 'LIKE', '%EPIC%')
            ->wherenotin('ticket_status',['Ticket - CLOSED'])
            ->get();

        return $epic;
    }

    public function by_day(){

        $by_day = SysaidDashboard::selectRaw('ticket_dow, count(*) as total')
            ->where('request_time', '>=', Carbon::now()->subDays(30))
            ->groupby('ticket_dow')
            ->orderby('ticket_dow')
            ->get();

        return $by_day;

    }

    public function by_shift(){

        $by_day = SysaidDashboard::selectRaw('ticket_shift, count(*) as total')
            ->where('request_time', '>=', Carbon::now()->subDays(30))
            ->groupby('ticket_shift')
            ->orderby('ticket_shift')
            ->get();

        return $by_day;

    }

    public function by_hour(){

        $by_hour = SysaidDashboard::selectRaw('ticket_hour, count(*) as total')
            ->where('request_time', '>=', Carbon::now()->subDays(30))
            ->groupby('ticket_hour')
            ->orderby('ticket_hour')
            ->get();

        return $by_hour;

    }

    public function by_user(){

        $by_user = SysaidDashboard::selectRaw('requested_user, count(*) as total')
            ->where('request_time', '>=', Carbon::now()->subDays(30))
            ->wherenotin('requested_user',['none','no-reply.support@surescripts.com','SGMC.ORG\doclog'])
            ->groupby('requested_user')
            ->orderby('total','DESC')
            ->take(5)
            ->get();

        return $by_user;
    }

    public function by_manager(){

        $by_user = SysaidDashboard::selectRaw('process_manager, count(*) as total')
            ->where('request_time', '>=', Carbon::now()->subDays(30))
            ->wherein('ticket_status',['Ticket - CLOSED'])
            ->groupby('process_manager')
            ->orderby('total','DESC')
            ->take(5)
            ->get();

        return $by_user;
    }

    public function by_year(){

        $by_user = SysaidDashboard::selectRaw('ticket_year, count(*) as total')
            ->where('ticket_year', '>', '2016')
            //->wherein('ticket_status',['Ticket - CLOSED'])
            ->groupby('ticket_year')
            ->orderby('ticket_year')
            //->take(5)
            ->get();

        return $by_user;
    }

    public function by_esclation(){

        $by_user = SysaidDashboard::selectRaw('esclation, count(*) as total')
            //->where('request_time', '>=', Carbon::now()->subDays(30))
            ->wherenotin('ticket_status',['Ticket - CLOSED','Change - Completed'])
            //->wherenotin('esclation',['0'])
            ->groupby('esclation')
            ->orderby('esclation','DESC')
            //->take(5)
            ->get();

        return $by_user;
    }

    public function sysaid_admin_request(){

        $category = SysaidDashboard::selectRaw('category,count(*) as total')
            ->where('request_time', '>=', Carbon::now()->subDays(30))
            ->where('category','=','System Administration Related Request')


            ->wherenotin('ticket_status',['Ticket - CLOSED'])
            ->wherenotin('category',['none','System Administration Related Request'])
            ->groupBy('category')
            ->orderBy('category')
            ->get();

        return $category;

    }

    public function is_breakdown(){

        $by_user = SysaidDashboard::selectRaw('sgmc_category, count(*) as total')
            ->where('request_time', '>=', Carbon::now()->subDays(30))
            ->wherenotin('ticket_status',['Ticket - CLOSED','Change - Completed'])
            ->wherenotin('sgmc_category',['Not Organized'])
            ->groupby('sgmc_category')
            //->orderby('esclation','DESC')
            //->take(5)
            ->get();

        return $by_user;
    }

    public function epic_breakdown(){

        $by_user = SysaidDashboard::selectRaw('sgmc_sub_category, count(*) as total')
            ->where('request_time', '>=', Carbon::now()->subDays(30))
            ->wherenotin('ticket_status',['Ticket - CLOSED','Change - Completed'])
            ->wherenotin('sgmc_sub_category',['Not Organized'])
            ->groupby('sgmc_sub_category')
            //->orderby('esclation','DESC')
            //->take(5)
            ->get();

        return $by_user;
    }



}
