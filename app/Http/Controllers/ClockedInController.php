<?php

namespace App\Http\Controllers;

use App\ClockedIn;
use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use App\Productivity;

class ClockedInController extends Controller
{
    public function index()
    {
        DB::statement('SET ANSI_NULLS ON; SET ANSI_WARNINGS ON');

        $OrgUnit = DB::table('Productivity_Unit')->orderby('org_desc')->pluck('org_desc', 'Number');

        if(Input::has('submit_data')) {
            //Pulls hidden date value which is set to current date - developed this way for upgrade in future.
            $start_date = Input::get('start_date'). ' 00:00:00';
            //dd($start_date);
            $unit = Input::get('unit');
            
            $prod = ClockedIn::
            //where('work_date',$start_date)
                where('Number', $unit)
                //->selectRaw('sum(total_hours) as total_hours, emp_number, FirstName, LastName, Description, Number, org_desc, Code')
                //->groupby('emp_number', 'FirstName', 'LastName', 'Description', 'Number', 'org_desc', 'Code')
                ->orderby('LastName')
                ->get();

        }else{
            $prod = 'nothing';

        }

        return View('clockedin.index', compact('OrgUnit','prod', 'start_date', 'unit','sum'));
    }
}
