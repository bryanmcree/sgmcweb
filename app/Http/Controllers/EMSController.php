<?php

namespace App\Http\Controllers;

use Uuid;
use App\EMS;
use App\EMSZero;
use App\EMSNET;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;


class EMSController extends Controller
{
    public function index(){

        //Records that were not properly parsed
        $not_classified = EMS::where('incident_key','=','Not Classified')
            ->where('patient_disposition','<>','Cancelled Prior to arrival on Scene')
            ->get();

        $most_recent = EMS::selectRaw('dispatch_notified_time')
            ->wherenotnull('id')
            ->orderby('dispatch_notified_time', 'desc')
            ->first();

            // dd(Carbon::parse($most_recent->dispatch_notified_time)->startOfMonth());

        //Call Count
        $call_count = EMS::wherenotnull('id')
            ->where('dispatch_notified_time', '>', Carbon::parse($most_recent->dispatch_notified_time)->startOfMonth())
            ->get();
            
        //Get Incident Key Dropdown
        $incident_key = EMS::select('incident_key')
            ->wherenotin('incident_key',['Not Classified'])
            ->distinct()
            ->get();
        //dd($incident_key);

        //Line Chart calls for current year by month
        $sub = EMS::orderBy('call_month');

        $by_month = EMS::selectRaw('call_month_name, count(*) as total, call_month, call_year')
            ->where('dispatch_notified_time', '>=', Carbon::now()->subMonths(12)->startOfMonth())
            ->groupby(['call_month_name', 'call_year', 'call_month'])
            ->orderby('call_year', 'desc')
            ->orderby('call_month', 'desc')
            ->get();

        $responded_to = EMS::selectRaw('call_month_name, count(*) as total, call_month, call_year')
            ->where('dispatch_notified_time', '>=', Carbon::now()->subMonths(12)->startOfMonth())
            ->wherenotnull('unit_enroute_time')
            ->groupby(['call_month_name', 'call_year', 'call_month'])
            ->orderby('call_year', 'desc')
            ->orderby('call_month', 'desc')
            ->get();

        $transported = EMS::selectRaw('call_month_name, count(*) as total, call_month, call_year')
            ->where('dispatch_notified_time', '>=', Carbon::now()->subMonths(12)->startOfMonth())
            ->where('patient_disposition', 'Treated and Transported')
            ->groupby(['call_month_name', 'call_year', 'call_month'])
            ->orderby('call_year', 'desc')
            ->orderby('call_month', 'desc')
            ->get();

        // dd($transported, Carbon::now()->subMonths(12)->startOfMonth());

        //Lowndes County zone 1 numbers Numbers
        $lowndes_zone1 = EMS::selectRaw('
                                call_sign,
                                AVG(miles_to_call) as miles_out,
                                AVG(response_time) as response_time,
                                AVG(time_on_scene) as out_service
                                ')
            ->where('county','Lowndes')
            //->where( selectRaw('MONTH(dispatch_notified_time)'), '=', date('n') )
            ->where('dispatch_notified_time', '>', Carbon::parse($most_recent->dispatch_notified_time)->startOfMonth())
            ->wherein('incident_zone',['1'])
            ->wherein('call_sign',['SQUAD 1','SQUAD 2','SQUAD 3','SQUAD 4','SQUAD 5', 'SQUAD 6'])
            ->groupby('call_sign')
            ->orderby('call_sign')
            ->get();

        // dd($lowndes_zone1);


        $lowndes_zone2 = EMS::selectRaw('
                                call_sign,
                                AVG(miles_to_call) as miles_out,
                                AVG(response_time) as response_time,
                                AVG(time_on_scene) as out_service
                                ')
            ->where('county','Lowndes')
            //->where( selectRaw('MONTH(dispatch_notified_time)'), '=', date('n') )
            ->where('dispatch_notified_time', '>', Carbon::parse($most_recent->dispatch_notified_time)->startOfMonth())
            ->wherein('incident_zone',['2'])
            ->wherein('call_sign',['SQUAD 1','SQUAD 2','SQUAD 3','SQUAD 4','SQUAD 5', 'SQUAD 6'])
            ->groupby('call_sign')
            ->orderby('call_sign')
            ->get();

        $lowndes_zone3 = EMS::selectRaw('
                                call_sign,
                                AVG(miles_to_call) as miles_out,
                                AVG(response_time) as response_time,
                                AVG(time_on_scene) as out_service
                                ')
            ->where('county','Lowndes')
            //->where( selectRaw('MONTH(dispatch_notified_time)'), '=', date('n') )
            ->where('dispatch_notified_time', '>', Carbon::parse($most_recent->dispatch_notified_time)->startOfMonth())
            ->wherein('incident_zone',['3'])
            ->wherein('call_sign',['SQUAD 1','SQUAD 2','SQUAD 3','SQUAD 4','SQUAD 5', 'SQUAD 6'])
            ->groupby('call_sign')
            ->orderby('call_sign')
            ->get();

        $lowndes_zone4 = EMS::selectRaw('
                                call_sign,
                                AVG(miles_to_call) as miles_out,
                                AVG(response_time) as response_time,
                                AVG(time_on_scene) as out_service,
                                COUNT(*) as Total
                                ')
            ->where('county','Lowndes')
            //->where( selectRaw('MONTH(dispatch_notified_time)'), '=', date('n') )
            ->where('dispatch_notified_time', '>', Carbon::parse($most_recent->dispatch_notified_time)->startOfMonth())
            ->wherein('incident_zone',['4'])
            ->wherein('call_sign',['SQUAD 1','SQUAD 2','SQUAD 3','SQUAD 4','SQUAD 5', 'SQUAD 6'])
            ->groupby('call_sign')
            ->orderby('call_sign')
            ->get();

        $lowndes_zone5 = EMS::selectRaw('
                                call_sign,
                                AVG(miles_to_call) as miles_out,
                                AVG(response_time) as response_time,
                                AVG(time_on_scene) as out_service
                                ')
            ->where('county','Lowndes')
            ->where('dispatch_notified_time', '>', Carbon::parse($most_recent->dispatch_notified_time)->startOfMonth())
            ->wherein('incident_zone',['5'])
            ->wherein('call_sign',['SQUAD 1','SQUAD 2','SQUAD 3','SQUAD 4','SQUAD 5', 'SQUAD 6'])
            ->groupby('call_sign')
            ->orderby('call_sign')
            ->get();

        $lowndes_zone6 = EMS::selectRaw('
                        call_sign,
                        AVG(miles_to_call) as miles_out,
                        AVG(response_time) as response_time,
                        AVG(time_on_scene) as out_service
                        ')
            ->where('county','Lowndes')
            ->where('dispatch_notified_time', '>', Carbon::parse($most_recent->dispatch_notified_time)->startOfMonth())
            ->wherein('incident_zone',['6'])
            ->wherein('call_sign',['SQUAD 1','SQUAD 2','SQUAD 3','SQUAD 4','SQUAD 5', 'SQUAD 6'])
            ->groupby('call_sign')
            ->orderby('call_sign')
            ->get();

        //squad totals adv
        $lowndes_squad_totals = EMS::selectRaw('
                                call_sign,
                                AVG(miles_to_call) as miles_out,
                                AVG(response_time) as response_time,
                                AVG(time_on_scene) as out_service,
                                COUNT(*) as total
                                ')
            ->where('county','Lowndes')
            //->where( selectRaw('MONTH(dispatch_notified_time)'), '=', date('n') )
            ->where('dispatch_notified_time', '>', Carbon::parse($most_recent->dispatch_notified_time)->startOfMonth())
            ->wherein('incident_zone',['1', '2', '3', '4', '5', '6'])
            ->wherein('call_sign',['SQUAD 1','SQUAD 2','SQUAD 3','SQUAD 4','SQUAD 5','SQUAD 6'])
            ->groupby('call_sign')
            ->orderby('call_sign')
            ->get();
        
        //zone totals adv
        $lowndes_zone_totals = EMS::selectRaw('
                                incident_zone,
                                AVG(miles_to_call) as miles_out,
                                AVG(response_time) as response_time,
                                AVG(time_on_scene) as out_service,
                                COUNT(*) as total
                                ')
            ->where('county','Lowndes')
            ->where('dispatch_notified_time', '>', Carbon::parse($most_recent->dispatch_notified_time)->startOfMonth())
            ->groupby('incident_zone')
            ->orderby('incident_zone')
            ->get();

        // dd($lowndes_squad_totals, $lowndes_zone_totals);

        //Lowndes County zone 1 numbers Numbers
        $lanier_zone1 = EMS::selectRaw('
                                call_sign,
                                AVG(miles_to_call) as miles_out,
                                AVG(response_time) as response_time,
                                AVG(time_on_scene) as out_service
                                ')
            ->where('county','Lanier')
            //->where( selectRaw('MONTH(dispatch_notified_time)'), '=', date('n') )
            ->where('dispatch_notified_time', '>', Carbon::parse($most_recent->dispatch_notified_time)->startOfMonth())
            ->wherein('incident_zone',['9                - 1  Lakeland'])
            ->wherein('call_sign',['LANIER COUNTY - M1','LANIER COUNTY - M2','LANIER COUNTY - M3'])
            ->groupby('call_sign')
            ->orderby('call_sign')
            ->get();

        //dd($lanier_zone1);

        $lanier_zone2 = EMS::selectRaw('
                                call_sign,
                                AVG(miles_to_call) as miles_out,
                                AVG(response_time) as response_time,
                                AVG(time_on_scene) as out_service
                                ')
            ->where('county','Lanier')
            //->where( selectRaw('MONTH(dispatch_notified_time)'), '=', date('n') )
            ->where('dispatch_notified_time', '>', Carbon::parse($most_recent->dispatch_notified_time)->startOfMonth())
            ->wherein('incident_zone',['9 - 2 Teeterville'])
            ->wherein('call_sign',['LANIER COUNTY - M1','LANIER COUNTY - M2','LANIER COUNTY - M3'])
            ->groupby('call_sign')
            ->orderby('call_sign')
            ->get();
        //dd($lowndes_zone1);

        $lanier_zone3 = EMS::selectRaw('
                                call_sign,
                                AVG(miles_to_call) as miles_out,
                                AVG(response_time) as response_time,
                                AVG(time_on_scene) as out_service
                                ')
            ->where('county','Lanier')
            //->where( selectRaw('MONTH(dispatch_notified_time)'), '=', date('n') )
            ->where('dispatch_notified_time', '>', Carbon::parse($most_recent->dispatch_notified_time)->startOfMonth())
            ->wherein('incident_zone',['9 - 3 Mud Creek'])
            ->wherein('call_sign',['LANIER COUNTY - M1','LANIER COUNTY - M2','LANIER COUNTY - M3'])
            ->groupby('call_sign')
            ->orderby('call_sign')
            ->get();
        //dd($lowndes_zone1);

        $lanier_zone4 = EMS::selectRaw('
                                call_sign,
                                AVG(miles_to_call) as miles_out,
                                AVG(response_time) as response_time,
                                AVG(time_on_scene) as out_service
                                ')
            ->where('county','Lanier')
            //->where( selectRaw('MONTH(dispatch_notified_time)'), '=', date('n') )
            ->where('dispatch_notified_time', '>', Carbon::parse($most_recent->dispatch_notified_time)->startOfMonth())
            ->wherein('incident_zone',['9 - 4 West Side'])
            ->wherein('call_sign',['LANIER COUNTY - M1','LANIER COUNTY - M2','LANIER COUNTY - M3'])
            ->groupby('call_sign')
            ->orderby('call_sign')
            ->get();
        //dd($lowndes_zone1);

        $lanier_zone5 = EMS::selectRaw('
                                call_sign,
                                AVG(miles_to_call) as miles_out,
                                AVG(response_time) as response_time,
                                AVG(time_on_scene) as out_service
                                ')
            ->where('county','Lanier')
            //->where( selectRaw('MONTH(dispatch_notified_time)'), '=', date('n') )
            ->where('dispatch_notified_time', '>', Carbon::parse($most_recent->dispatch_notified_time)->startOfMonth())
            ->wherein('incident_zone',['9 - 5 Stockton'])
            ->wherein('call_sign',['LANIER COUNTY - M1','LANIER COUNTY - M2','LANIER COUNTY - M3'])
            ->groupby('call_sign')
            ->orderby('call_sign')
            ->get();
        //dd($lowndes_zone5);

        //Grand totals adv
        $lanier_zone6 = EMS::selectRaw('
                                call_sign,
                                AVG(miles_to_call) as miles_out,
                                AVG(response_time) as response_time,
                                AVG(time_on_scene) as out_service
                                ')
            ->where('county','Lanier')
            //->where( selectRaw('MONTH(dispatch_notified_time)'), '=', date('n') )
            ->where('dispatch_notified_time', '>', Carbon::parse($most_recent->dispatch_notified_time)->startOfMonth())
            ->wherein('incident_zone',['9 - 1  Lakeland','9 - 2 Teeterville','9 - 3 Mud Creek','9 - 4 West Side','9 - 5 Stockton'])
            ->wherein('call_sign',['LANIER COUNTY - M1','LANIER COUNTY - M2','LANIER COUNTY - M3'])
            ->groupby('call_sign')
            ->orderby('call_sign')
            ->get();
        //dd($lowndes_zone5);

        $lanier_zone_totals = EMS::selectRaw('
                                incident_zone,
                                AVG(miles_to_call) as miles_out,
                                AVG(response_time) as response_time,
                                AVG(time_on_scene) as out_service,
                                COUNT(*) as total
                                ')
            ->where('county','Lanier')
            ->where('dispatch_notified_time', '>', Carbon::parse($most_recent->dispatch_notified_time)->startOfMonth())
            ->groupby('incident_zone')
            ->orderby('incident_zone')
            ->get();

        // dd($lanier_zone_totals);


        $echols = EMS::selectRaw('
                                call_sign,
                                AVG(miles_to_call) as miles_out,
                                AVG(response_time) as response_time,
                                AVG(time_on_scene) as out_service
                                ')
            ->where('county','Echols')
            //->where( selectRaw('MONTH(dispatch_notified_time)'), '=', date('n') )
            ->where('dispatch_notified_time', '>', Carbon::parse($most_recent->dispatch_notified_time)->startOfMonth())
            ->wherein('incident_zone',['8 - Echols County'])
            //->wherein('call_sign',['SQUAD 1','SQUAD 2','SQUAD 4','SQUAD 5'])
            ->groupby('call_sign')
            ->orderby('call_sign')
            ->get();

        // dd($echols);

        $allStats = EMS::selectRaw('
                            AVG(miles_to_call) as miles_out,
                            AVG(response_time) as response_time,
                            AVG(time_on_scene) as out_service
                            ')
            ->where('dispatch_notified_time', '>', Carbon::parse($most_recent->dispatch_notified_time)->startOfMonth())
            ->first();

        // dd($allStats);

        $zeros = EMSZero::wherenotnull('id')
            ->orderby('date')
            ->get();

        $NETs = EMSNET::where('date', '>=', Carbon::now()->subMonths(3)->startOfMonth())
            ->orderby('date', 'desc')
            ->get();


        return View('ems.index', compact('not_classified','call_count','lowndes_zone1','lowndes_zone2', 'by_month', 'responded_to', 'transported',
            'lowndes_zone3','lowndes_zone4','lowndes_zone5', 'lowndes_zone6', 'lowndes_squad_totals','lowndes_zone_totals','lanier_zone1','lanier_zone2','lanier_zone3',
            'lanier_zone4','lanier_zone5','lanier_zone6', 'echols', 'allStats', 'zeros','incident_key', 'NETs', 'lanier_zone_totals', 'most_recent'));
    }

    public function chart_by_month(){

        //Line Chart calls for current year by month
        $by_month = EMS::selectRaw('call_month_name, count(*) as total, call_month, call_year')
            ->where('dispatch_notified_time', '>=', Carbon::now()->subMonths(12)->startOfMonth())
            ->groupby(['call_month_name', 'call_year', 'call_month'])
            ->orderby('call_year')
            ->orderby('call_month')
            ->get();

        return $by_month;
    }

    public function editUnclassified(){

        //dd();
        $incident = EMS::where('id','=', Input::get('id') );
        //dd($item);
        $incident->update(Input::except(['id','_token']));

        alert()->success('Item Updated!', 'Success!');
        return redirect('/ems');

    }

    public function zero(Request $request)
    {
        $request->offsetSet('id', Uuid::generate(4));

        EMSZero::create($request->all());

        alert()->success('Total Recorded!', 'Success!');
        return redirect()->back();
    }

    public function showZero()
    {
        $zeros = EMSZero::wherenotnull('id')
            ->orderby('date', 'desc')
            ->get();

        return view('ems.zero', compact('zeros'));
    }

    public function showNET()
    {
        $NETs = EMSNET::wherenotnull('id')
            ->orderby('date', 'desc')
            ->get();

        $totalNET = EMSNET::selectRaw('SUM(wheelchair) as wheelchair,
                                        SUM(stretcher) as stretcher,
                                        SUM(under_50) as under_50,
                                        SUM(hospice) as hospice,
                                        SUM(over_50) as over_50,
                                        SUM(punts) as punts,
                                        SUM(diff_campus) as diff_campus,
                                        MONTH(date) as month, YEAR(date) as year')
            ->groupby(\DB::raw('YEAR(date), MONTH(date)'))
            ->orderby(\DB::raw('YEAR(date), MONTH(date)'), 'desc')
            ->get();

            // dd($totalNET);

        return view('ems.NET', compact('NETs', 'totalNET'));
    }

    public function storeNET(Request $request)
    {
        $request->offsetSet('id', Uuid::generate(4));

        EMSNET::create($request->all());

        alert()->success('NET Recorded!', 'Success!');
        return redirect()->back();
    }

    public function destroyNET(EMSNET $NET)
    {
        $NET->delete();
    }

    public function zeroDestroy(EMSZero $zero)
    {
        $zero->delete();
    }

    public function report(Request $request)
    {
        $startDate = Carbon::parse($request->startDate)->format('Y-m-d H:i:s');
        $endDate = Carbon::parse($request->endDate)->format('Y-m-d H:i:s');
        $county = $request->county;

        $report = EMS::selectRaw('
                                AVG(miles_to_call) as miles_out,
                                AVG(response_time) as response_time,
                                AVG(time_on_scene) as out_service
                                ')
            ->where('county', $request->county)
            ->whereBetween('dispatch_notified_time', [$startDate, $endDate])
            ->first();

            // dd($startDate, $endDate, $report);

        return view('ems.report', compact('report', 'startDate', 'endDate', 'county'));
    }

    public function scriptMap()
    {
        $addresses = EMS::select('id','incident_address')
            ->wherenull('plotted')
            ->orderby('dispatch_notified_time', 'desc')
            ->get()
            ->take(1);

        // dd($addresses);

        return view('ems.scriptMap', compact('addresses'));
    }

    public function coordinates(EMS $ems, $coordinates)
    {
        if($coordinates == 'NA')
        {
            $ems->update([
                'coordinates' => NULL,
                'plotted' => '0'
            ]);
        }
        else
        {
            $ems->update([
                'coordinates' => $coordinates,
                'plotted' => '1'
            ]);
        }
       
    }

    public function map()
    {
        $coords = EMS::select('coordinates', 'lat', 'long', 'incident_address')
            ->where('plotted', '=', 1)
            ->get();
        
        return view('ems.map', compact('coords'));
    }


}
