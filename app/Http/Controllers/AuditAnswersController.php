<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Uuid;
use Auth;
use Carbon;
use Excel;
use App\History;
use App\CostCenters;
use App\Audit;
use App\AuditAnswer;
use App\AuditQuestion;
use App\AuditEPList;
use App\AuditStandardList;
use App\AuditQuestionEP;
use App\AuditQuestionStandard;

class AuditAnswersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Audit $audit)
    {
        $questions = AuditQuestion::with('question_answers')
            ->where('survey_id','=',$audit->id)
            ->orderby('question_order')
            ->get();

        $cost_centers = CostCenters::where('active_status','=',1)
            ->orderby('style1')
            ->get();

        return view('audit.survey', compact('questions', 'cost_centers', 'audit'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Lets find this survey and add one to the count.
        $count = Audit::where('id','=', $request->survey_id)->first();
        $count->taken_count = $count->taken_count + 1;
        $count->save();

        //Now lets generate the incident id
        $last_incident_id = AuditAnswer::select('incident_id')
            ->where('survey_id','=', $request->survey_id)
            ->orderby('incident_id','desc')
            ->first();

        $survey = Audit::where('id','=', $request->survey_id)->first();

        if($last_incident_id == NULL)
        {
            $new_incident_id = 1;
        }
        else
        {
            $new_incident_id = $last_incident_id->incident_id + 1;
        }

        // remove the token
        $arr = $request->except('_token','survey_id','submitted_by', 'cost_center');
        
        // dd($arr);
        foreach ($arr as $key => $value) 
        {
            $newAnswer = new AuditAnswer();
            if (! is_array( $value )) {
                $newValue = $value['answer'];
                $question_type = $value['question_type'];
            } else {
                $newValue = json_encode($value['answer']);
                $question_type = json_encode($value['question_type']);
            }
            // dd($newValue, $question_type);
            //$newAnswer->answer = str_replace('"','',$newValue);
            $newAnswer->survey_id = $request->survey_id;
            $newAnswer->question_id = $key;
            $newAnswer->incident_id = $new_incident_id;

            if(str_replace('"','',$question_type) == 'YesNoNA'){
                $newAnswer->answer = str_replace('"','',$newValue);
            }else{
                $newAnswer->answer_string = str_replace('"','',$newValue);
            }

            $newAnswer->submitted_by = $request->submitted_by;
            $newAnswer->question_type = str_replace('"','',str_replace('"','',$question_type));
            $newAnswer->id = Uuid::generate(4);
            $newAnswer->cost_center = $request->cost_center;
            $newAnswer->save();

            $answerArray[] = $newAnswer;
        };

        $history = new History();
        $history->id = \Uuid::generate(4);
        $history->action = 'Survey Submitted (Audit)';
        $history->userid = \Auth::user()->username;
        $history->search_string = 'None';
        $history->user_ip = \Request::ip();
        $history->save();

        alert()->success('Survey Completed!', 'Success!');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function report(Audit $audit)
    {
        // $startDate = Carbon::parse($request->startDate)->format('Y-m-d H:i:s');
        // $endDate = Carbon::parse($request->endDate)->addDay()->format('Y-m-d H:i:s');

        $questions = AuditQuestion::with('question_answers')
            ->where('survey_id','=',$audit->id)
            ->whereNull('deleted_at')
            ->orderby('question_order')
            ->get();

        $yesnoNA = AuditAnswer::selectRaw('question_id, answer, cost_center, count(*) as total')
            ->where('survey_id','=',$audit->id)
            ->where('question_type','=','YesNoNA')
            ->groupby(['question_id','answer', 'cost_center'])
            ->orderby('answer','DESC')
            ->get();

        $shortAnswer = AuditAnswer::selectRaw('month(created_at) submitted_month, year(created_at) submitted_year, count(*) as total, question_id')
            ->where('survey_id','=',$audit->id) 
            ->where('question_type', '=', 'short_answer')
            ->groupby([\DB::raw('month(created_at)'), \DB::raw('year(created_at)'), 'question_id'])
            ->get();

        $hasShort = AuditQuestion::where('survey_id','=',$audit->id) 
            ->where('question_type', '=', 'short_answer')
            ->count();

        $ccs = AuditAnswer::select('cost_center')->where('survey_id', $audit->id)->distinct()->get();
        $qs = AuditAnswer::select('question_id')->where('survey_id', $audit->id)->where('question_type', 'YesNoNA')->distinct()->get();

        // dd($qs);

        $num_denom = AuditAnswer::selectRaw('month(created_at) submitted_month, year(created_at) submitted_year, cost_center, sum(answer) as num_of_yes, count(*) as total, question_id')
            ->where('survey_id','=',$audit->id)
            ->where('answer', '<>', 2)
            ->groupby([\DB::raw('month(created_at)'), \DB::raw('year(created_at)'), 'question_id', 'cost_center'])
            ->get();

        $aggregates = AuditAnswer::selectRaw('month(created_at) submitted_month, year(created_at) submitted_year, sum(answer) as num_of_yes, count(*) as total, question_id')
            ->where('survey_id','=',$audit->id) 
            ->where('answer', '<>', 2)
            ->groupby([\DB::raw('month(created_at)'), \DB::raw('year(created_at)'), 'question_id'])
            ->get();

        $standards = AuditQuestionStandard::wherenotnull('id')->get();
        $eps = AuditQuestionEP::wherenotnull('id')->get();

        // dd($num_denom);

        return view('audit.report', compact('audit','questions', 'num_denom', 'yesnoNA', 'ccs', 'qs', 'aggregates', 'standards', 'eps', 'shortAnswer', 'hasShort'));
    }

    public function print(Audit $audit)
    {
        // $startDate = Carbon::parse($request->startDate)->format('Y-m-d H:i:s');
        // $endDate = Carbon::parse($request->endDate)->addDay()->format('Y-m-d H:i:s');

        $questions = AuditQuestion::with('question_answers')
            ->where('survey_id','=',$audit->id)
            ->orderby('question_order')
            ->get();

        $yesnoNA = AuditAnswer::selectRaw('question_id, answer, cost_center, count(*) as total')
            ->where('survey_id','=',$audit->id)
            ->where('question_type','=','YesNoNA')
            ->groupby(['question_id','answer', 'cost_center'])
            ->orderby('answer','DESC')
            ->get();

        $ccs = AuditAnswer::select('cost_center')->where('survey_id', $audit->id)->distinct()->get();
        $qs = AuditAnswer::select('question_id')->where('survey_id', $audit->id)->where('question_type', 'YesNoNA')->distinct()->get();

        // dd($qs);

        $num_denom = AuditAnswer::selectRaw('month(created_at) submitted_month, year(created_at) submitted_year, cost_center, sum(answer) as num_of_yes, count(*) as total, question_id')
            ->where('survey_id','=',$audit->id)
            ->where('answer', '<>', 2)
            ->groupby([\DB::raw('month(created_at)'), \DB::raw('year(created_at)'), 'question_id', 'cost_center'])
            ->get();

        $aggregates = AuditAnswer::selectRaw('month(created_at) submitted_month, year(created_at) submitted_year, sum(answer) as num_of_yes, count(*) as total, question_id')
            ->where('survey_id','=',$audit->id) 
            ->where('answer', '<>', 2)
            ->groupby([\DB::raw('month(created_at)'), \DB::raw('year(created_at)'), 'question_id'])
            ->get();

        $standards = AuditQuestionStandard::wherenotnull('id')->get();
        $eps = AuditQuestionEP::wherenotnull('id')->get();

        // dd($num_denom);

        return view('audit.print', compact('audit','questions', 'num_denom', 'yesnoNA', 'ccs', 'qs', 'aggregates', 'standards', 'eps'));
    }

    public function excel(Audit $audit)
    {
        $aggregates = AuditAnswer::selectRaw('question_id as question, month(created_at) submitted_month, year(created_at) submitted_year, sum(answer) as num_of_yes, count(*) as total, 0 as numerator_denominator, 0 as compliance_percentage')
            ->where('survey_id','=',$audit->id) 
            ->where('answer', '<>', 2)
            ->groupby([\DB::raw('month(created_at)'), \DB::raw('year(created_at)'), 'question_id'])
            ->get()
            ->toArray();

        $byMonth = AuditAnswer::selectRaw('month(created_at) submitted_month, year(created_at) submitted_year, sum(answer) as num_of_yes, count(*) as total, question_id')
            ->where('survey_id','=',$audit->id) 
            ->where('answer', '<>', 2)
            ->groupby([\DB::raw('month(created_at)'), \DB::raw('year(created_at)'), 'question_id'])
            ->get();

        // dd($aggregates);

        foreach($aggregates as $key => $value) 
        {
            // var_dump($aggregates[$key]['question_id']);
            foreach($byMonth as $by)
            {
                if($aggregates[$key]['question'] == $by->question_id)
                {
                    // var_dump($t->question_title->title);
                    // var_dump($aggregates[$key]['question_id']);

                    $aggregates[$key]['question'] = $by->question_title->title;
                    $aggregates[$key]['numerator_denominator'] = $by->num_of_yes . '/' . $by->total;
                    $aggregates[$key]['compliance_percentage'] = $by->num_of_yes/$by->total;
                }
            }
        }

        // dd($aggregates);

        $detailed = AuditAnswer::selectRaw('question_id as question, month(created_at) submitted_month, year(created_at) submitted_year, cost_center, sum(answer) as num_of_yes, count(*) as total, 0 as numerator_denominator, 0 as compliance_percentage')
            ->where('survey_id','=',$audit->id)
            ->where('answer', '<>', 2)
            ->groupby([\DB::raw('month(created_at)'), \DB::raw('year(created_at)'), 'question_id', 'cost_center'])
            ->get()
            ->toArray();

        $byCC = AuditAnswer::selectRaw('month(created_at) submitted_month, year(created_at) submitted_year, cost_center, sum(answer) as num_of_yes, count(*) as total, question_id')
            ->where('survey_id','=',$audit->id)
            ->where('answer', '<>', 2)
            ->groupby([\DB::raw('month(created_at)'), \DB::raw('year(created_at)'), 'question_id', 'cost_center'])
            ->get();

        // dd($num_denom);

        foreach($detailed as $key => $value) 
        {
            foreach($byCC as $by)
            {
                if($detailed[$key]['question'] == $by->question_id && $detailed[$key]['cost_center'] == $by->cost_center)
                {
                    $detailed[$key]['question'] = $by->question_title->title;
                    $detailed[$key]['numerator_denominator'] = $by->num_of_yes . '/' . $by->total;
                    $detailed[$key]['compliance_percentage'] = $by->num_of_yes/$by->total;
                }
            }
        }

        // dd($detailed);

        Excel::create( $audit->name . ' ' . date('m-d-Y_hia'), function($excel) use($aggregates, $detailed) {

            $excel->sheet('Aggregate by Month', function($sheet) use($aggregates) {

                $sheet->setColumnFormat(array(
                    'D' => '0',
                    'E' => '0',
                    'F' => '@',
                    'G' => '0.00%',
                ));

                $sheet->fromArray($aggregates, null, 'A1', true);

            });

            $excel->sheet('By Cost Center, By Month', function($sheet) use($detailed) {

                $sheet->setColumnFormat(array(
                    'D' => '@',
                    'E' => '0',
                    'F' => '0',
                    'G' => '@',
                    'H' => '0.00%',
                ));

                $sheet->fromArray($detailed, null, 'A1', true);

            });

        })->export('xls');
    }

    public function shortExcel(Audit $audit)
    {
        $answers = AuditAnswer::selectRaw('audit_questions.title as question, month(audit_answers.created_at) submitted_month, year(audit_answers.created_at) submitted_year, audit_answers.answer_string')
        ->where('audit_answers.survey_id', '=', $audit->id)
        ->where('audit_answers.question_type', '=', 'short_answer')
        ->where('audit_answers.answer_string', '<>', '')
        ->leftJoin('audit_questions', 'audit_answers.question_id', '=', 'audit_questions.id')
        ->orderby('audit_questions.title')
        ->get()
        ->toArray();

        Excel::create( $audit->name . ' ' . date('m-d-Y_hia'), function($excel) use($answers) {

            $excel->sheet('Short Answers by Month', function($sheet) use($answers) {

                $sheet->fromArray($answers, null, 'A1', true);

            });

        })->export('xls');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
