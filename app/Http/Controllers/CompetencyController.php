<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Uuid;
use Auth;
use Carbon;
use App\User;
use App\NurseCompetency;
use App\NursingCompetency;
use App\NursingCompCategory;
use App\NursingCompQuestion;
use App\NursingCompChecklist;

class CompetencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->hasRole('Nursing Competency Admin') == 1)
        {
            $nurses = User::selectRaw('name, id')->where('emp_status', 'active')->wherenotnull('id')->orderby('name')->get();
            $comps = NursingCompetency::wherenotnull('id')->orderby('title')->get();

            $nurse_detail = NurseCompetency::wherenotnull('id')->distinct()->get(['nurse_id']);
            $comp_detail = NurseCompetency::wherenotnull('id')->distinct()->get(['comp_id']);

            $assigned90 = NurseCompetency::where('created_at', '>=', Carbon::now()->subDays(90))
                ->count();

            $completed90 = NurseCompetency::where('created_at', '>=', Carbon::now()->subDays(90))
                ->wherenotnull('date_completed')
                ->count();

            $completedAll = NurseCompetency::wherenotnull('date_completed')
                ->count();

            $activeComps = NurseCompetency::wherenull('date_completed')
                ->count();

            $expired = NurseCompetency::selectRaw('nurse_competencies.date_completed, nurse_competencies.created_at, nursing_competencies.days_to_complete as days_to_complete')
                ->wherenotnull('nurse_competencies.id')
                ->whereRaw('DATEADD(day, nursing_competencies.days_to_complete, nurse_competencies.created_at) < nurse_competencies.date_completed')
                ->orwhereRaw('DATEADD(day, nursing_competencies.days_to_complete, nurse_competencies.created_at) < getdate()')
                ->join('nursing_competencies', 'nurse_competencies.comp_id', '=', 'nursing_competencies.id')
                ->count();

            // dd($assigned90, $completed90, $completedAll, $activeComps, $notComplete);

            return view('competencies.admin.index', compact('comps', 'nurses', 'nurse_detail', 'assigned90', 'completed90', 'completedAll', 'activeComps', 'expired', 'comp_detail'));
        }
        else
        {
            $user = User::where('id', Auth::user()->id)->first();
            $nurses = NurseCompetency::wherenotnull('id')->distinct()->get(['nurse_id']);
            $competencies = NursingCompetency::wherenotnull('id')->orderby('title')->get();

            // dd($nurses);

            return view('competencies.user.index', compact('user', 'nurses', 'competencies'));
        }
        
    }

    public function nurseDashboard($id)
    {   
        $incomplete = NurseCompetency::where('nurse_id', $id)->wherenull('date_completed')->get();

        $completed = NurseCompetency::where('nurse_id', $id)->wherenotnull('date_completed')->get();

        $nurse = User::where('id', $id)->first();

        return view('competencies.user.dashboard', compact('incomplete', 'completed', 'nurse'));
    }

    public function compDashboard($id)
    {   
        $incomplete = NurseCompetency::where('comp_id', $id)->wherenull('date_completed')->get();

        $completed = NurseCompetency::where('comp_id', $id)->wherenotnull('date_completed')->get();

        $comp = NursingCompetency::where('id', $id)->first();

        return view('competencies.admin.dashboard', compact('incomplete', 'completed', 'comp'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->offsetSet('id', Uuid::generate(4));

        NursingCompetency::create($request->all());

        alert()->success('Competency Checklist Created!', 'Success!');
        return back();
    }

    public function assign(Request $request)
    {
        $comp = $request->competencies;

        // dd($nurse, $comp);

        foreach($comp as $c)
        {
            NurseCompetency::create([
                'id' => Uuid::generate(4),
                'nurse_id' => $request->nurse_id,
                'comp_id' => $c,
            ]);
        }

        alert()->success('Competency Assigned!', 'Success!');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(NursingCompetency $competency)
    {
        $categories = NursingCompCategory::where('comp_id', $competency->id)->orderby('category_order')->get();
        $questions = NursingCompQuestion::where('comp_id', $competency->id)->orderby('question_order')->get();

        return view('competencies.admin.show', compact('competency', 'categories', 'questions'));
    }

    public function notComplete()
    {
        $expired = NurseCompetency::selectRaw('nurse_competencies.nurse_id, nurse_competencies.comp_id, nurse_competencies.date_completed, nurse_competencies.created_at, nursing_competencies.days_to_complete as days_to_complete')
            ->wherenotnull('nurse_competencies.id')
            ->whereRaw('DATEADD(day, nursing_competencies.days_to_complete, nurse_competencies.created_at) < nurse_competencies.date_completed')
            ->orwhereRaw('DATEADD(day, nursing_competencies.days_to_complete, nurse_competencies.created_at) < getdate()')
            ->join('nursing_competencies', 'nurse_competencies.comp_id', '=', 'nursing_competencies.id')
            ->get();

        // dd($expired);

        return view('competencies.admin.expired', compact('expired'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(NursingCompetency $comp)
    {
        $comp->delete();

        $pivots = NurseCompetency::where('comp_id', $comp->id)->get();

        foreach($pivots as $pivot)
        {
            $pivot->delete();
        }
    }
}
