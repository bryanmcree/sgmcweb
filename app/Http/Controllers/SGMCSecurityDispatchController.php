<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;

use App\Http\Requests;
use App\SecurityBadges;
use App\SecurityCitation;
use App\SecurityActivityList;
use App\SecurityLocationList;
use App\SecurityDispatch;
use App\SecurityRounds;
use App\SecurityViolation;
use App\SecurityIncident;
use App\SecurityIncidentList;
use App\User;
use App\History;
use Auth;
use Carbon;
use DB;

class SGMCSecurityDispatchController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $activities = SecurityActivityList::wherenotnull('id')->get();
        $locations = SecurityLocationList::wherenotnull('id')->get();
        $officers = User::where('unit_code_description', '=', 'SAFETY & SECURITY')
            ->where('emp_status', '=', 'Active')
            ->get();

        //dd($activities, $locations, $officers);

        return view('sgmcsecurity.dispatch', compact('user', 'activities', 'locations', 'officers'));
    }

    public function store(Request $request)
    {
        $dispatched = new Carbon($request->dispatched);
        $arrived =new Carbon($request->arrived);
        $response = $arrived->diff($dispatched)->format('%H:%I:%S'); // Response Time Calculated

        if($request->completed != '')
        {
            $dispatchedT = new Carbon($request->dispatched);
            $completed =new Carbon($request->completed);
            $total = $completed->diff($dispatchedT)->format('%H:%I:%S'); // Total Time Calculated

            $request->offsetSet('total', $total);
        }
        else
        {
            $request->offsetSet('total', '00:00:00.000000');
        }

        //dd($response, $total);

        $request->offsetSet('response', $response);
        $request->offsetSet('id', Uuid::generate(4));
        
        $dispatch = $request->all();

        SecurityDispatch::create($dispatch);

        alert()->success('Dispatch Recorded!', 'Success!');
        return redirect()->back();
    }

    public function edit($id)
    {
        $dis = SecurityDispatch::find($id);

        //dd($dis);
        $activities = SecurityActivityList::wherenotnull('id')->get();
        $locations = SecurityLocationList::wherenotnull('id')->get();
        $officers = User::where('unit_code_description', '=', 'SAFETY & SECURITY')
            ->where('emp_status', '=', 'Active')
            ->get();

        return view('sgmcsecurity.edit.dispatch', compact('dis', 'activities', 'locations', 'officers'));
    }

    public function update(Request $request, $id)
    {
        $dispatched = new Carbon($request->dispatched);
        $arrived =new Carbon($request->arrived);
        $response = $arrived->diff($dispatched)->format('%H:%I:%S'); // Response Time Calculated

        if($request->completed == '00:00:00' OR $request->completed == '00:00')
        {
            $total = '00:00:00.000000';
        }
        else
        {
            $dispatchedT = new Carbon($request->dispatched);
            $completed =new Carbon($request->completed);
            $total = $completed->diff($dispatchedT)->format('%H:%I:%S'); // Total Time Calculated
        }

        $new = SecurityDispatch::find($id);

        $new->id = $request->id;
        $new->date = $request->date;
        $new->dispatched = $request->dispatched;
        $new->arrived = $request->arrived;
        $new->activity = $request->activity;
        $new->location = $request->location;
        $new->room_num = $request->room_num;
        $new->officer = $request->officer;
        $new->completed = $request->completed;
        $new->remarks = $request->remarks;
        $new->dispatcher = $request->dispatcher;
        $new->response = $response;
        $new->total = $total;

        $new->save();

        alert()->success('Dispatch Updated!', 'Success!');
        return redirect('/security');  
        
    }

    public function show(Request $request, $id)
    {
        $dispatch = SecurityDispatch::find($id);

        return view('sgmcsecurity.reports.showDispatch', compact('dispatch'));
    }

    public function destroy(SecurityDispatch $dispatch)
    {
        $dispatch->delete();
    }

    public function ongoing()
    {
        $on = SecurityDispatch::where('completed', '=', '00:00:00.000000')
            ->get();

        $allDis = SecurityDispatch::where('date', '>', Carbon::now()->subDays(10))
            ->orderBy('date', 'desc')
            ->get();

        return view('sgmcsecurity.ongoing', compact('on', 'allDis'));        
    }
    
}
