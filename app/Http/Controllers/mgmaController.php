<?php

namespace App\Http\Controllers;

use App\MGMA;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;

class mgmaController extends Controller
{
    public function index(){

     $dropdown = MGMA::whereNotNull('10')
         ->orderby('Specialty')
         ->get();

     //dd($dropdown);

    if(Input::get('specialty')==''){

        $data = MGMA::select('Specialty','10 as TEN','25 as TWENTY','50 as FIFTY','75 as SEVENTY','90 as NINETY','formula')
            ->where('Specialty','=','Allergy/Immunology')
            ->first();

    }else{

        $data = MGMA::select('Specialty','10 as TEN','25 as TWENTY','50 as FIFTY','75 as SEVENTY','90 as NINETY','formula')
            ->where('Specialty','=',Input::get('specialty'))
            ->first();

    }

        \Session::flash('status', 'Loaded  ..... '. $data->Specialty );

        return view('mgma.index', compact('names','data','dropdown'));
    }

    public function formula(){

        $updateformula = MGMA::where('Specialty','=',Input::get('specialty'))->first();

        //dd($updateformula);

        $updateformula->update(Input::only('formula'));

        \Session::flash('status', 'Formula Updated' );

        return redirect('/mgma');

    }
}
