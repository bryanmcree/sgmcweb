<?php

namespace App\Http\Controllers;

use App\CostCenters;
use App\Role;
use App\RoleUser;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SecurityController extends Controller
{
    public function index(){

        $costcenters = CostCenters::wherenotnull('id')
            ->where('active_status','1')
            ->orderby('cost_center')
            ->get();

        $roles = Role::wherenotnull('id')
            ->orderby('name')
            ->get();

        return view('security.index', compact('costcenters','roles'));
    }

    public function assign_roles(Requests\SystemListRequest $request){


        //First find all employees assigned to the cost center

        $employees = User::where('unit_code','=', $request->cost_center)
            ->where('emp_status','<>','Terminated')
            ->get();
        //dd($employees);

        //now lets loop through all employees and assign the role to them.

        foreach($employees as $employee){

            //Lets check for the role first
            $check = RoleUser::where('role_id','=',$request->role)
                ->where('user_id','=', $employee->id)
                ->first();

            if(empty($check)){
                $newrole = new RoleUser();
                $newrole ->role_id = $request->role;
                $newrole->user_id = $employee->id;
                $newrole->save();
            }


        }
        alert()->success('Roles have been updated.', 'Roles Submitted');
        return redirect('/securityBulk');
    }
}
