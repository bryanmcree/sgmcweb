<?php

namespace App\Http\Controllers;

use App\EmployeeHealthGrits;
use App\History;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Webpatser\Uuid\Uuid;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class EmployeeHealthGritsController extends Controller
{
    public function edit_grits(Requests\SurveyRequest $request){

        //Create current allergies
        //First lets see if there is already a record.
        $grits = EmployeeHealthGrits::where('employee_number','=', $request->employee_number)
            ->first();

        if(is_null($grits)){
            //No record, lets create one
            $request->offsetSet('id', Uuid::generate(4));
            $answers = $request->only('employee_number', 'grits_number','created_by','id');
            EmployeeHealthGrits::create($answers);
        }else {
            //Found one lets update it
            $grits->grits_number = $request->grits_number;
            $grits->save();
        }

        $history = new History();
        $history->id = \Uuid::generate(4);
        $history->action = 'Allergies Updated';
        $history->userid = \Auth::user()->username;
        $history->search_string = 'None';
        $history->user_ip = \Request::ip();
        $history->save();


        alert()->success('GRITS Number Updated!', 'Success!');
        //lets see where we send the user after making a note
        if($request->page_source ==  'index'){
            return redirect('/employee_health/' . $request->employee_number.'#nav-dashboard');
        }

        if($request->page_source ==  'hs'){
            return redirect('/employee_health/hs/' . $request->employee_number.'?hs='.$request->hs_update_id.'#nav-dashboard');
        }
    }
}
