<?php

namespace App\Http\Controllers;

use DB;
use App\Stats;
use App\StatsValues;
use App\StatsView;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class StatValuesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $manuals = Stats::where('calc_field', 0)
            ->where('auto_fill', 0)
            ->orderby('stat_type')
            ->get();

        // foreach($manuals as $man)
        // {
        //     foreach($man->statsValues as $test)
        //     {
        //         var_dump($test->value);
        //     }
        // }

        return view('stats.manual', compact('manuals'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        // dd($request->all());

        $stat = Stats::where('stat_link', $request->stat_link)->first();

        // dd($stat);

        $exists = StatsValues::where('stat_link', $request->stat_link)
            ->where('start_date', $request->start_date)
            ->first();

        // Deletes old value if one exitts, essentially an update function
        if(!empty($exists))
        {
            $exists->delete();
        }

        StatsValues::create($request->all());

        alert()->success('Stat Value Added!', 'Success!');
        return redirect()->back();
    }

    public function recalculate(Request $request)
    {      
        // dd($request->all());

        //----- UNCOMMENT WHEN READY FOR GO LIVE -----//

        $start_date = $request->start_date;
        $end_date = $request->end_date;

        $procedure = DB::insert('EXEC dynamic_stats ?,?', [$start_date, $end_date]);

        // dd($procedure);

        //----- UNCOMMENT WHEN READY FOR GO LIVE -----//

        alert()->success('Stats Recalculated!', 'Success!');
        return back();
    }

    public function varianceExplanation(Request $request)
    {
        $value = StatsValues::where('id', $request->value_id)->first();

        $value->update([
            'variance_explanation' => $request->variance_explanation
        ]);

        alert()->success('Variance Explanation Recorded!', 'Success!');
        return back();
    }

    public function userCreate()
    {
        return view('stats.userCreate');
    }

    public function userStore(Request $request)
    {
        $stat = Stats::where('stat_link', $request->stat_link)->first();
        
        StatsValues::create([
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'stat_name' => $stat->campus . $stat->stat_description,
            'location_code' => 0,
            'stat_category' => $stat->stat_type,
            'value' => $request->value,
            'stat_link' => $request->stat_link
        ]);
        
        alert()->success('Stat Value Recorded!', 'Success!');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {   
        $value = StatsValues::find($request->value_id);

        $value->value = $request->stat_value;

        $value->save();

        alert()->success('Stat Value Updated!', 'Success!');
        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {   
        $value = StatsValues::find($request->value_id);
        $date = $request->report_date;

        $value->stat_value = $request->stat_value;
        $value->report_date = $date;
        $value->report_month = Carbon::parse($date)->format('n');
        $value->report_year = Carbon::parse($date)->format('Y');

        $value->save();

        alert()->success('Stat Value Updated!', 'Success!');
        return redirect()->back();
    }

    public function updateManual(Request $request)
    {
        // dd($request->all());

        $current = StatsValues::where('stat_link', $request->stat_link)->where('start_date', $request->start_date)->first();

        // dd($current);

        if($request->value == '')
        {
            return back();
        }
        elseif(!empty($current))
        {
            $current->update([
                'value' => $request->value
            ]);

            alert()->success('Stat Value Updated!', 'Success!');
            return back();
        }
        else
        {
            StatsValues::create($request->all());

            alert()->success('Stat Value Updated!', 'Success!');
            return back();
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(StatsValues $value)
    {
        $value->delete();
    }
}
