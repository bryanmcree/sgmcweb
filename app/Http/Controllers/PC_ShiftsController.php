<?php

namespace App\Http\Controllers;

use App\History;
use App\pc_positions;
use App\PC_Shifts;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;

class PC_ShiftsController extends Controller
{
    public function add_shift(Requests\SystemListRequest $request){

        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();
        //Input has been created lets save it to the database.
        PC_Shifts::create($input);

        //Lets write this action to the history table.
        $history = new History();
        $history->id = Uuid::generate(4);
        $history->action = 'PC - Add Shift';
        $history->userid = \Auth::user()->username;
        $history->search_string = 'none';
        $history->user_ip = \Request::ip();
        $history->save();

        //Everything is good lets send feedback to the user and then redirect
        alert()->success('Shift Added!', 'Success!');
        return redirect('/pc/' . $request->cost_center);
    }

    public function assign_shift(Requests\SystemListRequest $request){
        //dd($request);
        $position = pc_positions::find($request->position_id);

        $position->shift_id = $request->shift_id;
        $position->save();

        //Everything is good lets send feedback to the user and then redirect
        alert()->success('Position added to shift!', 'Success!');
        return redirect('/pc/' . $request->cost_center);

    }

    public function remove_from_shift($id){

        $position = pc_positions::find($id);

        $position->shift_id = null;
        $position->save();

        return back();
    }

    public function delete_shift($id){

        //Lets find that shift.
        $shift = PC_Shifts::find($id);

        //$shift->shift_id = null;
        $shift->delete();

        return back();
    }

    public function edit_shift(){

        //Lets find that shift.
        $shift = PC_Shifts::find(Input::get('id'));
        $shift->update(Input::except(['id']));

        return back();
    }
}
