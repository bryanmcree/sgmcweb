<?php

namespace App\Http\Controllers;

use App\AxiomJobCodes;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AxiomController extends Controller
{
    public function index(){


        return View('axiom.index', compact('data','user'));
    }

    public function job_codes(){

        //Because this uses a view with linked servers.
        $handle = \DB::connection()->getPdo();
        $handle->exec('SET QUOTED_IDENTIFIER ON');
        $handle->exec('SET ANSI_WARNINGS ON');
        $handle->exec('SET ANSI_PADDING ON');
        $handle->exec('SET ANSI_NULLS ON');
        $handle->exec('SET CONCAT_NULL_YIELDS_NULL ON');

        $job_codes = AxiomJobCodes::all();

        //dd($pay_codes);
        return View('axiom.jobcodes', compact('job_codes','user'));
    }
}
