<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Uuid;
use Auth;
use App\Audit;
use App\AuditAnswer;
use App\AuditQuestion;
use App\AuditQuestionEP;
use App\AuditQuestionStandard;

class AuditQuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order_num = AuditQuestion::where('survey_id', '=', $request->survey_id)->count();
        $request->offsetSet('question_order', $order_num+1);

        $request->offsetSet('id', Uuid::generate(4));

        $data = $request->except(['EP', 'Standard']);

        $eps = $request->EP;
        $standards = $request->Standard;

        $question = AuditQuestion::create($data);

        if(!empty($eps))
        {
            foreach($eps as $ep)
            {
                AuditQuestionEP::create([
                    'id' => Uuid::generate(4),
                    'question_id' => $question->id,
                    'EP' => $ep
                ]);
            }
        }
        
        if(!empty($standards))
        {
            foreach($standards as $standard)
            {
                AuditQuestionStandard::create([
                    'id' => Uuid::generate(4),
                    'question_id' => $question->id,
                    'standard' => $standard
                ]);
            }
        }
        

        alert()->success('Question Created!', 'Success!');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function shortAnswer(Audit $audit, AuditQuestion $question)
    {
        $answers = AuditAnswer::where('question_id', $question->id)
            ->orderby('created_at', 'desc')
            ->get();

        return view('audit.short_results', compact('answers', 'audit', 'question'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function updateOrder(Request $request)
    {
        // allows users to drag and drop the order of the questions.

        $questions = AuditQuestion::where('survey_id', '=', $request->survey_id)->get();
        //dd($questions);

        foreach ($questions as $question) {
            $question->timestamps = false; // To disable update_at field updation
            $id = $question->id;

            foreach ($request->order as $order) {
                if ($order['id'] == $id) {
                    $question->update(['question_order' => $order['position']]);
                }
            }
        }

        return response('Update Successfully.', 200);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(AuditQuestion $question)
    {
        $question->delete();

        $answers = AuditAnswer::where('question_id', $question->id)->get();

        foreach($answers as $answer)
        {
            $answer->delete();
        }
    }
}
