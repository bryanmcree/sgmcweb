<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use App\Payroll;
use Maatwebsite\Excel\Facades\Excel;
use App\History;

class PayrollController extends Controller
{
    public function index()
    {
        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'Viewed Payroll';
        $history->userid = \Auth::user()->username;
        $history->search_string = 'User clicked on a payroll report.';
        $history->save();

        return View('payroll.index');
    }
    
    public function payrollReport(){
        DB::statement('SET ANSI_NULLS ON; SET ANSI_WARNINGS ON');

        $start_date = Input::get('start_date'). ' 00:00:00';
//dd($start_date);

    //    $tests = Payroll::where('EndDate', $start_date)->take(10)->get();
     //

        $tests = Payroll::select('EntityCode', 'GLDepartment', 'JobCode','PayType', 'EmployeeID', 'EmployeeName', 'Hours'
        ,'Dollars', 'PeriodNumber', 'GLAct')->where('EndDate', $start_date)->get()->toArray();

        //dd($tests);
        Excel::create('PayrollReport '.date('m-d-Y_hia'), function($excel) use($tests) {
            $excel->sheet('New sheet', function($sheet) use($tests) {
                $sheet->fromArray($tests);
            })->download('csv');
        });

        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'Downloaded Payroll File';
        $history->userid = \Auth::user()->username;
        $history->search_string = 'User downloaded a payroll report.';
        $history->save();
        //return View('payroll.reports.report', compact('payroll'));

    }
}
