<?php

namespace App\Http\Controllers;

use App\CostCenters;
use App\History;
use App\PR_questions;
use App\PRCheck;
use App\PRFiles;
use App\PRReportSurveybyEmployee;
use App\PRTwo;
use App\PublicRelationsPatients;
use App\ServiceRecovery;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Http\Requests;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

class PRController extends Controller
{
    public function index(){

        $total_patients = PublicRelationsPatients::orderby('created_at','DESC')->get();

        $floors = PublicRelationsPatients::selectRaw('count(*) as total, dept')
            ->wherenull('discharged_yn')
            ->groupBy('dept')
            //->pluck('dept');
            ->get();
        //dd($floors);

        $total_part_one = PRCheck::where('part_one_completed_by','=', \Auth::user()->employee_number)
            ->where('created_at','>=',Carbon::today())
            ->get();

        //dd($total_part_one);

        $total_part_two = PRTwo::where('part_two_completed_by','=', \Auth::user()->employee_number)
            ->where('created_at','>=',Carbon::today())
            ->get();

        //$questions = PR_questions::get();


        $history = new History();
        $history->id = \Uuid::generate(4);
        $history->action = 'Patient Relations Check';
        $history->userid = \Auth::user()->username;
        $history->user_ip = \Request::ip();
        $history->search_string = 'N/A';
        $history->save();

        return view('pr.index', compact('floors','total_patients','total_part_one','total_part_two'));
    }

    public function SelectFloor($floor){

        $patients = PublicRelationsPatients::where('dept','=', $floor)
            ->wherenull('discharged_yn')
            ->orderby('check_back','DESC')
            ->orderby('room')
            ->get();

        $total_part_one = PRCheck::where('part_one_completed_by','=', \Auth::user()->employee_number)
            ->where('created_at','=',Carbon::today())
            ->get();

        $total_part_two = PRTwo::where('part_two_completed_by','=', \Auth::user()->employee_number)
            ->where('created_at','=',Carbon::today())
            ->get();

        //dd($total_part_one);

        return view('pr.floor', compact('patients','total_part_one','total_part_two'));

    }

    public function add_question(Requests\SystemListRequest $request){

        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();
        PR_questions::create($input);

        alert()->success('Question has been added!', 'Success!');

        return redirect('/pr');

    }

    public function survey($encounter){

        $patient = PublicRelationsPatients::where('pat_enc_csn_id','=', $encounter)
            ->first();

        $part_one = PRCheck::where('encounter_id','=', $encounter)
            ->first();

        $part_two = PRTwo::where('encounter_id','=', $encounter)
            ->first();

        $files = PRFiles::where('encounter_id','=', $encounter)
            ->get();

        //dd($patient);
        return view('pr.survey', compact('patient','part_one','part_two','files'));
    }

    public function ServiceRecovery(){

        $cost_centers = CostCenters::orderby('style1')->get();

        return view('pr.service_recovery.index', compact('questions','cost_centers'));
    }

    public function AddServiceRecovery(Requests\SystemListRequest $request){

        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();
        ServiceRecovery::create($input);
        return redirect('/pr/service_recovery/certificate/'. $request->id);
    }

    public function PrintCertificate($id){

        $presented_to = ServiceRecovery::where('id', '=', $id)
            ->first();

        return view('pr.service_recovery.certificate', compact('questions','presented_to'));

    }

    public function NewCheck(Requests\SystemListRequest $request){

        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();
        PRCheck::create($input);

        //Remove Check Back flag

        $check_back = PublicRelationsPatients::where('pat_enc_csn_id','=',$request->encounter_id)
            ->first();
        $check_back->check_back = null;
        $check_back->save();

        return redirect('/pr/floor/'. $request->patient_floor);

    }

    public function two(Requests\SystemListRequest $request){

        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();
        PRTwo::create($input);

        //Remove Check Back flag

        $check_back = PublicRelationsPatients::where('pat_enc_csn_id','=',$request->encounter_id)
            ->first();
        $check_back->check_back = null;
        $check_back->save();

        If($request->recgonize_staff == 1){
            //Send Email About Complements
            Mail::send('pr.email.complement', $input, function ($m) use ($input) {

                $m->from('sgmcweb-no-reply@sgmc.org', 'PR - Compliment');
                $m->cc(Auth::user()->mail);
                $m->to('jessica.mckinney@SGMC.ORG');
                //$m->cc('dana.massingill@SGMC.ORG');
                //$m->cc('bryan.mcree@SGMC.ORG');
                $m->subject('PR - Compliment');
                //$m->CC('sghelpdesk@sgmc.org');
            });
        }


        return redirect('/pr/floor/'. $request->patient_floor);

    }

    public function file_upload(){

        $file = Input::file('file_name');
        $encounter_id = Input::get('encounter_id');
        $extension = $file->getClientOriginalExtension();
        $entry = new PRFiles();
        $entry->id = Uuid::generate(4);
        $entry->file_mime = $file->getClientMimeType();
        $entry->old_filename = $file->getClientOriginalName();
        $entry->new_filename = Uuid::generate(4) .'.'.$extension;
        $entry->encounter_id = $encounter_id;
        $entry->file_extension = $extension;
        $entry->created_by = \Auth::user()->employee_number;
        $entry->file_size = $file->getClientSize();
        $entry->file_description = Input::get('file_description');

        Storage::disk('local')->put($entry->new_filename,  File::get($file));

        $entry->save();
        alert()->success('File has been uploaded.', 'File Uploaded');

        return redirect('/pr/survey/' . $encounter_id);

    }

    public function nir($encounter){

        //dd();
        $patient = PublicRelationsPatients::where('pat_enc_csn_id','=',$encounter)
            ->first();
        //dd($item);
        $patient->gone = 'Y';
        $patient->save();
        //$patient->update(Input::only(['gone']));
        //alert()->success('Item Updated!', 'Success!');
        return redirect('/pr/floor/' . Input::get('floor'));

    }

    public function ir($encounter){

        //dd();
        $patient = PublicRelationsPatients::where('pat_enc_csn_id','=',$encounter)
            ->first();
        //dd($item);
        $patient->gone = Null;
        $patient->save();
        //$patient->update(Input::only(['gone']));
        //alert()->success('Item Updated!', 'Success!');
        return redirect('/pr/floor/' . Input::get('floor'));

    }


    public function swing($encounter){

        //dd();
        $patient = PublicRelationsPatients::where('pat_enc_csn_id','=',$encounter)
            ->first();
        //dd($item);
        $patient->check_back = 1;
        $patient->save();
        //$patient->update(Input::only(['gone']));
        //alert()->success('Item Updated!', 'Success!');
        return redirect('/pr/floor/' . $patient->dept);

    }

    public function dashboard(){


        $patients = PublicRelationsPatients::where('admission', '>=', Carbon::now()->startOfMonth())
            ->get();

        $part_one = PRCheck::where('created_at', '>=', Carbon::now()->startOfMonth())
            ->get();

        $part_two = PRTwo::where('created_at', '>=', Carbon::now()->startOfMonth())
            ->orderby('created_at','desc')
            ->get();


        $by_employee_2 = PRTwo::selectRaw('part_two_completed_by, count(*) as total')
           // ->with('completedBy')
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->groupby('part_two_completed_by')
            ->get();

        $by_employee_1 = PRCheck::selectRaw('part_one_completed_by, count(*) as total')
            // ->with('completedBy')
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->groupby('part_one_completed_by')
            ->get();

        $patients_floor = PublicRelationsPatients::selectRaw('dept, count(*) as total')
            // ->with('completedBy')
            ->where('admission', '>=', Carbon::now()->startOfMonth())
            ->groupby('dept')
            ->get();

        $emp_list = PRReportSurveybyEmployee::select('emp')
            ->wherenotnull('emp')
            ->distinct('emp')
            ->get();

        $all_floors = PublicRelationsPatients::select('dept')
            ->orderby('dept')
            ->distinct()
            ->get();
        //dd($all_floors);


        //dd($by_employee_1);

        return view('pr.dashboard', compact('patients','part_one','part_two','by_employee_2','by_employee_1',
            'patients_floor','emp_list','all_floors'));

    }

    public function filter(){

        $start_date = Input::get('start_date'). ' 00:00:00';
        $end_date = Input::get('end_date'). ' 23:59:59';
        $department = Input::get('department');
        $employee = Input::get('employee');

        //dd($start_date);


        $patients = PublicRelationsPatients::whereBetween('admission', array($start_date, $end_date))
            ->when($department, function ($query) use ($department) {
            return $query->where('dept','=', $department);
        })
            ->get();

        //dd($patients);

        $part_one = PRCheck::whereBetween('created_at', array($start_date, $end_date))
            ->when($department, function ($query) use ($department) {
                return $query->where('patient_floor','=', $department);
            })
            ->get();

        $part_two = PRTwo::whereBetween('created_at', array($start_date, $end_date))
            ->when($department, function ($query) use ($department) {
                return $query->where('patient_floor','=', $department);
            })
            ->orderby('created_at','desc')
            ->get();


        $by_employee_2 = PRTwo::selectRaw('part_two_completed_by, count(*) as total')
            // ->with('completedBy')
            ->when($department, function ($query) use ($department) {
                return $query->where('patient_floor','=', $department);
            })
            ->whereBetween('created_at', array($start_date, $end_date))
            ->groupby('part_two_completed_by')
            ->get();

        $by_employee_1 = PRCheck::selectRaw('part_one_completed_by, count(*) as total')
            // ->with('completedBy')
            ->whereBetween('created_at', array($start_date, $end_date))
            ->groupby('part_one_completed_by')
            ->get();

        $patients_floor = PublicRelationsPatients::selectRaw('dept, count(*) as total')
            ->whereBetween('admission', array($start_date, $end_date))
            ->when($department, function ($query) use ($department) {
                return $query->where('dept','=', $department);
            })
            ->groupby('dept')
            ->get();
        //dd($patients_floor);

        $emp_list = PRReportSurveybyEmployee::select('emp')
            ->wherenotnull('emp')
            ->distinct('emp')
            ->get();

        $all_floors = PublicRelationsPatients::select('dept')
            ->orderby('dept')
            ->distinct()
            ->get();
        //dd($all_floors);


        //dd($by_employee_1);

        return view('pr.dashboard', compact('patients','part_one','part_two','by_employee_2','by_employee_1',
            'patients_floor','emp_list','all_floors'));

    }

    public function surveysbyemployeebyfloor(){

        $chart = PRReportSurveybyEmployee::selectRaw('emp, floor, count(*) as total')
            ->where('emp','=',Input::get('employee'))
            ->groupby('emp')
            ->groupby('floor')
            ->orderby('emp')
            ->get();

        //dd($chart);

        return view('pr.reports.surveysbyfloorbyemployee', compact('chart'));
    }
}
