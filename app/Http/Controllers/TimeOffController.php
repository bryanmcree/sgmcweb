<?php

namespace App\Http\Controllers;

use App\TimeOffEmails;
use App\TimeOffRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests;
use Webpatser\Uuid\Uuid;

class TimeOffController extends Controller
{
    public function index()
    {

        return View('time-off.index', compact('ActiveRequest','transporters'));
    }

    public function add(Requests\SystemListRequest $request){


        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();
        TimeOffRequest::create($input);
        alert()->success('Your request has been received', 'Success!')->persistent();

        //Lets send emails

        $send_emails = TimeOffEmails::where('cost_center','=', $request->cost_center)
            ->pluck('emails')->toArray();

        $request_detail = TimeOffRequest::where('id','=', $request->id)
            ->with('requested_by')
            ->first()
            ->toArray();

        //dd($request_detail);

        //Mail::raw('Message here', function ($message) use ($send_emails, $request_detail){
            //$message->to($send_emails);
        //    $message->to('bryan.mcree@sgmc.org');
        //    $message->subject('Time Off Request');
        //});

        Mail::send('time-off.email', $request_detail, function ($m) use ($request_detail, $send_emails) {
            $m->from('sgmcweb-no-reply@sgmc.org', 'Web.SGMC.org');
            $m->to($send_emails);
            //$m->to('bryan.mcree@sgmc.org');
            $m->subject('Time Off Request');
            //$m->CC('sghelpdesk@sgmc.org');
            //$m->CC($data['mail']);
            //$m->replyTo($data['mail']);
        });


        return View('time-off.index', compact('ActiveRequest','transporters'));
    }
}
