<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;

class UploadController extends Controller
{
    public function index(){

        return View('upload.index', compact('daily','chart_lables','ed_running_adv','ed_monthly_adv'));

    }

    public function upload(){

        $file = Input::file('file_name');
        Storage::disk('local')->put('/public/email/'.$file->getClientOriginalName(), File::get($file));

        //$entry->save();
        alert()->success('File has been uploaded.', 'File Uploaded');

        return redirect('/home');
    }

}
