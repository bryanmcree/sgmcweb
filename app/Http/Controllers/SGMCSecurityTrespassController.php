<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Webpatser\Uuid\Uuid;

use App\SecurityBadges;
use App\SecurityCitation;
use App\SecurityActivityList;
use App\SecurityLocationList;
use App\SecurityDispatch;
use App\SecurityRounds;
use App\SecurityViolation;
use App\SecurityIncident;
use App\SecurityIncidentList;
use App\SecurityTrespass;
use App\User;
use App\History;
use Auth;
use Carbon;
use DB;


class SGMCSecurityTrespassController extends Controller
{
    public function index()
    {
        $locations = SecurityLocationList::wherenotnull('id')->orderby('location')->get();

        $trespassing = SecurityTrespass::where('created_at', '>=', Carbon::now()->subMonths(3))->orderby('date_time', 'desc')->get();

        return view('sgmcsecurity.trespass', compact('locations', 'trespassing'));
    }

    public function store(Request $request)
    {
        $request->offsetSet('id', Uuid::generate(4));
        $request->offsetSet('date_time', Carbon::parse($request->date_time)->format('Y-m-d H:i:s'));

        SecurityTrespass::create($request->all());

        alert()->success('Trespassing Recorded!', 'Success!');
        return redirect()->back();
    }

    public function show(SecurityTrespass $trespass)
    {
        return view('sgmcsecurity.show.trespass', compact('trespass'));
    }

    public function edit(SecurityTrespass $trespass)
    {
        $locations = SecurityLocationList::wherenotnull('id')->orderby('location')->get();

        return view('sgmcsecurity.edit.trespass', compact('trespass', 'locations'));
    }

    public function update(Request $request, SecurityTrespass $trespass)
    {
        $request->offsetSet('date_time', Carbon::parse($request->date_time)->format('Y-m-d H:i:s'));

        $new = $request->all();

        $trespass->update($new);

        alert()->success('Trespassing Updated!', 'Success!');
        return redirect()->back();
    }

    public function destroy(SecurityTrespass $trespass)
    {
        $trespass->delete();
    }
}
