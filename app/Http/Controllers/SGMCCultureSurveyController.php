<?php

namespace App\Http\Controllers;

use App\CostCenters;
use App\SGMCCultureSurvey;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SGMCCultureSurveyController extends Controller
{
    public function index()
    {
        $costcenters = CostCenters::wherenotnull('style1')
            ->get();
        return view('SGMCCultureSurvey.index', compact('costcenters'));
    }

    public function submit(Request $request){

        $input = $request->all();
        SGMCCultureSurvey::create($input);

        alert()->success('Feedback Received!', 'Success!');
        return redirect('/SGMCCultureSurvey');
    }
}
