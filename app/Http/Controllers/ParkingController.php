<?php

namespace App\Http\Controllers;

use App\Parking;
use Illuminate\Http\Request;

use App\Http\Requests;

use Webpatser\Uuid\Uuid;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;

class ParkingController extends Controller
{
    public function index(){

        $tickets = Parking::wherenotnull('id')
            ->get();

        return view('parking.index', compact('tickets'));

    }

    public function SubmitTicket(Requests\SystemListRequest $request){

        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();
        Parking::create($input);

        alert()->success('Ticket Recorded!', 'Success!');

        return redirect('/parking');

    }


}
