<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Carbon;
use App\Event;
use App\EventSchedule;
use App\EventScheduleReserve;
use App\EventScheduleTime;
use App\History;
use App\Http\Requests;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class EventScheduleController extends Controller
{
    public function store(Request $request)
    {
        $event = Event::find($request->event_id);

        $start = new Carbon($request->start_time);
        $end = new Carbon($request->end_time);
        $range = $end->diffInMinutes($start);

        if($event->multiple == 1)
        {
            $interval = $range;
        }
        else
        {
            $interval = $request->interval;
        }

        $slots = $range / $interval;
        $begin = $request->start_time;

        $schedule = EventSchedule::create([
            'id' => Uuid::generate(4),
            'event_id' => $request->event_id,
            'start_time' => $request->start_time,
            'end_time' => $request->end_time,
            'interval' => $interval
        ]);

        //Maths
        for($c = 0; $c < $slots; $c++)
        {
            $ending = $start->addMinutes($interval)->format('H:i');

            EventScheduleTime::create([
                'id' => Uuid::generate(4),
                'schedule_id' => $schedule->id,
                'time_slot' => $begin . ' - ' . $ending,
                'reserved' => 0,
                'isComplete' => 0,
                'isCancelled' => 0
            ]);

            $begin = $ending;
        }

        alert()->success('Schedule Created', 'Success');
        return redirect()->back();
    }

    public function reserve(Request $request, EventScheduleTime $reserve)
    {
        $reserve->update([
            'reserved' => 1,
            'reserved_by' => $request->reserved_by
        ]);

        $cost_center = $request->cost_center;
        
        // $current_registered = EventScheduleReserve::where('event_id', $request->event_id)->distinct()->get(['reserved_by'])->count();
        // $event = Event::where('id', $request->event_id)->first();
        // dd($current_registered);
        
        foreach($cost_center as $cc)
        {
            EventScheduleReserve::create([
                'id' => Uuid::generate(4),
                'event_id' => $request->event_id,
                'schedule_id' => $request->schedule_id,
                'time_id' => $reserve->id,
                'cost_center' => $cc,
                'reserved_by' =>$request->reserved_by
            ]);
        }

        alert()->success('Time Reserved!', 'Success');
        return redirect()->back();

        // MAIL && REMINDER
        // $event = Event::where('id', '=', $request->event_id)->first();
        // $times = EventSchedule::where('id', '=', $request->schedule_id)->first();

        // $startDate = Carbon::parse($event->start_date)->format('Y-m-d');
        // $start = Carbon::parse($times->start_time)->format('H:i:s');
        // $end = Carbon::parse($times->end_time)->format('H:i:s');

        // $formated_start = $startDate . ' ' . $start;
        // $formated_end = $startDate . ' ' . $end;

        // $startTime = Carbon::createFromFormat('Y-m-d H:i:s', $formated_start)->toDateTimeString();
        // $endTime = Carbon::createFromFormat('Y-m-d H:i:s', $formated_end)->toDateTimeString();

        //"2017-12-19 16:43:31"
        //dd(Carbon::now()->toDateTimeString());
        
        // dd($startTime, $endTime);

        //$date = Carbon::now()->toDateTimeString();

        //dd($date);
        // $date = $startTime;
        // $content = $event->name;
        // $subject = 'Event Reservation Reminder';

        // $dateTimeArr = explode(' ', $date);
        // $newDate = $dateTimeArr[0];
        // $dateArr = explode('-', $newDate);
        // $newDate = $dateArr[0] . $dateArr[1] . $dateArr[2] . '-' . $dateTimeArr[1];
        // $Startdate = substr($newDate, 0, -9);

        // //dd($date);

        // $startTime = str_replace(":", "", substr($newDate, 9, -3));


        // $dateTimeArr = explode(' ', $endTime);
        // $newDate = $dateTimeArr[0];
        // $dateArr = explode('-', $newDate);
        // $newDate = $dateArr[0] . $dateArr[1] . $dateArr[2] . '-' . $dateTimeArr[1];
        // $Startdate = substr($newDate, 0, -9);
        // $EndTime = str_replace(":", "", substr($newDate, 9, -3));

        // //$endTime = '1530';
        //     //dd($EndTime);

        // $strip = str_replace("\r", " ", strip_tags('This is a reminder for your reserved event meeting.'));
        // // //$desc1 = str_replace("&#10;", "\n", $request->comments);
        //     $desc = str_replace("\n", "", $strip);
        //     //$desc = $request->comments;

        //     //dd($desc);

        // //dd($desc);
        // //dd("DTSTART;TZID=\"Eastern Standard Time\":" . $Startdate . "T" . $startTime . "00Z");

        // // ICS
        // $mail[0] = "BEGIN:VCALENDAR";
        // $mail[1] = "PRODID:-//Microsoft Corporation//Outlook 16.0 MIMEDIR//EN";
        // $mail[2] = "VERSION:2.0";
        // $mail[3] = "METHOD:PUBLISH";
        // $mail[4] = "X-MS-OLK-FORCEINSPECTOROPEN:TRUE";
        // $mail[5] = "BEGIN:VTIMEZONE"; // Start VTimezone
        // $mail[6] = "TZID:Eastern Standard Time";
        // $mail[7] = "BEGIN:STANDARD";  //BEGIN Standard
        // $mail[8] = "DTSTART:" . $Startdate . "T" . $startTime . "00Z";
        // $mail[9] = "RRULE:FREQ=YEARLY;BYDAY=1SU;BYMONTH=11";
        // $mail[10] = "TZOFFSETFROM:-0400";
        // $mail[11] = "TZOFFSETTO:-0000";
        // $mail[12] = "END:STANDARD";  //END Standard
        // $mail[13] = "BEGIN:DAYLIGHT";  //Begin Daylight
        // $mail[14] = "DTSTART:" . $Startdate . "T" . $startTime . "00Z";
        // $mail[15] = "RRULE:FREQ=YEARLY;BYDAY=2SU;BYMONTH=3";
        // $mail[16] = "TZOFFSETFROM:-0000";
        // $mail[17] = "TZOFFSETTO:-0400";
        // $mail[18] = "END:DAYLIGHT";  //END daylight
        // $mail[19] = "END:VTIMEZONE"; //END VTIMEZone
        // $mail[20] = "BEGIN:VEVENT";  //Start the event
        // $mail[21] = "ATTENDEE;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;RSVP=TRUE;ORGANIZER;CN=McRee, Bryan E., Test:MAILTO:bryan.mcree@sgmc.org";  //The person who made the reqeust
        // $mail[22] = "CLASS:PUBLIC";
        // $mail[23] = "DESCRIPTION:". $desc;
        // $mail[24] = "DTEND;TZID=\"Eastern Standard Time\":" . $Startdate . "T" . $EndTime . "00Z";
        // $mail[25] = "DTSTART;TZID=\"Eastern Standard Time\":" . $Startdate . "T" . $startTime . "00Z";
        // $mail[26] = "DTSTAMP:" . gmdate('Ymd') . 'T' . gmdate('His') . "Z";
        // $mail[27] = "LOCATION: Not Specified";
        // $mail[28] = "ORGANIZER;CN=\"No, Reply\":mailto:no.reply@SGMC.ORG";
        // $mail[29] = "PRIORITY:1";
        // $mail[30] = "SEQUENCE:0";
        // $mail[31] = "SUMMARY;LANGUAGE=en-us:DOR Reminder";
        // $mail[32] = "TRANSP:OPAQUE";
        // $mail[33] = "BEGIN:VALARM";
        // $mail[34] = "TRIGGER:-PT15M";
        // $mail[35] = "ACTION:DISPLAY";
        // $mail[37] = "END:VALARM";
        // $mail[38] = "END:VEVENT";
        // $mail[39] = "END:VCALENDAR";
        // $mail[36] = "X-MICROSOFT-CDO-BUSYSTATUS:FREE";
        // //$mail[37] = "X-MICROSOFT-CDO-INTENDEDSTATUS:FREE";  //Meeting Organizer
        // // $mail[38] = "";
        // // $mail[39] = "";

        // //set correct content-type-header
        // $filename = $subject . '.ics';
        // $mail = implode("\r\n", $mail);

        // header("text/calendar");

        // Storage::put('public/events/'.$filename, $mail);

        // //Lets an email to the requester notifying them of the approval.
        // $data = array('name'=>"Virat Gandhi");

        // Mail::raw('See the attached reminder and add it to your calendar', function($message) {
        //     $message->to(Auth::user()->mail, 'Event Reservation Reminder')->subject
        //     ('See the attached reminder and add it to your calendar.');
        //     $message->from('no.reply@sgmc.org','Web.SGMC.org');
        //     $message->attach(storage_path('app/public/public/events/Event Reservation Reminder.ics'));
        // });

        
    }

    public function cancel(EventScheduleTime $reserve)
    {
        $reserve->update([
            'isCancelled' => 1,
            'reserved' => 0,
            'reserved_by' => NULL,
            'notes' => NULL
        ]); // need to loop to remove reserved ccs 

        $cost_centers = EventScheduleReserve::where('time_id', '=', $reserve->id)
            ->where('reserved_by', '=', Auth::user()->employee_number)
            ->get();
        
        foreach($cost_centers as $cc)
        {
            $cc->delete();
        }

        alert()->success('Reservation Cancelled!', 'Success');
        return redirect()->back();
    }

    public function complete(EventScheduleTime $reserve)
    {
        if($reserve->isComplete == 0)
        {
            $reserve->update([
                'isComplete' => 1
            ]);

            alert()->success('Meeting Completed!', 'Success');
        }
        else
        {
            $reserve->update([
                'isComplete' => 0
            ]);

            alert()->success('Meeting Marked Incomplete!', 'Success');
        }

        return redirect()->back();
    }

    public function notes(Request $request)
    {
        $time_slot = EventScheduleTime::find($request->id);

        $time_slot->update([
            'notes' => $request->notes
        ]);

        alert()->success('Notes Added!', 'Success');
        return redirect()->back();
    }

}
