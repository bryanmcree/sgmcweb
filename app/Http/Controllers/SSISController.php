<?php

namespace App\Http\Controllers;

use App\History;
use App\SSIS;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use App\Http\Requests;

class SSISController extends Controller
{
    public function index(){

        //Lets get all the records for just TODAY.
        $daily = SSIS::where('run_date','>=', \Carbon\Carbon::today())
            ->orderBy('job_name')
            ->get();
        //dd($daily);

        $monthly = SSIS::where('run_date','>=', \Carbon\Carbon::now()->startOfMonth())
            ->orderBy('run_date','DESC')
            ->get();

        $history = new History();
        $history->id = \Uuid::generate(4);
        $history->action = 'View SSIS';
        $history->userid = \Auth::user()->username;
        $history->user_ip = \Request::ip();
        $history->search_string = 'N/A';
        $history->save();

        return View('ssis.index', compact('daily','monthly'));
    }
}
