<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\History;
use Carbon\Carbon;
use App\PIVariance;
use App\CostCenters;
use App\Http\Requests;
use App\PIVarianceData;
use Webpatser\Uuid\Uuid;

class VarianceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $variances = PIVariance::wherenotnull('id')
            ->orderBy('created_at', 'desc')
            ->get();

        $costcenters = CostCenters::wherenotnull('id')
            ->where('active_status', '=', '1')
            ->orderby('cost_center')
            ->get();


        $monthFTEVar = DB::table('pi_variance')
            ->where('pay_end', '>', Carbon::now()->subMonths(1))
            ->avg('curr_paid_FTE_var');
        

        //dd($last7FTEVar);

        return view('variance.index', compact('variances', 'costcenters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $auto = PIVarianceData::wherenotnull('id')
            ->orderby('pay_period', 'asc')
            ->get();

        $costcenters = PIVarianceData::select('cost_center')
            ->distinct()
            ->get();

        return view('variance.create', compact('costcenters', 'auto'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->offsetSet('id', Uuid::generate(4));
        
        // dd($request->all(), $cc);

        PIVariance::create($request->all());

        alert()->success('Productivity Index Variance Explanation Submitted', 'Success!');
        return redirect('/variance/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(PIVariance $var)
    {
        return view('variance.show', compact('var'));
    }

    public function print(PIVariance $var)
    {
        return view('variance.print', compact('var'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PIVariance $var)
    {
        $var->delete();
    }
}
