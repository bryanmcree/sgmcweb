<?php

namespace App\Http\Controllers;

use App\Employee;
use App\RequestNew;
use App\Servers;
use App\Verify;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\RoleUser;
use App\User;
use React\Promise\SimpleFulfilledTestThenable;
use Webpatser\Uuid\Uuid;
use Image;
use DB;
use App\State;
use App\Role;
use App\Http\Controllers\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use App\SystemList;
use App\SystemsContacts;
use App\SystemsVendors;
use Maatwebsite\Excel\Facades\Excel;
use Alert;
use App\Productivity;
use App\SystemsGroup;
use App\CostCenters;
use App\History;
use App\SystemListTerms;

class SystemListController extends Controller
{
    public function index()
    {
        $SystemList = SystemList::with('systemContacts', 'systemServers','systemVerification','systemVendors')
            ->wherenotnull('id')
            ->orderBy('sys_name')
            ->get();
        return View('systemlist.index', compact('SystemList'));
    }

    public function create()
    {
        return view('systemlist.create', compact('states'));
    }

    public function store(Requests\SystemListRequest $request)
    {
        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();
        SystemList::create($input);
        alert()->success('New system has been created!', 'Success!');


        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'New System Added';
        $history->userid = \Auth::user()->username;
        $history->search_string = $request->sys_name;
        $history->user_ip = \Request::ip();
        $history->save();
        return redirect('/systemlist/'.$request->id);
    }

    public function show($id)
    {
        $SystemList = SystemList::with('systemContacts', 'systemServers','systemVendors')->where('id', $id)->first();
        return view('systemlist.show', compact('SystemList'));
    }

    public function edit($id)
    {
        $SystemList = SystemList::find($id);
        return View('systemlist.edit', compact('SystemList'));
    }

    public function AddContact(Requests\SystemListRequest $request)
    {
        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();
        SystemsContacts::create($input);
        alert()->success('System Contact has been Added!', 'Success!');

        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'New System Contact Added';
        $history->userid = \Auth::user()->username;
        $history->search_string = $request->last_name .', '.$request->first_name;
        $history->user_ip = \Request::ip();
        $history->save();

        return redirect('/systemlist/' . $request->systems_id);
    }


    public function AddServer(Requests\SystemListRequest $request)
    {
        $request->offsetSet('srv_id', Uuid::generate(4));
        $input = $request->all();
        Servers::create($input);
        alert()->success('Servers / Devices / Equipment has been Added!', 'Success!');

        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'New System Server Added';
        $history->userid = \Auth::user()->username;
        $history->search_string = $request->srv_name;
        $history->user_ip = \Request::ip();
        $history->save();

        return redirect('/systemlist/' . $request->sys_id);
    }


    public function AddVendor(Requests\SystemListRequest $request)
    {
        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();
        SystemsVendors::create($input);
        alert()->success('Vendor has been Added!', 'Success!');

        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'New System Venodor Contact Added';
        $history->userid = \Auth::user()->username;
        $history->search_string = $request->last_name .', '.$request->first_name;
        $history->user_ip = \Request::ip();
        $history->save();

        return redirect('/systemlist/' . $request->systems_id);
    }

    public function UpdateContact()
    {
        $SystemsContacts = SystemsContacts::find( Input::get('id') );
        $SystemsContacts->update(Input::except(['systems_id']));
        alert()->success('System Contact has been Updated!', 'Success!');
        return redirect('/systemlist/' . Input::get('systems_id'));
    }

    public function UpdateVendor()
    {
        $SystemsVendors = SystemsVendors::find( Input::get('id') );
        $SystemsVendors->update(Input::except(['systems_id']));
        alert()->success('Vendor Contact has been Updated!', 'Success!');
        return redirect('/systemlist/' . Input::get('systems_id'));
    }

    public function UpdateContact2($cost_center)
    {
        $SystemsContacts = SystemsContacts::find( Input::get('id') );
        $SystemsContacts->update(Input::except(['systems_id']));
        alert()->success('System Contact has been Updated!', 'Success!');

        return redirect('/systemlist/inventory/systems?unit='.$cost_center);
        //return redirect('/systemlist/' . Input::get('systems_id'));
    }
    
    public function DeleteContact($id)
    {
        $SystemsContacts = SystemsContacts::find( $id );
        $SystemsContacts->delete();
        $systems_id = Input::get('system_id');
        return redirect('/systemlist/' . $systems_id);
    }
    
    public function DeleteVendor($id)
    {
        $SystemsVendors = SystemsVendors::find( $id );
        $SystemsVendors->delete();
        $systems_id = Input::get('system_id');
        return redirect('/systemlist/' . $systems_id);
    }

    public function update(Requests\SystemListRequest $request, $id)
    {
        $SystemList = SystemList::find($id);
        if($request->disable_sys == 'YES' )
        {$request->offsetSet('deleted_at', Carbon::now()); }

        $SystemList->update($request->except('disable_sys'));
        alert()->success('System has been updated!', 'Success!');

        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'System Updated';
        $history->userid = \Auth::user()->username;
        $history->search_string = $request->sys_name;
        $history->user_ip = \Request::ip();
        $history->save();
        if($request->disable_sys == 'YES')
        {alert()->success('System has been DISABLED!  It will no longer be visiable.', 'Success!');
            return redirect('/systemlist/');

        }else{
            alert()->success('System has been updated!', 'Success!');
            return redirect('/systemlist/'.$id);
        }

    }

    public function delete($id)
    {
        $SystemList = SystemList::find( $id );
        $SystemList->delete();

        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'System Deleted';
        $history->userid = \Auth::user()->username;
        $history->search_string = $SystemList->sys_name;
        $history->user_ip = \Request::ip();
        $history->save();

        return redirect('/systemlist')->with('msg', 'System has been deleted.');
    }

    public static function printList()
    {
        //$SystemLists->systemContacts2()->count();

        $tests = SystemList::select('id','sys_name','system_priority')->get()->toArray();
        Excel::create('SGMC_SystemList '.date('m-d-Y_hia'), function($excel) use($tests) {
            $excel->sheet('New sheet', function($sheet) use($tests) {
                $sheet->fromArray($tests);
            })->download('xlsx');
        });
    }

    public function inventoryIndex()
    {
        $OrgUnit = CostCenters::orderby('style1')->pluck('style1', 'cost_center');
        $grouphistory = SystemsGroup::select(DB::raw('count(*) as totalsystems, cost_center, groupname, max(created_at) as created_at, max(addedby) as addedby'))
            ->groupBy('cost_center','groupname')
            ->orderby('created_at','DESC')
            ->get();

        //DD($grouphistory);
        return View('systemlist.inventory.index', compact('OrgUnit','grouphistory'));
    }

    public function inventorySystems()
    {
            //dd('not a unique one');
        if(Input::get('unit') == ''){
            alert()->error('Please select from the drop down list.', 'Ops!');
            return \Redirect::to('systemlist/inventory');
        }else{

            $unit = Input::get('unit');
            $cost_center = CostCenters::where('cost_center', '=',$unit )->first();
            //dd($cost_center);
            $CostCenterSystems = SystemsGroup::with('groupSystems')
                ->where('cost_center', '=', $unit)
                ->get();
            //dd($CostCenterSystems);

            $SystemList = SystemList::with('systemContacts', 'systemServers')
                ->where('inv_visiable','=', Null)
                ->whereNotIn('id', $CostCenterSystems->pluck('systems_id') )
                ->orderBy('sys_name')
                ->get();
            //dd($SystemList);
            return View('systemlist.inventory.systems', compact('cost_center','SystemList','unit_description','CostCenterSystems'));

        }

    }

    public function add2Group(Requests\SystemListRequest $request)
    {

        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();

        if (SystemsGroup::where('cost_center', '=', Input::get('cost_center'))
        ->where('systems_id', '=', Input::get('systems_id'))->exists()) {
            alert()->error('System has already been added to the group!', 'oops!')->persistent("Got it?");
            return redirect('/systemlist/inventory/systems?unit='.$request->cost_center);
        }else{

            SystemsGroup::create($input);
            alert()->success('System added to cost center!', 'Success!');
            return redirect('/systemlist/inventory/systems?unit='.$request->cost_center);
        }

    }

    public function deletefromGroup($id)
    {
        $GroupSystem = SystemsGroup::find( $id );
        $cost_center = Input::get('cost_center');
        $GroupSystem->delete();
        return redirect('/systemlist/inventory/systems?unit='.$cost_center);
    }

    public function storeSystem(Requests\SystemListRequest $request, $cost_center)
    {

        $request->offsetSet('id', Uuid::generate(4));
        $request->offsetSet('added_by', \Auth::user()->name);
        $input = $request->except(['groupname']);
        SystemList::create($input);

        $server = new SystemsGroup;
            $server->id = Uuid::generate(4);
            $server->systems_id = $request->id;
            $server->addedby = \Auth::user()->name;
            $server->groupname = $request->groupname;
            $server->cost_center = $cost_center;
            $server->contact = 0;
        $server->save();

        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'New System Added - System Group Page';
        $history->userid = \Auth::user()->username;
        $history->search_string = $request->sys_name;
        $history->user_ip = \Request::ip();
        $history->save();


        alert()->success('New system has been created!', 'Success!');

        return redirect('/systemlist/inventory/systems?unit='.$cost_center);

    }

    public function AddContact2(Requests\SystemListRequest $request, $cost_center)
    {

        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->except(['group_id']);;
        //dd($input);
        SystemsContacts::create($input);

        $updategroup = SystemsGroup::where('id', '=', $request->group_id)->first();
        //dd($updategroup);
        $updategroup->contact = 1;
        $updategroup->save();

        alert()->success('System Contact has been Added!', 'Success!');
        return redirect('/systemlist/inventory/systems?unit='.$cost_center);
    }

    public function storeNew($request)
    {
        dd('you got this far!');
        $request->offsetSet('id', Uuid::generate(4));
        //var_dump($request['id']);
        //dd($request);
        $input = $request->all();
        User::create($input);
        alert()->success('Your SGMC user has been added.  Please remember to only use Google Chrome!  You can now login.', 'Done!')->persistent("Got it?");
        //return redirect('/user')->with('msg', 'User has been added.');
    }

    public function finished()
    {
        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'System Group Complete';
        $history->userid = \Auth::user()->username;
        $history->search_string = 'User clicked finshed button on system group add';
        $history->user_ip = \Request::ip();
        $history->save();

        alert()->success('All of your systems and group information has been saved.  You are welcome to come back and edit any of your settings.', 'Saved!')->persistent("Got it?");
        return redirect('/systemlist/inventory');
    }

    public function searchcostcenter()
    {

        $data = CostCenters::select("cost_center","style1")
            ->where('style1', 'LIKE','%' . Input::get('query') . '%')
            ->get();
        //var_dump($data);

        foreach ($data as $key => $row) {

            $results['suggestions'][] = array('value' => $row['style1'],'data' => $row['cost_center']);
        }

        return \Response::json($results);
    }

    public function searchad()
    {

        $data = Employee::select('first_name', 'last_name','email')
            ->where('Status','=','Active')
            ->where('email', 'LIKE','%' . Input::get('query') . '%')
            //->where('first_name', 'LIKE','%' . Input::get('query') . '%')
            ->get();


        //var_dump(json_encode($data));
        //dd($data);

        foreach ($data as $key => $row) {
           $results['suggestions'][] = array('value' => $row['last_name'].', '.$row['first_name'],'data' => $row['email']);
        }

        return \Response::json($results);

    }

    // public function searchSystemlistContacts()
    // {

    //     $data = SystemsContacts::distinct()
    //         ->select('first_name','last_name','title','department','contact_type','office_phone','mobile_phone','email_address')
    //         ->where('first_name', 'LIKE','%' . Input::get('query') . '%')
    //         ->Orwhere('last_name', 'LIKE','%' . Input::get('query') . '%')
    //         ->get();
    //     //var_dump($data);

    //     foreach ($data as $key => $row) {

    //         $results['suggestions'][] = array('value' => $row['last_name'].', '.$row['first_name'],
    //             'first_name' => $row['first_name'],
    //             'last_name' => $row['last_name'],
    //             'title' => $row['title'],
    //             'department' => $row['department'],
    //             'contact_type' => $row['contact_type'],
    //             'office_phone' => $row['office_phone'],
    //             'mobile_phone' => $row['mobile_phone'],
    //             'email_address' => $row['email_address']

    //         );
    //     }

    //     return \Response::json($results);
    // }

    public function searchSystemlistContacts()
    {

        $data = User::distinct()
            ->select('first_name','last_name','mail', 'title', 'unit_code_description')
            ->where('first_name', 'LIKE','%' . Input::get('query') . '%')
            ->Orwhere('last_name', 'LIKE','%' . Input::get('query') . '%')
            ->get();
        //var_dump($data);

        foreach ($data as $key => $row) {

            $results['suggestions'][] = array('value' => $row['last_name'].', '.$row['first_name'],
                'first_name' => $row['first_name'],
                'last_name' => $row['last_name'],
                'title' => $row['title'],
                'unit_code_description' => $row['unit_code_description'],
                'mail' => $row['mail']

            );
        }

        return \Response::json($results);
    }

    public function UpdateServer()
    {
        $servers = Servers::find( Input::get('srv_id') );
        //dd(Input::all());
        $servers->update(Input::except(['systems_id']));
        //dd($servers);
        alert()->success('Servers / Devices / Equipment has been Updated!', 'Success!');

        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'System Server Updated';
        $history->userid = \Auth::user()->username;
        $history->search_string = Input::get('srv_name');
        $history->user_ip = \Request::ip();
        $history->save();

        return redirect('/systemlist/' . Input::get('systems_id'));
    }

    public function sendEmail(){
        //Send notification to each system contact that it is time to verify their system.

        $data = $request->all();

        Mail::send('email.helpRequest', $data, function ($m) use ($data) {

            $m->from('sgmcweb-no-reply@web.sgmc.org', 'Help Request');
            $m->to('eddy.west@sgmc.org');
            $m->subject('web.sgmc.org HELP Request!');
            //$m->CC('sghelpdesk@sgmc.org');
            $m->CC($data['mail']);
            $m->replyTo($data['mail']);
        });
    }


    public function systemsVerify(Requests\SystemListRequest $request){

        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();
        Verify::create($input);
        alert()->success('System has been verified!', 'Success!');
        return redirect('/systemlist/'.$request->systems_id);

    }

    public function audit()
    {
        $request = SystemLIstTerms::All();
        return View('systemlist.audit', compact('request'));

    }

    public function systemSearch()
    {
        $data = RequestNew::where('system_name', '=',Input::get('system_name'))->get();
        //dd($data);
        return view('systemlist.auditresults', compact('data'));
    }


}
