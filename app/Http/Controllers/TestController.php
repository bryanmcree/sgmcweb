<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Test;
use Illuminate\Support\Facades\Input;
use DB;


class TestController extends Controller
{
    public function index()
    {
        if(Input::has('search')) {
            $query = Input::get('search');
            $tests = DB::table('McRee_test')->where('first_name', 'LIKE', '%' . $query . '%')
                ->orWhere('last_name', 'LIKE', '%' . $query . '%')
                ->orWhere('emp_number', 'LIKE', '%' . $query . '%')
                ->orWhere('unit_code', 'LIKE', '%' . $query . '%')->paginate(25);
        }Else{
            //DD($request);
            $tests = Test::orderBy('last_name', 'asc')->paginate(25);
        }
        return View('welcome', compact('tests'));
        //return view('people.index');
    }
}
