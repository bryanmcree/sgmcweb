<?php

namespace App\Http\Controllers;

use App\EmployeeHealthAllergies;
use App\EmployeeHealthBloodPressure;
use App\EmployeeHealthGrits;
use App\EmployeeHealthNotes;
use App\EmployeeHealthScreens;
use App\EmployeeHealthWeight;
use App\EmployeeHealthDueDate;
use App\EmployeeHealthMedicalAlert;
use App\EmployeeHealthTST;
use App\History;
use App\User;
use Illuminate\Support\Facades\Input;
use Webpatser\Uuid\Uuid;
use App\Http\Requests;
use Carbon\Carbon;

class EmployeeHealthController extends Controller
{

    public function index($id){

        $employee = User::where('employee_number','=', $id)
            ->first();

        $allergies = EmployeeHealthAllergies::where('employee_number','=', $id)
            ->first();

        $screens = EmployeeHealthScreens::where('employee_number','=', $id)
            ->take(4)
            ->orderby('created_at','DESC')
            ->get();

        $next_screen = EmployeeHealthDueDate::where('user_id', '=', $id)
            ->first();

        $medAlerts = EmployeeHealthMedicalAlert::where('user_id', '=', $id)
            ->get();

        $TSThistory = EmployeeHealthTST::where('user_id', '=', $id)
            ->get();

        if(! $screens->isEmpty())
        {
            $findNext = $screens->first()->created_at;

            $next = new Carbon($findNext);

            $next->addYear();
        }

        //Blood pressure chart
        $bps = EmployeeHealthBloodPressure::select('systolic','distolic','created_at')
            ->where('employee_number','=', $id)
            ->orderby('created_at')
            ->get();

        //weight chart
        $weight = EmployeeHealthWeight::select('weight','created_at')
            ->where('employee_number','=', $id)
            ->orderby('created_at')
            ->get();

        //Notes
        $notes = EmployeeHealthNotes::where('employee_number','=', $id)
            ->orderby('created_at','desc')
            ->get();

        //GRITS
        $grits = EmployeeHealthGrits::where('employee_number','=', $id)
            ->first();

        //recent patients
        $recent = History::where('action','=','Employee Health')
            ->take(15)
            ->get();


        $history = new History();
        $history->id = \Uuid::generate(4);
        $history->action = 'Employee Health';
        $history->userid = \Auth::user()->username;
        $history->search_string = $id;
        $history->user_ip = \Request::ip();
        $history->save();

        return View('employee_health.index', compact('employee','allergies','screens','bps','weight',
            'notes','grits','recent', 'next_screen', 'next', 'medAlerts', 'TSThistory'));
    }

    public function new_screen(Requests\SurveyRequest $request){

        //Created new health screen
        $request->offsetSet('id', Uuid::generate(4));
        $answers = $request->only('employee_number','work_related','non_work_related','tobacco','alcohol','cough',
            'rash','sweat','fatigue','diarrhea','concerns','created_by','id','tb_category','tb_mask_size','bbp_category');
        $hs_id = $request->id;
        EmployeeHealthScreens::create($answers);

        //Create new BP record
        $request->offsetSet('id', Uuid::generate(4));
        $request->offsetSet('hs_id', $hs_id);// add hs id to link entry back to HS
        $answers = $request->only('employee_number', 'systolic','distolic','created_by','id','hs_id');
        EmployeeHealthBloodPressure::create($answers);

        //Create new weight record
        $request->offsetSet('id', Uuid::generate(4));
        $request->offsetSet('hs_id', $hs_id); // add hs id to link entry back to HS
        $answers = $request->only('employee_number', 'height_feet','height_inches','weight','created_by','id','hs_id');
        EmployeeHealthWeight::create($answers);

        //Create current allergies
        //First lets see if there is already a record.
        $allergy = EmployeeHealthAllergies::where('employee_number','=', $request->employee_number)
            ->first();

        if(is_null($allergy)){
            //No record, lets create one
            $request->offsetSet('id', Uuid::generate(4));
            $answers = $request->only('employee_number', 'allergies','created_by','id');
            EmployeeHealthAllergies::create($answers);
        }else{
            //Found one lets update it
            $allergy->allergies = $request->allergies;
            $allergy->save();
        }

        alert()->success('Health Screen Recorded!', 'Success!');
        return redirect('/employee_health/hs/' . $request->employee_number.'?hs='.$hs_id);

    }

    public function health_screen($id){

        $employee = User::where('employee_number','=', $id)
            ->first();

        $allergies = EmployeeHealthAllergies::where('employee_number','=', $id)
            ->first();

        $hs = EmployeeHealthScreens::where('id','=', Input::get('hs'))
            ->first();

        $hs_bp = EmployeeHealthBloodPressure::where('hs_id','=', $hs->id)
            ->first();

        $hs_weight = EmployeeHealthWeight::where('hs_id','=', $hs->id)
            ->first();

        $medAlerts = EmployeeHealthMedicalAlert::where('user_id', '=', $id)
            ->get();

        //Notes
        $notes = EmployeeHealthNotes::where('employee_number','=', $id)
            ->orderby('created_at','desc')
            ->get();

        //GRITS
        $grits = EmployeeHealthGrits::where('employee_number','=', $id)
            ->first();

        return View('employee_health.healthscreen', compact('employee','allergies','hs','hs_bp',
            'hs_weight','notes','grits', 'medAlerts'));
    }

    public function next_due_date($id, Requests\SurveyRequest $request)
    {
        $next = EmployeeHealthScreens::find($id);
        $next->next_due_date = $request->next_due_date;

        $next->save();

        alert()->success('Due Date Updated!', 'Success!');
        return redirect()->back();
    }

    public function enter_bp(Requests\SurveyRequest $request){

        $request->offsetSet('id', Uuid::generate(4));
        $bp = $request->all();
        EmployeeHealthBloodPressure::create($bp);

        alert()->success('Blood Pressure Recorded!', 'Success!');
        return redirect('/employee_health/' . $request->employee_number);
    }

    public function enter_weight(Requests\SurveyRequest $request){

        $request->offsetSet('id', Uuid::generate(4));
        $weight = $request->all();
        EmployeeHealthWeight::create($weight);

        alert()->success('Height and Weight Recorded!', 'Success!');
        return redirect('/employee_health/' . $request->employee_number);
    }

    public function update_hs(Requests\SurveyRequest $request){

        //Update
        $hs_update_id = EmployeeHealthScreens::where('id','=', $request->hs_update_id)
            ->first();
        $hs_update_id->work_related = $request->work_related;
        $hs_update_id->non_work_related = $request->non_work_related;
        $hs_update_id->tobacco = $request->tobacco;
        $hs_update_id->alcohol = $request->alcohol;
        $hs_update_id->cough = $request->cough;
        $hs_update_id->rash = $request->rash;
        $hs_update_id->sweat = $request->sweat;
        $hs_update_id->fatigue = $request->fatigue;
        $hs_update_id->diarrhea = $request->diarrhea;
        $hs_update_id->concerns = $request->concerns;
        $hs_update_id->tb_category = $request->tb_category;
        $hs_update_id->tb_mask_size = $request->tb_mask_size;
        $hs_update_id->bbp_category = $request->bbp_category;
        $hs_update_id->save();

        //update BP record
        $bp_update_id = EmployeeHealthBloodPressure::where('hs_id','=', $request->hs_update_id)
            ->first();
        $bp_update_id->systolic = $request->systolic;
        $bp_update_id->distolic = $request->distolic;
        $bp_update_id->save();


        //Update weight record
        $weight_update_id = EmployeeHealthWeight::where('hs_id','=', $request->hs_update_id)
            ->first();
        $weight_update_id->height_feet = $request->height_feet;
        $weight_update_id->height_inches = $request->height_inches;
        $weight_update_id->weight = $request->weight;
        $weight_update_id->save();

        //Create current allergies
        //First lets see if there is already a record.
        $allergy = EmployeeHealthAllergies::where('employee_number','=', $request->employee_number)
            ->first();

        if(is_null($allergy)){
            //No record, lets create one
            $request->offsetSet('id', Uuid::generate(4));
            $answers = $request->only('employee_number', 'allergies','created_by','id');
            EmployeeHealthAllergies::create($answers);
        }else {
            //Found one lets update it
            $allergy->allergies = $request->allergies;
            $allergy->save();
        }


        alert()->success('Health Screen Updated!', 'Success!');
        return redirect('/employee_health/hs/' . $request->employee_number.'?hs='.$request->hs_update_id);

    }

    public function edit_allergies(Requests\SurveyRequest $request){

        //Create current allergies
        //First lets see if there is already a record.
        $allergy = EmployeeHealthAllergies::where('employee_number','=', $request->employee_number)
            ->first();

        if(is_null($allergy)){
            //No record, lets create one
            $request->offsetSet('id', Uuid::generate(4));
            $answers = $request->only('employee_number', 'allergies','created_by','id');
            EmployeeHealthAllergies::create($answers);
        }else {
            //Found one lets update it
            $allergy->allergies = $request->allergies;
            $allergy->save();
        }

        $history = new History();
        $history->id = \Uuid::generate(4);
        $history->action = 'Allergies Updated';
        $history->userid = \Auth::user()->username;
        $history->search_string = 'None';
        $history->user_ip = \Request::ip();
        $history->save();


        alert()->success('Allergies Updated!', 'Success!');
        //lets see where we send the user after making a note
        if($request->page_source ==  'index'){
            return redirect('/employee_health/' . $request->employee_number.'#nav-dashboard');
        }

        if($request->page_source ==  'hs'){
            return redirect('/employee_health/hs/' . $request->employee_number.'?hs='.$request->hs_update_id.'#nav-dashboard');
        }
    }

}

