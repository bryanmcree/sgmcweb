<?php

namespace App\Http\Controllers;

use App\ClockedIn;
use App\CostCenterNotes;
use App\CostCenterRoll;
use App\CostCenters;
use App\History;
use App\PIDashboardClockedIn;
use App\Proclick;
use App\UnitService;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use Maatwebsite\Excel\Facades\Excel;
use Webpatser\Uuid\Uuid;

class CostCentersController extends Controller
{

    public function index(){


        $costcenters = CostCenters::wherenotnull('id')
            //->wherenotin('cost_center',['1700'])
            ->orderby('active_status','DESC')
            ->orderby('cost_center')
            ->get();

        $clockedin_time_id = PIDashboardClockedIn::select('time_id')
            //->take(1)
            ->orderby('time_id', 'desc')
            ->first();

        $clockedin = PIDashboardClockedIn::selectRaw('cost_center as cost_center, count(employee_number) as total')
            ->where('time_id','=',$clockedin_time_id->time_id)
            ->groupby('cost_center')
            //->take(5)
            ->get();
        //dd($clockedin_time_id);


        $history = new History();
        $history->id = \Uuid::generate(4);
        $history->action = 'CC Application';
        $history->userid = \Auth::user()->username;
        $history->user_ip = \Request::ip();
        $history->search_string = 'N/A';
        $history->save();

        return view('costcenters.index', compact('costcenters','clockedin'));


    }

    public function LE()
    {
        $ccs = CostCenters::wherenotnull('id')->where('active_status', 1)->where('style1', '<>', '')->orderby('cost_center')->get();
        
        $adminselect = User::wherenotnull('id')
            ->where('emp_status', 'active')
            ->where(function($q) {
                $q->where('title', 'like', '%president%')
                ->orWhere('title', 'like', '%chief%officer%');
            })
            ->orderby('username')->get();

        $divisionSelect = CostCenters::select('csuite')->where('csuite', '<>', '')->distinct()->get();

        return view('costcenters.LE', compact('ccs', 'adminselect', 'divisionSelect'));
    }

    public function updateLE(Request $request, CostCenters $cc)
    {
        $cc->update($request->all());

        alert()->success('Cost Center Updated!', 'Success!');
        return back();
    }

    //Add Notes to CC
    public function notes(Requests\SystemListRequest $request){

        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();
        CostCenterNotes::create($input);
        alert()->success('Note Saved', 'Success!');
        return redirect('/costcenters/' . $request->cost_center);

    }

    public function edit(Requests\SystemListRequest $request){

        // dd($request->all());
        
        $costcenter=CostCenters::where('id','=', $request->id)
            ->first();

        $request->offsetSet('style1', $request->cost_center. ' - ' .$request->unit_code_description);
        $request->offsetSet('style2', $request->unit_code_description. ' - ' .$request->cost_center);

        $costcenter->update($request->except('cost_center_roll_up'));

        alert()->success('Cost Center Updated!', 'Success!');
        return redirect('/costcenters/' . $costcenter->cost_center);
    }

    //Cost Center Detail
    public function detail($id){

        $costcenter = CostCenters::where('cost_center','=', $id)
            ->first();
        
        $costcenters = CostCenters::wherenotnull('id')->get();

        $rollup = CostCenterRoll::where('cost_center','=', $id)
            ->first();

        $rolledupto = CostCenterRoll::where('cost_center_roll_up','=', $id)
            ->get();

        $uos = UnitService::wherenotnull('id')
            ->orderby('unit_of_service')
            ->get();

        $clockedin_time_id = PIDashboardClockedIn::select('time_id')
            ->take(1)
            ->orderby('time_id', 'desc')
            ->first();
        //dd($clockedin_time_id);

        $costcenter_notes = CostCenterNotes::where('cost_center','=', $id)
            ->orderby('created_at')
            ->get();

        $clockedin = PIDashboardClockedIn::selectRaw('employee.name, PI_Dashboard_clockedIn.employee_number')
            ->leftJoin('users as employee', 'employee.employee_number', '=', 'PI_Dashboard_clockedIn.employee_number')
            //->wherenotnull('analyst')
            ->where('PI_Dashboard_clockedIn.time_id','=',$clockedin_time_id->time_id)
            ->where('cost_center','=', $id)
            ->wherenull('employee.deleted_at')  //Soft deletes
            ->orderby('employee.employee_number')
            ->get();

        $csuites = CostCenters::select('csuite')->where('csuite', '<>', '')->distinct()->get();

        $emps = User::select('employee_number', 'name')->where('emp_status', 'Active')->orderby('name')->get();

        return view('costcenters.detail', compact('costcenter', 'costcenters','uos','rollup','rolledupto','clockedin'
        ,'costcenter_notes', 'csuites', 'emps'));
    }

    //bulk update cost centers
    public function add(){

        $costcenters = CostCenters::where('active_status','=', 2)
            ->orderby('cost_center')
            ->get();

        //dd($costcenters);

        return view('costcenters.add', compact('costcenters'));
    }

    public function create()
    {
        return view('costcenters.create');
    }

    public function store(Request $request)
    {
        CostCenters::create([
            'cost_center' => $request->cost_center,
            'unit_code_description' => $request->unit_code_description,
            'style1' => $request->cost_center . '-' . $request->unit_code_description,
            'style2' => $request->unit_code_description . '-' . $request->cost_center,
            'active_status' => 1
        ]);

        alert()->success('Success!', 'Cost Center Created!');
        return redirect('/costcenters');
    }

    public function update(Requests\SurveyRequest $request)
    {

        $arr = $request->except('_token');
        //dd($arr);

        foreach ($arr as $key => $value) {

            $find_cc = CostCenters::where('cost_center','=', $key )->first();

                if(!empty($value['director'])){
                    $find_cc->director = $value['director'];
                }else{
                    $find_cc->director = null;
                }

                if(!empty($value['manager'])){
                    $find_cc->manager = $value['manager'];
                }else{
                    $find_cc->manager = null;
                }

                if(!empty($value['admin'])){
                    $find_cc->administrator = $value['admin'];
                }else{
                    $find_cc->administrator = null;
                }

                if(!empty($value['csuite'])){
                    $find_cc->csuite = $value['csuite'];
                }else{
                    $find_cc->csuite = null;
                }

                if(!empty($value['analyst'])){
                    $find_cc->analyst = $value['analyst'];
                }else{
                    $find_cc->analyst = null;
                }

            $find_cc->save();

        };

        return redirect('/costcenters');
    }

    public function report(){


        $csuite = CostCenters::selectraw('costcenters_new.csuite, csuiteEmployee.name ,avg(pc.variance) as variance, avg(pi.pi) as PI, count(*) as totalcc
        , sum(pi.fte_variance) as fte_variance, sum(pi.contract_fte) as contract_fte, sum(ce.Activity) as contract_expense')
            ->leftJoin('users as csuiteEmployee', 'csuiteEmployee.employee_number', '=', 'costcenters_new.csuite')
            ->leftJoin('pro_click_budget as pc', 'pc.cost_center', '=', 'costcenters_new.cost_center')
            ->leftJoin('contract_expense as ce', 'ce.cost_center', '=', 'costcenters_new.cost_center')
            ->leftJoin('sgmc_pi as pi', function($piJoin){
                $piJoin->on('pi.cost_center', '=', 'costcenters_new.cost_center')
                    ->where('pi.month','=', Carbon::now()->format('m')-2);
            })
            ->wherenotnull('costcenters_new.csuite')
            ->where('costcenters_new.active_status','=',1)
            //->where('pi.month','=', Carbon::now()->format('m')-1)
            ->where('pc.calendar_month','=', Carbon::now()->format('m')-2)
            ->groupby('costcenters_new.csuite','csuiteEmployee.name')
            ->orderby('csuiteEmployee.name')
            ->get();
        //dd($csuite);

        $history = new History();
        $history->id = \Uuid::generate(4);
        $history->action = 'CC PI Report';
        $history->userid = \Auth::user()->username;
        $history->user_ip = \Request::ip();
        $history->search_string = 'N/A';
        $history->save();

        return view('costcenters.report', compact('costcenters','director','csuite'));

    }

    public function report_dir($dir){

        $director = CostCenters::selectraw('costcenters_new.director, directorEmployee.name ,avg(pc.variance) as variance, avg(pi.pi) as PI, count(*) as totalcc, sum(expected_fte) as expected_fte,
        sum(worked_fte) as worked_fte, sum(pi.paid_fte) as paid_fte, sum(pi.fte_variance) as fte_variance, sum(pi.contract_fte) as contract_fte, sum(ce.Activity) as contract_expense')
            ->leftJoin('users as csuiteEmployee', 'csuiteEmployee.employee_number', '=', 'costcenters_new.csuite')
            ->leftJoin('users as directorEmployee', 'directorEmployee.employee_number', '=', 'costcenters_new.director')
            ->leftJoin('pro_click_budget as pc', 'pc.cost_center', '=', 'costcenters_new.cost_center')
            ->leftJoin('contract_expense as ce', 'ce.cost_center', '=', 'costcenters_new.cost_center')
            ->leftJoin('sgmc_pi as pi', function($piJoin){
                $piJoin->on('pi.cost_center', '=', 'costcenters_new.cost_center')
                    ->where('pi.month','=', Carbon::now()->format('m')-2);
            })
            ->wherenotnull('costcenters_new.director')
            ->where('costcenters_new.active_status','=',1)
            ->where('costcenters_new.csuite','=', $dir)
            //->where('pi.month','=', Carbon::now()->format('m')-1)
            ->where('pc.calendar_month','=', Carbon::now()->format('m')-2)
            ->groupby('costcenters_new.director','directorEmployee.name')
            ->get();

        $csuite_name = User::where('employee_number','=',$dir)
            ->first();
        //dd($csuite_name);
        return view('costcenters.director_report', compact('costcenters','director','csuite_name'));
    }

    public function report_cc($dir){

        //dd(Carbon::now()->format('m')-1);

        $costcenters = CostCenters::
            leftJoin('users as csuiteEmployee', 'csuiteEmployee.employee_number', '=', 'costcenters_new.csuite')
            ->leftJoin('users as directorEmployee', 'directorEmployee.employee_number', '=', 'costcenters_new.director')
            ->leftJoin('pro_click_budget as pc', 'pc.cost_center', '=', 'costcenters_new.cost_center')
            ->leftJoin('CostCenter_rollup as rollup', 'rollup.cost_center', '=', 'costcenters_new.cost_center')
            ->leftJoin('contract_expense as ce', 'ce.cost_center', '=', 'costcenters_new.cost_center')
            ->leftJoin('sgmc_pi as pi', function($piJoin){
                $piJoin->on('pi.cost_center', '=', 'costcenters_new.cost_center')
                    ->where('pi.month','=', Carbon::now()->format('m')-2);
            })
            ->wherenotnull('costcenters_new.csuite')
            ->where('costcenters_new.active_status','=',1)
            ->where('costcenters_new.director','=', $dir)
            //->where('pi.month','=', Carbon::now()->format('m')-1)
            ->where('pc.calendar_month','=', Carbon::now()->format('m')-2)
            ->get();

        //dd($costcenters);

        $csuite = CostCenters::
            where('director','=', $dir)
            ->wherenotnull('csuite')
            ->first();

        $csuite_name = User::where('employee_number','=',$csuite->csuite)
            ->first();

        //dd($csuite_name);
        //dd($director);
        return view('costcenters.costcenter_report', compact('costcenters','director','csuite','csuite_name'));
    }

    public function download(){

        //Because this uses a view with linked servers.
        $handle = \DB::connection()->getPdo();
        $handle->exec('SET QUOTED_IDENTIFIER ON');
        $handle->exec('SET ANSI_WARNINGS ON');
        $handle->exec('SET ANSI_PADDING ON');
        $handle->exec('SET ANSI_NULLS ON');
        $handle->exec('SET CONCAT_NULL_YIELDS_NULL ON');

        $emps = CostCenters::select('costcenters_new.cost_center','costcenters_new.unit_code_description as cost_center_description', 'csuiteEmployee.name as csuite', 'adminEmployee.name as Administrator'
        ,'directorEmployee.name as Director', 'managerEmployee.name as Manager', 'analystEmployee.name as Analyst', 'uos.unit_of_service as UOS','actionoi','clinical','active_status','rollup.cost_center_roll_up'
        )
            ->leftJoin('users as csuiteEmployee', 'csuiteEmployee.employee_number', '=', 'costcenters_new.csuite')
            ->leftJoin('users as adminEmployee', 'adminEmployee.employee_number', '=', 'costcenters_new.administrator')
            ->leftJoin('users as directorEmployee', 'directorEmployee.employee_number', '=', 'costcenters_new.director')
            ->leftJoin('users as managerEmployee', 'managerEmployee.employee_number', '=', 'costcenters_new.manager')
            ->leftJoin('users as analystEmployee', 'analystEmployee.employee_number', '=', 'costcenters_new.analyst')
            ->leftJoin('unit_of_service as uos', 'uos.id', '=', 'costcenters_new.flexed')
            ->leftJoin('CostCenter_rollup as rollup', 'rollup.cost_center', '=', 'costcenters_new.cost_center')
            //->where('countries.country_name', $country)
            //->take(10)
            ->get()
            ->toArray();

        //dd($emps);

        Excel::create('Cost_Center '.date('m-d-Y_hia'), function($excel) use($emps) {
            $excel->sheet('New sheet', function($sheet) use($emps) {
                $sheet->fromArray($emps);
            })->download('csv');
        });
    }

    public function analyst(){

        $costcentersUnassigned = CostCenters::wherenotnull('id')
            ->where('active_status','=',1)
            ->orderby('director')
            ->get();
        //dd($costcenters);

        $analysts = CostCenters::selectRaw('analystEmployee.name as analyst, count(*) as TotalCC, sum(clinical) as totalClinical, analyst as analyst_number')
            ->leftJoin('users as analystEmployee', 'analystEmployee.employee_number', '=', 'costcenters_new.analyst')
            //->wherenotnull('analyst')
            ->where('active_status','=', 1)
            ->groupby('analystEmployee.name','analyst')
            ->orderby('analystEmployee.name')
            ->get();

        return view('costcenters.analyst', compact('costcentersUnassigned','analysts'));
    }

    public function editPI(Request $request, CostCenters $costcenter)
    {

        // dd($request->all(), $costcenter);

        $costcenter->update([
            'actionoi' => $request->actionoi,
            'pi_norm' => $request->pi_norm,
            'pi_act' => $request->pi_act,
            'flexed' => $request->flexed,
            'uos_amount' => $request->uos_amount,
            'paid_fte' => $request->paid_fte,
            'worked_target' => $request->worked_target,
            'updated_at' => Carbon::now()
        ]);

        alert()->success('Cost Center PI Updated!', 'Success!');
        return redirect('/costcenters/' . $costcenter->cost_center);


    }

}
