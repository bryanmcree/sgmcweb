<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Policy;
use App\CostCenters;
use App\PolicyFacilities;
use App\Http\Requests;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;


class PolicyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $policies = Policy::where('active', '=', 'True')->get();

        $all = Policy::wherenotnull('id')
            ->orderBy('active', 'desc')
            ->get();

        return view('policy.index', compact('policies', 'all'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = CostCenters::wherenotnull('id')->get();

        return view('policy.create', compact('departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = $request->file('file_name');
        
        $policy = new Policy();

        $policy->id = Uuid::generate(4);
        $policy->description = $request->description;
        $policy->file_name = $file->getClientOriginalName();
        $policy->file_mime = $file->getClientMimeType();
        $policy->keywords = $request->keywords;
        $policy->active = 'True';
        $policy->title = $request->title;
        $policy->policy_num = $request->policy_num;
        $policy->responsibility = $request->responsibility;
        $policy->department = $request->department;
        $policy->last_rev_date = $request->last_rev_date;
        $policy->approval_date = $request->approval_date;

        Storage::disk('local')->put($policy->file_name,  File::get($file));

        $policy->save();

        if(!is_null($request->facilites))
        {
            foreach($request->facilities as $facility)
            {
                PolicyFacilities::create([
                    'id' => Uuid::generate(4),
                    'policy_id' => $policy->id,
                    'facility' => $facility
                ]);
            }
        }

        alert()->success('File Uploaded', 'Success!');
        return redirect()->back();
        
    }

    public function download(Policy $policy)
    {
        $file = Storage::disk('local')->get($policy->file_name);

        return (new Response($file, 200))
            ->header('Content-Type', $policy->file_mime)
            ->header('content-disposition', 'attachment; filename="'. $policy->file_name .'"');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Policy $policy)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Policy $policy)
    {
        $departments = CostCenters::wherenotnull('id')->get();

        $chosen = PolicyFacilities::where('policy_id', '=', $policy->id)->get();

        return view('policy.edit', compact('policy', 'departments', 'chosen'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, Policy $policy)
    {
        $file = $request->file('file_name');
        // dd($request->all(), $file, $policy);
        // $policy = new Policy();

        if(is_null($file))
        {
            $policy->description = $request->description;
            $policy->keywords = $request->keywords;
            $policy->title = $request->title;
            $policy->policy_num = $request->policy_num;
            $policy->responsibility = $request->responsibility;

            $policy->save();
        }
        else
        {
            $policy->description = $request->description;
            $policy->file_name = $file->getClientOriginalName();
            $policy->file_mime = $file->getClientMimeType();
            $policy->keywords = $request->keywords;
            $policy->active = 'True';
            $policy->title = $request->title;
            $policy->policy_num = $request->policy_num;
            $policy->responsibility = $request->responsibility;

            Storage::disk('local')->put($policy->file_name,  File::get($file));

            $policy->save();
        }

        alert()->success('File Updated', 'Success!');
        return redirect()->back();
    }


    public function active(Request $request, Policy $policy)
    {
        if($request->has('active'))
        {
            $policy->update([
                'active' => 'True'
            ]);
        }
        else
        {
            $policy->update([
                'active' => 'False'
            ]);
        }

        return redirect('/policies');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Policy $policy)
    {
        //
    }
}
