<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\CovidScreening;
use Uuid;

class CovidController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $screenings = CovidScreening::wherenotnull('id')->count();
        $failed = CovidScreening::where('is_passing', 0)->count();
        $avgTemp = CovidScreening::wherenotnull('id')->avg('temp');
        $all = CovidScreening::wherenotnull('id')->orderby('created_at', 'desc')->get();

        return view('covid.index', compact('screenings', 'failed', 'all', 'avgTemp'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lastTen = CovidScreening::wherenotnull('id')->orderby('created_at', 'desc')->get()->take(10);

        return view('covid.create', compact('lastTen'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->offsetSet('id', Uuid::generate(4));

        CovidScreening::create($request->all());

        alert()->success('Screening Recorded', 'Success!');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(CovidScreening $covid)
    {
        $covid->delete();
    }
}
