<?php

namespace App\Http\Controllers;

use App\History;
use App\PIDashboardClockedIn;
use App\SGMCCensus;
use App\SGMCCensusEmp;
use App\SGMCCensusPatients;
use App\SGMCCensusRatios;
use App\SGMCCensusSum;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class SGMCCensusController extends Controller
{
    public function index(){

        $floor_ratio = SGMCCensusRatios::wherenotnull('id')
            ->get();

        $last_batch = SGMCCensus::take(1)
            ->orderby('seq_id','DESC')
            ->first();
        //dd($last_batch); Carbon::now()->subHours(3)->toDateTimeString()

        $current_census = SGMCCensus::where('seq_id','=', $last_batch->seq_id)
            //->where('created_at', '>=', \Carbon\Carbon::today())
            ->where('created_at', '>=', \Carbon\Carbon::now()->subHours(12)->toDateTimeString())
            ->get();
        //dd($current_census);

        $distinct_unit = SGMCCensus::select('unit',DB::raw('max(total_traveler) as travelers'),'cost_center')
            //->where('created_at', '>=', \Carbon\Carbon::today())
            ->where('created_at', '>=', \Carbon\Carbon::now()->subHours(12)->toDateTimeString())
            ->groupby('unit','cost_center')
            ->orderby('unit')
            ->get();
        //dd($distinct_unit);

        $distinct_time = SGMCCensus::select('created_at')
            //->where('created_at', '>=', \Carbon\Carbon::today())
            ->where('created_at', '>=', \Carbon\Carbon::now()->subHours(12)->toDateTimeString())
            ->distinct()
            ->orderby('created_at')
            ->get();
        //dd($distinct_unit);

        $daily_census = SGMCCensus::where('created_at', '>', \Carbon\Carbon::now()->subHours(12)->toDateTimeString())
        //where('created_at', '>=', \Carbon\Carbon::today())
            //->where('unit','SGA2T')
            //->distinct()
            ->orderby('created_at')
            ->get();
        //dd($daily_census);

        //Clocked in
        $last_batch_emp = SGMCCensusEmp::take(1)
            ->orderby('created_at','DESC')
            ->first();
        //dd($last_batch_emp);

        $clocked_in = SGMCCensusEmp::where('created_at', '>=', \Carbon\Carbon::now()->subHours(12)->toDateTimeString())
        //where('created_at', '>=', \Carbon\Carbon::today())
            //->take(5)
            ->get();
        //dd($clocked_in);


        $total_census = SGMCCensus::
            where('seq_id','=', $last_batch->seq_id)
            ->get();

        $all_total_census = SGMCCensusSum::
            wherenotnull('id')
            ->get();
        //dd($all_total_census);

        $total_census_ed = SGMCCensus::where('seq_id','=', $last_batch->seq_id)
            ->where('unit','=','SGAER')
            ->first();
        //dd($total_census->sum('total_patients'));

        $clockedin_time_id = PIDashboardClockedIn::select('time_id')
            ->take(1)
            ->orderby('time_id', 'desc')
            ->first();
        //dd($clockedin_time_id);


        $clockedin = PIDashboardClockedIn::where('PI_Dashboard_clockedIn.time_id','=',$clockedin_time_id->time_id)
            ->get();

        //dd($clockedin);

        $census_history = History::where('action','=','Viewed Census')
            //->where('created_at', '>=', \Carbon\Carbon::today())
            ->where('created_at', '>=', \Carbon\Carbon::now()->subHours(12)->toDateTimeString())
            ->orderby('created_at','desc')
            ->get();

        $census_history_group = History::select('userid',DB::raw('count(userid) as user_total'))
            ->where('action','=','Viewed Census')
            //->where('created_at', '>=', \Carbon\Carbon::today())
            ->where('created_at', '>=', \Carbon\Carbon::now()->subHours(12)->toDateTimeString())
            ->groupby('userid')
            ->orderby('user_total','desc')
            ->get();
        //dd($census_history_group);

        $trends = SGMCCensusSum::where('created_at', '>=', \Carbon\Carbon::now()->subHours(12)->toDateTimeString())
        //where('created_at', '>=', \Carbon\Carbon::today())
            ->orderby('seq_id')
            ->get();
        //dd($trends);

        $trends_ed = SGMCCensusSum::where('created_at', '>=', \Carbon\Carbon::now()->subHours(12)->toDateTimeString())
            //where('created_at', '>=', \Carbon\Carbon::today())
            ->orderby('seq_id')
            ->get();

        $trends_weekly = SGMCCensusSum::where('created_at', '>=', \Carbon\Carbon::now()->subDays(3)->toDateTimeString())
            //where('created_at', '>=', \Carbon\Carbon::today())
            ->orderby('seq_id')
            ->get();

        $all_units = SGMCCensus::select('unit')
            ->distinct()
            ->orderby('unit')
            ->get();

        $current_patients = SGMCCensusPatients::wherenotnull('id')
            ->get();

        $history = new History();
        $history->id = \Uuid::generate(4);
        $history->action = 'Viewed Census';
        $history->userid = \Auth::user()->username;
        $history->user_ip = \Request::ip();
        $history->search_string = 'None';
        $history->save();

        return view('census.index', compact('distinct_unit','daily_census','distinct_time','clocked_in',
            'total_census','current_census','clockedin','census_history','census_history_group','trends','trends_weekly',
            'all_units','floor_ratio','current_patients','total_census_ed','trends_ed','all_total_census'));
    }

    public function censusReport(Requests\SurveyRequest $request){



        $startDate = \Carbon\Carbon::parse($request->startdate)->format('Y-m-d H:i:s');
        $endDate = \Carbon\Carbon::parse($request->enddate)->format('Y-m-d H:i:s');
        //dd(\Carbon\Carbon::parse($request->startdate)->format('Y-m-d H:i:s'));

        $last_batch = SGMCCensus::take(1)
            ->orderby('seq_id','DESC')
            ->first();
        //dd($last_batch); Carbon::now()->subHours(3)->toDateTimeString()

        $floor_ratio = SGMCCensusRatios::where('unit','=', $request->unit)
            ->first();

        $current_census = SGMCCensus::where('unit','=', $request->unit)
            ->whereBetween('created_at', [$startDate, $endDate])
            //->where('created_at', '>=', \Carbon\Carbon::today())
            //->where('created_at', '>=', \Carbon\Carbon::now()->subHours(12)->toDateTimeString())
            ->orderby('seq_id')
            ->get();
        //dd($current_census);

        $distinct_unit = SGMCCensus::select('unit',DB::raw('max(total_traveler) as travelers'),'cost_center')
            //->where('created_at', '>=', \Carbon\Carbon::today())
            ->where('created_at', '>=', \Carbon\Carbon::now()->subHours(12)->toDateTimeString())
            ->groupby('unit','cost_center')
            ->orderby('unit')
            ->get();
        //dd($distinct_unit);

        $distinct_time = SGMCCensus::select('created_at')
            //->where('created_at', '>=', \Carbon\Carbon::today())
            ->where('created_at', '>=', \Carbon\Carbon::now()->subHours(12)->toDateTimeString())
            ->distinct()
            ->orderby('created_at')
            ->get();
        //dd($distinct_unit);

        $daily_census = SGMCCensus::where('created_at', '>', \Carbon\Carbon::now()->subHours(12)->toDateTimeString())
            //where('created_at', '>=', \Carbon\Carbon::today())
            //->where('unit','SGA2T')
            //->distinct()
            ->orderby('created_at')
            ->get();
        //dd($daily_census);

        //Clocked in
        $last_batch_emp = SGMCCensusEmp::take(1)
            ->orderby('created_at','DESC')
            ->first();
        //dd($last_batch_emp);

        $clocked_in = SGMCCensusEmp::where('created_at', '>=', \Carbon\Carbon::now()->subHours(12)->toDateTimeString())
            //where('created_at', '>=', \Carbon\Carbon::today())
            //->take(5)
            ->get();
        //dd($clocked_in);


        $total_census = SGMCCensus::
        where('seq_id','=', $last_batch->seq_id)
            ->get();
        //dd($total_census->sum('total_patients'));

        $clockedin_time_id = PIDashboardClockedIn::select('time_id')
            ->take(1)
            ->orderby('time_id', 'desc')
            ->first();
        //dd($clockedin_time_id);


        $clockedin = PIDashboardClockedIn::where('PI_Dashboard_clockedIn.time_id','=',$clockedin_time_id->time_id)
            ->get();

        //dd($clockedin);

        $census_history = History::where('action','=','Viewed Census')
            //->where('created_at', '>=', \Carbon\Carbon::today())
            ->where('created_at', '>=', \Carbon\Carbon::now()->subHours(12)->toDateTimeString())
            ->orderby('created_at','desc')
            ->get();

        $census_history_group = History::select('userid',DB::raw('count(userid) as user_total'))
            ->where('action','=','Viewed Census')
            //->where('created_at', '>=', \Carbon\Carbon::today())
            ->where('created_at', '>=', \Carbon\Carbon::now()->subHours(12)->toDateTimeString())
            ->groupby('userid')
            ->orderby('user_total','desc')
            ->get();
        //dd($census_history_group);

        $trends = SGMCCensusSum::where('created_at', '>=', \Carbon\Carbon::now()->subHours(12)->toDateTimeString())
            //where('created_at', '>=', \Carbon\Carbon::today())
            ->orderby('seq_id')
            ->get();
        $trends_weekly = SGMCCensusSum::where('created_at', '>=', \Carbon\Carbon::now()->subDays(10)->toDateTimeString())
            //where('created_at', '>=', \Carbon\Carbon::today())
            ->orderby('seq_id')
            ->get();

        $all_units = SGMCCensus::select('unit')
            ->distinct()
            ->orderby('unit')
            ->get();


        $history = new History();
        $history->id = \Uuid::generate(4);
        $history->action = 'Census Report';
        $history->userid = \Auth::user()->username;
        $history->user_ip = \Request::ip();
        $history->search_string = 'Unit:'. $request->unit .' Start:'. $startDate . ' End:'. $endDate;
        $history->save();

        return view('census.report', compact('distinct_unit','daily_census','distinct_time','clocked_in',
            'total_census','current_census','clockedin','census_history','census_history_group','trends','trends_weekly',
            'all_units','startDate','endDate','floor_ratio'));
    }
}
