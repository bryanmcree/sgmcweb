<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\EmployeeHealthDueDate;
use App\EmployeeHealthScreens;
use Webpatser\Uuid\Uuid;
use App\History;

class EmployeeHealthDueDateController extends Controller
{
    public function next_due(Requests\SurveyRequest $request)
    {
        $due_date = EmployeeHealthDueDate::where('user_id', '=', $request->user_id)->first();

        if(is_null($due_date)){
            //No record, lets create one
            $request->offsetSet('id', Uuid::generate(4));
            $input = $request->only('user_id', 'next_due_date','id');
            EmployeeHealthDueDate::create($input);
        }else {
            //Found one lets update it
            $due_date->next_due_date = $request->next_due_date;
            $due_date->save();
        }

        $history = new History();
        $history->id = \Uuid::generate(4);
        $history->action = 'Next Due Date Updated';
        $history->userid = \Auth::user()->username;
        $history->search_string = 'None';
        $history->user_ip = \Request::ip();
        $history->save();


        alert()->success('Due Date Updated!', 'Success!');
        return redirect()->back();
    }
}
