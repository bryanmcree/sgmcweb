<?php

namespace App\Http\Controllers;

use Auth;
use Uuid;
use App\Roster;
use App\Education;
use App\EducationMethod;
use App\EducationReason;
use App\EducationPresenter;

use Illuminate\Http\Request;
use App\Http\Requests;

class EducationalController extends Controller
{
    public function index()
    {
        $forms = Education::wherenotnull('id')->get();

        return view('education.index', compact('forms'));
    }

    public function show(Education $edu)
    {
        $roster = Roster::where('id', '=', $edu->roster_id)->first();

        $reasons = EducationReason::where('edu_attendance_id', '=', $edu->id)->get();

        $methods = EducationMethod::where('edu_attendance_id', '=', $edu->id)->get();

        $presenters = EducationPresenter::where('edu_id', '=', $edu->id)->get();

        return view('education.show', compact('edu', 'reasons', 'methods', 'roster', 'presenters'));
    }

    public function print(Education $edu)
    {
        $roster = Roster::where('id', '=', $edu->roster_id)->first();

        $reasons = EducationReason::where('edu_attendance_id', '=', $edu->id)->get();

        $methods = EducationMethod::where('edu_attendance_id', '=', $edu->id)->get();

        $presenters = EducationPresenter::where('edu_id', '=', $edu->id)->get();

        return view('education.print', compact('edu', 'reasons', 'methods', 'roster', 'presenters'));
    }

    public function create()
    {
        $rosters = Roster::where('created_by', '=', Auth::user()->id)->get();
        
        return view('education.create', compact('rosters'));
    }

    public function store(Request $request)
    {
        $request->offsetSet('id', Uuid::generate(4));

        // dd($request->all());

        $edu = Education::create($request->except(['reason', 'methodology', 'presenter', 'department', 'badge_num', 'telephone_num']));

        foreach($request->reason as $reason)
        {
            EducationReason::create([
                'id' => Uuid::generate(4),
                'edu_attendance_id' => $edu->id,
                'reason' => $reason
            ]);
        }

        foreach($request->methodology as $method)
        {
            EducationMethod::create([
                'id' => Uuid::generate(4),
                'edu_attendance_id' => $edu->id,
                'methodology' => $method
            ]);
        }

        for($c = 0; $c < count($request->presenter); $c++)
        {
            if(!empty($request->presenter[$c]))
            {
                EducationPresenter::create([
                    'id' => Uuid::generate(4),
                    'edu_id' => $edu->id,
                    'presenter' => $request->presenter[$c],
                    'department' => $request->department[$c],
                    'badge_num' => $request->badge_num[$c],
                    'telephone_num' =>$request->telephone_num[$c]
                ]);
            }
        }

        alert()->success('Form Submitted!', 'Success!');
        return redirect('/education');
    }

    public function edit(Education $edu)
    {   
        $rosters = Roster::where('created_by', '=', Auth::user()->id)->get(); 
        $reasons = EducationReason::where('edu_attendance_id', '=', $edu->id)->get();
        $methods = EducationMethod::where('edu_attendance_id', '=', $edu->id)->get();

        return view('education.edit', compact('edu', 'rosters', 'reasons', 'methods'));
    }

    public function update(Request $request, Education $edu)
    {
        $edu->update($request->except(['reason', 'methodology']));

        foreach($request->reason as $reason)
        {
            EducationReason::where('edu_attendance_id', '=', $edu->id)->delete();
        }

        foreach($request->methodology as $method)
        {
            EducationMethod::where('edu_attendance_id', '=', $edu->id)->delete();
        }

        foreach($request->reason as $reason)
        {
            EducationReason::create([
                'id' => Uuid::generate(4),
                'edu_attendance_id' => $edu->id,
                'reason' => $reason
            ]);
        }

        foreach($request->methodology as $method)
        {
            EducationMethod::create([
                'id' => Uuid::generate(4),
                'edu_attendance_id' => $edu->id,
                'methodology' => $method
            ]);
        }

        alert()->success('Form Updated!', 'Success!');
        return redirect('/education');
    }

    public function destroy(Education $edu)
    {
        $edu->delete();
    }

}
