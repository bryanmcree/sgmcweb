<?php

namespace App\Http\Controllers;

use App\History;
use App\StaffingTool;
use App\StaffingToolNotes;
use Illuminate\Http\Request;

use App\Http\Requests;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Input;

class StaffingToolController extends Controller
{
    public function index()
    {
        //unit drop down
        $units = StaffingTool::select('shift_unit')
            ->distinct()
            ->orderby('shift_unit')
            ->get();
        //dd($units);

        return view('staffing_tool.index', compact('units'));
    }

    public function results(){

        //unit drop down
        $units = StaffingTool::select('shift_unit')
            ->distinct()
            ->orderby('shift_unit')
            ->get();

        //Vars
        $round_down = 3;

        $total_patients = Input::get('total_patients');
        //dd($total_patients);

        //Lets check to see if the input is valid




        $results = StaffingTool::where('shift_unit','=', Input::get('shift_unit'))
            ->where('shift','=', Input::get('shift'))
            ->first();

        if(empty($results)){
            //echo "no shift for that time";
            return view('staffing_tool.index', compact('units'))->withErrors(['That shift does not exist for that unit.']);;
        }

        if($total_patients > $results->total_beds){
            //echo "no shift for that time";
            return view('staffing_tool.index', compact('units'))->withErrors(['The number of patients entered EXCEEDS the number of available beds for that unit.']);;
        }
        //dd($results);

        //Lets dance....

        //figure licensed ratio

        $licensed_staff = $total_patients / $results->licensed_ratio;
        //need to determine to round up or down if .3 go up and .2 go down
        if(floor($licensed_staff)-$licensed_staff <= $round_down){
            $licensed_staff = ceil($licensed_staff);
        }else{
            $licensed_staff = floor($licensed_staff);
        }
        //dd(round($licensed_staff));


        //figure non-licensed ratio
        $nonlicensed_staff = $total_patients / $results->non_licensed_ratio;
        //need to determine to round up or down if .3 go up and .2 go down
        if(floor($nonlicensed_staff)-$nonlicensed_staff <= $round_down){
            $nonlicensed_staff = ceil($nonlicensed_staff);
        }else{
            $nonlicensed_staff = floor($nonlicensed_staff);
        }

        //dd($nonlicensed_staff);

        $unit = Input::get('shift_unit');
        $shift = Input::get('shift');


        //History
        $history = new History();
        $history->id = \Uuid::generate(4);
        $history->action = 'Staffing Tool';
        $history->userid = \Auth::user()->username;
        $history->search_string = $unit .' with' . $total_patients .' patients for ' .$shift .' shift.';
        $history->user_ip = \Request::ip();
        $history->save();



        return view('staffing_tool.results', compact('licensed_staff','results','nonlicensed_staff',
            'total_patients','unit','shift','units'));
    }

    public function add_note(Requests\SystemListRequest $request){

        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();
        StaffingToolNotes::create($input);

        alert()->success('Note Recorded!', 'Success!');

        return redirect('/staffing_tool');
    }

    public function history(){

        //History

        $history = History::where('action','=','Staffing Tool')
            ->orderby('created_at','DESC')
            ->get();

        $comments = StaffingToolNotes::wherenotnull('id')
            ->orderby('created_at','DESC')
            ->get();

        return view('staffing_tool.history', compact('history','comments'));
    }
}
