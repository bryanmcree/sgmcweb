<?php

namespace App\Http\Controllers;

use App\AxiomEarningDetail;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AxiomEarningDetailController extends Controller
{
    public function pay_codes(){

        //Because this uses a view with linked servers.
        $handle = \DB::connection()->getPdo();
        $handle->exec('SET QUOTED_IDENTIFIER ON');
        $handle->exec('SET ANSI_WARNINGS ON');
        $handle->exec('SET ANSI_PADDING ON');
        $handle->exec('SET ANSI_NULLS ON');
        $handle->exec('SET CONCAT_NULL_YIELDS_NULL ON');

        $pay_codes = AxiomEarningDetail::all();

        //dd($pay_codes);
        return View('axiom.paycodes', compact('pay_codes','user'));

    }
}
