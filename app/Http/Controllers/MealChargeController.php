<?php

namespace App\Http\Controllers;


use App\History;
use App\MealCharge;
use App\User;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;

class MealChargeController extends Controller
{
    public function index(){

        return view('meal_charge.index');

    }

    public function employeesearch()
    {

        $data = User::select("name",'employee_number')
            ->where('name', 'LIKE','%' . Input::get('query') . '%')
            ->whereIn('emp_status',['Active','Contract','Leave of Absence'])
            //->wherenotin('unit_code',['7009','8213','8210'])
            ->wherenull('deleted_at')
            ->orderby('name')
            ->get();
        //var_dump($data);

        foreach ($data as $key => $row) {
            $results['suggestions'][] = array('value' => $row['name'],'data' => $row['employee_number']);
        }

        return \Response::json($results);
    }

    public function AddEmployee(Requests\SystemListRequest $request){

        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();
        MealCharge::create($input);

        //alert()->success('Question has been added!', 'Success!');

        return redirect('/meal_charge');
    }
}
