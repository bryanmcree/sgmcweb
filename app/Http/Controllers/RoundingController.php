<?php

namespace App\Http\Controllers;

use App\Employee;
use App\RoundingSurvey;
use App\Http\Requests;
use App\RoleUser;
use App\User;
use Webpatser\Uuid\Uuid;
use Image;
use DB;
use App\State;
use App\Role;
use App\Http\Controllers\Auth;
use Alert;
use App\History;
use App\RoundingQuestion;
use App\RoundingAnswer;
use Illuminate\Support\Facades\Input;

class RoundingController extends Controller
{
    public function index()
    {
        $survey = RoundingSurvey::with('completedSurveys')->get();
        $completedSurveys = RoundingAnswer::select('survey_id','id')->with('surveys')->where('answeredby','=','bmcree')->distinct('id')->get();
        //dd($completedSurveys);
        return view('rounding.index',compact('survey','completedSurveys'));
    }

    public function newSurvey()
    {
        return view('rounding.index');
    }

    public function store(Requests\SystemListRequest $request)
    {
        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();
        RoundingSurvey::create($input);
        alert()->success('Survey has been added!', 'Survey Added');
        return redirect('/rounding');
    }

    public function show($id)
    {
        $survey = RoundingSurvey::with('surveyQuestion')->where('id', $id)->first();
        //$SystemList = SystemList::with('systemContacts', 'systemServers')->where('id', $id)->first();
        //dd($survey);
        return view('rounding.show', compact('survey'));
    }

    public function storeQuestion(Requests\SystemListRequest $request)
    {
        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();
        RoundingQuestion::create($input);
        alert()->success('Question has been added!', 'Question Added');
        return redirect('/rounding/show/' . $request->survey_id);
    }

    public function takeSurvey($id)
    {
        $survey = RoundingSurvey::with('surveyQuestion')->where('id', $id)->first();
        //$SystemList = SystemList::with('systemContacts', 'systemServers')->where('id', $id)->first();
        //dd($survey);
        return view('rounding.survey', compact('survey'));
    }

    public function storingSurvey(Requests\SystemListRequest $request)
    {

        // remove the token
        $arr = $request->except('_token','survey_id');
        $instance_id = Uuid::generate(4);
        //dd($arr);
        foreach ($arr as $key => $value) {
            $newAnswer = new RoundingAnswer();
            if (! is_array( $value )) {
                $newValue = $value['answer'];
            } else {
                $newValue = json_encode($value['answer']);
            }
            //dd($newValue);
            $newAnswer->question_answer = str_replace('"','',$newValue);
            $newAnswer->question_id = $key;
            //$newAnswer->comments_flag = $newValue->comments;
            $newAnswer->answeredby = \Auth::user()->username;
            $newAnswer->survey_id = $request->survey_id;
            //$newAnswer->comments_flag = $request->comments_flag;
            $newAnswer->id = $instance_id;

            $newAnswer->save();

            $answerArray[] = $newAnswer;
        };

        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'Rounding Survey Completed';
        $history->userid = \Auth::user()->username;
        $history->search_string = 'N/A';
        $history->user_ip = \Request::ip();
        $history->save();

        alert()->success('Survey Recorded!', 'COMPLETE!');
        return redirect('/rounding');
    }
    
    public function modifySurvey($id)
    {
        $data = RoundingAnswer::with('surveyQuestion','surveyAnswers')->where('id', $id)->get();
        return view('rounding.survey', compact('data'));
    }

    public function editQuestion(){
        $question = RoundingQuestion::find( Input::get('id') );
        $question->update(Input::all());
        alert()->success('Question Updated', 'Success!');
        return redirect('/rounding/show/'.Input::get('survey_id'));
    }

    public function deleteQuestion($id)
    {
        $question = RoundingQuestion::find( $id );
        $question->delete();
        return redirect('/rounding/show/'.Input::get('survey_id'));
    }
}
