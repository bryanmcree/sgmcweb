<?php

namespace App\Http\Controllers;

use App\Transport;
use Illuminate\Http\Request;

use App\Http\Requests;

use App\Tardy;
use Illuminate\Support\Facades\Input;
use DB;
use Carbon\Carbon;
use App\History;
use App\nedocs;
use App\ClockedIn;
use App\TrendEmployees;
use App\AD;
use App\RequestNew;
use Webpatser\Uuid\Uuid;
use App\Http\Controllers\Auth;
use App\User;

class TransportController extends Controller
{
    public function index()
    {
        $transporters = User::select('first_name','last_name','employee_number')->where('unit_code','=','6254')
            ->orderBy('last_name')
            ->get();
        //dd($transporters);
        //$ActiveRequest = Transport::with('requesterInfo')
        //    ->get();
        return View('transport.index', compact('ActiveRequest','transporters'));
    }

    public function dashboard(){
        $ActiveRequest = Transport::with('requesterInfo','transporterInfo')
            ->where('created_at', '>=', Carbon::today())
            ->whereIn('status', ['Assigned','Unassigned','Delay'])
            ->orderby('created_at','DESC')
            ->get();

        $TotalRequest = Transport::where('created_at', '>=', Carbon::today())
            //->whereIn('status', ['Assigned','Unassigned'])
            //->orderby('created_at','DESC')
            ->count();

        $TotalAssigned = Transport::where('created_at', '>=', Carbon::today())
            ->whereIn('status', ['Assigned'])
            //->orderby('created_at','DESC')
            ->count();

        $TotalComplete = Transport::where('created_at', '>=', Carbon::today())
            ->whereIn('status', ['Complete'])
            //->orderby('created_at','DESC')
            ->count();

        $TotalDelay = Transport::where('created_at', '>=', Carbon::today())
            ->whereIn('status', ['Delay'])
            //->orderby('created_at','DESC')
            ->count();

        $TotalCanceled = Transport::where('created_at', '>=', Carbon::today())
            ->whereIn('status', ['Canceled'])
            //->orderby('created_at','DESC')
            ->count();

        $TotalRescheduled = Transport::where('created_at', '>=', Carbon::today())
            ->whereIn('status', ['Rescheduled'])
            //->orderby('created_at','DESC')
            ->count();

        $TotalUnassigned = Transport::where('created_at', '>=', Carbon::today())
            ->whereIn('status', ['Unassigned'])
            //->orderby('created_at','DESC')
            ->count();

        $LastRequest = History::select('created_at')
            ->where('action','=','New Transport Request')
            ->orderby('created_at','DESC')
            ->first();
        //dd($LastRequest->created_at);



        return View('transport.dashboard', compact('ActiveRequest','transporters','TotalRequest','TotalAssigned'
        ,'TotalComplete','TotalDelay','TotalCanceled','TotalRescheduled','TotalUnassigned','LastRequest'));
    }

    public function completed(){
        $ActiveRequest = Transport::with('requesterInfo','transporterInfo')
            ->where('created_at', '>=', Carbon::today())
            ->whereNotIn('status', ['Assigned','Unassigned','Delay'])
            ->orderby('created_at','DESC')
            ->get();
        return View('transport.completed', compact('ActiveRequest','transporters'));
    }

    public function newRequest(Requests\Transport $request){


        if(date('Hi') > '2330' && date('Hi') < '0700' && \Auth::user()->hasRole('Transport') == FALSE){
            alert()->info('Transport is currently closed and will reopen at 7am')->persistent('Close');
            return redirect('/transport');
        }

        else{
            $request->offsetSet('id', Uuid::generate(4));
            $input = $request->all();
            Transport::create($input);

            $history = new History;
            $history->id = \Uuid::generate(4);
            $history->action = 'New Transport Request';
            $history->userid = \Auth::user()->username;
            $history->search_string = 'None';
            $history->user_ip = \Request::ip();
            $history->save();

            alert()->success('Request has been submitted.', 'Success!');
            return redirect('/transport');
        }


    }

    public function assign(){
        $trequest = Transport::find( Input::get('id') );



        if($trequest->assigned_at == NULL){
            if(Input::get('status') == 'Assigned'){
                $trequest->assigned_at = Carbon::now();
            }
        }


        if($trequest->completed_at == NULL){
            if(Input::get('status') == 'Complete'){
                $trequest->completed_at = Carbon::now();
            }
        }

        
        $trequest->update(Input::all());
        alert()->success('Transport Assigned', 'Success!');
        return redirect('/transport');
    }
}
