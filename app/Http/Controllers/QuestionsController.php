<?php

namespace App\Http\Controllers;

use App\Survey;
use App\Questions;
use App\SurveyAnswers;
use App\SurveyDropdown;
use Illuminate\Http\Request;

use App\Http\Requests;
use Webpatser\Uuid\Uuid;

class QuestionsController extends Controller
{
    public function add_question(Requests\SurveyRequest $request){
        //Add question to survey

        $order_num = Questions::where('survey_id', '=', $request->survey_id)->count();
        $request->offsetSet('question_order', $order_num+1);
        $request->offsetSet('id', Uuid::generate(4));

        $question = $request->except('dropdown_label');

        $q = Questions::create($question);
       // $options = array();
        //dd($request->dropdown_label);

        // dd($question, $q->id);

        //Loop through the dropdown values if they exist.
        foreach($request->dropdown_label as $option) {
            if(strlen($option) > 0){
                $newDropdown = new SurveyDropdown();
                $newDropdown->id = Uuid::generate(4);
                $newDropdown->survey_id = $request->survey_id;
                $newDropdown->question_id = $q->id;
                $newDropdown->dropdown_choice = $option;
                //$newDropdown->completed_by = $request->completed_by;
                $newDropdown->save();
            }

        }

        return redirect('/survey/details/'. $request->survey_id);
    }

    public function edit(Survey $survey, Questions $question)
    {
        $drops = SurveyDropdown::where('question_id', '=', $question->id)->get();

        return view('survey.edit_question', compact('question', 'drops', 'survey'));
    }

    public function update(Request $request, Questions $question)
    {
        $question->update([
            'title' => $request->title,
            'question_type' => $request->question_type,
            'question_notes' => $request->question_notes,
            'required' => $request->required
        ]);

        if($question->question_type == 'dropdown')
        {
            $drops = SurveyDropdown::where('question_id', '=', $question->id)->get();

            foreach($drops as $drop)
            {
                $drop->delete();
            }

            foreach($request->dropdown_label as $option) 
            {
                if(strlen($option) > 0)
                {
                    $newDropdown = new SurveyDropdown();
                    $newDropdown->id = Uuid::generate(4);
                    $newDropdown->survey_id = $request->survey_id;
                    $newDropdown->question_id = $question->id;
                    $newDropdown->dropdown_choice = $option;
                    $newDropdown->created_by = $request->created_by;
                    $newDropdown->save();
                }
            }
        }

        alert()->success('Success', 'Survey Updated');
        return redirect()->route('SurveyDetail', $request->survey_id);
    }


}
