<?php

namespace App\Http\Controllers;

use App\FormRequest;
use App\FormRequestStatus;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Tardy;
use Illuminate\Support\Facades\Input;
use DB;
use Carbon\Carbon;
use App\History;
use App\nedocs;
use App\ClockedIn;
use App\TrendEmployees;
use App\AD;
use App\RequestNew;
use Webpatser\Uuid\Uuid;
use App\Http\Controllers\Auth;
use App\User;
use Illuminate\Support\Facades\Mail;

class ReportRequestController extends Controller
{
    public function index()
    {
        $ManagerInfo = User::where('employee_number','=',\Auth::user()->manager_emp_number)
            ->get()
            ->first();

        $adusers = User::where('emp_status','=','Active')
            ->whereNotNull('employee_number')
            ->get();

        $Pending = FormRequest::with('formStatus','requesterInfo','managerInfo')
            ->where('requester_emp_number','=', \Auth::user()->employee_number)
            ->orderby('created_at','DESC')
            ->get();

        $actionrequired = FormRequest::with('formStatus','requesterInfo','managerInfo')
            ->where('requester_manager_emp_number','=', \Auth::user()->employee_number)
            ->whereNull('to_comittee')
            ->whereNull('denied')
            ->get();

        dd($actionrequired);


        return View('reports.request.index', compact('ManagerInfo','adusers','Pending','actionrequired'));
    }

    public function NewRequest(Requests\SystemListRequest $request){
        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->except('manager_email');

        //dd(Input::get('manager_email'));

        FormRequest::create($input);


        $FormStatus = new FormRequestStatus;
        $FormStatus->id = \Uuid::generate(4);
        $FormStatus->form_id = $request->id;
        $FormStatus->status = 'Pending Manager Approval';
        $FormStatus->updated_by = $request->requester_emp_number;
        $FormStatus->comments = 'New form request.';
        $FormStatus->save();

        //$mailData = $request->toArray();
        $mailData = FormRequest::with('formStatus','requesterInfo','managerInfo')
            ->where('id','=',$request->id)
            ->get()
            ->first()
            ->toArray();
        //dd($mailData);

        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'New Form Request';
        $history->userid = \Auth::user()->username;
        $history->search_string = 'None';
        $history->user_ip = \Request::ip();
        $history->save();
        

        //Lets send the manager an email for approval.
        Mail::send('reports.request.mail.manager', $mailData, function ($m) use ($mailData) {
            $m->from('sgmcweb-no-reply@sgmc.org', 'Web.SGMC.org');
            $m->to(Input::get('manager_email'));
            $m->subject('Report Request Notification');
            //$m->CC('sghelpdesk@sgmc.org');
            //$m->CC($data['mail']);
            //$m->replyTo($data['mail']);
        });

        alert()->success('Request has been submitted to your supervisor for approval.', 'Success!');
        return redirect('/reports/request');
    }






    
    public function submitRequest(Requests\SystemListRequest $request)
    {
        $request->offsetSet('id', Uuid::generate(4));
        $request->offsetSet('requester_name', \Auth::user()->name);
        $request->offsetSet('requester_email', \Auth::user()->mail);
        $request->offsetSet('requester_userid', \Auth::user()->username);
        $input = $request->all();
        RequestNew::create($input);
        alert()->success('Request has been submitted to your supervisor for approval.', 'Success!');


        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'New Request';
        $history->userid = \Auth::user()->username;
        $history->search_string = 'None';
        $history->user_ip = \Request::ip();
        $history->save();
        return redirect('/home');
    }

    public function RequestFeed()
    {
        $newrequest = RequestNew::distinct()->Select('request_type','assigned_email','system_name','request_priority')
            ->where('assigned_email', '=',\Auth::user()->mail)
            ->whereNull('request_completed')
            ->get();
        //dd($newrequest);
        return View('reports.request.feed', compact('newrequest'));
    }

    public function ManagerResponse($id)
    {
        $reviewrequest = RequestNew::where('id','=',$id)->first();
        return View('reports.request.manager', compact('reviewrequest'));

    }

    public function TerminationResponse($id)
    {
        $terminations = RequestNew::where('system_name','=',$id)
            ->whereNull('request_completed')
            ->get();
        //dd($terminations);
        return View('reports.request.systemtermination', compact('terminations','id'));

    }

    public function clearTerminated()
    {
       // dd(Input::except('_token'));

        $arr = Input::except('_token');

        foreach ($arr as $key => $value) {
            $tableData = RequestNew::findOrFail($key);

            $tableData['response'] = $value;
            $tableData['request_completed'] = Carbon::now();
            $tableData['completed_by'] = \Auth::user()->username;
            $tableData->update();
            //dd($tableData);
           // if (!$setting->save()) return Redirect::back()->withInput()->withErrors($setting->errors());



            $history = new History;
            $history->id = \Uuid::generate(4);
            $history->action = 'Clear Terminations IS';
            $history->userid = \Auth::user()->username;
            $history->search_string = $key .', Response'. $value;
            $history->user_ip = \Request::ip();
            $history->save();
        }
        alert()->success('Request Updated', 'Success!');
        //$SystemsContacts->update(Input::except(['systems_id']));
        return redirect('/home');


    }
    
    public function managerApproval(){

        //dd(Input::get('approve'));

        if(Input::get('approve') == 'Approve Request'){

            $tocommittee = FormRequest::find(Input::get('request_id'));
            $tocommittee->to_comittee = 1;
            $tocommittee->update();

            //dd(Input::get('requester_email'));

            $FormStatus = new FormRequestStatus;
            $FormStatus->id = \Uuid::generate(4);
            $FormStatus->form_id = Input::get('request_id');
            $FormStatus->status = 'Pending Committee Approval';
            $FormStatus->updated_by = \Auth::user()->employee_number;
            $FormStatus->comments = 'Request approved by manager.';
            $FormStatus->save();

            $history = new History;
            $history->id = \Uuid::generate(4);
            $history->action = 'Form Request - Manager Approval';
            $history->userid = \Auth::user()->username;
            $history->search_string = 'N/A';
            $history->user_ip = \Request::ip();
            $history->save();

            //$mailData = $request->toArray();
            $mailData = FormRequest::with('formStatus','requesterInfo','managerInfo')
                ->where('id','=',Input::get('request_id'))
                ->get()
                ->first()
                ->toArray();

            //Lets an email to the requester notifying them of the approval.
            Mail::send('reports.request.mail.managerapproval', $mailData, function ($m) use ($mailData) {
                $m->from('sgmcweb-no-reply@sgmc.org', 'Web.SGMC.org');
                $m->to(Input::get('requester_email'));
                $m->subject('Report Request Notification - Approval');
                //$m->CC('sghelpdesk@sgmc.org');
                //$m->CC($data['mail']);
                //$m->replyTo($data['mail']);
            });

            alert()->success('You have approved this request.', 'Success!');
            return redirect('/reports/request');
        }

        if(Input::get('deny') == 'Deny Request'){

            $tocommittee = FormRequest::find(Input::get('request_id'));
            $tocommittee->denied = 1;
            $tocommittee->update();


            $FormStatus = new FormRequestStatus;
            $FormStatus->id = \Uuid::generate(4);
            $FormStatus->form_id = Input::get('request_id');
            $FormStatus->status = 'Request denied by manager, returned to requester';
            $FormStatus->updated_by = \Auth::user()->employee_number;
            $FormStatus->comments = 'Request denied by manager.';
            $FormStatus->save();

            $history = new History;
            $history->id = \Uuid::generate(4);
            $history->action = 'Form Request - Manager Denial';
            $history->userid = \Auth::user()->username;
            $history->search_string = 'N/A';
            $history->user_ip = \Request::ip();
            $history->save();

            //$mailData = $request->toArray();
            $mailData = FormRequest::with('formStatus','requesterInfo','managerInfo')
                ->where('id','=',Input::get('request_id'))
                ->get()
                ->first()
                ->toArray();

            //Lets an email to the requester notifying them of the denial.
            Mail::send('reports.request.mail.managerdenial', $mailData, function ($m) use ($mailData) {
                $m->from('sgmcweb-no-reply@sgmc.org', 'Web.SGMC.org');
                $m->to(Input::get('requester_email'));
                $m->subject('Report Request Notification - Denied');
                //$m->CC('sghelpdesk@sgmc.org');
                //$m->CC($data['mail']);
                //$m->replyTo($data['mail']);
            });

            alert()->success('You have denied this request.', 'Success!');
            return redirect('/reports/request');
        }

    }

    
}
