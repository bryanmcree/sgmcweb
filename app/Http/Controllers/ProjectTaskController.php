<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;
use App\History;
use App\Http\Requests;
use Webpatser\Uuid\Uuid;

class ProjectTaskController extends Controller
{
    public function newTask(Requests\SystemListRequest $request){

        //$request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();
        //Input has been created lets save it to the database.
        Task::create($input);
        //$project_id = Project::create($input)->id;


        //Lets write this action to the history table.
        $history = new History();
        $history->id = Uuid::generate(4);
        $history->action = 'New Task';
        $history->userid = \Auth::user()->username;
        $history->search_string = $request->task_name;
        $history->user_ip = \Request::ip();
        $history->save();

        //Everything is good lets send feedback to the user and then redirect
        //alert()->success('Cost Center Added!', 'Success!');
        //return redirect('/project/' . $request->id);
        return back();
    }
}
