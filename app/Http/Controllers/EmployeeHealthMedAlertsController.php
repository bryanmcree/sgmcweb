<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\EmployeeHealthMedicalAlert;
use Webpatser\Uuid\Uuid;

class EmployeeHealthMedAlertsController extends Controller
{
    public function create(Request $request)
    {
        $request->offsetSet('id', Uuid::generate(4));
        $alert = $request->all();

        //dd($alert);
        EmployeeHealthMedicalAlert::create($alert);

        alert()->success('Alert Added!', 'Success!');
        return redirect()->back();
    }

    public function updateAlert(Request $request)
    {
        $alert = EmployeeHealthMedicalAlert::find($request->id);
        
        $alert->id = $request->id;
        $alert->alert = $request->alert;
        $alert->importance = $request->importance;
        $alert->created_by = $request->created_by;
        $alert->user_id = $request->user_id;

        $alert->save();

        alert()->success('Alert Updated', 'Success!');
        return redirect()->back();
    }

    public function deleteAlert($id)
    {
        $alert = EmployeeHealthMedicalAlert::find($id);

        $alert->delete();
    }
}
