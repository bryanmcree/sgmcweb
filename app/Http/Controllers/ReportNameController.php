<?php

namespace App\Http\Controllers;

use App\ReportDetails;
use App\ReportHistory;
use App\ReportName;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Webpatser\Uuid\Uuid;
use Image;
use DB;
use App\User;
use Illuminate\Support\Facades\Mail;

class ReportNameController extends Controller
{
    public function index(){

        //Manager's name
        $ManagerInfo = User::where('employee_number','=',\Auth::user()->manager_emp_number)
            ->get()
            ->first();

        //dd($ManagerInfo);

        $managerview = ReportName::whereHas('reportDetail', function($q) {
            $q->where('report_details.requestor_sup', '=', \Auth::user()->employee_number);
        })
            ->where('report_status','=','Pending Manager Approval')
            ->orderby('created_at','DESC')
            ->get();

        $reports = ReportName::with('requestedBy','reportDetail')
            ->where('requested_by','=',\Auth::user()->employee_number)
            ->orderby('created_at','DESC')
            ->get();

        $Allreports = ReportName::with('requestedBy','reportDetail')
            //->where('requested_by','=',\Auth::user()->employee_number)
            ->orderby('created_at','DESC')
            ->get();


        //dd($reports);
        return view('reports.request.index', compact('reports','adusers','managerview',
            'ManagerInfo','Allreports'));
    }

    public function newReport(Requests\SystemListRequest $request){

        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();
        ReportName::create($input);

        $ReportDetail = new ReportDetails;
        $ReportDetail->id = Uuid::generate(4);
        $ReportDetail->report_id = $request->id;
        $ReportDetail->report_priority = 0;
        $ReportDetail->requestor_sup = \Auth::user()->manager_emp_number;
        $ReportDetail->save();

        $reportHistory = new ReportHistory();
        $reportHistory->id = Uuid::generate(4);
        $reportHistory->report_id = $request->id;
        $reportHistory->status = 'New Report Created';
        $reportHistory->name =\Auth::user()->name;
        $reportHistory->notes = 'None';
        $reportHistory->created_at = Carbon::now();
        $reportHistory->updated_at = Carbon::now();
        $reportHistory->save();

        return redirect('/report/request/detail/view/'. $request->id);

    }
    //View report details
    public function detailReport($id){

        $adusers = User::where('emp_status','=','Active')
            ->whereNotNull('employee_number')
            ->get();

        dd($adusers);

        $ManagerInfo = User::where('employee_number','=',\Auth::user()->manager_emp_number)
            ->get()
            ->first();

        $ReportName = ReportName::with('requestedBy','reportDocs')->find($id);

        return view('reports.request.ReportDetails', compact('adusers','ReportName','ManagerInfo'));

    }

    //Submit report details
    public function detailSubmit(){

        //dd(Input::All());
        $update = ReportName::find( Input::get('report_id') );
        $update->report_status = 'Pending Manager Approval';
        $update->update(Input::except(['report_id','managerMail']));

        $ManagerInfo = User::where('employee_number','=',\Auth::user()->manager_emp_number)
            ->get()
            ->first();

        //$mailData = $request->toArray();

        $mailData = ReportDetails::with('managerInfo')
            ->where('report_id','=',Input::get('report_id'))
            ->first()
            ->toArray();

        //Lets send the manager an email for approval.
        Mail::send('reports.request.mail.manager', $mailData, function ($m) use ($mailData) {
            $m->from('sgmcweb-no-reply@sgmc.org', 'Report Request');
            $m->to(Input::get('managerMail'));
            //$m->to('bryan.mcree@sgmc.org');
            $m->subject('Report Request Notification');
            $m->CC('bryan.mcree@sgmc.org');
            //$m->CC($data['mail']);
            //$m->replyTo($data['mail']);
        });

        $reportHistory = new ReportHistory();
        $reportHistory->id = Uuid::generate(4);
        $reportHistory->report_id = Input::get('report_id');
        $reportHistory->status = 'Report Submitted to Manager';
        $reportHistory->name =\Auth::user()->name;
        $reportHistory->notes = 'None';
        $reportHistory->created_at = Carbon::now();
        $reportHistory->updated_at = Carbon::now();
        $reportHistory->save();

        alert()->success('Form request completed! A SysAid Ticket has been created for you.', 'Success!')->persistent();

        return redirect('/reports/request/');

    }

    public function viewReport($id){

        $ManagerInfo = User::where('employee_number','=',\Auth::user()->manager_emp_number)
            ->get()
            ->first();

        $report_history = ReportHistory::where('report_id','=',$id)
            ->orderby('created_at','DESC')
            ->get();

        $ReportName = ReportName::with('requestedBy','reportDocs','reportDetail')->find($id);
        //dd($ReportName);

        //Write to report history
        $reportHistory = new ReportHistory();
        $reportHistory->id = Uuid::generate(4);
        $reportHistory->report_id = $id;
        $reportHistory->status = 'Report Request Viewed';
        $reportHistory->name =\Auth::user()->name;
        $reportHistory->notes = 'None';
        $reportHistory->created_at = Carbon::now();
        $reportHistory->updated_at = Carbon::now();
        $reportHistory->save();

        return view('reports.request.view', compact('ReportName','ManagerInfo','report_history'));

    }

    public function updateDetails(){

        $update = ReportDetails::find( Input::get('id') );
        $update->update(Input::All());

        
        //Write to report history
        $reportHistory = new ReportHistory();
        $reportHistory->id = Uuid::generate(4);
        $reportHistory->report_id = Input::get('report_id');
        $reportHistory->status = 'Report Request Updated';
        $reportHistory->name =\Auth::user()->name;
        $reportHistory->notes = 'Phone or description was updated on report request.';
        $reportHistory->created_at = Carbon::now();
        $reportHistory->updated_at = Carbon::now();
        $reportHistory->save();

        alert()->success('Request Updated', 'Success!');

        return redirect('/report/request/detail/view/'. Input::get('report_id'));


    }

    public function approveReport($id){
        //This is where the manager will approve request.
        //Depending on the severity of the request will determine where the next
        //Step will be.

        //Lets get the report ID to approve.
        //dd($id);
        $report = ReportName::find($id);
        //Lets determine if the request is critical or normal.
        $importance = Input::get('importance');
        //dd($importance);

        if($importance == 'Critical'){
            $rpt_status = 'CRITICAL Request Approved';
        }else{
            $rpt_status = 'Report Request Approved';
        }

        //Lets put it all together update the report
        $report->report_status = $rpt_status;
        $report->importance = $importance;
        $report->approved_by = \Auth::user()->name;
        $report->approved_on = Carbon::now();
        $report->save();

        //Write to report history
        $reportHistory = new ReportHistory();
        $reportHistory->id = Uuid::generate(4);
        $reportHistory->report_id = $id;
        $reportHistory->status = $rpt_status;
        $reportHistory->name =\Auth::user()->name;
        $reportHistory->notes = 'None';
        $reportHistory->created_at = Carbon::now();
        $reportHistory->updated_at = Carbon::now();
        $reportHistory->save();

        if($importance == 'Critical'){
            //Report is critical so lets kick it to the admins

            $mailData = ReportName::with('reportDetail','requestedBy')
                ->where('id','=',$id)
                ->first()
                ->toArray();

            //$emails = ['bryan.mcree@sgmc.org','deborah.stone@SGMC.ORG','david.schott@sgmc.org','marc.perkins-carrillo@SGMC.ORG','carol.rowe@SGMC.ORG'];
            $emails = ['bryan.mcree@sgmc.org','jackie.marrero@SGMC.ORG'];
            Mail::send('reports.request.mail.criticalrequest', $mailData, function ($m) use ($mailData, $emails) {
                $m->from('sgmcweb-no-reply@sgmc.org', 'Report Request');
                $m->to($emails);
                $m->subject('CRITICAL - Report Request Notification');
                $m->priority('High');
            });

            //dd($mailData);
        }else{
            //report is normal lets kick it to the bids

            $mailData = ReportName::with('reportDetail','requestedBy')
                ->where('id','=',$id)
                ->first()
                ->toArray();

            //dd($mailData['requested_by']['mail']);

            //Notify user that request has been approved.
            Mail::send('reports.request.mail.approved', $mailData, function ($m) use ($mailData) {
                $m->from('sgmcweb-no-reply@sgmc.org', 'Report Request');
                $m->to('bryan.mcree@sgmc.org');
                $m->subject('Report Request Notification');
                $m->priority('High');
            });

        }



        alert()->success('Request Approved', 'Success!');

        return redirect('/report/request/detail/view/'. $id);
    }

    public function dg_view(){

        $managerview = ReportDetails::with('reportName')
            ->where('requestor_sup',\Auth::user()->employee_number)
            ->get();

        $reports = ReportName::with('requestedBy','reportDetail')
            //->where('requested_by','=',\Auth::user()->employee_number)
            //->where('status','Request Approved')
            ->orderby('created_at','DESC')
            ->get();
        //dd($reports);
        return view('reports.request.dgview', compact('reports','adusers','managerview'));

    }

    public function deny_request($id){

        //Get the report we are denying
        $report = ReportName::find($id);
        $notes = Input::get('notes');

        //Lets put it all together update the report
        $report->report_status = 'Report Request Denied';
        $report->save();

        //Write to report history
        $reportHistory = new ReportHistory();
        $reportHistory->id = Uuid::generate(4);
        $reportHistory->report_id = $id;
        $reportHistory->status = 'Report Request Denied';
        $reportHistory->name =\Auth::user()->name;
        $reportHistory->notes = $notes;
        $reportHistory->created_at = Carbon::now();
        $reportHistory->updated_at = Carbon::now();
        $reportHistory->save();

        //Now we need to notify the requester that their request was denied. So sad...

        $mailData = ReportName::with('reportDetail','requestedBy')
            ->where('id','=',$id)
            ->first()
            ->toArray();

        //dd($mailData);

        //Lets send the manager an email for approval.
        Mail::send('reports.request.mail.denied', $mailData, function ($m) use ($mailData) {
            $m->from('sgmcweb-no-reply@sgmc.org', 'Report Request');
            $m->to('bryan.mcree@sgmc.org');
            $m->subject('Report Request Denied');
            $m->priority('High');
        });

        alert()->success('Request Denied', 'Notifying Requester!');
        return redirect('/reports/request');
    }
}
