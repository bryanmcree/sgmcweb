<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests;
use Webpatser\Uuid\Uuid;
use Image;
use DB;
use App\State;
use App\Role;
use App\Http\Controllers\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;

class EpicReportController extends Controller
{
    public function index(){
        return view('epic_report.index', compact('questions','cost_centers'));
    }

    public function submit_request(Requests\SystemListRequest $request){

        //dd($request);

        $data = array(
            'requester_name' => $request->requester_name,
            'department_name'=>$request->department_name,
            'requester_phone'=>$request->requester_phone,
            'requester_email'=>$request->requester_email,
            'report_name'=>$request->report_name,
            'patient_safety' => $request->patient_safety,
            'regulatory_requirement' => $request->regulatory_requirement,
            'revenue' => $request->revenue,
            'productivity' => $request->productivity,
            'patient_and_family_experience' => $request->patient_and_family_experience,
            'organizational_impact' => $request->organizational_impact,
            'do_with_data' => $request->do_with_data,
            'what_information' => $request->what_information,
            'who_needs_access' => $request->who_needs_access,
            'when_need_report' => $request->when_need_report,
            'similar_report' => $request->similar_report,
            'criteria' => $request->criteria,
            'criteria_two' => $request->criteria_two,
            'info_to_display' => $request->info_to_display,
            'group_by' => $request->group_by,
            'criteria_text' => $request->criteria_text,
            'parameters_text' => $request->parameters_text,
            'date_range' => $request->date_range,
            'supervisor_name' => $request->supervisor_name,
            'supervisor_email' => $request->supervisor_email,
            'approved' => $request->approved,
        );

        Mail::send('epic_report.email', $data, function ($message) use($request) {

            //$message->from($request->mail,$request->name);
            $message->from("test@sgmc.org");
            $message->to('bryan.mcree@sgmc.org')->subject('Epic Report Request');
            $message->cc($request->requester_email)->subject('Epic Report Request');
            //$message->cc('bryan.mcree@sgmc.org')->subject('Epic Report Request');

        });

        //return "Your email has been sent successfully";
        alert()->success('Request has been submitted.', 'Success!');
        return redirect('/epic_report');
    }
}
