<?php

namespace App\Http\Controllers;

use App\SysaidLicenses;
use Illuminate\Http\Request;

use App\Http\Requests;

class LicenseMicrosoftController extends Controller
{
    public function index(){

        $microsoft = SysaidLicenses::where('license','=','Microsoft')
            ->orderby('created_at','desc')
            ->get();

        $e5_used = SysaidLicenses::select('e5_used')
            ->where('license','=','Microsoft')
            ->orderby('created_at','ASC')
            ->get();

        $f1_used = SysaidLicenses::select('f1_used','created_at')
            ->where('license','=','Microsoft')
            ->orderby('created_at','ASC')
            ->get();

        return view('sysaid.licenses.microsoft', compact('microsoft','e5_used','f1_used'));
    }

    public function add_microsoft(Requests\SystemListRequest $request){
        //Add new license entry
        //$request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();
        SysaidLicenses::create($input);
        alert()->success('License Updated!', 'Success!');
        return redirect('/sysaid');

    }
}
