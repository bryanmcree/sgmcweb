<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Tardy;
use Illuminate\Support\Facades\Input;
use DB;
use Carbon;
use App\History;
use App\nedocs;
use App\ClockedIn;
use App\TrendEmployees;
use App\AD;
use App\Census;


class ReportsController extends Controller
{
    public function index()
    {
        return view('reports.index');
    }

    public function countdownReport(){

        $nedocs = nedocs::orderBy('SCORE_DT','DESC')->first();
        //dd($nedocs);
        $nedocs24 = nedocs::select(DB::raw('sum(NUMBER_OF_ADMITS) AS NUMBER_OF_ADMITS, sum(ACUITY_1_PATIENTS) AS ACUITY_1_PATIENTS, 
        sum(NUMBER_OF_TRANSFERS) AS NUMBER_OF_TRANSFERS, sum(NUMBER_OF_DISCHARGES) AS NUMBER_OF_DISCHARGES'))
            ->where('SCORE_DT', '>=', Carbon\Carbon::today()->toDateString())
            ->where('NEDOCS_SCORE', '>', 1)
            ->first();

        //dd($nedocs24);


        $nedocsHi = nedocs::
            //->take(100)
            whereBetween('SCORE_DT', [
                Carbon\Carbon::parse('last monday')->startOfDay(),
                Carbon\Carbon::parse('next sunday')->endOfDay(),])
            ->where('NEDOCS_SCORE', '>', 1)
            ->max('NEDOCS_SCORE');
        //dd($nedocsHi);
        $nedocsavg = nedocs::
        //->take(100)
        whereBetween('SCORE_DT', [
            Carbon\Carbon::parse('last monday')->startOfDay(),
            Carbon\Carbon::parse('next sunday')->endOfDay(),])
            ->where('NEDOCS_SCORE', '>', 1)
            ->avg('NEDOCS_SCORE');
        //dd($nedocsadv);
        $nedocsNowHi = nedocs::select('NEDOCS_SCORE', 'SCORE_DT')
            ->where('SCORE_DT', '>=', Carbon\Carbon::today()->toDateString())
            ->where('NEDOCS_SCORE', '>', 1)
            ->max('NEDOCS_SCORE');
        $nedocsNowavg = nedocs::select('NEDOCS_SCORE', 'SCORE_DT')
            ->where('SCORE_DT', '>=', Carbon\Carbon::today()->toDateString())
            ->where('NEDOCS_SCORE', '>', 1)
            ->avg('NEDOCS_SCORE');

        //Nedocs Adv
        $longestwaitfortransfers = nedocs::select('LONGEST_WAIT_FOR_TRANSFER')->orderBy('SCORE_DT','DESC')->first()->avg('LONGEST_WAIT_FOR_TRANSFER');
        //dd($longestwaitfortransfers);
        $longestlobbywaitforbed = nedocs::select('LONGEST_LOBBY_WAIT_FOR_BED')->orderBy('SCORE_DT','DESC')->first()->avg('LONGEST_LOBBY_WAIT_FOR_BED');
        //dd($longestwaitfortransfers);
        $avgdecisiontodisposition = nedocs::select('AVG_DECISION_TO_DISP_TO_NOW')->orderBy('SCORE_DT','DESC')->first()->avg('AVG_DECISION_TO_DISP_TO_NOW');
        //dd($longestwaitfortransfers);
        $longetsdischargetime = nedocs::select('LONGEST_DISCHARGE_TIME')->orderBy('SCORE_DT','DESC')->first()->avg('LONGEST_DISCHARGE_TIME');
        //dd($longestwaitfortransfers);longestadmittime
        $longestadmittime = nedocs::select('LONGEST_ADMIT_TIME')->orderBy('SCORE_DT','DESC')->first()->avg('LONGEST_ADMIT_TIME');
        //dd($longestwaitfortransfers);


        //Census Code
        $data = Census::select('in_patient', 'observation','created_at')
            ->where('census_location','=','SGMC Campus')
            //->take(1)
            ->orderby('created_at')
            ->get();


        $results = array(
            'cols' => array (
                array('label' => 'Date/Time', 'type' => 'date'),
                array('label' => 'I/P', 'type' => 'number'),
                array('label' => 'OBS', 'type' => 'number'),
                array('label' => 'NEPD', 'type' => 'number')
            ),
            'rows' => array()
        );

        foreach ($data as $key => $value) {

            $created_at = Carbon\Carbon::parse($value['created_at'])->format('m-d-Y');
            $created_at_time = Carbon\Carbon::parse($value['created_at'])->format('H:i');
            $dateArr = explode('-', $created_at);
            $timeArr = explode(':', $created_at_time);
            $hour = $timeArr[0];
            $min = $timeArr[1];
            $year = $dateArr[2];
            $month = $dateArr[0] - 1; // subtract 1 because javascript uses a zero-based index for months
            $day = $dateArr[1];
            $results['rows'][] = array('c' => array(
                array('v' => "Date($year, $month, $day, $hour, $min)"),
                array('v' => $value['in_patient']),
                array('v' => $value['observation']),
                array('v' => $value['observation']+$value['in_patient']),

            ));

        }

        $results = json_encode($results);


        return view('reports.countdown.index2', compact('nedocs','nedocsHi','nedocsavg','nedocsNowHi','nedocsNowavg','longestwaitfortransfers','longestlobbywaitforbed',
            'avgdecisiontodisposition','longetsdischargetime','longestadmittime','nedocs24','results'));
    }

    public function tardyReport()
    {
        DB::statement('SET ANSI_NULLS ON; SET ANSI_WARNINGS ON');

        $tardy = '';

            if (Input::has('start_date')) 
            {
                $start_date = Input::get('start_date') . ' 00:00:00';
                $end_date = Input::get('end_date') . ' 23:59:59';
                $unit = Input::get('unit');

                if(Input::get('unit') == '') 
                {
                    alert()->error('Please select and "CLICK" a cost center from the drop down box.', 'You must CLICK the cost center you want!')->persistent();

                    $history = new History;
                    $history->id = \Uuid::generate(4);
                    $history->action = 'Error Viewing Tardy Report';
                    $history->userid = \Auth::user()->username;
                    $history->search_string = 'Unit: NO UNIT Start Date: ' . $start_date . '  End Date: ' . $end_date;
                    $history->user_ip = \Request::ip();
                    $history->save();

                    return \Redirect::to('reports/tardy');
                }

                //$tardy = Tardy::wherebetween('schedule',[$start_date, $end_date])
                $tardy = DB::table('tardyreport')
                    ->wherebetween('schedule', [$start_date, $end_date])
                    ->where('cost_center', $unit)
                    ->orderBy('last_name')
                    ->get();
                // DD($tardy);

                $report = '';

                $history = new History;
                $history->id = \Uuid::generate(4);
                $history->action = 'Viewed Tardy Report';
                $history->userid = \Auth::user()->username;
                $history->search_string = 'Unit: ' . $unit . ' Start Date: ' . $start_date . '  End Date: ' . $end_date;
                $history->user_ip = \Request::ip();
                $history->save();

            } 
            else 
            {
                $report = 'Nothing';
            }

        return view('reports.tardy.index',compact('tardy','report'));
    }


    public function nedocsguage()
    {

        $nedocs = nedocs::select("TOTAL_PATIENTS_IN_ED", "TOTAL_PATIENTS_IN_ER_BED","TOTAL_PATIENTS_IN_LOBBY","NUMBER_OF_ADMITS","ACUITY_1_PATIENTS","NUMBER_OF_TRANSFERS","NUMBER_OF_DISCHARGES")
            ->orderBy('SCORE_DT','DESC')->take(1)->get();
        //dd($nedocs);



// List of the chart rows
        $rows = [];

// Create rows for each month
        foreach ($nedocs as $key => $value) {
            $rows[] =  ['c' =>
                [
                    ['v' => $key],
                    ['v' => $value->TOTAL_PATIENTS_IN_ED],
                    ['v' => $value->TOTAL_PATIENTS_IN_ER_BED],
                    ['v' => $value->TOTAL_PATIENTS_IN_LOBBY],
                    ['v' => $value->NUMBER_OF_ADMITS],
                    ['v' => $value->ACUITY_1_PATIENTS],
                    ['v' => $value->NUMBER_OF_TRANSFERS],
                    ['v' => $value->NUMBER_OF_DISCHARGES]
                ]
            ];
        }



        $results = [

            'cols' => [
                ['id'=>'1','label'=> 'ED', 'type' => 'number'],
                ['id'=>'2',  'label'=> 'ED Bed', 'type' => 'number'],
                ['id'=>'3',  'label'=> 'Lobby', 'type' => 'number'],
                ['id'=>'4',  'label'=> 'Admits', 'type' => 'number'],
                ['id'=>'5',  'label'=> 'Acuity 1', 'type' => 'number'],
                ['id'=>'6',  'label'=> 'Transfers', 'type' => 'number'],
                ['id'=>'7',  'label'=> 'Discharges', 'type' => 'number']
            ], 'rows' => $rows

        ];


//dd($results);
        return \Response::json($results);


    }

    public function nedocsprogress()
    {
        $nedocs2 = nedocs::select('NEDOCS_SCORE')
            ->orderBy('SCORE_DT', 'DESC')->first();
        return view('reports.countdown.progress', compact('nedocs2'));
    }

    public function nedocFeed()
    {
        $data = nedocs::select('NEDOCS_SCORE', 'SCORE_DT')
            //->take(100)
            ->whereBetween('SCORE_DT', [
                Carbon\Carbon::parse('last monday')->startOfDay(),
                Carbon\Carbon::parse('next sunday')->endOfDay(),])
            ->where('NEDOCS_SCORE', '>', 1)
            ->orderBy('SCORE_DT')
            ->get();

        $results = array(
            'cols' => array (
                array('label' => 'SCORE_DT', 'type' => 'date'),
                array('label' => 'NEDOCS_SCORE', 'type' => 'number')
            ),
            'rows' => array()
        );
        foreach ($data as $key => $row) {
            $scoredate = Carbon\Carbon::parse($row['SCORE_DT'])->format('m-d-Y');
            $scoretime = Carbon\Carbon::parse($row['SCORE_DT'])->format('H:i:s');
            $dateArr = explode('-', $scoredate);
            $year = $dateArr[2];
            $month = $dateArr[0]-1; // subtract 1 because javascript uses a zero-based index for months
            $day = $dateArr[1];
            $timeArr = explode(':', $scoretime);
            $hour = $timeArr[0];
            $min = $timeArr[1];
            $sec = $timeArr[2];;
            $results['rows'][] = array('c' => array(
                array('v' => "Date($year, $month, $day, $hour, $min, $sec)"),
                array('v' => $row['NEDOCS_SCORE'])
            ));
        }

        return response($results);
    }

    public function nedocFeednow()
    {
        $data = nedocs::select('NEDOCS_SCORE', 'SCORE_DT')
            ->where('SCORE_DT', '>=', Carbon\Carbon::today()->toDateString())
            ->where('NEDOCS_SCORE', '>', 1)
            ->orderBy('SCORE_DT')
            ->get();

        $results = array(
            'cols' => array (
                array('label' => 'SCORE_DT', 'type' => 'date'),
                array('label' => 'NEDOCS_SCORE', 'type' => 'number')
            ),
            'rows' => array()
        );
        foreach ($data as $key => $row) {
            $scoredate = Carbon\Carbon::parse($row['SCORE_DT'])->format('m-d-Y');
            $scoretime = Carbon\Carbon::parse($row['SCORE_DT'])->format('H:i:s');
            $dateArr = explode('-', $scoredate);
            $year = $dateArr[2];
            $month = $dateArr[0]-1; // subtract 1 because javascript uses a zero-based index for months
            $day = $dateArr[1];
            $timeArr = explode(':', $scoretime);
            $hour = $timeArr[0];
            $min = $timeArr[1];
            $sec = $timeArr[2];;
            $results['rows'][] = array('c' => array(
                array('v' => "Date($year, $month, $day, $hour, $min, $sec)"),
                array('v' => $row['NEDOCS_SCORE'])
            ));
        }

        return response($results);
    }

    public function clockedin()
    {
        $data = CurrentlyClockedIn::select("current_clocked_in as cn")
            ->take(5)
            ->orderBy('id','desc')
            ->get();

        foreach ($data as $key => $row) {

            $results[] = array(
                array('cn' => $row['current_clocked_in'])
            );
        }
        return json_encode($data);
    }
    

    public function currentEmployees()
    {
        $data = TrendEmployees::select("CurrentEmployees","EmploymentStatus")->take(7)
            ->get();

        $results = array(
            'cols' => array (
                array('label' => 'EmploymentStatus', 'type' => 'string'),
                array('label' => 'CurrentEmployees', 'type' => 'number')
            ),
            'rows' => array()
        );
        foreach ($data as $key => $row) {

            $results['rows'][] = array('c' => array(
                array('v' => $row['EmploymentStatus']),
                array('v' => $row['CurrentEmployees'])
            ));
        }

        return response($results);
    }

}
