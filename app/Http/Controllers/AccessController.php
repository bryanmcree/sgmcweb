<?php

namespace App\Http\Controllers;

use App\CostCenters;
use Illuminate\Http\Request;

use App\Http\Requests;

class AccessController extends Controller
{
    public function index()
    {
        return view('access.index', compact('data'));
    }

    public function employee(){
        $cost_centers = CostCenters::all();
        return view('access.employee', compact('cost_centers'));
    }

    public function physician(){

        return view('access.physician', compact('data'));
    }
}
