<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\State;
use App\user;
use App\Http\Requests;
use Webpatser\Uuid\Uuid;

class RoleController extends Controller
{
    public function index()
    {
        //$users = User::paginate(10);
        $roles = Role::orderBy('name')->get();
        return View('role.index', compact('roles'));
    }

    public function create()
    {
        return view('role.create');
    }

    public function store(Requests\RoleRequest $request)
    {
        //dd($request);
        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();
        Role::create($input);
        return redirect('/role')->with('msg', 'Role has been added.');
    }

    public function edit($id)
    {
        $roles = Role::find($id);
        return View('role.edit', compact('roles'));
    }

    public function update(Requests\RoleRequest $request, $id)
    {
        $roles = Role::find($id);
        $roles->update($request->all());
        return redirect('/role')->with('msg', 'Role has been updated successfully');
    }

    public function delete($id) // Not being used ?
    {
        $roles = Role::find( $id );
        $roles->delete();
        return redirect('/role')->with('msg', 'Role has been deleted.');
    }

    public function destroy(Role $role)
    {
        $role->delete();
    }
}
