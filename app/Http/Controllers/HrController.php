<?php

namespace App\Http\Controllers;

use App\ExitInterview;
use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use PDF;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Employee;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use Symfony\Component\VarDumper\Dumper\DataDumperInterface;
use Webpatser\Uuid\Uuid;
use App\History;


class HrController extends Controller
{
    public function index()
    {
        return view('hr.index');
    }

    public function employeeSearch()
    {
        if(Input::has('search')) {
            $query = Input::get('search');
            $sqlStmt = DB::table('CurrentEmployees')->where('first_name', 'LIKE', '%' . $query . '%');

            if(Auth::user()->access != 'Admin'){
                $sqlStmt->where('Status', '=', 'Active');
            }


            $tests = $sqlStmt->orWhere('last_name', 'LIKE', '%' . $query . '%')
                ->orWhere('emp_number', 'LIKE', '%' . $query . '%')
                ->orWhere('unit_code', 'LIKE', '%' . $query . '%')->paginate(20)
                ->appends(['search' => $query]);
        }Else{
            //DD($request);
            $sqlStmt = DB::table('CurrentEmployees');

            if(Auth::user()->access != 'Admin'){
                $sqlStmt->where('Status', '=', 'Active');
            }
            $tests = $sqlStmt->orderBy('last_name', 'asc')->paginate(20);
        }
        return View('hr.index', compact('tests'));
        //return view('people.index');
    }

    public function par()
    {
        //$users = User::find($id);
        return view('hr.par', compact('users'));
    }

    public function dashboard()
    {
        //$users = User::find($id);
        return view('hr.dashboard', compact('users'));
    }
    
    public function exitInterview(){
        return view('hr.ExitInterview.index', compact('users'));
    }

    public function getexitInterview(Requests\ExitSurveyRequest $request){
        //dd($request);
        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();
        ExitInterview::create($input);

        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'Exit Survey Completed';
        $history->userid = \Auth::user()->username;
        $history->search_string = '';
        $history->user_ip = \Request::ip();
        $history->save();

        return redirect('/hr/exit');
    }

    public function complete(){

        $surveys = ExitInterview::with('employee')->get();
        //dd($surveys);
        return view('hr.ExitInterview.completed', compact('surveys'));
    }
    
    public function surveyView($id){
        $surveys = ExitInterview::with('employee')
            ->where('id','=',$id)
            ->get()
            ->first();
        //dd($surveys);
        return view('hr.ExitInterview.view', compact('surveys'));
    }

    public function report(){

        $start_date = Input::get('start_date'). ' 00:00:00';
        $end_date = Input::get('end_date'). ' 23:59:59';

        //dd($start_date);

        $TotalCompletedSurveys = ExitInterview::whereBetween('created_at', array($start_date, $end_date))
            ->count();

        $tenure = ExitInterview::select('tenure',DB::raw('COUNT(tenure) as tenuretotal'))
            ->whereBetween('created_at', array($start_date, $end_date))
            ->groupby('tenure')
            ->get();

        $emp_status = ExitInterview::select('emp_status',DB::raw('COUNT(emp_status) as empstatustotal'))
            ->whereBetween('created_at', array($start_date, $end_date))
            ->groupby('emp_status')
            ->get();
        //dd($emp_status);

        $reason_for_leaving = ExitInterview::select('reason_for_leaving',DB::raw('COUNT(reason_for_leaving) as reasonforleavingtotal'))
            ->whereBetween('created_at', array($start_date, $end_date))
            ->groupby('reason_for_leaving')
            ->get();

        $work_related_addressed = ExitInterview::select('work_related_addressed',DB::raw('COUNT(work_related_addressed) as workrelatedaddressedtotal'))
            ->whereBetween('created_at', array($start_date, $end_date))
            ->groupby('work_related_addressed')
            ->get();

        $like_least_about_job = ExitInterview::select('like_least_about_job',DB::raw('COUNT(like_least_about_job) as like_least_about_job_total'))
            ->whereBetween('created_at', array($start_date, $end_date))
            ->groupby('like_least_about_job')
            ->get();

        $like_most_about_job = ExitInterview::select('like_most_about_job',DB::raw('COUNT(like_most_about_job) as like_most_about_job_total'))
            ->whereBetween('created_at', array($start_date, $end_date))
            ->groupby('like_most_about_job')
            ->get();

        $say_in_what_i_did = ExitInterview::select('say_in_what_i_did',DB::raw('COUNT(say_in_what_i_did) as say_in_what_i_did_total'))
            ->whereBetween('created_at', array($start_date, $end_date))
            ->groupby('say_in_what_i_did')
            ->get();

        $workload_quality_job = ExitInterview::select('workload_quality_job',DB::raw('COUNT(workload_quality_job) as workload_quality_job_total'))
            ->whereBetween('created_at', array($start_date, $end_date))
            ->groupby('workload_quality_job')
            ->get();

        $proper_resources = ExitInterview::select('proper_resources',DB::raw('COUNT(workload_quality_job) as proper_resources_total'))
            ->whereBetween('created_at', array($start_date, $end_date))
            ->groupby('proper_resources')
            ->get();






        $supervisor_respect = ExitInterview::select('supervisor_respect',DB::raw('COUNT(supervisor_respect) as supervisor_respect_total'))
            ->whereBetween('created_at', array($start_date, $end_date))
            ->groupby('supervisor_respect')
            ->get();

        $project_completed = ExitInterview::select('project_completed',DB::raw('COUNT(project_completed) as project_completed_total'))
            ->whereBetween('created_at', array($start_date, $end_date))
            ->groupby('project_completed')
            ->get();

        $department_worked_well = ExitInterview::select('department_worked_well',DB::raw('COUNT(department_worked_well) as department_worked_well_total'))
            ->whereBetween('created_at', array($start_date, $end_date))
            ->groupby('department_worked_well')
            ->get();

        $stress_manageable = ExitInterview::select('stress_manageable',DB::raw('COUNT(stress_manageable) as stress_manageable_total'))
            ->whereBetween('created_at', array($start_date, $end_date))
            ->groupby('stress_manageable')
            ->get();

        $other_departments_pitch = ExitInterview::select('other_departments_pitch',DB::raw('COUNT(other_departments_pitch) as other_departments_pitch_total'))
            ->whereBetween('created_at', array($start_date, $end_date))
            ->groupby('other_departments_pitch')
            ->get();

        $opportunities_growth = ExitInterview::select('opportunities_growth',DB::raw('COUNT(opportunities_growth) as opportunities_growth_total'))
            ->whereBetween('created_at', array($start_date, $end_date))
            ->groupby('opportunities_growth')
            ->get();

        $knew_supposed_to_do = ExitInterview::select('knew_supposed_to_do',DB::raw('COUNT(knew_supposed_to_do) as knew_supposed_to_do_total'))
            ->whereBetween('created_at', array($start_date, $end_date))
            ->groupby('knew_supposed_to_do')
            ->get();

        $attitude_employees = ExitInterview::select('attitude_employees',DB::raw('COUNT(attitude_employees) as attitude_employees_total'))
            ->whereBetween('created_at', array($start_date, $end_date))
            ->groupby('attitude_employees')
            ->get();

        $believe_mission = ExitInterview::select('believe_mission',DB::raw('COUNT(believe_mission) as believe_mission_total'))
            ->whereBetween('created_at', array($start_date, $end_date))
            ->groupby('believe_mission')
            ->get();

        $enjoyed_sgmc = ExitInterview::select('enjoyed_sgmc',DB::raw('COUNT(enjoyed_sgmc) as enjoyed_sgmc_total'))
            ->whereBetween('created_at', array($start_date, $end_date))
            ->groupby('enjoyed_sgmc')
            ->get();




        return view('hr.ExitInterview.report', compact('start_date','end_date','TotalCompletedSurveys', 'tenure',
            'emp_status','reason_for_leaving','work_related_addressed','like_least_about_job','like_most_about_job',
            'say_in_what_i_did','workload_quality_job','proper_resources','supervisor_respect','project_completed',
            'department_worked_well','stress_manageable','other_departments_pitch','opportunities_growth',
            'knew_supposed_to_do','attitude_employees','believe_mission','enjoyed_sgmc'));
    }

    public function detail($id){

        $start_date = Input::get('start_date');
        $end_date = Input::get('end_date');
        $fieldname = $id;

        //dd($start_date);

        $detail = ExitInterview::select($fieldname,'employee_number')->with('employee')
            ->whereBetween('created_at', array($start_date, $end_date))
            ->get();

        $detailchart = ExitInterview::select(DB::raw(''.$fieldname.' as fieldname'),DB::raw('COUNT('.$fieldname.') as fieldnametotal'))
            ->whereBetween('created_at', array($start_date, $end_date))
            ->groupby($fieldname)
            ->get();

        $chartdata=array();
        foreach ($detailchart as $result) {
            $chartdata[$result->fieldname]=(int)$result->fieldnametotal;
        }

        //dd($chartdata);
        return view('hr.ExitInterview.detail', compact('detail','fieldname','chartdata'));
    }
}
