<?php

namespace App\Http\Controllers;

use App\EmployeeHealthNotes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Webpatser\Uuid\Uuid;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class EmployeeHealthNotesController extends Controller
{
    //Create Note
    public function enter_note(Requests\SurveyRequest $request){

        $request->offsetSet('id', Uuid::generate(4));
        $new_note = $request->except('page_source','hs_update_id');
        EmployeeHealthNotes::create($new_note);

        alert()->success('Nurse Note Recorded!', 'Success!');

        //lets see where we send the user after making a note
        if($request->page_source ==  'index'){
            return redirect('/employee_health/' . $request->employee_number.'#nav-notes');
        }

        if($request->page_source ==  'hs'){
            return redirect('/employee_health/hs/' . $request->employee_number.'?hs='.$request->hs_update_id.'#nav-notes');
        }

    }

    public function edit_note(Requests\SurveyRequest $request){

        //update BP record
        $note = EmployeeHealthNotes::where('id','=', $request->note_id)
            ->first();
        $note->followup_date = $request->followup_date;
        $note->note = $request->note;
        $note->save();

        alert()->success('Nurse Note Updated!', 'Success!');

        //lets see where we send the user after making a note
        if($request->page_source ==  'index'){
            return redirect('/employee_health/' . $request->employee_number.'#nav-notes');
        }

        if($request->page_source ==  'hs'){
            return redirect('/employee_health/hs/' . $request->employee_number.'?hs='.$request->hs_update_id.'#nav-notes');
        }
    }
}
