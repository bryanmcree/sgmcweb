<?php

namespace App\Http\Controllers;

use App\History;
use App\Readmissions;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;


class ReadmissionsController extends Controller
{
    public function index(){

        $total_golive = Readmissions::wherenotnull('id')
            ->get();
        //dd($total_golive);

        $total_year = Readmissions::whereyear('readmit_adm_date','=', Carbon::now()->year)
            ->get();

        $total_last_year = Readmissions::whereyear('readmit_adm_date','=', Carbon::now()->subYear(1)->year)
            ->get();

        $patients = Readmissions::wherenotnull('id')
            ->whereDate('readmit_adm_date', '>', Carbon::now()->subDays(10))
            ->orderby('readmit_adm_date', 'desc')
            ->get();
        //dd($patients);

        $history = new History();
        $history->id = \Uuid::generate(4);
        $history->action = 'Readmission';
        $history->userid = \Auth::user()->username;
        $history->user_ip = \Request::ip();
        $history->search_string = 'None';
        $history->save();


        return view('readmissions.index', compact('patients','total_golive','total_year','total_last_year'));
    }

    public function survey($id){


        $patient = Readmissions::where('id', '=', $id)
            ->first();
        //dd($patient);

        return view('readmissions.survey', compact('patient'));
    }
}
