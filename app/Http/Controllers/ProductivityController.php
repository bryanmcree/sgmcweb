<?php

namespace App\Http\Controllers;

use App\ProductivityUnitTotals;
use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use App\Productivity;
use App\uos;
use App\CostCenters;
use App\CurrentlyClockedIn;
use App\History;
use PDF;



class ProductivityController extends Controller
{
    public function index()
    {
        DB::statement('SET ANSI_NULLS ON; SET ANSI_WARNINGS ON');

        //Pull a current list of accurate cost centers
        $OrgUnit = CostCenters::orderby('style2')->pluck('style2', 'cost_center');

        //Pull stats to display on the main page.
        $totalcost = CostCenters::count(DB::raw('DISTINCT cost_center'));
        $totalrec = Productivity::count('id');
        $totalfixed = Productivity::where('unit_measure','=','Fixed')->count(DB::raw('DISTINCT cost_center'));
        $totaluos = Productivity::where('unit_measure','!=','Fixed')->count(DB::raw('DISTINCT cost_center'));
        $startdate = Productivity::min('search_date');
        $enddate = Productivity::max('search_date');
        $totalclockedin = CurrentlyClockedIn::count('id');



        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'Viewed Productivity';
        $history->userid = \Auth::user()->username;
        $history->search_string = 'None';
        $history->user_ip = \Request::ip();
        $history->save();

        //dd($ccData);

        return View('productivity.index', compact('OrgUnit','cost_center','prod', 'data','unit_measure','results','data3','data2','results2','result','totalcost','totalfixed','totaluos'
                    ,'startdate','enddate','totaldays','totalclockedin', 'totalrec'));
    }

    public function chart()
    {
        DB::statement('SET ANSI_NULLS ON; SET ANSI_WARNINGS ON');

        $unit = Input::get('unit');
        $prod = 'something';



        $data3 = Productivity::where('cost_center', '=',$unit )->first();

        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'Viewed Productivity Chart';
        $history->userid = \Auth::user()->username;
        $history->search_string = $unit .' - '.$data3->cost_center_description;
        $history->user_ip = \Request::ip();
        $history->save();

        //Pull a current list of accurate cost centers
        $OrgUnit = CostCenters::orderby('style2')->pluck('style2', 'cost_center');

        if(input::get('newucl') != ''){
            $uclValue = input::get('newucl');
        }else{
            $uclValue = 4;
        }

        if(input::get('newlcl') != ''){
            $lclValue = input::get('newlcl');
        }else{
            $lclValue = 4;
        }


        //Ok because some cost centers have units of measure and others are fixed we need to decide how to plot
        //the data depending on the unit of measure.
        if($data3->unit_measure == 'Fixed')
        {
            //Fixed Cost Center
            //Lets get the mean
            if(input::get('newmean') != ''){
                $data2 = input::get('newmean');
                $oldmean = Productivity::where('cost_center', '=',$unit )->where('total_hours','!=',0)->get()->AVG('total_hours');
                //DD('New Mean!!');
            }else{
                $data2 = Productivity::where('cost_center', '=',$unit )->where('total_hours','!=',0)->get()->AVG('total_hours');
                $oldmean = $data2;
            }

            //Lets count the number or records returned
            $countData = Productivity::where('cost_center', '=',$unit )->where('total_hours','!=',0)->get()->Count('pd_uos');
            //Calculate the step diff For fixed uses total hours
            $stDev = Productivity::select(DB::raw("STDEV(total_hours) / SQRT($countData) as stDev"))->where('cost_center', '=',$unit )->where('total_hours','!=',0)->first();
            //Set the collection to a value
            $standardDev = $stDev->stDev;

            $data3 = Productivity::where('cost_center', '=',$unit )->first();
            $data = Productivity::select("search_date","cost_center", "unit_measure", "cost_center_description", "pd_uos", "total_hours","dayofweek","overtime")
                ->where('cost_center', '=',$unit )
                ->where('total_hours','!=',0)
                ->orderBy('search_date')
                ->get();

            $data4 = Productivity::select("search_date","cost_center", "unit_measure", "cost_center_description", "uos_hours","total_hours","fte_hourly","fte_salary","pd_uos","dayofweek","overtime")
                ->where('cost_center', '=',$unit )
                ->orderBy('search_date')
                ->get();

            //This is where we will calculate the standard deviation and square root
            $ucl = round((((($standardDev*3)+$data2)/$uclValue)+$data2),2);
            $lcl = round((((($standardDev*3)-$data2)/$lclValue)+$data2),2);
            $mean = round($data2,2);

            //$result[] = ['Date ','Productivity','Goal '.$mean,'UCL '.$ucl,'LCL '.$lcl, 'Over Time'];
            //foreach ($data as $key => $value) {
            //    $result[++$key] = [$value->search_date, (float)$value->total_hours, (float)$mean, (float)$ucl, (float)$lcl, (float)$value->overtime];
            //}
            //$result2[] = ['Date','Cost Center','Measure','Description','UOS/FTE','Total Hours','FTE Hourly','FTE Salary','ADV HRS/UOS*','UCL*','LCL*','Day of Week','Over Time'];
            //foreach ($data4 as $key => $value) {
            //    $result2[++$key] = [$value->search_date, $value->cost_center, $value->unit_measure, $value->cost_center_description,
            ////        (float)$value->pd_uos, (float)$value->total_hours, (int)$value->fte_hourly, (int)$value->fte_salary, (float)$mean,
            //        (float)$ucl, (float)$lcl,$value->dayofweek, (float)$value->overtime];
            //}


            $results = array(
                'cols' => array (
                    array('label' => 'Date', 'type' => 'date'),
                    array('label' => 'Productivity', 'type' => 'number'),
                    array('label' => 'Goal '.$mean, 'type' => 'number'),
                    array('label' => 'UCL '.$ucl, 'type' => 'number'),
                    array('label' => 'LCL '.$lcl, 'type' => 'number'),
                    array('label' => 'Over Time', 'type' => 'number')
                ),
                'rows' => array()
            );

            foreach ($data as $key => $value) {


                $searchDate = Carbon::parse($value['search_date'])->format('m-d-Y');
                $dateArr = explode('-', $searchDate);
                $year = $dateArr[2];
                $month = $dateArr[0] - 1; // subtract 1 because javascript uses a zero-based index for months
                $day = $dateArr[1];
                $results['rows'][] = array('c' => array(
                    array('v' => "Date($year, $month, $day)"),
                    array('v' => $value['total_hours']),
                    array('v' => $mean),
                    array('v' => $ucl),
                    array('v' => $lcl),
                    array('v' => $value['overtime']),
                ));

            }

        }else{
            //Lets get the adverage



            if(input::get('newmean') != ''){
                $data2 = input::get('newmean');
                $oldmean = Productivity::where('cost_center', '=',$unit )->where('uos_pd', '>',0 )->get()->AVG('uos_pd');
                //DD('New Mean!!');
            }else{
                $data2 = Productivity::where('cost_center', '=',$unit )->where('uos_pd', '>',0 )->get()->AVG('uos_pd');
                $oldmean = $data2;
            }

            //Lets count the number or records returned
            $countData = Productivity::where('cost_center', '=',$unit )->where('uos_pd', '>',0 )->get()->Count('uos_pd');
            //Calculate the step diff
            $stDev = Productivity::select(DB::raw("STDEV(uos_hours) / SQRT($countData) as stDev"))->where('cost_center', '=',$unit )->where('uos_pd', '>',0 )->first();
            //Set the collection to a value
            $standardDev = $stDev->stDev;

            $data3 = Productivity::where('cost_center', '=',$unit )->first();

            $data = Productivity::select("search_date","cost_center", "unit_measure", "cost_center_description", "uos_pd", "total_hours","dayofweek","overtime")
                ->where('cost_center', '=',$unit )
                ->where('uos_pd', '>',0 )
                ->orderBy('search_date')
                ->get();
            //$queries = DB::getQueryLog();
            //dd($queries);

            $data4 = Productivity::select("search_date","cost_center", "unit_measure", "cost_center_description", "uos_hours","total_hours","fte_hourly","fte_salary","uos_pd","dayofweek","overtime")
                ->where('cost_center', '=',$unit )
                ->where('uos_pd', '>',0 )
                ->orderBy('search_date')
                ->get();

            //This is where we will calculate the standard deviation and square root
            $ucl = round((((($standardDev*3)+$data2)/$uclValue)+$data2),2);
            $lcl = round((((($standardDev*3)-$data2)/$lclValue)+$data2),2);
            $mean = round($data2,2);

            //Ok this cost center has a unit of measurement so lets chart that.... (waves wand... POOOF!) eat your heart out Harry Potter....
            //$result[] = ['Date','Productivity','Goal '.$mean,'UCL '.$ucl,'LCL '.$lcl,'Over Time'];
            //foreach ($data as $key => $value) {
            //    $result[++$key] = [$value->search_date .' '. $value->dayofweek, (float)$value->uos_pd, (float)$mean, (float)$ucl, (float)$lcl, (float)$value->overtime];
            //}
            //$result2[] = ['Date','Cost Center','Measure','Description','UOS/FTE','Total Hours','FTE Hourly','FTE Salary','ADV HRS/UOS*','UCL*','LCL*','Day of Week','Over Time'];
            //foreach ($data4 as $key => $value) {
            //    $result2[++$key] = [$value->search_date, $value->cost_center, $value->unit_measure, $value->cost_center_description,
            ///       (float)$value->uos_pd, (float)$value->total_hours, (int)$value->fte_hourly, (int)$value->fte_salary, (float)$mean, (float)$ucl,
            //        (float)$lcl,$value->dayofweek, (float)$value->overtime];
            //}

            $results = array(
                'cols' => array (
                    array('label' => 'Date', 'type' => 'date'),
                    array('label' => 'Productivity', 'type' => 'number'),
                    array('label' => 'Goal '.$mean, 'type' => 'number'),
                    array('label' => 'UCL '.$ucl, 'type' => 'number'),
                    array('label' => 'LCL '.$lcl, 'type' => 'number'),
                    array('label' => 'Over Time', 'type' => 'number')
                ),
                'rows' => array()
            );

            foreach ($data as $key => $value) {


                $searchDate = Carbon::parse($value['search_date'])->format('m-d-Y');
                $dateArr = explode('-', $searchDate);
                $year = $dateArr[2];
                $month = $dateArr[0] - 1; // subtract 1 because javascript uses a zero-based index for months
                $day = $dateArr[1];
                $results['rows'][] = array('c' => array(
                    array('v' => "Date($year, $month, $day)"),
                    array('v' => $value['uos_pd']),
                    array('v' => $mean),
                    array('v' => $ucl),
                    array('v' => $lcl),
                    array('v' => $value['overtime']),
                ));

            }

           // dd($results);

        }

        //Turn the results into a json string
        $results = json_encode($results);
        //$results2 = json_encode($result2);

        return View('productivity.charts', compact('OrgUnit','cost_center','prod', 'data','unit_measure','results','data3','data2','results2','result','totalcost','totalfixed','totaluos'
            ,'startdate','enddate','totaldays','oldmean','uclValue','lclValue'));
    }



    public function clockedin()
    {
        $data = CurrentlyClockedIn::select("run_time","current_clocked_in")->take(20)
            ->orderBy('id','desc')
            ->get()->reverse();

        $results = array(
            'cols' => array (
                array('label' => 'run_time', 'type' => 'string'),
                array('label' => 'Employees', 'type' => 'number')
            ),
            'rows' => array()
        );
        foreach ($data as $key => $row) {

            $results['rows'][] = array('c' => array(
                array('v' => $row['run_time']),
                array('v' => $row['current_clocked_in'])
            ));
        }

            return response($results);
    }












    public function ChartsNew($unit)
    {
       // $unit = \Request::input('id');
        //dd($unit);
        $prod = 'something';



        $data3 = Productivity::where('cost_center', '=',$unit )->first();

        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'Viewed Productivity Chart';
        $history->userid = \Auth::user()->username;
        $history->search_string = $unit .' - '.$data3->cost_center_description;
        $history->user_ip = \Request::ip();
        $history->save();

        //Pull a current list of accurate cost centers
        $OrgUnit = CostCenters::orderby('style2')->pluck('style2', 'cost_center');

        if(input::get('newucl') != ''){
            $uclValue = input::get('newucl');
        }else{
            $uclValue = 4;
        }

        if(input::get('newlcl') != ''){
            $lclValue = input::get('newlcl');
        }else{
            $lclValue = 4;
        }


        if(input::get('newmean') != ''){
            $data2 = input::get('newmean');
            $oldmean = Productivity::where('cost_center', '=',$unit )->where('total_hours','!=',0)->get()->AVG('total_hours');
            //DD('New Mean!!');
        }else{
            $data2 = Productivity::where('cost_center', '=',$unit )->where('total_hours','!=',0)->get()->AVG('total_hours');
            $oldmean = $data2;
        }

        //Lets count the number or records returned
        $countData = Productivity::where('cost_center', '=',$unit )->where('total_hours','!=',0)->get()->Count('pd_uos');
        //Calculate the step diff For fixed uses total hours
        $stDev = Productivity::select(DB::raw("STDEV(total_hours) / SQRT($countData) as stDev"))->where('cost_center', '=',$unit )->where('total_hours','!=',0)->first();
        //Set the collection to a value
        $standardDev = $stDev->stDev;

        $data3 = Productivity::where('cost_center', '=',$unit )->first();
        $data = Productivity::select("search_date","cost_center", "unit_measure", "cost_center_description", "pd_uos", "total_hours","dayofweek","overtime")
            ->where('cost_center', '=',$unit )
            ->where('total_hours','!=',0)
            ->orderBy('search_date')
            ->get();

        $data4 = Productivity::select("search_date","cost_center", "unit_measure", "cost_center_description", "uos_hours","total_hours","fte_hourly","fte_salary","pd_uos","dayofweek","overtime")
            ->where('cost_center', '=',$unit )
            ->orderBy('search_date')
            ->get();

        //This is where we will calculate the standard deviation and square root
        $ucl = round((((($standardDev*3)+$data2)/$uclValue)+$data2),2);
        $lcl = round((((($standardDev*3)-$data2)/$lclValue)+$data2),2);
        $mean = round($data2,2);

        //$result[] = ['Date ','Productivity','Goal '.$mean,'UCL '.$ucl,'LCL '.$lcl, 'Over Time'];

        $results = array(
            'cols' => array (
                array('label' => 'Date', 'type' => 'date'),
                array('label' => 'Hours', 'type' => 'number')
            ),
            'rows' => array()
        );

        foreach ($data as $key => $value) {


            $searchDate = Carbon::parse($value['search_date'])->format('m-d-Y');
            $dateArr = explode('-', $searchDate);
            $year = $dateArr[2];
            $month = $dateArr[0]-1; // subtract 1 because javascript uses a zero-based index for months
            $day = $dateArr[1];
            $results['rows'][] = array('c' => array(
                array('v' => "Date($year, $month, $day)"),
                array('v' => $value['total_hours'])
            ));

            //$result[++$key] = [$value->search_date, (float)$value->total_hours, (float)$mean, (float)$ucl, (float)$lcl, (float)$value->overtime];


        }

        //dd($results);
        return response($results);
        //return view ('productivity.charts2',compact('results'));

//return View('productivity.charts2', compact('OrgUnit','cost_center','prod', 'data','unit_measure','results','data3','data2','results2','result','totalcost','totalfixed','totaluos'
   // ,'startdate','enddate','totaldays','oldmean','uclValue','lclValue'));
    }

    public function ChartsNew2()
    {
        return View('productivity.charts2');
    }
    
    
    
    
    
    public function printChart()
    {
        DB::statement('SET ANSI_NULLS ON; SET ANSI_WARNINGS ON');

        $unit = Input::get('unit');
        
        $prod = 'something';



        $data3 = Productivity::where('cost_center', '=',$unit )->first();

        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'Viewed Productivity Chart';
        $history->userid = \Auth::user()->username;
        $history->search_string = $unit .' - '.$data3->cost_center_description;
        $history->user_ip = \Request::ip();
        $history->save();

        //Pull a current list of accurate cost centers
        $OrgUnit = CostCenters::orderby('style2')->pluck('style2', 'cost_center');

        if(input::get('newucl') != ''){
            $uclValue = input::get('newucl');
        }else{
            $uclValue = 4;
        }

        if(input::get('newlcl') != ''){
            $lclValue = input::get('newlcl');
        }else{
            $lclValue = 4;
        }


        //Ok because some cost centers have units of measure and others are fixed we need to decide how to plot
        //the data depending on the unit of measure.
        if($data3->unit_measure == 'Fixed')
        {
            //Fixed Cost Center
            //Lets get the mean
            if(input::get('newmean') != ''){
                $data2 = input::get('newmean');
                $oldmean = Productivity::where('cost_center', '=',$unit )->where('total_hours','!=',0)->get()->AVG('total_hours');
                //DD('New Mean!!');
            }else{
                $data2 = Productivity::where('cost_center', '=',$unit )->where('total_hours','!=',0)->get()->AVG('total_hours');
                $oldmean = $data2;
            }

            //Lets count the number or records returned
            $countData = Productivity::where('cost_center', '=',$unit )->where('total_hours','!=',0)->get()->Count('pd_uos');
            //Calculate the step diff For fixed uses total hours
            $stDev = Productivity::select(DB::raw("STDEV(total_hours) / SQRT($countData) as stDev"))->where('cost_center', '=',$unit )->where('total_hours','!=',0)->first();
            //Set the collection to a value
            $standardDev = $stDev->stDev;

            $data3 = Productivity::where('cost_center', '=',$unit )->first();
            $data = Productivity::select("search_date","cost_center", "unit_measure", "cost_center_description", "pd_uos", "total_hours","dayofweek","overtime")
                ->where('cost_center', '=',$unit )
                ->where('total_hours','!=',0)
                ->orderBy('search_date')
                ->get();

            $data4 = Productivity::select("search_date","cost_center", "unit_measure", "cost_center_description", "uos_hours","total_hours","fte_hourly","fte_salary","pd_uos","dayofweek","overtime")
                ->where('cost_center', '=',$unit )
                ->orderBy('search_date')
                ->get();

            //This is where we will calculate the standard deviation and square root
            $ucl = round((((($standardDev*3)+$data2)/$uclValue)+$data2),2);
            $lcl = round((((($standardDev*3)-$data2)/$lclValue)+$data2),2);
            $mean = round($data2,2);
            
            $results = array(
                'cols' => array (
                    array('label' => 'Date', 'type' => 'date'),
                    array('label' => 'Productivity', 'type' => 'number'),
                    array('label' => 'Goal '.$mean, 'type' => 'number'),
                    array('label' => 'UCL '.$ucl, 'type' => 'number'),
                    array('label' => 'LCL '.$lcl, 'type' => 'number'),
                    array('label' => 'Over Time', 'type' => 'number')
                ),
                'rows' => array()
            );

            foreach ($data as $key => $value) {

                $searchDate = Carbon::parse($value['search_date'])->format('m-d-Y');
                $dateArr = explode('-', $searchDate);
                $year = $dateArr[2];
                $month = $dateArr[0] - 1; // subtract 1 because javascript uses a zero-based index for months
                $day = $dateArr[1];
                $results['rows'][] = array('c' => array(
                    array('v' => "Date($year, $month, $day)"),
                    array('v' => $value['total_hours']),
                    array('v' => $mean),
                    array('v' => $ucl),
                    array('v' => $lcl),
                    array('v' => $value['overtime']),
                ));

            }

        }else{
            //Lets get the adverage

            if(input::get('newmean') != ''){
                $data2 = input::get('newmean');
                $oldmean = Productivity::where('cost_center', '=',$unit )->where('uos_pd', '>',0 )->get()->AVG('uos_pd');
                //DD('New Mean!!');
            }else{
                $data2 = Productivity::where('cost_center', '=',$unit )->where('uos_pd', '>',0 )->get()->AVG('uos_pd');
                $oldmean = $data2;
            }

            //Lets count the number or records returned
            $countData = Productivity::where('cost_center', '=',$unit )->where('uos_pd', '>',0 )->get()->Count('uos_pd');
            //Calculate the step diff
            $stDev = Productivity::select(DB::raw("STDEV(uos_hours) / SQRT($countData) as stDev"))->where('cost_center', '=',$unit )->where('uos_pd', '>',0 )->first();
            //Set the collection to a value
            $standardDev = $stDev->stDev;

            $data3 = Productivity::where('cost_center', '=',$unit )->first();

            $data = Productivity::select("search_date","cost_center", "unit_measure", "cost_center_description", "uos_pd", "total_hours","dayofweek","overtime")
                ->where('cost_center', '=',$unit )
                ->where('uos_pd', '>',0 )
                ->orderBy('search_date')
                ->get();
            //$queries = DB::getQueryLog();
            //dd($queries);

            $data4 = Productivity::select("search_date","cost_center", "unit_measure", "cost_center_description", "uos_hours","total_hours","fte_hourly","fte_salary","uos_pd","dayofweek","overtime")
                ->where('cost_center', '=',$unit )
                ->where('uos_pd', '>',0 )
                ->orderBy('search_date')
                ->get();

            //This is where we will calculate the standard deviation and square root
            $ucl = round((((($standardDev*3)+$data2)/$uclValue)+$data2),2);
            $lcl = round((((($standardDev*3)-$data2)/$lclValue)+$data2),2);
            $mean = round($data2,2);

            $results = array(
                'cols' => array (
                    array('label' => 'Date', 'type' => 'date'),
                    array('label' => 'Productivity', 'type' => 'number'),
                    array('label' => 'Goal '.$mean, 'type' => 'number'),
                    array('label' => 'UCL '.$ucl, 'type' => 'number'),
                    array('label' => 'LCL '.$lcl, 'type' => 'number'),
                    array('label' => 'Over Time', 'type' => 'number')
                ),
                'rows' => array()
            );

            foreach ($data as $key => $value) {

                $searchDate = Carbon::parse($value['search_date'])->format('m-d-Y');
                $dateArr = explode('-', $searchDate);
                $year = $dateArr[2];
                $month = $dateArr[0] - 1; // subtract 1 because javascript uses a zero-based index for months
                $day = $dateArr[1];
                $results['rows'][] = array('c' => array(
                    array('v' => "Date($year, $month, $day)"),
                    array('v' => $value['uos_pd']),
                    array('v' => $mean),
                    array('v' => $ucl),
                    array('v' => $lcl),
                    array('v' => $value['overtime']),
                ));

            }

        }

        //Turn the results into a json string
        $results = json_encode($results);
        //$results2 = json_encode($result2);

        //$pdf = PDF::loadView('productivity.print', $OrgUnit,  $prod, $data,  $results, $data3, $data2, $oldmean, $uclValue, $lclValue);
        //return $pdf->download('invoice.pdf');

       //$pdf = PDF::loadView('productivity.print', compact('OrgUnit','cost_center','prod', 'data','unit_measure','results','data3','data2','results2','result','totalcost','totalfixed','totaluos'
        //  ,'startdate','enddate','totaldays','oldmean','uclValue','lclValue'));
        //return $pdf->download('invoice.pdf');

        return View('productivity.print', compact('OrgUnit','cost_center','prod', 'data','unit_measure','results','data3','data2','results2','result','oldmean','uclValue','lclValue'));


    }



    public function chart2($id)
    {
        DB::statement('SET ANSI_NULLS ON; SET ANSI_WARNINGS ON');

        $unit = $id;
        $prod = 'something';



        $data3 = Productivity::where('cost_center', '=',$unit )->first();

        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'Viewed Productivity Chart';
        $history->userid = \Auth::user()->username;
        $history->search_string = $unit .' - '.$data3->cost_center_description;
        $history->user_ip = \Request::ip();
        $history->save();

        //Pull a current list of accurate cost centers
        $OrgUnit = CostCenters::orderby('style2')->pluck('style2', 'cost_center');

        if(input::get('newucl') != ''){
            $uclValue = input::get('newucl');
        }else{
            $uclValue = 4;
        }

        if(input::get('newlcl') != ''){
            $lclValue = input::get('newlcl');
        }else{
            $lclValue = 4;
        }


        //Ok because some cost centers have units of measure and others are fixed we need to decide how to plot
        //the data depending on the unit of measure.
        if($data3->unit_measure == 'Fixed')
        {
            //Fixed Cost Center
            //Lets get the mean
            if(input::get('newmean') != ''){
                $data2 = input::get('newmean');
                $oldmean = Productivity::where('cost_center', '=',$unit )->where('total_hours','!=',0)->get()->AVG('total_hours');
                //DD('New Mean!!');
            }else{
                $data2 = Productivity::where('cost_center', '=',$unit )->where('total_hours','!=',0)->get()->AVG('total_hours');
                $oldmean = $data2;
            }

            //Lets count the number or records returned
            $countData = Productivity::where('cost_center', '=',$unit )->where('total_hours','!=',0)->get()->Count('pd_uos');
            //Calculate the step diff For fixed uses total hours
            $stDev = Productivity::select(DB::raw("STDEV(total_hours) / SQRT($countData) as stDev"))->where('cost_center', '=',$unit )->where('total_hours','!=',0)->first();
            //Set the collection to a value
            $standardDev = $stDev->stDev;

            $data3 = Productivity::where('cost_center', '=',$unit )->first();
            $data = Productivity::select("search_date","cost_center", "unit_measure", "cost_center_description", "pd_uos", "total_hours","dayofweek","overtime")
                ->where('cost_center', '=',$unit )
                ->where('total_hours','!=',0)
                ->orderBy('search_date')
                ->get();

            $data4 = Productivity::select("search_date","cost_center", "unit_measure", "cost_center_description", "uos_hours","total_hours","fte_hourly","fte_salary","pd_uos","dayofweek","overtime")
                ->where('cost_center', '=',$unit )
                ->orderBy('search_date')
                ->get();

            //This is where we will calculate the standard deviation and square root
            $ucl = round((((($standardDev*3)+$data2)/$uclValue)+$data2),2);
            $lcl = round((((($standardDev*3)-$data2)/$lclValue)+$data2),2);
            $mean = round($data2,2);

            $result[] = ['Date ','Productivity','Goal '.$mean,'UCL '.$ucl,'LCL '.$lcl, 'Over Time'];
            foreach ($data as $key => $value) {
                $result[++$key] = [$value->search_date, (float)$value->total_hours, (float)$mean, (float)$ucl, (float)$lcl, (float)$value->overtime];
            }
            $result2[] = ['Date','Cost Center','Measure','Description','UOS/FTE','Total Hours','FTE Hourly','FTE Salary','ADV HRS/UOS*','UCL*','LCL*','Day of Week','Over Time'];
            foreach ($data4 as $key => $value) {
                $result2[++$key] = [$value->search_date, $value->cost_center, $value->unit_measure, $value->cost_center_description,
                    (float)$value->pd_uos, (float)$value->total_hours, (int)$value->fte_hourly, (int)$value->fte_salary, (float)$mean,
                  (float)$ucl, (float)$lcl,$value->dayofweek, (float)$value->overtime];
            }


        }else{
            //Lets get the adverage



            if(input::get('newmean') != ''){
                $data2 = input::get('newmean');
                $oldmean = Productivity::where('cost_center', '=',$unit )->where('uos_pd', '>',0 )->get()->AVG('uos_pd');
                //DD('New Mean!!');
            }else{
                $data2 = Productivity::where('cost_center', '=',$unit )->where('uos_pd', '>',0 )->get()->AVG('uos_pd');
                $oldmean = $data2;
            }

            //Lets count the number or records returned
            $countData = Productivity::where('cost_center', '=',$unit )->where('uos_pd', '>',0 )->get()->Count('uos_pd');
            //Calculate the step diff
            $stDev = Productivity::select(DB::raw("STDEV(uos_hours) / SQRT($countData) as stDev"))->where('cost_center', '=',$unit )->where('uos_pd', '>',0 )->first();
            //Set the collection to a value
            $standardDev = $stDev->stDev;

            $data3 = Productivity::where('cost_center', '=',$unit )->first();

            $data = Productivity::select("search_date","cost_center", "unit_measure", "cost_center_description", "uos_pd", "total_hours","dayofweek","overtime")
                ->where('cost_center', '=',$unit )
                ->where('uos_pd', '>',0 )
                ->orderBy('search_date')
                ->get();
            //$queries = DB::getQueryLog();
            //dd($queries);

            $data4 = Productivity::select("search_date","cost_center", "unit_measure", "cost_center_description", "uos_hours","total_hours","fte_hourly","fte_salary","uos_pd","dayofweek","overtime")
                ->where('cost_center', '=',$unit )
                ->where('uos_pd', '>',0 )
                ->orderBy('search_date')
                ->get();

            //This is where we will calculate the standard deviation and square root
            $ucl = round((((($standardDev*3)+$data2)/$uclValue)+$data2),2);
            $lcl = round((((($standardDev*3)-$data2)/$lclValue)+$data2),2);
            $mean = round($data2,2);

            //Ok this cost center has a unit of measurement so lets chart that.... (waves wand... POOOF!) eat your heart out Harry Potter....
            $result[] = ['Date','Productivity','Goal '.$mean,'UCL '.$ucl,'LCL '.$lcl,'Over Time'];
            foreach ($data as $key => $value) {
                $result[++$key] = [$value->search_date .' '. $value->dayofweek, (float)$value->uos_pd, (float)$mean, (float)$ucl, (float)$lcl, (float)$value->overtime];
            }
            $result2[] = ['Date','Cost Center','Measure','Description','UOS/FTE','Total Hours','FTE Hourly','FTE Salary','ADV HRS/UOS*','UCL*','LCL*','Day of Week','Over Time'];
            foreach ($data4 as $key => $value) {
                $result2[++$key] = [$value->search_date, $value->cost_center, $value->unit_measure, $value->cost_center_description,
                   (float)$value->uos_pd, (float)$value->total_hours, (int)$value->fte_hourly, (int)$value->fte_salary, (float)$mean, (float)$ucl,
                    (float)$lcl,$value->dayofweek, (float)$value->overtime];
            }



            // dd($results);

        }

        //Turn the results into a json string
        $results = json_encode($result);
        $results2 = json_encode($result2);

        return View('productivity.charts2', compact('OrgUnit','cost_center','prod', 'data','unit_measure','results','data3','data2','results2','result','totalcost','totalfixed','totaluos'
            ,'startdate','enddate','totaldays','oldmean','uclValue','lclValue'));
    }
    
    public function pushprint()
    {
        //$OrgUnit = CostCenters::pluck('cost_center');
        $OrgUnit = CostCenters::select('cost_center', 'cost_center_description')->get();
        //dd($OrgUnit);

        return View("productivity.print", compact('OrgUnit'));
    }
}
