<?php

namespace App\Http\Controllers;

use App\Census;
use App\DuplicateUsers;
use App\History;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use App\Http\Requests;
use App\Termination;
use App\ExtraMile;
use Carbon\Carbon;
use App\User;

class WidgetController extends Controller
{
    public function terminations(){
        $terms = Termination::WhereNull('termination_date')
            ->where('last_day','>=', Carbon::today()->toDateString())
            ->WhereNotNull('WebForm')
            ->Orderby('last_day')
            ->get();
        //DD($terms);
        return View('widgets.Termination', compact('terms'));
    }

    public function extramile(){
        $extramile = ExtraMile::with('receiverInfo','presenterInfo')
            ->take(20)
            ->orderby('created_at','DESC')
            ->get();
        // DD($term);
        return View('widgets.ExtraMile', compact('extramile'));
    }
    
    public function sgmcBar(){

        $totalemployees = User::where('emp_status','=','Active')->count();
        $totalSal = User::where('personneltype','=','Salaried/Exempt')->where('emp_status','=','Active')->count();
        $totalHr = User::where('personneltype','=','Hourly/Non-Exempt')->where('emp_status','=','Active')->count();
        $totalPRN = User::where('workstatus','=','PRN')->where('emp_status','=','Active')->count();
        $totalFT = User::where('workstatus','=','FT')->where('emp_status','=','Active')->count();
        $totalmale = User::where('gender','=','Male')->where('emp_status','=','Active')->count();
        $totalfemale = User::where('gender','=','Female')->where('emp_status','=','Active')->count();
        $campus_SGMC = User::where('location','=','SGMC')->where('emp_status','=','Active')->count();
        $campus_BERRIEN = User::where('location','=','BERRIEN')->where('emp_status','=','Active')->count();
        $campus_LANIER = User::where('location','=','LANIER')->where('emp_status','=','Active')->count();
        $totalRN = User::whereNotNull('RN')->where('emp_status','=','Active')->count();
        $totalDC = User::whereNotNull('DC')->where('emp_status','=','Active')->count();
        return View('widgets.SGMCbar', compact('totalemployees','totalmale','totalfemale','campus_SGMC','campus_BERRIEN',
            'campus_LANIER','totalFT','totalSal','totalHr','totalRN','totalDC', 'totalPRN'));
    }

        public function duplicate_users(){

            $dupes = DuplicateUsers::orderby('employee_number')
                ->orderby('id')
                ->get();

            return View('widgets.dupusers', compact('dupes'));
        }

        public function remove_dups($id){

            $user = User::find($id);

            $user->deleted_at = Carbon::now();
            $user->save();

            $history = new History();
            $history->id = \Uuid::generate(4);
            $history->action = 'Delete Dupes';
            $history->userid = \Auth::user()->username;
            $history->search_string = $id;
            $history->user_ip = \Request::ip();
            $history->save();

            return redirect('/home');

        }

}
