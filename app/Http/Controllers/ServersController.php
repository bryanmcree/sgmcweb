<?php

namespace App\Http\Controllers;

use App\Servers;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;

class ServersController extends Controller
{
    public function DeleteServer($id)
    {
        $server = Servers::find( $id );
        $server->delete();
        $systems_id = Input::get('system_id');
        return redirect('/systemlist/' . $systems_id);
    }
}
