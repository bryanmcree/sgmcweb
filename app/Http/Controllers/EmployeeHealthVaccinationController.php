<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;

use App\Http\Requests;
use App\EmployeeHealthTST;

class EmployeeHealthVaccinationController extends Controller
{
    public function createTST(Request $request)
    {   
        $request->offsetSet('id', Uuid::generate(4));
        

        if(empty($request->result_mm))
        {
            $request->offsetSet('result_mm', 0.00);
            $newTST = $request->all();
            EmployeeHealthTST::create($newTST);
        }
        else
        {
            $newTST = $request->all();
            EmployeeHealthTST::create($newTST);
        }
        
        alert()->success('TST Recorded!', 'Success!');
        return redirect()->back();
    }

    public function viewTST($id)
    {   
        $view = EmployeeHealthTST::find($id);

        return view('employee_health/vacResults/showTST', compact('view'));
    }

    public function editTST(Request $request)
    {
        $tst = EmployeeHealthTST::find($request->id);

        if(empty($request->result_mm))
        {
            $request->offsetSet('result_mm', 0.00);

            $tst->id = $request->id;
            $tst->name = $request->name;
            $tst->EE = $request->EE;
            $tst->date_given = $request->date_given;
            $tst->given_by = $request->given_by;
            $tst->prescription = $request->prescription;
            $tst->lot = $request->lot;
            $tst->exp_date = $request->exp_date;
            $tst->site = $request->site;
            $tst->date_read = $request->date_read;
            $tst->read_by = $request->read_by;
            $tst->result = $request->result;
            $tst->result_mm = $request->result_mm;
            $tst->created_by = $request->created_by;
        }
        else
        {
            $tst->id = $request->id;
            $tst->name = $request->name;
            $tst->EE = $request->EE;
            $tst->date_given = $request->date_given;
            $tst->given_by = $request->given_by;
            $tst->prescription = $request->prescription;
            $tst->lot = $request->lot;
            $tst->exp_date = $request->exp_date;
            $tst->site = $request->site;
            $tst->date_read = $request->date_read;
            $tst->read_by = $request->read_by;
            $tst->result = $request->result;
            $tst->result_mm = $request->result_mm;
            $tst->created_by = $request->created_by;
        }
        
        $tst->save();

        alert()->success('TST Updated!', 'Success!');
        return redirect()->back();
    }
}
