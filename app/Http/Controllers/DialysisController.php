<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\DialysisLog;
use App\DialysisSurveillance;
use Carbon;
use Uuid;
use App\History;

class DialysisController extends Controller
{
    public function index()
    {
        $ius = DialysisLog::selectRaw('citric_IS, count(*) as total')
            ->where('date', '>=', Carbon::now()->subDays(30))
            ->groupby('citric_IS')
            ->get();
        // dd($ius);

        $immune = DialysisLog::where('citric_IS', '=', 'Immune')
            ->where('date', '>=', Carbon::now()->subDays(30))
            ->count();

        $susceptible = DialysisLog::where('citric_IS', '=', 'Susceptible')
            ->where('date', '>=', Carbon::now()->subDays(30))
            ->count();

        $unknown = DialysisLog::where('citric_IS', '=', 'Unknown')
            ->where('date', '>=', Carbon::now()->subDays(30))
            ->count();

        $MD = DialysisLog::selectRaw('HD_MD, count(*) as total')
            ->where('date', '>=', Carbon::now()->subDays(30))
            ->groupby('HD_MD')
            ->get();
        // dd($MD);

        $Chiang = DialysisLog::where('HD_MD', '=', 'Chiang')
            ->where('date', '>=', Carbon::now()->subDays(30))
            ->count();

        $McCleish = DialysisLog::where('HD_MD', '=', 'McCleish')
            ->where('date', '>=', Carbon::now()->subDays(30))
            ->count();

        $Urbonas = DialysisLog::where('HD_MD', '=', 'Urbonas')
            ->where('date', '>=', Carbon::now()->subDays(30))
            ->count();

        $firstT = DialysisLog::selectRaw('first_treatment')
            ->where('date', '>=', Carbon::now()->subDays(30))
            ->where('first_treatment', '=', 1)
            ->count();
        // dd($firstT);

        $failedT = DialysisLog::selectRaw('failed_treatment')
            ->where('date', '>=', Carbon::now()->subDays(30))
            ->where('failed_treatment', '=', 1)
            ->count();
        // dd($failedT);

        $AMA = DialysisLog::selectRaw('AMA')
            ->where('date', '>=', Carbon::now()->subDays(30))
            ->where('AMA', '=', 1)
            ->count();
        // dd($firstT);

        $fourHours = DialysisLog::selectRaw('tx_time')
            ->where('date', '>=', Carbon::now()->subDays(30))
            ->where('tx_time', '>=', '04:00:00.00000')
            ->count();

        $txHerapin = DialysisLog::selectRaw('tx_herapin')
            ->where('date', '>=', Carbon::now()->subDays(30))
            ->where('tx_herapin', '=', 1)
            ->count();
        // dd($txHerapin);

        $txNaturalyte = DialysisLog::selectRaw('tx_cistrate')
            ->where('date', '>=', Carbon::now()->subDays(30))
            ->where('tx_cistrate', '=', 'Naturalyte')
            ->count();
        // dd($txNaturalyte);

        $fistulas = DialysisLog::selectRaw('fis_cath')
            ->where('date', '>=', Carbon::now()->subDays(30))
            ->where('fis_cath', '=', 'Fistula')
            ->count();
        // dd($fistulas);

        $catheters = DialysisLog::selectRaw('fis_cath')
            ->where('date', '>=', Carbon::now()->subDays(30))
            ->where('fis_cath', '=', 'Catheter')
            ->count();
        // dd($catheters);

        $clotted = DialysisLog::selectRaw('clotted_tx')
            ->where('date', '>=', Carbon::now()->subDays(30))
            ->where('clotted_tx', '=', '1')
            ->count();
        // dd($clotted);

        $logs = DialysisLog::wherenotnull('id')
            ->where('date', '>=', Carbon::now()->subDays(30))
            ->orderby('date', 'desc')
            ->get();

        $surveillance = DialysisSurveillance::wherenotnull('id')
            ->where('date', '>=', Carbon::now()->subDays(30))
            ->orderby('date', 'desc')
            ->get();

        $avgTreatment = DialysisLog::selectRaw('cast(cast(avg(cast(CAST(tx_time as datetime) as float)) as datetime) as time) treatment_length, MONTH(date) as submitted_month, YEAR(date) as submitted_year')
            ->wherenotnull('id')
            ->orderby(\DB::raw('year(date)'))
            ->orderby(\DB::raw('month(date)'))
            ->groupby([\DB::raw('month(date)'), \DB::raw('year(date)')])
            ->get();
            
        $nurses = User::select('name')->where('emp_status', 'active')->wherenotnull('id')->orderby('name')->get();

        $history = new History();
        $history->id = \Uuid::generate(4);
        $history->action = 'Viewed Dialysis Dashboard';
        $history->userid = \Auth::user()->username;
        $history->search_string = 'None';
        $history->user_ip = \Request::ip();
        $history->save();

        return view('dialysis.index', compact('logs', 'surveillance', 'ius', 'MD', 'firstT', 'txHerapin', 'txNaturalyte', 'fourHours',
                                              'immune', 'susceptible', 'unknown', 'Chiang', 'McCleish', 'Urbonas', 'nurses', 'fistulas', 
                                              'catheters', 'clotted', 'avgTreatment', 'failedT', 'AMA'));
    }

    public function create()
    {
        return view('dialysis.log.create');
    }

    public function store(Request $request)
    {
        $start = new Carbon($request->start);
        $stop =new Carbon($request->stop);
        $tx_time = $stop->diff($start)->format('%H:%I:%S'); // Response Time Calculated

        $request->offsetSet('id', Uuid::generate(4));
        $request->offsetSet('tx_time', $tx_time);

        DialysisLog::create($request->all());

        alert()->success('Log Updated', 'Success!');
        return redirect('/dialysis');
    }

    public function show()
    {
        $logs = DialysisLog::wherenotnull('id')
            ->orderby('date', 'desc')
            ->paginate(10);

        return view('dialysis.log.show', compact('logs'));
    }

    public function edit(DialysisLog $log)
    {
        return view('dialysis.log.edit', compact('log'));
    }

    public function update(Request $request, DialysisLog $log)
    {
        $start = new Carbon($request->start);
        $stop =new Carbon($request->stop);
        $tx_time = $stop->diff($start)->format('%H:%I:%S'); // Response Time Calculated

        $request->offsetSet('tx_time', $tx_time);

        $log->update($request->all());

        alert()->success('Log Updated', 'Success!');
        return redirect('/dialysis');
    }

    public function destroy(DialysisLog $log)
    {
        $log->delete();
    }

    public function report(Request $request)
    {
        $startDate = Carbon::parse($request->startDate)->format('Y-m-d H:i:s');
        $endDate = Carbon::parse($request->endDate)->format('Y-m-d H:i:s');

        $ius = DialysisLog::selectRaw('citric_IS, count(*) as total')
            ->whereBetween('date', [$startDate, $endDate])
            ->groupby('citric_IS')
            ->get();
        // dd($ius);

        $immune = DialysisLog::where('citric_IS', '=', 'Immune')
            ->whereBetween('date', [$startDate, $endDate])
            ->count();

        $susceptible = DialysisLog::where('citric_IS', '=', 'Susceptible')
            ->whereBetween('date', [$startDate, $endDate])
            ->count();

        $unknown = DialysisLog::where('citric_IS', '=', 'Unknown')
            ->whereBetween('date', [$startDate, $endDate])
            ->count();

        $MD = DialysisLog::selectRaw('HD_MD, count(*) as total')
            ->whereBetween('date', [$startDate, $endDate])
            ->groupby('HD_MD')
            ->get();
        // dd($MD);

        $Chiang = DialysisLog::where('HD_MD', '=', 'Chiang')
            ->whereBetween('date', [$startDate, $endDate])
            ->count();

        $McCleish = DialysisLog::where('HD_MD', '=', 'McCleish')
            ->whereBetween('date', [$startDate, $endDate])
            ->count();

        $Urbonas = DialysisLog::where('HD_MD', '=', 'Urbonas')
            ->whereBetween('date', [$startDate, $endDate])
            ->count();

        $firstT = DialysisLog::selectRaw('first_treatment')
            ->whereBetween('date', [$startDate, $endDate])
            ->where('first_treatment', '=', 1)
            ->count();
        // dd($firstT);

        $failedT = DialysisLog::selectRaw('failed_treatment')
            ->whereBetween('date', [$startDate, $endDate])
            ->where('failed_treatment', '=', 1)
            ->count();
        // dd($failedT);

        $AMA = DialysisLog::selectRaw('AMA')
            ->whereBetween('date', [$startDate, $endDate])
            ->where('AMA', '=', 1)
            ->count();
        // dd($firstT);

        $fourHours = DialysisLog::selectRaw('tx_time')
            ->whereBetween('date', [$startDate, $endDate])
            ->where('tx_time', '>=', '04:00:00.00000')
            ->count();

        $txHerapin = DialysisLog::selectRaw('tx_herapin')
            ->whereBetween('date', [$startDate, $endDate])
            ->where('tx_herapin', '=', 1)
            ->count();
        // dd($txHerapin);

        $txNaturalyte = DialysisLog::selectRaw('tx_cistrate')
            ->whereBetween('date', [$startDate, $endDate])
            ->where('tx_cistrate', '=', 'Naturalyte')
            ->count();
        // dd($txCistrate);

        $fistulas = DialysisLog::selectRaw('fis_cath')
            ->whereBetween('date', [$startDate, $endDate])
            ->where('fis_cath', '=', 'Fistula')
            ->count();
        // dd($fistulas);

        $catheters = DialysisLog::selectRaw('fis_cath')
            ->whereBetween('date', [$startDate, $endDate])
            ->where('fis_cath', '=', 'Catheter')
            ->count();
        // dd($catheters);

        $clotted = DialysisLog::selectRaw('clotted_tx')
            ->whereBetween('date', [$startDate, $endDate])
            ->where('clotted_tx', '=', '1')
            ->count();
        // dd($clotted);

        $logs = DialysisLog::wherenotnull('id')
            ->whereBetween('date', [$startDate, $endDate])
            ->orderby('date', 'desc')
            ->get();

        $surveillance = DialysisSurveillance::wherenotnull('id')
            ->whereBetween('date', [$startDate, $endDate])
            ->orderby('date', 'desc')
            ->get();

        return view('dialysis.report', compact('logs', 'surveillance', 'ius', 'MD', 'firstT', 'txHerapin', 'txNaturalyte', 'fourHours',
                                              'immune', 'susceptible', 'unknown', 'Chiang', 'McCleish', 'Urbonas', 'startDate', 'endDate',
                                               'fistulas', 'catheters', 'clotted', 'failedT', 'AMA'));
    }

}
