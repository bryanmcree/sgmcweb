<?php

namespace App\Http\Controllers;

use App\RoleUser;
use App\User;
use Webpatser\Uuid\Uuid;
use App\Http\Requests;
use Image;
use DB;
use App\State;
use App\Role;
use App\Http\Controllers\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use App\History;
use App\LemName;
use App\LemPillar;
use App\LemGoal;
use App\LemValue;

class LemController extends Controller
{
    public function index(){
        $names = LemName::all();
        $evals = LemValue::select("emp_id","lem_id","evaluator")->distinct()->with('evaluated','lemName','evalBy')->get();
        //dd($evals);
        return view('lem.index', compact('names','evals'));
    }
    
    public function addEval(Requests\SystemListRequest $request){

        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();
        LemName::create($input);


        $pillar = new LemPillar;
        $pillar->id = Uuid::generate(4);
        $pillar->lem_id = $request->id;
        $pillar->pillar_name = 'People';
        $pillar->pillar_percent = 10;
        $pillar->created_by = \Auth::user()->id;
        $pillar->save();

        $pillar = new LemPillar;
        $pillar->id = Uuid::generate(4);
        $pillar->lem_id = $request->id;
        $pillar->pillar_name = 'Service';
        $pillar->pillar_percent = 15;
        $pillar->created_by = \Auth::user()->id;
        $pillar->save();

        $pillar = new LemPillar;
        $pillar->id = Uuid::generate(4);
        $pillar->lem_id = $request->id;
        $pillar->pillar_name = 'Growth';
        $pillar->pillar_percent = 20;
        $pillar->created_by = \Auth::user()->id;
        $pillar->save();

        $pillar = new LemPillar;
        $pillar->id = Uuid::generate(4);
        $pillar->lem_id = $request->id;
        $pillar->pillar_name = 'Quality';
        $pillar->pillar_percent = 10;
        $pillar->created_by = \Auth::user()->id;
        $pillar->save();

        $pillar = new LemPillar;
        $pillar->id = Uuid::generate(4);
        $pillar->lem_id = $request->id;
        $pillar->pillar_name = 'Financial';
        $pillar->pillar_percent = 45;
        $pillar->created_by = \Auth::user()->id;
        $pillar->save();
        
        alert()->success('LEM Evaluation has been added!', 'Success!');

        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'New LEM Evaluation';
        $history->userid = \Auth::user()->username;
        $history->search_string = '';
        $history->user_ip = \Request::ip();
        $history->save();

        return redirect('/lem');
    }

    public function showEval($id){
        $eval = LemName::with('lemPillars.lemGoals')->find($id);
        //dd($eval);
        return view('lem.ViewEval', compact('eval'));
    }

    public function addPillar(Requests\SystemListRequest $request){
        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();
        LemPillar::create($input);
        alert()->success('Servers / Devices / Equipment has been Added!', 'Success!');
        return redirect('/lem/eval/' . $request->lem_id);
    }

    public function editPillar(){
        $pillar = LemPillar::find( Input::get('id') );
        $pillar->update(Input::only(['pillar_name','pillar_percent','created_by']));
        alert()->success('Pillar Updatedsl', 'Success!');
        return redirect('/lem/eval/' . Input::get('lem_id'));
    }

    public function addGoal(Requests\SystemListRequest $request){
        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();
        LemGoal::create($input);
        alert()->success('LEM Evaluation has been added!', 'Success!');
        return redirect('/lem/eval/' . $request->lem_id);
    }

    public function deletePillar($id){
        //have to delete in two places
        //First Goals
        $goals = LemGoal::where('pillar_id','=',$id)->get();
        //dd($goals);
        if(!$goals->isEmpty()){
            //dd($goals);
            $goals = LemGoal::where('pillar_id','=',$id)->delete();
        }

        //THEN Pillar
        $pillar = LemPillar::find($id);
        $pillar->delete();

        return redirect('/lem/' . Input::get('eval'));
    }

    public function newEval(Requests\SystemListRequest $request){

        //$eval = LemName::with('lemPillars.lemGoals')->where('id','=', $request->eval_id)->get();
        $eval = LemName::with('lemPillars.lemGoals')->find($request->eval_id);
        $emp = User::find($request->emp_id);
        $effective_date = $request->effective_date;
        //dd($eval);
        return view('lem.NewEval', compact('eval','emp','effective_date'));
    }

    public function create($id){
        $emp = User::find($id);
        $eval = LemName::all();
        return View('lem.create', compact('emp','eval'));
    }
    
    public function storeEval(Requests\SystemListRequest $request){

        $input = $request->only('lem_id','effective_date','emp_id','pillar_id','goal_id','actual_value','eval_notes');
        //dd($input);

        $lem_id = $input['lem_id'];
        $effective_date = $input['effective_date'];
        $emp_id = $input['emp_id'];
        //$pillar_id = $input['pillar_id'];
        $goal_id = $input['goal_id'];
        $actual_value = $input['actual_value'];
        $eval_notes = $input['eval_notes'];


        foreach( $goal_id as $key => $n ) {
            DB::table('lem_values')->insert(
                array(

                    'id' => Uuid::generate(4),
                    'emp_id' =>$emp_id,
                    'lem_id' =>$lem_id,
                    //'pillar_id' =>$pillar_id[$key],
                    'goal_id' =>$goal_id[$key],
                    'effective_date' =>$effective_date,
                    'actual_value' =>$actual_value[$key],
                    'evaluator' =>\Auth::user()->id
                )
            );
        }
        alert()->success('LEM Evaluation saved!', 'Success!');
        return redirect('/lem');
    }
}
