<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

use App\Stats;
use App\History;
use App\StatsReportView;
use App\StatsValues;
use App\StatsView;
use Carbon\Carbon;
use App\StatsMissingValueView;
use App\StatsMonthlyVarView;
use App\StatsReportAxiomView;

class StatsController extends Controller
{
    public function index()
    {
        $history = new History();
        $history->id = \Uuid::generate(4);
        $history->action = 'Viewed Stat Application';
        $history->userid = \Auth::user()->username;
        $history->user_ip = \Request::ip();
        $history->search_string = 'None';
        $history->save();

        $numOfStats = Stats::wherenotnull('id')->count();

        $numOfAuto = Stats::where('auto_fill', 1)->count();

        $numOfCalc = Stats::where('calc_field', 1)->count();

        $numOfPrimary = Stats::where('flex_stat', 1)->orWhere('finance_stat', 1)->count();

        $numOfFlex = Stats::where('flex_stat', 1)->count();

        $numOfFinance = Stats::where('finance_stat', 1)->count();

        $validatedStats = Stats::wherenotnull('validated_date')->orWhere('validated_date', '<>', '')->get();

        $invalidStats = Stats::where(function ($query) {
            $query->wherenull('validated_by')
                    ->orWhere('validated_by', '=', '');
                })
            ->where(function ($query) {
                $query->where('auto_fill', 1)
                        ->orWhere('calc_field', 1);
                    })
            ->get();

        $flaggedStats = Stats::where('flag_review', 1)->get();

        $unlinkedStats = Stats::wherenull('stat_link')->orWhere('stat_link', '=', '')->get();

        $deactivatedStats = Stats::onlyTrashed()->get();

        $missingStats = StatsMissingValueView::wherenotnull('id')->orderby('campus')->get();

        $outOfVar = StatsMonthlyVarView::wherenotnull('stat_id')->orderby('stat_type')->get();

        $stats_report_dates = StatsValues::select('start_date')
            ->where('start_date','>=', Carbon::now()->subMonth(4)->StartofMonth())
            ->where('start_date','<=', Carbon::now()->StartofMonth())
            ->orderby('start_date')
            ->distinct()
            ->get();

        $stats = Stats::wherenotnull('id')
            ->orderby('stat_description')
            ->paginate(13);
            
        // dd($outOfVar);

        return view('stats.index', compact('numOfStats', 'validatedStats', 'invalidStats', 'flaggedStats', 'unlinkedStats', 'deactivatedStats',
                                           'numOfAuto', 'numOfCalc', 'numOfPrimary', 'numOfFlex', 'numOfFinance', 'missingStats', 'outOfVar', 'stats_report_dates', 'stats'));
    }

    public function detail($id){

        $stat_detail = Stats::where('id','=', $id)
            ->first();

        $stat_detail_values = StatsValues::where('stat_link','=', $stat_detail->stat_link)
            ->orderby('start_date')
            ->get();

        // dd($stat_detail_values, $stat_detail);

        $stat_detail_values_chart = StatsValues::where('stat_link','=', $stat_detail->stat_link)
            ->where('start_date','>=', Carbon::now()->subMonths(14) )
            ->orderby('start_date')
            ->get();

        $stat_avg = $stat_detail_values_chart->avg('value');

        $stat_values_chart_monthly = StatsValues::where('stat_link','=', $stat_detail->stat_link)
            ->where('start_date','>=', Carbon::now()->subMonths(3) )
            ->orderby('start_date')
            ->get();

        $stat_values_chart_yearly = StatsValues::where('stat_link','=', $stat_detail->stat_link)
            ->where('start_date','=', Carbon::now()->subMonths(13)->startOfMonth() )
            ->orderby('start_date')
            ->first();

        // dd($stat_values_chart_monthly, $stat_values_chart_yearly);

        $stat_previous = StatsValues::where('stat_link','=', $stat_detail->stat_link)
            ->where('start_date','>=', Carbon::now()->subMonths(3) )
            ->orderby('start_date')
            ->first();

        $stat_current = StatsValues::where('stat_link','=', $stat_detail->stat_link)
            ->where('start_date','>=', Carbon::now()->subMonths(2) )
            ->orderby('start_date')
            ->first();

        $stat_prior_year = StatsValues::where('stat_link','=', $stat_detail->stat_link)
            ->where('start_date','>=', Carbon::now()->subMonths(14) )
            ->orderby('start_date')
            ->first();

        // dd($stat_previous, $stat_current);

        $last12 = StatsValues::selectRaw('MIN(value) as min, MAX(value) as max, AVG(value) as avg')
            ->where('stat_link','=', $stat_detail->stat_link)
            ->where('start_date','>=', Carbon::now()->subMonths(13)->startOfMonth())
            ->first();

        // Set Fiscal Year
        $FY = Carbon::create()->startOfMonth()->month(10);

        if(Carbon::now() < $FY)
        {
            $FY = Carbon::create()->startOfMonth()->month(10)->subYear(1);

            // dd($FY);
        }

        $lastFY = StatsValues::selectRaw('MIN(value) as min, MAX(value) as max, AVG(value) as avg')
            ->where('stat_link','=', $stat_detail->stat_link)
            ->where('start_date','>=', $FY)
            ->first();

        // dd($last12, $lastFY);


        if(is_null($stat_current) OR $stat_current->value == 0 OR is_null($stat_previous) OR $stat_previous->value == 0){
            $stat_var = 0;
        }else{
            $stat_var = (($stat_current->value - $stat_previous->value)/$stat_previous->value)*100;
        }

        if(is_null($stat_prior_year) OR $stat_prior_year->value == 0 OR is_null($stat_current) OR $stat_current->value == 0){
            $stat_var_year = 0;
        }else{
            $stat_var_year = (($stat_current->value - $stat_prior_year->value)/$stat_prior_year->value)*100;
        }


        return view('stats.detail', compact('stat_detail','stat_detail_values','stat_detail_values_chart','stat_avg',
            'stat_previous','stat_current','stat_var','stat_prior_year','stat_var_year', 'stat_values_chart_monthly', 'stat_values_chart_yearly',
            'last12', 'lastFY'));
    }

    public function create()
    {
        return view('stats.create');
    }

    public function store(Request $request)
    {
        Stats::create($request->all());

        alert()->success('Stat Created!', 'Success!');
        return redirect()->back();
    }

    public function edit(Stats $stat)
    {
        return view('stats.edit', compact('stat'));
    }

    public function update(Request $request, Stats $stat)
    {

        if($request->stat_validated == 1){
            $request->offsetSet('validated_date', Carbon::now());
            $request->offsetSet('validated_by', \Auth::user()->employee_number);
        }

        if($request->stat_validated == 0){
            $request->offsetSet('validated_date', null);
            $request->offsetSet('validated_by', null);
        }

        $stat->update($request->except('stat_validated'));
        alert()->success('Stat Updated!', 'Success!');
        return redirect()->route('statDetail', $stat->id);
    }

    public function destroy(Stats $stat)
    {
        $stat->delete();
    }

    public function flagged()
    {
        $flagged = Stats::where('flag_review', '=', '1')
            ->orderby('stat_type')
            ->get();

        return view('stats.flagged', compact('flagged'));
    }

    public function trashed()
    {
        $trashed = Stats::onlyTrashed()->get();

        // dd($trashed);

        return view('stats.trashed', compact('trashed'));
    }

    public function auto()
    {
        $autos = Stats::where('auto_fill', 1)->orderby('stat_description')->get();

        $stats_report_dates = StatsValues::select('start_date')
            ->where('start_date','>=', Carbon::now()->subMonth(2)->StartofMonth())
            ->where('start_date','<=', Carbon::now()->StartofMonth())
            ->orderby('start_date')
            ->distinct()
            ->get();


        return view('stats.auto', compact('autos', 'stats_report_dates'));
    }

    public function calculated()
    {
        $calculated = Stats::where('calc_field', 1)->orderby('stat_description')->get();

        $stats_report_dates = StatsValues::select('start_date')
            ->where('start_date','>=', Carbon::now()->subMonth(2)->StartofMonth())
            ->where('start_date','<=', Carbon::now()->StartofMonth())
            ->orderby('start_date')
            ->distinct()
            ->get();

        
        return view('stats.calculated', compact('calculated', 'stats_report_dates'));
    }

    public function empty()
    {
        $emptyStats = StatsMissingValueView::wherenotnull('id')->orderby('stat_description')->get();

        return view('stats.empty', compact('emptyStats'));
    }

    public function flexStats()
    {
        $flex = Stats::where('flex_stat', 1)
            ->orderby('stat_type')
            ->get();

        $flexValues = StatsValues::selectRaw('value, stat_link')->where('start_date', Carbon::now()->subMonth(1)->StartofMonth())
            ->get();

            // dd($flexValues);

        return view('stats.flex', compact('flex', 'flexValues'));
    }

    public function financeStats()
    {
        $finance = Stats::where('finance_stat', 1)
            ->orderby('stat_type')
            ->get();

        $outOfVar = StatsMonthlyVarView::where('finance_stat', 1)->orderby('stat_type')->get();

        $stats_report_dates = StatsValues::select('start_date')
            ->where('start_date','>=', Carbon::now()->subMonth(2)->StartofMonth())
            ->where('start_date','<=', Carbon::now()->StartofMonth())
            ->orderby('start_date')
            ->distinct()
            ->get();

        return view('stats.finance', compact('finance', 'stats_report_dates', 'outOfVar'));
    }

    public function primaryStats()
    {
        $primary = Stats::where('finance_stat', 1)
            ->orWhere('flex_stat', 1)
            ->orderby('stat_type')
            ->get();

        $stats_report_dates = StatsValues::select('start_date')
            ->where('start_date','>=', Carbon::now()->subMonth(1)->StartofMonth())
            ->where('start_date','<=', Carbon::now()->StartofMonth())
            ->orderby('start_date')
            ->distinct()
            ->get();

        return view('stats.primary', compact('primary', 'stats_report_dates'));
    }

    public function zeroStat(Stats $stat)
    {
        if($stat->zero_stat == 0)
        {
            $stat->update([
                'zero_stat' => 1
            ]);
        }
        else
        {
            $stat->update([
                'zero_stat' => 0
            ]);
        }

        alert()->success('Stat Updated!', 'Success!');
        return back();
    }

    public function restore($id)
    {
        Stats::withTrashed()->find($id)->restore();

        alert()->success('Stat Restored!', 'Success!');
        return redirect('/stats');
    }

    public function search()
    {
        $search = Input::get('search');

        $data = Stats::where(function($query) use ($search){
                $query->Where('campus', 'LIKE', '%' . $search . '%');
                $query->orWhere('stat_type', 'LIKE', '%' . $search . '%');
                $query->orWhere('stat_description', 'LIKE', '%' . $search . '%');
                $query->orWhere('dept1', 'LIKE', '%' . $search . '%');
                $query->orWhere('concatenate', 'LIKE', '%' . $search . '%');
                $query->orWhere('campus_number', 'LIKE', '%' . $search . '%');
                $query->orWhere('dept2', 'LIKE', '%' . $search . '%');
            });

        $results = $data
            ->wherenull('deleted_at')
            ->orderBy('stat_type')
            ->orderBy('campus')
            ->paginate(10000)
            ->appends(['search' => $search]);

        $report_dates = StatsValues::select('start_date')
            ->where('start_date','>=', Carbon::now()->subMonth(4)->StartofMonth())
            //->where('start_date','>=','10/1/'. Carbon::now()->subYear(1)->year)
            //->where('start_date','<=','9/30/'. Carbon::now()->year)
            ->where('start_date','<=', Carbon::now()->StartofMonth())
            ->groupby('start_date')
            ->orderby('start_date')
            ->get();

        return View('stats.search', compact('results', 'report_dates'));

    }

    public function unlinked_stats_auto(){
        //Lets get all SGMC Stats
        $stats_report_dates = StatsValues::select('start_date')
            ->where('start_date','>=', Carbon::now()->subMonth(4)->StartofMonth())
            ->where('start_date','<=', Carbon::now()->StartofMonth())
            ->orderby('start_date')
            ->distinct()
            //->take(6)
            ->get();

        //dd($stats_report_dates);

        $stats = Stats::wherenotnull('id')
            //->wherein('id',[487,328, 329])
            //->wherenotnull('stat_link')
            ->wherenull('stat_link')
            ->where('auto_fill','=', 1)
            ->where('calc_field','=', 0)
            ->orderby('stat_description')
            //->paginate(25);
            ->get();
        // dd($stats);

        return View('stats.report_unlinked_auto', compact('stats', 'stats_report_dates'));
    }

    public function variance(){

        //Lets get the last two months stats so we can compaire them
        $stats_report_dates = StatsValues::select('start_date')
            ->where('start_date','>=', Carbon::now()->subMonth(4)->StartofMonth())
            ->where('start_date','<=', Carbon::now()->StartofMonth())
            ->orderby('start_date','desc')
            ->distinct()
            ->take(2)
            ->get();
        //dd($stats_report_dates);

        $stats = Stats::wherenotnull('id')
            //->wherein('id',[487,328, 329])
            //->wherenotnull('stat_link')
            ->wherenull('stat_link')
            ->where('auto_fill','=', 1)
            ->where('calc_field','=', 0)
            ->orderby('stat_description')
            //->paginate(25);
            ->get();
        //dd($stats);
    }

    public function download_stats(){

        //Because this uses a view with linked servers.
        $handle = \DB::connection()->getPdo();
        $handle->exec('SET QUOTED_IDENTIFIER ON');
        $handle->exec('SET ANSI_WARNINGS ON');
        $handle->exec('SET ANSI_PADDING ON');
        $handle->exec('SET ANSI_NULLS ON');
        $handle->exec('SET CONCAT_NULL_YIELDS_NULL ON');

        $stats = StatsReportView::wherenotnull('org')->get();

        Excel::create('SGMC_Stats '.date('m-d-Y_hia'), function($excel) use($stats) {
            $excel->sheet('New sheet', function($sheet) use($stats) {

                //formats the F column to string
                $sheet->setColumnFormat(array(
                    'F' => '@',
                ));

                //Adds Zeros back in null fields
                $sheet->fromArray($stats, null, 'A1', true);

                //This is where the pane freeze happens
                $sheet->setFreeze('K2');

                //These are hidden columns
                $sheet->getColumnDimension('A')->setVisible(false);
                $sheet->getColumnDimension('B')->setVisible(false);
                $sheet->getColumnDimension('C')->setVisible(false);
                $sheet->getColumnDimension('D')->setVisible(false);
                $sheet->getColumnDimension('E')->setVisible(false);
                $sheet->getColumnDimension('F')->setVisible(false);
                // $sheet->getColumnDimension('G')->setVisible(false);

                // More hidden after some visiable ones
                // $sheet->getColumnDimension('K')->setVisible(false);
                // $sheet->getColumnDimension('L')->setVisible(false);
                // $sheet->getColumnDimension('M')->setVisible(false);
                // $sheet->getColumnDimension('N')->setVisible(false);
                // $sheet->getColumnDimension('O')->setVisible(false);

                //change sheet font
                //$sheet->setFontFamily('Comic Sans MS');

                //sets the top row a background color
                $sheet->row(1, ['Col 1', 'Col 2', 'Col 3']); // etc etc
                $sheet->row(1, function($row) { $row->setBackground('#CCCCCC'); });
                //Sets top row bold
                $sheet->cells('A1:AZ1', function($cells) {
                    $cells->setFont(array(
                        'bold'       =>  true
                    ));
                });
                //$sheet->setAutoFilter('A1:E10');

            })->download('xlsx');
        });
    }



    public function download_stats_axiom(){

        //Because this uses a view with linked servers.
        $handle = \DB::connection()->getPdo();
        $handle->exec('SET QUOTED_IDENTIFIER ON');
        $handle->exec('SET ANSI_WARNINGS ON');
        $handle->exec('SET ANSI_PADDING ON');
        $handle->exec('SET ANSI_NULLS ON');
        $handle->exec('SET CONCAT_NULL_YIELDS_NULL ON');

        $stats = StatsReportAxiomView::wherenotnull('key')->get();

        Excel::create('SGMC_Stats '.date('m-d-Y_hia'), function($excel) use($stats) {
            $excel->sheet('New sheet', function($sheet) use($stats) {

                //Adds Zeros back in null fields
                $sheet->fromArray($stats, null, 'A1', true);

                //This is where the pane freeze happens
                $sheet->setFreeze('K2');

                //sets the top row a background color
                $sheet->row(1, ['Col 1', 'Col 2', 'Col 3']); // etc etc
                $sheet->row(1, function($row) { $row->setBackground('#CCCCCC'); });

                //Sets top row bold
                $sheet->cells('A1:AZ1', function($cells) {
                    $cells->setFont(array(
                        'bold'       =>  true
                    ));
                });

            })->download('xlsx');
        });
    }
}
