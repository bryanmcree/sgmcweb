<?php

namespace App\Http\Controllers;

use App\History;
use App\Project;
use App\SubTask;
use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Webpatser\Uuid\Uuid;
use App\Http\Requests;

class ProjectController extends Controller
{
    public function index(){

        //$projects = Project::with('projectTask')->get();
        $projects = Project::get();

        $completed_projects = Project::wherenotnull('completed_at')
            ->get();

        //dd($projects);

        return view('project.index', compact('projects','completed_projects'));
    }

    public function newProject(Requests\SystemListRequest $request){

        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();
        //Input has been created lets save it to the database.
        Project::create($input);
        //$project_id = Project::create($input)->id;


        //Lets write this action to the history table.
        $history = new History();
        $history->id = Uuid::generate(4);
        $history->action = 'New Project';
        $history->userid = \Auth::user()->username;
        $history->search_string = $request->project_name;
        $history->user_ip = \Request::ip();
        $history->save();

        //Everything is good lets send feedback to the user and then redirect
        //alert()->success('Cost Center Added!', 'Success!');
        return redirect('/project/' . $request->id);

    }

    public function viewProject($id){

        $project = Project::find($id);
        $tasks = Task::where('project_id','=',$id)
            ->get();

        if(Input::get('task') != NULL){
            $selected_task = Task::where('id','=',Input::get('task'))
                ->first();
            //dd(Input::get('task'));
            $sub_task = SubTask::where('task_id','=',Input::get('task'))
                ->get();
        }else{
            $selected_task = NULL;
        }

        return view('project.view', compact('project','tasks','selected_task','sub_task'));
    }

    public function shareProject($id){

        $project = Project::find($id);
        $tasks = Task::where('project_id','=',$id)
            ->get();

        if(Input::get('task') != NULL){
            $selected_task = Task::where('id','=',Input::get('task'))
                ->first();
            //dd(Input::get('task'));
        }else{
            $selected_task = NULL;
        }

        return view('project.share', compact('project','tasks','selected_task'));
    }
}
