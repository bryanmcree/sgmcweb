<?php

namespace App\Http\Controllers;

use App\Handwashing;
use App\HandwashingAnswers;
use App\HandwashingComplete;
use App\HandwashingCostCenters;
use App\HandwashingReportAudit;
use App\History;
use Illuminate\Http\Request;
use App\HandwashingQuestions;

use App\Http\Requests;
use Webpatser\Uuid\Uuid;
use Image;
use DB;
use App\State;
use App\Role;
use App\Http\Controllers\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;

class HandwashingController extends Controller
{
    //New Refactor

    public function new_index(){
        //Refactored by Bryan McRee 11/28/2018
        //Get all CC that are required to do handwashing
        $hs_cost_centers = HandwashingCostCenters::wherenotnull('cost_center')
            ->orderby('cost_center')
            ->get();
        //dd($hs_cost_centers);

        //Get all the questions for the survey
        $questions = HandwashingQuestions::wherenotnull('id')
            ->orderby('created_at','DESC')
            ->get();
        //dd($questions);

        //These queries will help provide metrics regarding completed surveys
        $monthly_questions = HandwashingAnswers::where('created_at', '>=', Carbon::now()->startOfMonth())->count('id');
        $monthly_yes = HandwashingAnswers::where('created_at', '>=', Carbon::now()->startOfMonth())->sum('yes');
        $monthly_no = HandwashingAnswers::where('created_at', '>=', Carbon::now()->startOfMonth())->sum('no');
        $monthly_na = HandwashingAnswers::where('created_at', '>=', Carbon::now()->startOfMonth())->sum('na');

        $monthly_handwashing = HandwashingAnswers::whereHas('categoryType', function ($query) {
            $query->where('category','Handwashing');
        })
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->count('id');

        $monthly_handwashing_no = HandwashingAnswers::whereHas('categoryType', function ($query) {
            $query->where('category','Handwashing');
        })
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->sum('no');

        $monthly_cdc = HandwashingAnswers::whereHas('categoryType', function ($query) {
            $query->where('category','OSHA/CDC');
        })
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->count('id');

        $monthly_cdc_no = HandwashingAnswers::whereHas('categoryType', function ($query) {
            $query->where('category','OSHA/CDC');
        })
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->sum('no');

        $monthly_cdc_na = HandwashingAnswers::whereHas('categoryType', function ($query) {
            $query->where('category','OSHA/CDC');
        })
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->sum('na');

        //First of the month there will be no surveys complete, need to make sure we don't get an error.
        if(!is_null($monthly_no)){
            $hospital_score = 100-(number_format((float)($monthly_no/($monthly_questions-$monthly_na))*100,2,'.',''));
            $handwashing_score = 100-(number_format((float)($monthly_handwashing_no/($monthly_handwashing))*100,2,'.',''));
            $osha_score = 100-(number_format((float)($monthly_cdc_no/($monthly_cdc-$monthly_cdc_na))*100,2,'.',''));
        }else{
            $hospital_score = 0;
            $handwashing_score = 0;
            $osha_score = 0;
        }
        //dd($hospital_score);

        return view('handwashing2.index', compact('questions','hs_cost_centers','monthly_cdc',
            'monthly_cdc_na','monthly_cdc_no','monthly_handwashing','monthly_handwashing_no','monthly_na','monthly_no',
            'monthly_questions','monthly_yes','hospital_score','handwashing_score','osha_score'));

    }

    public function new_admin(){
        //Refactored by Bryan McRee 11/28/2018
        //First show me the cost centers, who is assigned to them, and how many surveys they must complete this month.
        $hs_cost_centers = HandwashingCostCenters::wherenotnull('cost_center')
            ->orderby('cost_center')
            ->get();

        //These queries will help provide metrics regarding completed surveys
        $monthly_questions = HandwashingAnswers::where('created_at', '>=', Carbon::now()->startOfMonth())->count('id');
        $monthly_yes = HandwashingAnswers::where('created_at', '>=', Carbon::now()->startOfMonth())->sum('yes');
        $monthly_no = HandwashingAnswers::where('created_at', '>=', Carbon::now()->startOfMonth())->sum('no');
        $monthly_na = HandwashingAnswers::where('created_at', '>=', Carbon::now()->startOfMonth())->sum('na');

        $monthly_handwashing = HandwashingAnswers::whereHas('categoryType', function ($query) {
            $query->where('category','Handwashing');
        })
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->count('id');

        $monthly_handwashing_no = HandwashingAnswers::whereHas('categoryType', function ($query) {
            $query->where('category','Handwashing');
        })
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->sum('no');

        $monthly_cdc = HandwashingAnswers::whereHas('categoryType', function ($query) {
            $query->where('category','OSHA/CDC');
        })
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->count('id');

        $monthly_cdc_no = HandwashingAnswers::whereHas('categoryType', function ($query) {
            $query->where('category','OSHA/CDC');
        })
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->sum('no');

        $monthly_cdc_na = HandwashingAnswers::whereHas('categoryType', function ($query) {
            $query->where('category','OSHA/CDC');
        })
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->sum('na');

        //First of the month there will be no surveys complete, need to make sure we don't get an error.
        if(!is_null($monthly_no)){
            $hospital_score = 100-(number_format((float)($monthly_no/($monthly_questions-$monthly_na))*100,2,'.',''));
            $handwashing_score = 100-(number_format((float)($monthly_handwashing_no/($monthly_handwashing))*100,2,'.',''));
            $osha_score = 100-(number_format((float)($monthly_cdc_no/($monthly_cdc-$monthly_cdc_na))*100,2,'.',''));
        }else{
            $hospital_score = 0;
            $handwashing_score = 0;
            $osha_score = 0;
        }
        //dd($hospital_score);

        //We need to know who has NOT done their surveys this month.  This is done by a view called handwashing_complete.  Boy this is ugly...
        $incomplete_surveys = DB::select("select ccn.style1 as cost_center, complete, to_do, u.name as manager from handwashing_complete hc
                join handwashing_cost_centers hcc on hcc.cost_center = hc.cost_center 
                join users u on u.employee_number = hcc.employee_number
                join CostCenters_new ccn on ccn.cost_center = hc.cost_center
                where complete < to_do");
        //dd($incomplete_surveys);

        return view('handwashing2.admin.index', compact('questions','hs_cost_centers','monthly_cdc',
            'monthly_cdc_na','monthly_cdc_no','monthly_handwashing','monthly_handwashing_no','monthly_na','monthly_no',
            'monthly_questions','monthly_yes','hospital_score','handwashing_score','osha_score','incomplete_surveys'));
    }


    //OLD but active functions
    public function index(){

        if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
            // Ignores notices and reports all other kinds... and warnings
            error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
            // error_reporting(E_ALL ^ E_WARNING); // Maybe this is enough
        }

        $questions = HandwashingQuestions::orderby('created_at')
            ->get();

        $cost_centers = HandwashingCostCenters::orderby('cost_center_name')
            ->get();

        return view('quality.infection.index', compact('questions','cost_centers'));
    }

    public function dashboard(){

        if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
            // Ignores notices and reports all other kinds... and warnings
            error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
            // error_reporting(E_ALL ^ E_WARNING); // Maybe this is enough
        }

        //Get all CC that are required to do handwashing
        $hs_cost_centers = HandwashingCostCenters::wherenotnull('cost_center')
            ->orderby('cost_center')
            ->get();

        $monthly_questions = HandwashingAnswers::where('created_at', '>=', Carbon::now()->startOfMonth())
            ->count('id');

        $monthly_yes = HandwashingAnswers::where('created_at', '>=', Carbon::now()->startOfMonth())
            ->sum('yes');

        $monthly_no = HandwashingAnswers::where('created_at', '>=', Carbon::now()->startOfMonth())
            ->sum('no');

        $monthly_na = HandwashingAnswers::where('created_at', '>=', Carbon::now()->startOfMonth())
            ->sum('na');


        $monthly_handwashing = HandwashingAnswers::whereHas('categoryType', function ($query) {
            $query->where('category','Handwashing');
        })
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->count('id');

        $monthly_handwashing_no = HandwashingAnswers::whereHas('categoryType', function ($query) {
            $query->where('category','Handwashing');
        })
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->sum('no');

        $monthly_cdc = HandwashingAnswers::whereHas('categoryType', function ($query) {
            $query->where('category','OSHA/CDC');
        })
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->count('id');

        $monthly_cdc_no = HandwashingAnswers::whereHas('categoryType', function ($query) {
            $query->where('category','OSHA/CDC');
        })
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->sum('no');

        $monthly_cdc_na = HandwashingAnswers::whereHas('categoryType', function ($query) {
            $query->where('category','OSHA/CDC');
        })
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->sum('na');

        //dd($monthly_handwashing);

        $questions = HandwashingQuestions::orderby('created_at','DESC')
            ->get();

        $cost_centers = HandwashingCostCenters::with("surveyCount")->orderby('cost_center_name')
            ->get();

        $completed = Handwashing::orderby('created_at','DESC')
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->get();

        //First of the month there will be no surveys complete, need to make sure we don't get an error.
        if(!is_null($monthly_no)){
            $hospital_score = 100-(number_format((float)($monthly_no/($monthly_questions-$monthly_na))*100,2,'.',''));
            $handwashing_score = 100-(number_format((float)($monthly_handwashing_no/($monthly_handwashing))*100,2,'.',''));
            $osha_score = 100-(number_format((float)($monthly_cdc_no/($monthly_cdc-$monthly_cdc_na))*100,2,'.',''));
        }else{
            $hospital_score = 0;
            $handwashing_score = 0;
            $osha_score = 0;
        }

        return view('quality.infection.dashboard', compact('questions','cost_centers','completed',
            'monthly_yes','monthly_na','monthly_no','monthly_questions','monthly_handwashing','monthly_handwashing_no',
            'monthly_cdc','monthly_cdc_no','monthly_cdc_na','hospital_score','handwashing_score','osha_score','hs_cost_centers'));

    }

    public function admin_index(){

        if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
            // Ignores notices and reports all other kinds... and warnings
            error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
            // error_reporting(E_ALL ^ E_WARNING); // Maybe this is enough
        }

        $monthly_questions = HandwashingAnswers::where('created_at', '>=', Carbon::now()->startOfMonth())
            ->count('id');

        $monthly_yes = HandwashingAnswers::where('created_at', '>=', Carbon::now()->startOfMonth())
            ->sum('yes');

        $monthly_no = HandwashingAnswers::where('created_at', '>=', Carbon::now()->startOfMonth())
            ->sum('no');

        $monthly_na = HandwashingAnswers::where('created_at', '>=', Carbon::now()->startOfMonth())
            ->sum('na');


        $monthly_handwashing = HandwashingAnswers::whereHas('categoryType', function ($query) {
            $query->where('category','Handwashing');
        })
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->count('id');

        $monthly_handwashing_no = HandwashingAnswers::whereHas('categoryType', function ($query) {
            $query->where('category','Handwashing');
        })
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->sum('no');

        $monthly_cdc = HandwashingAnswers::whereHas('categoryType', function ($query) {
            $query->where('category','OSHA/CDC');
        })
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->count('id');

        $monthly_cdc_no = HandwashingAnswers::whereHas('categoryType', function ($query) {
            $query->where('category','OSHA/CDC');
        })
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->sum('no');

        $monthly_cdc_na = HandwashingAnswers::whereHas('categoryType', function ($query) {
            $query->where('category','OSHA/CDC');
        })
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->sum('na');

        //dd($monthly_handwashing);

        $questions = HandwashingQuestions::orderby('created_at','DESC')
            ->get();

        $cost_centers = HandwashingCostCenters::with("surveyCount")->orderby('cost_center_name')
            ->get();

        //dd($cost_centers);

        $completed = Handwashing::orderby('created_at','DESC')
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->get();

        //Report Year
        $report_year = HandwashingReportAudit::select('year')
            ->distinct()
            ->get();
            
        //Report Month
        $report_month = HandwashingReportAudit::select('month')
            ->distinct()
            ->orderBy('month')
            ->get();

        $report_cc = HandwashingReportAudit::select('cost_center_number','cost_center')
            ->distinct()
            ->orderby('cost_center_number')
            ->get();

        return view('quality.infection.admin.index', compact('questions','cost_centers','completed',
            'monthly_yes','monthly_na','monthly_no','monthly_questions','monthly_handwashing','monthly_handwashing_no',
            'monthly_cdc','monthly_cdc_no','monthly_cdc_na','report_year','report_month','report_cc'));
    }

    public function add_question(Requests\SystemListRequest $request){

        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();
        HandwashingQuestions::create($input);

        alert()->success('Question has been added!', 'Success!');

        return redirect('/infection/admin');

    }

    public function submit_survey_Anonymous(Requests\HandwashingRequest $request){

        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->only('id','completed_by','cost_center','shift','practitioner','comments');
        Handwashing::create($input);


        //Submit answers
        $arr = Input::get('response');
        $main_id = $request->id;
        $cost_center = $request->cost_center;
        $completed_by = $request->completed_by;


        foreach ($arr as $key => $value) {
            $newAnswer = new HandwashingAnswers();
            if (! is_array( $value )) {
                $newValue = $value['answer'];
            } else {
                $newValue = json_encode($value['answer']);
            }
            //dd($newValue);

            if($newValue =='"Yes"'){
                $newAnswer->yes = 1;
                $newAnswer->no = 0;
                $newAnswer->na = 0;
            }

            if($newValue =='"No"'){
                $newAnswer->yes = 0;
                $newAnswer->no = 1;
                $newAnswer->na = 0;
            }

            if($newValue =='"NA"'){
                $newAnswer->yes = 0;
                $newAnswer->no = 0;
                $newAnswer->na = 1;
            }


            $newAnswer->id = Uuid::generate(4);
            $newAnswer->question_id = $key;
            $newAnswer->main_id = $main_id;
            $newAnswer->cost_center = $cost_center;
            $newAnswer->completed_by = $completed_by;
            $newAnswer->save();

            $answerArray[] = $newAnswer;
        };



        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'Handwashing Survey Completed';
        $history->userid = 'Anonymous';
        $history->user_ip = \Request::ip();
        $history->search_string = $request->cost_center .', '. $request->completed_by;
        $history->save();

        //dd($arr);
        //DB::table('handwashing_answers')->insert($arr);

        alert()->success('Survey Completed!', 'Success!');

        return redirect('/hw');




    }

    public function submit_survey(Requests\HandwashingRequest $request){

        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->only('id','completed_by','cost_center','shift','practitioner','comments');
        Handwashing::create($input);


        //Submit answers
        $arr = Input::get('response');
        $main_id = $request->id;
        $cost_center = $request->cost_center;
        $completed_by = $request->completed_by;


        foreach ($arr as $key => $value) {
            $newAnswer = new HandwashingAnswers();
            if (! is_array( $value )) {
                $newValue = $value['answer'];
            } else {
                $newValue = json_encode($value['answer']);
            }
            //dd($newValue);

            if($newValue =='"Yes"'){
                $newAnswer->yes = 1;
                $newAnswer->no = 0;
                $newAnswer->na = 0;
            }

            if($newValue =='"No"'){
                $newAnswer->yes = 0;
                $newAnswer->no = 1;
                $newAnswer->na = 0;
            }

            if($newValue =='"NA"'){
                $newAnswer->yes = 0;
                $newAnswer->no = 0;
                $newAnswer->na = 1;
            }


            $newAnswer->id = Uuid::generate(4);
            $newAnswer->question_id = $key;
            $newAnswer->main_id = $main_id;
            $newAnswer->cost_center = $cost_center;
            $newAnswer->completed_by = $completed_by;
            $newAnswer->save();

            $answerArray[] = $newAnswer;
        };



        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'Handwashing Survey Completed';
        $history->userid = \Auth::user()->username;
        $history->user_ip = \Request::ip();
        $history->search_string = $request->cost_center .', '. $request->completed_by;
        $history->save();

         //dd($arr);
        //DB::table('handwashing_answers')->insert($arr);

        alert()->success('Survey Completed!', 'Success!');

        return redirect('/handwashing');




    }

    public function admin_report(){

        $report_month = Input::get('report_month');
        $report_year = Input::get('report_year');
        $report_cc = Input::get('report_cc');

        //dd($report_cc);

        IF($report_cc == 'ALL'){
            $handwashing_audit = HandwashingReportAudit::where('month',$report_month)
                ->where('year',$report_year)
                ->get();

            $handwashing_comments = Handwashing::wherenotnull('comments')
                ->where('comments','<>','')
                ->whereMonth('created_at','=', $report_month)
                ->whereYear('created_at', '=', $report_year)
                ->get();

            $handwashing_stats = HandwashingAnswers::whereMonth('created_at','=', $report_month)
                ->whereYear('created_at', '=', $report_year)
                ->where('question_id','=','2c1fc614-8148-4bb0-9c41-f347175f7a23') //For handwashing ONLY
                ->get();

            $handwashing_stats_CDC = HandwashingAnswers::whereMonth('created_at','=', $report_month)
                ->whereYear('created_at', '=', $report_year)
                ->where('question_id','<>','2c1fc614-8148-4bb0-9c41-f347175f7a23') //For handwashing ONLY
                ->get();

            $handwashing_stats_hospital = HandwashingAnswers::whereMonth('created_at','=', $report_month)
                ->whereYear('created_at', '=', $report_year)
                //->where('question_id','=','2c1fc614-8148-4bb0-9c41-f347175f7a23') //For handwashing ONLY
                ->get();

            $handwashing_responses = HandwashingAnswers::Select('completed_by')
                ->selectRaw(' sum(yes) as yes')
                ->selectRaw(' sum(no) as no')
                ->selectRaw(' sum(na) as na')
                ->selectRaw(' CAST(sum(yes)/count(*) AS DECIMAL(10,2)) as yes_percent')
                ->selectRaw(' count(*) as TotalResponses')
                ->whereMonth('created_at','=', $report_month)
                ->whereYear('created_at', '=', $report_year)
                ->groupby('completed_by')
                ->orderby('yes_percent','desc')
                ->get();

            //dd($handwashing_responses);
        }ELSE{
            $handwashing_audit = HandwashingReportAudit::where('month',$report_month)
                ->where('year',$report_year)
                ->where('cost_center_number', $report_cc)
                ->get();

            $handwashing_comments = Handwashing::wherenotnull('comments')
                ->where('comments','<>','')
                ->whereMonth('created_at','=', $report_month)
                ->whereYear('created_at', '=', $report_year)
                ->where('cost_center', $report_cc)
                ->get();

            $handwashing_stats = HandwashingAnswers::whereMonth('created_at','=', $report_month)
                ->whereYear('created_at', '=', $report_year)
                ->where('question_id','=','2c1fc614-8148-4bb0-9c41-f347175f7a23') //For handwashing ONLY
                ->where('cost_center', $report_cc)
                ->get();

            $handwashing_stats_CDC = HandwashingAnswers::whereMonth('created_at','=', $report_month)
                ->whereYear('created_at', '=', $report_year)
                ->where('question_id','<>','2c1fc614-8148-4bb0-9c41-f347175f7a23') //For handwashing ONLY
                ->where('cost_center', $report_cc)
                ->get();

            $handwashing_stats_hospital = HandwashingAnswers::whereMonth('created_at','=', $report_month)
                ->whereYear('created_at', '=', $report_year)
                //->where('question_id','=','2c1fc614-8148-4bb0-9c41-f347175f7a23') //For handwashing ONLY
                ->where('cost_center', $report_cc)
                ->get();

            $handwashing_responses = HandwashingAnswers::Select('completed_by')
                ->selectRaw(' sum(yes) as yes')
                ->selectRaw(' sum(no) as no')
                ->selectRaw(' sum(na) as na')
                ->selectRaw(' CAST(sum(yes)/count(*) AS DECIMAL(10,2)) as yes_percent')
                ->selectRaw(' count(*) as TotalResponses')
                ->whereMonth('created_at','=', $report_month)
                ->whereYear('created_at', '=', $report_year)
                ->where('cost_center', $report_cc)
                ->groupby('completed_by')
                ->orderby('yes_percent','desc')
                ->get();
        }


        return view('quality.infection.admin.report', compact('handwashing_audit','report_month',
            'report_year','handwashing_comments','handwashing_stats','handwashing_responses','handwashing_stats_CDC',
            'handwashing_stats_hospital'));
    }

    //Printer friendly view of completed survey
    public function view_survey($id){

        $survey = Handwashing::find($id);

        $questions = HandwashingQuestions::get();

        // dd($survey);

        return view('quality.infection.admin.view_survey', compact('survey','questions'));


    }

    public function handwashing_widget(){
        $monthly_questions = HandwashingAnswers::where('created_at', '>=', Carbon::now()->startOfMonth())
            ->count('id');

        $monthly_yes = HandwashingAnswers::where('created_at', '>=', Carbon::now()->startOfMonth())
            ->sum('yes');

        $monthly_no = HandwashingAnswers::where('created_at', '>=', Carbon::now()->startOfMonth())
            ->sum('no');

        $monthly_na = HandwashingAnswers::where('created_at', '>=', Carbon::now()->startOfMonth())
            ->sum('na');


        $monthly_handwashing = HandwashingAnswers::whereHas('categoryType', function ($query) {
            $query->where('category','Handwashing');
        })
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->count('id');

        $monthly_handwashing_no = HandwashingAnswers::whereHas('categoryType', function ($query) {
            $query->where('category','Handwashing');
        })
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->sum('no');

        $monthly_cdc = HandwashingAnswers::whereHas('categoryType', function ($query) {
            $query->where('category','OSHA/CDC');
        })
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->count('id');

        $monthly_cdc_no = HandwashingAnswers::whereHas('categoryType', function ($query) {
            $query->where('category','OSHA/CDC');
        })
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->sum('no');

        $monthly_cdc_na = HandwashingAnswers::whereHas('categoryType', function ($query) {
            $query->where('category','OSHA/CDC');
        })
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->sum('na');


        //First of the month there will be no surveys complete, need to make sure we don't get an error.
        if(!is_null($monthly_no)){
            $hospital_score = 100-(number_format((float)($monthly_no/($monthly_questions-$monthly_na))*100,2,'.',''));
            $handwashing_score = 100-(number_format((float)($monthly_handwashing_no/($monthly_handwashing))*100,2,'.',''));
            $osha_score = 100-(number_format((float)($monthly_cdc_no/($monthly_cdc-$monthly_cdc_na))*100,2,'.',''));
        }else{
            $hospital_score = 0;
            $handwashing_score = 0;
            $osha_score = 0;
        }

        return view('quality.infection.admin.widget', compact('questions','cost_centers','completed',
            'monthly_yes','monthly_na','monthly_no','monthly_questions','monthly_handwashing','monthly_handwashing_no',
            'monthly_cdc','monthly_cdc_no','monthly_cdc_na','hospital_score','handwashing_score','osha_score'));

    }

    public function update(Request $request)
    {
        // dd($request->all());

        Handwashing::where('cost_center', $request->id)
            ->update(['cost_center' => $request->ccNumber]);

        HandwashingAnswers::where('cost_center', $request->id)
            ->update(['cost_center' => $request->ccNumber]);

        HandwashingCostCenters::where('cost_center', $request->id)
            ->update(
                ['cost_center' => $request->ccNumber,
                'cost_center_name' => $request->ccName,
                'employee_number' => $request->ccManager]
            );

        HandwashingReportAudit::where('cost_center_number', $request->id)
            ->update(
                ['cost_center_number' => $request->ccNumber,
                'cost_center' => $request->ccName]
            );

        alert()->success('Cost Center Updated!', 'Success!');
        return back();

    }
}
