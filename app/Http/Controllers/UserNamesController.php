<?php

namespace App\Http\Controllers;

use App\History;
use App\UserNames;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;

class UserNamesController extends Controller
{
    public function index(){



        $history = new History();
        $history->id = \Uuid::generate(4);
        $history->action = 'View User Names Page';
        $history->userid = \Auth::user()->username;
        $history->user_ip = \Request::ip();
        $history->search_string = 'N/A';
        $history->save();

        return View('usernames.index', compact('daily','monthly','ed_running_adv','ed_monthly_adv'));
    }

    public function search(){

        $searched_name = Input::get('user_name');


        $history = new History();
        $history->id = \Uuid::generate(4);
        $history->action = 'Search for user name';
        $history->userid = \Auth::user()->username;
        $history->user_ip = \Request::ip();
        $history->search_string = $searched_name;
        $history->save();


        $usernames = UserNames::where('UserName','=',Input::get('user_name') )
            ->get();

        return View('usernames.search', compact('usernames','searched_name','ed_running_adv','ed_monthly_adv'));
    }
}
