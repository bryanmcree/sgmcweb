<?php

namespace App\Http\Controllers;

use App\PC_Cost_Centers;
use App\pc_positions;
use App\PC_Shifts;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;

class PCController extends Controller
{

    public function index(){

        //Get all the available positions out of the pc_positions table.
        $positions = PC_Cost_Centers::get();

        return view('pc.index', compact('positions','data','dropdown'));
    }

    public function view($id){

        //Select the corrrect cost center
        $positions = pc_positions::where('cost_center','=', $id)
            ->where('status','=','Active')
            ->with('employee')
            ->orderby('job_code')
            ->orderby('position_number')
            ->get();

        //select prn positions
        $positions_prn = pc_positions::where('cost_center','=', $id)
            ->where('status','=','PRN')
            ->with('employee')
            ->orderby('job_code')
            ->orderby('position_number')
            ->get();

        //select prn positions
        $positions_trav = pc_positions::where('cost_center','=', $id)
            ->where('status','=','Traveler')
            ->with('employee')
            ->orderby('job_code')
            ->orderby('position_number')
            ->get();

        //Pass the Cost Center to the view
        $cost_center = PC_Cost_Centers::where('cost_center','=', $id)
            ->first();

        $positions_shift = pc_positions::where('cost_center','=', $id)
            //->where('status','=','Active')
            ->with('employee')
            ->orderby('job_code')
            ->orderby('position_number')
            ->get();
        //dd($cost_center);

        //Lets get all the shifts
        $shifts = PC_Shifts::where('cost_center','=', $id)
            ->orderby('shift_name')
            ->get();


        //Below are stats for the cost center

        $total_RN = pc_positions::where('cost_center','=', $id)
            ->where('lic_type','=','RN')
            ->count();

        return view('pc.view', compact('positions','cost_center','shifts','positions_prn',
            'positions_trav','positions_shift','total_RN'));
    }

    public function assign($id){

        $employee = NULL;
        $cost_center = $id;

        return view('pc.assign', compact('employee','cost_center','dropdown'));
    }

    public function find_employee(Requests\SystemListRequest $request){

        //This is the first process of putting an employee in a seat.

        //Find employee by name selected
        $employee = User::where('name',$request->director)
            ->wherenull('deleted_at')
            ->first();

        if(count($employee) ==0){

            $cost_center = $request->cost_center;
            return view('pc.assign', compact('employee','position','positions','cost_center'))
                ->with('success', 'That employee was not found.  Be sure to "CLICK" on the selected employee.');
        }Else{
            //Find what position this employee is assigned
            $position = pc_positions::where('employee_number','=',$employee->employee_number)
                ->first();

            //Select the corrrect cost center
            $positions = pc_positions::where('cost_center','=', $request->cost_center)
                ->wherenull('employee_number')
                ->with('employee')
                ->orderby('job_code')
                ->orderby('position_number')
                ->get();
            //$input = $request->all();

            $cost_center = $request->cost_center;


            //dd($cost_center);
            return view('pc.assign', compact('employee','position','positions','cost_center'));
        }



    }

    public function assign_employee(Requests\SystemListRequest $request){

        //this process posted the employee to their seat

        $employee = pc_positions::where('id','=', $request->position)
            ->first();
        //dd($request->employee_number);

        $employee->employee_number = $request->employee_number;
        $employee->position_status = 'Filled';
        $employee->save();

        //Everything is good lets send feedback to the user and then redirect
        alert()->success('Position added to shift!', 'Success!');
        return redirect('/pc/' . $request->cost_center);

    }

    public function position_manager($id){

        $positions = pc_positions::where('cost_center','=', $id)
            ->get();

        $cost_center = PC_Cost_Centers::where('cost_center','=', $id)
            ->first();

        return view('pc.positionmanager', compact('employee','position','positions','cost_center'));
    }

    public function update_position(Requests\SystemListRequest $request){

        $position = pc_positions::find($request->id);

        $position->update($request->all());
        alert()->success('Position Updated!', 'Success!');
        return redirect('/pc/pm/' . $request->cost_center);

    }

    public function unassign_employee($id){
        //This process will remove the employee from the seat and set the seat to vacant.

        $employee = pc_positions::find($id);
        //dd($request->employee_number);

        $employee->employee_number = Null;
        $employee->position_status = 'Vacant';
        $employee->save();

        //Everything is good lets send feedback to the user and then redirect
        alert()->success('Employee removed from position.', 'Success!');
        return back();
    }

    public function delete_position($id){

        $position = pc_positions::find( $id );
        //dd($id);
        $position->delete();
        $cost_center = Input::get('cost_center');
        return redirect('/pc/' . $cost_center);

    }
}
