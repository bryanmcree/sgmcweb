<?php

namespace App\Http\Controllers;

use App\SysaidLicenses;
use Illuminate\Http\Request;

use App\Http\Requests;

class LicenseAdobeController extends Controller
{
    public function index(){
        return view('sysaid.licenses.adobe', compact('names','evals'));
    }

    public function add_adobe(Requests\SystemListRequest $request){
        //Add new license entry
        //$request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();
        SysaidLicenses::create($input);
        alert()->success('License Updated!', 'Success!');
        return redirect('/sysaid');

    }
}
