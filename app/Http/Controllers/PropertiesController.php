<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Property;

class PropertiesController extends Controller
{
    public function index()
    {
        $properties = Property::wherenotnull('id')->get();

        return view('properties.index', compact('properties'));
    }
}
