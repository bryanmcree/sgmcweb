<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;

use App\Http\Requests;
use App\SecurityBadges;
use App\SecurityCitation;
use App\SecurityActivityList;
use App\SecurityLocationList;
use App\SecurityDispatch;
use App\SecurityRounds;
use App\SecurityViolation;
use App\SecurityIncident;
use App\SecurityIncidentList;
use App\User;
use App\History;
use Auth;
use Carbon;
use DB;

class SGMCSecurityIncidentController extends Controller
{
    public function index()
    {
        $incidents = SecurityIncidentList::wherenotnull('id')->get();
        $locations = SecurityLocationList::wherenotnull('id')->get();

        $userIncidents = SecurityIncident::where('user_name', '=', Auth::user()->name)
            ->get();

        $allIncidents = SecurityIncident::wherenotnull('id')
            ->orderby('datetime', 'desc')
            ->get();

        return view('sgmcsecurity.incident', compact('incidents', 'locations', 'userIncidents', 'allIncidents'));
    }

    public function store(Request $request)
    {
        $file_gen = SecurityIncident::withTrashed()->wherenotnull('id')->count();
        // dd($file_gen);
        $request->offsetSet('id', Uuid::generate(4));
        $request->offsetSet('datetime', Carbon::parse($request->datetime)->format('Y-m-d H:i:s'));
        $request->offsetSet('file_num', Carbon::now()->format('m/d/y') . ' -' . $file_gen);

        //dd($request->all());

        $incident = $request->all();

        SecurityIncident::create($incident);

        alert()->success('Incident Recorded!', 'Success!');
        return redirect()->back();
    }

    public function show(SecurityIncident $incident)
    {
        return view('sgmcsecurity.show.incident', compact('incident'));
    }

    public function edit(SecurityIncident $incident)
    {
        $incidents = SecurityIncidentList::wherenotnull('id')->get();
        $locations = SecurityLocationList::wherenotnull('id')->get();

        return view('sgmcsecurity.edit.incident', compact('incident', 'incidents', 'locations'));
    }

    public function update(Request $request, SecurityIncident $incident)
    {
        $request->offsetSet('datetime', Carbon::parse($request->datetime)->format('Y-m-d H:i:s'));
        $incident->update($request->all());

        alert()->success('Incident Updated!', 'Success!');
        return redirect('/security');
    }

    public function destroy(SecurityIncident $incident)
    {
        $incident->delete();
    }

}
