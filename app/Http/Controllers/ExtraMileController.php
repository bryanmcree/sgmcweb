<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Mail;
use App\User;
use \Auth;
use App\Employee;
use Yajra\Datatables\Datatables;
use App\ExtraMile;
use App\History;
use Webpatser\Uuid\Uuid;
use \PDF;
use Session;
use App;
use Carbon;
use Excel;

class ExtraMileController extends Controller
{
    public function index()
    {
        phpinfo();

        //$employee = Employee::where('Status','=','Active')->orderBy('last_name')->get();
        $users = User::Where('emp_status','=','Active')
        ->orderBy('last_name')->get();

        //return View('extramile.index', compact('users'));
        //return View('extramile.certificate', compact('users'));
    }

    public function requestAccess()
    {
        phpinfo();
        return View('extramileTemp.request');
    }

    public function create($id)
    {
        $employee = Employee::where('emp_number','=', $id)->get();
        //dd($employee);
        return View('extramileTemp.create', compact('employee'));
    }

    public function newExtraMile($id)
    {
        $user = User::find($id);
        return View('extramile.new', compact('user'));

    }

    public function store(Requests\ExtraMileRequest $request)
    {
        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();
        ExtraMile::create($input);

        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'Extra Mile Created';
        $history->userid = \Auth::user()->username;
        $history->search_string = 'none';
        $history->user_ip = \Request::ip();
        $history->save();

        //dd($input);
        alert()->success('Extra Mile Saved!  The certificate download should start in a moment.', 'COMPLETE!')->persistent('Close');

        Session::flash('AddedExtraMile', '/extramile/download/'. $request->id);
        return redirect('/search');
        //return redirect('/extramile/download/'. $request->id);
    }

    public function sendRequest(Requests\ExtraMileRequest $request) {

        $data = $request->all();

        Mail::send('email.extramileRequest', $data, function ($m) use ($data) {

            $m->from('sgmcweb-no-reply@web.sgmc.org', 'No Reply');
            $m->to('bryan.mcree@sgmc.org', 'HelpDesk');
            $m->subject('Request Access to ExtraMile');
            $m->CC('sghelpdesk@sgmc.org');
            $m->replyTo($data['email']);
        });
        return redirect('/')->with('msg', 'Your request for access to the ExtraMile site has been sent');
    }

    public function getIndex()
    {
        return view('extramileTemp.index');
    }

    public function anyData()
    {
        return Datatables::of(Employee::where('Status','=','Active'))->make(true);
    }

    public function printCertificate($id)
    {


        $data = ExtraMile::with('receiverInfo','presenterInfo')->find($id);
        $pdf = PDF::loadView('extramile.certificate', compact('data'))->setPaper('a4', 'landscape');
        //$pdf = PDF::loadView('extramile.certificate')->setPaper('a4', 'landscape'); //TEST
        //return $pdf->download('EM_Certificate.pdf');//TEST
        return $pdf->download('EM_Certificate'. $data->created_at .'.pdf');
        //$pdf = App::make('snappy.pdf.wrapper');
        //$pdf->loadHTML('<h1>Test</h1>');
        //return $pdf->inline();
    }

    // Loads all extra miles ever given, for link on home page nav
    public function allExtraMiles()
    {
        $allMiles = ExtraMile::orderby('created_at', 'DESC')
        ->get();

        //dd($allMiles);

        return view('extramile.all', compact('allMiles'));
    }

    public function report(Request $request)
    {
        $startDate = Carbon::parse($request->startDate)->format('Y-m-d H:i:s');
        $endDate = Carbon::parse($request->endDate)->format('Y-m-d H:i:s');

        $reportData = ExtraMile::whereBetween('created_at', [$startDate, $endDate])
            ->orderBy('created_at', 'asc')
            ->get();

        return view('extramile.report', compact('reportData', 'startDate', 'endDate'));
    }

    public function export($start, $end)
    {
        $reportData = ExtraMile::whereBetween('created_at', [$start, $end])
            ->orderBy('created_at', 'asc')
            ->get();

        Excel::create('ExtraMile '.date('m-d-Y_hia'), function($excel) use($reportData) {
            $excel->sheet('New sheet', function($sheet) use($reportData) {

                $sheet->getColumnDimension('A')->setVisible(false);
                $sheet->getColumnDimension('B')->setVisible(false);
                $sheet->getColumnDimension('D')->setVisible(false);

                //Adds Zeros back in null fields
                $sheet->fromArray($reportData, null, 'A1', true);

            })->download('xlsx');
        });
    }

}
