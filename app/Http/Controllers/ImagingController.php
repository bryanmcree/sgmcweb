<?php

namespace App\Http\Controllers;

use App\Imaging;
use App\ImagingAnswers;
use App\ImagingCostCenters;
use App\ImagingQuestions;
use Illuminate\Http\Request;

use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Webpatser\Uuid\Uuid;
use Image;
use DB;
use Illuminate\Support\Facades\Mail;

class ImagingController extends Controller
{
    public function index(){

        $areas = ImagingCostCenters::orderby('cost_center_name')
            ->get();

        //dd($areas);

        return view('imaging.index', compact('areas','cost_centers'));
    }

    public function survey(){

        $area = Input::get('area');

        $cost_center_area = ImagingCostCenters::select('cost_center_name')->where('cost_center', $area)
            ->first();

        //dd($cost_center_area->cost_center_name);

        $questions = ImagingQuestions::where('cost_center_name', $cost_center_area->cost_center_name)
            ->get();

        $cost_center_name = $cost_center_area->cost_center_name;

        return view('imaging.survey', compact('questions','area','cost_center_name'));
    }

    public function admin_index(){

        $ir_procedures = ImagingAnswers::whereHas('categoryType', function ($query) {
            $query->where('cost_center_name','IR');
        })
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->count('id');

        $ir_no = ImagingAnswers::whereHas('categoryType', function ($query) {
            $query->where('cost_center_name','IR');
        })
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->sum('no');

        //dd($ir_no);

        $monthly_questions = ImagingAnswers::where('created_at', '>=', Carbon::now()->startOfMonth())
            ->count('id');

        $monthly_yes = ImagingAnswers::where('created_at', '>=', Carbon::now()->startOfMonth())
            ->sum('yes');

        $monthly_no = ImagingAnswers::where('created_at', '>=', Carbon::now()->startOfMonth())
            ->sum('no');

        $monthly_na = ImagingAnswers::where('created_at', '>=', Carbon::now()->startOfMonth())
            ->sum('na');


        $areas = ImagingCostCenters::select('cost_center_name')
            ->orderby('cost_center_name')
            ->distinct()
            ->get();

        $questions = ImagingQuestions::orderby('created_at','DESC')
            ->get();

        $cost_centers = ImagingCostCenters::with("surveyCount")->orderby('cost_center_name')
            ->get();

        $completed = Imaging::orderby('created_at','DESC')
            ->where('created_at', '>=', Carbon::now()->startOfMonth())
            ->get();

        return view('imaging.admin.index', compact('questions','cost_centers','completed','areas',
            'monthly_no','monthly_na','monthly_yes','monthly_questions','ir_no','ir_procedures'));
    }

    public function add_question(Requests\SystemListRequest $request){

        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();
        ImagingQuestions::create($input);

        alert()->success('Question has been added!', 'Success!');

        return redirect('/imaging/admin');

    }

    public function submit_survey(Requests\SystemListRequest $request){

        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->only('id','completed_by','cost_center','accession_number','date_of_service');
        Imaging::create($input);


        //Submit answers
        $arr = Input::get('response');
        $main_id = $request->id;


        foreach ($arr as $key => $value) {
            $newAnswer = new ImagingAnswers();
            if (! is_array( $value )) {
                $newValue = $value['answer'];
            } else {
                $newValue = json_encode($value['answer']);
            }
            //dd($newValue);

            if($newValue =='"Yes"'){
                $newAnswer->yes = 1;
                $newAnswer->no = 0;
                $newAnswer->na = 0;
            }

            if($newValue =='"No"'){
                $newAnswer->yes = 0;
                $newAnswer->no = 1;
                $newAnswer->na = 0;
            }

            if($newValue =='"NA"'){
                $newAnswer->yes = 0;
                $newAnswer->no = 0;
                $newAnswer->na = 1;
            }


            $newAnswer->id = Uuid::generate(4);
            $newAnswer->question_id = $key;
            $newAnswer->main_id = $main_id;

            $newAnswer->save();

            $answerArray[] = $newAnswer;
        };

        //dd($arr);
        //DB::table('handwashing_answers')->insert($arr);

        alert()->success('Survey has been added!', 'Success!');

        If(Input::get('another_survey') =='YES'){
            return redirect('/imaging');
        }Else{
            return redirect('/imaging/admin');
        }

    }


    public function admin_report(){

        $start_date = Input::get('start_date'). ' 00:00:00';
        $end_date = Input::get('end_date'). ' 23:59:59';

        $ir_procedures = ImagingAnswers::whereHas('categoryType', function ($query) {
            $query->where('cost_center_name','IR');
        })
            ->whereBetween('created_at', array($start_date, $end_date))
            ->count('id');

        $ir_no = ImagingAnswers::whereHas('categoryType', function ($query) {
            $query->where('cost_center_name','IR');
        })
            ->whereBetween('created_at', array($start_date, $end_date))
            ->sum('no');

        //dd($ir_no);

        $monthly_questions = ImagingAnswers::whereBetween('created_at', array($start_date, $end_date))
            ->count('id');

        $monthly_yes = ImagingAnswers::whereBetween('created_at', array($start_date, $end_date))
            ->sum('yes');

        $monthly_no = ImagingAnswers::whereBetween('created_at', array($start_date, $end_date))
            ->sum('no');

        $monthly_na = ImagingAnswers::whereBetween('created_at', array($start_date, $end_date))
            ->sum('na');


        $areas = ImagingCostCenters::select('cost_center_name')
            ->orderby('cost_center_name')
            ->distinct()
            ->get();

        $questions = ImagingQuestions::orderby('created_at','DESC')
            ->get();

        $cost_centers = ImagingCostCenters::with("surveyCount")->orderby('cost_center_name')
            //->take(1)
            ->get();


        $cost_center_list = ImagingCostCenters::get();
        //dd($cost_center_list);



















        $data=collect();
        $recordCount = 0;
        foreach($cost_center_list as $key => $value){
        $recordCount = $recordCount + 1;
           // dd($value);

            $completed_surveys = Imaging::where('cost_center','=', $value['cost_center'])
                ->whereBetween('created_at', array($start_date, $end_date))
                ->count();

            $data ->push(['cost_center' => $value['cost_center'], 'total' => $completed_surveys]) ;
            //$data = $cost_center_list->merge($completed_surveys);
            //$data=collect([
                //(object)['total_id'] => $recordCount,
             //   (object)['TotalSurvey'=>$completed_surveys,'cost_center'=> $value['cost_center']]]);

            //$cost_center_list->map(function ($completed_surveys) {
             //   $cost_center_list['total'] = $completed_surveys;
                //return $post;
            //});


        }
        //dd(json_encode($data));
        //dd($data->toArray());
        //dd($cost_center_list);








        $completed = Imaging::orderby('created_at','DESC')
            ->whereBetween('created_at', array($start_date, $end_date))
            ->get();










        return view('imaging.admin.report', compact('questions','cost_centers','completed','areas',
            'monthly_no','monthly_na','monthly_yes','monthly_questions','ir_no','ir_procedures','data','cost_center_list'));

    }
}
