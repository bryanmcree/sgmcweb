<?php

namespace App\Http\Controllers;

use App\History;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use DB;
use Carbon;

class HistoryController extends Controller
{
    public function historyFeed ()
    {
        //$entries = History::orderBy('created_at','DESC')->take(100)->get();
        //dd($entries);
        //$entries2 = History::orderBy('created_at','DESC')->get();

        return view('history.feed');
    }

    public function reportHistory()
    {
        $dropdown_action = History::select('action')->distinct()->orderBy('action')->get();
        //$dropdown_action[''] = 'Any Action';

        $dropdown_user = History::select('userid')->distinct()->orderBy('userid')->get();
        //$dropdown_user[''] = 'All Users';



        if (Input::has('RunReport'))
        {
            $dropdown_action = History::select('action')->distinct()->orderBy('action')->get();
            //$dropdown_action[''] = 'Any Action';

            $dropdown_user = History::select('userid')->distinct()->orderBy('userid')->get();
            //$dropdown_user[''] = 'All Users';


            if (Input::get('action') =='' || Input::get('users') =='')
            {
                $history = History::orderby('created_at', 'DESC')->get();
            }else{
                $history = History::
                where('action','=', Input::get('action'))
                    ->where('userid','=', Input::get('users'))
                    ->orderby('created_at', 'DESC')
                    ->get();
            }


            $report = '';

            //dd($history);
        }else{

            $report = 'Nothing';

        }



        return view('reports.history.index',compact('dropdown_action','dropdown_user','report','history'));
    }

    public function dashboard(){

        $actions = History::selectRaw('action,count(*) as total')
            ->distinct()
            ->where('created_at', '>=', \Carbon\Carbon::today())
            ->groupBy('action')
            //->orderBy('total','DESC')
            ->orderBy('action')
            ->get();
        //dd($actions);

        $data2 = [];
        foreach ($actions as $action)
        {
            $data2[] = [
                'action' => $action->action,
                'total' => $action->total
            ];
        }

        //$actions->jsonarray = $data2;

        //dd(json_encode(['jsonarray'=>$data2]));





        return response(json_encode(['jsonarray'=>$data2]));
    }

    public function daily(){

        $entries = History::where('created_at','>=', \Carbon\Carbon::today())
            ->orderBy('created_at', 'desc')
            ->get();

        //dd($entries);

        return view('history.daily', compact('entries','entries2','results'));
    }
}
