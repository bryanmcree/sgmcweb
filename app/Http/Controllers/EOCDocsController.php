<?php

namespace App\Http\Controllers;

use App\EOCDocs;
use Illuminate\Http\Request;

use App\Http\Requests;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use App\User;
use \Auth;

class EOCDocsController extends Controller
{
    public function addFile() {

        $file = Input::file('file_name');
        $category_id = Input::get('category_id');
        $survey_id = Input::get('survey_id');
        $extension = $file->getClientOriginalExtension();
        $entry = new EOCDocs();
        $entry->id = Uuid::generate(4);
        $entry->file_mime = $file->getClientMimeType();
        $entry->old_filename = $file->getClientOriginalName();
        $entry->new_filename = Uuid::generate(4) .'.'.$extension;
        $entry->category_id = $category_id;
        $entry->file_extension = $extension;
        $entry->added_by = \Auth::user()->employee_number;
        $entry->file_size = $file->getClientSize();
        $entry->file_description = Input::get('file_description');
        $entry->survey_id = $survey_id;

        Storage::disk('local')->put($entry->new_filename,  File::get($file));

        $entry->save();
        alert()->success('File has been uploaded.', 'File Uploaded');

        return redirect('/eoc/survey/category/'.$category_id .'?survey_id='.$survey_id);

    }

    public function downloadFile($id){

        $entry = EOCDocs::where('id', '=', $id)->firstOrFail();
        $file = Storage::disk('local')->get($entry->new_filename);
        return (new Response($file, 200))
            ->header('Content-Type', $entry->file_mime)
            ->header('content-disposition', 'attachment; filename="'. $entry->old_filename .'"');
    }

    public function deleteFile($id)
    {
        $entry = EOCDocs::where('id', '=', $id)->firstOrFail();
        $file = Storage::disk('local')->get($entry->new_filename);
        File::delete($file);
        $entry->delete();
        alert()->success('File Deleted.', 'File Deleted');
        return redirect('/report/request/detail/view/'.Input::get('report_id'));
    }
}
