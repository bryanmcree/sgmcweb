<?php

namespace App\Http\Controllers;

use App\History;
use App\Palliative;
use App\PalliativeVisits;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Webpatser\Uuid\Uuid;

class PalliativeController extends Controller
{
    public function index(){

        //get all patients
        $patients = Palliative::whereNotNull('id')->get();

        $history = new History();
        $history->id = \Uuid::generate(4);
        $history->action = 'Viewed Palliative Care App';
        $history->userid = \Auth::user()->username;
        $history->user_ip = \Request::ip();
        $history->search_string = 'None';
        $history->save();

        return view('palliative.index', compact('patients','evals'));
    }

    public function addPatient(Request $request){

        //Create unique ID via uuid
        $request->offsetSet('id', Uuid::generate(4));
        //Calculate patient's age from DOB
        $request->offsetSet('patient_age', Carbon::parse($request->date_of_birth)->age);

        //If there is a discharge date then lets calculate the LOS
        if(!is_null($request->date_of_discharge)){
            $request->offsetSet('admit_to_discharge', Carbon::parse($request->date_of_admission)->diffInDays(Carbon::parse($request->date_of_discharge)));
        }

        //If there is a consult date lets calculate time between admission and consult.
        if(!is_null($request->date_of_consult)){
            $request->offsetSet('admit_to_discharge', Carbon::parse($request->date_of_admission)->diffInDays(Carbon::parse($request->date_of_consult)));
        }

        //Get values from fields
        $new_patient = $request->all();

        //Create new record and save to dB
        Palliative::create($new_patient);

        alert()->success('Patient Added!', 'Success!');
        return redirect()->back();

    }

    public function detail($id){

        $patient = Palliative::where('id','=', $id)->first();

        return view('palliative.detail', compact('patient','evals'));

    }

    public function addVisit(Request $request){

        //Create unique ID via uuid
        $request->offsetSet('id', Uuid::generate(4));

        //Get values from fields
        $new_visit = $request->all();

        //Create new record and save to dB
        PalliativeVisits::create($new_visit);

        alert()->success('Visit Added!', 'Success!');
        return redirect()->back();
    }

    public function deletePatient($id){

        $patient = Palliative::find($id);

        $patient->delete();

        return redirect('/palliative');

    }

    public function deleteVisit($id){

        $patient_id = Input::get('patient_id');
        $visit = PalliativeVisits::find($id);
        //dd($visit);

        $visit->delete();

        return redirect('/palliative/patient/' . $patient_id);

    }

}
