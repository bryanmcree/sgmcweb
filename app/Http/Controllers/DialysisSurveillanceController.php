<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\RoleUser;
use App\User;
use App\DialysisNurses;
use App\DialysisLog;
use App\DialysisSurveillance;
use Carbon;
use Uuid;

class DialysisSurveillanceController extends Controller
{
    public function create()
    {
        $nurses = DialysisNurses::wherenotnull('id')
            ->get();

        return view('dialysis.surveillance.create', compact('nurses'));
    }

    public function store(Request $request)
    {
        $request->offsetSet('id', Uuid::generate(4));

        DialysisSurveillance::create($request->all());

        alert()->success('Surveillance Updated', 'Success!');
        return redirect('/dialysis');
    }

    public function show()
    {
        $surveillances = DialysisSurveillance::wherenotnull('id')
            ->orderby('date', 'desc')
            ->paginate(10);

        return view('dialysis.surveillance.show', compact('surveillances'));
    }

    public function edit(DialysisSurveillance $surveillance)
    {
        $nurses = DialysisNurses::wherenotnull('id')
            ->get();

        return view('dialysis.surveillance.edit', compact('surveillance', 'nurses'));
    }

    public function update(Request $request, DialysisSurveillance $surveillance)
    {
        $surveillance->update($request->all());

        alert()->success('Surveillance Updated', 'Success!');
        return redirect('/dialysis');
    }

    public function destroy(DialysisSurveillance $surveillance)
    {
        $surveillance->delete();
    }

    public function nurse(Request $request)
    {
        $request->offsetSet('id', Uuid::generate(4));

        DialysisNurses::create($request->all());

        alert()->success('Nurse Added', 'Success!');
        return redirect('/dialysis');
    }
}
