<?php

namespace App\Http\Controllers;

use App\Roster;
use App\RosterAttendants;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\RoleUser;
use App\User;
use Webpatser\Uuid\Uuid;
use Image;
use DB;
use App\State;
use App\Role;
use App\Http\Controllers\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;

class RosterController extends Controller
{
    public function index(){
       // $names = LemName::all();
       // $evals = LemValue::select("emp_id","lem_id","evaluator")->distinct()->with('evaluated','lemName','evalBy')->get();
        //dd($evals);
        $rosters = Roster::where('created_by','=', \Auth::user()->id)
            ->orderby('created_at','desc')
            ->get();
        
        $others = Roster::wherenotnull('id')->orderby('created_at', 'desc')->get();

        $attendants = RosterAttendants::selectRaw('roster_id, count(*) as total')
            ->wherenotnull('id')
            ->groupby('roster_id')
            ->get();

        return view('roster.index', compact('rosters','others', 'attendants'));
    }

    public function createRoster(Request $request){

        $request->offsetSet('id', Uuid::generate(4));
        
        Roster::create($request->all());

        alert()->success('Roster has been created!', 'Success!');
        return redirect('/roster');
    }

    public function loadRoster($id){

        $roster = Roster::with('attendants')
            ->where('id','=',$id)
            ->first();
        //dd($roster);
        $attendants = RosterAttendants::with('attendantInfo')
            ->where('roster_id','=',$id)
            ->orderby('created_at','DESC')
            ->get();

        return view('roster.load', compact('roster','attendants'));
    }

    public function download(Roster $roster)
    {
        $attendInfo = RosterAttendants::select('users.name', 'users.employee_number', 'users.title', 'users.unit_code', 'users.unit_code_description')
            ->leftJoin('users', 'users.employee_number', '=', 'roster_attendants.employee_number')
            ->where('roster_attendants.roster_id', '=', $roster->id)
            ->get()
            ->toArray();

        Excel::create('Roster_' . $roster->roster_name . ' ' . date('m-d-Y'), function($excel) use($attendInfo) {
            $excel->sheet('New sheet', function($sheet) use($attendInfo) {
                $sheet->fromArray($attendInfo);
            })->download('csv');
        });

    }

    public function submitAttendant(Requests\SystemListRequest $request){

        //Dino Code, for employees who have a short employee number
        //Lets remove the 0 if one exist use just the other numbers
        if (preg_match('/^0/', $request->employee_number)){
            $employee_number = substr($request->employee_number, 1);
        }else{
            $employee_number = $request->employee_number;
        }


        $attendant = User::where('employee_number','=', $employee_number)
            ->first();
       // dd($attendant);

        IF(empty($attendant)){
            //Not found
            alert()->error('Employee Not Found!  Try again.', 'Warning!');
            return redirect('/roster/load/'.$request->roster_id);

        }else{
            //found a record

            //Lets make sure they are not ALREADY on the list

            $check = RosterAttendants::where('roster_id','=',$request->roster_id)
                ->where('employee_number','=', $employee_number)
                ->first();
            IF(empty($check)){
                //Ok the user has not signed into this roster.. lets add them to it.

                $newAttendant = new RosterAttendants;
                $newAttendant->id = Uuid::generate(4);
                $newAttendant->roster_id = $request->roster_id;
                $newAttendant->employee_number = $employee_number;
                $newAttendant->save();

                alert()->success( 'Welcome!', $attendant->first_name .' '.$attendant->last_name);
                return redirect('/roster/load/'.$request->roster_id);

            }else{
                alert()->info('You have already signed in!', 'Ooops!');
                return redirect('/roster/load/'.$request->roster_id);
            }

        }

    }

    public function removeAttendant(RosterAttendants $attendant)
    {
        $attendant->delete();

        alert()->success('Attendant Removed!', 'Success');
        return redirect()->back();
    }

    public function update(Request $request)
    {
        $roster = Roster::find($request->id);

        $roster->update($request->all());

        alert()->success('Roster has been updated!', 'Success!');
        return redirect('/roster');
    }

    public function destroy(Roster $roster)
    {
        $roster->delete();
    }

}
