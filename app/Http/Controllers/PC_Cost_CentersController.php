<?php

namespace App\Http\Controllers;

use App\History;
use App\PC_Cost_Centers;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;

class PC_Cost_CentersController extends Controller
{
    public function add_cost_center(Requests\SystemListRequest $request){

        $input = $request->all();
        //Input has been created lets save it to the database.
        PC_Cost_Centers::create($input);

        //Lets write this action to the history table.
        $history = new History();
        $history->id = Uuid::generate(4);
        $history->action = 'PC - Add Cost Center';
        $history->userid = \Auth::user()->username;
        $history->search_string = 'none';
        $history->user_ip = \Request::ip();
        $history->save();

        //Everything is good lets send feedback to the user and then redirect
        alert()->success('Cost Center Added!', 'Success!');
        return redirect('/pc');
    }

    public function edit_cost_center(){

        //Lets find that shift.
        $shift = PC_Cost_Centers::find(Input::get('id'));
        $shift->update(Input::except(['id']));

        return back();
    }
}
