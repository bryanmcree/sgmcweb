<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

use Auth;
use DB;
use Uuid;
use Carbon;
use App\Stats;
use App\LaborEfficiencyCalculator;
use App\LaborEfficiencyMissing;
use App\CostCenters;

class LaborEfficiencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ccs = CostCenters::where('active_status', 1)
            ->whereNotIn('uos', ['', NULL])
            ->orderby('cost_center')
            ->get();

        $divisions = CostCenters::select('csuite')
            ->wherenotnull('csuite')
            ->where('csuite', '<>', '')
            ->groupBy('csuite')
            ->get();

        $flexHours = DB::table('LE_calculator')
            ->leftJoin('CostCenters_new', 'LE_calculator.cost_center_id', '=', 'CostCenters_new.id')
            ->where('date', '=', Carbon::now()->subDays(1)->startOfDay())
            ->where('LE_calculator.deleted_at', NULL)
            ->get();
        
        $missings = LaborEfficiencyMissing::wherenotnull('id')
            ->orderby('cost_center')
            ->get();

        $topTen = LaborEfficiencyCalculator::where('date', '=', Carbon::now()->subDays(1)->startOfDay())
            ->wherenotnull('flex_hours')
            ->orderBy('flex_hours', 'desc')
            ->take(10)
            ->get();

        $bottomTen = LaborEfficiencyCalculator::where('date', '=', Carbon::now()->subDays(1)->startOfDay())
            ->wherenotnull('flex_hours')
            ->orderBy('flex_hours')
            ->take(10)
            ->get();

        $weeklyTrend = LaborEfficiencyCalculator::selectRaw('date, AVG(flex_hours) as avgflex')->where('date', '>=', Carbon::now()->subDays(7)->startOfDay())
            ->groupBy('date')
            ->orderBy('date')
            ->get();

        $dirflexHours = DB::table('LE_calculator')
            ->leftJoin('CostCenters_new', 'LE_calculator.cost_center_id', '=', 'CostCenters_new.id')
            ->where('date', '>=', Carbon::now()->subDays(7)->startOfDay())
            ->where('LE_calculator.deleted_at', NULL)
            ->where(function($q) {
                $q->where('CostCenters_new.director', Auth::user()->employee_number)
                    ->orWhere('CostCenters_new.manager', Auth::user()->employee_number);
            })
            ->orderby('date', 'desc')
            ->get();
        
        $dirmissings = DB::table('LE_calculator')
            ->leftJoin('CostCenters_new', 'LE_calculator.cost_center_id', '=', 'CostCenters_new.id')
            ->where('date', '=',  Carbon::now()->subDays(1)->startOfDay())
            ->wherenull('worked_hours')
            ->where('LE_calculator.deleted_at', NULL)
            ->where(function($q) {
                $q->where('CostCenters_new.director', Auth::user()->employee_number)
                    ->orWhere('CostCenters_new.manager', Auth::user()->employee_number);
            })
            ->get();

        $dirtopTen = DB::table('LE_calculator')
            ->leftJoin('CostCenters_new', 'LE_calculator.cost_center_id', '=', 'CostCenters_new.id')
            ->where('date', '=', Carbon::now()->subDays(1)->startOfDay())
            ->wherenotnull('flex_hours')
            ->where('LE_calculator.deleted_at', NULL)
            ->where(function($q) {
                $q->where('CostCenters_new.director', Auth::user()->employee_number)
                    ->orWhere('CostCenters_new.manager', Auth::user()->employee_number);
            })
            ->orderBy('flex_hours', 'desc')
            ->take(10)
            ->get();

        $dirbottomTen = DB::table('LE_calculator')
            ->leftJoin('CostCenters_new', 'LE_calculator.cost_center_id', '=', 'CostCenters_new.id')
            ->where('date', '=', Carbon::now()->subDays(1)->startOfDay())
            ->wherenotnull('flex_hours')
            ->where('LE_calculator.deleted_at', NULL)
            ->where(function($q) {
                $q->where('CostCenters_new.director', Auth::user()->employee_number)
                    ->orWhere('CostCenters_new.manager', Auth::user()->employee_number);
            })
            ->orderBy('flex_hours')
            ->take(10)
            ->get();

        // dd($dirflexHours, $dirmissings, $dirtopTen, $dirbottomTen);

        return view('labor-efficiency/index', compact('ccs', 'flexHours', 'missings', 'topTen', 'bottomTen', 'weeklyTrend',
                                                      'dirflexHours', 'dirmissings', 'dirtopTen', 'dirbottomTen', 'divisions'));
    }

    public function division($division)
    {
        $flexHours = DB::table('LE_calculator')
            ->leftJoin('CostCenters_new', 'LE_calculator.cost_center_id', '=', 'CostCenters_new.id')
            ->where('date', '=', Carbon::now()->subDays(1)->startOfDay())
            ->where('LE_calculator.deleted_at', NULL)
            ->where('CostCenters_new.csuite', $division)
            ->get();
        
        $missings = LaborEfficiencyMissing::wherenotnull('id')
            ->where('csuite', $division)
            ->orderby('cost_center')
            ->get();
            
        $topTen = DB::table('LE_calculator')
            ->leftJoin('CostCenters_new', 'LE_calculator.cost_center_id', '=', 'CostCenters_new.id')
            ->where('date', '=', Carbon::now()->subDays(1)->startOfDay())
            ->wherenotnull('flex_hours')
            ->where('LE_calculator.deleted_at', NULL)
            ->where('csuite', $division)
            ->orderBy('flex_hours', 'desc')
            ->take(10)
            ->get();

        $bottomTen = DB::table('LE_calculator')
            ->leftJoin('CostCenters_new', 'LE_calculator.cost_center_id', '=', 'CostCenters_new.id')
            ->where('date', '=', Carbon::now()->subDays(1)->startOfDay())
            ->wherenotnull('flex_hours')
            ->where('LE_calculator.deleted_at', NULL)
            ->where('csuite', $division)
            ->orderBy('flex_hours')
            ->take(10)
            ->get();

        return view('labor-efficiency/division', compact('division','flexHours', 'missings', 'topTen', 'bottomTen'));
    }

    public function report(Request $request)
    {   
        $startDate = $request->start_date;
        $endDate = $request->end_date;

        $flexHours = DB::table('LE_calculator')
            ->leftJoin('CostCenters_new', 'LE_calculator.cost_center_id', '=', 'CostCenters_new.id')
            ->whereBetween('date', [$startDate, $endDate])
            ->where('LE_calculator.deleted_at', NULL)
            ->get();

        $procedure = DB::select('EXEC LE_missing_Report ?,?', [$startDate, $endDate]);

        // dd($procedure);
    
        return view('labor-efficiency/report', compact('flexHours', 'procedure', 'startDate', 'endDate'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $finder = LaborEfficiencyCalculator::where('cost_center_id', $request->cost_center_id)->where('date', $request->date)->first();
        
        if(!is_null($finder))
        {
            alert()->warning('You Already Entered This Date!', 'Warning!');
            return back();
        }
        
        $request->offsetSet('id', Uuid::generate(4));

        $expectedHours = $request->expected_volume * $request->benchmark;

        $request->offsetSet('expected_work_hours', $expectedHours);

        LaborEfficiencyCalculator::create($request->all());

        alert()->success('Calculation Saved!', 'Success!');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(CostCenters $cc)
    {
        $avg = Stats::select('var_avg')->where('stat_link', $cc->stat_link)->first();

        if(!is_null($avg))
        {
            $avg = $avg->var_avg/30;
        }
        else
        {
            $avg = 0;
        }
        
        $recents = LaborEfficiencyCalculator::where('date', '>=', Carbon::now()->subDays(60))->where('cost_center_id', $cc->id)->orderby('date', 'desc')->get();
        $flexChart = LaborEfficiencyCalculator::where('date', '>=', Carbon::now()->subDays(30))->where('cost_center_id', $cc->id)->orderby('date', 'asc')->get();
        $avgWork = LaborEfficiencyCalculator::selectRaw('AVG(worked_hours) as avg_work')->where('date', '>=', Carbon::now()->subDays(30))->where('cost_center_id', $cc->id)->first();
        $avgFlex = LaborEfficiencyCalculator::selectRaw('AVG(flex_hours) as avg_flex')->where('date', '>=', Carbon::now()->subDays(30))->where('cost_center_id', $cc->id)->first();
        $avgProductivity = LaborEfficiencyCalculator::selectRaw('AVG(daily_pi) as avg_pi')->where('date', '>=', Carbon::now()->subDays(30))->where('cost_center_id', $cc->id)->first();
        $avgMHstat = LaborEfficiencyCalculator::selectRaw('AVG(MH_stat) as mh_stat')->where('date', '>=', Carbon::now()->subDays(30))->where('cost_center_id', $cc->id)->first();

        return view('labor-efficiency.show', compact('cc', 'recents', 'avg', 'flexChart', 'avgWork', 'avgFlex', 'avgProductivity', 'avgMHstat'));
    }

    public function ccReport()
    {
        $ccs = CostCenters::where('active_status', 1)
            ->whereNotIn('uos', ['', NULL])
            ->orderby('cost_center')
            ->get();

        return view('labor-efficiency.ccReport', compact('ccs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $current = LaborEfficiencyCalculator::where('id', $request->id)->first();
        
        $current->date = $request->date;
        $current->benchmark = $request->benchmark;
        $current->expected_volume = $request->expected_volume;
        $current->expected_work_hours = $current->expected_volume * $request->benchmark;

        $current->save();

        $new = LaborEfficiencyCalculator::where('id', $request->id)->first();

        // dd($new, $request->all());

        if($request->worked_hours != '')
        {
            $new->worked_hours = $request->worked_hours;
            $new->flex_hours = $new->expected_work_hours - $request->worked_hours;
            $new->MH_stat = $request->worked_hours / $new->expected_volume;

            if($new->expected_work_hours == 00 && $request->worked_hours == 0)
            {
                $new->daily_pi = NULL;
            }
            else if($new->expected_work_hours != 0 && $request->worked_hours == 0)
            {
                $new->daily_pi = NULL;
            }
            else
            {
                $new->daily_pi = ($new->expected_work_hours / $request->worked_hours) * 100;
            }
            

            if($new->daily_pi < 100 && $request->comments == '')
            {
                alert()->warning('You Must Leave a Comment!', 'Warning!');
                return back();
            }
            elseif($new->daily_pi < 100 && $request->comments == ' ')
            {
                alert()->warning('You Must Leave a Comment!', 'Warning!');
                return back();
            }

            $new->comments = $request->comments;

            $new->save();
        }



        alert()->success('Calculation Updated!', 'Success!');
        return back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function reportExtract(Request $request)
    {
        $startDate = $request->start_date;
        $endDate = $request->end_date;

        $flexHours = DB::table('LE_calculator')
            ->selectRaw('LE_calculator.date AS Date, CostCenters_new.style1 AS Cost_Center, CostCenters_new.csuite AS Division, CostCenters_new.uos AS UOS
                    , LE_calculator.benchmark AS Benchmark, LE_calculator.expected_volume, LE_calculator.expected_work_hours AS Expected_Worked_Hours
                    , LE_calculator.worked_hours AS Worked_Hours, LE_calculator.daily_pi AS PI_Percentage
                    , LE_calculator.expected_work_hours / 8 AS Expected_FTEs, LE_calculator.worked_hours / 8 AS Worked_FTEs')
            ->leftJoin('CostCenters_new', 'LE_calculator.cost_center_id', '=', 'CostCenters_new.id')
            ->whereBetween('date', [$startDate, $endDate])
            ->where('LE_calculator.deleted_at', NULL)
            ->get();

            // dd($flexHours[0]);

        // $procedure = DB::select('EXEC LE_missing_Report ?,?', [$startDate, $endDate]);

        Excel::create('Labor_Efficiency_ '.date('m-d-Y_hia'), function($excel) use($flexHours) {
            $excel->sheet('New sheet', function($sheet) use($flexHours) {

                $data= json_decode( json_encode($flexHours), true);
                $sheet->fromArray($data);

            })->download('xlsx');
        });
    }

    public function reportExtractDivision(Request $request)
    {
        $startDate = $request->start_date;
        $endDate = $request->end_date;
        $division = $request->division;

        $flexHours = DB::table('LE_calculator')
        ->selectRaw('LE_calculator.date AS Date, CostCenters_new.style1 AS Cost_Center, CostCenters_new.csuite AS Division, CostCenters_new.uos AS UOS
                    , LE_calculator.benchmark AS Benchmark, LE_calculator.expected_volume, LE_calculator.expected_work_hours AS Expected_Worked_Hours
                    , LE_calculator.worked_hours AS Worked_Hours, LE_calculator.daily_pi AS PI_Percentage
                    , LE_calculator.expected_work_hours / 8 AS Expected_FTEs, LE_calculator.worked_hours / 8 AS Worked_FTEs')
            ->leftJoin('CostCenters_new', 'LE_calculator.cost_center_id', '=', 'CostCenters_new.id')
            ->whereBetween('date', [$startDate, $endDate])
            ->where('CostCenters_new.csuite', $division)
            ->where('LE_calculator.deleted_at', NULL)
            ->get();

            // dd($flexHours[0]);

        // $procedure = DB::select('EXEC LE_missing_Report ?,?', [$startDate, $endDate]);

        Excel::create('Labor_Efficiency_ '.date('m-d-Y_hia'), function($excel) use($flexHours) {
            $excel->sheet('New sheet', function($sheet) use($flexHours) {

                $data= json_decode( json_encode($flexHours), true);
                $sheet->fromArray($data);

            })->download('xlsx');
        });
    }
}
