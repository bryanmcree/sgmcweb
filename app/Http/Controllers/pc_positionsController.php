<?php

namespace App\Http\Controllers;

use App\History;
use App\pc_positions;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;

use App\Http\Requests;

class pc_positionsController extends Controller
{

    public function create_position(Requests\SystemListRequest $request){

        //Lets get the values of the form
        $request->offsetSet('id', Uuid::generate(4));
        //dd($input);

        //Check and see if any positions exist for that cost center or job code
        //if not lets start at 1, if so lets get that number and add 1 to it.

        $position_check = pc_positions::where('cost_center','=', $request->cost_center)
            ->where('job_code','=', $request->job_code)
            ->orderby('created_at', 'desc')
            ->first();
        //dd($position_check);

        if(is_null($position_check)){
            $position_number = 1;
        }else{
            $position_number = $position_check->position_number + 1;
        }
        //dd($position_number);

        $request->offsetSet('position_number', $position_number);

        $input = $request->all();

        //Input has been created lets save it to the database.
        pc_positions::create($input);

        //Lets write this action to the history table.
        $history = new History();
        $history->id = Uuid::generate(4);
        $history->action = 'Create Position';
        $history->userid = \Auth::user()->username;
        $history->search_string = 'none';
        $history->user_ip = \Request::ip();
        $history->save();

        //Everything is good lets send feedback to the user and then redirect
        alert()->success('Creation Created!', 'Success!');
        return redirect('/pc');

    }
}
