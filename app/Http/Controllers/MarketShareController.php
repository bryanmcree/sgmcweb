<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\MarketShareIP;
use App\MarketShareOP;
use App\MarketShareIPGoing;
use App\MarketShareIPComing;
use App\MarketShareOPGoing;
use App\MarketShareOPComing;

class MarketShareController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ipCategoriesGoing = MarketShareIPGoing::select('DRG')->orderby('DRG')->distinct()->get();
        $ipCategoriesComing = MarketShareIPComing::select('DRG')->orderby('DRG')->distinct()->get();
        $opCategoriesGoing = MarketShareOPGoing::select('county')->orderby('county')->distinct()->get();
        $opCategoriesComing = MarketShareOPComing::select('facility')->orderby('facility')->distinct()->get();
        
        $ipDataGoing = MarketShareIPGoing::wherenotnull('id')->orderby('facility')->get();
        $ipDataComing = MarketShareIPComing::wherenotnull('id')->orderby('zip')->get();
        $opDataGoing = MarketShareOPGoing::wherenotnull('id')->orderby('facility')->get();
        $opDataComing = MarketShareOPComing::wherenotnull('id')->orderby('county')->get();

        $totalSharesIPGoing = MarketShareIPGoing::selectRaw('SUM(num_records) as total, DRG')->groupBy('DRG')->get();
        $totalSharesIPComing = MarketShareIPComing::selectRaw('SUM(num_records) as total, DRG')->groupBy('DRG')->get();
        $totalSharesOPGoing = MarketShareOPGoing::selectRaw('SUM(num_records) as total, county')->groupBy('county')->get();
        $totalSharesOPComing = MarketShareOPComing::selectRaw('SUM(num_records) as total, facility')->groupBy('facility')->get();

        // dd($totalSharesIPGoing, $totalSharesIPComing);

        return view('market-share.index', compact('ipDataGoing', 'ipDataComing', 'opDataGoing', 'opDataComing', 'ipCategoriesGoing', 'ipCategoriesComing', 'opCategoriesGoing',
                                                    'opCategoriesComing','totalSharesIPGoing', 'totalSharesIPComing', 'totalSharesOPGoing', 'totalSharesOPComing'));
    }

    public function printAll()
    {
        $ipCategoriesGoing = MarketShareIPGoing::select('DRG')->orderby('DRG')->distinct()->get();
        $ipCategoriesComing = MarketShareIPComing::select('DRG')->orderby('DRG')->distinct()->get();
        $opCategoriesGoing = MarketShareOPGoing::select('county')->orderby('county')->distinct()->get();
        $opCategoriesComing = MarketShareOPComing::select('facility')->orderby('facility')->distinct()->get();
        
        $ipDataGoing = MarketShareIPGoing::wherenotnull('id')->orderby('facility')->get();
        $ipDataComing = MarketShareIPComing::wherenotnull('id')->orderby('zip')->get();
        $opDataGoing = MarketShareOPGoing::wherenotnull('id')->orderby('county')->get();
        $opDataComing = MarketShareOPComing::wherenotnull('id')->orderby('facility')->get();

        $totalSharesIPGoing = MarketShareIPGoing::selectRaw('SUM(num_records) as total, DRG')->groupBy('DRG')->get();
        $totalSharesIPComing = MarketShareIPComing::selectRaw('SUM(num_records) as total, DRG')->groupBy('DRG')->get();
        $totalSharesOPGoing = MarketShareOPGoing::selectRaw('SUM(num_records) as total, county')->groupBy('county')->get();
        $totalSharesOPComing = MarketShareOPComing::selectRaw('SUM(num_records) as total, facility')->groupBy('facility')->get();

        // dd($totalSharesIPGoing, $totalSharesIPComing);

        return view('market-share.print.print-all', compact('ipDataGoing', 'ipDataComing', 'opDataGoing', 'opDataComing', 'ipCategoriesGoing', 'ipCategoriesComing', 'opCategoriesGoing',
                                                    'opCategoriesComing','totalSharesIPGoing', 'totalSharesIPComing', 'totalSharesOPGoing', 'totalSharesOPComing'));
    }

    public function inpatientGoing($category)
    {
        $goingCategories = MarketShareIPGoing::select('DRG')->orderby('DRG')->distinct()->get();
        $comingCategories = MarketShareIPComing::select('DRG')->orderby('DRG')->distinct()->get();
        $opCategoriesGoing = MarketShareOPGoing::select('county')->orderby('county')->distinct()->get();
        $opCategoriesComing = MarketShareOPComing::select('facility')->orderby('facility')->distinct()->get();

        $data = MarketShareIPGoing::where('DRG', $category)->orderBy('num_records')->get();
        // dd($data);
        $totalShare = MarketShareIPGoing::selectRaw('SUM(num_records) as total')->where('DRG', $category)->first();

        return view('market-share.inpatient-going', compact('data', 'goingCategories', 'comingCategories', 'category', 'totalShare', 'opCategoriesGoing','opCategoriesComing'));
    }

    public function inpatientComing($category)
    {
        $goingCategories = MarketShareIPGoing::select('DRG')->orderby('DRG')->distinct()->get();
        $comingCategories = MarketShareIPComing::select('DRG')->orderby('DRG')->distinct()->get();
        $opCategoriesGoing = MarketShareOPGoing::select('county')->orderby('county')->distinct()->get();
        $opCategoriesComing = MarketShareOPComing::select('facility')->orderby('facility')->distinct()->get();

        $data = MarketShareIPComing::where('DRG', $category)->orderBy('num_records')->get();

        $totalShare = MarketShareIPComing::selectRaw('SUM(num_records) as total')->where('DRG', $category)->first();

        // dd($data, $totalShare);

        return view('market-share.inpatient-coming', compact('data', 'goingCategories', 'comingCategories', 'category', 'totalShare', 'opCategoriesGoing','opCategoriesComing'));
    }

    public function outpatientGoing($category)
    {
        $goingCategories = MarketShareIPGoing::select('DRG')->orderby('DRG')->distinct()->get();
        $comingCategories = MarketShareIPComing::select('DRG')->orderby('DRG')->distinct()->get();
        $opCategoriesGoing = MarketShareOPGoing::select('county')->orderby('county')->distinct()->get();
        $opCategoriesComing = MarketShareOPComing::select('facility')->orderby('facility')->distinct()->get();

        $data = MarketShareOPGoing::where('county', $category)->orderBy('num_records')->get();
        // dd($data);
        $totalShare = MarketShareOPGoing::selectRaw('SUM(num_records) as total')->where('county', $category)->first();

        return view('market-share.outpatient-going', compact('data', 'goingCategories', 'comingCategories', 'category', 'totalShare', 'opCategoriesGoing','opCategoriesComing'));
    }

    public function outpatientComing($category)
    {
        $goingCategories = MarketShareIPGoing::select('DRG')->orderby('DRG')->distinct()->get();
        $comingCategories = MarketShareIPComing::select('DRG')->orderby('DRG')->distinct()->get();
        $opCategoriesGoing = MarketShareOPGoing::select('county')->orderby('county')->distinct()->get();
        $opCategoriesComing = MarketShareOPComing::select('facility')->orderby('facility')->distinct()->get();

        $data = MarketShareOPComing::where('facility', $category)->orderBy('num_records')->get();

        $totalShare = MarketShareOPComing::selectRaw('SUM(num_records) as total')->where('facility', $category)->first();

        return view('market-share.outpatient-coming', compact('data', 'goingCategories', 'comingCategories', 'category', 'totalShare', 'opCategoriesGoing','opCategoriesComing'));
    }

    public function ipGoingPrint($drg)
    {
        $data = MarketShareIPGoing::where('DRG', $drg)->orderBy('facility')->get();
        
        $totalShare = MarketShareIPGoing::selectRaw('SUM(num_records) as total')->where('DRG', $drg)->first();

        // dd($data, $totalShare);

        return view('market-share.print.ip-going', compact('data', 'totalShare', 'drg'));
    }

    public function ipComingPrint($drg)
    {
        $data = MarketShareIPComing::where('DRG', $drg)->orderBy('zip')->get();
        
        $totalShare = MarketShareIPComing::selectRaw('SUM(num_records) as total')->where('DRG', $drg)->first();

        // dd($data, $totalShare);

        return view('market-share.print.ip-coming', compact('data', 'totalShare', 'drg'));
    }

    public function opGoingPrint($county)
    {
        $data = MarketShareOPGoing::where('county', $county)->orderBy('facility')->get();
        
        $totalShare = MarketShareOPGoing::selectRaw('SUM(num_records) as total')->where('county', $county)->first();

        // dd($data, $totalShare);

        return view('market-share.print.op-going', compact('data', 'totalShare', 'county'));
    }

    public function opComingPrint($facility)
    {
        $data = MarketShareOPComing::where('facility', $facility)->orderBy('facility')->get();
        
        $totalShare = MarketShareOPComing::selectRaw('SUM(num_records) as total')->where('facility', $facility)->first();

        // dd($data, $totalShare);

        return view('market-share.print.op-coming', compact('data', 'totalShare', 'facility'));
    }

    // Inactive //

    public function inpatient($category)
    {
        $ipCategories = MarketShareIP::select('category')->orderby('category')->distinct()->get();
        $opCategories = MarketShareOP::select('category')->orderby('category')->distinct()->get();

        $data = MarketShareIP::where('category', $category)->orderBy('num_records')->get();

        $totalShare = MarketShareIP::selectRaw('SUM(num_records) as total')->where('category', $category)->first();

        return view('market-share.inpatient', compact('data', 'ipCategories', 'opCategories', 'category', 'totalShare'));
    }

    public function outpatient($category)
    {
        $ipCategories = MarketShareIP::select('category')->orderby('category')->distinct()->get();
        $opCategories = MarketShareOP::select('category')->orderby('category')->distinct()->get();

        $data = MarketShareOP::where('category', $category)->orderBy('num_records')->get();

        $totalShare = MarketShareOP::selectRaw('SUM(num_records) as total')->where('category', $category)->first();

        return view('market-share.outpatient', compact('data', 'ipCategories', 'opCategories', 'category', 'totalShare'));
    }

}
