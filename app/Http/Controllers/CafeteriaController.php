<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\CafeteriaMenu;
use App\CafeteriaItem;
use App\CafeteriaMenuItem;
use Webpatser\Uuid\Uuid;

class CafeteriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = CafeteriaMenu::wherenotnull('id')->orderby('created_at', 'desc')->get();

        return view('cafeteria.index', compact('menus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function items(CafeteriaMenu $menu)
    {   
        $chosen = CafeteriaMenuItem::where('menu_id', '=', $menu->id)->get();

        $items = CafeteriaItem::wherenotnull('id')->get();

        return view('cafeteria.create', compact('items', 'menu', 'chosen'));
    }

    public function storeItems(CafeteriaMenu $menu, Request $request)
    {
        // dd($menu, $request->all());

        $current = CafeteriaMenuItem::where('menu_id', '=', $menu->id)->delete();

        $items = $request->items;
        
        foreach($items as $item)
        {
            CafeteriaMenuItem::create([
                'id' => Uuid::generate(4),
                'menu_id' => $menu->id,
                'item_id' => $item
            ]);
        }

        alert()->success('Items Added to Menu!', 'Success!');
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->offsetSet('id', Uuid::generate(4));

        CafeteriaMenu::create($request->all());

        alert()->success('Menu Created!', 'Success!');
        return redirect()->back();
    }

    public function menu(CafeteriaMenu $menu)
    {
        $menu = CafeteriaMenu::where('id', '=', $menu->id)->first();

        return view('cafeteria.mainDish', compact('menu'));
    }

}