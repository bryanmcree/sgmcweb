<?php

namespace App\Http\Controllers;

use App\CapitalRequest;
use App\CapitalRequestEmailHistory;
use App\CapitalRequestFile;
use App\CostCenters;
use App\History;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;

class CapitalRequestController extends Controller
{
    protected $budget_year;

    public function __construct()
    {
        //Lets figure the current budget year. 10-1 to 9-31

        if(Carbon::now() >= '10/1/'. Carbon::now()->year){
            $this->budget_year = Carbon::now()->year;
        }else{
            $this->budget_year = Carbon::now()->addyears(2)->year;
        }

    }

    public function index(){

        $budget_year = $this->budget_year;
        //dd($this->budget_year);

        $select_cost_center = CostCenters::select('style1','cost_center')
            ->whereNotIn('style2',['FINANCIAL PLANNING - 8213','BUSINESS INTELLIGENCE - 7009',''])
            ->wherenotnull('style2')
            ->orderby('cost_center')
            ->get();

        $items = CapitalRequest::wherenotnull('id');

        $total_pending = CapitalRequest::where('approved','=','1900-01-01')
            ->where('purchased','=','1900-01-01')
            ->where('denied_date','=','1900-01-01')
            ->where('class','=','Capital')
            ->sum('extended_total');
        //dd($total_pending);

        $total_approved = CapitalRequest::where('approved','<>','1900-01-01')
            ->where('purchased','=','1900-01-01')
            ->where('class','=','Capital')
            ->sum('extended_total');

        $total_purchased = CapitalRequest::where('approved','<>','1900-01-01')
            ->where('purchased','<>','1900-01-01')
            ->where('class','=','Capital')
            ->sum('extended_total');


        //History
        $history = new History();
        $history->id = \Uuid::generate(4);
        $history->action = 'Viewed Capital Request Tool';
        $history->userid = \Auth::user()->username;
        $history->search_string = 'None';
        $history->user_ip = \Request::ip();
        $history->save();

        return view('cr.index', compact('budget_year','select_cost_center','items','total_pending',
            'total_purchased','total_approved'));
    }

    public function detail($id){

        $budget_year = $this->budget_year;

        $cost_center = CostCenters::where('cost_center','=', $id)
            ->first();


        $items = CapitalRequest::where('cost_center','=', $id)
            ->get();

        $items_year = CapitalRequest::select('budget_year')
            ->where('cost_center','=', $id)
            ->distinct()
            ->get();
        // dd($items, $items_year);

        $total_pending = CapitalRequest::where('cost_center','=', $id)
            ->where('approved','=','1900-01-01')
            ->where('purchased','=','1900-01-01')
            ->where('denied_date','=','1900-01-01')
            ->where('class','=','Capital')
            ->sum('extended_total');
        //dd($total_pending);

        $total_approved = CapitalRequest::where('cost_center','=', $id)
            ->where('approved','<>','1900-01-01')
            ->where('purchased','=','1900-01-01')
            ->where('class','=','Capital')
            ->sum('extended_total');

        $total_purchased = CapitalRequest::where('cost_center','=', $id)
            ->where('approved','<>','1900-01-01')
            ->where('purchased','<>','1900-01-01')
            ->where('class','=','Capital')
            ->sum('extended_total');

        $total_capital = CapitalRequest::where('cost_center','=', $id)
            ->where('class','=','Capital')
            ->sum('extended_total');

        $total_operational = CapitalRequest::where('cost_center','=', $id)
            ->where('class','=','Operational')
            ->sum('extended_total');

        $total_tbd = CapitalRequest::where('cost_center','=', $id)
            ->where('class','=','TBD')
            ->sum('extended_total');


        $files = CapitalRequestFile::where('cost_center','=', $id)
            ->get();

        return view('cr.detail', compact('budget_year','cost_center','items','items_year','total_pending',
            'total_approved','total_purchased','files','total_capital','total_operational','total_tbd'));

    }

    public function add_item(Requests\CapitalRequestRequest $request){


        $request->offsetSet('id', Uuid::generate(4));
        $request->offsetSet('extended_total', $request->unit_price * $request->qty);
        $request->offsetSet('approved', '1900-01-01');
        $request->offsetSet('purchased', '1900-01-01');
        $request->offsetSet('inservice_date', '1900-01-01');
        $request->offsetSet('item_received', '1900-01-01');
        $request->offsetSet('denied_date', '1900-01-01');
        $request->offsetSet('forwarded_legal', '1900-01-01');
        $input = $request->all();
        //Input has been created lets save it to the database.
        CapitalRequest::create($input);
        //$project_id = Project::create($input)->id;

        //Lets write this action to the history table.
        $history = new History();
        $history->id = Uuid::generate(4);
        $history->action = 'New Capital Request Item';
        $history->userid = \Auth::user()->username;
        $history->search_string = $request->item_description;
        $history->user_ip = \Request::ip();
        $history->save();

        //Everything is good lets send feedback to the user and then redirect
        //alert()->success('Cost Center Added!', 'Success!');
        return redirect('/cr/detail/' . $request->cost_center);

    }

    public function item_detail($id){

        $budget_year = $this->budget_year;

        $item = CapitalRequest::where('id','=', $id)
            ->first();

        $cost_center = CostCenters::where('cost_center','=', $item->cost_center)
            ->first();

        $files = CapitalRequestFile::where('item_id','=', $id)
            ->where('cost_center','=', $item->cost_center)
            ->get();

            // dd($item, $files);

        return view('cr.item_detail', compact('budget_year','item','cost_center',
            'files'));
    }

    public function edit_item(){

        //dd();
        $item = CapitalRequest::find( Input::get('id') );
        //dd($item);
        $item->update(Input::except(['id']));
        alert()->success('Item Updated!', 'Success!');
        return redirect('/cr/detail/item/' . $item->id);
    }

    public function edit_settings(){

        //dd();
        $item = CapitalRequest::find( Input::get('id') );
        //dd($item);
        $item->update(Input::except(['id']));

        //Item is updated now lets send some emails

        //get project managers email
        $pm_email = User::where('employee_number','=',$item->project_manager)
            ->first();

        $creator_email = User::where('employee_number','=',$item->created_by)
            ->first();

        //dd($creator_email->mail);


        //dd(Input::get('approved'));


        // REMOVED FOR BUDGET PREP //


        // $pm_mail = $pm_email->mail; //Program manager
        // $creator_mail = $creator_email->mail; //Creator manager
        // $director = ['bryan.mcree@sgmc.org', 'trinh.ramirez@sgmc.org'];
        // $supply = ['colby.yates@sgmc.org', 'jeremy.foster@sgmc.org'];



        // if(Input::get('approved') != ''){

        //     //lets see if the user has already got an email
        //     $email_check = CapitalRequestEmailHistory::where('item_number','=',Input::get('id'))
        //         ->where('status_update','=','Approved')
        //         ->first();

        //     //dd($email_check);

        //     //send email
        //     if(empty($email_check)){
        //         Mail::raw('Your capital request item (' . $item->item_description .' - CER '. $item->cer_number .') has been approved.', function ($message) use ($pm_mail, $director, $supply, $creator_mail){
        //             $message->to($pm_mail);
        //             $message->cc($director);
        //             $message->cc($creator_mail);
        //             $message->subject('Capital Request Item - APPROVED');
        //         });
        //     //Update history so user does not get another one
        //         $email_history = new CapitalRequestEmailHistory();
        //         $email_history->item_number = Input::get('id');
        //         $email_history->status_update = 'Approved';
        //         $email_history->save();
        //     }

        // }

        // if(Input::get('purchased') != ''){

        //     $email_check = CapitalRequestEmailHistory::where('item_number','=',Input::get('id'))
        //         ->where('status_update','=','Purchased')
        //         ->first();

        //     if(empty($email_check)){
        //         Mail::raw('Your capital request item (' . $item->item_description .' - CER '. $item->cer_number .') has been ordered.', function ($message)use ($pm_mail, $director, $supply, $creator_mail){
        //             $message->to($pm_mail);
        //             $message->cc($director);
        //             $message->cc($creator_mail);
        //             $message->subject('Capital Request Item - ORDERED');
        //         });

        //         //Update history so user does not get another one
        //         $email_history = new CapitalRequestEmailHistory();
        //         $email_history->item_number = Input::get('id');
        //         $email_history->status_update = 'Purchased';
        //         $email_history->save();
        //     }


        // }

        // if(Input::get('inservice_date') != ''){

        //     $email_check = CapitalRequestEmailHistory::where('item_number','=',Input::get('id'))
        //         ->where('status_update','=','Inservice')
        //         ->first();

        //     if(empty($email_check)){
        //         Mail::raw('Your capital request item (' . $item->item_description .' - CER '. $item->cer_number .') has been placed in service.', function ($message) use ($pm_mail, $director, $supply, $creator_mail){
        //             $message->to($pm_mail);
        //             $message->cc($director);
        //             $message->cc($creator_mail);
        //             $message->subject('Capital Request Item - IN-SERVICE');
        //         });

        //         //Update history so user does not get another one
        //         $email_history = new CapitalRequestEmailHistory();
        //         $email_history->item_number = Input::get('id');
        //         $email_history->status_update = 'Inservice';
        //         $email_history->save();
        //     }

        // }

        // if(Input::get('item_received') != ''){

        //     $email_check = CapitalRequestEmailHistory::where('item_number','=',Input::get('id'))
        //         ->where('status_update','=','Received')
        //         ->first();

        //     if(empty($email_check)){
        //         Mail::raw('Your capital request item (' . $item->item_description .' - CER '. $item->cer_number .') has been received by materials management.', function ($message) use ($pm_mail, $director, $supply, $creator_mail){
        //             $message->to($pm_mail);
        //             $message->cc($director);
        //             $message->cc($creator_mail);
        //             $message->subject('Capital Request Item - RECEIVED');
        //         });

        //         //Update history so user does not get another one
        //         $email_history = new CapitalRequestEmailHistory();
        //         $email_history->item_number = Input::get('id');
        //         $email_history->status_update = 'Recieved';
        //         $email_history->save();
        //     }


        // }

        // if(Input::get('denied_date') != ''){

        //     $email_check = CapitalRequestEmailHistory::where('item_number','=',Input::get('id'))
        //         ->where('status_update','=','Denied')
        //         ->first();

        //     if(empty($email_check)){
        //         Mail::raw('Your capital request item (' . $item->item_description .') has been denied.  Login to view reason.', function ($message) use ($pm_mail, $director, $supply, $creator_mail){
        //             $message->to($pm_mail);
        //             $message->cc($director);
        //             $message->cc($creator_mail);
        //             $message->subject('Capital Request Item - DENIED');
        //         });

        //         //Update history so user does not get another one
        //         $email_history = new CapitalRequestEmailHistory();
        //         $email_history->item_number = Input::get('id');
        //         $email_history->status_update = 'Denied';
        //         $email_history->save();
        //     }

        // }


        alert()->success('Settings Updated!', 'Success!');
        return redirect('/cr/detail/item/' . $item->id);
    }

    public function edit_sbar(){

        //dd();
        $item = CapitalRequest::find( Input::get('id') );
        //dd($item);
        $item->update(Input::except(['id']));
        alert()->success('Sbar Updated!', 'Success!');
        return redirect('/cr/detail/item/' . $item->id);
    }

    public function file_upload(){

        $file = Input::file('file_name');
        $item_id = Input::get('item_id');
        $cost_center = Input::get('cost_center');
        $extension = $file->getClientOriginalExtension();
        $entry = new CapitalRequestFile();
        $entry->id = Uuid::generate(4);
        $entry->file_mime = $file->getClientMimeType();
        $entry->old_filename = $file->getClientOriginalName();
        $entry->new_filename = Uuid::generate(4) .'.'.$extension;
        $entry->item_id = $item_id;
        $entry->cost_center = $cost_center;
        $entry->file_extension = $extension;
        $entry->created_by = \Auth::user()->employee_number;
        $entry->file_size = $file->getClientSize();
        $entry->file_description = Input::get('file_description');

        Storage::disk('local')->put($entry->new_filename,  File::get($file));

        $entry->save();
        alert()->success('File has been uploaded.', 'File Uploaded');

        return redirect('/cr/detail/item/' . $item_id);

    }

    public function employeesearch()
    {

        $data = User::select("name",'employee_number')
            ->where('name', 'LIKE','%' . Input::get('query') . '%')
            ->whereIn('emp_status',['Active','Contract','Leave of Absence'])
            ->wherenotin('unit_code',['7009','8213','8210'])
            ->wherenull('deleted_at')
            ->orderby('name')
            ->get();
        //var_dump($data);

        foreach ($data as $key => $row) {
            $results['suggestions'][] = array('value' => $row['name'],'data' => $row['employee_number']);
        }

        return \Response::json($results);
    }

    public function destroy(CapitalRequest $item)
    {
        $item->delete();
    }
}
