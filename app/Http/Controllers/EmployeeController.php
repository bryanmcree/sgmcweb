<?php

namespace App\Http\Controllers;

use Adldap\Classes\Users;
use App\CostCenters;
use App\EmpTrend;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Employee;
use Illuminate\Support\Facades\Input;
use DB;
use PDF;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Listeners\UpdateLastLoginOnLogin;
use App\History;
use App\User;

class EmployeeController extends Controller
{
    public function test(){
        $users = User::find('11');

        //$users->manager_emp_number = 'TEST';
        //$users->save();
        dd($users);
    }


    public function index()
    {
        if(Input::has('search')) {
            $query = Input::get('search');
            //$sqlStmt = DB::table('CurrentEmployees')->where('first_name', 'LIKE', '%' . $query . '%');
            $tests = Users::where('first_name', 'LIKE', '%' . $query . '%')->paginate(20);
            
            $history = new History;
            $history->id = \Uuid::generate(4);
            $history->action = 'Employee Search';
            $history->userid = \Auth::user()->username;
            $history->user_ip = \Request::ip();
            $history->search_string = $query;
            $history->save();
        }
        $user ='';
        return View('employees.index', compact('tests','user'));
        //return view('people.index');
    }
    
    public function show($id)
    {
        $tests = Employee::where('emp_number', $id)->first();
        return view('employees.show', compact('tests'));
    }

    public static function printList()
    {
        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'Employee Print List';
        $history->userid = \Auth::user()->username;
        $history->search_string = 'None';
        $history->save();

        $tests = Employee::select('emp_number','last_name', 'first_name', 'unit_description','location')->where('Status', '=', 'Active')->get()->toArray();
        Excel::create('employees '.date('m-d-Y_hia'), function($excel) use($tests) {
            $excel->sheet('New sheet', function($sheet) use($tests) {
                $sheet->fromArray($tests);
            })->download('xlsx');
        });
    }
    
    public function employeeSearch(){

        $search = Input::get('search');

        $sqlStmt = User::
           where(function($query) use ($search){
                $query->Where('first_name', 'LIKE', '%' . $search . '%');
                $query->orWhere('last_name', 'LIKE', '%' . $search . '%');
                $query->orWhere('employee_number', 'LIKE', '%' . $search . '%');
                $query->orWhere('username', 'LIKE', '%' . $search . '%');
                $query->orWhere('unit_code', 'LIKE', '%' . $search . '%');
                $query->orWhere('unit_code_description', 'LIKE', '%' . $search . '%');
                $query->orWhere('title', 'LIKE', '%' . $search . '%');
            });
            if(Auth::user()->access != 'Admin'){
                //Allowing users to issue extra miles to contact employees
                //$sqlStmt->where('emp_status', '=', 'Active');
                $sqlStmt->wherein('emp_status', array('Active','Contract','Intermittent FMLA'));
            }
        $tests = $sqlStmt
            ->wherenull('deleted_at')
            //->where('emp_status', '<>', 'Terminated')
            ->orderBy('last_name')
            ->orderBy('first_name')
            ->paginate(20)
            ->appends(['search' => $search]);


        //dd($sqlStmt);
        //event(new UpdateLastLoginOnLogin('test'));

        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'Employee Search';
        $history->userid = \Auth::user()->username;
        $history->search_string = $search;
        $history->user_ip = \Request::ip();
        $history->save();

        return View('employees.index', compact('tests'));
    }

    public function widget_chart_json(){

        $employees = EmpTrend::selectRaw('CONVERT(varchar(12),created_at,110) as created_at,emp_value')
            ->where('emp_class','Employees')
            //->where('created_at', '<', '2019-01-01')
            ->whereyear('created_at', '=', Carbon::now()->year)
            ->orderby('created_at')
            ->get();


        //return View('employees.chart', compact('employees'));

        return $employees;
    }

    public function prn(){

        $employees = EmpTrend::selectRaw('CONVERT(varchar(12),created_at,110) as created_at,emp_value')
            ->where('emp_class','PRN')
            ->whereyear('created_at', '=', Carbon::now()->year)
            ->orderby('created_at')
            ->get();

        //return View('employees.chart', compact('employees'));

        return $employees;
    }

    public function RN(){

        $employees = EmpTrend::selectRaw('CONVERT(varchar(12),created_at,110) as created_at,emp_value')
            ->where('emp_class','RN')
            //->where('created_at', '<', '2019-01-01')
            ->whereyear('created_at', '=', Carbon::now()->year)
            ->orderby('created_at')
            ->get();

        //return View('employees.chart', compact('employees'));

        return $employees;
    }

    public function emp_chart(){


        $emp_count_chart_last_year = EmpTrend::selectRaw('CONVERT(varchar(12),created_at,110) as created_at,emp_value')
            ->where('emp_class','Employees')
            //->where('created_at', '<', '2019-01-01')
            ->whereyear('created_at', '=', Carbon::now()->subyear(1)->year)
            ->orderby('created_at')
            ->get();

        $emp_count_chart_current_year = EmpTrend::selectRaw('CONVERT(varchar(12),created_at,110) as created_at,emp_value')
            ->where('emp_class','Employees')
            //->where('created_at', '<', '2019-01-01')
            ->whereyear('created_at', '=', Carbon::now()->year)
            ->orderby('created_at')
            ->get();

        //dd($emp_count_chart);

        $employeesMax = EmpTrend::select('emp_value')
            ->where('emp_class','Employees')
            //->where('created_at', '<', '2019-01-01')
            ->whereyear('created_at', '=', Carbon::now()->year)
            ->orderby('emp_value','desc')
            ->first();

        $employeesMin = EmpTrend::select('emp_value')
            ->where('emp_class','Employees')
            //->where('created_at', '<', '2019-01-01')
            ->whereyear('created_at', '=', Carbon::now()->year)
            ->orderby('emp_value')
            ->first();

        $employeesCurrent = EmpTrend::select('emp_value')
            ->where('emp_class','Employees')
            //->where('created_at', '<', '2019-01-01')
            ->whereyear('created_at', '=', Carbon::now()->year)
            ->orderby('created_at','desc')
            ->first();
        //dd($employeesCurrent->emp_value);

        $avg_pay = User::where('emp_status','Active')
            ->where('workstatus','FT')
            ->avg('rate');
        //dd($avg_pay);


        return View('employees.chart', compact('employeesMax','employeesMin','employeesCurrent',
            'avg_pay','emp_count_chart_current_year','emp_count_chart_last_year','emp_count_chart_two_years'));
    }


    public function advanced(){

        $costcenters = CostCenters::wherenotnull('id')
            ->where('active_status', '=', '1')
            ->orderby('cost_center')
            ->get();


        $job_codes = User::select('job_code','title')
            ->where('emp_status','<>','Terminated')
            ->wherenotnull('job_code')
            ->wherenotnull('title')
            ->distinct('job_code')
            ->orderby('job_code')
            ->get();
        //dd($job_codes);

        return View('employees.advanced', compact('costcenters','job_codes'));
    }
}
