<?php

namespace App\Http\Controllers;

use GuzzleHttp\Collection;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Termination;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;

class TerminationController extends Controller
{
    public function index()
    {
        $terms = Termination::all();
        //dd($terms);
       return view('reports.terminations.index', compact('terms'));
    }
}
