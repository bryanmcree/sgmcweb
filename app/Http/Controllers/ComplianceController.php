<?php

namespace App\Http\Controllers;

use App\Compliance;
use App\History;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class ComplianceController extends Controller
{
    public function index(){

        return view('compliance.survey.index');
    }

    public function AddNewSurvey(Requests\SurveyRequest $request){

        $request->offsetSet('id', Uuid::generate(4));
        $request->offsetSet('ip_address', \Request::ip());
        $answers = $request->all();
        Compliance::create($answers);

        //dd($request->voluntary_info);

        //if(strlen($request->voluntary_info)<5){
        //    dd($request->voluntary_info);
        //}


        if(!is_null($request->voluntary_info)){

            if(strlen($request->voluntary_info)>5){
                Mail::raw($request->voluntary_info, function ($message){
                    $message->to('amanda.nijem@SGMC.ORG');
                    //$message->cc('bryan.mcree@sgmc.org');
                    $message->cc('alison.peterman@sgmc.org');
                    $message->subject('Compliance Survey Follow Up!');
                });
            }
        }

        return redirect('/compliance/survey/certificate');
    }

    public function certificate(){

        return view('compliance.survey.certificate');
    }

    public function dashboard(){

        $all_surveys = Compliance::where('voluntary_info','<>','')
            ->get();
        //dd($all_surveys);

        //Chart for question one
        $addressing_yes = Compliance::selectRaw('count(*) as yeses')
            ->where('addressing', '=', 1)
            ->first();
        //dd($addressing_yes);
        $addressing_no = Compliance::selectRaw('count(*) as nos')
            ->where('addressing', '=', 0)
            ->first();

        //Chart for question two
        $hotline_yes = Compliance::selectRaw('count(*) as yeses')
            ->where('hotline', '=', 1)
            ->first();
        $hotline_no = Compliance::selectRaw('count(*) as nos')
            ->where('hotline', '=', 0)
            ->first();

        //Chart for question three
        $non_retaliation_yes = Compliance::selectRaw('count(*) as yeses')
            ->where('non_retaliation', '=', 1)
            ->first();
        $non_retaliation_no = Compliance::selectRaw('count(*) as nos')
            ->where('non_retaliation', '=', 0)
            ->first();

        //Chart for question three
        $pressure_yes = Compliance::selectRaw('count(*) as yeses')
            ->where('pressure', '=', 1)
            ->first();
        $pressure_no = Compliance::selectRaw('count(*) as nos')
            ->where('pressure', '=', 0)
            ->first();

        //Chart for question three
        $rate_culture_1 = Compliance::selectRaw('count(*) as one')
            ->where('rate_culture', '=', 1)
            ->first();
        $rate_culture_2 = Compliance::selectRaw('count(*) as two')
            ->where('rate_culture', '=', 2)
            ->first();
        $rate_culture_3 = Compliance::selectRaw('count(*) as three')
            ->where('rate_culture', '=', 3)
            ->first();
        $rate_culture_4 = Compliance::selectRaw('count(*) as four')
            ->where('rate_culture', '=', 4)
            ->first();
        $rate_culture_5 = Compliance::selectRaw('count(*) as five')
            ->where('rate_culture', '=', 5)
            ->first();

        //cultural sum
        //$sum_culture = Compliance::get()
        //    ->sum('rate_culture');


        //Lets write this action to the history table.
        $history = new History();
        $history->id = Uuid::generate(4);
        $history->action = 'View Compliance Survey Dashboard';
        $history->userid = \Auth::user()->username;
        $history->search_string = 'N/A';
        $history->user_ip = \Request::ip();
        $history->save();

        return view('compliance.survey.dashboard',compact('all_surveys','addressing_no','addressing_yes','hotline_yes','hotline_no','non_retaliation_no'
        ,'non_retaliation_yes','pressure_no','pressure_yes','rate_culture_5','rate_culture_4','rate_culture_3','rate_culture_2','rate_culture_1',
            'sum_culture'));
    }
}
