<?php

namespace App\Http\Controllers;


use App\Http\Requests;
use Illuminate\Http\Request;

use DB;
use Carbon;
use App\PCCFoley;
use App\PCCRTCurrent;
use App\PCCCentralLine;
use App\PCCRoundingTool;
use Webpatser\Uuid\Uuid;


class PCCRoundingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $current = PCCRTCurrent::wherenotnull('id')
            ->orderBy('unit')    
            ->get();

        $complete = PCCRoundingTool::where('date', '=', Carbon::now()->startOfDay())
            ->orderBy('created_at', 'desc')
            ->get();

        $units = PCCRTCurrent::select('unit')
            ->distinct()
            ->orderby('unit')
            ->get();

        $nurse_leaders = DB::table('pcc_rounding_tool')
            ->select('nurse_leader')
            ->distinct()
            ->orderby('nurse_leader')
            ->get();

        $comp_count = 0;
        
        foreach($current as $curr)
        {
            foreach($complete as $comp)
            {
                if($comp->patient == $curr->patient_name && $comp->room_num == $curr->bed)
                {
                    $comp_count += 1;
                }
            }
        }

        // dd($comp_count);

        $foley = PCCRoundingTool::where('foley', '=', 'Y')->get();
        $central = PCCRoundingTool::where('central_line', '=', 'Y')->get();

        // dd(count($foley), count($central));

        $perfectF = count($foley) * 4;
        $perfectCL = count($central) * 6;


        $actualF = PCCFoley::wherenotnull('id')->count();
        $actualCL = PCCCentralLine::wherenotnull('id')->count();

        // dd($actualF);

        // foreach($foley as $criteria)
        // {
        //     $actualF += count($criteria->foleys);
        // }

        // foreach($central as $criteria)
        // {
        //     $actualCL += count($criteria->centrals);
        // }

        if($actualF == 0)
        {
            $complianceF = 0;
        }
        else
        {
            $complianceF = ($actualF / $perfectF) * 100;
        }

        if($actualCL == 0)
        {
            $complianceCL = 0;
        }
        else
        {
            $complianceCL = ($actualCL / $perfectCL) * 100;
        }

        // dd($perfectF, $perfectCL, $actualF, $actualCL, $complianceF, $complianceCL);
    
        return view('pcc_rounding.index', compact('current', 'complete', 'units', 'complianceF', 'complianceCL', 'nurse_leaders', 'comp_count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $patient = PCCRTCurrent::find($id);

        return view('pcc_rounding.create', compact('patient'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->offsetSet('id', Uuid::generate(4));

        $rounding = $request->except('foley_bundle', 'cl_bundle');
        $foley = $request->foley_bundle;
        $central = $request->cl_bundle;

        //dd($rounding, $foley, $central);

        PCCRoundingTool::create($rounding);
        
        if(!is_null($foley))
        {
            foreach($foley as $f)
            {
                PCCFoley::create([
                    'id' => Uuid::generate(4),
                    'pcc_rt_id' => $request->id,
                    'criteria' => $f,
                ]);
            }
        }

        if(!is_null($central))
        {
            foreach($central as $c)
            {
                PCCCentralLine::create([
                    'id' => Uuid::generate(4),
                    'pcc_rt_id' => $request->id,
                    'criteria' => $c,
                ]);
            }
        }

        alert()->success('Survey Recorded!', 'Success!');
        return redirect()->route('unitDetail', $request->unit);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(PCCRoundingTool $survey)
    {
        return view('pcc_rounding.show', compact('survey'));
    }

    public function report(Request $request)
    {
        $startDate = Carbon::parse($request->startDate)->format('Y-m-d H:i:s');
        $endDate = Carbon::parse($request->endDate)->format('Y-m-d H:i:s');

        if($request->has('nurse'))
        {
            $survey = PCCRoundingTool::whereBetween('date', [$startDate, $endDate])
                ->where('nurse_leader', '=', $request->nurse)
                ->orderBy('date', 'desc')
                ->get();

            $foley = PCCRoundingTool::whereBetween('date', [$startDate, $endDate])
                ->where('nurse_leader', '=', $request->nurse)
                ->where('foley', '=', 'Y')
                ->get();
            $central = PCCRoundingTool::whereBetween('date', [$startDate, $endDate])
                ->where('nurse_leader', '=', $request->nurse)
                ->where('central_line', '=', 'Y')
                ->get();
            
            $leader = PCCRoundingTool::where('nurse_leader', '=', $request->nurse)
                ->first();
        }
        else
        {
            $survey = PCCRoundingTool::whereBetween('date', [$startDate, $endDate])
                ->orderBy('date', 'desc')
                ->get();

            $foley = PCCRoundingTool::whereBetween('date', [$startDate, $endDate])
                ->where('foley', '=', 'Y')
                ->get();
            $central = PCCRoundingTool::whereBetween('date', [$startDate, $endDate])
                ->where('central_line', '=', 'Y')
                ->get();
        }
        

        $perfectF = count($foley) * 4;
        $perfectCL = count($central) * 6;

        $actualF = 0;
        $actualCL = 0;

        foreach($foley as $criteria)
        {
            $actualF += count($criteria->foleys);
        }

        foreach($central as $criteria)
        {
            $actualCL += count($criteria->centrals);
        }

        if($actualF == 0)
        {
            $complianceF = 0;
        }
        else
        {
            $complianceF = ($actualF / $perfectF) * 100;
        }

        if($actualCL == 0)
        {
            $complianceCL = 0;
        }
        else
        {
            $complianceCL = ($actualCL / $perfectCL) * 100;
        }

        return view('pcc_rounding.report', compact('startDate', 'endDate', 'survey', 'complianceF', 'complianceCL','leader'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PCCRoundingTool $survey)
    {   
        return view('pcc_rounding.edit', compact('survey'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PCCRoundingTool $survey)
    {
        $foley = PCCFoley::where('pcc_rt_id', '=', $survey->id)->get();
        $central = PCCCentralLine::where('pcc_rt_id', '=', $survey->id)->get();

        $survey->update($request->except('foley_bundle', 'cl_bundle'));

        foreach($foley as $f)
        {
            $f->delete();
        }

        foreach($central as $c)
        {
            $c->delete();
        }

        if(!is_null($request->foley_bundle))
        {
            foreach($request->foley_bundle as $f)
            {
                PCCFoley::create([
                    'id' => Uuid::generate(4),
                    'pcc_rt_id' => $survey->id,
                    'criteria' => $f,
                ]);
            }
        }

        if(!is_null($request->cl_bundle))
        {
            foreach($request->cl_bundle as $c)
            {
                PCCCentralLine::create([
                    'id' => Uuid::generate(4),
                    'pcc_rt_id' => $survey->id,
                    'criteria' => $c,
                ]);
            }
        }

        alert()->success('Survey Updated!', 'Success!');
        return redirect('/pcc');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PCCRoundingTool $survey)
    {
        $foley = PCCFoley::where('pcc_rt_id', '=', $survey->id)->get();
        $central = PCCCentralLine::where('pcc_rt_id', '=', $survey->id)->get();

        $survey->delete();

        foreach($foley as $f)
        {
            $f->delete();
        }

        foreach($central as $c)
        {
            $c->delete();
        }

    }

    public function unit($unit)
    {
        $patients = PCCRTCurrent::where('unit', '=', $unit)
            ->get();

        $complete = PCCRoundingTool::where('date', '=', Carbon::now()->startOfDay())
            ->where('unit', '=', $unit)
            ->orderBy('created_at', 'desc')
            ->get();

        $comp_count = 0;
        
        foreach($patients as $curr)
        {
            foreach($complete as $comp)
            {
                if($comp->patient == $curr->patient_name && $comp->room_num == $curr->bed)
                {
                    $comp_count += 1;
                }
            }
        }

        $foley = PCCRoundingTool::where('foley', '=', 'Y')
            ->where('unit', '=', $unit)
            ->get();

        $central = PCCRoundingTool::where('central_line', '=', 'Y')
            ->where('unit', '=', $unit)
            ->get();

        $perfectF = count($foley) * 4;
        $perfectCL = count($central) * 6;

        $actualF = 0;
        $actualCL = 0;

        foreach($foley as $criteria)
        {
            $actualF += count($criteria->foleys);
        }

        foreach($central as $criteria)
        {
            $actualCL += count($criteria->centrals);
        }

        if($actualF == 0)
        {
            $complianceF = 0;
        }
        else
        {
            $complianceF = ($actualF / $perfectF) * 100;
        }

        if($actualCL == 0)
        {
            $complianceCL = 0;
        }
        else
        {
            $complianceCL = ($actualCL / $perfectCL) * 100;
        }

        return view('pcc_rounding.unit', compact('patients', 'comp_count', 'complete', 'complianceF', 'complianceCL'));
    }


}
