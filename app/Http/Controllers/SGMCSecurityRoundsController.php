<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;

use App\Http\Requests;
use App\SecurityBadges;
use App\SecurityCitation;
use App\SecurityActivityList;
use App\SecurityLocationList;
use App\SecurityDispatch;
use App\SecurityRounds;
use App\SecurityViolation;
use App\SecurityIncident;
use App\SecurityIncidentList;
use App\User;
use App\History;
use Auth;
use Carbon;
use DB;

class SGMCSecurityRoundsController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $locations = SecurityLocationList::wherenotnull('id')->get();
        $officers = User::where('unit_code_description', '=', 'SAFETY & SECURITY')
            ->where('emp_status', '=', 'Active')
            ->get();

        $start = Carbon::now()->startOfWeek();
        $end = Carbon::now()->endOfWeek();

        $goal = SecurityRounds::whereBetween('date_time', [$start, $end])
            ->count();

        return view('sgmcsecurity.rounds', compact('user', 'locations', 'officers', 'goal', 'start', 'end'));
    }

    public function store(Request $request)
    {
        $request->offsetSet('id', Uuid::generate(4));

        $round = $request->all();

        SecurityRounds::create($round);

        alert()->success('Round Recorded!', 'Success!');
        return redirect()->back();
    }

    public function edit(SecurityRounds $round)
    {
        $user = Auth::user();
        $locations = SecurityLocationList::wherenotnull('id')->get();
        $officers = User::where('unit_code_description', '=', 'SAFETY & SECURITY')
            ->where('emp_status', '=', 'Active')
            ->get();

        return view('sgmcsecurity.edit.rounds', compact('user', 'locations', 'officers', 'round'));
    }

    public function update(SecurityRounds $round, Request $request)
    {
        $round->update($request->all());

        alert()->success('Round Updated!', 'Success!');
        return redirect('/security');
    }

    public function destroy(SecurityRounds $round)
    {
        $round->delete();
    }
}
