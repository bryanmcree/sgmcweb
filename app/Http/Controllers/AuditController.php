<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Uuid;
use Auth;
use App\Audit;
use App\AuditAnswer;
use App\AuditQuestion;
use App\AuditEPList;
use App\AuditStandardList;
use App\CostCenters;

class AuditController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $surveys = Audit::wherenotnull('id')
            ->where('visible', '1')
            ->orderby('name')
            ->get();

        $mySurveys = Audit::wherenotnull('id')
            ->where('created_by', Auth::user()->employee_number)
            ->orderby('name')
            ->get();

            // dd($mySurveys);
        
        return view('audit.index', compact('surveys', 'mySurveys'));
    }

    public function detail(Audit $audit)
    {
        $eps = AuditEPList::select('EP')
            ->wherenotnull('id')
            ->orderby('EP')
            ->get();

        $standards = AuditStandardList::select('standard')
            ->wherenotnull('id')
            ->orderby('standard')
            ->get();

        $questions = AuditQuestion::with('question_answers')
            ->where('survey_id','=',$audit->id)
            ->orderby('question_order')
            ->get();

        $cost_centers = CostCenters::where('active_status','=',1)
            ->orderby('style1')
            ->get();

        $yesnoNA = AuditAnswer::selectRaw('question_id, answer, count(*) as total')
            ->where('question_type','=','YesNoNA')
            ->groupby(['question_id','answer'])
            ->orderby('answer','DESC')
            ->get();
        // dd($yesnoNA);

        return view('audit.details', compact('audit', 'questions', 'eps', 'standards', 'yesnoNA', 'cost_centers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->offsetSet('id', Uuid::generate(4));

        Audit::create($request->all());

        alert()->success('Audit Survey Created!', 'Success!');
        return redirect('/audit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Audit $audit)
    {
        $audit->update($request->all());

        alert()->success('Audit Survey Updated!', 'Success!');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
