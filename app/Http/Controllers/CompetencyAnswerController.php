<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Uuid;
use Auth;
use Carbon;
use App\User;
use App\NurseCompetency;
use App\NursingCompetency;
use App\NursingCompCategory;
use App\NursingCompQuestion;
use App\NursingCompChecklist;

class CompetencyAnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(NursingCompetency $comp)
    {
        $categories = NursingCompCategory::where('comp_id', $comp->id)->get();
        $questions = NursingCompQuestion::where('comp_id', $comp->id)->get();
        $answers = NursingCompChecklist::where('comp_id', $comp->id)->where('nurse_id', Auth::user()->id)->where('is_comfortable', 1)->get();
        $answersComplete = NursingCompChecklist::where('comp_id', $comp->id)->where('nurse_id', Auth::user()->id)->where('is_complete', 1)->count();
        $answersPossible = NursingCompChecklist::where('comp_id', $comp->id)->where('nurse_id', Auth::user()->id)->count();
        $answersCheckoff = NursingCompChecklist::selectRaw('question_id, count(*) as total')->where('comp_id', $comp->id)->where('nurse_id', Auth::user()->id)->where('is_complete', 1)->groupby('question_id')->get();

        // dd($categories, $questions, $answers, $answersCheckoff);

        return view('competencies.user.complete', compact('comp', 'categories', 'questions', 'answers', 'answersComplete', 'answersPossible', 'answersCheckoff'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
                
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function detail(NurseCompetency $pivot)
    {
        $categories = NursingCompCategory::where('comp_id', $pivot->comp_id)->get();
        $questions = NursingCompQuestion::where('comp_id', $pivot->comp_id)->orderby('question_order')->get();

        $answers = NursingCompChecklist::where('comp_id', $pivot->comp_id)
            ->where('nurse_id', $pivot->nurse_id)
            ->get();
        
        // dd($categories, $questions, $answers);

        return view('competencies.manager.detail', compact('pivot', 'categories', 'questions', 'answers'));
    }

    public function approvalView($nurse, NursingCompetency $comp, NurseCompetency $pivot)
    {
        $awaitingApproval = NursingCompChecklist::selectRaw('id, comp_id, question_id, nurse_id')
            ->where('is_comfortable', 1)
            ->where('is_complete', 0)
            ->where('nurse_id', $nurse)
            ->where('comp_id', $comp->id)
            ->groupby(['comp_id', 'question_id', 'nurse_id', 'id'])
            ->get();

        $completionDay = $pivot->created_at->addDays($comp->days_to_complete);

        $remainingDays = $completionDay->diffInDays(Carbon::now());

        return view('competencies.manager.approve', compact('awaitingApproval', 'pivot', 'nurse', 'completionDay', 'remainingDays'));
    }

    public function approve(Request $request, NursingCompChecklist $comp)
    {
        $unit = User::select('unit_code_description')->where('id', $comp->nurse_id)->first();

        $comp->update([
            'is_complete' => 1,
            'manager_id' => Auth::user()->id,
            'date_completed' => $request->date_completed,
            'validation_method' => $request->validation_method,
            'age_group' => $request->age_group,
            'unit' => $unit->unit_code_description
        ]);

        $total = NursingCompChecklist::where('comp_id', $comp->comp_id)->where('nurse_id', $comp->nurse_id)->where('is_complete', 1)->count();
        $totalPossible = NursingCompChecklist::where('comp_id', $comp->comp_id)->where('nurse_id', $comp->nurse_id)->count();

        // dd($total, $totalPossible);

        if($total == $totalPossible)
        {
            $nurseComp = NurseCompetency::where('id', $request->pivot_id)->first();
            $date = NursingCompChecklist::selectRaw('MAX(date_completed) as completed_date')->wherenotnull('date_completed')->where('nurse_id', $comp->nurse_id)->where('comp_id', $comp->comp_id)->first();

            $nurseComp->update([
                'date_completed' => $date->completed_date,
            ]);
        }

        alert()->success('Competencies Approved!', 'Success!');
        return back();
    }

    public function updateNotes(Request $request, NurseCompetency $pivot)
    {
        $pivot->update([
            'notes' => $request->notes
        ]);

        alert()->success('Notes Updated!', 'Success!');
        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $answersCount = NursingCompChecklist::where('comp_id', $request->comp_id)->where('nurse_id', Auth::user()->id)->get();
        
        // dd($answersCount, $request->all_questions);

        foreach($request->all_questions as $question)
        {
            $answersCheck = NursingCompChecklist::where('question_id', $question)->where('comp_id', $request->comp_id)->where('nurse_id', Auth::user()->id)->first();
            $questionData = NursingCompQuestion::where('id', $question)->first();

            if(empty($answersCheck))
            {
                if($questionData->is_checkoff == 1)
                {
                    for($c = 0; $c < $questionData->checks_required; $c++)
                    {
                        NursingCompChecklist::create([
                            'id' => Uuid::generate(4),
                            'comp_id' => $request->comp_id,
                            'question_id' => $question,
                            'nurse_id' => $request->nurse_id,
                            'is_complete' => 0
                        ]);
                    }
                }
                else
                {
                    NursingCompChecklist::create([
                        'id' => Uuid::generate(4),
                        'comp_id' => $request->comp_id,
                        'question_id' => $question,
                        'nurse_id' => $request->nurse_id,
                        'is_complete' => 0
                    ]);
                }
                
            }
        }

        $formAnswers = $request->answers;

        if(!is_null($formAnswers))
        {
            // dd($request->all(), $formAnswers);

            foreach($request->all_questions as $question)
            {
                $data = NursingCompChecklist::where('question_id', $question)->where('nurse_id', Auth::user()->id)->get();
                
                foreach($data as $d)
                {
                    $d->update([
                        'is_comfortable' => 0,
                    ]);
                }               
            }
            
            foreach($formAnswers as $chosen)
            {
                $chosenData = NursingCompChecklist::where('question_id', $chosen)->where('nurse_id', Auth::user()->id)->get();
                
                foreach($chosenData as $chosen)
                {
                    $chosen->update([
                        'is_comfortable' => 1,
                    ]);
                }                
            }
        }

        alert()->success('Competency Checklist Updated!', 'Success!');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
