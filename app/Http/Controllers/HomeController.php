<?php

namespace App\Http\Controllers;

use App\ExtraMile;
use App\Http\Requests;
use App\SSIS;
use App\TrendEmployees;
use App\User;
use Illuminate\Http\Request;
use \Khill\Lavacharts;
use App\Employee;
use App\Termination;
use DB;
use Carbon\Carbon;
use App\AD;
use App\History;
use App\Paydays;
use App\EmployeeHealthMedicalAlert;
use Auth;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function ie()
    {
        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'Someone Opened in IE';
        $history->userid = "No Information";
        $history->search_string = "No information";
        $history->user_ip = \Request::ip();
        $history->save();

        return view("ie.index");
    }

    public function index()
    {



        //removes generic AD user so they don't get errors when logged in.

        if(\Auth::user()->username =='radspu01') {

            //return redirect('/logout')->with('success', 'Please do not use "GENERIC" login credentials.  Use your assigned SGMC login.');
            \Auth::logout();
            return view("hpf")->with('success', 'Please do not use "GENERIC" login credentials.  Use your assigned SGMC login.');

        }ELSE{
            $empstats = User::where('username','=',\Auth::user()->username)
                ->whereNotNull('username')
                ->first();


            $ssis = SSIS::select('run_status')
                ->where('run_date','>=', \Carbon\Carbon::today())
                ->where('run_status','=', 0)
                ->get();
            //dd($ssis);

            $extra_mile_6 = ExtraMile::where("created_at",">", Carbon::now()->subMonths(6))
                ->orderby('created_at','DESC')
                //->paginate(10);
                ->get();

            $extra_mile = ExtraMile::where("created_at",">", Carbon::now()->subMonths(1))
                ->orderby('created_at','DESC')
                //->paginate(10);
                ->get();

            $medicalAlerts = EmployeeHealthMedicalAlert::where('user_id', '=', Auth::user()->employee_number)
                ->where('importance', '=', '1')
                ->get();

            //Return view with ALL above graphs
            return view('home', compact('ssis', 'extra_mile_6', 'extra_mile', 'empstats', 'medicalAlerts'));
        }



    }
}
