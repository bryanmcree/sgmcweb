<?php

namespace App\Http\Controllers;



use App\History;
use App\SGMCDaily;
use App\SGMCDailyEpicStats;
use App\SGMCDailyPayroll;
use Carbon\Carbon;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;


class SGMCDailyController extends Controller

{

    public function index(){

        //Get Yesterdays Data

        $data = SGMCDaily::where('report_date', '=', Carbon::now()->format('Y-m-d'))
            ->first();

        $yesterday = SGMCDaily::where('report_date', '=', Carbon::now()->subDays(1)->format('Y-m-d'))
            ->first();

        if(is_null($yesterday))
        {
            $isYesterday = false;
        }
        else
        {
            $isYesterday = true;
        }

        $all_days = SGMCDaily::where('report_date', '>=', Carbon::now()->subDays(10)->format('Y-m-d'))
            ->orderby('report_date')
            ->get();

        $all = SGMCDaily::wherenotnull('id')->get();

        // dd($data, $yesterday);


        $history = new History();
        $history->id = \Uuid::generate(4);
        $history->action = 'Daily Scorecard';
        $history->userid = \Auth::user()->username;
        $history->user_ip = \Request::ip();
        $history->search_string = 'N/A';
        $history->save();

        return View('daily.index', compact('data','yesterday','all_days','all', 'isYesterday'));
    }

    public function download_overtime(){


        $overtime = SGMCDailyPayroll::select('start_date','last_name','first_name','employee_number','cost_center','unit_description','pay_code','pay_code_description','total_hours')
            ->where('start_date', '=', Carbon::now()->format('Y-m-d'))
            ->wherein('pay_code',['BOD','BOE','BON','CALLBACK','CBD','CBE','CBN','DAYOVT','DBLTIME','DOT',
                'EDDOUTSOVT','EOT','EPIC EDU OVT','EVEOVT','INHSEDDOVT','MEETING OVT','MOD','MOE','NIGHTOVT','NOT','ORTOVT','SALNONEXMTP'])
            ->get();
        //dd($overtime);

        Excel::create('SGMC_Daily_Overtime_Employees '.date('m-d-Y_hia'), function($excel) use($overtime) {
            $excel->sheet('New sheet', function($sheet) use($overtime) {

                //Adds Zeros back in null fields
                $sheet->fromArray($overtime, null, 'A1', true);

                //This is where the pane freeze happens
                $sheet->setFreeze('K2');

                //sets the top row a background color
                //$sheet->row(1, ['Col 1', 'Col 2', 'Col 3']); // etc etc
                $sheet->row(1, function($row) { $row->setBackground('#CCCCCC'); });

                //Sets top row bold
                $sheet->cells('A1:I1', function($cells) {
                    $cells->setFont(array(
                        'bold'       =>  true
                    ));
                });


            })->download('xlsx');
        });
    }

    public function download_epic_stats(){

        $epic_stats = SGMCDailyEpicStats::select('start_date','end_date','stat_description','stat_location','stat_category','stat_value')
            ->where('start_date', '=', Carbon::now()->subDays(1)->format('Y-m-d'))
            ->get();

        //dd($epic_stats);

        Excel::create('SGMC_Daily_Epic_Stats '.date('m-d-Y_hia'), function($excel) use($epic_stats) {
            $excel->sheet('New sheet', function($sheet) use($epic_stats) {

                //Adds Zeros back in null fields
                $sheet->fromArray($epic_stats, null, 'A1', true);

                //This is where the pane freeze happens
                $sheet->setFreeze('K2');

                //sets the top row a background color
                //$sheet->row(1, ['Col 1', 'Col 2', 'Col 3']); // etc etc
                $sheet->row(1, function($row) { $row->setBackground('#CCCCCC'); });

                //Sets top row bold
                $sheet->cells('A1:F1', function($cells) {
                    $cells->setFont(array(
                        'bold'       =>  true
                    ));
                });


            })->download('xlsx');
        });

    }

    public function download_history(){

        $history = SGMCDaily::select('report_date','worked_hours','paid_hours','overtime_hours','overtime_percent','equivalent_admissions',
            'observation_patients','acute_admissions','clinic_visits','surgery_cases','discharges'
            ,'vil_worked_hours'
            ,'vil_paid_hours'
            ,'vil_overtime_hours'
            ,'lan_worked_hours'
            ,'lan_paid_hours'
            ,'lan_overtime_hours'
            ,'ber_worked_hours'
            ,'ber_paid_hours'
            ,'ber_overtime_hours'
            ,'snh_worked_hours'
            ,'snh_paid_hours'
            ,'snh_overtime_hours'
            ,'sgmc_worked_hours'
            ,'sgmc_paid_hours'
            ,'sgmc_overtime_hours'
            ,'ems_worked_hours'
            ,'ems_paid_hours'
            ,'ems_overtime_hours'
            ,'reflab_worked_hours'
            ,'reflab_paid_hours'
            ,'reflab_overtime_hours'
            ,'clinic_worked_hours'
            ,'clinic_paid_hours'
            ,'clinic_overtime_hours'
            ,'hospice_worked_hours'
            ,'hospice_paid_hours'
            ,'hospice_overtime_hours'
            ,'foundation_worked_hours'
            ,'foundation_paid_hours'
            ,'foundation_overtime_hours')
            ->wherenotnull('id')
            ->orderby('report_date', 'desc')
            ->get();

        Excel::create('SGMC_Daily_Epic_Stats '.date('m-d-Y_hia'), function($excel) use($history) {
            $excel->sheet('New sheet', function($sheet) use($history) {

                //Adds Zeros back in null fields
                $sheet->fromArray($history, null, 'A1', true);

                //This is where the pane freeze happens
                // $sheet->setFreeze('K2');

                //sets the top row a background color
                //$sheet->row(1, ['Col 1', 'Col 2', 'Col 3']); // etc etc
                $sheet->row(1, function($row) { $row->setBackground('#CCCCCC'); });

                //Sets top row bold
                $sheet->cells('A1:J1', function($cells) {
                    $cells->setFont(array(
                        'bold'       =>  true
                    ));
                });


            })->download('xlsx');
        });

    }

}
