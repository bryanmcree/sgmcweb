<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Chaplain;
use App\History;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ChaplainController extends Controller
{
    public function index()
    {

        $data = Chaplain::orderBy('created_at','DESC')
            ->where('created_at', '>=', Carbon::now()->subDay(4))
            ->get();
        return view('chaplain.index', compact('data'));
    }
    
    public function search ()
    {
        $query = Input::get('search');
        $data = Chaplain::orderBy('created_at','DESC')
            ->orWhere('chaplain', 'LIKE', '%' . $query . '%')
            ->orWhere('LastName', 'LIKE', '%' . $query . '%')
            ->orWhere('MRN', 'LIKE', '%' . $query . '%')
            ->orWhere('type', 'LIKE', '%' . $query . '%')
            ->orWhere('department', 'LIKE', '%' . $query . '%')
            ->orWhere('followup', 'LIKE', '%' . $query . '%')
            ->orWhere('notes', 'LIKE', '%' . $query . '%')
            ->orWhere('FirstName', 'LIKE', '%' . $query . '%')->paginate(20)
            ->appends(['search' => $query]);
            //->take(15)
        return view('chaplain.search', compact('data'));
        
    }
    
    public function AddEntry(Requests\SystemListRequest $request)
    {
        //$request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();
        Chaplain::create($input);
        alert()->success('Pastoral encounter has been added!', 'Success!');

        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'Chaplain made new entry';
        $history->userid = \Auth::user()->username;
        $history->search_string = 'None';
        $history->user_ip = \Request::ip();
        $history->save();

        return redirect('/chaplain');
    }

    public function EditEntry()
    {
        $entry = Chaplain::find( Input::get('id') );
        $entry->update(Input::all());

        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'Chaplain updated entry';
        $history->userid = \Auth::user()->username;
        $history->search_string = 'None';
        $history->user_ip = \Request::ip();
        $history->save();

        alert()->success('Pastoral Encounter has been Updated!', 'Success!');
        return redirect('/chaplain');
    }

    public function DeleteEntry($id)
    {
        $SystemsContacts = Chaplain::find( $id );
        $SystemsContacts->delete();
        return redirect('/chaplain');
    }

    public function report_index(){

        return view('chaplain.reports.index');
    }

    public function report_view(){

        //Select Month and Year
        $start_date  = Input::get('start_date');
        $end_date  = Input::get('end_date');

        $total = Chaplain::whereBetween('created_at', array($start_date, $end_date))
            ->count();

        $types = Chaplain::select(DB::raw('type, count(*) as total'))
            ->whereBetween('created_at', array($start_date, $end_date))
            ->groupby('type')
            ->get();


        $chaplains = Chaplain::select(DB::raw('chaplain, count(*) as total'))
            ->whereBetween('created_at', array($start_date, $end_date))
            ->groupby('chaplain')
            ->get();

        $departments = Chaplain::select(DB::raw('department, count(*) as total'))
            ->whereBetween('created_at', array($start_date, $end_date))
            ->groupby('department')
            ->get();

        //dd($type);

        return view('chaplain.reports.view', compact('types','chaplains','departments','total',
            'start_date','end_date'));
    }

    public function print_report(){

        //Select Month and Year
        $start_date  = Input::get('start_date');
        $end_date  = Input::get('end_date');
        //dd($start_date);

        $encounters = Chaplain::whereBetween('created_at', array($start_date, $end_date))
            ->get();
        //dd($encounters);

        return view('chaplain.reports.encounter', compact('encounters'));
    }
}
