<?php

namespace App\Http\Controllers;

use App\ExecSummary;
use App\ExecSummaryBudget;
use App\History;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;

class ExecSummaryController extends Controller
{
    public function index(){

        $year_summary = ExecSummary::where('report_date','>=', Carbon::now()->startOfYear())
            ->get();

        $month_summary = ExecSummary::where('report_date', '>=', Carbon::now()->startOfMonth())
            ->orderby('report_date')
            ->get();

        $yesterday_summary = ExecSummary::where('report_date','=', Carbon::yesterday()->format('Y-m-d'))
            ->first();
        $day_before_summary = ExecSummary::where('report_date','=', Carbon::now()->subDays(2)->format('Y-m-d'))
            ->first();

        $budget = ExecSummaryBudget::where('budget_month','=', Carbon::now()->format('m'))
            ->where('budget_year','=', Carbon::now()->format('Y'))
            ->first();

        $days_in_month = cal_days_in_month(CAL_GREGORIAN,Carbon::now()->format('m'),Carbon::now()->format('Y'));
        $day_of_month = Carbon::yesterday()->format('d');
        //dd($day_of_month);

        $history = new History();
        $history->id = \Uuid::generate(4);
        $history->action = 'Viewed ES Dashboard';
        $history->userid = \Auth::user()->username;
        $history->user_ip = \Request::ip();
        $history->search_string = 'N/A';
        $history->save();

        return view('exec_summary.index', compact('yesterday_summary','day_before_summary','year_summary'
        ,'month_summary','budget','days_in_month','day_of_month'));
    }
}
