<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Uuid;
use App\SecurityOrientation;
use Carbon;

class SGMCSecurityOrientationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orientations = SecurityOrientation::where('created_at', '>=', Carbon::now()->subMonths(3))->orderby('date', 'desc')->get();
        
        return view('sgmcsecurity.orientation', compact('orientations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->offsetSet('id', Uuid::generate());

        SecurityOrientation::create($request->all());

        alert()->success('Form Submitted', 'Success!');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SecurityOrientation $orientation)
    {
        return view('sgmcsecurity.edit.orientation', compact('orientation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SecurityOrientation $orientation)
    {
        $new = $request->all();

        $orientation->update($new);

        alert()->success('Form Submitted', 'Success!');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SecurityOrientation $orientation)
    {
        $orientation->delete();
    }
}
