<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\RoleUser;
use App\User;
use Webpatser\Uuid\Uuid;
use Image;
use DB;
use App\State;
use App\Role;
use App\Http\Controllers\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use App\SystemList;
use App\SystemsContacts;
use Maatwebsite\Excel\Facades\Excel;
use Alert;
use App\Productivity;
use App\SystemsGroup;
use App\CostCenters;
use App\DataSubmit;

class DataSubmitController extends Controller
{
    public function index(){
        return view('data.index');
    }

    public function store(Requests\SystemListRequest $request){
        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();
        DataSubmit::create($input);
        alert()->success('Your information has been submitted!', 'Success!');
        return redirect('/');
    }
}
