<?php

namespace App\Http\Controllers;

use App\History;
use App\MenuCategory;
use App\MenuItems;
use App\MenuDaily;
use App\ItemMenuPivot;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use App\Http\Requests;
use Carbon\Carbon;
use DateTime;

class MenuController extends Controller
{

    public function index()
    {
        $menu_daily = MenuDaily::wherenotnull('id')->orderBy('created_at', 'desc')
            ->get();

        return view('menu.admin.index', compact('menu_daily'));
    }

    // ITEMS CRUD //

    public function items(){

        $menu_category = MenuCategory::select('id','category')
            ->get();

        $menu_item = MenuItems::wherenotnull('id')->orderBy('category', 'asc')
            ->get();

        $history = new History();
        $history->id = \Uuid::generate(4);
        $history->action = 'Menu Application';
        $history->userid = \Auth::user()->username;
        $history->user_ip = \Request::ip();
        $history->search_string = 'N/A';
        $history->save();

        return view('menu.admin.items', compact('menu_category','menu_item'));

    }

    public function addItem(Requests\SystemListRequest $request){

        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();

        MenuItems::create($input);
        
        alert()->success('Item Entered', 'Success!');
        return redirect('/menu/admin/items');
    }

    public function editItem($id)
    {
        $menu_category = MenuCategory::select('id','category')
            ->get();

        $item = MenuItems::Find($id);

        return view('menu.admin.editItem', compact('item', 'menu_category'));
    }

    public function updateItem(Request $request, $id)
    {
        $item = MenuItems::Find($id);

        $item->id = $request->id;
        $item->title = $request->title;
        $item->price = $request->price;
        $item->calories = $request->calories;
        $item->category = $request->category;
        $item->created_by = $request->created_by;
        $item->ingredients = $request->ingredients;

        $item->save();

        alert()->success('Item Updated', 'Success!');
        return redirect('/menu/admin/items');

    }

    public function destroyItem($id)
    {
        $item = MenuItems::Find($id);

        $item->delete();

        alert()->success('Item Deleted', 'Success!');
        return redirect('/menu/admin/items');
    }




    // MENU CRUD //

    public function menus(){

        $menu_daily = MenuDaily::wherenotnull('id')
            ->get();

        $menu_category = MenuCategory::select('id','category')
        ->get();

        $main = MenuItems::where('category', '=', '1')
        ->get();

        $side = MenuItems::where('category', '=', '2')
        ->get();

        $extra = MenuItems::where('category', '=', '3')
        ->get();

        $feature = MenuItems::where('category', '=', '4')
        ->get();

        return view('menu.admin.menus', compact('menu_daily', 'menu_category', 'main', 'side', 'extra', 'feature'));
    }

    public function addMenu(Request $request)
    {
        //dd($request);
        $menu = new MenuDaily;

        $menu->offsetSet('id', Uuid::generate(4));
        $menu->created_by = $request->created_by;
        $menu->start_date = $request->start_date;
        $menu->location = $request->location;
        $menu->Display = $request->Display;

        $menu->save();

        $menu->menuItems()->sync($request->item, false);

        alert()->success('Menu Created', 'Success!');
        return redirect('/menu/admin/menus');
    }

    // public function show($id)
    // {
    //     $menu = MenuDaily::find($id);

    //     $main = $menu->menuItems()
    //     ->where('category', '=', '1')
    //     ->get();

    //     $side = $menu->menuItems()
    //     ->where('category', '=', '2')
    //     ->get();

    //     $extra = $menu->menuItems()
    //     ->where('category', '=', '3')
    //     ->get();

    //     $feature = $menu->menuItems()
    //     ->where('category', '=', '4')
    //     ->get();

    //     //$waffles = MenuItems::where('id', '=', '11de19de-72e6-4a25-8a01-7337486e5e7d');

    //     $counter = 0;
    //     $half = (count($main) + count($side) + count($extra) + count($feature)) / 2;
    //     //dd($half);
    //     return view('menu.admin.show', compact('menu', 'main', 'side', 'extra', 'feature', 'counter', 'half'));
    // }

    public function showGrill()
    {
        $menuBreakfast = MenuDaily::find('b90f5a41-b12d-4805-a7ec-749be1ff7a09');

        $menu = MenuDaily::find('ce0822f0-7d6f-42d1-a47c-f4e6d903e8f4');

        //dd($menu);

        //Breakfast
        $mainBreak = $menuBreakfast->menuItems()->get();

        $sideBreak = $menuBreakfast->menuItems()
        ->where('category', '=', '2')
        ->get();

        $extraBreak = $menuBreakfast->menuItems()
        ->where('category', '=', '3')
        ->get();

        $featureBreak = $menuBreakfast->menuItems()
        ->where('category', '=', '4')
        ->get();

        // Lunch
        $main = $menu->menuItems()
        ->where('category', '=', '1')
        ->get();

        $side = $menu->menuItems()
        ->where('category', '=', '2')
        ->get();

        $extra = $menu->menuItems()
        ->where('category', '=', '3')
        ->get();

        $feature = $menu->menuItems()
        ->where('category', '=', '4')
        ->get();

        //dd($side, $mainBreak);

        $counter = 0;
        $half = (count($main) + count($side) + count($extra) + count($feature)) / 2;
        //dd($half);

        return view('menu.admin.grill', compact('menu', 'menuBreakfast', 'mainBreak', 'sideBreak', 'extraBreak', 'featureBreak', 'main', 'side', 'extra', 'feature', 'counter', 'half'));
    }

    public function showDeli()
    {
        $menuBreakfast = MenuDaily::find('cd743d71-1558-4f24-b3b7-668206026be2');

        $menu = MenuDaily::find('8b11e6ce-677b-4a54-852b-c1fbab3de9db');

        $mainBreak = $menuBreakfast->menuItems()->get();

        $sideBreak = $menuBreakfast->menuItems()
        ->where('category', '=', '2')
        ->get();

        $extraBreak = $menuBreakfast->menuItems()
        ->where('category', '=', '3')
        ->get();

        $featureBreak = $menuBreakfast->menuItems()
        ->where('category', '=', '4')
        ->get();

        //DELI

        $main = $menu->menuItems()
        ->where('category', '=', '1')
        ->get();

        $side = $menu->menuItems()
        ->where('category', '=', '2')
        ->get();

        $extra = $menu->menuItems()
        ->where('category', '=', '3')
        ->get();

        $feature = $menu->menuItems()
        ->where('category', '=', '4')
        ->get();

        //dd($sideBreak);

        $counter = 0;
        $half = (count($main) + count($side) + count($extra) + count($feature)) / 2;
        //dd($half);

        return view('menu.admin.deli', compact('menu', 'menuBreakfast', 'mainBreak', 'sideBreak', 'extraBreak', 'featureBreak', 'main', 'side', 'extra', 'feature', 'counter', 'half'));
    }

    public function showSalad()
    {
        // $menuBreakfast = MenuDaily::find('8b9a06a0-1ebe-4d0e-b1b5-ac0ae22836d6');

        // $menu = MenuDaily::find('5092c905-47e8-4f37-86c3-11faba938f30');

        // $mainBreak = $menuBreakfast->menuItems()->get();

        // $sideBreak = $menuBreakfast->menuItems()
        // ->where('category', '=', '2')
        // ->get();

        // $extraBreak = $menuBreakfast->menuItems()
        // ->where('category', '=', '3')
        // ->get();

        // $featureBreak = $menuBreakfast->menuItems()
        // ->where('category', '=', '4')
        // ->get();

        //SALAD

        // $main = $menu->menuItems()
        // ->where('category', '=', '1')
        // ->get();

        // $side = $menu->menuItems()
        // ->where('category', '=', '2')
        // ->get();

        // $extra = $menu->menuItems()
        // ->where('category', '=', '3')
        // ->get();

        // $feature = $menu->menuItems()
        // ->where('category', '=', '4')
        // ->get();


        // $counter = 0;
        // $half = (count($main) + count($side) + count($extra) + count($feature)) / 2;
        //dd($half);

       // , compact('menu', 'menuBreakfast', 'mainBreak', 'sideBreak', 'extraBreak', 'featureBreak', 'main', 'side', 'extra', 'feature', 'counter', 'half')

        return view('menu.admin.salad');
    }

    public function edit($id)
    {
        $menu = MenuDaily::find($id);

        $chosenMain = $menu->menuItems()->where('category', '=', '1')->orderBy('id', 'asc')
            ->getRelatedIds();

        $chosenSide = $menu->menuItems()->where('category', '=', '2')->orderBy('id', 'asc')
            ->getRelatedIds();

        $chosenExtra = $menu->menuItems()->where('category', '=', '3')->orderBy('id', 'asc')
            ->getRelatedIds();

        $chosenFeature = $menu->menuItems()->where('category', '=', '4')->orderBy('id', 'asc')
            ->getRelatedIds();

        $maxMain = count($chosenMain);
        $maxSide = count($chosenSide);
        $maxExtra = count($chosenExtra);
        $maxFeature = count($chosenFeature);
        
        $counter = 0;

        $main = MenuItems::where('category', '=', '1')->orderBy('id', 'asc')
        ->get();

        $side = MenuItems::where('category', '=', '2')->orderBy('id', 'asc')
        ->get();

        $extra = MenuItems::where('category', '=', '3')->orderBy('id', 'asc')
        ->get();

        $feature = MenuItems::where('category', '=', '4')->orderBy('id', 'asc')
        ->get();

        //dd($chosen, $max, $main, $side);

        $newFormat = date('Y-m-d', strtotime($menu->start_date));

        return view('menu.admin.edit', compact('menu', 'newFormat', 'main', 'side', 'extra', 'feature', 'chosenMain', 'chosenSide', 'chosenExtra', 'chosenFeature', 'maxMain', 'maxSide', 'maxExtra', 'maxFeature', 'counter'));
    }

    public function update(Request $request, $id)
    {
        $menu = MenuDaily::find($id);

        $menu->id = $request->id;
        $menu->created_by = $request->created_by;
        $menu->start_date = $request->start_date;
        $menu->location = $request->location;
        $menu->Display = $request->Display;

        $menu->save();

        $menu->menuItems()->detach();
        $menu->menuItems()->sync($request->item, false);

        alert()->success('Menu Updated', 'Success!');
        return redirect('/menu/admin');
    }

    public function destroy($id)
    {
        $menu = MenuDaily::find($id);

        $menu->menuItems()->detach();

        $menu->delete();

        alert()->success('Menu Deleted!', 'Success!');
        return redirect('/menu/admin');

    }

}
