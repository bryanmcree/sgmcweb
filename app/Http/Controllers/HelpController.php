<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests;
use Illuminate\Support\Facades\Mail;
use App\History;

class HelpController extends Controller
{
    public function sendHelp(Requests\SystemListRequest $request) {

        $data = $request->all();

        //dd($data);

        Mail::send('email.helpRequest', $data, function ($m) use ($data) {

            $m->from('sgmcweb-no-reply@sgmc.org', 'Help Request');
            $m->to('business.intelligence@sgmc.org');
            // $m->CC('landon.courson@sgmc.org');
            $m->subject('web.sgmc.org HELP Request!');
            //$m->CC('sghelpdesk@sgmc.org');
            $m->CC($data['mail']);
            $m->replyTo($data['mail']);
        });


        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'HELP Requested';
        $history->userid = \Auth::user()->username;
        $history->search_string = '<b>Phone Number:</b> '. $data['phone_number'] .' / <b>Problem:</b> '. $data['problem'];
        $history->save();

        alert()->success('Help is on the way!', 'Success!');
        return redirect('/home');
    }

    public function mobileHelp(Requests\SystemListRequest $request) {

        $data = $request->all();

        //dd($data);

        Mail::send('email.airwatch', $data, function ($m) use ($data) {

            $m->from('sgmcweb-no-reply@sgmc.org', 'Airwatch Help');
            $m->to('bryan.mcree@sgmc.org','donnie.williams@SGMC.ORG','derrick.clement@SGMC.ORG');
            $m->subject('Airwatch');
            $m->CC('sghelpdesk@sgmc.org');
            //$m->CC($data['mail']);
            $m->replyTo($data['email_address']);
        });

        $file = storage_path('app/public/public/email/airwatch_steps.pdf');
        //$file = Storage::url("app/public/public/email/");

        Mail::send('email.airwatch', $data, function ($m) use ($data, $file) {

            $m->from('sgmcweb-no-reply@sgmc.org', 'Airwatch Help');
            $m->to($data['email_address']);
            $m->subject('Airwatch help is on the way!');
            //$m->CC('sghelpdesk@sgmc.org');
            //$m->CC($data['mail']);
            //$m->replyTo($data['email_address']);
            $m->attach($file);
        });


        alert()->success('Airwatch help on the way!', 'Success!');
        return redirect('/home');
    }
}
