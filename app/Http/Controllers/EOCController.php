<?php

namespace App\Http\Controllers;

use App\EOC;
use App\EOCAnswers;
use App\EOCCategory;
use App\EOCDocs;
use App\EOCQuestions;
use Illuminate\Http\Request;

use App\Http\Requests;

use Webpatser\Uuid\Uuid;
use Image;
use DB;
use App\State;
use App\Role;
use App\Http\Controllers\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use App\CostCenters;
use App\EOCSafety;

class EOCController extends Controller
{


    public function index()
    {
        if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
            // Ignores notices and reports all other kinds... and warnings
            error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
            // error_reporting(E_ALL ^ E_WARNING); // Maybe this is enough
        }


        $categories = EOCCategory::with('EOCQuestions')
            ->get();

        //dd($categories);
        //Pull a current list of accurate cost centers
        $CostCenters = CostCenters::select('cost_center','style1')->orderby('style1')->get();
       // dd($CostCenters);

        $surveys = EOC::orderby('created_at','desc')->get();

        $inprogress = EOCAnswers::select('survey_id')->get();

        

        return view('eoc.index',compact('categories','CostCenters','surveys','inprogress'));
    }

    public function newCategory(Requests\SystemListRequest $request){
        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();
        EOCCategory::create($input);

        alert()->success('Category has been added!', 'Success!');

        return redirect('/eoc');
    }

    public function newQuestion(Requests\SystemListRequest $request){
        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();
        EOCQuestions::create($input);

        alert()->success('Question has been added!', 'Success!');

        return redirect('/eoc');
    }

    public function editQuestion(){
        $question = EOCQuestions::find( Input::get('id') );
        $question->update(Input::all());
        alert()->success('Question Updated', 'Success!');
        return redirect('/eoc');
    }

    public function editCategory(){
        $question = EOCCategory::find( Input::get('id') );
        $question->update(Input::all());
        alert()->success('Category Updated', 'Success!');
        return redirect('/eoc');
    }

    public function deleteQuestion($id)
    {
        $question = EOCQuestions::find( $id );
        $question->delete();
        return redirect('/eoc');
    }

    public function deleteCategory($id)
    {
        $category = EOCCategory::find($id);
        $category->delete();

        $question = EOCQuestions::where('category_id','=', $id );
        $question->delete();
        return redirect('/eoc');
    }

    public function newSurvey(Requests\SystemListRequest $request){

        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->all();
        EOC::create($input);

        alert()->success('Starting Survey!', 'Success!');
        return redirect('/eoc/survey/'. $request->id);
    }

    public function survey($id){

        $survey = EOC::where('id', $id)
            ->first();

        $complete = EOCAnswers::where('survey_id', $id)
            ->get();

        // dd($survey, $complete);

        $categories = EOCCategory::wherenotnull('id')
            ->with('EOCQuestions','eocAnswers')
            ->orderby('category_name')
            ->get();

        //dd($categories);

        return view('eoc.survey',compact('categories','survey','complete'));
    }

    public function surveyCategory($id){

        $cat = EOCCategory::find($id);

        $survey_id = Input::get('survey_id');
        $survey = EOC::find($survey_id);

        $questions = EOCQuestions::where('category_id','=',$id)
            ->get();
        //dd($questions);

        $docs = EOCDocs::where('survey_id',$survey_id)
            ->get();

        return view('eoc.surveycategory',compact('questions','survey','cat','survey','docs'));

    }

    public function submitAnswers(Requests\EOCRequest $request){


        //Create the ID for the answer table
        $request->offsetSet('id', Uuid::generate(4));
        $input = $request->only('id','completed_by','cost_center','shift','practitioner','comments');
        //EOCAnswers::create($input);

        //dd($request);

        //Submit answers
        $arr = Input::get('response');
        //$main_id = $request->id;
        $cost_center = $request->cost_center;
        $completed_by = $request->completed_by;
        //$survey_id = $request->survey_id;
        $survey_id = Input::get('survey_id');
        $question_id = $request->question_id;


        foreach ($arr as $key => $value) {
            $newAnswer = new EOCAnswers();
            if (! is_array( $value )) {
                $newValue = $value['answer'];
            } else {
                $newValue = json_encode($value['answer']);
            }
            //dd($newValue);

            if($newValue =='"Yes"'){
                $newAnswer->yes = 1;
                $newAnswer->no = 0;
                $newAnswer->na = 0;
            }

            if($newValue =='"No"'){
                $newAnswer->yes = 0;
                $newAnswer->no = 1;
                $newAnswer->na = 0;
            }

            if($newValue =='"NA"'){
                $newAnswer->yes = 0;
                $newAnswer->no = 0;
                $newAnswer->na = 1;
            }


            $newAnswer->id = Uuid::generate(4);
            $newAnswer->question_id = $value['question_id'];
            $newAnswer->category_id = $value['category_id'];
            $newAnswer->corrected = $value['corrected'];
            $newAnswer->survey_id = Input::get('survey_id');
            $newAnswer->cost_center = $cost_center;
            $newAnswer->completed_by = $completed_by;
            $newAnswer->notes = $value['notes'];
            $newAnswer->save();

            $answerArray[] = $newAnswer;
        };


        alert()->success('Answers Saved!', 'COMPLETE!');
        return redirect('/eoc/survey/'. $survey_id);

    }

    public function editAnswer(EOCAnswers $answer)
    {
        return view('eoc.editAnswer', compact('answer'));
    }

    public function updateAnswer(Request $request, EOCAnswers $answer)
    {
        if($request->answer_yna == 1)
        {
            $answer->update([
                'yes' => 1,
                'no' => 0,
                'na' => 0,
                'corrected' => $request->corrected,
                'notes' => $request->notes
            ]);
        }
        elseif($request->answer_yna == 0)
        {
            $answer->update([
                'yes' => 0,
                'no' => 1,
                'na' => 0,
                'corrected' => $request->corrected,
                'notes' => $request->notes
            ]);
        }
        else
        {
            $answer->update([
                'yes' => 0,
                'no' => 0,
                'na' => 1,
                'corrected' => $request->corrected,
                'notes' => $request->notes
            ]);
        }

        alert()->success('Answers Updated!', 'Success!');
        return back();
    }


    public function surveyCategoryEdit($id){

        $cat = EOCCategory::find($id);

        $survey_id = Input::get('survey_id');

        $questions = EOCQuestions::where('category_id','=',$id)
            ->get();
        //dd($questions);

        $docs = EOCDocs::where('survey_id',$survey_id)
            ->get();

        $answers = EOCAnswers::with('eocQuestion')
            ->where('survey_id', $survey_id)
            ->where('category_id', $id)
            ->get();


        return view('eoc.surveycategoryedit',compact('questions','cat','survey_id','docs','answers'));

    }

    public function report(EOC $eoc)
    {
        $compliance = EOCAnswers::selectRaw('category_id, SUM(yes) as yes, SUM(no) as no, SUM(yes + no) as total')
            ->where('survey_id', $eoc->id)
            ->groupby('category_id')
            ->get();

        $survey_answers = EOCAnswers::where('survey_id','=', $eoc->id)
            ->get();

        return view('eoc.report',compact('survey_answers', 'eoc', 'compliance'));

    }
}
