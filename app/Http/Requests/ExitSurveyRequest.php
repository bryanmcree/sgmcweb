<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ExitSurveyRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'say_in_what_i_did' => 'required',
            'workload_quality_job' => 'required',
            'proper_resources' => 'required',
            'supervisor_respect' => 'required',
            'project_completed' => 'required',
            'department_worked_well' => 'required',
            'stress_manageable' => 'required',
            'other_departments_pitch' => 'required',
            'opportunities_growth' => 'required',
            'knew_supposed_to_do' => 'required',
            'attitude_employees' => 'required',
            'believe_mission' => 'required',
            'enjoyed_sgmc' => 'required',

            'recommend_care' => 'required',
            'recommend_work' => 'required',


        ];
    }

    public function messages()
    {
        return [
            'say_in_what_i_did.required' => 'Please answer the question: I had a say in the work that I did',

        ];
    }
}
