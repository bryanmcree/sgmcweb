<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\User;

class UserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = User::find($this->users);

        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [

                    'access'        => 'required',
                    'username'      => 'required|unique:users',
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [

                    'access'        => 'required',
                    'username'      => 'required'
                ];
            }
            default:break;
        }
    }
}
