<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ExtraMileRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mtn'    => 'required|unique:extramile',

        ];

    }

    public function messages()
    {
        return [
            'mtn.required' => 'Please enter a Meal Ticket Number',
            'mtn.unique'  => 'That Meal Ticket Number has already been used.',
            
        ];
    }
}
