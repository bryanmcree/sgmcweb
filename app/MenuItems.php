<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MenuItems extends Model
{
    use SoftDeletes;
    protected $table = 'menu_items';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d';

    public function menuDaily()
    {
        return $this->belongsToMany('App\MenuDaily', 'Item_Menu_Pivot', 'menu_item_id', 'menu_daily_id');
    }

    public function menuCategory()
    {
        return $this->hasOne('App\MenuCategory', 'id', 'category');
    }

    public function createdBy()
    {
        return $this->hasOne('App\User', 'employee_number', 'created_by');
    }
}
