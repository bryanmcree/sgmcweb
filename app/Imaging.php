<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Imaging extends Model
{
    use SoftDeletes;
    protected $table = 'imaging_main';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    public function cost_centers(){

        return $this->hasOne('App\ImagingCostCenters', 'cost_center', 'cost_center');
    }
}
