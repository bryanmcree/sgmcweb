<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LaborEfficiencyCalculator extends Model
{
    use SoftDeletes;
    protected $table = 'LE_calculator';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    public function cost_center()
    {
        return $this->hasOne('App\CostCenters', 'id', 'cost_center_id');
    }

    public function createdBy()
    {
        return $this->hasOne('App\User', 'employee_number', 'created_by');
    }

}
