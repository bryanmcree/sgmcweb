<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SGMCPi extends Model
{
    protected $table = 'sgmc_pi';
    protected $primaryKey = null;
    public $incrementing = false;
}
