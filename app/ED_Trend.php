<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ED_Trend extends Model
{
    protected $table = 'ed_adv_disposition_time';
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $dates =['rpt_date'];
}
