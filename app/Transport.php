<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transport extends Model
{
    use SoftDeletes;
    protected $table = 'Transport';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';


    public function requesterInfo()
    {
        return $this->hasOne('App\User', 'employee_number', 'requester');
    }

    public function transporterInfo()
    {
        return $this->hasOne('App\User', 'employee_number', 'tech1');
    }
}
