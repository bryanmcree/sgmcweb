<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Roster extends Model
{
    use SoftDeletes;
    protected $table = 'roster';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    public function createdBy()
    {
        return $this->hasOne('App\User', 'id', 'created_by');
    }

    public function attendants()
    {
        return $this->hasmany('App\RosterAttendants', 'roster_id', 'id');
    }

}
