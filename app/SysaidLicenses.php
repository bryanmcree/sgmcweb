<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SysaidLicenses extends Model
{
    use SoftDeletes;
    protected $table = 'sysaid_licenses';
    protected $guarded = [];
    //public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $dates = ['created_at'];
}
