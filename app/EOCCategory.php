<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EOCCategory extends Model
{
    use SoftDeletes;
    protected $table = 'EOC_category';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    //Links the LemName back to the correct pillars
    public function eocQuestions()
    {
        return $this->hasMany('App\EOCQuestions', 'category_id', 'id')->orderBy('created_at');
    }

    public function eocAnswers()
    {
        return $this->hasMany('App\EOCAnswers', 'category_id', 'id');
    }
}
