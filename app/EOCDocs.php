<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EOCDocs extends Model
{
    use SoftDeletes;
    protected $table = 'EOC_docs';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    public function addedBy()
    {
        return $this->hasOne('App\User', 'employee_number', 'added_by');
    }
}

