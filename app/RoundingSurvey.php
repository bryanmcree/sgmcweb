<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoundingSurvey extends Model
{
    protected $table = 'rounding_surveys';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    public function surveyQuestion()
    {
        return $this->hasMany('App\RoundingQuestion', 'survey_id')->orderBy('question_type','DESC');
        //return $this->hasMany('App\SystemsContacts', 'systems_id')->orderBy('Pri');
    }

    public function surveyAnswers()
    {
        return $this->hasMany('App\RoundingAnswer', 'survey_id', 'id');
        //return $this->hasMany('App\SystemsContacts', 'systems_id')->orderBy('Pri');
    }

    public function completedSurveys()
    {
        return $this->hasMany('App\RoundingAnswer', 'survey_id', 'id');
        //return $this->hasMany('App\SystemsContacts', 'systems_id')->orderBy('Pri');
    }
}
