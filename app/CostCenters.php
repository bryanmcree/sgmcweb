<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CostCenters extends Model
{
    protected $table = 'CostCenters_new';
    protected $guarded = [];

    public function uosName()
    {
        return $this->hasOne('App\UnitService', 'id', 'flexed');
    }

    public function employees()
    {
        return $this->hasMany('App\User', 'unit_code', 'cost_center')->where('users.emp_status','Active')->orderby('users.last_name');
    }

    public function crRequest()
    {
        return $this->hasMany('App\CapitalRequest', 'cost_center', 'cost_center');
    }

    public function csuiteEmployee()
    {
        return $this->hasOne('App\User', 'employee_number', 'csuite');
    }

    public function adminEmployee()
    {
        return $this->hasOne('App\User', 'employee_number', 'administrator');
    }

    public function directorEmployee()
    {
        return $this->hasOne('App\User', 'employee_number', 'director');
    }

    public function managerEmployee()
    {
        return $this->hasOne('App\User', 'employee_number', 'manager');
    }

    public function analystEmployee()
    {
        return $this->hasOne('App\User', 'employee_number', 'analyst');
    }

    public function pro_click()
    {
        return $this->hasOne('App\Proclick', 'cost_center', 'cost_center');
    }

    public function pro_clickRevenue()
    {
        return $this->hasOne('App\ProclickRevenue', 'cost_center', 'cost_center');
    }

    public function sgmc_pi()
    {
        return $this->hasOne('App\SGMCPi', 'cost_center', 'cost_center');
    }

}
