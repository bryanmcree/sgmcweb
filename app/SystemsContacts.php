<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Auth;
use Carbon\Carbon;

class SystemsContacts extends Model
{
    protected $table = 'systems_contacts';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';
    //public $primaryKey  = 'id';
    //protected $dates = ['deleted_at', 'updated_at', 'created_at'];

    public function getDates(){
        return array('created_at','updated_at','deleted_at');
    }

    public function systemsList()
    {
        return $this->belongsTo('App\SystemList', 'systems_id', 'id');
    }

    public function getCreatedAtAttribute($date)
    {
        if(Auth::check())
            return Carbon::createFromFormat('Y-m-d H:i:s', $date)->copy()->tz(Auth::user()->timezone)->format('F j, Y @ g:i A');
        else
            return Carbon::createFromFormat('Y-m-d H:i:s', $date)->copy()->tz('America/Toronto')->format('F j, Y @ g:i A');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('F j, Y @ g:i A');
    }
}
