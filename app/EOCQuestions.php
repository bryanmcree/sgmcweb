<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EOCQuestions extends Model
{
    use SoftDeletes;
    protected $table = 'EOC_questions';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    public function eocAnswers()
    {
        return $this->hasMany('App\EOCAnswers', 'question_id', 'id');
    }
}
