<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExitInterview extends Model
{
    use SoftDeletes;
    protected $table = 'employee_exit';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    public function employee()
    {
        return $this->hasOne('App\User', 'employee_number', 'employee_number');
    }
}
