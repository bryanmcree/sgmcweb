<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmpTrend extends Model
{
    protected $table = 'emp_stat_trend';
    protected $dateFormat = 'm-d-Y';
}
