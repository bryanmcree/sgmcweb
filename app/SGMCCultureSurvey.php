<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SGMCCultureSurvey extends Model
{
    protected $table = 'SGMCCultureSurvey';
    protected $guarded = [];
    use SoftDeletes;
}
