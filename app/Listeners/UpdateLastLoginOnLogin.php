<?php
namespace App\Listeners;

use App\User;
use Carbon\Carbon;
use Auth;
use Request;
Use Webpatser\Uuid;
use App\History;

class UpdateLastLoginOnLogin
{
public function login($event)
{
            $user = Auth::user();
            $user->last_login_ip = Request::getClientIp();
            $user->last_login = Carbon::now();
            $user->save();
    //Update History File
            $history = new History;
            $history->id = \Uuid::generate(4);
            $history->action = 'User Login';
            $history->userid = \Auth::user()->username;
            $history->search_string = 'User has logged into web.sgmc.org';
            $history->user_ip = \Request::ip();
            $history->save();
}

    public function logout ($event)
    {
        //Update History File
        $history = new History;
        $history->id = \Uuid::generate(4);
        $history->action = 'User Logout';
        $history->userid = \Auth::user()->username;
        $history->search_string = 'User has logged out of web.sgmc.org';
        $history->user_ip = \Request::ip();
        $history->save();
    }

    public function searchEmployee($query)
    {
      //  $history = new History;
      //  $history->id = \Uuid::generate(4);
      //  $history->action = 'Employee Search';
     //   $history->userid = \Auth::user()->username;
      //  $history->search_string = $query;
      //  $history->user_ip = \Request::ip();
      //  $history->save();

    }
}