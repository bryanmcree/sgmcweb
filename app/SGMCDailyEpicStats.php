<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SGMCDailyEpicStats extends Model
{

    protected $table = 'SGMC_Daily_Epic_Stats';
    protected $dateFormat = 'Y-m-d H:i:s';

}
