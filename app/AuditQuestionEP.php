<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditQuestionEP extends Model
{
    protected $table = 'audit_question_EP';
    protected $guarded = [];
    public $timestamps = FALSE;
    public $incrementing = FALSE;
}
