<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Right extends Model
{
    protected $table = 'permission_role';
    public $timestamps = false;
    protected $guarded = [];
    public $incrementing = false;

    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }

    public function givePermissionTo(Permission $permission)
    {
        return $this->permissions()->save($permission);
    }

    public function permissionName ()
    {
        return $this->belongsTo('App\Permission', 'permission_id', 'id')->orderBy('name');
    }
}
