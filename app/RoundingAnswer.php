<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoundingAnswer extends Model
{
    protected $table = 'rounding_answers';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    public function surveyQuestion()
    {
        return $this->hasMany('App\RoundingQuestion', 'id', 'question_id');
        //return $this->hasMany('App\SystemsContacts', 'systems_id')->orderBy('Pri');
    }

    public function surveys()
    {
        return $this->hasOne('App\RoundingSurvey', 'id', 'survey_id');
        //return $this->hasMany('App\SystemsContacts', 'systems_id')->orderBy('Pri');
    }
}
