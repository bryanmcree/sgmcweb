<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProclickBudget extends Model
{
    protected $table = 'pro_click_budget';
}
