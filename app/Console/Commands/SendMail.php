<?php

namespace App\Console\Commands;

use App\HandwashingCostCenters;
use App\ImagingCostCenters;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SendMail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:mail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command checks for daily email notifications then sends them.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //EMAIL for immaging
        $imaging = ImagingCostCenters::select('mail')
            ->join('users','users.employee_number','=','imaging_cost_centers.employee_number')
            ->distinct()
            ->get()->toArray();

        $myArray = array();
        foreach ($imaging as $key => $value) {
            $myArray[] = $value['mail'];
        }

        //for Testing
        //$myArray=['bryan.mcree@sgmc.org','bryanmcree@gmail.com'];


        Mail::send('imaging.emails.reminder', [], function($message) use ($myArray)
        {
            $message->to($myArray)->subject('Friendly Reminder');
            $message->CC('bryan.mcree@sgmc.org','kenny.jacobs@sgmc.org');
            $message->From('no-reply@sgmc.org');
        });


        //EMAIL for handwashing
        $handwashing = HandwashingCostCenters::select('mail')
            ->join('users','users.employee_number','=','handwashing_cost_centers.employee_number')
            ->distinct()
            ->get()->toArray();

        $myArray2 = array();
        foreach ($handwashing as $key => $value) {
            $myArray2[] = $value['mail'];
        }

        //for Testing
        //$myArray2=['bryan.mcree@sgmc.org','scarlett.rivera@sgmc.org'];


        Mail::send('quality.infection.emails.reminder', [], function($message) use ($myArray2)
        {
            $message->to($myArray2)->subject('Friendly Reminder');
            $message->CC('bryan.mcree@sgmc.org','scarlett.rivera@sgmc.org');
            $message->From('no-reply@sgmc.org');
        });






    }
}
