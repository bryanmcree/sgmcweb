<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RoleUser extends Model
{
    // use SoftDeletes;
    public $timestamps = false;
    protected $table = 'users_roles';

    public function dialysisRole()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

}
