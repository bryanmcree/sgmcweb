<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NursingCompChecklist extends Model
{
    use SoftDeletes;
    protected $table = 'nursing_comp_checklist';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    public function compTitle()
    {
        return $this->hasOne('App\NursingCompetency', 'id', 'comp_id');
    }

    public function questionTitle()
    {
        return $this->hasOne('App\NursingCompQuestion', 'id', 'question_id');
    }

    public function nurseName()
    {
        return $this->hasOne('App\User', 'id', 'nurse_id');
    }
}
