<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Servers extends Model
{
    use SoftDeletes;
    protected $table = 'servers';
    protected $guarded = [];
    public $incrementing = false;
    public $primaryKey  = 'srv_id';
    //Needed to format SQL server dates
    protected $dateFormat = 'Y-m-d H:i:s';
    
    
}
