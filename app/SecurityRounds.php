<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SecurityRounds extends Model
{
    use SoftDeletes;
    protected $table = 'security_rounds';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';
}
