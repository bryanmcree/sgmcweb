<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PublicRelationsPatients extends Model
{
    protected $table = 'patient_relations_patients_table';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $dates = ['dob','admission'];

    public function PartOne()
    {
        return $this->hasOne('App\PRCheck', 'encounter_id', 'pat_enc_csn_id');
    }

    public function PartTwo()
    {
        return $this->hasOne('App\PRTwo', 'encounter_id', 'pat_enc_csn_id');
    }
}
