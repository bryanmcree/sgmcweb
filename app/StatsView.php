<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatsView extends Model
{
    protected $table = 'stats_view';
}
