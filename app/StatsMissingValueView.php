<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatsMissingValueView extends Model
{
    protected $table = 'stats_missing_value';

    public function statsValues()
    {
        return $this->hasMany('App\StatsValues', 'stat_link', 'stat_link')->orderby('start_date');
    }
}
