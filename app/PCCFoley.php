<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PCCFoley extends Model
{
    protected $table = 'pcc_foley';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';
}
