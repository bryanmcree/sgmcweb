<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PCCCentralLine extends Model
{
    protected $table = 'pcc_central_line';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';
}
