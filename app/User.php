<?php

namespace App;


use Illuminate\Foundation\Auth\User as Authenticatable;
use Adldap\Laravel\Traits\AdldapUserModelTrait;
use Carbon\Carbon;
use \Auth;
use app\State;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Role;

class User extends Authenticatable
{
    use SoftDeletes;
    use AdldapUserModelTrait;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     *
     */
    //public $timestamps = false;
    public $incrementing = true;
    protected $guarded = [];
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $dates = ['dob','hire_date'];
    //public $timestamps = false;




    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles(){
        return $this->belongsToMany('\App\Role','users_roles');
    }

    public function hasRole($check){
        return in_array($check, array_pluck($this->roles->toArray(),'name'));
    }

    public function states () {
        return $this->belongsTo('App\State','state', 'id');
        //return $this->hasOne('App\State', 'id');
    }
    


    public function isEmployee()
    {
        //check and see if the user is authorized and find out what roles they have.
        return ($this->roles()->count()) ? true : false ;
    }


    private function getIdInArray($array, $term)
    {
         foreach ($array as $key => $value) {
             if ($value == $term) {
                 return $key;
             }
         }
         throw new UnexpectedValueException;
    }

    public function competencies()
    {
        return $this->belongsToMany('App\NursingCompetency', 'nurse_competencies', 'nurse_id', 'comp_id');
    }

}
