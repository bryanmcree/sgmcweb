<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Palliative extends Model
{
    use SoftDeletes;
    protected $table = 'PalliativeCare';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $dates = ['date_of_birth','date_of_discharge','date_of_consult','date_of_admission'];

    public function createdBy()
    {
        return $this->hasOne('App\User', 'employee_number', 'created_by');
    }

    public function visits()
    {
        return $this->hasMany('App\PalliativeVisits', 'patient_id', 'id');
    }

}
