<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proclick extends Model
{
    protected $table = 'proclick';
    protected $primaryKey = null;
    public $incrementing = false;
}
