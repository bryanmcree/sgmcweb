<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use \Auth;
use App\User;

class Role extends Model
{
   // protected $table = 'roles';
    public $timestamps = false;
    protected $guarded = [];
    public $incrementing = false;

    public function users(){
        return $this->belongsToMany('User', 'users_roles');
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }

    public function givePermissionTo(Permission $permission)
    {
        return $this->permissions()->save($permission);
    }

    
}
