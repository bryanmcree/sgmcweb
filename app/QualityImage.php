<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QualityImage extends Model
{
    protected $table = 'quality_control_cost_centers';

    public function perMonth()
    {
        return $this->hasMany('App\Quality', 'cost_center', 'cost_center');
    }
}


