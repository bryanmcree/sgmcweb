<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Termination extends Model
{
    protected $table = 'Terminations';
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $guarded = [];
    use SoftDeletes;
}
