<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TimeOffEmails extends Model
{
    protected $table = 'time_off_emails';
    protected $guarded = [];
    public $incrementing = true;
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $dates = [];
}
