<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Quality extends Model
{
    use SoftDeletes;
    protected $table = 'quality_imaging';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    public function completedBy()
    {
        return $this->hasOne('App\User', 'id', 'completed_by');
    }

    public function perMonth()
    {
        return $this->hasOne('App\QualityImage', 'cost_center', 'cost_center');
    }
}
