<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SGMCCensus extends Model
{
    protected $table = 'SGMC_census';
    protected $dateFormat = 'Y-m-d H:i:s';
}
