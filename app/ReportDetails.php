<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReportDetails extends Model
{
    use SoftDeletes;
    protected $table = 'report_details';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';


    public function managerInfo()
    {
        return $this->hasOne('App\User', 'employee_number', 'requestor_sup');
    }

    public function requesterInfo()
    {
        return $this->hasOne('App\User', 'employee_number', 'requested_by');
    }

    public function reportName()
    {
        return $this->hasOne('App\ReportName', 'id', 'report_id');
    }
}
