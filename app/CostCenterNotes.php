<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CostCenterNotes extends Model
{
    protected $table = 'CostCenter_notes';
    protected $guarded = [];
    use SoftDeletes;
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    public function createdBy()
    {
        return $this->hasOne('App\User', 'employee_number', 'created_by');
    }
}
