<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LemName extends Model
{
    use SoftDeletes;
    protected $table = 'lem_names';
    protected $guarded = [];
    public $incrementing = false;
    protected $dateFormat = 'Y-m-d H:i:s';

    //Links the User table to the ID number of the created by ID
    public function createdBy()
    {
        return $this->hasOne('App\User', 'id', 'created_by');
    }

    //Links the LemName back to the correct pillars
    public function lemPillars()
    {
        return $this->hasMany('App\LemPillar', 'lem_id', 'id')->orderBy('pillar_name');
    }
    

}
