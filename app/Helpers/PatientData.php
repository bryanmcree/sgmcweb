<?php

/**
 * Created by PhpStorm.
 * User: bmcree
 * Date: 6/19/2017
 * Time: 8:20 AM
 */
class PatientData
{
 public static function scrambledata($patient_data)
    {
        //$dataLen = strlen($patient_data)
        $patient_data = str_repeat('*', strlen($patient_data) - 3) . substr($patient_data, - 3);

        return $patient_data;
    }
}