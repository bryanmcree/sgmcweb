<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SystemList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('systemlist', function (Blueprint $table) {
            //ID - Uses UUid to prevent duplicate ID's
            $table->string('id', 36)->primary();

            //Stuff
            $table->string('system_name');
            $table->string('system_description');
            $table->string('system_admin1');
            $table->string('system_admin1_email');
            $table->string('system_admin2')->nullable();
            $table->string('system_admin2_email')->nullable();
            $table->string('dept_admin1')->nullable();
            $table->string('dept_admin1_email')->nullable();
            $table->string('system_priority');

            //Laravel Stuff
            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('systemlist');
    }
}
