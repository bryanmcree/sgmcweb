<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            //ID - Uses UUid to prevent duplicate ID's
            //$table->string('id', 36)->primary();
            //Active Directory Requres Auto ID's
            $table->increments('id', 36)->primary();

            //Name Stuff
            $table->string('first_name');
            $table->string('middle_name')->nullable();
            $table->string('last_name');

            //Phone Stuff
            $table->string('mobile_phone')->nullable();
            $table->string('work_phone')->nullable();
            $table->string('fax_phone')->nullable();

            //Address Stuff
            $table->string('address')->nullable();
            $table->string('address2')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('zip')->nullable();

            //Security Fields (active or inactive)
            $table->string('access')->nullable();

            //Optional Fields
            $table->string('title')->nullable();
            $table->string('company')->nullable();
            $table->string('managermail');
            $table->string('password');

            //Active Directory Stuff
            $table->string('mail');
            $table->string('name');
            $table->string('username');
            $table->dateTime('last_login');
            $table->string('last_login_ip');
            

            //Laravel Stuff
            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
