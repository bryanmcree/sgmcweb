<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExtraMile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('extramile', function (Blueprint $table) {
            //ID - Uses UUid to prevent duplicate ID's
            $table->string('id', 36)->primary();

            //Name Stuff
            $table->string('presented_to_first_name');
            $table->string('presented_to_middle_name')->nullable();
            $table->string('presented_to_last_name');
            $table->string('presented_to_employee_number')->nullable();
            $table->string('presented_to_department_number')->nullable();
            $table->string('presented_to_campus')->nullable();
            $table->string('presented_to_email')->nullable();
            
            //Authorized By
            $table->string('authorized_by_first_name');
            $table->string('authorized_by_middle_name')->nullable();
            $table->string('authorized_by_last_name');
            $table->string('authorized_by_employee_number')->nullable();
            $table->string('authorized_by_email');

            //Ticket Number
            $table->string('ticket_number')->nullable();
            $table->string('reason')->nullable();
            

            //Laravel Stuff
            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('extramile');
    }
}
