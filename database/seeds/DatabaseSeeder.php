<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //We need to add a super user so that they can create new users

        DB::table('users')->insert([
            //ID - Uses UUid to prevent duplicate ID's
            'id' => Uuid::generate(4),

            //Name Stuff
            'first_name' => 'Bryan',
            'middle_name' => 'Earl',
            'last_name' => 'McRee',

            //Phone Stuff
            'mobile_phone' => '2294124213',
            'work_phone' => '2293331145',
            'fax_phone' => '2293331139',

            //Address Stuff
            'address' => '123 South Main Street',
            'address2' => 'Suite 401',
            'city' => 'Valdosta',
            'state' => 'GA',
            'zip' => '31602',

            //Security Fields (active or inactive)
            'access' => 'Active',

            'title' => 'SQL Programmer / Developer',
            'company' => 'South Georgia Medical Center',
            'email' => 'bryan.mcree@sgmc.org',
            'password' => bcrypt('timeflash'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);



        $faker = Faker::create();
        foreach (range(1,100) as $index) {
            DB::table('users')->insert([
                //ID - Uses UUid to prevent duplicate ID's
                'id' => Uuid::generate(4),

                //Name Stuff
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,

                //Address Stuff
                'address' => $faker->streetAddress,
                'address2' => 'Suite 401',
                'city' => $faker->city,
                'state' => 'GA',
                'zip' => '31602',

                //Phone Stuff
                'mobile_phone' => '2293335151',
                'fax_phone' => '2293335151',
                'work_phone' => '2293335151',

                //Security Fields (active or inactive)
                'access' => 'Active',

                'title' => $faker->jobTitle,
                'company' => $faker->company,
                'email' => $faker->email,
                'password' => bcrypt('secret'),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }




        //This is where we insert all the states into the states table to be used later.

        DB::table('states')->insert(
            array(
                array('name' => 'Alabama', 'code' => 'AL'),
                array('name' => 'Alaska', 'code' => 'AK'),
                array('name' => 'Arizona', 'code' => 'AZ'),
                array('name' => 'Arkansas', 'code' => 'AR'),
                array('name' => 'California', 'code' => 'CA'),
                array('name' => 'Colorado', 'code' => 'CO'),
                array('name' => 'Connecticut', 'code' => 'CT'),
                array('name' => 'Delaware', 'code' => 'DE'),
                array('name' => 'District of Columbia', 'code' => 'DC'),
                array('name' => 'Florida', 'code' => 'FL'),
                array('name' => 'Georgia', 'code' => 'GA'),
                array('name' => 'Hawaii', 'code' => 'HI'),
                array('name' => 'Idaho', 'code' => 'ID'),
                array('name' => 'Illinois', 'code' => 'IL'),
                array('name' => 'Indiana', 'code' => 'IN'),
                array('name' => 'Iowa', 'code' => 'IA'),
                array('name' => 'Kansas', 'code' => 'KS'),
                array('name' => 'Kentucky', 'code' => 'KY'),
                array('name' => 'Louisiana', 'code' => 'LA'),
                array('name' => 'Maine', 'code' => 'ME'),
                array('name' => 'Maryland', 'code' => 'MD'),
                array('name' => 'Massachusetts', 'code' => 'MA'),
                array('name' => 'Michigan', 'code' => 'MI'),
                array('name' => 'Minnesota', 'code' => 'MN'),
                array('name' => 'Mississippi', 'code' => 'MS'),
                array('name' => 'Missouri', 'code' => 'MO'),
                array('name' => 'Montana', 'code' => 'MT'),
                array('name' => 'Nebraska', 'code' => 'NE'),
                array('name' => 'Nevada', 'code' => 'NV'),
                array('name' => 'New Hampshire', 'code' => 'NH'),
                array('name' => 'New Jersey', 'code' => 'NJ'),
                array('name' => 'New Mexico', 'code' => 'NM'),
                array('name' => 'New York', 'code' => 'NY'),
                array('name' => 'North Carolina', 'code' => 'NC'),
                array('name' => 'North Dakota', 'code' => 'ND'),
                array('name' => 'Ohio', 'code' => 'OH'),
                array('name' => 'Oklahoma', 'code' => 'OK'),
                array('name' => 'Oregon', 'code' => 'OR'),
                array('name' => 'Pennsylvania', 'code' => 'PA'),
                array('name' => 'Rhode Island', 'code' => 'RI'),
                array('name' => 'South Carolina', 'code' => 'SC'),
                array('name' => 'South Dakota', 'code' => 'SD'),
                array('name' => 'Tennessee', 'code' => 'TN'),
                array('name' => 'Texas', 'code' => 'TX'),
                array('name' => 'Utah', 'code' => 'UT'),
                array('name' => 'Vermont', 'code' => 'VT'),
                array('name' => 'Virginia', 'code' => 'VA'),
                array('name' => 'Washington', 'code' => 'WA'),
                array('name' => 'West Virginia', 'code' => 'WV'),
                array('name' => 'Wisconsin', 'code' => 'WI'),
                array('name' => 'Wyoming', 'code' => 'WY')
            ));
    }
}
