{{--New file Template--}}

{{--Add Security for this page below--}}


    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <h3><b>Action OI</b></h3>
            </div>
            <div class="panel-body">
                <div class="col-lg-4">
                    <div class="well">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <td><b>Date</b></td>
                                    <td align="center"><b>Start Time</b></td>
                                    <td align="center"><b>End Time</b></td>
                                    <td align="center"><b>Duration</b></td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($ava_dates as $ava_date)
                                <tr>
                                    <td>{{$ava_date->meeting_date->format('m-d-Y')}} - {{$ava_date->meeting_date->format('l')}}</td>
                                    <td align="center">{{$ava_date->start_time->format('H:i')}}</td>
                                    <td align="center">{{$ava_date->end_time->format('H:i')}}</td>
                                    <td align="center">{{\Carbon\Carbon::createFromTimeStamp(strtotime($ava_date->start_time))->diffInMinutes(\Carbon\Carbon::createFromTimeStamp(strtotime($ava_date->end_time)))}}</td>
                                    <td>
                                        @if($ava_date->blocked_out ==1)
                                                <input type="submit" value="Not Available" class="btn btn-default btn-sm disabled btn-block">
                                        @elseif(is_null($ava_date->scheduled))
                                            <input type="submit" value="Available" class="btn btn-success btn-sm  btn-block" data-toggle="modal" data-target="#signUp">
                                        @elseif(!is_null($ava_date->scheduled))
                                            @if($ava_date->scheduled == Auth::user()->employee_number)
                                                <input type="submit" value="{{Auth::user()->name}}" class="btn btn-info btn-sm btn-block" data-toggle="modal" data-target="#signDown">
                                            @else
                                                <input type="submit" value="Not Available" class="btn btn-danger disabled btn-sm btn-block">
                                            @endif
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include("schedule.modals.signup")
@include("schedule.modals.signdown")
@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
