{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Web Request Forms') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-lg-12">
        <div class="alert alert-info"><h2>Request System Access</h2></div>
    </div>


    <div class="col-lg-4 col-lg-offset-1">
        <p><h3 align="center">New or current employees who need access to various computer systems.</h3></p>
        <a href="/access/employee" class="btn btn-lg btn-primary btn-block">Employee</a>
    </div>

    <div class="col-lg-2">

    </div>

    <div class="col-lg-4">
        <p><h3 align="center">Physician or their office staff who need access to various SGMC computer systems.</h3></p>
        <a href="/access/physician" class="btn btn-lg btn-default btn-block">Physician or Staff</a>
    </div>


@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
@endif