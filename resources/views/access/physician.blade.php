{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Web Request Forms') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <b><h3>Physicians Dashboard</h3></b>
            </div>
            <div class="panel-body">
                <ul class="nav nav-pills">
                    <li class="active"><a data-toggle="pill" href="#home">Previous Request</a></li>
                    <li><a data-toggle="pill" href="#menu1">New Physicians Request</a></li>
                </ul>

                <div class="tab-content">
                    <div id="home" class="tab-pane fade in active">
                        <h3>Previous Request</h3>
                        <p>This is where you can view the previous request.</p>
                    </div>
                    <div id="menu1" class="tab-pane fade in">
                        <h3>Request Access for New Physican</h3>
                        <form action="/action_page.php">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <b>Physican's Demographics</b>
                                        Julie this is where we will collect information about the DR.  License stuff comes later.
                                    </div>
                                    <div class="panel-body">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="email">First Name:</label>
                                                <input type="text" name="first_name" class="form-control" id="first_name" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="phone_number">Phone Number:</label>
                                                <input type="text" class="form-control" id="phone_number">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="last_name">Last Name:</label>
                                                <input type="text" name="last_name" class="form-control" id="last_name">
                                            </div>
                                            <div class="form-group">
                                                <label for="pwd">Outside Email:</label>
                                                <input type="email" class="form-control" id="pwd">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <b>Physican's Office Details</b>
                                        Julie here we can collect information about the practice.
                                    </div>
                                    <div class="panel-body">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="practice_name">Practice Name:</label>
                                                <input type="text" name="practice_name" class="form-control" id="practice_name" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="practice_address">Address:</label>
                                                <input type="text" name="practice_address" class="form-control" id="practice_address">
                                            </div>
                                            <div class="form-group">
                                                <label for="practice_city">City:</label>
                                                <input type="text" name="practice_city" value="Valdosta" class="form-control" id="practice_city">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="practice_phone">Practice Phone:</label>
                                                <input type="text" name="practice_phone" class="form-control" id="practice_phone" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="pwpractice_faxd">Practice Fax:</label>
                                                <input type="text" name="practice_fax" class="form-control" id="practice_fax">
                                            </div>
                                            <div class="form-group">
                                                <label for="practice_state">State:</label>
                                                <input type="text" name="practice_state" value="GA" class="form-control" id="practice_state">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <b>Physican's Credentials</b>
                                        Julie this is where we will collect the information about specialty, and all "NUMBERS"
                                    </div>
                                    <div class="panel-body">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="specialty_primary">Specialty Primary:</label>
                                                <input type="text" name="specialty_primary" class="form-control" id="specialty_primary" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="physican_number">Doctor Number:</label>
                                                <input type="text" name="physican_number" class="form-control" id="physican_number" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="phone_number">DEA Number:</label>
                                                <input type="text" class="form-control" id="phone_number">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="specialty_secondary">Specialty Secondary:</label>
                                                <input type="text" name="specialty_secondary" class="form-control" id="specialty_secondary" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="npi_number">NPI Number:</label>
                                                <input type="text" name="npi_number" class="form-control" id="npi_number" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <b>Hospital Systems</b>
                                        <div class="panel-sub-title"><i>Select systems this user will need access to, keep in mind some systems require justification or the purchase of a license.</i></div>
                                    </div>
                                    <div class="panel-body">
                                        <div class="col-sm-6">
                                            <table class="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <td></td>
                                                        <td><b>System Name</b></td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td><input type="checkbox" name="epic" class="form-control" id="epic"></td>
                                                        <td>EPIC</td>
                                                    </tr>
                                                    <tr>
                                                        <td><input type="checkbox" name="epic" class="form-control" id="epic"></td>
                                                        <td>OneContent</td>
                                                    </tr>
                                                    <tr>
                                                        <td><input type="checkbox" name="epic" class="form-control" id="epic"></td>
                                                        <td>OBQS GE Centricity Perinatal</td>
                                                    </tr>
                                                    <tr>
                                                        <td><input type="checkbox" name="epic" class="form-control" id="epic"></td>
                                                        <td>ProvationMD</td>
                                                    </tr>
                                                    <tr>
                                                        <td><input type="checkbox" name="epic" class="form-control" id="epic"></td>
                                                        <td>Remote Access <i>(Requires justification in notes below)</i></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-sm-6">
                                            <table class="table table-hover">
                                                <thead>
                                                <tr>
                                                    <td></td>
                                                    <td><b>System Name</b></td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td><input type="checkbox" name="epic" class="form-control" id="epic"></td>
                                                    <td>Dragon Medical One <i>(Requires Approval)</i></td>
                                                </tr>
                                                <tr>
                                                    <td><input type="checkbox" name="epic" class="form-control" id="epic"></td>
                                                    <td>McKesson Cardiology </td>
                                                </tr>
                                                <tr>
                                                    <td><input type="checkbox" name="epic" class="form-control" id="epic"></td>
                                                    <td>Theradoc</td>
                                                </tr>
                                                <tr>
                                                    <td><input type="checkbox" name="epic" class="form-control" id="epic"></td>
                                                    <td>MediLinks</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
@endif