{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Web Request Forms') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-8 col-lg-offset-2">
        <div class="panel panel-default">
            <div class="panel panel-heading panel-heading-custom">
                <h4>Employee Information</h4>
                <div class="panel-sub-title">
                    Indicates required fields
                </div>
            </div>
            <div class="panel-body">
                <form>
                    <div class="form-group">
                        <label for="exampleInputEmail1">SGMC User Name:</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter User Name if Known">
                    </div>
                    <div class="form-group">
                        <label for="employee_number">SGMC Employee Number:</label>
                        <small id="emailHelp" class="form-text text-muted">Employee number is required for access on some systems.</small>
                        <input type="text" class="form-control" id="employee_number" aria-describedby="emailHelp" placeholder="Employee Number" required>
                    </div>
                    <div class="form-group">
                        <label for="first_name">First Name:</label>
                        <div class="input-group">
                            <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                            <input type="text" class="form-control" name="first_name" id="first_name" aria-describedby="emailHelp" placeholder="First Name" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="middle_name">Middle Name:</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Middle Name">
                    </div>
                    <div class="form-group">
                        <label for="last_name">Last Name:</label>
                        <div class="input-group">
                            <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                            <input type="text" class="form-control" name="first_name" id="first_name" aria-describedby="emailHelp" placeholder="Last Name" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone:</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Phone">
                    </div>
                    <div class="form-group">
                        <label for="phone">Cost Center:</label>
                        <div class="input-group">
                            <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                        <select class="form-control">
                            <option>[Select Cost Center]</option>
                            @foreach($cost_centers as $cc)
                                <option>{{$cc->style1}}</option>
                            @endforeach
                            <option>Cost Center NOT Listed</option>
                        </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="phone">Service Level:</label>
                        <div class="input-group">
                            <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                            <select class="form-control">
                                <option>[Select Service Level]</option>

                                <option>Employee</option>
                                <option>Manager</option>
                                <option>Director</option>
                                <option>Assistant Administrator</option>
                                <option>Chief</option>

                            </select>
                        </div>
                    </div>


                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>

    <div class="col-md-8 col-lg-offset-2">
        <div class="panel panel-default">
            <div class="panel panel-heading panel-heading-custom">
                <h4>System Selection</h4>
                <div class="panel-sub-title">
                    Select all systems this employee will need to have access to.
                </div>
            </div>
            <div class="panel-body">

                <ul class="list-group checked-list-box">
                    <li class="list-group-item">Cras justo odio</li>
                    <li class="list-group-item">Dapibus ac facilisis in</li>
                    <li class="list-group-item">Morbi leo risus</li>
                    <li class="list-group-item">Porta ac consectetur ac</li>
                    <li class="list-group-item">Vestibulum at eros</li>
                    <li class="list-group-item">Cras justo odio</li>
                    <li class="list-group-item">Dapibus ac facilisis in</li>
                    <li class="list-group-item">Morbi leo risus</li>
                    <li class="list-group-item">Porta ac consectetur ac</li>
                    <li class="list-group-item">Vestibulum at eros</li>
                </ul>

            </div>
        </div>
    </div>


    <div class="col-md-8 col-lg-offset-2">
        <div class="panel panel-default">
            <div class="panel panel-heading panel-heading-custom">
                <h4>File Shares and Email Groups</h4>
                <div class="panel-sub-title">
                    Below you can request the user to be added to W drive directories or email groups.
                </div>
            </div>
            <div class="panel-body">



            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">
    $(function () {
        $('.list-group.checked-list-box .list-group-item').each(function () {

            // Settings
            var $widget = $(this),
                $checkbox = $('<input type="checkbox" class="hidden" />'),
                color = ($widget.data('color') ? $widget.data('color') : "primary"),
                style = ($widget.data('style') == "button" ? "btn-" : "list-group-item-"),
                settings = {
                    on: {
                        icon: 'glyphicon glyphicon-check'
                    },
                    off: {
                        icon: 'glyphicon glyphicon-unchecked'
                    }
                };

            $widget.css('cursor', 'pointer')
            $widget.append($checkbox);

            // Event Handlers
            $widget.on('click', function () {
                $checkbox.prop('checked', !$checkbox.is(':checked'));
                $checkbox.triggerHandler('change');
                updateDisplay();
            });
            $checkbox.on('change', function () {
                updateDisplay();
            });


            // Actions
            function updateDisplay() {
                var isChecked = $checkbox.is(':checked');

                // Set the button's state
                $widget.data('state', (isChecked) ? "on" : "off");

                // Set the button's icon
                $widget.find('.state-icon')
                    .removeClass()
                    .addClass('state-icon ' + settings[$widget.data('state')].icon);

                // Update the button's color
                if (isChecked) {
                    $widget.addClass(style + color + ' active');
                } else {
                    $widget.removeClass(style + color + ' active');
                }
            }

            // Initialization
            function init() {

                if ($widget.data('checked') == true) {
                    $checkbox.prop('checked', !$checkbox.is(':checked'));
                }

                updateDisplay();

                // Inject the icon if applicable
                if ($widget.find('.state-icon').length == 0) {
                    $widget.prepend('<span class="state-icon ' + settings[$widget.data('state')].icon + '"></span>');
                }
            }
            init();
        });

        $('#get-checked-data').on('click', function(event) {
            event.preventDefault();
            var checkedItems = {}, counter = 0;
            $("#check-list-box li.active").each(function(idx, li) {
                checkedItems[counter] = $(li).text();
                counter++;
            });
            $('#display-json').html(JSON.stringify(checkedItems, null, '\t'));
        });
    });
</script>

@endsection
@endif