<nav id="custom-bootstrap-menu" class="navbar navbar-default " role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/home') }}">
                <img alt="logo" src="{{ asset('img/sgmc_logo_navbar.png') }}">
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav navbar-left">
                @if (Auth::user())
                    <li><a href="#Help" data-toggle="modal" data-target=".Help"><i class="fas fa-medkit"></i> Help</a></li>
                    <li><a href="#Airwatch" data-toggle="modal" data-target=".Airwatch"><i class="fas fa-mobile-alt"></i> Airwatch</a></li>
                @endif
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">

                <!-- Authentication Links -->
                @if (Auth::guest())
                    <form class="navbar-form navbar-right" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}
                    <div class="input-group">
                        <div class="input-group-addon alert-danger"><i class="fa fa-user" aria-hidden="true"></i></div>
                        {!! Form::text('username', old('username'),array('class'=>'form-control input-sm', 'autofocus'=>'autofocus', 'placeholder'=>'User Name')) !!}
                    </div>
                        <div class="input-group">
                            <div class="input-group-addon alert-danger"><i class="fa fa-lock" aria-hidden="true"></i></div>
                            <input id="password" type="password" class="form-control input-sm" name="password" placeholder="Password">
                        </div>
                        <div class="btn-group">
                        <button type="submit" class="btn input-sm">
                            <i class="fa fa-btn fa-sign-in"></i> Login
                        </button>
                            </div>
                    </form>
                @else
                    @if ( Auth::user()->hasRole('ExecSummary'))
                    <li><a href="/exec_sum" target="_blank" > ES Dashboard</a></li>
                    @endif

                            @if ( Auth::user()->hasRole('Reports'))
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true">
                                Reports<span class="caret"></span></a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="/user/print" target="">Print Employee List</a></li>
                                <li><a href="/WebForms">Employee Termination</a></li>
                                @if ( Auth::user()->hasRole('DOS'))
                                    <li><a href="/dos">Daily Operational Summary</a></li>
                                @endif
                                @if ( Auth::user()->hasRole('Epic Security'))
                                <li><a href="/epicsecurity">Epic Security</a></li>
                                @endif
                                @if ( Auth::user()->hasRole('ED Report'))
                                    <li><a href="/ed">ED Report</a></li>
                                @endif
                                @if ( Auth::user()->hasRole('SSIS'))
                                    <li><a href="/ssis">SSIS</a></li>
                                @endif
                                <li><a href="/roster">Rosters</a></li>
                                <li><a href="/reports/tardy">Tardy</a></li>
                                <li><a href="/usernames">Username Search</a></li>
                            </ul>
                        </li>
                            @endif

                        @if ( Auth::user()->hasRole('Business Intelligence'))
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true">
                                    BI<span class="caret"></span></a>

                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="/costcenters">Cost Center List</a></li>
                                    <li><a href="/exec_sum" target="_blank">ES Dashboard</a></li>
                                    <li><a href="/pi" target="_blank">PI Dashboard</a></li>
                                </ul>
                            </li>
                        @endif


                            @if ( Auth::user()->hasRole('IS'))
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true">
                                IS<span class="caret"></span></a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="https://helpdesk.sgmc.org/Login.jsp" target="_blank">SysAid</a></li>
                                <li><a href="https://ithelpdesk.sgmc.org/app/itdesk/HomePage.do" target="_blank">Zoho</a></li>
                                <li><a href="http://speedtest.sgmc.org" target="_blank">Speed Test</a></li>
                                <li><a href="/systemlist">System List</a></li>
                                <li><a href="/sysaid" target="_blank">IS Dashboard</a></li>
                            </ul>
                        </li>
                            @endif

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true">
                            ..more<span class="caret"></span></a>

                        <ul class="dropdown-menu" role="menu">

                            @if ( Auth::user()->hasRole('EOC'))
                                <li><a href="{{ url('/eoc') }}"><b>EOC</b></a></li>
                            @endif
                            @if ( Auth::user()->hasRole('EMS'))
                                <li><a href="{{ url('/ems') }}"><b>EMS Dashboard</b></a></li>
                            @endif

                            <li><a href="{{ url('/staffing_tool') }}"><b>Staffing Tool</b></a></li>

                            <li><a href="{{ url('/handwashing') }}"><b>Handwashing</b></a></li>

                            @if ( Auth::user()->hasRole('Imaging-Admin'))
                                <li><a href="{{ url('/imaging') }}"><b>Imaging</b></a></li>
                            @endif

                            @if ( Auth::user()->hasRole('Patient Relations'))
                                <li><a href="{{ url('/pr') }}" target="_blank"><b>Patient Relations</b></a></li>
                            @endif

                            <li><a href="{{ url('/roster') }}"><b>Roster Tool</b></a></li>
                            <li><a href="{{ url('/audit') }}"><b>Survey Tool</b></a></li>

                        </ul>
                    </li>
                        <form class="navbar-form navbar-left" action="/search" method="get">
                            <div class="input-group">
                                <input class="form-control input-sm" type="text"  name="search" placeholder="Employee Search.."/>
                                <span class="fa fa-search form-control-feedback fa-xs"></span>
                            </div>
                        </form>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                @if ( Auth::user()->hasRole('Admin'))
                                    <li><a href="/role"><i class="fa fa-btn fa-user-secret"></i>Roles</a></li>
                                    <li><a href="/permission"><i class="fa fa-btn fa-unlock"></i>Permissions</a></li>

                                @endif
                                <li><a href="{{ url('/user/'. Auth::user()->id .'/edit') }}"><i class="fas fa-edit"></i> Edit Account</a></li>
                                <li><a href="{{ url('/logout') }}"><i class="fas fa-sign-in-alt"></i> Logout</a></li>
                            </ul>
                        </li>
                @endif
            </ul>
        </div>
    </div>
</nav>