<nav id="custom-bootstrap-menu" class="navbar navbar-default " role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ Html::image('img/sgmc_logo_navbar.png') }}
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav navbar-left">
                @if (Auth::user())
                    <li><a href="{{ url('/home') }}"><b>Home</b></a></li>
                @endif
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">




                <!-- Authentication Links -->
                @if (Auth::guest())
                    <form class="navbar-form navbar-right" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}
                        <div class="input-group">
                            <div class="input-group-addon alert-danger"><i class="fa fa-user" aria-hidden="true"></i></div>
                            {!! Form::text('username', old('username'),array('class'=>'form-control input-sm', 'autofocus'=>'autofocus', 'placeholder'=>'User Name')) !!}
                        </div>
                        <div class="input-group">
                            <div class="input-group-addon alert-danger"><i class="fa fa-lock" aria-hidden="true"></i></div>
                            <input id="password" type="password" class="form-control input-sm" name="password" placeholder="Password">
                        </div>
                        <div class="btn-group">
                            <button type="submit" class="btn input-sm">
                                <i class="fa fa-btn fa-sign-in"></i> Login
                            </button>
                        </div>

                    </form>
                @else

                    <li><a href="{{ url('/rounding') }}">Survey Home</a></li>
                    <li><a href="#" data-toggle="modal" data-target=".AddSurvey">Add Survey</a></li>
                    <li><a href="{{ url('/phs/search') }}">Advanced Search</a></li>
                    <li><a href="{{ url('/phs/export') }}">Export</a></li>
                    <li><a href="{{ url('/phs/report') }}">Report</a></li>


                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('/user/'. Auth::user()->id .'/edit') }}"><i class="fa fa-btn fa-pencil"></i>Edit Account</a></li>
                            <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>