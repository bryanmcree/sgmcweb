<nav class="navbar navbar-expand-lg navbar-custom">
    <a class="navbar-brand" href="/home"><img alt="logo" src="{{ asset('img/sgmc_logo_navbar.png') }}"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"><i class="fas fa-bars" style="color: black;"></i></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">


            @if (Auth::guest())


            @else
                <li class="nav-item">
                    <a class="nav-link" href="#Help" data-toggle="modal" data-target=".Help"><i class="fas fa-medkit"></i> Help</a>
                </li>
                {{-- <li class="nav-item">
                    <a class="nav-link" href="#Airwatch" data-toggle="modal" data-target=".Airwatch"><i class="fas fa-mobile-alt"></i> Airwatch</a>
                </li> --}}
                @if ( Auth::user()->hasRole('ExecSummary'))
                    <li class="nav-item">
                        <a class="nav-link" href="/exec_sum" target="_blank" > ES Dashboard</a>
                    </li>
                @endif

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Reports
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="/stats">Stats</a>
                        @if ( Auth::user()->hasRole('Market Share'))
                            <a class="dropdown-item" href="/market-share">Market Share Data</a>
                        @endif
                        @if ( Auth::user()->hasRole('SGMC Properties'))
                            <a class="dropdown-item" href="/properties">SGMC Properties & Holdings</a>
                        @endif
                        <a class="dropdown-item" href="/user/print" target="">Print Employee List</a>
                        <a class="dropdown-item" href="/WebForms">Employee Termination</a>
                        @if ( Auth::user()->hasRole('Epic Security'))
                            <a class="dropdown-item" href="/epicsecurity">Epic Security</a>
                        @endif
                        @if ( Auth::user()->hasRole('ED Report'))
                            <a class="dropdown-item" href="/ed">ED Report</a>
                        @endif
                        @if ( Auth::user()->hasRole('Readmission'))
                            <a class="dropdown-item" href="/readmissions">Readmission</a>
                        @endif
                        @if ( Auth::user()->hasRole('SSIS'))
                            <a class="dropdown-item" href="/ssis">SSIS</a>
                        @endif
                        <a class="dropdown-item" href="/roster">Rosters</a>
                        <a class="dropdown-item" href="/reports/tardy">Tardy</a>
                        <a class="dropdown-item" href="/usernames">Username Search</a>
                    </div>
                </li>

                @if ( Auth::user()->hasRole('Business Intelligence'))
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            BI
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="/costcenters/edit/LE">Edit LE</a>
                            <a class="dropdown-item" href="/costcenters">Cost Center List</a>
                            <a class="dropdown-item" href="/axiom">Axiom Resources</a>
                            <a class="dropdown-item" href="/stats">Stats</a>
                            <a class="dropdown-item" href="/exec_sum" target="_blank">ES Dashboard</a>
                            <a class="dropdown-item" href="https://southga.erp.premierinc.com/ematerials/include/Signon.aspx" target="_blank">ProClick</a>
                            <a class="dropdown-item" href="https://ithelpdesk.sgmc.org/app/itdesk/HomePage.do" target="_blank">Zoho</a>
                        </div>
                    </li>
                @endif
                @if ( Auth::user()->hasRole('IS'))
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            IS
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="https://ithelpdesk.sgmc.org/app/itdesk/HomePage.do" target="_blank">Zoho</a>
                            <a class="dropdown-item" href="http://speedtest.sgmc.org" target="_blank">Speed Test</a>
                            <a class="dropdown-item" href="/systemlist">System List</a>
                            <a class="dropdown-item" href="/sysaid" target="_blank">IS Dashboard</a>
                        </div>
                    </li>
                @endif


                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Surveys
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ url('/handwashing') }}"><b>Handwashing</b></a>
                        <a class="dropdown-item" href="{{ url('/handwashing/admin') }}"><b>Handwashing Admin</b></a>
                        <a class="dropdown-item" href="{{ url('/audit') }}"><b>Survey Tool</b></a>
                        <a class="dropdown-item" href="{{ url('/pcc') }}"><b>Quality Rounding Tool</b></a>
                        @if( Auth::user()->hasRole('EOC'))
                            <a class="dropdown-item" href="{{ url('/eoc') }}"><b>EOC Rounds</b></a>
                        @endif
                        <a class="dropdown-item" href="{{ url('/eoc/safety/create') }}"><b>Safety Star Nomination</b></a>
                        @if( Auth::user()->hasRole('anesthesia_admin'))
                            <a class="dropdown-item" href="{{ url('/anesthesia') }}"><b>Anesthesia Monitoring Tool</b></a>  
                        @else
                            <a class="dropdown-item" href="{{ url('/anesthesia/create') }}"><b>Anesthesia Monitoring Tool</b></a>
                        @endif
                    </div>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="/labor-efficiency">Labor Efficiency</a>
                </li>

                {{-- @if(Auth::user()->hasRole('EM_Admin'))
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Emergency Management
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="/policies">Forms</a>
                        </div>
                    </li>
                @endif --}}

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        ...More
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        
                        @if ( Auth::user()->hasRole('EMS'))
                            <a class="dropdown-item" href="{{ url('/ems') }}"><b>EMS Dashboard</b></a>
                        @endif

                        @if ( Auth::user()->hasRole('dialysis'))
                            <a class="dropdown-item" href="{{ url('/dialysis') }}"><b>Dialysis Dashboard</b></a>
                        @endif

                        @if ( Auth::user()->hasRole('Menu'))
                            <a class="dropdown-item" href="{{ url('/menu/admin') }}"><b>AllSpice Menus</b></a>
                        @endif

                        @if ( Auth::user()->hasRole('Cafeteria Menu'))
                            <a class="dropdown-item" href="{{ url('/cafeteria') }}"><b>Cafeteria Menus</b></a>
                        @endif

                        <a class="dropdown-item" href="{{ url('/staffing_tool') }}"><b>Staffing Tool</b></a>

                        @if ( Auth::user()->hasRole('Imaging-Admin'))
                            <a class="dropdown-item" href="{{ url('/imaging') }}"><b>Imaging</b></a>
                        @endif

                        @if ( Auth::user()->hasRole('Patient Relations'))
                            <a class="dropdown-item" href="{{ url('/pr') }}" target="_blank"><b>Patient Relations</b></a>
                        @endif

                        @if ( Auth::user()->hasRole('SGMC Safety & Security'))
                            <a class="dropdown-item" href="{{ url('/security') }}"><b>Security</b></a>
                        @endif

                        @if( Auth::user()->hasRole('Educational Services'))
                            <a class="dropdown-item" href="{{ url('/education') }}"><b>Educational Services</b></a>  
                        @endif

                        <a class="dropdown-item" href="{{ url('/variance/create') }}"><b>PI Variance Explanation</b></a>
                        <a class="dropdown-item" href="{{ url('/roster') }}"><b>Roster Tool</b></a>
                        
                        <a class="dropdown-item" href="{{ url('/cr') }}"><b>Capital Request</b></a>
                        <a class="dropdown-item" href="/events"><b>Events</b></a>

                        @if ( Auth::user()->hasRole('Chaplain'))
                            <a class="dropdown-item" href="/chaplain"><b>Chaplain DB</b></a>
                        @endif

                            @if ( Auth::user()->hasRole('Palliative Care'))
                            <a class="dropdown-item" href="/palliative"><b>Palliative Care</b></a>
                        @endif

                    </div>
                </li>

                {{-- <li class="nav-item">
                    <a class="nav-link" href="/covid/create">Covid Screening</a>
                </li> --}}

                {{-- <span class="badge text-dark" style="background-color:gold; height:16px;">New!</span> --}}

                {{-- <li class="nav-item">
                    <a class="nav-link" href="/policies">Policies</a>
                </li> --}}

            @endif
        </ul>
        <ul class="nav navbar-nav pull-sm-right">
            @if ( Auth::user()->hasRole('Admin'))
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/role') }}" ><i class="fas fa-lock"></i> Security Roles</a>
                </li>
            @endif
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/logout') }}" > Logout</a>
                </li>
        </ul>


        <form class="form-inline my-2 my-lg-0" action="/search" method="get">
                <input class="form-control mr-sm-2 custom-select-sm" type="text"  name="search" placeholder="Employee Search.."/>
            <button class="btn btn-outline-dark my-2 my-sm-0 btn-sm" type="submit">Search</button>
        </form>

    </div>
</nav>
