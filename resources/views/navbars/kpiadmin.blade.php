<nav class="navbar navbar-inverse navbar-fixed-bottom">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">KPI Admin</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="active"><a href="/scorecard/dashboard">Dashboard</a></li>
            <li><a href="/scorecard/admin/view">Custom Metrics</a></li>
            <li><a href="/scorecard/report" >Reports</a></li>
        </ul>
    </div>
</nav>