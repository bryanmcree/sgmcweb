@extends('layouts.loginpage')

@section('content')
    <div class="container h-100">
        <div class="row">
            @if (isset($success))
                <div class="alert alert-info alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {!! $success !!}.
                </div>
            @endif
        </div>
        <br><br><br><br><br><br>
        <div class="row h-100 justify-content-center align-items-center">
            <div class="card col-lg-5 mb-3 border-light special-card">
                <div class="card-header border-light ">
                    <b>Login to web.sgmc.org</b>
                </div>
                
                <form class="form-horizontal" method="POST" action="{{ url('/login') }}">
                    {{ csrf_field() }}

                    <div class="card-body">

                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label for="username" class="col-xs-12 control-label">User Name</label>

                            <div class="col-md-12">
                                <input id="username" type="username" class="form-control" name="username" value="{{ old('username') }}" required autofocus>

                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong style="color:red">{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-xs-12 control-label">Password</label>

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="card-footer border-light">
                        <button type="submit" class="btn btn-light btn-block">
                            Login
                        </button>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>

@endsection
