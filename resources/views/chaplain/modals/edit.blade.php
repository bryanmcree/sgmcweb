<div class="modal fade Edit" id="EditEntry_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">
                
                <h4 class="modal-title" id="myModalLabel"><b>Edit Entry</b></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

            </div>
            <div class="modal-body bg-default">
                {!! Form::open(array('action' => ['ChaplainController@EditEntry'], 'class' => 'form_control')) !!}
                {!! Form::hidden('id', null,['class'=>'id']) !!}
                <div class="form-group">
                    {!! Form::label('chaplain', 'Chaplain:') !!}
                    {!! Form::text('chaplain', null, ['class' => 'form-control chaplain', 'id'=>'chaplain']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('FirstName', 'First Name:') !!}

                        {!! Form::text('FirstName', null, ['class' => 'form-control FirstName', 'id'=>'FirstName']) !!}

                </div>

                <div class="form-group">
                    {!! Form::label('LastName', 'Last Name:') !!}

                        {!! Form::text('LastName', null, ['class' => 'form-control LastName', 'id'=>'last_name']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('department', 'Department:') !!}
                    {!! Form::text('department', null, ['class' => 'form-control department', 'id'=>'department']) !!}
                </div>

                    <div class="form-group">
                        {!! Form::label('type', 'Type:') !!}
                        {!! Form::select('type', [

                                            '' => '[Select]',
                                            'ad' => 'ad',
                                            'pvi' => 'pvi',
                                            'pvf' => 'pvf',
                                            'pall' => 'pall',
                                            'fst' => 'fst',
                                            'staff' => 'staff',
                                            'code' => 'code',
                                            'death' => 'death',
                                            'fd' => 'fd',
                                            'visitor' => 'visitor',
                                            'phone' => 'phone',
                                            'CISM' => 'CISM',
                                            'Other' => 'Other',
                                            'ER Lobby' => 'ER Lobby',
                                            'Courtesy Cart' => 'Courtesy Cart',

                                            ], null, ['class' => 'form-control type']) !!}
                    </div>

                <div class="form-group">
                    {!! Form::label('followup', 'Follow Up:') !!}
                        {!! Form::select('followup', [

                                            '' => '[Select]',

                                            'Yes' => 'Yes',

                                            'No' => 'No',

                                            ], null, ['class' => 'form-control followup']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('MRN', 'MRN:') !!}
                        {!! Form::text('MRN', null, ['class' => 'form-control MRN', 'id'=>'office_phone']) !!}
                </div>


                <div class="form-group">
                    {!! Form::label('notes', 'Notes:') !!}
                    {!! Form::textarea('notes', null, ['class' => 'form-control notes', 'id'=>'sys_comments', 'rows' => 4]) !!}
                </div>

            </div>
            <div class="modal-footer bg-default">
                <div class="RightLeft">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
                    &nbsp;&nbsp;
                    {!! Form::submit('Edit  Encounter', ['class'=>'btn btn-sgmc', 'id'=>'AddButton']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>