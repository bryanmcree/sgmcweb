@extends('layouts.sgmc_nonav')

@section('content')
        <div class="col-md-12">
            <div class="card mb-3">
                <div class="card-header">
                    <b>Chaplain Encounters</b>
                </div>
                <div class="card-body">
                    <table class="table table-condensed">
                        <thead>
                        <tr>
                            <td></td>
                            <td><div style="font-size: 10px;"><b>Chaplain</b></div></td>
                            <td><div style="font-size: 10px;"><b>First</b></div></td>
                            <td><div style="font-size: 10px;"><b>Last</b></div></td>
                            <td><div style="font-size: 10px;"><b>Type</b></div></td>
                            <td><div style="font-size: 10px;"><b>Department</b></div></td>
                            <td><div style="font-size: 10px;"><b>MRN</b></div></td>
                            <td><div style="font-size: 10px;"><b>Followup</b></div></td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $rowcount = 0 ?>
                        @foreach($encounters as $encounter)
                            <?php $rowcount = $rowcount + 1?>
                            <tr>
                                <td><div style="font-size: 10px;"><b>{{$rowcount}})</b></div></td>
                                <td><div style="font-size: 10px;"> {{$encounter->chaplain}}</div></td>
                                <td><div style="font-size: 10px;"> {{$encounter->FirstName}}</div></td>
                                <td><div style="font-size: 10px;"> {{$encounter->LastName}}</div></td>
                                <td><div style="font-size: 10px;"> {{$encounter->type}}</div></td>
                                <td><div style="font-size: 10px;"> {{$encounter->department}}</div></td>
                                <td><div style="font-size: 10px;"> {{$encounter->MRN}}</div></td>
                                <td><div style="font-size: 10px;"> {{$encounter->followup}}</div></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
            <div class="divFooter">Chaplain Encounter Report - Printed on {{\Carbon\Carbon::now()->format('m/d/Y H:i')}}</div>
        </div>
@endsection

