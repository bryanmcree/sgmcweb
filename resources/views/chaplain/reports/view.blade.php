@extends('layouts.app')
@if ( Auth::user()->hasRole('Chaplain') == FALSE)
    @include('layouts.unauthorized')
    @Else

@section('content')

    <div class="card-deck">
        <div class="card col-md-7 mb-3">
            <div class="card-header">
                <b>Enter Dates</b>
            </div>
            <div class="card-body">
                <div class="form-horizontal">
                    {!! Form::open(array('action' => ['ChaplainController@report_view'], 'class' => 'form_control')) !!}
                    {!! Form::label('start_date', 'Start Date:') !!}
                    {!! Form::input('date','start_date', '', ['class' => 'form-control','required']) !!}
                    <BR>
                    {!! Form::label('end_date', 'End Date:') !!}
                    {!! Form::input('date','end_date', '', ['class' => 'form-control','required']) !!}
                    <BR>
                    <div class="RightLeft">
                        {!! Form::reset('Clear Form', array('class' => 'btn btn-default btn-sm')) !!}
                        {!! Form::submit('Run Report', array('class' => 'btn btn-sgmc btn-sm' )) !!}
                    </div>
                    {!! Form::close() !!}
                    <br>
                    <form method="POST" action="/chaplain/reports/print" target="_blank">
                        {{ csrf_field() }}
                        <input type="hidden" name="start_date" value="{{$start_date}}">
                        <input type="hidden" name="end_date" value="{{$end_date}}">
                        <input type="Submit" class="btn btn-block btn-default" Value="Print Report">
                    </form>

                </div>
            </div>
        </div>

        <div class="card col-md-5 mb-3">
            <div class="card-header">
                <b>Total</b>
            </div>
            <div class="card-body">
                {{$total}}
            </div>
        </div>
    </div>



    <div class="row">
        <div class="col-md-4">
            <div class="card mb-3">
                <div class="card-header">
                    <b>Types of Encounters</b>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <td><b>Type</b></td>
                            <td><b>Total</b></td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($types as $type)
                            <tr>
                                <td>{{$type->type}}</td>
                                <td>{{$type->total}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


        <div class="col-md-4">
            <div class="card mb-3">
                <div class="card-header">
                    <b>Chaplain Totals</b>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <td><b>Type</b></td>
                            <td><b>Total</b></td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($chaplains as $chaplain)
                            <tr>
                                <td>{{$chaplain->chaplain}}</td>
                                <td>{{$chaplain->total}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card mb-3">
                <div class="card-header">
                    <b>Department Totals</b>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <td><b>Type</b></td>
                            <td><b>Total</b></td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($departments as $department)
                            <tr>
                                <td>{{$department->department}}</td>
                                <td>{{$department->total}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>




@endsection
@section('scripts')



@endsection

@endif