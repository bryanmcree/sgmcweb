@extends('layouts.app')
@if ( Auth::user()->hasRole('Chaplain') == FALSE)
    @include('layouts.unauthorized')
    @Else

@section('content')
<div class="col-lg-6">
    <div class="card mb-3">
        <div class="card-header">
            <b>Enter Dates</b>
        </div>
        <div class="card-body">
            <div class="form-horizontal">
                {!! Form::open(array('action' => ['ChaplainController@report_view'], 'class' => 'form_control')) !!}
                {!! Form::label('start_date', 'Start Date:') !!}
                {!! Form::input('date','start_date', '', ['class' => 'form-control','required']) !!}
                <BR>
                {!! Form::label('end_date', 'End Date:') !!}
                {!! Form::input('date','end_date', '', ['class' => 'form-control','required']) !!}
                <BR>
                <div class="RightLeft">
                    {!! Form::reset('Clear Form', array('class' => 'btn btn-default btn-sm')) !!}
                    {!! Form::submit('Run Report', array('class' => 'btn btn-sgmc btn-sm' )) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>


@endsection
@section('scripts')



@endsection

@endif