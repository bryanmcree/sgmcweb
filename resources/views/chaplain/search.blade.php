@extends('layouts.app')
@if ( Auth::user()->hasRole('Chaplain') == FALSE)
    @include('layouts.unauthorized')
    @Else

@section('content')
    <div class="col-md-12">
        <div class="card mb-3">
            <div class="card-header"><i class="fa fa-question-circle"  data-toggle="popover" data-placement="bottom"
                title="System List"
                data-content="Searchable list of all critical computer applications"></i> -><b>Pastoral Encounters </b>  -> {{$data->total()}}
            </div>
            <div class="card-body">
                @if ($data->isEmpty())
                    <div class="row">
                        <form method="post" action="/chaplain/search">
                            {{ csrf_field() }}
                            <div class="input-group">

                                <a href = "/chaplain"  class = "btn btn-sgmc" role = "button">Home</a>&nbsp;&nbsp;
                                {!! Form::text('search', null,array('class'=>'form-control','id'=>'myInputTextField','placeholder'=>'Search pastoral encounters.', 'autofocus'=>'autofocus')) !!}
                                <span class="input-group-btn">
                                    <input type="submit" class="btn btn-primary" value="Search">&nbsp;&nbsp;
                                </span>
                                    <a href = "#AddEntry" data-toggle="modal" data-target=".AddEntry" class = "btn btn-sgmc" role = "button">Add  Encounter</a>
                            </div>
                        </form>
                    </div>
                    <div class="alert alert-info" role="alert">No Information. </div>
                @else
                    <div class="container-fluid">
                        <div class="row">
                            <form method="post" action="/chaplain/search">
                                {{ csrf_field() }}
                                <div class="input-group">
                                    <a href = "/chaplain"  class = "btn btn-sgmc" role = "button">Home</a>&nbsp;&nbsp;
                                    {!! Form::text('search', null,array('class'=>'form-control','id'=>'myInputTextField','placeholder'=>'Search pastoral encounters.', 'autofocus'=>'autofocus')) !!}
                                    <span class="input-group-btn">
                                        <input type="submit" class="btn btn-primary" value="Search"> &nbsp;&nbsp;
                                    </span>
                                    <a href = "#AddEntry" data-toggle="modal" data-target=".AddEntry" class = "btn btn-sgmc" role = "button">Add  Encounter</a>
                                </div>
                            </form>
                        </div>
                    </div>
                    <BR>
                    <table class="table table-condensed table-hover table-responsive table-bordered" id="chaplain">
                        <thead>
                        <tr >
                            <td><b>Date</b></td>
                            <td><b>Chaplain</b></td>
                            <td class="hidden-xs hidden-sm hidden-md"><b>Name</b></td>
                            <td class="hidden-xs hidden-sm hidden-md"><b>Type</b></td>
                            <td class="hidden-xs hidden-sm hidden-md"><b>Department</b></td>
                            <td align="center" class="hidden-xs hidden-sm hidden-md"><b>MRN</b></td>
                            <td align="center" class="hidden-xs hidden-sm hidden-md"><b>Follow Up?</b></td>
                            <td align="center" class="hidden-xs hidden-sm hidden-md no-sort"><b>Notes</b></td>
                            <td class='no-sort' align="center" ></td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($data as $chaplain)
                            <tr>
                                <td nowrap="">@if($chaplain->created_at =='')) @else {{$chaplain->created_at->format('m-d-Y')}} @endif</td>
                                <td nowrap="">{{$chaplain->chaplain}} <div style="display: none"> {{$chaplain->notes}}</div></td>
                                <td nowrap="" class="hidden-xs hidden-sm hidden-md">{{$chaplain->LastName}}, {{$chaplain->FirstName}}</td>
                                <td align="center" class="hidden-xs hidden-sm hidden-md">{{$chaplain->type}}</td>
                                <td align="center" class="hidden-xs hidden-sm hidden-md">{{$chaplain->department}}</td>
                                <td align="center" class="hidden-xs hidden-sm hidden-md">{{$chaplain->MRN}}</td>
                                <td align="center" class="hidden-xs hidden-sm hidden-md">{{$chaplain->followup}}</td>
                                <td class="hidden-xs hidden-sm hidden-md">{{$chaplain->notes}}</td>
                                <td nowrap="" style="vertical-align:middle">

                                    <a href="#" data-toggle="modal"  class="EditEntry btn btn-sm btn-primary"
                                       data-FirstName="{{$chaplain->FirstName}}"
                                       data-LastName="{{$chaplain->LastName}}"
                                       data-type="{{$chaplain->type}}"
                                       data-id="{{$chaplain->id}}"
                                       data-department="{{$chaplain->department}}"
                                       data-MRN="{{$chaplain->MRN}}"
                                       data-followup="{{$chaplain->followup}}"
                                       data-notes="{{$chaplain->notes}}"
                                       data-chaplain="{{$chaplain->chaplain}}"
                                    ><i class="fas fa-edit" aria-hidden="true"></i></span></a>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
                    <div class="container-fluid">
                        <div class="row">
                            {!! $data->render() !!}
                        </div>
                    </div>
            </div>
        </div>
    </div>

    @include('chaplain.modals.add')
    @include('chaplain.modals.edit')

@endsection
@section('scripts')

    <script type="text/javascript">

        $(document).ready(function() {
            oTable = $('#chaplain1').DataTable(
                    {
                        "info":     false,
                        "pageLength": 15,
                        "lengthChange": false,
                        "order": [],
                        "columnDefs": [
                            { targets: 'no-sort', orderable: false }
                        ],
                        "dom": '<l<t>ip>'

                    }
            );
            $('#myInputTextField').keyup(function(){
                oTable.search($(this).val()).draw() ;
            })

        });


        $(document).on("click", ".EditEntry", function () {
            //alert('Edit Server Button Clicked');
            $('.FirstName').val($(this).attr("data-FirstName"));
            $('.LastName').val($(this).attr("data-LastName"));
            $('.type').val($(this).attr("data-type"));
            $('.id').val($(this).attr("data-id"));
            $('.department').val($(this).attr("data-department"));
            $('.MRN').val($(this).attr("data-MRN"));
            $('.followup').val($(this).attr("data-followup"));
            $('.notes').val($(this).attr("data-notes"));
            $('.chaplain').val($(this).attr("data-chaplain"));
            $('#EditEntry_modal').modal('show');
            //alert($(this).attr("data-id"));
        });

        $(document).on("click", ".DeleteEntry", function () {
            var id = $(this).attr("data-id");
            //alert('Edit Server Button Clicked');
            deleteEntry(id);
        });

        function deleteEntry(id) {
            swal({
                title: "Delete Entry?",
                text: "Are you sure that you want to delete this entry?",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                confirmButtonText: "Yes, delete it!",
                confirmButtonColor: "#ec6c62"
            }, function () {
                $.ajax({
                    url: "/chaplain/del/" + id,
                    type: "GET"
                })
                        .done(function (data) {
                            swal({
                                        title: "Deleted",
                                        text: "Entry was Deleted",
                                        type: "success",
                                        timer: 1800,
                                        showConfirmButton: false

                                    }
                            );
                            setTimeout(function () {
                                window.location.replace('/systemlist/' + systemId)
                            }, 1900);
                        })
                        .error(function (data) {
                            swal("Oops", "We couldn't connect to the server!", "error");
                        });
            });
        }
    </script>
    @endif
@endsection