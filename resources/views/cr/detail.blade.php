{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="card bg-dark text-white mb-3">

            <div class="card-header">
                <h4><b>{{$cost_center->style1}}</b></h4>
                <i>{{$budget_year+1}} is the current budget year.</i>
                <br> <br>
                <div class="btn-group">
                    <a class="btn btn-sm btn-success" href="/cr"><i class="fas fa-arrow-left"></i> Select New Cost Center</a>
                    <a class="btn btn-sm btn-primary" data-toggle="collapse" href="#AddQuestionForm" aria-expanded="false" aria-controls="AddQuestionForm"><i class="fas fa-plus"></i> Add Capital Item</a>
                </div>
            </div>

            <div class="card-body">

                <div class="collapse" id="AddQuestionForm">
                    <div class="col-md-12">
                        <form method="post" action="/cr/detail/add" style="width:50%;">
                            {{ csrf_field() }}
                            <input type="hidden" value="{{ Auth::user()->employee_number }}" name="created_by">
                            <input type="hidden" value="{{$cost_center->cost_center}}" name="cost_center">
                            <input type="hidden" value="TBD" name="class">
                            <input type="hidden" name="project_manager" id="project_manager" required>

                            <div class="form-group">
                                <label for="required_id">Project Manager</label>
                                <small id="emailHelp" class="form-text text-muted">The project manager will receive all updates and questions regarding this item.  They will also be responsible for all supporting documentation and keeping the project within approved costs. MUST SELECT FROM LIST!</small>
                                <input type="text" id="autocomplete" class="form-control" placeholder="Search by employee name" minlength="8" required>
                            </div>

                            <div class="form-group">
                                <label for="question_type_id">Request Reason:</label>
                                <small id="emailHelp" class="form-text text-muted">Select the best reason for this item.</small>
                                <select class="form-control" id="question_type_id" name="request_reason" required>
                                    <option value="" selected>[Select Reason]</option>
                                    <option value="Incremental Revenue Generating">Incremental Revenue Generating (Pro Forma Required)</option>
                                    <option value="End of Life">End of Life</option>
                                    <option value="Regulatory / Compliance">Regulatory / Compliance</option>
                                    <option value="Building Improvements">Building Improvements</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="required_id">Physician Request?</label>
                                <small id="emailHelp" class="form-text text-muted">Was this item requested by a physician?</small>
                                <select class="form-control" id="required_id" name="dr_request" required>
                                    <option value="0" selected>No</option>
                                    <option value="1">Yes</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="required_id">Break/fix Purchase?</label>
                                <select class="form-control" id="required_id" name="break_fix" required>
                                    <option value="0" selected>No</option>
                                    <option value="1">Yes</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="title_id">Item Title:</label>
                                <small id="emailHelp" class="form-text text-muted">The title of the item you are requesting.</small>
                                <input type="text" name="item_description" class="form-control" id="title_id" required>
                            </div>
                            <div class="form-group">
                                <label for="title_id">Request Description:</label>
                                <small id="emailHelp" class="form-text text-muted">Description of request type.</small>
                                <select name="request_description" class="form-control" required>
                                    <option value="">[Choose Description]</option>
                                    <option value="LAND (LD)">LAND (LD)</option>
                                    <option value="LAND IMPROVEMENT (LI)">LAND IMPROVEMENT (LI)</option>
                                    <option value="BUILDINGS (BD)">BUILDINGS (BD)</option>
                                    <option value="FIXED EQUIPMENT (FE)">FIXED EQUIPMENT (FE)</option>
                                    <option value="MAJOR MOVEABLE EQUIPMENT (MM)">MAJOR MOVEABLE EQUIPMENT (MM)</option>
                                    <option value="DIAGNOSTIC EQUIPMENT (DG)">DIAGNOSTIC EQUIPMENT (DG)</option>
                                    <option value="HARDWARE COMPUTER (HD)">HARDWARE COMPUTER (HD)</option>
                                    <option value="SOFTWARE COMPUTER ">SOFTWARE COMPUTER</option>
                                    <option value="CAPITAL LEASE EQUIPMENT (CL)">CAPITAL LEASE EQUIPMENT (CL)</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="question_notes_id">Articulation:</label>
                                <small id="emailHelp" class="form-text text-muted">Explain why this item is needed.</small>
                                <textarea class="form-control" name="articulation" id="question_notes_id" rows="3"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="consumables_id">Consumables:</label>
                                <small id="emailHelp" class="form-text text-muted">List any consumables that this item will require.</small>
                                <textarea class="form-control" name="consumables" id="consumables_id" rows="3"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="required_id">Campus?</label>
                                <small id="emailHelp" class="form-text text-muted">Select location for item to determine capital limit.</small>
                                <select class="form-control" id="campus" name="location" onchange="leaveChange()" required>
                                    <option value="" selected>[Select Location]</option>
                                    <option value="SGMC Main">SGMC Main - $1000</option>
                                    <option value="SGMC Outpatient Plaza">SGMC Outpatient Plaza (SNH) - $1000</option>
                                    <option value="Berrien">Berrien - $1000</option>
                                    <option value="Lakeland Villa">Lakeland Villa - $5000</option>
                                    <option value="Lanier">Lanier - $5000</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="title_id">Unit Price:</label>
                                <small id="emailHelp" class="form-text text-muted">Cost of $1,000 or greater at SGMC Main, Outpatient Plaza (SNH) and Berrien; $5,000 at Lanier Campus and SGMC Lakeland Villa.</small>
                                <input type="number" name="unit_price" class="form-control" id="id_unit_price" step="0.01" min="1000.00" required>
                            </div>
                            <div class="form-group">
                                <label for="title_id">Quantity:</label>
                                <input type="number" name="qty" class="form-control" id="title_id" step="1.0" min="0" required>
                            </div>
                            <div class="form-group">
                                <label for="question_type_id">Priority:</label>
                                <select class="form-control" id="question_type_id" name="priority" required>
                                    <option value="" selected>[Select Priority]</option>
                                    <option value="1">1 = Priority</option>
                                    <option value="2">2 = Needed</option>
                                    <option value="3">3 = Desired</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="question_type_id">What budget year is this item for?  The current budget year is {{$budget_year}}:</label>
                                <select class="form-control" id="question_type_id" name="budget_year" required>
                                    <option value="{{$budget_year+1}}" selected>{{$budget_year+1}}</option>
                                    <option value="{{$budget_year+2}}">{{$budget_year+2}}</option>
                                </select>
                            </div>

                            <hr>

                            <input type="Submit" class="btn btn-primary btn-block" value="Add Item">
                        </form>
                        
                        <hr>
                    </div>
                </div>


                @if($items_year->isEmpty())
                    <div class="col-md-12">
                        <div class="alert alert-info">There are no items.</div>
                    </div>

                @else

                @foreach($items_year as $fy_year)
                    <div class="card bg-secondary text-white mb-3">

                        <div class="card-header">
                            <b>FY {{$fy_year->budget_year}}</b>
                        </div>

                        <div class="card-body">

                            <div class="card-deck mb-3">
                                <div class="card bg-danger">
                                    <div class="card-header">
                                        <b>Capital Pending Approval</b>
                                    </div>
                                    <div class="card-body">
                                        @if(is_null($total_pending)) ${{number_format(0,2)}} @else ${{number_format($total_pending,2)}} @endif
                                    </div>
                                </div>
                                <div class="card bg-warning">
                                    <div class="card-header">
                                        <b>Capital Approved</b>
                                    </div>
                                    <div class="card-body">
                                        @if(is_null($total_approved)) ${{number_format(0,2)}} @else ${{number_format($total_approved,2)}} @endif
                                    </div>
                                </div>
                                <div class="card bg-success">
                                    <div class="card-header">
                                        <b>Capital Purchased</b>
                                    </div>
                                    <div class="card-body">
                                        @if(is_null($total_purchased)) ${{number_format(0,2)}} @else ${{number_format($total_purchased,2)}} @endif
                                    </div>
                                </div>
                            </div>
                            <div class="card-deck">
                                @if ( Auth::user()->hasRole('CR Admin'))
                                    <div class="card bg-dark text-white mb-3">
                                        <div class="card-header">
                                            <b>Total Operational</b>
                                        </div>
                                        <div class="card-body">
                                            @if(is_null($total_operational)) ${{number_format(0,2)}} @else ${{number_format($total_operational,2)}} @endif
                                        </div>
                                    </div>

                                    <div class="card bg-dark text-white mb-3">
                                        <div class="card-header">
                                            <b>TBD Capital or Operating Expense</b>
                                        </div>
                                        <div class="card-body">
                                            @if(is_null($total_tbd)) ${{number_format(0,2)}} @else ${{number_format($total_tbd,2)}} @endif
                                        </div>
                                    </div>
                                @endif
                            </div>

                            <table class="table table-dark table-striped table-bordered table-hover" id="items">
                                <thead>
                                    <tr>
                                        <td>#</td>
                                        <td><b>Item</b></td>
                                        <td><b>Description</b></td>
                                        <td><b>Class</b></td>
                                        <td><b>Project Mgr.</b></td>
                                        <td align="center"><b>Status</b></td>
                                        <td align="center"><b>Qty.</b></td>
                                        <td align="right"><b>Unit Price</b></td>
                                        <td align="right"><b>Total</b></td>
                                        <td align="center"><b>Actions</b></td>
                                    </tr>
                                </thead>
                                <tbody>

                                <?PHP $row_number = 0 ?>
                                @foreach($items->where('budget_year',$fy_year->budget_year) as $item)
                                <?PHP $row_number = $row_number + 1 ?>
                                    <tr>
                                        <td><b>{{$row_number}})</b></td>
                                        <td>
                                            {{$item->item_description}} 
                                            @if($item->requires_attention == 1)<span data-toggle="tooltip" title="Item requires attention."><i class="fas fa-cog fa-spin" style="color:orangered"></i></span> @endif
                                            @if($item->dr_request == 1) <span data-toggle="tooltip" title="Item was requested by physician."><i class="fas fa-user-md" data-toggle="tooltip" title="Hooray!" style="color:Red"></i></span> @endif
                                        </td>
                                        <td>{{$item->request_description}}</td>
                                        <td>{{$item->class}}</td>
                                        <td>{{$item->projectManager->last_name}}, {{$item->projectManager->first_name}}</td>
                                        <td align="center">

                                            @if(Carbon::parse($item->denied_date)->format('Y-m-d') <> '1900-01-01')
                                                <span data-toggle="tooltip" title="Item has been denied."><i class="fas fa-times-circle" style="color:black"></i></span>
                                            @else
                                                @if($item->class =='Operational')
                                                    <span data-toggle="tooltip" title="Item is not capital."><i class="fas fa-ban" style="color:purple"></i></span>
                                                @else

                                                    @if($item->approved->format('Y-m-d') <> '1900-01-01' && $item->purchased->format('Y-m-d') == '1900-01-01')
                                                        <span data-toggle="tooltip" title="Item has been approved."><i class="fas fa-check-circle" style="color:greenyellow"></i></span>
                                                    @elseif($item->approved->format('Y-m-d') <> '1900-01-01' && $item->purchased->format('Y-m-d') <> '1900-01-01')
                                                        <span data-toggle="tooltip" title="Item has been ordered."><i class="fas fa-shopping-cart" style="color:green"></i></span>
                                                    @else
                                                            @if($item->class == 'TBD')
                                                                <span data-toggle="tooltip" title="Item requires classification of operational expense or capital."><i class="fas fa-adjust" style="color:GrayText"></i></span>
                                                            @else
                                                        <span data-toggle="tooltip" title="Item is pending approval."><i class="fas fa-clock" style="color:Red"></i></span>
                                                            @endif

                                                    @endif

                                                @endif
                                            @endif

                                        </td>
                                        <td align="center">{{$item->qty}}</td>
                                        <td align="right">${{number_format($item->unit_price,2)}}</td>
                                        <td align="right">${{number_format($item->qty*$item->unit_price,2)}}</td>
                                        <td align="center">
                                            <a href="/cr/detail/item/{{$item->id}}" class="btn btn-sm btn-info">Detail</a> 
                                            @if(Auth::user()->hasRole('CR Admin') || Auth::user()->employee_number == $item->created_by) 
                                                <a href="#" cr_id="{{ $item->id }}" class="btn btn-sm btn-danger deleteCR">Delete</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                @endforeach

            @endif
            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">


        $(document).ready(function() {
            $('#items').DataTable( {
                "pageLength": 25

            } );
        } );


    //Project manager autocomplete script
    $('#autocomplete').autocomplete({
        serviceUrl: '/cr/employeesearch',
        dataType: 'json',
        type:'GET',
        width: 418,
        minChars:2,
        onSelect: function(suggestion) {
            //alert(suggestion.data);
            $("#project_manager").val(suggestion.data);
        }
    });

    //ToolTip Script
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });

    //Budget limits conditional script
    function leaveChange() {
        //alert(this.value)

        if (document.getElementById("campus").value == "Lakeland Villa" || document.getElementById("campus").value == "Lanier"){
            //alert("got it!");
            $('#id_unit_price').prop({
                'min': 5000.00
            });
        }
        else{
            $('#id_unit_price').prop({
                'min': 1000.00
            });
        }
    }


</script>


<script type="application/javascript">

    $(document).on("click", ".deleteCR", function () {
        //alert($(this).attr("data-cost_center"));
        var cr_id = $(this).attr("cr_id");
        DeleteDispatch(cr_id);
    });
    
    function DeleteDispatch(cr_id) {
        swal({
            title: "Delete Capital Request?",
            text: "Deleting this Capital Request cannot be undone.",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            confirmButtonText: "Yes, delete it!",
            confirmButtonColor: "#ec6c62"
        }, function() {
            $.ajax({
                url: "/cr/detail/item/destroy/" + cr_id,
                type: "GET"
            })
                .done(function(data) {
                    swal({
                            title: "Deleted",
                            text: "Capital Request Deleted",
                            type: "success",
                            timer: 1800,
                            showConfirmButton: false
    
                        },function() {
                            location.reload();
                        });
                })
                .error(function(data) {
                    swal("Oops", "We couldn't connect to the server!", "error");
                });
        });
    }
    
    </script>

@endsection
