{{--New file Template--}}

{{--Add Security for this page below--}}


    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')
    @if ( Auth::user()->hasRole('CR Admin'))
        <div class="card bg-dark text-white mb-3">
            <div class="card-header">
                <h4><b>Quick View</b></h4>
            </div>
            <div class="card-body">

                <div class="row">
                    <div class="col-sm-2">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <b>Unclassified</b>
                            </div>
                            <div class="panel-body">
                                {{$items->where('class','TBD')->count()}}
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <b>Total Pending</b>
                            </div>
                            <div class="panel-body">
                                @if(is_null($total_pending)) ${{number_format(0,2)}} @else ${{number_format($total_pending,2)}} @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <b>Total Approved</b>
                            </div>
                            <div class="panel-body">
                                @if(is_null($total_approved)) ${{number_format(0,2)}} @else ${{number_format($total_approved,2)}} @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <b>Total Purchased</b>
                            </div>
                            <div class="panel-body">
                                @if(is_null($total_purchased)) ${{number_format(0,2)}} @else ${{number_format($total_purchased,2)}} @endif
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    @endif

    <div class="card bg-dark text-white mb-3">
        <div class="card-header">
            <h3><b>Capital Budget Request</b></h3>
            <i>2019 is the current budget year.</i>
        </div>
        <div class="card-body">

            <table class="table table-striped table-dark table-bordered table-hover" id="example">
                <thead>
                <tr>
                    <td><b>Cost Center</b></td>
                    <td><b>Request</b></td>
                    <td></td>
                </tr>
                </thead>
                <tbody>
                @foreach($select_cost_center as $costcenter)
                    <tr>
                        <td>{{$costcenter->style1}}</td>
                        <td>{{$costcenter->crRequest->count()}}</td>
                        <td align="right"><a href="/cr/detail/{{$costcenter->cost_center}}" class="btn btn-primary btn-sm btn-block">Select</a> </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="application/javascript">
        $(document).ready(function() {
            $('#example').DataTable( {
                "pageLength": 10

            } );
        } );
    </script>

@endsection
