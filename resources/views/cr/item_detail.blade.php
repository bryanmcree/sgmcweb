{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Page Design Goes Below--}}
@section('content')

    <div class="card bg-dark text-white mb-3">
        <div class="card-header">
            <h4><b>{{$cost_center->style1}}</b></h4>
            <i>{{$budget_year}} is the current budget year.</i>
            <br>
            <br>
            <a class="btn btn-sm btn-success" href="/cr/detail/{{$cost_center->cost_center}}"><i class="fas fa-arrow-left"></i> Back to All Items</a>
        </div>
        <div class="card-body">

            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab">Item Detail</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="dashboard-tab" data-toggle="tab" href="#menu1" role="tab">Settings</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="show-tab" data-toggle="tab" href="#menu2" role="tab">Sbar</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="history-tab" data-toggle="tab" href="#files" role="tab">Files</a>
                </li>
            </ul>

            <div class="tab-content">
                <div id="home" class="tab-pane fade show active" role="tabpanel">
                    <br>
                    <div class="col-md-12">
                        <div class="card bg-secondary text-white mb-3 col-6 m-auto">
                            <div class="card-header">
                                <b>ITEM DETAIL - {{$item->item_description}}</b>
                            </div>
                            <div class="card-body">

                                <form method="post" action="/cr/detail/edit">
                                    {{ csrf_field() }}
                                    <input type="hidden" value="{{$item->id}}" name="id">
                                    <div class="form-group">
                                        <label for="question_type_id">Request Reason:</label>
                                        <small id="emailHelp" class="form-text text-white">Select the best reason for this item.</small>
                                        <select class="form-control" id="question_type_id" name="request_reason" required>
                                            <option value="{{ $item->request_reason }}" selected>{{ $item->request_reason }}</option>
                                            <option value="Incremental Revenue Generating">Incremental Revenue Generating (Pro Forma Required)</option>
                                            <option value="End of Life">End of Life</option>
                                            <option value="Regulatory / Compliance">Regulatory / Compliance</option>
                                            <option value="Capital Building Request">Capital Building Request</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="required_id">Physician Request?</label>
                                        <small id="emailHelp" class="form-text text-white">Was this item requested by a physician?</small>
                                        <select class="form-control" id="required_id" name="dr_request" required>
                                            @if($item->dr_request == 0)
                                                <option value="0" selected>No</option>
                                                <option value="1">Yes</option>
                                            @else
                                                <option value="1" selected>Yes</option>
                                                <option value="0">No</option>
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="required_id">Break/fix Purchase?</label>
                                        <select class="form-control" id="required_id" name="break_fix" required>
                                            <option value="{{ $item->break_fix }}">@if($item->break_fix == 1) Yes @else No @endif</option>
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="title_id">Item Title:</label>
                                        <small id="emailHelp" class="form-text text-white">The title of the item you are requesting.  NOT JUSTIFICATION</small>
                                        <input type="text" value="{{$item->item_description}}" name="item_description" class="form-control" id="title_id" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="title_id">Request Description:</label>
                                        <small id="emailHelp" class="form-text text-muted">Description of request type.</small>
                                        <select name="request_description" class="form-control" required>
                                            <option value="{{ $item->request_description }}" selected>{{ $item->request_description }}</option>
                                            <option value="LAND (LD)">LAND (LD)</option>
                                            <option value="LAND IMPROVEMENT (LI)">LAND IMPROVEMENT (LI)</option>
                                            <option value="BUILDINGS (BD)">BUILDINGS (BD)</option>
                                            <option value="FIXED EQUIPMENT (FE)">FIXED EQUIPMENT (FE)</option>
                                            <option value="MAJOR MOVEABLE EQUIPMENT (MM)">MAJOR MOVEABLE EQUIPMENT (MM)</option>
                                            <option value="DIAGNOSTIC EQUIPMENT (DG)">DIAGNOSTIC EQUIPMENT (DG)</option>
                                            <option value="HARDWARE COMPUTER (HD)">HARDWARE COMPUTER (HD)</option>
                                            <option value="SOFTWARE COMPUTER ">SOFTWARE COMPUTER</option>
                                            <option value="CAPITAL LEASE EQUIPMENT (CL)">CAPITAL LEASE EQUIPMENT (CL)</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="question_notes_id">Articulation:</label>
                                        <small id="emailHelp" class="form-text text-white">Explain why you need this item, how will it improve your department.</small>
                                        <textarea class="form-control" name="articulation" id="question_notes_id" rows="3">{{$item->articulation}}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="title_id">Unit Price:</label>
                                        <input type="number" value="{{$item->unit_price}}" name="unit_price" class="form-control" id="title_id" step="0.01" min="0" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="title_id">Quantity:</label>
                                        <input type="number" value="{{$item->qty}}" name="qty" class="form-control" id="title_id" step="1.0" min="0" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="question_type_id">Priority:</label>
                                        <small id="emailHelp" class="form-text text-white">Assign priority depending on your list for this cost center.  </small>
                                        <select class="form-control" id="question_type_id" name="priority" required>
                                            <option value="{{$item->priority}}" selected>{{$item->priority}}</option>
                                            <option value="1">1 = Highest</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5 = Lowest</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="question_type_id">What budget year is this item for?  The current budget year is {{$budget_year}}:</label>
                                        <select class="form-control" id="question_type_id" name="budget_year" required>
                                            <option value="{{$item->budget_year}}" selected>{{$item->budget_year}}</option>
                                            <option value="{{$budget_year}}">{{$budget_year}}</option>
                                            <option value="{{$budget_year+1}}">{{$budget_year+1}}</option>
                                        </select>
                                    </div>
                                    <hr>
                                    <input type="Submit" class="btn btn-primary btn-block" value="Update Item Detail">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="menu1" class="tab-pane fade" role="tabpanel">
                    <br>
                    <div class="col-md-12">
                        <div class="card bg-secondary text-white mb-3 col-6 m-auto">
                            <div class="card-header">
                                <b>Other Details - {{$item->item_description}}</b>
                            </div>
                            <div class="card-body">
                                @if ( !Auth::user()->hasRole('CR Admin'))<i> Only administrators can update fields below. </i>@endif
                                <form method="post" action="/cr/detail/edit_settings">
                                    {{ csrf_field() }}
                                    <input type="hidden" value="{{$item->id}}" name="id">
                                <div class="form-group">
                                    <label for="class_id">Classification?</label>
                                    <small id="emailHelp" class="form-text text-white">Operating Expense or Capital Determination</small>
                                    <select class="form-control" id="class_id" name="class" required @if ( !Auth::user()->hasRole('CR Admin')) disabled @endif>
                                        <option value="{{$item->class}}" selected>{{$item->class}}</option>
                                        <option value="TBD">TBD</option>
                                        <option value="Capital">Capital</option>
                                        <option value="Operational">Operational</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="title_id">Denied:</label>
                                    @if(Carbon::parse($item->denied_date)->format('Y-m-d') == '1900-01-01')
                                        <input type="date" name="denied_date" class="form-control" id="approved_id" @if ( !Auth::user()->hasRole('CR Admin')) disabled @endif>
                                    @else
                                        <input type="date" value="{{ Carbon::parse($item->denied_date)->format('Y-m-d')}}" name="denied_date" class="form-control" id="approved_id" @if ( !Auth::user()->hasRole('CR Admin')) disabled @endif>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="question_notes_id">Denied Reason:</label>
                                    <textarea class="form-control" name="denied_reason" id="question_notes_id" rows="3" @if ( !Auth::user()->hasRole('CR Admin')) disabled @endif>{{$item->denied_reason}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="title_id">Approved:</label>
                                    @if( Carbon::parse($item->approved)->format('Y-m-d') == '1900-01-01')
                                    <input type="date" name="approved" class="form-control" id="approved_id" @if ( !Auth::user()->hasRole('CR Admin')) disabled @endif>
                                    @else
                                    <input type="date" value="{{ Carbon::parse($item->approved)->format('Y-m-d')}}" name="approved" class="form-control" id="approved_id" @if ( !Auth::user()->hasRole('CR Admin')) disabled @endif>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="title_id">CER Number:</label>
                                    <small id="emailHelp" class="form-text text-white">Capital Expenditure Request (CER) Number</small>
                                    <input type="text" value="{{$item->cer_number}}" name="cer_number" class="form-control" id="title_id" @if ( !Auth::user()->hasRole('CR Admin')) disabled @endif>
                                </div>
                                <div class="form-group">
                                    <label for="title_id">Forwarded to Legal:</label>
                                    @if( Carbon::parse($item->forwarded_legal)->format('Y-m-d') == '1900-01-01')
                                        <input type="date"  name="forwarded_legal" class="form-control" id="purchased_id" @if ( !Auth::user()->hasRole('CR Admin')) disabled @endif>
                                    @else
                                        <input type="date" value="{{ Carbon::parse($item->forwarded_legal)->format('Y-m-d')}}" name="forwarded_legal" class="form-control" id="purchased_id" @if ( !Auth::user()->hasRole('CR Admin')) disabled @endif>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="title_id">Ordered:</label>
                                    @if( Carbon::parse($item->purchased)->format('Y-m-d') == '1900-01-01')
                                    <input type="date"  name="purchased" class="form-control" id="purchased_id" @if ( !Auth::user()->hasRole('CR Admin')) disabled @endif>
                                    @else
                                    <input type="date" value="{{ Carbon::parse($item->purchased)->format('Y-m-d')}}" name="purchased" class="form-control" id="purchased_id" @if ( !Auth::user()->hasRole('CR Admin')) disabled @endif>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="title_id">Item Received:</label>
                                    @if( Carbon::parse($item->item_received)->format('Y-m-d') == '1900-01-01')
                                        <input type="date" name="item_received" class="form-control" id="purchased_id" @if ( !Auth::user()->hasRole('CR Admin')) disabled @endif>
                                    @else
                                        <input type="date" value="{{ Carbon::parse($item->item_received)->format('Y-m-d')}}" name="item_received" class="form-control" id="purchased_id" @if ( !Auth::user()->hasRole('CR Admin')) disabled @endif>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="title_id">In-Service:</label>
                                    @if( Carbon::parse($item->inservice_date)->format('Y-m-d') == '1900-01-01')
                                    <input type="date" name="inservice_date" class="form-control" id="purchased_id" >
                                    @else
                                    <input type="date" value="{{ Carbon::parse($item->inservice_date)->format('Y-m-d')}}" name="inservice_date" class="form-control" id="purchased_id" >
                                    @endif
                                </div>
                                <hr>
                                <input type="Submit" class="btn btn-primary btn-block" value="Update Settings" @if ( !Auth::user()->hasRole('CR Admin')) disabled @endif>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="menu2" class="tab-pane fade" role="tabpanel">
                    <br>
                    <div class="col-md-12">
                        <div class="card bg-secondary text-white mb-3 col-6 m-auto">
                            <div class="card-header">
                                <b>Sbar - {{$item->item_description}}</b>
                            </div>
                            <div class="card-body">
                                <form method="post" action="/cr/detail/edit_sbar">
                                    {{ csrf_field() }}
                                    <input type="hidden" value="{{$item->id}}" name="id">
                                <div class="form-group">
                                    <label for="question_notes_id">SBAR - Situation:</label>
                                    <small id="emailHelp" class="form-text text-white">(What, Why, Where, level of concern)</small>
                                    <textarea class="form-control" name="sbar_situation" id="question_notes_id" rows="3">{{$item->sbar_situation}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="question_notes_id">SBAR - Background:</label>
                                    <small id="emailHelp" class="form-text text-white">(current or historical performance/justification)</small>
                                    <textarea class="form-control" name="sbar_background" id="question_notes_id" rows="3">{{$item->sbar_background}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="question_notes_id">SBAR - Assessment:</label>
                                    <small id="emailHelp" class="form-text text-white">(Qualitative/Quantitative, ROI)</small>
                                    <textarea class="form-control" name="sbar_assessment" id="question_notes_id" rows="3">{{$item->sbar_assessment}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="question_notes_id">SBAR - Recommendation:</label>
                                    <small id="emailHelp" class="form-text text-white">(Qualitative/Quantitative, ROI)</small>
                                    <textarea class="form-control" name="sbar_recommendation" id="question_notes_id" rows="3">{{$item->sbar_recommendation}}</textarea>
                                </div>
                                <hr>
                                <input type="Submit" class="btn btn-primary btn-block" value="Update Sbar">
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
                <div id="files" class="tab-pane fade" role="tabpanel">
                    <br>
                    <div class="card-deck">
                        <div class="card bg-secondary text-white">
                            <div class="card-header">
                                <b>Attach Files - {{$item->item_description}}</b>
                                    <i>Attach files such as quotes, contracts or other relevant documents for this item. </i>
                            </div>
                            <div class="card-body">
                                <form action="/cr/detail/file_upload" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <input type="hidden" value="{{$item->id}}" name="item_id">
                                    <input type="hidden" value="{{$item->cost_center}}" name="cost_center">

                                    <div class="form-group">
                                        <label for="title_id">Select File:</label>
                                        <small id="emailHelp" class="form-text text-white">Attach files such as quotes.</small>
                                        <input type="file" name="file_name" class="form-control" id="title_id">
                                    </div>
                                    <div class="form-group">
                                        <label for="question_notes_id">File Description:</label>
                                        <small id="emailHelp" class="form-text text-white">(Describe what this file contains.)</small>
                                        <textarea class="form-control" name="file_description" id="question_notes_id" rows="3" required></textarea>
                                    </div>
                                    <hr>
                                    <input type="Submit" class="btn btn-primary btn-block" value="Upload File">
                                </form>
                                
                            </div>
                        </div>

                        <div class="card bg-secondary text-white">
                            <div class="card-header">
                                Current Files
                            </div>
                            <div class="card-body">

                                @if($files->isEmpty())
                                    <div class="alert alert-info">No Files Attached</div>
                                @else
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <td><b>File Name</b></td>
                                                <td><b>Description</b></td>
                                                <td><b>Type</b></td>
                                                <td><b>Size</b></td>
                                                <td><b>Added By</b></td>
                                                <td><b>Date</b></td>
                                                <td></td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($files as $file)
                                            <tr>
                                                <td>{{$file->old_filename}}</td>
                                                <td>{{$file->file_description}}</td>
                                                <td>{{$file->file_mime}}</td>
                                                <td>{{$file->file_size}}</td>
                                                <td>{{$file->added_by->first_name}} {{$file->added_by->last_name}}</td>
                                                <td>{{$file->created_at}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                @endif

                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>

@endsection
