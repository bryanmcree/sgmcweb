<div class="modal fade AddCostCenter" id="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">
                <button type="button" class="close btn-sgmc" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-file-o" aria-hidden="true"></i> <b>Add Cost Center</b></h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/pc/addcostcenter">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label for="cost_center_id">Cost Center:</label>
                                        <input type="text" class="form-control" id="cost_center_id" maxlength="4" minlength="4" name="cost_center" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="job_code_id">Cost Center Description:</label>
                                        <input type="text" class="form-control" id="cost_center_description_id" name="cost_center_description" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <div class="RightLeft">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-sgmc" value="Create Cost Center">
                </div>
            </div>
            </form>
        </div>
    </div>
</div>