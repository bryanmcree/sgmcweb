<div class="modal fade " id="AssignShift_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">
                <button type="button" class="close btn-sgmc" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-file-o" aria-hidden="true"></i> <b>Assign Shift</b></h4>
            </div>
            <div class="modal-body">
                <p>Assign selected employee to the below shift.</p>
                <form method="post" action="/pc/assignshift">
                    <input type="hidden" name="position_id" id="position_id" class="position_id" >
                    <input type="hidden" name="cost_center" id="cost_center" class="cost_center" >
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label for="shift_id">Select Shift:</label>
                                        <select class="form-control" name="shift_id">
                                            <option selected>[Select Shift]</option>
                                            @foreach ($shifts as $shift)
                                            <option value="{{$shift->id}}">{{$shift->shift_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <div class="RightLeft">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-sgmc" value="Assign to Shift">
                </div>
            </div>
            </form>
        </div>
    </div>
</div>