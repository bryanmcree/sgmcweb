<div class="modal fade" id="EditShift_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">
                <button type="button" class="close btn-sgmc" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-file-o" aria-hidden="true"></i> <b>Edit Shift</b></h4>
            </div>
            <div class="modal-body">
                <p>Create a unique name for this shift.</p>
                <form method="post" action="/pc/editshift">
                    <input type="hidden" name="id" class="id" value="">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label for="shift_id">Shift Name:</label>
                                        <input type="text" class="form-control shift_name" id="shift_id" name="shift_name" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <div class="RightLeft">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-sgmc" value="Update Shift">
                </div>
            </div>
            </form>
        </div>
    </div>
</div>