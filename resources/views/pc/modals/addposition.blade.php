<div class="modal fade AddPosition" id="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">
                <button type="button" class="close btn-sgmc" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-file-o" aria-hidden="true"></i> <b>Create Position</b></h4>
            </div>
            <div class="modal-body">
                <p>Directions on how to enter a new position.</p>
                <form method="post" action="/pc/addposition">
                    {{ csrf_field() }}
                    <input type="hidden" name="position_status" value="Vacant">
                    <input type="hidden" name="no_count" value="1">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                @include("pc.menu_includes.position")
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="RightLeft">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-sgmc" value="Create Position">
                </div>
            </div>
            </form>
        </div>
    </div>
</div>