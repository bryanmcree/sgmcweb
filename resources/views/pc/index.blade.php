{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Position Control') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-8">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <b>Position Control</b>
            </div>
            <div class="panel-body">

                @if ( Auth::user()->hasRole('Position Control Admin'))
                <a href="#" class="btn btn-primary btn-xs" data-target=".AddCostCenter" data-toggle="modal">Add Cost Center</a>
                <a href="#" class="btn btn-primary btn-xs" data-target=".AddPosition" data-toggle="modal">Create New Position</a>
                @endif

                <p>First step is to select the cost center or area you want to control.  Below will be a list of the available cost centers to work with.
                As we add new cost centers they will show up in the searchable table below.  For position control administrator, a number of buttons will
                appear throughout the application allowing that person to make changes to positions, add positions, and modify FTE's.</p>

                @if($positions->isEmpty())
                    <div class="alert alert-info"><h3>No Positions Listed</h3></div>
                @else
                    <div class="col-md-8 col-lg-offset-2">
                        <table class="table table-hover" id="costcenters">
                            <thead>
                            <tr>
                                <td>Cost Center</td>
                                <td class="no-sort"></td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($positions as $position)
                                <tr>
                                    <td style="vertical-align : middle;">@if ( Auth::user()->hasRole('Position Control Admin'))<a href="#" class="EditCostCenter" data-id="{{$position->id}}"
                                                                            data-cost_center="{{$position->cost_center}}"
                                                                            data-cost_center_description="{{$position->cost_center_description}}">
                                            <i class="fa fa-pencil-square-o" title='Edit Cost Center' aria-hidden="true"></i></a>@endif {{$position->cost_center}} - {{$position->cost_center_description}}</td>
                                    <td style="vertical-align : middle;" align="right"><a href="/pc/{{$position->cost_center}}" class="btn btn-sm btn-primary btn-xs">Select</a> </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <b>Position Search</b>
            </div>
            <div class="panel-body">

                <div class="form-group">
                    <label for="cost_center_id">Cost Center:</label>
                    <input type="text" class="form-control" id="cost_center_id" maxlength="4" minlength="4" name="cost_center" required>
                </div>
                <div class="form-group">
                    <label for="job_code_id">Job Code:</label>
                    <input type="text" class="form-control" id="job_code_id" maxlength="4" minlength="3" name="job_code" required>
                </div>
                <div class="form-group">
                    <label for="status_id">Status:</label>
                    <select class="form-control status" name="status" id="status_id" required>
                        <option selected>[Select Status]</option>
                        <option value="Filled">Filled</option>
                        <option value="Vacant">Vacant</option>
                        <option value="All">All</option>
                    </select>
                </div>
                <input type="submit" class="btn btn-primary btn-block btn-xs">

            </div>
        </div>
    </div>


    @include("pc.modals.addposition")
    @include("pc.modals.addcostcenter")
    @include("pc.modals.editcostcenter")

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">
    $(document).ready(function() {
        $('#costcenters').DataTable({
            "autoWidth": false,
            "columnDefs": [
                { targets: 'no-sort', orderable: false }
            ],
        });
    } );


    //Edit Cost Center
    $(document).on("click", ".EditCostCenter", function () {
        // alert($(this).attr("data-cost_center"));

        $('.id').val($(this).attr("data-id"));
        $('.cost_center').val($(this).attr("data-cost_center"));
        $('.cost_center_description').val($(this).attr("data-cost_center_description"));
        $('#EditCostCenter_modal').modal('show');
        //alert($(this).attr("data-id"));
    });
</script>

@endsection
@endif