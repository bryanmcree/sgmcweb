<div class="row">
    <div class="col-md-3">
        <div class="form-group">
            <label for="cost_center_id">Cost Center:</label>
            <input type="text" class="form-control cost_center" id="cost_center_id" maxlength="4" minlength="4" name="cost_center" required>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="job_code_id">Job Code:</label>
            <input type="text" class="form-control job_code" id="job_code_id" maxlength="4" minlength="3" name="job_code" required>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="status_id">Status:</label>
            <select class="form-control status" name="status" id="status_id" required>
                <option value="Active" selected>Active</option>
                <option value="Contract">Contract</option>
                <option value="PRN">PRN</option>
            </select>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="position_type_id">Position Type:</label>
            <select class="form-control position_type" name="position_type" id="position_type_id" required>
                <option selected>[Select Position Type]</option>
                <option value="N/A">N/A</option>
                <option value="HUC I" >HUC I</option>
                <option value="HUC II" >HUC II</option>
                <option value="PCC" >PCC</option>
                <option value="PCT I" >PCT I</option>
                <option value="PCT II" >PCT II</option>
                <option value="PCT III" >PCT III</option>
            </select>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-3">
        <div class="form-group">
            <label for="shift_id">Shift:</label>
            <select class="form-control shift" name="shift" id="shift_id" required>
                <option selected>[Select Shift]</option>
                <option value="Day">Day</option>
                <option value="Evening">Evening</option>
                <option value="Night">Night</option>
                <option value="Varied">Varied</option>
            </select>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="classification_id">Classification:</label>
            <select class="form-control classification" name="classification" id="classification_id" required>
                <option selected>[Select Classification]</option>
                <option value="Hourly/Non-Exempt">Hourly/Non-Exempt</option>
                <option value="Salaried/Exempt">Salaried/Exempt</option>
            </select>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="lic_type_id">License Type:</label>
            <select class="form-control lic_type" name="lic_type" id="lic_type_id" required>
                <option selected>[Select License Type]</option>
                <option value="N/A">N/A</option>
                <option value="RN">RN</option>
                <option value="LPN">LPN</option>
            </select>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="fte_id">FTE:</label>
            <input type="number" class="form-control fte" id="fte_id" step="0.1" min="0" max="1" name="fte" required>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-3">
        <div class="form-group">
            <label for="no_count_id">Counted Position:</label>
            <select class="form-control no_count" name="no_count" id="no_count_id" required>
                <option selected>[Select License Type]</option>
                <option value="1" selected>Yes</option>
                <option value="0">No</option>
            </select>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="traveler_id">Traveler?:</label>
            <select class="form-control traveler" name="traveler" id="traveler_id" required>
                <option value="0" selected>No</option>
                <option value="1">Yes</option>
            </select>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="ndnqi_id">NDNQI?:</label>
            <select class="form-control ndnqi" name="ndnqi" id="ndnqi_id" required>
                <option value="0" >No</option>
                <option value="1" selected>Yes</option>
            </select>
        </div>
    </div>
    <div class="col-md-3">

    </div>
</div>









