@if(!is_null($position->employee_number))
    <a href="/pc/remove/{{$position->id}}"><i class="fa fa-user-times text-danger" title="Remove Person from Position" aria-hidden="true"></i></a>
@endif
<a href="/pc/assign/{{$position->cost_center}}"><i class="fa fa-user text-success" title="Assign Person to Position" aria-hidden="true"></i></a>
<a href="#" class="AssignShift" title="Assign Position to Shift"
   data-position_id="{{$position->id}}"
   data-cost_center="{{$cost_center->cost_center}}"
><i class="fa fa-clock-o text-primary" aria-hidden="true"></i></a>
<i class="fa fa-pencil-square-o" title="Edit Position" aria-hidden="true"></i>