{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Position Control') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <b>Assign Employee</b>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <b>Select Employee</b>
                            </div>
                            <div class="panel-body">
                                @if (isset($success))
                                    <div class="alert alert-info alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        {!! $success !!}.
                                    </div>
                                @endif
                                <form method="post" action="/pc/assign/emp">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="director" id="director">
                                    <input type="hidden" name="cost_center" value="{{$cost_center}}">
                                    <div class="input-group">
                                        <input type="search" id="autocomplete" class="form-control input-sm" placeholder="Search" required>
                                        <div class="input-group-btn">
                                            <button class="btn btn-default btn-sm" type="submit">
                                                Select
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                @if(!is_null($employee))

                    <form method="post" action="/pc/assign/emp/assign">
                        {{ csrf_field() }}
                        <input type="hidden" name="cost_center" value="{{$cost_center}}">
                <div class="row">

                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                @if(!is_null($employee->employee_number))<a href="data:image/jpeg;base64,{{str_replace('</photo>','',str_replace('<photo>','',$employee->photo))}}" target="_blank"><img src="data:image/jpeg;base64,{{str_replace('</photo>','',str_replace('<photo>','',$employee->photo))}}" height="60" width="60"/></a>@else {{ Html::image('img/nopic.png', 'alt', array( 'width' => 60, 'height' => 60 )) }} @endif<b><P>{{$employee->name}}</P></b>
                            </div>
                            <input type="hidden" value="{{$employee->employee_number}}" name="employee_number">
                            <div class="panel-body">
                                <div class="col-md-3">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <b>Employee Current Position Status</b>
                                        </div>
                                        <div class="panel-body">
                                            @if(is_null($position))
                                                <div class="alert alert-warning">This position is currently vacant.</div>
                                            @else
                                                <b>Cost Center</b>: {{$position->cost_center}} - {{$position->description->cost_center_description}}<br>
                                                <b>Position ID</b>: {{$position->cost_center}}-{{$position->job_code}}-0{{$position->position_number}}<br>
                                                <b>Shift</b>: {{$position->shift}}<br>
                                                <b>FTE</b>: {{$position->fte}}<br>
                                                <b>Classification</b>: {{$position->classification}}
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <b>Modification and Reason</b>
                                        </div>
                                        <div class="panel-body">
                                            @if(is_null($position))

                                                <div class="form-group">
                                                    <select class="form-control" name="modification" id="modification_id" required>
                                                        <option selected>[Select Modification]</option>
                                                        <option value="New Employee">New Employee</option>
                                                        <option value="Internal Transfer">Transfer</option>
                                                        <option value="Internal Transfer">Contractor</option>
                                                        <option value="Internal Transfer">Travelers</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="reason_id">Reason:</label>
                                                    <textarea class="form-control" id="reason_id" name="reason" rows="5"></textarea>
                                                </div>


                                            @else
                                                Selection box for terminate / transfer
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <b>Employee New Position</b>
                                        </div>
                                        <div class="panel-body">
                                            <table class="table table-hover" id="positions">
                                                <tbody>
                                                @foreach($positions as $position)
                                                    <tr>
                                                        <td>@if(!is_null($position->employee_number))<a href="data:image/jpeg;base64,{{str_replace('</photo>','',str_replace('<photo>','',$position->employee->photo))}}" target="_blank"><img src="data:image/jpeg;base64,{{str_replace('</photo>','',str_replace('<photo>','',$position->employee->photo))}}" height="60" width="60"/></a>@else {{ Html::image('img/nopic.png', 'alt', array( 'width' => 60, 'height' => 60 )) }} @endif</td>
                                                        <td>@if(is_null($position->employee_number)) VACANT  @else {{$position->employee->name}} @endif <br>
                                                            {{$position->cost_center}}-{{$position->job_code}}-0{{$position->position_number}} <br>
                                                            {{$position->description->cost_center_description}}
                                                        </td>
                                                        <td>{{$position->shift}}</td>
                                                        <td>{{number_format($position->fte,1)}}</td>
                                                        <td width="10%" align="right">
                                                            <input type="radio" name="position" class="form-control" value="{{$position->id}}">
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
            <div class="panel-footer">
                <input type="submit" class="btn btn-block btn-primary" value="Assign {{$employee->name}}">
            </div>
        </form>
            @endif
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">

    $('#autocomplete').autocomplete({
        serviceUrl: '/scorecard/employeesearch',
        dataType: 'json',
        type:'GET',
        width: 418,
        minChars:2,
        onSelect: function(suggestion) {
            //alert(suggestion.data);
            $("#director").val(suggestion.data);
        }


    });

</script>

@endsection
@endif