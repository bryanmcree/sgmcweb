{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Position Control') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')
<div id="wrapper">
        <div id="sidebar-wrapper">
            <div class="panel panel-default">
                <div class="panel panel-heading">
                    <a href="/pc"><i class="fa fa-exchange" title="Return to List of Cost Centers" aria-hidden="true"></i></a> <b>{{$cost_center->cost_center_description}}</b> <a href="/pc/pm/{{$cost_center->cost_center}}" class="pull-right"><i class="fa fa-users" title="Position Manager" aria-hidden="true"></i></a>
                </div>
                <div class="panel-body">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#home">Full Time ({{$positions->sum('fte')}})</a></li>
                        <li><a data-toggle="tab" href="#menu1">PRN's ({{$positions_prn->sum('fte')}})</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="home" class="tab-pane fade in active">
                            <br>
                            <table class="table table-hover " id="avapos">
                                <thead>
                                <tr>
                                    <td class="no-sort"></td>
                                    <td class="no-sort"></td>
                                    <td class="no-sort"></td>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($positions as $position)
                                    <tr>
                                        <td>@if(!is_null($position->employee_number) AND $position->employee->photo !='')<a href="data:image/jpeg;base64,{{str_replace('</photo>','',str_replace('<photo>','',$position->employee->photo))}}" target="_blank"><img src="data:image/jpeg;base64,{{str_replace('</photo>','',str_replace('<photo>','',$position->employee->photo))}}" height="40" width="40"/></a>@else {{ Html::image('img/nopic.png', 'alt', array( 'width' => 40, 'height' => 40 )) }} @endif</td>
                                        <td>@if(is_null($position->employee_number)) {{$position->position_status}}  @else {{$position->employee->name}} @endif @if($position->lic_type != 'N/A'), {{$position->lic_type}} @endif <br>
                                            {{$position->cost_center}}-{{$position->job_code}}-0{{$position->position_number}}
                                            @if($position->no_count == 0)<i class="fa fa-ban text-danger" title="Position Not Counted" aria-hidden="true"></i>@endif
                                            @if($position->traveler == 1)<i class="fa fa-plane text-success" title="Traveler" aria-hidden="true"></i>@endif
                                        </td>
                                        <td align="right">
                                            @include("pc.menu_includes.allpositions")
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div id="menu1" class="tab-pane fade">
                            <br>
                            @if($positions_prn->isEmpty())
                                <div class="alert alert-info">No PRN Positions</div>
                            @else
                                <table class="table table-hover" id="avapos_prn">
                                    <thead>
                                    <tr>
                                        <td class="no-sort"></td>
                                        <td class="no-sort"></td>
                                        <td class="no-sort"></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($positions_prn as $position)
                                        <tr>
                                            <td>@if(!is_null($position->employee_number) AND $position->employee->photo !='')<a href="data:image/jpeg;base64,{{str_replace('</photo>','',str_replace('<photo>','',$position->employee->photo))}}" target="_blank"><img src="data:image/jpeg;base64,{{str_replace('</photo>','',str_replace('<photo>','',$position->employee->photo))}}" height="40" width="40"/></a>@else {{ Html::image('img/nopic.png', 'alt', array( 'width' => 40, 'height' => 40 )) }} @endif</td>
                                            <td>@if(is_null($position->employee_number)) {{$position->position_status}}  @else {{$position->employee->name}} @endif @if($position->lic_type != 'N/A'), {{$position->lic_type}} @endif <br>
                                                {{$position->cost_center}}-{{$position->job_code}}-0{{$position->position_number}}
                                                @if($position->no_count == 0)<i class="fa fa-ban text-danger" title="Position Not Counted" aria-hidden="true"></i>@endif
                                                @if($position->traveler == 1)<i class="fa fa-plane text-success" title="Traveler" aria-hidden="true"></i>@endif
                                            </td>
                                            <td align="right">
                                                @include("pc.menu_includes.allpositions")
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>


    <div id="page-content-wrapper">
        <div class="container-fluid">


            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="/pc"><i class="fa fa-exchange" title="Return to List of Cost Centers" aria-hidden="true"></i></a> <b>{{$cost_center->cost_center_description}}</b> <a data-toggle="collapse" href="#Stats"><i class="fa fa-line-chart" aria-hidden="true"></i></a>
                    </div>
                    <div id="Stats" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="col-md-1">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <b>Total Emp</b>
                                    </div>
                                    <div class="panel-body">
                                        <p align="center">{{$positions_shift->count()}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <b>Total RN's</b>
                                    </div>
                                    <div class="panel-body">
                                        <p align="center">{{$positions_shift->where('lic_type','RN')->count()}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <b>Total LPN's</b>
                                    </div>
                                    <div class="panel-body">
                                        <p align="center">{{$positions_shift->where('lic_type','LPN')->count()}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <b>Total PCC's</b>
                                    </div>
                                    <div class="panel-body">
                                        <p align="center">{{$positions_shift->where('lic_type','PCC')->count()}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <b>Total Filled</b>
                                    </div>
                                    <div class="panel-body">
                                        <p align="center">{{$positions_shift->where('position_status','Filled')->count()}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <b>Total Vacant</b>
                                    </div>
                                    <div class="panel-body">
                                        <p align="center">{{$positions_shift->where('position_status','Vacant')->count()}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <b>% Vacant</b>
                                    </div>
                                    <div class="panel-body">
                                        <p align="center">@if($positions_shift->count() == 0) 0 @ELSE{{number_format((float)($positions_shift->count() / $positions_shift->where('position_status','Vacant')->count()),2,'.','') }}% @endif</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel panel-heading">
                        <a href="#menu-toggle" class="" id="menu-toggle"><i class="fa fa-bars" title="Toggle Positions" aria-hidden="true"></i></a>  <b>Shifts</b> <a href="#" class=" pull-right" data-target=".AddShift" data-toggle="modal"><i class="fa fa-plus-square" title="Add Shift" aria-hidden="true"></i></a>
                    </div>
                    <div class="panel-body">
                        @if($shifts->isEmpty())
                            <div class="alert alert-info">There are no shifts for this cost center.</div>
                        @else


                        @foreach($shifts as $shift)
                            <div class="col-md-3">
                                <div class="panel panel-default">
                                    <div class="panel panel-heading">
                                        <a href="/pc/shift/delete/{{$shift->id}}"><i class="fa fa-times text-danger" title="Delete Shift" aria-hidden="true"></i></a> <b>{{$shift->shift_name}}</b> / FTE's ({{$positions_shift->where('shift_id', $shift->id)->sum('fte')}})  <a href="#" class=" pull-right  EditShift" data-id="{{$shift->id}}"
                                                                                                                                                                                  data-shift_name="{{$shift->shift_name}}"><i class="fa fa-pencil-square-o" title="Edit Shift" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="panel-body" style="height:265px; overflow-y: auto;">
                                        @if($positions_shift->where('shift_id', $shift->id)->isEmpty())
                                            <div class="alert alert-info">No Positions Assigned</div>
                                        @else
                                            <table class="table table-hover table-condensed">
                                                <tbody>
                                                @foreach($positions_shift->where('shift_id', $shift->id) as $position)
                                                    <tr>
                                                        <td>@if(!is_null($position->employee_number) AND $position->employee->photo !='')<a href="data:image/jpeg;base64,{{str_replace('</photo>','',str_replace('<photo>','',$position->employee->photo))}}" target="_blank"><img src="data:image/jpeg;base64,{{str_replace('</photo>','',str_replace('<photo>','',$position->employee->photo))}}" height="40" width="40"/></a>@else {{ Html::image('img/nopic.png', 'alt', array( 'width' => 40, 'height' => 40 )) }} @endif</td>
                                                        <td><td>@if(is_null($position->employee_number)) {{$position->position_status}}  @else {{$position->employee->name}} @endif @if($position->lic_type != 'N/A'), {{$position->lic_type}} @endif <br>
                                                            {{$position->cost_center}}-{{$position->job_code}}-0{{$position->position_number}}
                                                            @if($position->no_count == 0)<i class="fa fa-ban text-danger" title="Position Not Counted" aria-hidden="true"></i>@endif
                                                            @if($position->traveler == 1)<i class="fa fa-plane text-success" title="Traveler" aria-hidden="true"></i>@endif
                                                        </td>
                                                        <td align="right"><a href="#" class="AssignShift"
                                                                             data-position_id="{{$position->id}}"
                                                                             data-cost_center="{{$cost_center->cost_center}}"
                                                            ><i class="fa fa-suitcase" title="Move Position to New Shift" aria-hidden="true"></i></a><br>
                                                            <a href="/pc/shift/remove/{{$position->id}}"><i class="fa fa-times text-danger" title="Remove Position from Shift" aria-hidden="true"></i></a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                            @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



    @include("pc.modals.addshift")
@include("pc.modals.editshift")
    @include("pc.modals.assignshift")

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">

    window.onload = function() {
        $("#wrapper").toggleClass("toggled");
    };

    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");

    });




        $(document).ready(function() {
            $('#avapos').DataTable({
                "autoWidth": false,
                "columnDefs": [
                    { targets: 'no-sort', orderable: false }
                ],

            });
        } );

        $(document).ready(function() {
            $('#avapos_prn').DataTable({
                "autoWidth": false,
                "columnDefs": [
                    { targets: 'no-sort', orderable: false }
                ],

            });
        } );


    //Edit Metric Value
    $(document).on("click", ".AssignShift", function () {
        // alert($(this).attr("data-cost_center"));

        $('.position_id').val($(this).attr("data-position_id"));
        $('.cost_center').val($(this).attr("data-cost_center"));
        $('#AssignShift_modal').modal('show');
        //alert($(this).attr("data-first_name"));
    });

        //Edit Metric Value
        $(document).on("click", ".EditShift", function () {
            // alert($(this).attr("data-cost_center"));

            $('.id').val($(this).attr("data-id"));
            $('.shift_name').val($(this).attr("data-shift_name"));
            $('#EditShift_modal').modal('show');
            //alert($(this).attr("data-id"));
        });

</script>

@endsection
@endif