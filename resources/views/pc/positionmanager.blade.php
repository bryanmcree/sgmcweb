{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Position Control') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <b>Position Manager</b> - {{$cost_center->cost_center_description}} <a href="/pc/{{$cost_center->cost_center}}" class="btn btn-primary pull-right btn-xs" >Return to View</a>
            </div>
            <div class="panel-body">
                <table class="table table-hover" id="positions">
                    <thead>
                        <tr>
                            <td><b>Photo</b></td>
                            <td><b>Summary</b></td>
                            <td><b>Job Code</b></td>
                            <td><b>Shift</b></td>
                            <td><b>FTE</b></td>
                            <td><b>Status</b></td>
                            <td><b>Classification</b></td>
                            <td><b>License</b></td>
                            <td><b>Type</b></td>
                            <td><b>Filled?</b></td>
                            <td><b>Counted?</b></td>
                            <td><b>NDNQI?</b></td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($positions as $position)
                        <tr>
                            <td>@if(!is_null($position->employee_number))<a href="data:image/jpeg;base64,{{str_replace('</photo>','',str_replace('<photo>','',$position->employee->photo))}}" target="_blank"><img src="data:image/jpeg;base64,{{str_replace('</photo>','',str_replace('<photo>','',$position->employee->photo))}}" height="60" width="60"/></a>@else {{ Html::image('img/nopic.png', 'alt', array( 'width' => 60, 'height' => 60 )) }} @endif</td>
                            <td style="vertical-align : middle;">
                                @if(is_null($position->employee_number)) {{$position->position_status}}  @else {{$position->employee->name}} @endif @if($position->lic_type != 'N/A'), {{$position->lic_type}} @endif <br>
                                {{$position->cost_center}}-{{$position->job_code}}-0{{$position->position_number}} <br>
                                {{$position->description->cost_center_description}}
                            </td>
                            <td style="vertical-align : middle;">{{$position->job_code}}</td>
                            <td style="vertical-align : middle;">{{$position->shift}}</td>
                            <td style="vertical-align : middle;">{{number_format($position->fte,1)}}</td>
                            <td style="vertical-align : middle;">{{$position->status}}</td>
                            <td style="vertical-align : middle;">{{$position->classification}}</td>
                            <td style="vertical-align : middle;">{{$position->lic_type}}</td>
                            <td style="vertical-align : middle;">{{$position->position_type}}</td>
                            <td style="vertical-align : middle;">{{$position->position_status}}</td>
                            <td style="vertical-align : middle;">{{$position->no_count}}</td>
                            <td style="vertical-align : middle;">{{$position->ndnqi}}</td>
                            <td style="vertical-align : middle;" align="right"><a href="#" class="EditPosition btn btn-sm btn-primary"
                                                                                  data-position_id="{{$position->id}}"
                                                                                  data-cost_center="{{$position->cost_center}}"
                                                                                  data-job_code="{{$position->job_code}}"
                                                                                  data-position_number="{{$position->position_number}}"
                                                                                  data-shift="{{$position->shift}}"
                                                                                  data-fte="{{$position->fte}}"
                                                                                  data-status="{{$position->status}}"
                                                                                  data-classification="{{$position->classification}}"
                                                                                  data-lic_type="{{$position->lic_type}}"
                                                                                  data-no_count="{{$position->no_count}}"
                                                                                  data-traveler="{{$position->traveler}}"
                                                                                  data-position_type="{{$position->position_type}}"
                                                                                  data-ndnqi="{{$position->ndnqi}}"
                                >Edit Position</a> <a href="/pc/pm/delete/{{$position->id}}?cost_center={{$cost_center->cost_center}}" class="btn-danger btn-sm btn">Delete</a> </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@include("pc.modals.editposition")
@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="application/javascript">


        $(document).ready(function() {
            $('#positions').DataTable({
                "autoWidth": true,
                "columnDefs": [
                    { targets: 'no-sort', orderable: false }
                ],
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                    if ( aData[8] == "Vacant" )
                    {
                        $('td', nRow).css('background-color', '#ffc9c9');
                    }
                }
            });
        } );


        //Edit Metric Value
        $(document).on("click", ".EditPosition", function () {
             //alert($(this).attr("data-no_count"));

            $('.position_id').val($(this).attr("data-position_id"));
            $('.cost_center').val($(this).attr("data-cost_center"));
            $('.job_code').val($(this).attr("data-job_code"));
            $('.position_number').val($(this).attr("data-position_number"));
            $('.shift').val($(this).attr("data-shift"));
            $('.classification').val($(this).attr("data-classification"));
            $('.lic_type').val($(this).attr("data-lic_type"));
            $('.fte').val($(this).attr("data-fte"));
            $('.no_count').val($(this).attr("data-no_count"));
            $('.status').val($(this).attr("data-status"));
            $('.traveler').val($(this).attr("data-traveler"));
            $('.position_type').val($(this).attr("data-position_type"));
            $('.ndnqi').val($(this).attr("data-ndnqi"));
            $('#EditPosition_modal').modal('show');
            //alert($(this).attr("data-first_name"));
        });

    </script>

@endsection
@endif