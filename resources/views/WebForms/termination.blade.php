
@if ( Auth::user()->hasRole('WebForms') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.app')
@section('content')


    <div class="col-md-3">
        <div class="panel panel-default">
            <div class="panel-heading-custom">
                <div class="panel-title">Date Range</div>
                <div class="panel-sub-title">Test Sub Title</div>
            </div>
            <div class="panel-body">
                BODY
            </div>
            <div class="panel-footer">Footer</div>
        </div>
    </div>

@endsection
@section('scripts')



@endsection
@endif