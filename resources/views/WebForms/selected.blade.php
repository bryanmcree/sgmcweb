<div class="col-md-12">
    <div class="card bg-dark text-white mb-3">
        <div class="card-body">
             <div class="col-md-3">
                 <table>
                     <tr>
                         <td>@if($user->photo != '')<img src="data:image/jpeg;base64,{{str_replace('</photo>','',str_replace('<photo>','',$user->photo))}}" height="120" width="120"/>@else {{ Html::image('img/nopic.png', 'alt', array( 'width' => 60, 'height' => 60 )) }} @endif</td>
                         <td>
                         <td>
                             <b>Employee: </b>{{$user->first_name}} {{$user->last_name}} <br>
                             <b>Title: </b> {{$user->title}}<br>
                             <b>Employee #: </b> {{$user->employee_number}}<br>
                             <b>Cost Center: </b>{{$user->unit_code}}<br>
                             <b>Username: </b> {{$user->username}}
                         </td>
                     </tr>
                 </table>
             </div>
            {!! Form::open(array('action' => ['WebFormsController@addTerm'], 'class' => 'form_control')) !!}
            {!! Form::hidden('emp_number', $user->employee_number) !!}
            {!! Form::hidden('last_name', $user->last_name) !!}
            {!! Form::hidden('first_name', $user->first_name) !!}
            {!! Form::hidden('ssn', $user->ssn) !!}
            {!! Form::hidden('birthdate', $user->dob) !!}
            {!! Form::hidden('hire_date', $user->hire_date) !!}
            {!! Form::hidden('cost_center', $user->unit_code) !!}
            {!! Form::hidden('cost_center_description', $user->unit_code_description) !!}
            {!! Form::hidden('job_code', $user->job_code) !!}
            {!! Form::hidden('title', $user->title) !!}
            {!! Form::hidden('termination_code', 'T' )!!}
            {!! Form::hidden('race', $user->race) !!}
            {!! Form::hidden('gender', $user->gender) !!}
            {!! Form::hidden('shift', $user->shift) !!}
            {!! Form::hidden('WebForm', 'Yes') !!}
            {!! Form::hidden('notice_by', Auth::user()->employee_number) !!}
            <div class="col-md-3">
                <div class="form">
                    {!! Form::label('last_day', 'Termination Date:') !!}
                    {!! Form::input('date','last_day', '', ['class' => 'form-control','required']) !!}
                    {!! Form::label('forward_to', 'Forward Email To:') !!}
                    {!! Form::text('forward_to', null, ['class' => 'form-control', 'id'=>'forward_to']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form">
                    {!! Form::checkbox('emergency_term', 'YES', null, ['class' => 'field', 'id'=>'emergency_term'])  !!}{!! Form::label('emergency_term', ' - Emergency Termination') !!}<br>
                    {!! Form::checkbox('keep_legal', 'YES', null, ['class' => 'field', 'id'=>'keep_legal'])  !!}{!! Form::label('keep_legal', ' - Legal Necessity to Maintain Active Directory') !!}<br>
                    {!! Form::checkbox('access_email', 'YES', null, ['class' => 'field', 'id'=>'access_email'])  !!}{!! Form::label('access_email', ' - I need access to their email') !!}<br>
                    {!! Form::checkbox('u_drive', 'YES', null, ['class' => 'field', 'id'=>'u_drive'])  !!}{!! Form::label('u_drive', ' - I need access to their U drive') !!}<br>
                    {!! Form::checkbox('equipment_return', 'YES', null, ['class' => 'field', 'id'=>'equipment_return'])  !!}{!! Form::label('equipment_return', ' - Employee has SGMC equipment to return') !!}
                </div>
            </div>
            <div class="col-md-3" style="vertical-align:middle;">
                All computer access for SGMC systems will be turned off the day after the termination date at 8AM.  You can edit this date or delete this request up until that point.
                Emergency Termination will revoke access immediately despite the entered termination date.

                {!! Form::submit('Terminate access for '. $user->first_name.' '. $user->last_name .' on above date?', ['class'=>'btn btn-sgmc btn-block', 'id'=>'updateButton']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>


