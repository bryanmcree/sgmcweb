
@if ( Auth::user()->hasRole('WebForms') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.app')
@section('content')


    {!! Form::open(array('action' => ['WebFormsController@search'], 'class' => 'form_control')) !!}
    <div class="col-md-12">
        <div class="card text-white bg-dark mb-3">
            <div class="card-header">
                <h4>Employee Search</h4>
            </div>
            <div class="card-body">

                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"><i class="fa fa-search input-sm"></i></span>
                    </div>
                    <input class="form-control input-sm" type="text"  name="search" placeholder="Employee Search.."/>
                </div>

                </form>
            </div>
            <div class="card-footer">
                <div class="RightLeft">
                    {!! Form::submit('Find Employee', ['class'=>'btn btn-default']) !!}
                    {!! Form::reset('Clear', ['class'=>'btn btn-default']) !!}
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>


    <div class="col-md-12">
        <div class="card  bg-dark mb-3">
            <div class="card-header">
                <h4 class="text-white"><b>Employees ({{$tests->count()}})</b></h4>
            </div>
            <div class="card-body">
                @if ($tests->isEmpty())
                    <div class="container-fluid">

                        <div class="alert alert-danger text-white" role="alert"><b>No employees to display.</b> </div>
                        @else

                            <table class="table table-condensed table-responsive" id="employees11">

                                <thead class="text-white">
                                <tr>
                                    <td></td>
                                    <td class=""><b>Name</b></td>
                                    <td class=""><b>Emp #</b></td>
                                    <td class=""><b>UserName</b></td>
                                    <td class=""><b>Status</b></td>
                                    <td class=""><b>Hire Date</b></td>
                                    <td class=""><b>Department</b></td>
                                    <td class="no-sort" align="center" style="vertical-align:middle"><b>Detail</b></td>

                                </tr>
                                </thead>


                                @foreach ($tests as $test)
                                    <tbody>
                                    <tr @if($test->emp_status == 'Terminated')bgcolor="#FAD2D2"@endif @if($test->emp_status == 'Contractor')bgcolor="#D2FAFA"@endif @if($test->emp_status == 'Active')bgcolor="#E6FAD2"@endif>
                                        <td style="vertical-align:middle; text-align:center;">@if($test->photo != '')<img src="data:image/jpeg;base64,{{str_replace('</photo>','',str_replace('<photo>','',$test->photo))}}" height="60" width="60"/>@else {{ Html::image('img/nopic.png', 'alt', array( 'width' => 60, 'height' => 60 )) }} @endif</td>
                                        <td style="vertical-align:middle;" class=""><b>{{$test->last_name}}, {{$test->first_name}}</b> <br> {{$test->title}}</td>
                                            <td style="vertical-align:middle;" class="">{{$test->employee_number}}</td>
                                            <td style="vertical-align:middle;" class="">{{$test->username}}</td>
                                            <td style="vertical-align:middle;" class="">{{$test->emp_status}}</td>
                                            <td style="vertical-align:middle;" class="">@if($test->hire_date != NULL){{$test->hire_date}}<br><i>({{\Carbon\Carbon::createFromTimeStamp(strtotime($test->hire_date))->diffForHumans()}})</i> @endif</td>
                                        <td style="vertical-align:middle;" class="">{{$test->unit_code}}<br>{{$test->unit_code_description}}</td>
                                        <td align="center" style="vertical-align:middle;" class=""><a href="#"
                                            data-id="{{$test->id}}"
                                            role="button" class="btn btn-sgmc btn-xs SelectEmployee"><span class="fa fa-btn fa-info-circle" aria-hidden="true"></span> Select</a></td>
                                    </tr>
                                    </tbody>
                                @endforeach
                                <tr>
                                    <tc colspan="5"><hr></tc>
                                </tr>
                            </table>
                        @endif
                    </div>
            </div>
        </div>




           <div class="" id="employee_detail">
           </div>

@endsection

@section('scripts')

<script type="application/javascript">

    $('.SelectEmployee').click(function(){
        //alert($(this).attr('data-id'));
        var id = $(this).attr('data-id');

        $.ajax({
            url:"select/"+id,
            method:"GET",
            cache:false,
            success:function(result){
                $("#employee_detail").hide().html(result).fadeIn();
            }});
    });

</script>

@endsection
@endif