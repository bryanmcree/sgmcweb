{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('WebForms') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel panel-heading panel-heading-custom">
            <h4><b>Doctor / Office Staff Information</b></h4>
            </div>
            <div class="panel-body">

                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('first_name', 'First Name:') !!}
                        <div class="input-group">
                            <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                            {!! Form::text('first_name', null, ['class' => 'form-control', 'id'=>'first_name', 'required']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('middle_name', 'Middle Name:') !!}
                        <div class="input-group">
                            <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                            {!! Form::text('middle_name', null, ['class' => 'form-control', 'id'=>'middle_name', 'required']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('last_name', 'Last Name:') !!}
                        <div class="input-group">
                            <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                            {!! Form::text('last_name', null, ['class' => 'form-control', 'id'=>'last_name', 'required']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('user_dob', 'User Date of Birth:') !!}
                        <div class="input-group">
                            <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                            {!! Form::input('Date','user_dob', '', ['class' => 'form-control user_dob','required']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('practice_name', 'Group / Practice Name:') !!}
                        <div class="input-group">
                            <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                            {!! Form::text('practice_name', null, ['class' => 'form-control', 'id'=>'practice_name', 'required']) !!}
                        </div>
                    </div>
                </div>


                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('main_phone', 'Main Phone:') !!}
                        <div class="input-group">
                            <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                            {!! Form::text('main_phone', null, ['class' => 'form-control ', 'id'=>'main_phone', 'required', 'maxlength'=>10, 'minlength'=>10, 'placeholder'=>'Area Code + Phone / Numbers Only']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('cell_phone', 'Cell Phone:') !!}
                        {!! Form::text('cell_phone', null, ['class' => 'form-control', 'id'=>'cell_phone', 'maxlength'=>10, 'minlength'=>10, 'placeholder'=>'Area Code + Phone / Numbers Only']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('fax_phone', 'Fax Phone:') !!}
                            {!! Form::text('fax_phone', null, ['class' => 'form-control', 'id'=>'fax_phone', 'maxlength'=>10, 'minlength'=>10, 'placeholder'=>'Area Code + Phone / Numbers Only']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('other_phone', 'Pager / Other Phone:') !!}
                            {!! Form::text('other_phone', null, ['class' => 'form-control', 'id'=>'other_phone', 'maxlength'=>10, 'minlength'=>10, 'placeholder'=>'Area Code + Phone / Numbers Only']) !!}
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('address', 'Address:') !!}
                        <div class="input-group">
                            <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                            {!! Form::text('address', null, ['class' => 'form-control', 'id'=>'address', 'required']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('address2', 'Address:') !!}
                        {!! Form::text('address2', null, ['class' => 'form-control', 'id'=>'address2']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('city', 'City:') !!}
                        <div class="input-group">
                            <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                            {!! Form::text('city', null, ['class' => 'form-control', 'id'=>'city', 'required']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        {!! Form::label('state', 'State:') !!}
                        <div class="input-group">
                            <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                        <select name="state" class="form-control">
                            <option selected value="GA">Georgia</option>
                            @foreach ($state as $states)
                                <option value="{{$states->code}}">{{$states->name}}</option>
                            @endforeach
                        </select>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel panel-heading  panel-heading-custom">
                <h4><b>Doctor Details</b></h4>
            </div>
            <div class="panel-body">
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('dr_number', 'Doctor Number:') !!}
                        {!! Form::text('dr_number', null, ['class' => 'form-control', 'id'=>'dr_number']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('specialty_primary', 'Specialty Primary:') !!}
                        {!! Form::text('specialty_primary', null, ['class' => 'form-control', 'id'=>'specialty_primary']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('dictation_number', 'Dictation Number:') !!}
                        {!! Form::text('dictation_number', null, ['class' => 'form-control', 'id'=>'dictation_number']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('dea_number', 'DEA Number:') !!}
                        {!! Form::text('dea_number', null, ['class' => 'form-control', 'id'=>'dea_number']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('npi_number', 'NPI Number:') !!}
                        {!! Form::text('npi_number', null, ['class' => 'form-control', 'id'=>'npi_number']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel panel-heading  panel-heading-custom">
                <h4><b>Office Manager Information</b></h4>
            </div>
            <div class="panel-body">
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('om_first_name', 'First Name:') !!}
                        <div class="input-group">
                            <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                            {!! Form::text('om_first_name', null, ['class' => 'form-control', 'id'=>'om_first_name', 'required']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('last_name', 'Last Name:') !!}
                        <div class="input-group">
                            <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                            {!! Form::text('last_name', null, ['class' => 'form-control', 'id'=>'last_name', 'required']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('om_phone', 'Office Phone:') !!}
                        <div class="input-group">
                            <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                            {!! Form::text('om_phone', null, ['class' => 'form-control', 'id'=>'om_phone', 'required', 'maxlength'=>10, 'minlength'=>10, 'placeholder'=>'Area Code + Phone / Numbers Only']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('om_email', 'Office Manager Email:') !!}
                        <div class="input-group">
                            <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                            {!! Form::text('om_email', null, ['class' => 'form-control', 'id'=>'om_email', 'required']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel panel-heading  panel-heading-custom">
                <h4><b>Document Attachment</b></h4>
            </div>
            <div class="panel-body">
                <div class="col-md-2">
                    <div class="form-group">
                    {!! Form::label('file_name', 'Select File:') !!}
                    {!! Form::file('file_name', array('class'=>'form-control')) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel panel-heading  panel-heading-custom">
                <h4><b>System List</b></h4>
            </div>
            <div class="panel-body">

                List of Software here

            </div>
        </div>
    </div>





@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">
    var wf_main_phone = document.getElementById('main_phone');
    wf_main_phone.onkeypress = function(){
        wf_main_phone.value = wf_main_phone.value.replace(/[^0-9+]/g, '');
    }

    var wf_cell_phone = document.getElementById('cell_phone');
    wf_cell_phone.onkeypress = function(){
        wf_cell_phone.value = wf_cell_phone.value.replace(/[^0-9+]/g, '');
    }

    var wf_fax_phone = document.getElementById('fax_phone');
    wf_fax_phone.onkeypress = function(){
        wf_fax_phone.value = wf_fax_phone.value.replace(/[^0-9+]/g, '');
    }

    var wf_other_phone = document.getElementById('other_phone');
    wf_other_phone.onkeypress = function(){
        wf_other_phone.value = wf_other_phone.value.replace(/[^0-9+]/g, '');
    }

    var om_phone = document.getElementById('om_phone');
    om_phone.onkeypress = function(){
        om_phone.value = om_phone.value.replace(/[^0-9+]/g, '');
    }

    $(document).ready(function() {
        oTable = $('#systems').DataTable(
                {
                    "info":     false,
                    "pageLength": 10,
                    "lengthChange": false,
                    "order": [],
                    "columnDefs": [
                        { targets: 'no-sort', orderable: false }
                    ],
                    "dom": '<l<t>ip>'

                }
        );
        $('#myInputTextField').keyup(function(){
            oTable.search($(this).val()).draw() ;
        })

    });

</script>

@endsection
@endif



