
@if ( Auth::user()->hasRole('WebForms') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.app')
@section('content')



    {!! Form::open(array('action' => ['WebFormsController@search'], 'class' => 'form_control')) !!}
    <div class="col-md-12">
        <div class="card text-white bg-dark mb-3">
            <div class="card-header">
                <h4>Employee Search</h4>
                <i>Search by name or employee number.</i>
            </div>
            <div class="card-body">

                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"><i class="fa fa-search input-sm"></i></span>
                    </div>
                    <input class="form-control input-sm" type="text"  name="search" placeholder="Employee Search.."/>
                </div>
                

                </form>
            </div>
            <div class="card-footer">
                <div class="RightLeft">
                    {!! Form::submit('Find Employee', ['class'=>'btn btn-default']) !!}
                    {!! Form::reset('Clear', ['class'=>'btn btn-default']) !!}
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>

@if (!$term->isEmpty())
    <div class="col-md-12">
        <div class="card text-white bg-dark">
            <div class="card-header">
                <h4>Pending Terminations ({{$term->count()}})</h4>
            </div>
            <div class="card-body">
                <table class="table table-responsive">
                    <thead>
                    <tr>
                        <td><b>Name</b></td>
                        <td><b>Employee #</b></td>
                        <td><b>Title</b></td>
                        <td><b>Cost Center</b></td>
                        <td><b>Last Day</b></td>
                        <td><B>Days</B></td>
                        <td><b>Revoke On</b></td>
                        <td></td>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($term as $terms)
                    <tr>
                        <td>{{$terms->last_name}}, {{$terms->first_name}}</td>
                        <td>{{$terms->emp_number}}</td>
                        <td>{{$terms->title}}</td>
                        <td>{{$terms->cost_center_description}}</td>
                        <td>{{$terms->last_day}}</td>
                        <td>{{\Carbon\Carbon::createFromTimeStamp(strtotime($terms->last_day))->diffInDays()}}</td>
                        <td>{{\Carbon\Carbon::createFromTimeStamp(strtotime($terms->last_day))->addDays(1)->addHours(8)}}</td>
                        <td align="right">
                            <div class="RightLeft">
                                <a href="#" class="btn btn-sgmc btn-xs EditTerm" style="display: inline-block;" data-last_day = {{$terms->last_day}} data-id = {{$terms->id}}><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                                <a href="#" class="btn btn-danger btn-xs DeleteTerm" style="display: inline-block;" data-id = {{$terms->id}}><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                            </div>
                        </td>

                    </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
@endif
@endsection
@include("WebForms.modals.editTerm")
@section('scripts')

<script type="application/javascript">
    $(document).on("click", ".EditTerm", function () {
        //alert($(this).attr("data-user_id"));
        $('.last_day').val($(this).attr("data-last_day"));
        $('.id').val($(this).attr("data-id"));
        $('#EditTerm').modal('show');
    });

    $(document).on("click", ".DeleteTerm", function () {
        //alert($(this).attr("data-id"))
        var id = $(this).attr("data-id");
        deleteContact(id);
    });

    function deleteContact(id) {
        swal({
            title: "Delete Record?",
            text: "Are you sure that you want to delete this termination?",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            confirmButtonText: "Yes, delete it!",
            confirmButtonColor: "#ec6c62"
        }, function() {
            $.ajax({
                url: "/WebForms/delete/" + id,
                type: "GET"
            })
                    .done(function(data) {
                        swal({
                                    title: "Deleted",
                                    text: "Termination was Deleted",
                                    type: "success",
                                    timer: 1800,
                                    showConfirmButton: false

                                }
                        );
                        setTimeout(function(){window.location.replace('/WebForms/')},1900);
                    })
                    .error(function(data) {
                        swal("Oops", "We couldn't connect to the server!", "error");
                    });
        });
    }
</script>

@endsection
@endif