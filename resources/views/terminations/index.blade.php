@extends('layouts.app')

@section('content')
    @barchart('Termination', 'term_div')


    <div class="col-md-2">
        <div class="panel panel-success">
            <div class="panel-heading">
                Select Range
            </div>
            <div class="panel-body" >
                {!! Form::open(array('url' => '/terminations', 'method' => 'GET', 'files' => true,'class' => 'form_control')) !!}
                    <div class="form-group">
                        {!! Form::label('quarter', 'Quarter:') !!}
                        {!!  Form::select('quarter', $quarters, null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('year', 'Year:') !!}
                        {!!  Form::select('year', $years, null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        <input type="submit" name="update_graph" class="btn btn-success" value="Update Graph">
                    </div>
                {!! Form::close() !!}

                @foreach($data2 as $newData)
                    {{$newData->description}} - {{$newData->code}} <br>
                @endforeach
            </div>
        </div>
    </div>
    <div class="col-md-10">
        <div class="panel panel-success">
            <div class="panel-heading"><i class="fa fa-question-circle"  data-toggle="popover" data-placement="top"
                                          title="Termination by Status"
                                          data-content="Termination reason totals for employees this quarter."></i> -><b> Termination by Status</b> -> <a href="/terminations/" ><span class="fa fa-bar-chart"></span></a></div>
            <div class="panel-body">
                <div id="term_div"></div>
            </div>
        </div>
    </div>

@endsection