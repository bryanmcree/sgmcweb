{{--New file Template--}}

{{--Add Security for this page below--}}
@if( Auth::user()->hasRole('') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')


    <div class="card mb-4 text-white bg-dark">
        <div class="card-header">
            Blank Template
        </div>
        <div class="card-body">
            Card Body
        </div>
    </div>


@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection

@endif