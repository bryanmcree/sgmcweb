@extends('layouts.app')

@section('content')

    <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading panel-heading-custom"><b>Login</b>-> web.SGMC.org</div>

                <ol class="list-group">
                    <li class="list-group-item">
                        <b>Welcome!</b><br>
                        Please use only Google Chrome to access web.sgmc.org.  Enter your SGMC Active Directory credentials below.
                    </li>
                </ol>

                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">User Name</label>

                            <div class="col-md-6">
                                <div class="input-group">
                                <div class="input-group-addon alert-danger"><i class="fa fa-user" aria-hidden="true"></i></div>
                                {!! Form::text('username', old('username'),array('class'=>'form-control', 'autofocus'=>'autofocus', 'placeholder'=>'User Name')) !!}
                                </div>
                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <div class="input-group">
                                <div class="input-group-addon alert-danger"><i class="fa fa-lock" aria-hidden="true"></i></div>
                                <input id="password" type="password" class="form-control" name="password" placeholder="Password">
                                </div>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-sgmc ">
                                    <i class="fa fa-btn fa-sign-in"></i> Login
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
</div>


@endsection

@section('scripts')

    <script type="text/javascript">

        $('#autocomplete').autocomplete({
            serviceUrl: '/ad',
            dataType: 'json',
            type:'GET',
            width: 418,
            minChars:2,
            lookupLimit:10,
            onSelect: function(suggestion) {
                //alert(suggestion.data);
                $("#manageremail").val(suggestion.data);
            }


        });

    </script>

@endsection
