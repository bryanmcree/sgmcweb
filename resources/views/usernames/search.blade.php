{{--New file Template--}}

{{--Add Security for this page below--}}

@extends('layouts.app')
{{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-6 col-md-offset-3">
        <div class="card bg-dark mb-3 text-white">
            <div class="card-header">
                <b>Search for User Names</b>
            </div>
            <div class="card-body">
                <form method="post" action="/usernames/search">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="user_name_id">Enter Desired User Name</label>
                        <input type="text" name="user_name" class="form-control" id="user_name_id" aria-describedby="usernamehelp" placeholder="Enter User Name" required autofocus>
                        <small id="usernamehelp" class="form-text text-muted">Only exact matches will be returned.</small>
                    </div>
                    <input type="submit" class="btn btn-primary" name="" value="Check Availability">
                </form>
            </div>
        </div>
    </div>


    <div class="col-md-6 col-md-offset-3">
        <div class="card bg-dark mb-3 text-white">
            <div class="card-header">
                <b>Results</b>
            </div>
            <div class="card-body">
                @if($usernames->isempty())
                    <div class="alert alert-success"><b>{{$searched_name}} was NOT found in Active Directory or API and is available.</b></div>
                @else
                    @foreach($usernames as $username)
                    <div class="alert alert-danger"><b>{{$username->UserName}} has been located in {{$username->Source}}.</b></div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
