{{--New file Template--}}

{{--Add Security for this page below--}}
@if( Auth::user()->hasRole('Educational Services') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

@section('content')


    <div class="card mb-4">
        <div class="card-body">
            <div class="card-title">
                Educational Attendance Roster Form
                <span class="float-right"> <a href="/education" class="btn btn-sm btn-secondary">Return to Dashboard</a> <a href="/education/print/{{$edu->id}}" class="btn btn-sm btn-info">Print View</a> </span>
            </div>
        <hr>

            <table class="table table-bordered">

                <tbody>

                    <tr>
                        <td>
                            {{ $roster->roster_name }}
                            <small class="form-text">Roster</small>
                        </td>
                    </tr>

                </tbody>

            </table>

            <hr class="bg-info">
        
            <table class="table table-bordered">

                <tbody>

                    <tr>
                        <td style="width:33.33%">
                            {{ $edu->title }} <hr>
                            <small class="form-text">Program Title</small>
                        </td>
                        <td style="width:33.33%">
                            {{ $edu->length }} <hr>
                            <small class="form-text">Program Length (Hours & Minutes)</small>
                        </td>
                        <td style="width:33.33%">
                            {{ Carbon::parse($edu->dates)->format('m-d-Y') }} <hr>
                            <small class="form-text">Date</small>
                        </td>
                    </tr>

                    <tr>
                        <td style="width:33.33%">
                            {{ $edu->contact_hours }} <hr>
                            <small class="form-text"># of Contact Hours</small>
                        </td>
                        <td style="width:33.33%">
                            {{ $edu->CEUs }} <hr>
                            <small class="form-text"># of CEUs</small>
                        </td>
                        <td style="width:33.33%">
                            {{ $edu->created_by }} <hr>
                            <small class="form-text">Name & Extension of Submitter</small>
                        </td>
                    </tr>

                </tbody>

            </table>

            <hr class="bg-info">

            <table class="table table-bordered">

                <tbody>

                    <tr>
                        <td style="width:33.33%">
                            {{ $edu->classification }} <hr>
                            <small class="form-text">Program Classification (check the one which best applies)</small>
                        </td>
                        <td style="width:33.33%">
                            @foreach ($reasons as $reason)
                                {{ $reason->reason }} <br>
                            @endforeach <hr>
                            <small class="form-text">Reason for Training (choose ALL that apply)</small>
                        </td>
                        <td style="width:33.33%">
                            @foreach ($methods as $method)
                                {{ $method->methodology }} <br>
                            @endforeach <hr>
                            <small class="form-text">Training Methodology (choose ALL that apply)</small>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="3">
                            {{ $edu->learning_objectives }} <hr>
                            <small class="form-text">Learning Objectives (What will participants know or be able to do as a result of this training?</small>
                        </td>
                    </tr>

                </tbody>

            </table>

            <hr class="bg-info">

            <table class="table table-bordered">

                <tbody>

                    @foreach($presenters as $present)
                        <tr>
                            <td style="width:25%">
                                {{ $present->presenter }} <hr>
                                <small class="form-text">Instructor/Presenter</small>
                            </td>
                            <td style="width:25%">
                                {{ $present->department }} <hr>
                                <small class="form-text">Department</small>
                            </td>
                            <td style="width:25%">
                                {{ $present->badge_num }} <hr>
                                <small class="form-text">Badge #</small>
                            </td>
                            <td style="width:25%">
                                {{ $present->telephone_num }} <hr>
                                <small class="form-text">Telephone #</small>
                            </td>
                        </tr>
                    @endforeach

                </tbody>

            </table>

            {{-- <hr class="bg-info">

            <table class="table table-bordered">

                <tbody>

                    <tr>
                        <td style="width:25%">
                            {{ $edu->scanned }} <hr>
                            <small class="form-text">Scanned?</small>
                        </td>
                        <td style="width:25%">
                            {{ $edu->api }} <hr>
                            <small class="form-text">API</small>
                        </td>
                        <td style="width:25%">
                            {{ $edu->right_start }} <hr>
                            <small class="form-text">Right Start</small>
                        </td>
                        <td style="width:25%">
                            {{ $edu->productivity_data }} <hr>
                            <small class="form-text">Productivity Data</small>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            @if(!empty($edu->api_code))
                                {{ $edu->api_code }}
                            @else
                                <span class="text-danger">No API Code given</span>
                            @endif <hr>
                            <small class="form-text">API Code</small>
                        </td>
                        <td>
                            {{ Carbon::parse($edu->sig_date)->format('m-d-Y') }} <hr>
                            <small class="form-text">Date</small>
                        </td>
                        <td>
                            {{ $edu->signature }} <hr>
                            <small class="form-text">Signature</small>
                        </td>
                    </tr>

                </tbody>

            </table> --}}

        </div>
    </div>


@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script>
    $(document).ready(function() {
        $(".multi-2").select2();
    });
</script>

@endsection

@endif