{{--New file Template--}}

{{--Add Security for this page below--}}
@if( Auth::user()->hasRole('Educational Services') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

@section('content')


    <div class="card mb-4 text-white bg-dark">
        <div class="card-header">
            Educational Attendance Roster Dashboard
        </div>
        <div class="card-body">
            
            <table class="table table-dark table-hover table-striped table-bordered">
                <thead>
                    <th>Title</th>
                    <th>Date</th>
                    <th>Created By</th>
                    <th>Details</th>
                    <th>Roster</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </thead>
                <tbody>
                    @foreach($forms as $form)
                        <tr>
                            <td> {{ $form->title }} </td>
                            <td> {{ Carbon::parse($form->dates)->format('m-d-Y') }} </td>
                            <td> {{ $form->created_by }} </td>
                            <td> <a href="/education/show/{{$form->id}}" class="btn btn-info btn-block">Details</a> </td>
                            <td> <a href="/roster/load/{{$form->roster_id}}" class="btn btn-dark btn-block" target="_blank">Load Roster</a> </td>
                            <td> <a href="/education/edit/{{$form->id}}" class="btn btn-warning btn-block">Edit</a> </td>
                            <td> <a href="#" form_id ="{{$form->id}}" class="btn btn-danger btn-block deleteForm">Delete</a> </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
        <div class="card-footer">
            <a href="/education/create" class="btn btn-primary">New Attendance Form</a>
        </div>
    </div>


@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script>
    $(document).ready(function() {
        $(".multi-2").select2();
    });
</script>

<script type="application/javascript">

    $(document).on("click", ".deleteForm", function () {
        //alert($(this).attr("data-cost_center"));
        var form_id = $(this).attr("form_id");
        DeleteForm(form_id);
    });
    
    function DeleteForm(form_id) {
        swal({
            title: "Delete Form?",
            text: "Deleting this Form cannot be undone.",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            confirmButtonText: "Yes, delete it!",
            confirmButtonColor: "#ec6c62"
        }, function() {
            $.ajax({
                url: "/education/destroy/" + form_id,
                type: "GET"
            })
                .done(function(data) {
                    swal({
                            title: "Deleted",
                            text: "Form Deleted",
                            type: "success",
                            timer: 1800,
                            showConfirmButton: false

                        });
                        setTimeout(function(){window.location.replace('/education')},1900);
                })
                .error(function(data) {
                    swal("Oops", "We couldn't connect to the server!", "error");
                });
        });
    }


</script>


@endsection

@endif