{{--New file Template--}}

{{--Add Security for this page below--}}
@if( Auth::user()->hasRole('Educational Services') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

@section('content')


    <div class="card mb-4 text-white bg-dark">
        <div class="card-body">
            <div class="card-title">
                Educational Attendance Roster Form
                <span class="float-right"> <a href="/education" class="btn btn-sm btn-secondary">Return to Dashboard</a> <a href="/education/print/{{$edu->id}}" class="btn btn-sm btn-info">Print View</a> </span>
            </div>
        <hr>

            <table class="table table-dark table-bordered">

                <tbody>

                    <tr>
                        <td>
                            {{ $roster->roster_name }}
                            <small class="form-text text-warning">Roster</small>
                        </td>
                    </tr>

                </tbody>

            </table>

            <hr class="bg-info">
        
            <table class="table table-dark table-bordered">

                <tbody>

                    <tr>
                        <td style="width:33.33%">
                            {{ $edu->title }} <hr>
                            <small class="form-text text-warning">Program Title</small>
                        </td>
                        <td style="width:33.33%">
                            {{ $edu->length }} <hr>
                            <small class="form-text text-warning">Program Length (Hours & Minutes)</small>
                        </td>
                        <td style="width:33.33%">
                            {{ Carbon::parse($edu->dates)->format('m-d-Y') }} <hr>
                            <small class="form-text text-warning">Date</small>
                        </td>
                    </tr>

                    <tr>
                        <td style="width:33.33%">
                            {{ $edu->contact_hours }} <hr>
                            <small class="form-text text-warning"># of Contact Hours</small>
                        </td>
                        <td style="width:33.33%">
                            {{ $edu->CEUs }} <hr>
                            <small class="form-text text-warning"># of CEUs</small>
                        </td>
                        <td style="width:33.33%">
                            {{ $edu->created_by }} <hr>
                            <small class="form-text text-warning">Name & Extension of Submitter</small>
                        </td>
                    </tr>

                </tbody>

            </table>

            <hr class="bg-info">

            <table class="table table-dark table-bordered">

                <tbody>

                    <tr>
                        <td style="width:33.33%">
                            {{ $edu->classification }} <hr>
                            <small class="form-text text-warning">Program Classification (check the one which best applies)</small>
                        </td>
                        <td style="width:33.33%">
                            @foreach ($reasons as $reason)
                                {{ $reason->reason }} <br>
                            @endforeach <hr>
                            <small class="form-text text-warning">Reason for Training (choose ALL that apply)</small>
                        </td>
                        <td style="width:33.33%">
                            @foreach ($methods as $method)
                                {{ $method->methodology }} <br>
                            @endforeach <hr>
                            <small class="form-text text-warning">Training Methodology (choose ALL that apply)</small>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="3">
                            {{ $edu->learning_objectives }} <hr>
                            <small class="form-text text-warning">Learning Objectives (What will participants know or be able to do as a result of this training?</small>
                        </td>
                    </tr>

                </tbody>

            </table>

            <hr class="bg-info">

            <table class="table table-dark table-bordered">

                <tbody>

                    @foreach($presenters as $present)
                        <tr>
                            <td style="width:25%">
                                {{ $present->presenter }} <hr>
                                <small class="form-text text-warning">Instructors/Presenter</small>
                            </td>
                            <td style="width:25%">
                                {{ $present->department }} <hr>
                                <small class="form-text text-warning">Department</small>
                            </td>
                            <td style="width:25%">
                                {{ $present->badge_num }} <hr>
                                <small class="form-text text-warning">Badge #</small>
                            </td>
                            <td style="width:25%">
                                {{ $present->telephone_num }} <hr>
                                <small class="form-text text-warning">Telephone #</small>
                            </td>
                        </tr>
                    @endforeach

                </tbody>

            </table>

            {{-- <hr class="bg-info">

            <table class="table table-dark table-bordered">

                <tbody>

                    <tr>
                        <td style="width:25%">
                            {{ $edu->scanned }} <hr>
                            <small class="form-text text-warning">Scanned?</small>
                        </td>
                        <td style="width:25%">
                            {{ $edu->api }} <hr>
                            <small class="form-text text-warning">API</small>
                        </td>
                        <td style="width:25%">
                            {{ $edu->right_start }} <hr>
                            <small class="form-text text-warning">Right Start</small>
                        </td>
                        <td style="width:25%">
                            {{ $edu->productivity_data }} <hr>
                            <small class="form-text text-warning">Productivity Data</small>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            @if(!empty($edu->api_code))
                                {{ $edu->api_code }}
                            @else
                                <span class="text-danger">No API Code given</span>
                            @endif <hr>
                            <small class="form-text text-warning">API Code</small>
                        </td>
                        <td>
                            {{ Carbon::parse($edu->sig_date)->format('m-d-Y') }} <hr>
                            <small class="form-text text-warning">Date</small>
                        </td>
                        <td>
                            {{ $edu->signature }} <hr>
                            <small class="form-text text-warning">Signature</small>
                        </td>
                    </tr>

                </tbody>

            </table> --}}

        </div>
    </div>


@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script>
    $(document).ready(function() {
        $(".multi-2").select2();
    });
</script>

@endsection

@endif