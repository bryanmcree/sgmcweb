{{--New file Template--}}

{{--Add Security for this page below--}}
@if( Auth::user()->hasRole('Educational Services') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

@section('content')


    <div class="card mb-4 text-white bg-dark">
        <div class="card-body">
            <div class="card-title">
                <h4>
                    Educational Attendance Roster Form
                    <span class="float-right"> <a href="/education" class="btn btn-secondary btn-sm">Return to Dashboard</a> </span>
                </h4>
                
            </div>
        <hr>
            <form action="/education/update/{{$edu->id}}" method="POST" class="mb-0">
                {{ csrf_field() }}

                <table class="table table-dark table-bordered">

                    <tbody>

                        <tr>
                            <td>
                                <select name="roster_id" class="form-control" required>
                                    <option value="{{$edu->roster_id}}">{{$edu->eduRoster->roster_name}}</option>
                                    @foreach($rosters as $roster)
                                        <option value="{{$roster->id}}">{{$roster->roster_name}}</option>
                                    @endforeach
                                </select>
                                <small class="form-text text-warning">Choose the Roster This Form Belongs to</small>
                            </td>
                        </tr>

                    </tbody>

                </table>
            
                <table class="table table-dark table-bordered">

                    <tbody>

                        <tr>
                            <td style="width:33.33%">
                                <input type="text" class="form-control" name="title" placeholder="Program Title" value="{{$edu->title}}">
                                <small class="form-text text-warning">Program Title</small>
                            </td>
                            <td style="width:33.33%">
                                <input type="text" class="form-control" name="length" placeholder="Program Length (Hours & Minutes)"  value="{{$edu->length}}">
                                <small class="form-text text-warning">Program Length (Hours & Minutes)</small>
                            </td>
                            <td style="width:33.33%">
                                <input type="date" name="dates" class="form-control" value={{$edu->dates}}>
                                <small class="form-text text-warning">Date</small>
                            </td>
                        </tr>

                        <tr>
                            <td style="width:33.33%">
                                <input type="text" class="form-control" name="contact_hours" placeholder="# of Contact Hours"  value="{{$edu->contact_hours}}">
                                <small class="form-text text-warning"># of Contact Hours</small>
                            </td>
                            <td style="width:33.33%">
                                <input type="text" class="form-control" name="CEUs" placeholder="# of CEUs" value="{{$edu->CEUs}}">
                                <small class="form-text text-warning"># of CEUs</small>
                            </td>
                            <td style="width:33.33%">
                                <input type="text" class="form-control" name="created_by" placeholder="Name & Extension of Submitter"  value="{{$edu->created_by}}">
                                <small class="form-text text-warning">Name & Extension of Submitter</small>
                            </td>
                        </tr>

                    </tbody>

                </table>

                <hr class="bg-info">

                <table class="table table-dark table-bordered">

                    <tbody>

                        <tr>
                            <td style="width:33.33%">
                                <select name="classification" class="multi-2" style="width:100%;" required>
                                    <option value="{{$edu->classification}}">{{$edu->classification}}</option>
                                    <option value="ARW">ARW</option>
                                    <option value="General Orientation">General Orientation</option>
                                    <option value="Other Orientation (Department, unit)">Other Orientation (Department, unit)</option>
                                    <option value="Patient Safety">Patient Safety</option>
                                    <option value="CPR">CPR</option>
                                    <option value="ACLS, PALS, NRP">ACLS, PALS, NRP</option>
                                    <option value="Policy or Procedure">Policy or Procedure</option>
                                    <option value="Skills Training, Competency">Skills Training, Competency</option>
                                    <option value="New Equipment Training">New Equipment Training</option>
                                    <option value="Equipment Re-training">Equipment Re-training</option>
                                    <option value="Special Patient Populations – Age, culture, disease">Special Patient Populations – Age, culture, disease</option>
                                    <option value="Success Training; Interpersonal Skills, Customer Service">Success Training; Interpersonal Skills, Customer Service</option>
                                    <option value="Management Development">Management Development</option>
                                    <option value="Information Management/Data Analysis/Computer">Information Management/Data Analysis/Computer</option>
                                    <option value="Other: (Nursing Education Follow-Up)">Other: (Nursing Education Follow-Up)</option>
                                    <option value="Meeting – Not Training (Do not turn in to Education)">Meeting – Not Training (Do not turn in to Education)</option>
                                </select>
                                <small class="form-text text-warning">Program Classification (check the one which best applies)</small>
                            </td>
                            <td style="width:33.33%">
                                <select name="reason[]" class="multi-2" style="width:100%;" multiple="multiple" required>
                                    @foreach($reasons as $reason)
                                        <option value="{{$reason->reason}}" selected>{{$reason->reason}}</option>
                                    @endforeach
                                    <option value="To address specific patient population needs">To address specific patient population needs</option>
                                    <option value="To improve staff performance">To improve staff performance</option>
                                    <option value="To meet mandatory training requirements">To meet mandatory training requirements</option>
                                    <option value="To meet advances in science & technology or in health care management">To meet advances in science & technology or in health care management</option>
                                    <option value="To be able to work with new equipment">To be able to work with new equipment</option>
                                    <option value="To be able to perform a new procedure">To be able to perform a new procedure</option>
                                    <option value="To improve quality">To improve quality</option>
                                    <option value="To address policy or regulatory requirement">To address policy or regulatory requirement</option>
                                </select>
                                <small class="form-text text-warning">Reason for Training (choose ALL that apply)</small>
                            </td>
                            <td style="width:33.33%">
                                <select name="methodology[]" class="multi-2" style="width:100%;" multiple="multiple" required>
                                    @foreach($methods as $method)
                                        <option value="{{$method->methodology}}" selected>{{$method->methodology}}</option>
                                    @endforeach
                                    <option value="Lecture">Lecture</option>
                                    <option value="Discussion">Discussion</option>
                                    <option value="Group activities/experiential exercises">Group activities/experiential exercises</option>
                                    <option value="Video-Based training">Video-Based training</option>
                                    <option value="Audio/Telnet training">Audio/Telnet training</option>
                                    <option value="Satellite broadcast">Satellite broadcast</option>
                                    <option value="Written Self-Learning Packet">Written Self-Learning Packet</option>
                                    <option value="Computer Based Training (CBT)">Computer Based Training (CBT)</option>
                                    <option value="Other">Other</option>
                                </select>
                                <small class="form-text text-warning">Training Methodology (choose ALL that apply)</small>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="3">
                                <textarea name="learning_objectives" rows="5" class="form-control" placeholder="Learning Objectives (What will participants know or be able to do as a result of this training?)">{{$edu->learning_objectives}}</textarea>
                                <small class="form-text text-warning">Learning Objectives (What will participants know or be able to do as a result of this training?</small>
                            </td>
                        </tr>

                    </tbody>

                </table>

                <hr class="bg-info">

                <table class="table table-dark table-bordered">

                    <tbody>

                        <tr>
                            <td style="width:25%">
                                <input type="text" name="presenter" class="form-control" placeholder="Instructors/Presenter" value="{{$edu->presenter}}">
                                <small class="form-text text-warning">Instructors/Presenter</small>
                            </td>
                            <td style="width:25%">
                                <input type="text" name="department" class="form-control" placeholder="Department" value="{{$edu->department}}">
                                <small class="form-text text-warning">Department</small>
                            </td>
                            <td style="width:25%">
                                <input type="text" name="badge_num" class="form-control" placeholder="Badge #" value="{{$edu->badge_num}}">
                                <small class="form-text text-warning">Badge #</small>
                            </td>
                            <td style="width:25%">
                                <input type="text" name="telephone_num" class="form-control" placeholder="Telephone #" value="{{$edu->telephone_num}}">
                                <small class="form-text text-warning">Telephone #</small>
                            </td>
                        </tr>

                    </tbody>

                </table>

                <hr class="bg-info">

                <table class="table table-dark table-bordered">

                    <tbody>

                        <tr>
                            <td style="width:25%">
                                <select name="scanned" class="form-control" required>
                                    <option value="{{$edu->scanned}}">{{$edu->scanned}}</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                                <small class="form-text text-warning">Scanned?</small>
                            </td>
                            <td style="width:25%">
                                <select name="api" class="form-control" required>
                                    <option value="{{$edu->api}}">{{$edu->api}}</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                                <small class="form-text text-warning">API</small>
                            </td>
                            <td style="width:25%">
                                <select name="right_start" class="form-control" required>
                                    <option value="{{$edu->right_start}}">{{$edu->right_start}}</option>
                                    <option value="Yes">Yes</option>
                                    <option value="N/A">N/A</option>
                                </select>
                                <small class="form-text text-warning">Right Start</small>
                            </td>
                            <td style="width:25%">
                                <select name="productivity_data" class="form-control" required>
                                    <option value="{{$edu->productivity_data}}">{{$edu->productivity_data}}</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                                <small class="form-text text-warning">Productivity Data</small>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
                                <input type="text" name="api_code" class="form-control" placeholder="API Code" value="{{$edu->api_code}}">
                                <small class="form-text text-warning">API Code</small>
                            </td>
                            <td>
                                <input type="date" class="form-control" name="sig_date" value={{Carbon::now()}} readonly>
                                <small class="form-text text-warning">Date</small>
                            </td>
                            <td>
                                <input type="text" class="form-control" name="signature" value="{{Auth::user()->name}}" readonly>
                                <small class="form-text text-warning">Signature</small>
                            </td>
                        </tr>

                    </tbody>

                </table>

                <hr class="bg-info">

                <input type="submit" class="btn btn-primary btn-block" value="Update">

            </form>

        </div>
    </div>


@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script>
    $(document).ready(function() {
        $(".multi-2").select2();
    });
</script>

@endsection

@endif