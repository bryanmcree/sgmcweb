@extends('layouts.app')

@section('content')

    <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading panel-heading-custom">
                    <div class="panel-title"><h2><b>Why You’re Losing Good Employees</b></h2></div>
                    <div class="panel-sub-title">Ruslan Desyatnikov -  <i>Founder & CEO at QA Mentor, Inc.</i>  Published on February 4, 2017 - Featured in: Leadership & Management, Recruiting & Hiring </div>

                </div>

                <div class="panel-body">

                   <h3>
                       The question has such an obvious answer, it seems too easy. Good employees quit because of bad managers.  Really, that’s the primary reason. This has been shown to be true over and over and over again.
                    <br><br>
                       So why is it constantly ignored? Why does upper management hire or promote such poor middle or lower managers? Even better, how do such poor managers get promoted up to upper management?
                       <br><br>
                       I remember one experience of mine where an extremely poor manager was promoted to director despite an office-wide outcry against their promotion. When I queried as to how someone so outrageously horrible at their job could get promoted, one answer I received was that said person was being promoted “up and out”. They couldn’t be fired, yet they couldn’t really stay where they were either since their people skills were atrocious.  So, this person was promoted so that they weren’t dealing with lots of staff daily.  I have no idea how prevalent such behavior is within companies, but I personally found it horrifying, and yet enlightening regarding how horrible bosses get moved up.
                       <br><br>
                       But why are we talking about this and how can it help you? Well, does your company have a high turnover rate? Have you been blaming the “poor quality employees” for it?  Or “those lazy, whining kids”?  Or any other derogatory excuse as to why people keep leaving your company?  Perhaps – just perhaps – you may want to look at the supervisors and managers within your organization – or even at yourself.
                       <br><br>
                       Here are some possibilities.
                       <br><br>
                       <B>You’re hiring/promoting the wrong people for the job</B>
                       <br><br>
                       A fantastic employee can only be good at their job if they’re the right fit for the job. Otherwise, they’re going to suffer and then their work is going to suffer.  Also, promoting people who don’t deserve it is a sure-fire way to upset worthwhile employees who feel they’ve been overlooked.
                       <br><br>
                       <B>You’re not recognizing good work</B>
                       <br><br>
                       No, you don’t need to hand out gold stars to your employees or give everyone a trophy, but when employees have done an objectively good job, they should be told so. Especially if they have gone out of their way to do a good job, such as working nights, weekends, etc. Just simply saying “I appreciate all the hard work you put in so this project would be successful” can go a long way.
                       <br><br>
                       <B>You’re overworking your employees</B>
                       <br><br>
                       Asking your team to work a few extra hours to complete a project isn’t a big deal. But if they need to work extra hours as a general rule, then you should hire another team member. To be clear – short term extra hours is okay.  Long term extra hours are not okay.  At the very least, reward those hard-working employees with promotions or raises. Make it worth their while, or they will walk.
                       <br><br>
                       <B>You’re supporting stagnation</B>
                       <br><br>
                       Either actively or unintentionally, you could be supporting employees in being too complacent with their career development. Good managers need to encourage development, continuing education, and/or challenge their employees. Otherwise, good employees will get bored or feel as though they have no upward mobility within the organization. Ask my guys in QA Mentor and they will tell you how I challenge them every day :)
                       <br><br>
                       When I quit my job for greener pastures, it was because of all those things I listed.
                       <br><br>
                       <ul>
                           <li>I wasn’t growing any longer in my career, and not only was I not encouraged to do so, I wasn’t given the opportunity to.</li>
                           <br><br>
                           <li>There was no recognition or appreciation to be had. No matter how hard I worked, how many projects were successful beyond expectation, or how many extra days I put in a year – no one recognized my efforts.</li>
                           <br><br>
                           <li>The culture and morale were terrible, mostly because of the above two issues. I wasn’t the only one who felt the way I did, and that was obvious throughout the team. I wasn’t the first – or the last – to walk away.</li>
                           <br><br>
                           <li>If you’ve quit a job, what were your reasons? Did they fall into the things I’ve mentioned above?</li>
                           <br><br>
                       </ul>


                       <br>
                       <b>About the Author:</b>
                       <br><br>
                       The author, Ruslan Desyatnikov, is the CEO & Founder of QA Mentor- US based Independent Software Testing Company with presence in 8 different countries. He created QA Mentor to fill the gap he has witnessed in QA service providers during his 20+ years in QA. With Ruslan’s guidance, unique services and methodologies were developed at QA Mentor to aid clients in their QA difficulties while still offering a high ROI.  Ruslan also has developed unique e-learning QA Courses for entire testing community which you can find by visiting http://elearning.qamentor.com portal. To learn more about QA Mentor and testing services please visit www.qamentor.com or contact Ruslan directly by sending email to rdesyatnikov@qamentor.com
                   </h3>

                </div>
            </div>
    </div>
@endsection

