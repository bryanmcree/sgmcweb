<div class="modal fade Airwatch" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><b>Airwatch Help - Airwatch</b></h4>
            </div>
            <div class="modal-body bg-default">

                {!! Form::open(array('action' => ['HelpController@mobileHelp'], 'class' => 'form_control')) !!}

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('name', 'Name:') !!}
                            <div class="input-group">
                                <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                {!! Form::text('name', null, ['class' => 'form-control', 'id'=>'name', 'required','placeholder'=>'Name']) !!}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::label('phone_number', 'Phone Number:') !!}
                            <div class="input-group">
                                <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                {!! Form::text('phone_number', null, ['class' => 'form-control', 'id'=>'phone_number', 'required','placeholder'=>'NUMBERS ONLY','maxlength' => '10']) !!}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('email_address', 'Email Address:') !!}
                            <div class="input-group">
                                <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                <input type="email" required name="email_address" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('platform', 'Device Platform:') !!}
                            <div class="input-group">
                                <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                <select name="platform" class="form-control" required>
                                    <option value="">[Select]</option>
                                    <option value="Android">Android</option>
                                    <option value="iOS">iOS</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('type_device', 'Device Type:') !!}
                            <div class="input-group">
                                <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                <select name="type_device" class="form-control" required>
                                    <option value="">[Select Device Type]</option>
                                    <option value="Ipad">Ipad</option>
                                    <option value="Phone">Phone</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('owner', 'Who owns device?') !!}
                            <div class="input-group">
                                <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                <select name="owner" class="form-control" required>
                                    <option value="">[Select]</option>
                                    <option value="SGMC">SGMC</option>
                                    <option value="BYOD">I own it.</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    {!! Form::label('use', 'What will this device be used for?') !!}
                    <div class="input-group">
                        <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                        {!! Form::textarea('use', null, ['class' => 'form-control', 'id'=>'use', 'rows' => 4]) !!}
                    </div>
                </div>



            </div>
            <div class="modal-footer bg-default">
                <div class="RightLeft">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    {!! Form::submit('HELP!', ['class'=>'btn btn-sgmc', 'id'=>'AddButton']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>