<!-- Modal -->
<div class="modal fade" id="mooseSupport" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content text-white bg-dark">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Moose Support</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <img src="{{URL::asset('img/Cat-icon.png')}}" class="" style="display: block; margin-left: auto; margin-right: auto;">
                <p>Epic Downtime 03/05/2019 11 pm - 4am</p>
                <p style="text-align:justify">Attention all Users! On the evening of Tuesday (March 5th, 2019), please save any open files, shut down any running applications, log out of Windows, and then leave your workstation powered ON before you leave for the day. Information Services will be pushing out important software updates during the evening of March 5th. These updates are required for the Epic software upgrade taking place later that same night. Once these updates are installed, your workstation will automatically reboot.
                </p>
                <hr class="bg-warning">
                <p>Greetings Human!</p>
                <p style="text-align:justify">My name is Moose! When I am all grown up I will be your Support Solution Specialist.
                    I will help you find what you are looking for on web.sgmc.org everything from
                    handwashing surveys, tardy reports to the extra mile.  It won’t be long before
                    I am ready, but until then you can always use the “Help” button at the top
                    left of your blue nav bar.</p>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>