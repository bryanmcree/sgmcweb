<div class="modal fade Help" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">
                <h4 class="modal-title" id="myModalLabel"><b>I need help NOW!!!</b></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                
            </div>
            <div class="modal-body bg-default">
                Help is on the way.  First, please make sure you are doing the following:

                    <ul class="custom-bullet">
                        <li>You MUST use Google Chrome.. NOT Internet Explorer</li>
                        <li>When searching for cost centers, you must CLICK on your selection from the drop down menu.</li>
                    </ul>
                If the problem is none of the above, please contact Bryan McRee at ext. 1145 (available Monday – Friday, 8-5pm) or complete the form below and he will contact you shortly!

                If this is after hours, please indicate if you work night shift, and he will try to respond to you as soon as possible.

                Thank you!

                <br>
                <br>
                {!! Form::open(array('action' => ['HelpController@sendHelp'], 'class' => 'form_control')) !!}
                {!! Form::hidden('first_name', auth::user()->first_name) !!}
                {!! Form::hidden('last_name', auth::user()->last_name) !!}
                {!! Form::hidden('mail', auth::user()->mail) !!}
                {!! Form::hidden('userid', auth::user()->username) !!}

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::label('phone_number', 'Phone Number:') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text bg-danger" id="basic-addon1"><i class="fa fa-exclamation-triangle"></i></span>
                                </div>
                                {!! Form::text('phone_number', null, ['class' => 'form-control', 'id'=>'phone_number', 'required','placeholder'=>'NUMBERS ONLY','maxlength' => '10']) !!}
                            </div>
                        </div>
                    </div>
                </div>


                    <div class="form-group">
                        {!! Form::label('problem', 'Brief Description of the Problem:') !!}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text bg-danger" id="basic-addon1"><i class="fa fa-exclamation-triangle"></i></span>
                            </div>
                            {!! Form::textarea('problem', 'Description of the Problem', ['class' => 'form-control', 'id'=>'problem', 'rows' => 4]) !!}
                        </div>
                    </div>



            </div>
            <div class="modal-footer bg-default">
                <div class="RightLeft">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    {!! Form::submit('HELP!', ['class'=>'btn btn-sgmc', 'id'=>'AddButton']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>