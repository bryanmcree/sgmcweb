@extends('layouts.app')

@section('content')
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {!! Form::open(array('action' => ['RightController@store'], 'class' => 'form_control')) !!}
    <div class="panel panel-default">
        <div class="panel-heading"><b>Create Roles</b></div>
        <div class="panel-body">
    @include('role.form')
        </div>
    </div>
    {!! Form::submit('Add Role', ['class'=>'btn btn-default']) !!}
    {!! Form::reset('Clear', ['class'=>'btn btn-default']) !!}
    {!! Form::close() !!}

@endsection