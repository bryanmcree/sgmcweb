@extends('layouts.app')

@section('content')
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {!! Form::model($roles, ['method'=>'PATCH', 'action'=>['RoleController@update', $roles->id], 'files' => true]) !!}
    <div class="panel panel-default">
        <div class="panel-heading"><b>Edit Role</b></div>
        <div class="panel-body">
            @include('role.form')
        </div>
    </div>
    {!! Form::submit('Update Role', ['class'=>'btn btn-default']) !!}
    {!! Form::reset('Clear', ['class'=>'btn btn-default']) !!}
    {!! Form::close() !!}

@endsection