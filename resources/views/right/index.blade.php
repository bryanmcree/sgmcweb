@extends('layouts.app')

@section('content')
    <div class="panel panel-success">
        <div class="panel-heading"><b>Roles Permissions ({{$rights->count()}}) for {{$roleName->name}}</b></div>
        <div class="panel-body">
    @if ($rights->isEmpty())
                <div class="container-fluid">
                    <div class="row">
                        {!! Form::open(array('url' => '/right/'. $id, 'method' => 'GET', 'files' => true,'class' => 'form_control')) !!}
                        {!! Form::hidden('role_id', $id) !!}
                        <div class="input-group">
                            {!! Form::select('permission_id', $permissions, null, ['class' => 'form-control']) !!}
                 <span class="input-group-btn">
                {!! Form::submit('Add Permission', ['class'=>'btn btn-success']) !!}
                 </span>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            <br>
                <table class="table table-hover table-responsive table-bordered table-striped" id="table">

                    <thead>
                    <tr class="success">
                        <td><b>This role has NO permissions.</b></td>
                    </tr>
                    </thead>
                </table>
    @else
                <div class="container-fluid">
                    <div class="row">
                        {!! Form::open(array('url' => '/right/'. $id, 'method' => 'GET', 'files' => true,'class' => 'form_control')) !!}
                        {!! Form::hidden('role_id', $id) !!}
                        <div class="input-group">
                            {!! Form::select('permission_id', $permissions, null, ['class' => 'form-control']) !!}
                            <span class="input-group-btn">
                {!! Form::submit('Add Permission', ['class'=>'btn btn-success']) !!}
                 </span>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            <BR>
        <table class="table table-hover table-responsive table-bordered table-striped" id="table">

            <thead>
            <tr class="success">
                <td><b>Permission</b></td>
                <td align="center" colspan="4"></td>
            </tr>
            </thead>
            @foreach ($rights as $right)
                <tbody>
                <tr>
                    <td>{{$right->permissionName->name}}</td>
                    <td align="center"><a href="/right/del/{{$right->permission_id}}?role_id={{$id}}" role="button" class="btn btn-danger btn-xs" onclick="return ConfirmDelete()"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Delete</a></td>
                </tr>
                </tbody>
            @endforeach
        </table>



    @endif

    </div>
    </div>

@endsection
