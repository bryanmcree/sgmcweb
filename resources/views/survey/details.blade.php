{{--New file Template--}}

{{--Add Security for this page below--}}


    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

<div style="width:80%" class="mx-auto">

    <div class="card bg-dark">
        <div class="card-header">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Survey Dashboard</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Questions</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Survey Options</a>
                </li>
                {{-- <li class="nav-item">
                    <a class="nav-link" id="completed-tab" data-toggle="tab" href="#completed" role="tab" aria-controls="completed" aria-selected="false">Completed Surveys</a>
                </li> --}}
                <li class="nav-item">
                    <a class="nav-link" id="take-tab" data-toggle="tab" href="#take" role="tab" aria-controls="take" aria-selected="false">Take this Survey</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#surveyReport" data-toggle="modal">Report</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="contact-tab"  href="/survey" role="tab" aria-selected="false">Back to Surveys</a>
                </li>
            </ul>
        </div>
    </div>

    @if($survey->active ==0)
        <br>
        <h2 class="alert alert-warning">This survey is NOT ACTIVE.  Click survey options to turn on survey.</h2>
    @endif

    <div class="alert bg-danger mt-3">
        <h4>Alert: This survey application will be depreciated on November 1, 2019. The new location for surveys will be here: 
            <a href="/audit" class="text-white">web.sgmc.org/audit</a>
        </h4>
    </div>

    <div class="tab-content" id="myTabContent">

        <div class="tab-pane fade show active " id="home" role="tabpanel" aria-labelledby="home-tab">
            <div class="card-deck mt-4">
                <div class="card mb-3 text-white bg-dark">
                    <div class="card-body">
                        <b>Total Questions:</b> {{$questions->count()}}
                    </div>
                </div>
                <div class="card mb-3 text-white bg-dark">
                    <div class="card-body">
                        <b>Submissions:</b> {{$survey->taken_count}}
                    </div>
                </div>
            </div>
            <div class="card mb-3 text-white bg-dark">
                @if($survey->visibility != 1)
                    <div class="card-body">
                        <p>This survey is <B>PRIVATE</B>.  Below is a link to share with users so they can take the survey.  Cut and paste this into an email to distribute this survey.</p>
                        <div class="form-group">
                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="1">http://web.sgmc.org/survey/{{$survey->id}}</textarea>
                        </div>
                        <button class="btn btn-primary" onclick="myFunction()">Copy Link Above to Clipboard</button>
                    </div>
                @else
                    <div class="card-body">
                        <p>This survey is public.  Everyone will have the ability to participate in this survey.  If you would like to email a link to this survey you will find it below.</p>
                        <div class="form-group">
                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="1">http://web.sgmc.org/survey/{{$survey->id}}</textarea>
                        </div>
                        <button class="btn btn-primary" onclick="myFunction()">Copy Link Above to Clipboard</button>
                    </div>
                @endif
            </div>
            {{-- <div class="card mb-3 text-white bg-dark">
                <div class="card-body" style="height:200px; overflow-y: auto;">
                    <table class="table table-condensed table-hover" id="completedSurveys">
                        <thead>
                        <tr>
                            <td><b>Participant</b></td>
                            <td><b>Cost Center</b></td>
                            <td><b>Date Time</b></td>
                            <td class="no-sort"><b></b></td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($who_took as $took)
                            <tr>
                                <td style="vertical-align : middle;">{{$took->submittedBy->name}}</td>
                                <td style="vertical-align : middle;">{{$took->submittedBy->unit_code}} - {{$took->submittedBy->unit_code_description}}</td>
                                <td style="vertical-align : middle;">{{$took->created_at}}</td>
                                <td style="vertical-align : middle;" align="right"><a href="/survey/print/{{$survey->id}}?incident_id={{$took->incident_id}}" target="_blank"><i class="fas fa-eye"></i></a> </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div> --}}
                @foreach($questions as $question)
                <div class="card mb-3 text-white bg-dark">
                    <div class="card-header">
                        <h5>{{$question->title}}</h5>
                    </div>
                    <div class="card-body" style="height:345px; overflow-y: auto;">
                            @if($question->question_type == 'Short Answer')
                            <h3 class="jumbotron bg-secondary">Short Answer Submissions</h3>
                            <a href="/survey/shortresults/{{$survey->id}}/{{$question->id}}" class="btn btn-primary btn-block mb-2">View Results</a>

                            <br><p><b>Type:</b> {{$question->question_type}}</p>

                        @elseif($question->question_type == 'employee')

                            <h3 class="jumbotron bg-secondary">Employee Selection</h3>
                            <a href="/survey/empresults/{{$survey->id}}/{{$question->id}}" class="btn btn-primary btn-block mb-2">View Results</a>

                            <br><p><b>Type:</b> {{$question->question_type}}</p>

                        @elseif($question->question_type == 'date_time')

                            <h3 class="jumbotron bg-secondary">Date & Time selection</h3>
                            <a href="/survey/datetime_results/{{$survey->id}}/{{$question->id}}" class="btn btn-primary btn-block mb-2">View Results</a>

                            <br><p><b>Type:</b> {{$question->question_type}}</p>

                        @elseif($question->question_type == 'cost_center')
                        
                            <h3 class="jumbotron bg-secondary">Cost Center Selection</h3>
                            <a href="/survey/ccresults/{{$survey->id}}/{{$question->id}}" class="btn btn-primary btn-block mb-2">View Results</a>

                            <br><p><b>Type:</b> {{$question->question_type}}</p>
                            
                        @elseif($question->question_type == 'YesNoNA')
                        
                            <canvas id="{{$question->id}}" height="50"></canvas>
                            <p><b>Type:</b> {{$question->question_type}} <span class="float-right"><a href="/survey/pie/{{$survey->id}}/{{$question->id}}" class="btn btn-primary btn-sm btn-block mb-4">View Results</a></span> </p>
                            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.js"></script>
                            <script type="application/javascript">

                                new Chart(document.getElementById('{{$question->id}}'), {
                                    type: 'bar',

                                    data: {
                                        labels: [
                                            @foreach($yesnoNA->where('question_id', $question->id) as $answer)
                                                @if($answer->answer == 1)
                                                    "Yes",
                                                @elseif($answer->answer == 0)
                                                    "No",
                                                @else
                                                    "N/A",
                                                @endif
                                            @endforeach
                                        ],
                                        datasets: [{
                                            label: "",
                                            backgroundColor: [
                                                @foreach($yesnoNA->where('question_id', $question->id) as $answer)
                                                    @if($answer->answer == 1)
                                                        'rgba(0, 99, 132, 0.6)',
                                                    @elseif($answer->answer == 0)
                                                        'rgba(124, 86, 188, 0.6)',
                                                    @else
                                                        'rgba(168, 70, 200, 0.6)',
                                                    @endif
                                                @endforeach
                                            ],
                                            data: [

                                                @foreach($yesnoNA->where('question_id', $question->id) as $answer)
                                                    {{$answer->total}},
                                                @endforeach
                                            ]
                                        }]
                                    },
                                    options: {
                                        legend: { display: false },
                                        title: {
                                            //display: true,
                                            //text: 'Predicted world population (millions) in 2050'
                                        },
                                        scales: {
                                            yAxes: [{
                                                stacked:false,
                                            ticks: {
                                                beginAtZero: true
                                                }
                                            }]
                                        }
                                    }
                                });
                            </script>
                                        
                        @else
                            <canvas id="{{$question->id}}" height="50"></canvas>
                            <p><b>Type:</b> {{$question->question_type}} <span class="float-right"><a href="/survey/pie/{{$survey->id}}/{{$question->id}}" class="btn btn-primary btn-sm btn-block mb-4">View Results</a></span> </p>
                            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.js"></script>
                            <script type="application/javascript">

                                new Chart(document.getElementById('{{$question->id}}'), {
                                    type: 'pie',

                                    @if($question->question_type == 'YesNo')
                                    data: {
                                        labels: [
                                            @foreach($yesno->where('question_id', $question->id) as $answer)
                                                @if($answer->answer == 1)
                                                    "Yes",
                                                @else
                                                    "No"
                                                @endif
                                            @endforeach
                                        ],
                                        datasets: [{
                                            label: "",
                                            backgroundColor: [
                                                @foreach($yesno->where('question_id', $question->id) as $answer)
                                                    @if($answer->answer == 1)
                                                        'rgba(0, 99, 132, 0.6)',
                                                    @else
                                                        'rgba(124, 86, 188, 0.6)'
                                                    @endif
                                                @endforeach
                                            ],
                                            data: [

                                                @foreach($yesno->where('question_id', $question->id) as $answer)
                                                    {{$answer->total}},
                                                @endforeach
                                            ]
                                        }]
                                    },
                                    options: 
                                    {
                                        // scales: {
                                        //     xAxes: [{
                                        //         ticks: {
                                        //             beginAtZero: true
                                        //         }
                                        //     }]
                                        // }, --for bar charts                         
                                        title: 
                                        {
                                            display: true,
                                            //text: 'Anything Else'
                                        }
                                    }
                                        
                                    @endif

                                    @if($question->question_type == 'Scale_5')
                                    data: {
                                        labels: [
                                            @foreach($scale5->where('question_id', $question->id) as $answer)
                                            {{$answer->answer}},
                                            @endforeach

                                        ],
                                        datasets: [{
                                            label: "Scale 1-5",
                                            backgroundColor: [
                                                'rgba(0, 99, 132, 0.6)',
                                                'rgba(30, 99, 132, 0.6)',
                                                'rgba(60, 99, 132, 0.6)',
                                                'rgba(90, 99, 132, 0.6)',
                                                'rgba(120, 99, 132, 0.6)',
                                                'rgba(150, 99, 132, 0.6)',
                                                'rgba(180, 99, 132, 0.6)',
                                                'rgba(210, 99, 132, 0.6)',
                                            ],
                                            data: [

                                                @foreach($scale5->where('question_id', $question->id) as $answer)
                                                {{$answer->total}},
                                                @endforeach
                                            ]
                                        }]
                                    },
                                    options: {
                                        title: {
                                            display: true,
                                            //text: 'Anything Else'
                                        }
                                    }
                                    @endif

                                    @if($question->question_type == 'dropdown')
                                    data: {
                                        labels: [
                                            @foreach($joinDrop->where('question_id', $question->id) as $answer)
                                                @if(!is_null($answer->dropdown_choice))
                                                    "{{$answer->dropdown_choice}}",
                                                @endif
                                            @endforeach

                                        ],
                                        datasets: [{
                                            label: "Dropdown",
                                            backgroundColor: [
                                                'rgba(0, 99, 132, 0.6)',
                                                'rgba(165, 140, 220, 0.6)',
                                                'rgba(60, 200, 300, 0.6)',
                                                'rgba(90, 99, 132, 0.6)',
                                                'rgba(120, 99, 132, 0.6)',
                                                'rgba(145, 99, 132, 0.6)',
                                                'rgba(170, 99, 132, 0.6)',
                                                'rgba(200, 99, 132, 0.6)',
                                                'rgba(225, 99, 132, 0.6)',
                                                'rgba(255, 99, 132, 0.6)'
                                            ],
                                            data: [

                                                @foreach($joinDrop->where('question_id', $question->id) as $answer)
                                                    @if(!is_null($answer->dropdown_choice))
                                                        {{$answer->total_count}},
                                                    @endif
                                                @endforeach
                                            ]
                                        }]
                                    },
                                    options: {
                                        title: {
                                            display: true,
                                            //text: 'Anything Else'
                                        }
                                    }
                                    @endif


                                    @if($question->question_type == 'Scale_10')
                                    data: {
                                        labels: [
                                            @foreach($scale10->where('question_id', $question->id) as $answer)
                                            {{$answer->answer}},
                                            @endforeach

                                        ],
                                        datasets: [{
                                            label: "Scale 1-10",
                                            backgroundColor: [
                                                'rgba(0, 99, 132, 0.6)',
                                                'rgba(30, 99, 132, 0.6)',
                                                'rgba(60, 99, 132, 0.6)',
                                                'rgba(90, 99, 132, 0.6)',
                                                'rgba(120, 99, 132, 0.6)',
                                                'rgba(150, 99, 132, 0.6)',
                                                'rgba(180, 99, 132, 0.6)',
                                                'rgba(210, 99, 132, 0.6)',
                                            ],
                                            data: [

                                                @foreach($scale10->where('question_id', $question->id) as $answer)
                                                {{$answer->total}},
                                                @endforeach
                                            ]
                                        }]
                                    },
                                    options: {
                                        title: {
                                            display: true,
                                            //text: 'Anything Else'
                                        }
                                    }
                                    @endif


                                });
                            </script>
                        @endif
                    </div>
                </div>
        @endforeach
    </div> <!-- HOME -->

        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
            <div class="card mt-4 card-block text-white bg-dark">
                <div class="card-header">
                    <h3>Questions</h3>
                    <a class="btn btn-sm btn-success" data-toggle="collapse" href="#AddQuestionForm" aria-expanded="false" aria-controls="AddQuestionForm"><i class="far fa-plus-square"></i> Add Question</a>
                </div>

            <div class="collapse" id="AddQuestionForm">
                <div class="card card-block pt-2 pb-2 text-white bg-dark">
                    <div class="col-md-6">
                        <form method="post" action="/survey/question/create">
                            {{ csrf_field() }}
                            <input type="hidden" value="{{ Auth::user()->employee_number }}" name="created_by">
                            <input type="hidden" value="{{$survey->id}}" name="survey_id">
                            <div class="form-group">
                                <label for="question_type_id">Question Type</label>
                                <select class="form-control" id="question_type_id" name="question_type" required>
                                    <option value="" selected>[Select Question Type]</option>
                                    <option value="YesNo">Yes/No</option>
                                    <option value="YesNoNA">Yes/No/NA</option>
                                    <option value="Short Answer">Short Answer</option>
                                    <option value="Scale_5">Scale 1-5</option>
                                    <option value="Scale_10">Scale 1-10</option>
                                    <option value="dropdown">Drop Down Select</option>
                                    <option value="date_time">Date and Time</option>
                                    <option value="cost_center">Cost Center Select</option>
                                    <option value="employee">Employee Name Auto Complete</option>
                                </select>
                            </div>
                            <div id="hidden_div" style="display: none;">
                                <p style="color:greenyellow; font-style: italic">Enter your drop down list labels</p>
                                <div class="card col-4 mb-3 text-white bg-dark">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <input type="text" name="dropdown_label[]" class="form-control" id="title_id" placeholder="Drop down option." >
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="dropdown_label[]" class="form-control" id="title_id" placeholder="Drop down option." >
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="dropdown_label[]" class="form-control" id="title_id" placeholder="Drop down option." >
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="dropdown_label[]" class="form-control" id="title_id" placeholder="Drop down option." >
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="dropdown_label[]" class="form-control" id="title_id" placeholder="Drop down option." >
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="dropdown_label[]" class="form-control" id="title_id" placeholder="Drop down option." >
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="dropdown_label[]" class="form-control" id="title_id" placeholder="Drop down option." >
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="dropdown_label[]" class="form-control" id="title_id" placeholder="Drop down option." >
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="dropdown_label[]" class="form-control" id="title_id" placeholder="Drop down option." >
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="dropdown_label[]" class="form-control" id="title_id" placeholder="Drop down option." >
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="required_id">Answer Required</label>
                                <select class="form-control" id="required_id" name="required" required>
                                    <option value="0" selected>No</option>
                                    <option value="1">Yes</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="title_id">Question</label>
                                <input type="text" name="title" class="form-control" id="title_id" placeholder="Remember to word your question to the correct type." required>
                            </div>
                            <div class="form-group">
                                <label for="question_notes_id">Question Notes</label>
                                <textarea class="form-control" name="question_notes" id="question_notes_id" rows="3"></textarea>
                            </div>
                            <input type="Submit" class="btn btn-primary" value="Submit">
                        </form>
                    </div>

                </div>
            </div>

            @if($questions->isEmpty())
                <div class="card-body">
                    <div class="alert alert-info fa-lg mt-3"><i class="far fa-plus-square"></i> You don't have any questions for this survey! Let's fix that.</div>
                </div>
                
            @else
            <div class="card-body">
                <p>Drag rows to reorder questions.</p>

                <table class="table table-hover text-white bg-dark">
                    <thead>
                    <tr><td></td>
                        <td><b>Question</b></td>
                        <td align="center"><b>Required</b></td>
                        <td><b>Type</b></td>
                        <td><b>Created At</b></td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody id="tablecontents">
                    <?php $rowCount = 0 ?>
                    @foreach($questions as $question)
                        <?php $rowCount = $rowCount +1 ?>
                        <tr class="row1" data-id="{{ $question->id }}">
                            <td style="vertical-align : middle;"><b>{{$rowCount}}.</b></td>
                            <td style="vertical-align : middle;">{{$question->title}}</td>
                            <td style="vertical-align : middle;" align="center">@if($question->required == 1)<i class="fas fa-check" style="color:green"></i>@else <i class="fas fa-times" style="color:red"></i> @endif</td>
                            <td nowrap style="vertical-align : middle;">{{$question->question_type}}</td>
                            <td nowrap style="vertical-align : middle;">{{$question->created_at}}</td>
                            <td nowrap style="vertical-align : middle;" align="right">

                                <a href="/survey/question/edit/{{$survey->id}}/{{$question->id}}" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></a>
                                {{-- <a href="#" class="btn btn-primary btn-sm EditQuestion" disabled title="Not working just yet..." data-toggle="modal"
                                    data-question_id="{{$question->id}}"
                                    data-survey_id="{{$question->survey_id}}"
                                    data-question_type="{{$question->question_type}}"
                                    @if($question->question_type == "dropdown")
                                        @foreach($dropdown as $option)
                                            data-dropdown_choice[]="{{$option->dropdown_choice}}"
                                        @endforeach
                                    @endif
                                    data-required="{{$question->required}}"
                                    data-title="{{$question->title}}"
                                    data-question_notes="{{$question->question_notes}}"> 
                                    <i class="fas fa-edit"></i>
                                </a> --}}

                                <a href="#" class="btn btn-danger btn-sm DeleteQuestion" title="Delete Question and All Answers"
                                   data-question_id="{{$question->id}}"
                                   data-survey_id="{{$question->survey_id}}"
                                ><i class="fas fa-trash-alt"></i>
                                </a>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            @endif
        </div>
    </div> <!-- PROFILE -->
         
        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
            <div class="card bg-dark text-white mt-4 mb-3">
                <div class="card-header">
                    <h3>Survey Options</h3>
                </div>
                <form method="post" action="/survey/options/{{$survey->id}}">
                    {{ csrf_field() }}
                    <div class="card-body">
                            <input type="submit" class="btn btn-primary btn-block mb-3" value="Update Survey">

                        <p>Do you want this survey to be on or off? Turning this off will de-activate this survey where others cannot take it.</p>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="active" id="exampleRadios1" value="1" @if($survey->active == 1) checked @endif>
                            <label class="form-check-label" for="exampleRadios1">
                                On
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="active" id="exampleRadios2" value="0" @if($survey->active != 1) checked @endif>
                            <label class="form-check-label" for="exampleRadios2">
                                Off
                            </label>
                        </div>

                        <p class="mt-3">Show results to user? When the user submits a survey display the survey results to the user.</p>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="show_results" id="exampleRadios1a" value="1" @if($survey->show_results == 1) checked @endif>
                            <label class="form-check-label" for="exampleRadios1a">
                                On
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="show_results" id="exampleRadios2a" value="0" @if($survey->show_results != 1) checked @endif>
                            <label class="form-check-label" for="exampleRadios2a">
                                Off
                            </label>
                        </div>

                        <p class="mt-3">When this survey is complete send user back to blank survey. <b><i>This will be disabled if you limit one submission per person.</i></b></p>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="survey_redirect" id="exampleRadios1b" value="1" @if($survey->survey_redirect == 1) checked @endif>
                            <label class="form-check-label" for="exampleRadios1b">
                                On
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="survey_redirect" id="exampleRadios2b" value="0" @if($survey->survey_redirect != 1) checked @endif>
                            <label class="form-check-label" for="exampleRadios2b">
                                Off
                            </label>
                        </div>

                        <p class="mt-3">Do you want the user to see the question notes?</p>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="show_question_notes" id="exampleRadios1c" value="1" @if($survey->show_question_notes == 1) checked @endif>
                            <label class="form-check-label" for="exampleRadios1c">
                                Yes
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="show_question_notes" id="exampleRadios2c" value="0" @if($survey->show_question_notes != 1) checked @endif>
                            <label class="form-check-label" for="exampleRadios2c">
                                No
                            </label>
                        </div>

                        <p class="mt-3">Do you want to limit the user to taking this survey only once?</p>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="limit_to_one" id="exampleRadios1d" value="1" @if($survey->limit_to_one == 1) checked @endif>
                            <label class="form-check-label" for="exampleRadios1d">
                                Yes
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="limit_to_one" id="exampleRadios2d" value="0" @if($survey->limit_to_one != 1) checked @endif>
                            <label class="form-check-label" for="exampleRadios2d">
                                No
                            </label>
                        </div>

                        <p class="mt-3">Survey visibility.</p>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="visibility" id="exampleRadios1e" value="1" @if($survey->visibility == 1) checked @endif>
                            <label class="form-check-label" for="exampleRadios1e">
                                Public - Everyone will see this survey in their "Other Surveys" tab.
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="visibility" id="exampleRadios2e" value="0" @if($survey->visibility != 1) checked @endif>
                            <label class="form-check-label" for="exampleRadios2e">
                                Private - Only users with the link can take the survey. (Does not prevent users from sharing the link.)
                            </label>
                        </div>

                        <p class="mt-3">Email when survey is completed?</p>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="email_completed" id="exampleRadios1f" value="1" @if($survey->email_completed == 1) checked @endif>
                            <label class="form-check-label" for="exampleRadios1f">
                                Yes
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="email_completed" id="exampleRadios2f" value="0" @if($survey->email_completed != 1) checked @endif>
                            <label class="form-check-label" for="exampleRadios2f">
                                No
                            </label>
                        </div>
                        
                        <input type="submit" class="btn btn-primary btn-block mt-3" value="Update Survey">

                    </div>
                </form>
            </div>
        </div> <!-- CONTACT -->

        <div class="tab-pane fade" id="completed" role="tabpanel" aria-labelledby="completed-tab">
            <div class="alert bg-success mt-3">Completed!</div>
        </div> <!-- COMPLETED -->

        <div class="tab-pane fade mb-3" id="take" role="tabpanel" aria-labelledby="take-tab">
            @include('survey.includes.takesurvey')
        </div> <!-- TAKE -->

        
        
    </div>

</div>

@include('survey.includes.report')

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">

    function myFunction() {
        /* Get the text field */
        var copyText = document.getElementById("exampleFormControlTextarea1");

        /* Select the text field */
        copyText.select();

        /* Copy the text inside the text field */
        document.execCommand("copy");

        /* Alert the copied text */
        //alert("Copied the text: " + copyText.value);
    }

    //show hidden dropdown answers
    document.getElementById('question_type_id').addEventListener('change', function () {
        var style = this.value == 'dropdown' ? 'block' : 'none';
        document.getElementById('hidden_div').style.display = style;
    });



    $(function () {
        $("#table").DataTable();

        $( "#tablecontents" ).sortable({
            items: "tr",
            cursor: 'move',
            opacity: 0.6,
            update: function() {
                sendOrderToServer();
            }
        });

        function sendOrderToServer() {

            var order = [];
            $('tr.row1').each(function(index,element) {
                order.push({
                    id: $(this).attr('data-id'),
                    position: index+1
                });
            });
            //alert(order);

            $.ajax({
                type: "POST",
                dataType: "json",
                url: "{{ url('/survey/order') }}",
                data: {
                    order:order,
                    _token: '{{csrf_token()}}',
                    survey_id:'{{$survey->id}}'
                },
                success: function(response) {
                    if (response.status == "success") {
                        console.log(response);
                    } else {
                        console.log(response);
                    }
                }
            });

        }
    });


    $(document).on("click", ".EditQuestion", function () {
            //alert('Edit Server Button Clicked');
            $('.question_id').val($(this).attr("data-question_id"));
            $('.survey_id').val($(this).attr("data-survey_id"));
            $('.question_type').val($(this).attr("data-question_type"));
            var test = $(this).attr("data-question_type");
            $('.required').val($(this).attr("data-required"));
            $('.title').val($(this).attr("data-title"));
            $('.notes').val($(this).attr("data-question_notes"));
            $('.dropdown').val($(this).attr("data-dropdown_choice[]"));

            $('#edit-question').modal('show');
            //alert($(this).attr("data-id"));
        });


    //Delete Question
        $(document).on("click", ".DeleteQuestion", function () {
            //alert($(this).attr("data-cost_center"));
            var question_id = $(this).attr("data-question_id");
            var survey_id = $(this).attr("data-survey_id");
            DeleteMonthValues(question_id, survey_id);
        });

        function DeleteMonthValues(question_id, survey_id) {
            swal({
                title: "Delete Question?",
                text: "Deleting this question will also delete all the answers associated with it.",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                confirmButtonText: "Yes, delete it!",
                confirmButtonColor: "#ec6c62"
            }, function() {
                $.ajax({
                    url: "/survey/deletequestion/" + question_id + "?survey_id=" + survey_id ,
                    type: "GET"
                })
                    .done(function(data) {
                        swal({
                                title: "Deleted",
                                text: "Question Deleted",
                                type: "success",
                                timer: 1800,
                                showConfirmButton: false

                            }
                        );
                        setTimeout(function(){window.location.replace('/survey/details/'+ survey_id)},1900);
                    })
                    .error(function(data) {
                        swal("Oops", "We couldn't connect to the server!", "error");
                    });
            });
        }

</script>


            


@endsection
