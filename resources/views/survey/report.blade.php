
@extends('layouts.app')

@section('content')


    @foreach($questions as $question)
    <div class="card mb-3 text-white bg-dark">
        <div class="card-header">
            <h5>{{$question->title}}</h5>
        </div>

        @if($question->question_type == 'Short Answer')
            <div class="card-body">
                <div style="height:500px; overflow-y:auto; width:90%" class="mx-auto">
                    <table class="table table-dark table-bordered table-hover">
                        <thead>
                            <th>Participant</th>
                            <th>Response</th>
                            <th>Submit Date</th>
                        </thead>
                        <tbody>
                            @foreach($short->where('question_id', $question->id) as $answer)
                                <tr>
                                    <td>{{$answer->submittedBy->name or ''}}</td>
                                    <td>{{str_replace(array('\r', '\n','\\'), array(chr(13), chr(10),'&#x22;'), $answer->answer_string)}}{{$answer->answer}}</td>
                                    <td>{{Carbon\Carbon::parse($answer->created_at)->format('M jS, Y')}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <br><b>Type:</b> {{$question->question_type}}
            </div>

        @elseif($question->question_type == 'employee')

                <h3 class="jumbotron bg-secondary">Employee Selection</h3>
                <a href="/survey/empresults/{{$survey->id}}/{{$question->id}}" class="btn btn-primary btn-block mb-2">View Results</a>

                <b>Type:</b> {{$question->question_type}}

        @elseif($question->question_type == 'date_time')

                <h3 class="jumbotron bg-secondary">Date & Time selection</h3>
                <a href="/survey/datetime_results/{{$survey->id}}/{{$question->id}}" class="btn btn-primary btn-block mb-2">View Results</a>

                <b>Type:</b> {{$question->question_type}}

        @elseif($question->question_type == 'cost_center')
            
                <h3 class="jumbotron bg-secondary">Cost Center Selection</h3>
                <a href="/survey/ccresults/{{$survey->id}}/{{$question->id}}" class="btn btn-primary btn-block mb-2">View Results</a>

                <b>Type:</b> {{$question->question_type}}
                
        @elseif($question->question_type == 'YesNoNA')
            <div class="card-body">
                <canvas id="{{$question->id}}_1" height="50"></canvas>
                <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.js"></script>
                <script type="application/javascript">

                    new Chart(document.getElementById('{{$question->id}}_1'), {
                        type: 'bar',

                        data: {
                            labels: [
                                @foreach($yesnoNA->where('question_id', $question->id) as $answer)
                                    @if($answer->answer == 1)
                                        "Yes",
                                    @elseif($answer->answer == 0)
                                        "No",
                                    @else
                                        "N/A",
                                    @endif
                                @endforeach
                            ],
                            datasets: [{
                                label: "",
                                backgroundColor: [
                                    @foreach($yesnoNA->where('question_id', $question->id) as $answer)
                                        @if($answer->answer == 1)
                                            'rgba(0, 99, 132, 0.6)',
                                        @elseif($answer->answer == 0)
                                            'rgba(80, 70, 188, 0.6)',
                                        @else
                                            'rgba(168, 70, 200, 0.6)',
                                        @endif
                                    @endforeach
                                ],
                                data: [

                                    @foreach($yesnoNA->where('question_id', $question->id) as $answer)
                                        {{$answer->total}},
                                    @endforeach
                                ]
                            }]
                        },
                        options: {
                            legend: { display: false },
                            title: {
                                //display: true,
                                //text: 'Predicted world population (millions) in 2050'
                            },
                            scales: {
                                yAxes: [{
                                    stacked:false,
                                ticks: {
                                    beginAtZero: true
                                    }
                                }]
                            }
                        }
                    });
                </script> 

                <canvas id="{{$question->id}}_2" height="50"></canvas>
                <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.js"></script>
                <script type="application/javascript">

                    new Chart(document.getElementById('{{$question->id}}_2'), {
                        type: 'pie',

                        data: {
                            labels: [
                                @foreach($yesnoNA->where('question_id', $question->id) as $answer)
                                    @if($answer->answer == 1)
                                        "Yes",
                                    @elseif($answer->answer == 0)
                                        "No",
                                    @else
                                        "NA",
                                    @endif
                                @endforeach
                            ],
                            datasets: [{
                                label: "",
                                backgroundColor: [
                                    @foreach($yesnoNA->where('question_id', $question->id) as $answer)
                                        @if($answer->answer == 1)
                                            'rgba(0, 260, 132, 0.6)',
                                        @elseif($answer->answer == 0)
                                            'rgba(40, 150, 265, 0.6)',
                                        @else
                                            'rgba(192, 80, 188, 0.6)',
                                        @endif
                                    @endforeach
                                ],
                                data: [

                                    @foreach($yesnoNA->where('question_id', $question->id) as $answer)
                                        {{$answer->total}},
                                    @endforeach
                                ]
                            }]
                        },
                        options: 
                        {
                            // scales: {
                            //     xAxes: [{
                            //         ticks: {
                            //             beginAtZero: true
                            //         }
                            //     }]
                            // }, --for bar charts                         
                            title: 
                            {
                                display: true,
                                //text: 'Anything Else'
                            }
                        }
                        
                    });

                </script>
                <b>Type:</b> {{$question->question_type}}
            </div>
        @elseif($question->question_type == 'YesNo')
            <div class="card-body">
                <canvas id="{{$question->id}}_1" height="50"></canvas>
                <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.js"></script>
                <script type="application/javascript">

                    new Chart(document.getElementById('{{$question->id}}_1'), {
                        type: 'pie',

                        data: {
                            labels: [
                                @foreach($yesno->where('question_id', $question->id) as $answer)
                                    @if($answer->answer == 1)
                                        "Yes",
                                    @else
                                        "No"
                                    @endif
                                @endforeach
                            ],
                            datasets: [{
                                label: "",
                                backgroundColor: [
                                    @foreach($yesno->where('question_id', $question->id) as $answer)
                                        @if($answer->answer == 1)
                                            'rgba(0, 99, 132, 0.6)',
                                        @else
                                            'rgba(124, 86, 188, 0.6)'
                                        @endif
                                    @endforeach
                                ],
                                data: [

                                    @foreach($yesno->where('question_id', $question->id) as $answer)
                                        {{$answer->total}},
                                    @endforeach
                                ]
                            }]
                        },
                        options: 
                        {                     
                            title: 
                            {
                                display: true,
                                //text: 'Anything Else'
                            }
                        }
                    });
                </script>

                <br>

                <canvas id="{{$question->id}}_2" height="50"></canvas>
                <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.js"></script>
                <script type="application/javascript">

                    new Chart(document.getElementById('{{$question->id}}_2'), {
                        type: 'bar',

                        data: {
                            labels: [
                                @foreach($yesno->where('question_id', $question->id) as $answer)
                                    @if($answer->answer == 1)
                                        "Yes",
                                    @else
                                        "N/A",
                                    @endif
                                @endforeach
                            ],
                            datasets: [{
                                label: "",
                                backgroundColor: [
                                    @foreach($yesno->where('question_id', $question->id) as $answer)
                                        @if($answer->answer == 1)
                                            'rgba(0, 99, 132, 0.6)',
                                        @else
                                            'rgba(168, 70, 200, 0.6)',
                                        @endif
                                    @endforeach
                                ],
                                data: [

                                    @foreach($yesno->where('question_id', $question->id) as $answer)
                                        {{$answer->total}},
                                    @endforeach
                                ]
                            }]
                        },
                        options: {
                            legend: { display: false },
                            title: {
                                //display: true,
                                //text: 'Predicted world population (millions) in 2050'
                            },
                            scales: {
                                yAxes: [{
                                    stacked:false,
                                ticks: {
                                    beginAtZero: true
                                    }
                                }]
                            }
                        }
                    });
                </script>
                <b>Type:</b> {{$question->question_type}}
            </div>

        @elseif($question->question_type == 'Scale_5')
            <div class="card-body">
                <canvas id="{{$question->id}}" height="50"></canvas>
                <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.js"></script>
                <script type="application/javascript">
                
                    new Chart(document.getElementById('{{$question->id}}'), {
                        type: 'pie',

                        data: {
                            labels: [
                                @foreach($scale5->where('question_id', $question->id) as $answer)
                                    {{$answer->answer}},
                                @endforeach

                            ],
                            datasets: [{
                                label: "Scale 1-5",
                                backgroundColor: [
                                    'rgba(0, 99, 132, 0.6)',
                                    'rgba(30, 99, 132, 0.6)',
                                    'rgba(60, 99, 132, 0.6)',
                                    'rgba(90, 99, 132, 0.6)',
                                    'rgba(120, 99, 132, 0.6)',
                                    'rgba(150, 99, 132, 0.6)',
                                    'rgba(180, 99, 132, 0.6)',
                                    'rgba(210, 99, 132, 0.6)',
                                ],
                                data: [

                                    @foreach($scale5->where('question_id', $question->id) as $answer)
                                    {{$answer->total}},
                                    @endforeach
                                ]
                            }]
                        },
                        options: 
                        {
                            title: 
                            {
                                display: true,
                                //text: 'Anything Else'
                            }
                        }
                    });
                </script>
                <b>Type:</b> {{$question->question_type}}
            </div>

        @elseif($question->question_type == 'dropdown')
            <div class="card-body">
                <canvas id="{{$question->id}}" height="50"></canvas>
                <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.js"></script>
                <script type="application/javascript">
                
                    new Chart(document.getElementById('{{$question->id}}'), {
                        type: 'pie',

                        data: {
                            labels: [
                                @foreach($joinDrop->where('question_id', $question->id) as $answer)
                                    @if(!is_null($answer->dropdown_choice))
                                        "{{$answer->dropdown_choice}}",
                                    @endif
                                @endforeach

                            ],
                            datasets: [{
                                label: "Dropdown",
                                backgroundColor: [
                                    'rgba(0, 99, 132, 0.6)',
                                    'rgba(165, 140, 220, 0.6)',
                                    'rgba(60, 200, 300, 0.6)',
                                    'rgba(90, 99, 132, 0.6)',
                                    'rgba(120, 99, 132, 0.6)',
                                    'rgba(145, 99, 132, 0.6)',
                                    'rgba(170, 99, 132, 0.6)',
                                    'rgba(200, 99, 132, 0.6)',
                                    'rgba(225, 99, 132, 0.6)',
                                    'rgba(255, 99, 132, 0.6)'
                                ],
                                data: [

                                    @foreach($joinDrop->where('question_id', $question->id) as $answer)
                                        @if(!is_null($answer->dropdown_choice))
                                            {{$answer->total_count}},
                                        @endif
                                    @endforeach
                                ]
                            }]
                        },
                        options: {
                            title: {
                                display: true,
                                //text: 'Anything Else'
                            }
                        }
                    });
                </script>
                                
                <br>

                <canvas id="{{$question->id}}_2" height="50"></canvas>
                <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.js"></script>
                <script type="application/javascript">

                    new Chart(document.getElementById('{{$question->id}}_2'), {
                        type: 'bar',

                        data: {
                            labels: [
                                @foreach($joinDrop->where('question_id', $question->id) as $answer)
                                    @if(!is_null($answer->dropdown_choice))
                                        "{{$answer->dropdown_choice}}",
                                    @endif
                                @endforeach
                            ],
                            datasets: [{
                                label: "",
                                backgroundColor: [
                                    'rgba(0, 99, 132, 0.6)',
                                    'rgba(165, 140, 220, 0.6)',
                                    'rgba(60, 200, 300, 0.6)',
                                    'rgba(90, 99, 132, 0.6)',
                                    'rgba(120, 99, 132, 0.6)',
                                    'rgba(145, 99, 132, 0.6)',
                                    'rgba(170, 99, 132, 0.6)',
                                    'rgba(200, 99, 132, 0.6)',
                                    'rgba(225, 99, 132, 0.6)',
                                    'rgba(255, 99, 132, 0.6)',
                                ],
                                data: [

                                    @foreach($joinDrop->where('question_id', $question->id) as $answer)
                                        @if(!is_null($answer->dropdown_choice))
                                            {{$answer->total_count}},
                                        @endif
                                    @endforeach
                                ]
                            }]
                        },
                        options: {
                            legend: { display: false },
                            title: {
                                //display: true,
                                //text: 'Predicted world population (millions) in 2050'
                            },
                            scales: {
                                yAxes: [{
                                    stacked:false,
                                ticks: {
                                    beginAtZero: true
                                    }
                                }]
                            }
                        }
                    });
                </script>
                <b>Type:</b> {{$question->question_type}}
            </div>
                   
        @elseif($question->question_type == 'Scale_10')
            <div class="card-body">
                <canvas id="{{$question->id}}" height="50"></canvas>
                <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.js"></script>
                <script type="application/javascript">
                
                    new Chart(document.getElementById('{{$question->id}}'), {
                        type: 'pie',

                        data: {
                            labels: [
                                @foreach($scale10->where('question_id', $question->id) as $answer)
                                {{$answer->answer}},
                                @endforeach

                            ],
                            datasets: [{
                                label: "Scale 1-10",
                                backgroundColor: [
                                    'rgba(0, 99, 132, 0.6)',
                                    'rgba(30, 99, 132, 0.6)',
                                    'rgba(60, 99, 132, 0.6)',
                                    'rgba(90, 99, 132, 0.6)',
                                    'rgba(120, 99, 132, 0.6)',
                                    'rgba(150, 99, 132, 0.6)',
                                    'rgba(180, 99, 132, 0.6)',
                                    'rgba(210, 99, 132, 0.6)',
                                ],
                                data: [

                                    @foreach($scale10->where('question_id', $question->id) as $answer)
                                    {{$answer->total}},
                                    @endforeach
                                ]
                            }]
                        },
                        options: {
                            title: {
                                display: true,
                                //text: 'Anything Else'
                            }
                        }

                    });
                </script>
                <b>Type:</b> {{$question->question_type}}
            </div>

        @endif

    </div> {{-- Main Card --}}

    @endforeach

@endsection