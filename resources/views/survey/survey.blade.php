{{--New file Template--}}

{{--Add Security for this page below--}}

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="card mb-4 border-info text-white bg-dark col-12">
        <div class="card-header">{{$survey->survey_name}}</div>
        <div class="card-body">
            @include('survey.includes.takesurvey')
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
