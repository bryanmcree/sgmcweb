
<div class="alert bg-danger mt-3">
    <h4>Alert: This survey application will be depreciated on November 1, 2019. The new location for surveys will be here: 
        <a href="/audit" class="text-white">web.sgmc.org/audit</a>
    </h4>
</div>

<form method="post" action="/survey/submit">
    {{ csrf_field() }}
    <input type="hidden" value="{{ Auth::user()->employee_number }}" name="submitted_by">
    <input type="hidden" value="{{$survey->id}}" name="survey_id">
    <table class="table bg-dark text-white table-bordered mb-4 mt-4">
        <thead>
        <tr>
            <td></td>
            <td><b>Question</b></td>
        </tr>
        <?php $rowCount = 0 ?>
        </thead>
        <tbody>
        @foreach($questions as $question)
            <input type="hidden" value="{{$question->question_type}}" name="{{$question->id}}[question_type]">
            <?php $rowCount = $rowCount +1 ?>

            <tr>
                <td><b>{{$rowCount}}.</b></td>
                <td>
                    <!-- Loop through the questions -->
                    {{$question->title}}
                    <br>
                    <!-- Display input depending on the type of question -->
                    @if($question->question_type == 'Short Answer')
                        <p><b><i>Short Answer</i></b></p>
                        @if($question->required == 1) <p style="color: red"><i class="fas fa-exclamation-triangle"></i> <b>REQUIRED</b></p> @endif
                        <div class="col">
                            <div class="form-group">
                                <textarea class="form-control" id="exampleFormControlTextarea1" name="{{$question->id}}[answer]" rows="3" @if($question->required == 1) required @endif></textarea>
                            </div>
                        </div>
                        @if($survey->show_question_notes == 1)
                            <div class="col">
                                <p><i>{{$question->question_notes}}</i></p>
                            </div>
                        @endif
                    @elseif($question->question_type == 'YesNo')
                        <p><b><i>Yes or No</i></b></p>
                        @if($question->required == 1) <p style="color: red"><i class="fas fa-exclamation-triangle"></i> <b>REQUIRED</b></p> @endif
                        <div class="col">
                            <div class="form-check form-check-inline">
                                <input style="width:30px; height:30px;" class="form-check-input" type="radio" name="{{$question->id}}[answer]" id="inlineRadio{{$question->id}}" value="1" @if($question->required == 1) required @endif>
                                <label style="font-size: 30px;" class="form-check-label" for="inlineRadio{{$question->id}}">Yes</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input style="width:30px; height:30px;" class="form-check-input" type="radio" name="{{$question->id}}[answer]" id="inlineRadio2{{$question->id}}" value="0" @if($question->required == 1) required @endif>
                                <label style="font-size: 30px;" class="form-check-label" for="inlineRadio2{{$question->id}}">No</label>
                            </div>
                        </div>
                        @if($survey->show_question_notes ==1)
                            <div class="col">
                                <p><i>{{$question->question_notes}}</i></p>
                            </div>
                        @endif
                    @elseif($question->question_type == 'YesNoNA')
                        <p><b><i>Yes, No, N/A</i></b></p>
                        @if($question->required == 1) <p style="color: red"><i class="fas fa-exclamation-triangle"></i> <b>REQUIRED</b></p> @endif
                        <div class="col">
                            <div class="form-check form-check-inline">
                                <input style="width:30px; height:30px;" class="form-check-input" type="radio" name="{{$question->id}}[answer]" id="inlineRadio{{$question->id}}" value="1" @if($question->required == 1) required @endif>
                                <label style="font-size: 30px;" class="form-check-label" for="inlineRadio{{$question->id}}">Yes</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input style="width:30px; height:30px;" class="form-check-input" type="radio" name="{{$question->id}}[answer]" id="inlineRadio2{{$question->id}}" value="0" @if($question->required == 1) required @endif>
                                <label style="font-size: 30px;" class="form-check-label" for="inlineRadio2{{$question->id}}">No</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input style="width:30px; height:30px;" class="form-check-input" type="radio" name="{{$question->id}}[answer]" id="inlineRadio2{{$question->id}}" value="2" @if($question->required == 1) required @endif>
                                <label style="font-size: 30px;" class="form-check-label" for="inlineRadio2{{$question->id}}">N/A</label>
                            </div>
                        </div>
                        @if($survey->show_question_notes ==1)
                            <div class="col">
                                <p><i>{{$question->question_notes}}</i></p>
                            </div>
                        @endif

                    @elseif($question->question_type == 'Scale_5')
                        <p><b><i>Scale 1-5</i></b></p>
                        @if($question->required == 1) <p style="color: red"><i class="fas fa-exclamation-triangle"></i> <b>REQUIRED</b></p> @endif
                        <div class="col">
                            <div class="form-group">
                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                    <label class="btn btn-lg btn-default btn-secondary">
                                        <input type="radio" name="{{$question->id}}[answer]" value="1" id="option2" autocomplete="off" @if($question->required == 1) required @endif> <b>1</b>
                                    </label>
                                    <label class="btn btn-lg btn-default btn-secondary">
                                        <input type="radio" name="{{$question->id}}[answer]" value="2" id="option3" autocomplete="off" @if($question->required == 1) required @endif> <b>2</b>
                                    </label>
                                    <label class="btn btn-lg btn-default btn-secondary">
                                        <input type="radio" name="{{$question->id}}[answer]" value="3" id="option3" autocomplete="off" @if($question->required == 1) required @endif> <b>3</b>
                                    </label>
                                    <label class="btn btn-lg btn-default btn-secondary">
                                        <input type="radio" name="{{$question->id}}[answer]" value="4" id="option3" autocomplete="off" @if($question->required == 1) required @endif> <b>4</b>
                                    </label>
                                    <label class="btn btn-lg btn-default btn-secondary">
                                        <input type="radio" name="{{$question->id}}[answer]" value="5" id="option3" autocomplete="off" @if($question->required == 1) required @endif> <b>5</b>
                                    </label>
                                </div>
                            </div>
                        </div>
                        @if($survey->show_question_notes ==1)
                            <div class="col">
                                <p><i>{{$question->question_notes}}</i></p>
                            </div>
                        @endif

                    @elseif($question->question_type == 'Scale_10')
                        <p><b><i>Scale 1-10</i></b></p>
                        @if($question->required == 1) <p style="color: red"><i class="fas fa-exclamation-triangle"></i> <b>REQUIRED</b></p> @endif
                        <div class="col">
                            <div class="form-group">
                                <div class="form-group">
                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                        <label class="btn btn-lg btn-default btn-secondary">
                                            <input type="radio" name="{{$question->id}}[answer]" value="1" id="option2" autocomplete="off" @if($question->required == 1) required @endif> <b>1</b>
                                        </label>
                                        <label class="btn btn-lg btn-default btn-secondary">
                                            <input type="radio" name="{{$question->id}}[answer]" value="2" id="option3" autocomplete="off" @if($question->required == 1) required @endif> <b>2</b>
                                        </label>
                                        <label class="btn btn-lg btn-default btn-secondary">
                                            <input type="radio" name="{{$question->id}}[answer]" value="3" id="option3" autocomplete="off" @if($question->required == 1) required @endif> <b>3</b>
                                        </label>
                                        <label class="btn btn-lg btn-default btn-secondary">
                                            <input type="radio" name="{{$question->id}}[answer]" value="4" id="option3" autocomplete="off" @if($question->required == 1) required @endif> <b>4</b>
                                        </label>
                                        <label class="btn btn-lg btn-default btn-secondary">
                                            <input type="radio" name="{{$question->id}}[answer]" value="5" id="option3" autocomplete="off" @if($question->required == 1) required @endif> <b>5</b>
                                        </label>
                                        <label class="btn btn-lg btn-default btn-secondary">
                                            <input type="radio" name="{{$question->id}}[answer]" value="6" id="option3" autocomplete="off" @if($question->required == 1) required @endif> <b>6</b>
                                        </label>
                                        <label class="btn btn-lg btn-default btn-secondary">
                                            <input type="radio" name="{{$question->id}}[answer]" value="7" id="option3" autocomplete="off" @if($question->required == 1) required @endif> <b>7</b>
                                        </label>
                                        <label class="btn btn-lg btn-default btn-secondary">
                                            <input type="radio" name="{{$question->id}}[answer]" value="8" id="option3" autocomplete="off" @if($question->required == 1) required @endif> <b>8</b>
                                        </label>
                                        <label class="btn btn-lg btn-default btn-secondary">
                                            <input type="radio" name="{{$question->id}}[answer]" value="9" id="option3" autocomplete="off" @if($question->required == 1) required @endif> <b>9</b>
                                        </label>
                                        <label class="btn btn-lg btn-default btn-secondary">
                                            <input type="radio" name="{{$question->id}}[answer]" value="10" id="option3" autocomplete="off" @if($question->required == 1) required @endif> <b>10</b>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if($survey->show_question_notes ==1)
                            <div class="col">
                                <p><i>{{$question->question_notes}}</i></p>
                            </div>
                        @endif

                    @elseif($question->question_type == 'cost_center')
                        <p><b><i>Scale 1-10</i></b></p>
                        @if($question->required == 1) <p style="color: red"><i class="fas fa-exclamation-triangle"></i> <b>REQUIRED</b></p> @endif
                        <div class="col">
                            <div class="form-group">
                                <select name="{{$question->id}}[answer]" class="form-control" id="exampleFormControlSelect1" @if($question->required == 1) required @endif>
                                    <option value="">[Select Cost Center]</option>
                                    @foreach($cost_centers as $cost_center)
                                        <option value="{{$cost_center->cost_center}}">{{$cost_center->style1}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @if($survey->show_question_notes ==1)
                            <div class="col">
                                <p><i>{{$question->question_notes}}</i></p>
                            </div>
                        @endif

                    @elseif($question->question_type == 'dropdown')
                        <p><b><i>Drop Down Select</i></b></p>
                        @if($question->required == 1) <p style="color: red"><i class="fas fa-exclamation-triangle"></i> <b>REQUIRED</b></p> @endif
                        <div class="col">
                            <div class="form-group">
                                <select name="{{$question->id}}[answer]" class="form-control" id="exampleFormControlSelect1" @if($question->required == 1) required @endif>
                                    <option value="">[Select]</option>
                                    @foreach($dropdown as $option)
                                        @if($question->id == $option->question_id)<option value="{{$option->id}}">{{$option->dropdown_choice}}</option>@endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @if($survey->show_question_notes ==1)
                            <div class="col">
                                <p><i>{{$question->question_notes}}</i></p>
                            </div>
                        @endif

                    @elseif($question->question_type == 'date_time')
                        <p><b><i>Enter date AND time</i></b></p>
                        @if($question->required == 1) <p style="color: red"><i class="fas fa-exclamation-triangle"></i> <b>REQUIRED</b></p> @endif
                        <div class="col">
                            <div class="form-group">
                                <input type="datetime-local" name="{{$question->id}}[answer]" class="form-control" @if($question->required == 1) required @endif>
                            </div>
                        </div>
                        @if($survey->show_question_notes ==1)
                            <div class="col">
                                <p><i>{{$question->question_notes}}</i></p>
                            </div>
                        @endif

                    @elseif($question->question_type == 'employee')
                        <p><b><i>Employee Search</i></b> - Remember to click on employees name once they appear.</p>
                        @if($question->required == 1) <p style="color: red"><i class="fas fa-exclamation-triangle"></i> <b>REQUIRED</b></p> @endif
                        <div class="col">
                            <div class="form-group">
                                <input type="hidden" name="{{$question->id}}[answer]" id="employee_value_{{$question->id}}" @if($question->required == 1) required @endif>
                                <input type="search" id="autocomplete_{{$question->id}}" class="form-control" placeholder="Search"  @if($question->required == 1) required @endif>
                            </div>
                        </div>
                        @if($survey->show_question_notes ==1)
                            <div class="col">
                                <p><i>{{$question->question_notes}}</i></p>
                            </div>
                        @endif
                        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
                        {!! Html::script('js/jquery.autocomplete.js') !!}
                        <script type="application/javascript">
                            $('#autocomplete_{{$question->id}}').autocomplete({
                                serviceUrl: '/survey/employeesearch',
                                dataType: 'json',
                                type:'GET',
                                width: 418,
                                minChars:2,
                                onSelect: function(suggestion) {
                                    //alert(suggestion.data);
                                    $("#employee_value_{{$question->id}}").val(suggestion.data);
                                }


                            });
                        </script>
                    @endif
                </td>
            </tr>
        @endforeach
        <tr>
            <td colspan="3"><input type="submit" class="btn btn-primary btn-block" value="Submit Survey"></td>
        </tr>
        </tbody>
    </table>
</form>