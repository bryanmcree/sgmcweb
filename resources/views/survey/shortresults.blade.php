{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Page Design Goes Below--}}
@section('content')

<div class="card bg-dark text-white mb-3">
    <div class="card-body">
        <table class="table table-condensed table-striped table-hover" id="shortAnswer_{{$survey->id}}">
            <thead>
            <tr>
                <td><b>Participant</b></td>
                <td><b>Response</b></td>
                <td><b>Submit Date</b></td>
                <td class="no-sort"><b></b></td>
            </tr>
            </thead>
            <tbody>
            @foreach($results as $result)
                <tr>
                    <td>{{$result->submittedBy->name or ''}}</td>
                    <td>{{str_replace(array('\r', '\n','\\'), array(chr(13), chr(10),'&#x22;'), $result->answer_string)}}{{$result->answer}}</td>
                    <td>{{Carbon\Carbon::parse($result->created_at)->format('M jS, Y')}}</td>
                    <td align="right"><a href="/survey/print/{{$survey->id}}?incident_id={{$result->incident_id}}" target="_blank"><i class="fas fa-eye" id="printbtn"></i></a></td>
                </tr>
            @endforeach
            </tbody>
        </table> 
    </div>
</div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

<script>
    $(document).ready(function() {
        $('#shortAnswer_{{$survey->id}}').DataTable({
            "pageLength": 30,
            "autoWidth": true,
            "ordering": false,
            "columnDefs": [
                { targets: 'no-sort', orderable: false }
            ],
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                if ( aData[1] == "" )
                {
                    $('td', nRow).css('background-color', '#b53838');
                    $('td:eq(1)', nRow).html('<b>NO RESPONSE</b>');
                    $('#printbtn', nRow).css('display', 'none');
                }
            }
        });
    });
</script>

@endsection