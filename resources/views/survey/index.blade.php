{{--New file Template--}}

{{--Add Security for this page below--}}

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="alert bg-danger mt-3">
        <h4>Alert: This survey application will be depreciated on November 1, 2019. The new location for surveys will be here: 
            <a href="/audit" class="text-white">web.sgmc.org/audit</a>
        </h4>
    </div>

    <div class="card text-white bg-dark mb-3">
        <div class="card-header">
            <h4>Surveys</h4>
        </div>

        <div class="card-body">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Dashboard</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">My Surveys</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Other Surveys</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="build-tab" data-toggle="tab" href="#build" role="tab" aria-controls="build" aria-selected="false">Create Survey</a>
                </li>
            </ul>

            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <br>
                    <div class="card-deck col-lg-12">
                        <div class="card mb-3 text-white bg-secondary">
                            <div class="card-header">Total Surveys</div>
                            <div class="card-body"> {{$user_survey->count()}}</div>
                        </div>
                        <div class="card mb-3 text-white bg-secondary">
                            <div class="card-header">Total Active Surveys</div>
                            <div class="card-body">{{$user_survey->where('active',1)->count()}}</div>
                        </div>
                    </div>
                </div> <!-- HOME -->

                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <div class="card mb-3 mt-3 text-white bg-secondary">
                        <div class="card-header"> 
                            <h3>My Surveys ({{$user_survey->count()}})</h3>
                            <p>All current users surveys</p>
                        </div>
                        <div class="card-body">
                            <table class="table table-condensed table-hover table-dark" width="100%" id="mySurveys">
                                <thead>
                                <tr>
                                    <td><b>Survey Name</b></td>
                                    <td class="d-none d-sm-table-cell"><b>Visibility</b></td>
                                    <td class="d-none d-sm-table-cell" align="center"><b>Taken</b></td>
                                    <td class="d-none d-sm-table-cell" align="center"><b>Active</b></td>
                                    <td class="d-none d-sm-table-cell"><b>Created By</b></td>
                                    <td class="d-none d-sm-table-cell"><b>Created At</b></td>
                                    <td class="no-sort"></td>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($user_survey as $my_survey)
                                    <tr>
                                        <td>{{$my_survey->survey_name}}</td>
                                        <td class="d-none d-sm-table-cell" align="center">@if($my_survey->visibility ==0) <i class="fas fa-user" style="color:red;"></i>  @else <i class="fas fa-users" style="color:green;"></i> @endif</td>
                                        <td class="d-none d-sm-table-cell" align="center">{{$taken->where('survey_id',$my_survey->id)->count()}}</td>
                                        <td class="d-none d-sm-table-cell" align="center">@if($my_survey->active ==0) <i class="far fa-times-circle" style="color:red;"></i>  @else <i class="far fa-check-circle" style="color:green;"></i> @endif</td>
                                        <td class="d-none d-sm-table-cell">{{$my_survey->createdBy->name}}</td>
                                        <td class="d-none d-sm-table-cell">{{$my_survey->created_at}}</td>
                                        <td align="right">
                                            <div class="btn-group" role="group" aria-label="Basic example">
                                                <a href="/survey/details/{{$my_survey->id}}" class="btn btn-sm btn-primary d-none d-sm-block"><i class="fas fa-edit"></i></a>
                                                <a href="/survey/{{$my_survey->id}}" title="Take Survey" target="_blank" class="btn btn-sm btn-primary"><i class="fas fa-clipboard-check"></i></a>
                                                <a href="#" class="btn btn-danger btn-sm DeleteSurvey d-none d-sm-block" title="Delete"
                                                   data-survey_id="{{$my_survey->id}}"
                                                ><i class="fas fa-trash-alt"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div> <!-- PROFILE -->

                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                    <div class="card mb-3 mt-3 text-white bg-secondary">
                        <div class="card-header">
                            <h3>Shared Surveys</h3>
                            <p>Shared surveys available to take.</p>
                        </div>
                        <div class="card-body">
                            <table class="table table-condensed table-hover table-dark" width="100%" id="otherSurveys">
                                <thead>
                                <tr>
                                    <td><b>Survey Name</b></td>
                                    <td class="d-none d-sm-table-cell"><b>Visibility</b></td>
                                    <td class="d-none d-sm-table-cell" align="center"><b>Taken</b></td>
                                    <td class="d-none d-sm-table-cell" align="center"><b>Active</b></td>
                                    <td class="d-none d-sm-table-cell"><b>Created By</b></td>
                                    <td class="d-none d-sm-table-cell"><b>Created At</b></td>
                                    <td class="no-sort"></td>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($public_survey as $my_survey)
                                    <tr>
                                        <td>{{$my_survey->survey_name}}</td>
                                        <td class="d-none d-sm-table-cell" align="center">@if($my_survey->visibility ==0) <i class="fas fa-user" style="color:red;"></i>  @else <i class="fas fa-users" style="color:green;"></i> @endif</td>
                                        <td class="d-none d-sm-table-cell" align="center">{{$taken->where('survey_id',$my_survey->id)->count()}}</td>
                                        <td class="d-none d-sm-table-cell" align="center">@if($my_survey->active ==0) <i class="far fa-times-circle" style="color:red;"></i>  @else <i class="far fa-check-circle" style="color:green;"></i> @endif</td>
                                        <td class="d-none d-sm-table-cell">{{$my_survey->createdBy->name}}</td>
                                        <td class="d-none d-sm-table-cell">{{$my_survey->created_at}}</td>
                                        <td align="right">
                                            <div class="btn-group" role="group" aria-label="Basic example">
                                                <a href="/survey/details/{{$my_survey->id}}" title="Survey Details" class="btn btn-sm btn-primary"><i class="fas fa-edit" ></i></a>
                                                <a href="/survey/{{$my_survey->id}}" title="Take Survey" target="_blank" class="btn btn-sm btn-primary"><i class="fas fa-clipboard-check"></i></a>
                                                @if ( Auth::user()->hasRole('Survey Admin'))
                                                    <a href="#" class="btn btn-danger btn-sm DeleteSurvey d-none d-sm-block" title="Delete"
                                                       data-survey_id="{{$my_survey->id}}"
                                                    ><i class="fas fa-trash-alt"></i></a>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div> <!-- CONTACT -->

                <div class="tab-pane fade" id="build" role="tabpanel" aria-labelledby="build-tab">
                    <div class="card mb-3 mt-3 text-white bg-secondary" style="width: 40%">
                        <div class="card-header">
                            <h3>Build Survey</h3>
                        </div>
                        <div class="card-body">
                            <form method="post" action="/survey/new/create">
                                {{ csrf_field() }}
                                <input type="hidden" name="created_by" value="{{ Auth::user()->employee_number }}">
                                <input type="hidden" name="survey_redirect" value="0">
                                <div class="form-group">
                                    <label for="survey_name_id">Survey Title:</label>
                                    <input type="text" class="form-control" id="survey_name_id" name ='survey_name'  required>
                                </div>
                                <div class="form-group">
                                    <label for="survey_description_id">Survey Description:</label>
                                    <textarea class="form-control" id="survey_description_id" name="survey_description"  required></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="visibility_id">Who do you want to see this survey?</label>
                                    <select name="visibility" class="form-control" id="visibility_id" required>
                                        <option value="" selected>[Select Visibility]</option>
                                        <option value="0">Only You</option>
                                        <option value="1">Everyone in your Organization</option>
                                    </select>
                                </div>
                                <input type="submit" class="btn btn-primary" value="Save and Continue">
                            </form>
                        </div>
                    </div>
                </div> <!-- BUILD -->

            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="application/javascript">
        $(document).ready(function() {
            $('#mySurveys').DataTable( {
                "pageLength": 10,
                "columnDefs": [ {
                    "targets"  : 'no-sort',
                    "orderable": false,
                }]
            } );
        } );

        $(document).ready(function() {
            $('#otherSurveys').DataTable( {
                "pageLength": 10,
                "columnDefs": [ {
                    "targets"  : 'no-sort',
                    "orderable": false,
                }]
            } );
        } );


        //Delete Question
        $(document).on("click", ".DeleteSurvey", function () {
            //alert($(this).attr("data-cost_center"));
            var survey_id = $(this).attr("data-survey_id");
            DeleteMonthValues(survey_id);
        });

        function DeleteMonthValues(survey_id) {
            swal({
                title: "Delete Survey?",
                text: "Deleting this survey will delete all the questions and answers.",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                confirmButtonText: "Yes, delete it!",
                confirmButtonColor: "#ec6c62"
            }, function() {
                $.ajax({
                    url: "/survey/deletesurvey/" + survey_id ,
                    type: "GET"
                })
                    .done(function(data) {
                        swal({
                                title: "Deleted",
                                text: "Question Deleted",
                                type: "success",
                                timer: 1800,
                                showConfirmButton: false

                            }
                        );
                        setTimeout(function(){window.location.replace('/survey')},1900);
                    })
                    .error(function(data) {
                        swal("Oops", "We couldn't connect to the server!", "error");
                    });
            });
        }



    </script>

@endsection
