{{--New file Template--}}

{{--Add Security for this page below--}}


    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="card bg-light">
            <div class="card-header">
                <h1><b>Thank You!</b></h1>
            </div>
            <div class="card-body">
                @if(is_null($user_check))
                Thank you for your input!  Your results have been recorded.
                @else
                Sorry!  You are only allowed one submission!
                @endif
            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
