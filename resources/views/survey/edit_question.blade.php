

@extends('layouts.app')
{{--Page Design Goes Below--}}
@section('content')

<div class="card bg-dark text-white mb-3">
    <div class="card-header">
        Edit Question 
        <span class="float-right"><a href="/"></a></span>
    </div>
    <div class="card-body">

        <form method="POST" action="/survey/question/update/{{$question->id}}">
            {{ csrf_field() }}
            <input type="hidden" value="{{$survey->id}}" name="survey_id">
            <input type="hidden" value="{{ Auth::user()->employee_number }}" name="created_by">

            <div class="form-group">
                <label for="question_type">Question Type</label>
                <select class="form-control" id="edit_question_type" name="question_type" required>
                    <option value="{{$question->question_type}}" selected>{{$question->question_type}}</option>
                    <option value="YesNo">Yes/No</option>
                    <option value="YesNoNA">Yes/No/NA</option>
                    <option value="Short Answer">Short Answer</option>
                    <option value="Scale_5">Scale 1-5</option>
                    <option value="Scale_10">Scale 1-10</option>
                    <option value="dropdown">Drop Down</option>
                    <option value="date_time">Date and Time</option>
                    <option value="cost_center">Cost Center Select</option>
                    <option value="employee">Employee Name Auto Complete</option>
                </select>
            </div>

            <div id="hidden_div_edit" @if($question->question_type != 'dropdown') style="display:none;" @endif>

                <p style="color:orange; font-style: italic;">Enter your drop down list choices</p>
                
                <div class="form-group">
                    <input type="text" name="dropdown_label[]" style="width:25%;" class="form-control dropdown" id="title_id" @if(!empty($drops[0])) value="{{ $drops[0]->dropdown_choice }}" @endif placeholder="Drop down option.">
                </div>
                <div class="form-group">
                    <input type="text" name="dropdown_label[]" style="width:25%;" class="form-control dropdown" id="title_id" @if(!empty($drops[1])) value="{{ $drops[1]->dropdown_choice }}" @endif placeholder="Drop down option.">
                </div>
                <div class="form-group">
                    <input type="text" name="dropdown_label[]" style="width:25%;" class="form-control dropdown" id="title_id" @if(!empty($drops[2])) value="{{ $drops[2]->dropdown_choice }}" @endif placeholder="Drop down option.">
                </div>
                <div class="form-group">
                    <input type="text" name="dropdown_label[]" style="width:25%;" class="form-control dropdown" id="title_id" @if(!empty($drops[3])) value="{{ $drops[3]->dropdown_choice }}" @endif placeholder="Drop down option.">
                </div>
                <div class="form-group">
                    <input type="text" name="dropdown_label[]" style="width:25%;" class="form-control dropdown" id="title_id" @if(!empty($drops[4])) value="{{ $drops[4]->dropdown_choice }}" @endif placeholder="Drop down option.">
                </div>
                <div class="form-group">
                    <input type="text" name="dropdown_label[]" style="width:25%;" class="form-control dropdown" id="title_id" @if(!empty($drops[5])) value="{{ $drops[5]->dropdown_choice }}" @endif placeholder="Drop down option.">
                </div>
                <div class="form-group">
                    <input type="text" name="dropdown_label[]" style="width:25%;" class="form-control dropdown" id="title_id" @if(!empty($drops[6])) value="{{ $drops[6]->dropdown_choice }}" @endif placeholder="Drop down option.">
                </div>
                <div class="form-group">
                    <input type="text" name="dropdown_label[]" style="width:25%;" class="form-control dropdown" id="title_id" @if(!empty($drops[7])) value="{{ $drops[7]->dropdown_choice }}" @endif placeholder="Drop down option.">
                </div>
                <div class="form-group">
                    <input type="text" name="dropdown_label[]" style="width:25%;" class="form-control dropdown" id="title_id" @if(!empty($drops[8])) value="{{ $drops[8]->dropdown_choice }}" @endif placeholder="Drop down option.">
                </div>
                <div class="form-group">
                    <input type="text" name="dropdown_label[]" style="width:25%;" class="form-control dropdown" id="title_id" @if(!empty($drops[9])) value="{{ $drops[9]->dropdown_choice }}" @endif placeholder="Drop down option.">
                </div>

            </div>

            <div class="form-group">
                <label for="required_id">Answer Required</label>
                <select class="form-control" id="required_id" name="required" required>
                    <option value="{{$question->required}}" selected>@if($question->required == 0) No @else Yes @endif</option>
                    <option value="0">No</option>
                    <option value="1">Yes</option>
                </select>
            </div>

            <div class="form-group">
                <label for="title_id">Question</label>
                <input type="text" name="title" class="form-control" id="title_id" placeholder="Remember to word your question to the correct type." value="{{$question->title}}" required>
            </div>

            <div class="form-group">
                <label for="question_notes_id">Question Notes</label>
                <textarea class="form-control notes" name="question_notes" id="question_notes_id" rows="3">{{$question->question_notes}}</textarea>
            </div>

            <input type="Submit" class="btn btn-primary" value="Update Question"> {{-- BUTTON DISABLED UNTIL UPDATE IS FUNCTIONAL --}}

        </form>

    </div>
</div>

@endsection

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>

<script>
    $(function () {
        $("#edit_question_type").change(function() {
            var val = $(this).val();
            if(val === "dropdown") {
                $("#hidden_div_edit").show();
            }
            else if(val != "dropdown") {
                $("#hidden_div_edit").hide();
            }
        });
    });
</script>