
@extends('layouts.new_nonav')
@section('content')
<div class="jumbotron bg-danger">
    <h2 class=" text-white">This survey tool has been depreciated, visit this link to take your surveys from this point forward.</h2>
    <h3><a href="/audit">web.sgmc.org/audit</a></h3>
</div>
@endsection