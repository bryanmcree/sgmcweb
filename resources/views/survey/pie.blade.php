{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Page Design Goes Below--}}
@section('content')

<div class="card bg-dark text-white mb-3">
    <div class="card-body">
        <table class="table table-condensed table-striped table-hover" id="datetime_{{$survey->id}}">
            <thead>
                <th>Participant</th>
                <th>Response</th>
                <th>Submit Date</th>
                <th></th>
            </thead>
            <tbody>
            @foreach($results as $result)
                <tr>
                    <td>{{$result->submittedBy->name}}</td>
                    <td>
                        @if($result->question_type == 'YesNo' || $result->question_type == 'YesNoNA') 
                            @if($result->answer == 1) Yes @elseif($result->answer == 0) No @else NA @endif
                        @elseif($result->question_type == 'dropdown' && !empty($result->dropDown))
                            {{ $result->dropDown->dropdown_choice }}  
                        @elseif($result->question_type == 'Scale_5')
                            {{ $result->answer }} 
                        @elseif($result->question_type == 'Scale_10')
                            {{ $result->answer }} 
                        @endif
                    </td>
                    <td>{{Carbon\Carbon::parse($result->created_at)->format('m-d-Y')}}</td>
                    <td align="right"><a href="/survey/print/{{$survey->id}}?incident_id={{$result->incident_id}}" target="_blank"><i class="fas fa-eye" id="printbtn"></i></a></td>
                </tr>
            @endforeach
            </tbody>
        </table> 
    </div>
</div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

<script>
    $(document).ready(function() {
        $('#datetime_{{$survey->id}}').DataTable({
            "pageLength": 30,
            "autoWidth": true,
            "ordering": false,
            "columnDefs": [
                { targets: 'no-sort', orderable: false }
            ],
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                if ( aData[1] == "" )
                {
                    $('td', nRow).css('background-color', '#b53838');
                    $('td:eq(1)', nRow).html('<b>NO RESPONSE</b>');
                    $('#printbtn', nRow).css('display', 'none');
                }
            }
        });
    });
</script>

@endsection