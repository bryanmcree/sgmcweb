{{--New file Template--}}

{{--Add Security for this page below--}}


    @extends('layouts.new_print')
    {{--Page Design Goes Below--}}
@section('content')
    <div class="col-print-12">
        <div class="card border-info mb-3">
            <div class="card-header">
                <b>{{$survey->survey_name}}</b>
            </div>
            <div class="card-body">
                <div class="card-deck mb-3">
                    <div class="card">
                        <div class="" style="margin-left: 5px;">
                            <p><b>Submitted By</b></p>
                            <p>{{$answers->first()->submittedBy->name}}</p>
                        </div>

                    </div>
                    <div class="card" >
                        <div style="margin-left: 5px;">
                            <p><b>Date</b></p>
                            <p>{{$survey->created_at}}</p>
                        </div>

                    </div>
                    <div class="card">
                        <div style="margin-left: 5px;">
                            <p><b>Total Questions</b></p>
                            <p>{{$questions->count()}}</p>
                        </div>

                    </div>
                </div>
                <div class="col-lg-12">
                    <b>Questions:</b>
                    @foreach($answers as $question)
                        @if($question->question_type == 'Short Answer')
                            <div class="card mb-3">
                                <div style="margin-left: 5px;">
                                    <p><b>{{$question->surveyQuestion->title}}</b></p>
                                    <p>{{nl2br($question->answer_string)}}{{$question->answer}}</p>
                                </div>
                            </div>
                        @endif
                        @if($question->question_type == 'cost_center')
                            <div class="card mb-3">
                                <div style="margin-left: 5px;">
                                    <p><b>{{$question->surveyQuestion->title}}</b></p>
                                    <p>{{$question->costCenter->style1}}</p>
                                </div>
                            </div>
                        @endif
                            @if($question->question_type == 'YesNo')
                                <div class="card mb-3">
                                    <div style="margin-left: 5px;">
                                        <p><b>{{$question->surveyQuestion->title}}</b></p>
                                        <p>@if($question->answer == 1) Yes @else No @endif</p>
                                    </div>

                                </div>
                            @endif
                            @if($question->question_type == 'Scale_5')
                                <div class="card mb-3">
                                    <div style="margin-left: 5px;">
                                        <p><b>{{$question->surveyQuestion->title}}</b></p>
                                        <p>{{$question->answer}}</p>
                                    </div>

                                </div>
                            @endif
                            @if($question->question_type == 'Scale_10')
                                <div class="card mb-3">
                                    <div style="margin-left: 5px;">
                                        <p><b>{{$question->surveyQuestion->title}}</b></p>
                                        <p>{{$question->answer}}</p>
                                    </div>

                                </div>
                            @endif
                            {{-- @if($question->question_type == 'dropdown')
                                <div class="card mb-3">
                                    <div style="margin-left: 5px;">
                                        <p><b>{{$question->surveyQuestion->title}}</b></p>
                                        <p>{{$question->dropDown->dropdown_choice}}</p>
                                    </div>
                                </div>
                            @endif --}}
                            @if($question->question_type == 'employee')
                                <div class="card mb-3">
                                    <div style="margin-left: 5px;">
                                        <p><b>{{$question->surveyQuestion->title}}</b></p>
                                        <p>{{$question->employeeSelect->name}}</p>
                                    </div>

                                </div>
                            @endif
                            @if($question->question_type == 'date_time')
                                <div class="card mb-3">
                                    <div style="margin-left: 5px;">
                                        <p><b>{{$question->surveyQuestion->title}}</b></p>
                                        <p>{{$question->answer_string}}</p>
                                    </div>

                                </div>
                            @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
