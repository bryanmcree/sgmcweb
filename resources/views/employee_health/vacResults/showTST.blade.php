{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Employee Health') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1

    --}}
@section('content')


<div class="card bg-dark text-white mb-3"  style="width: 50%;">
    <div class="card-header">
        TST Results for: {{$view->created_at->format('m-d-Y')}}
    </div>

    <div class="card-body">
        <div class="row">
            <h4 class="col-lg-3">Name: </h4>
            <h4 class="col-lg-9">{{$view->name}}</h4>
        </div>
        <hr>
        <div class="row">
            <h4 class="col-lg-3">EE#: </h4>
            <h4 class="col-lg-9">{{$view->EE}}</h4>
        </div>
        <hr>
        <div class="row">
            <h4 class="col-lg-3">Date Given: </h4>
            <h4 class="col-lg-9">{{$view->date_given->format('m-d-Y')}}</h4>
        </div>
        <hr>
        <div class="row">
            <h4 class="col-lg-3">Given By: </h4>
            <h4 class="col-lg-9">{{$view->given_by}}</h4>
        </div>
        <hr>
        <div class="row">
            <h4 class="col-lg-3">Prescription: </h4>
            <h4 class="col-lg-9">{{$view->prescription}}</h4>
        </div>
        <hr>
        <div class="row">
            <h4 class="col-lg-3">Lot#: </h4>
            <h4 class="col-lg-9">{{$view->lot}}</h4>
        </div>
        <hr>
        <div class="row">
            <h4 class="col-lg-3">Expiration Date: </h4>
            <h4 class="col-lg-9">{{$view->exp_date->format('m-d-Y')}}</h4>
        </div>
        <hr>
        <div class="row">
            <h4 class="col-lg-3">Site: </h4>
            <h4 class="col-lg-9">{{$view->site}}</h4>
        </div>
        <hr>
        <div class="row">
            <h4 class="col-lg-3">Date Read: </h4>
            <h4 class="col-lg-9">{{$view->date_read->format('m-d-Y')}}</h4>
        </div>
        <hr>
        <div class="row">
            <h4 class="col-lg-3">Read By: </h4>
            <h4 class="col-lg-9">{{$view->read_by}}</h4>
        </div>
        <hr>
        <div class="row">
            <h4 class="col-lg-3">Result: </h4>
            <h4 class="col-lg-9">{{$view->result}} &nbsp; {{$view->result_mm}}mm</h4>
        </div>

    </div>

@endsection

@endif