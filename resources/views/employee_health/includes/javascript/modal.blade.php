//Pass allergies to modal
$(document).on("click", ".NewHealthScreen", function () {
$('.emp_allergies').val($(this).attr("data-allergies"));
$('#new_health_screen').modal('show');
});

//Pass allergies to modal
$(document).on("click", ".edit_allergies", function () {
$('.emp_allergies').val($(this).attr("data-allergies"));
$('#editallergies').modal('show');
});

//Pass notes to edit note model
$(document).on("click", ".editNote", function () {
$('.followup_date').val($(this).attr("data-followup_date"));
$('.note').text($(this).attr("data-note"));
$('.id').val($(this).attr("data-id"));
$('#edit_note').modal('show');
});

//Pass grits to modal
$(document).on("click", ".edit_grits", function () {
$('.grits_number').val($(this).attr("data-grits_number"));
$('#editgrits').modal('show');
});

//Pass medical alerts to modal
$(document).on("click", ".edit-alert", function () {
$('.med-alert').val($(this).attr("data-alert"));
$('.med-importance').val($(this).attr("data-importance"));
$('.med-id').val($(this).attr("data-id"));
$('#editAlerts').modal('show');
});

//Pass TST info to modal
$(document).on("click", ".edit-TST", function () {
$('.tst-id').val($(this).attr("data-id"));
$('.tst-name').val($(this).attr("data-name"));
$('.tst-ee').val($(this).attr("data-EE"));
$('.tst-date-given').val($(this).attr("data-date-given"));
$('.tst-given-by').val($(this).attr("data-given-by"));
$('.tst-prescription').val($(this).attr("data-prescription"));
$('.tst-lot').val($(this).attr("data-lot"));
$('.tst-exp-date').val($(this).attr("data-exp-date"));
$('.tst-site').val($(this).attr("data-site"));
$('.tst-createdBy').val($(this).attr("data-createdby"));
$('#editformTST').modal('show');
});