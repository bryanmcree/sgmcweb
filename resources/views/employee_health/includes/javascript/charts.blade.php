//Main dashboard BP chart
@if(!empty($bps))
Chart.defaults.global.defaultFontColor = '#fff';
new Chart(document.getElementById("bloodpressure"), {
type: 'line',
data: {
labels: [
@foreach($bps as $bp)
    "{{$bp->created_at->format('m-d-Y')}}",
@endforeach
],
datasets: [{
data: [
@foreach($bps as $bp)
    {{$bp->systolic}},
@endforeach
],
label: "Systolic",
borderColor: "#3e95cd",
fill: false
}, {
data: [
@foreach($bps as $bp)
    {{$bp->distolic}},
@endforeach
],
label: "Diastolic",
borderColor: "#8e5ea2",
fill: false
}
]
},
options: {
title: {
display: true,
text: 'Patient Blood Pressure History'
},
scales: {
yAxes: [{
stacked:false,
ticks: {
beginAtZero: true
}
}]
}
}
});
@endif


//Main dashboard weight chart
@if(!empty($weight))
new Chart(document.getElementById("weight"), {
type: 'line',
data: {
labels: [
@foreach($weight as $lbs)
    "{{$lbs->created_at->format('m-d-Y')}}",
@endforeach
],
datasets: [{
data: [
@foreach($weight as $lbs)
    {{$lbs->weight}},
@endforeach
],
label: "Weight",
borderColor: "#3e95cd",
fill: false
}
]
},
options: {
title: {
display: true,
text: 'Patient Weight History'
},
scales: {
yAxes: [{
stacked:false,
ticks: {
beginAtZero: true
}
}]
}
}
});
@endif