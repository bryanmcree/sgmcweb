
<nav>
    <div class="nav nav-tabs mb-3" id="nav-tab" role="tablist">
        <a class="nav-item nav-link active" id="nav-vac-record-tab" data-toggle="tab" href="#nav-vac-records" role="tab" aria-controls="nav-vac-records" aria-selected="true">Home</a>
        <a class="nav-item nav-link" id="nav-TST-tab" data-toggle="tab" href="#nav-TST" role="tab" aria-controls="nav-TST" aria-selected="false">TST</a>
    </div>
</nav>

<div class="tab-content " id="nav-tabContent">
    <div class="tab-pane fade show active mb-4" id="nav-vac-records" role="tabpanel" aria-labelledby="nav-vac-record-dashboard-tab">

        nav-vac-records HOME

    </div>

    <div class="tab-pane fade show mb-4" id="nav-TST" role="tabpanel" aria-labelledby="nav-TST-tab">

        <div class="card bg-dark text-white mb-3">
            <div class="card-body">
                @if(count($TSThistory) == 0)
                    You have no TST records!
                @else
                <table class="table table-hover table-dark">
                    <thead class="text-center">
                        <th>Name</th>
                        <th>Created At</th>
                        <th>Results</th> <!-- if result is null then show enter results to finish form, else show results -->
                    </thead>
                    <tbody class="text-center"> 
                        @foreach ($TSThistory as $history)
                            <tr>
                                <td> {{$history->name}} </td>
                                <td> {{$history->created_at->format('m-d-Y')}} </td>
                                @if(empty($history->result) && Auth::user()->hasRole('Nurse Notes') == TRUE)
                                    <td class="bg-danger">
                                        <a href="#" class="btn btn-primary edit-TST"
                                        data-id = {{$history->id}}
                                        data-createdby = {{$history->created_by}}
                                        data-name = "{{$history->name}}"
                                        data-EE = {{$history->EE}}
                                        data-date-given = {{$history->date_given}}
                                        data-given-by = "{{$history->given_by}}"
                                        data-prescription = {{$history->prescription}}
                                        data-lot = {{$history->lot}}
                                        data-exp-date = {{$history->exp_date}}
                                        data-site = {{$history->site}}
                                        >Complete TST</a>
                                    </td>
                                @elseif(empty($history->result))
                                    <td class="bg-warning">Results Pending</td>
                                @else
                                    <td><a href="/employee_health/tst/{{$history->id}}" class="btn btn-primary" target="_blank">View Results</a></td>
                                @endif
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                @endif
            </div>
        </div>

        <a href="#formTST" data-toggle="modal" class="btn btn-primary btn-block">New TST</a>

    </div>



</div>