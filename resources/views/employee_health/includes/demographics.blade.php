<div class="card-deck">
    <div class="card col-sm-9 mb-3 border-info text-white bg-dark">
        <div class="row justify-content-md pt-2">
            <div class="col-sm-auto align-self-center">@if($employee->photo != '')<a href="data:image/jpeg;base64,{{str_replace('</photo>','',str_replace('<photo>','',$employee->photo))}}" target="_blank"><img src="data:image/jpeg;base64,{{str_replace('</photo>','',str_replace('<photo>','',$employee->photo))}}" height="60" width="60"/></a>@else {{ Html::image('img/nopic.png', 'alt', array( 'width' => 60, 'height' => 60 )) }} @endif</div>
            <div class="col-sm-auto"><b>{{$employee->name}}</b> ({{$employee->employee_number}})<br>{{$employee->title}}<br>{{$employee->unit_code}}-{{$employee->unit_code_description}}</div>
            <div class="col-sm-auto"><b>DOB:</b> {{$employee->dob->format('m-d-Y')}} / {{\Carbon\Carbon::createFromTimeStamp(strtotime($employee->dob))->diffForHumans()}}
                <br><b>Gender:</b> {{$employee->gender}}
                <br><b>Phone:</b>  @if(is_null($employee->phone)) No phone @else {{PhoneNumbers::formatNumber($employee->phone)}} @endif
            </div>
            <div class="col-sm-auto">
                <b>Email:</b> {{ Html::mailto($employee->mail) }}
                <br><b>Hire Date:</b> {{$employee->hire_date->format('m-d-Y')}}
                <br><b>Address:</b> @if(is_null($employee->address)) No address on file @else {{$employee->address}}, {{$employee->city}}, {{$employee->state}} {{$employee->zip}} @endif
            </div>
            <div class="col-sm-auto">
                <b>GRITS #:</b> @if(is_null($grits))No Number @else {{$grits->grits_number}} @endif &nbsp; <a href="#" class="btn-primary btn-sm float-right edit_grits" data-grits_number="@if(!is_null($grits)){{$grits->grits_number}} @endif"><i class="far fa-edit"></i></a>
            </div>
        </div>
    </div>
    
    <div class="card col-sm-3 mb-3 border-danger text-white bg-dark">
        <div class="card-header" style="color:red;">
            Employee Allergies <a href="#" class="btn-primary btn-sm float-right edit_allergies" data-allergies="@if(!is_null($allergies)){{$allergies->allergies}} @endif">Update Allergies</a>
        </div>
        <div class="p-2">
            @if(is_null($allergies))
                None
            @else
                {{$allergies->allergies}}
            @endif
        </div>
    </div>
</div>

<div class="card col-lg-12 bg-dark mb-3 text-white border-warning">
    <div class="card-header text-warning">
        Medical Alerts <a href="#medAlert" data-toggle="modal" class="btn btn-primary btn-sm float-right">Add Alert</a>
    </div>
    <div class="card-body">
        @if(count($medAlerts) == 0)
            You have no alerts!
        @else
            <table class="table table-hover table-dark" style="width:100%;">
                <thead>
                    <th>Alert!</th>
                    <th>Importance Level</th>
                    <th>Created By</th>
                    @if (Auth::user()->hasRole('Nurse Notes') == TRUE)
                        <th class="text-right">Edit</th>
                        <th class="text-right">Mark as Complete / Remove</th>
                    @endif
                </thead>
                <tbody>
                    @foreach($medAlerts as $alert)
                        @if($alert->importance == 0)
                            <tr>
                                <td style="max-width: 750px;">{{ $alert->alert }}</td>
                                <td>Low</td>
                                <td>{{ $alert->createdBy->name }}</td>
                                @if (Auth::user()->hasRole('Nurse Notes') == TRUE)
                                    <td class="text-right"><a href="#" class="btn btn-warning btn-sm edit-alert"
                                        data-alert="{{ $alert->alert }}"
                                        data-importance="{{ $alert->importance }}"
                                        data-id="{{ $alert->id }}"
                                        >Edit Alert</a></td>
                                    <td class="text-right"><a href="#" class="btn btn-danger btn-sm deletebtn" alert_id = {{$alert->id}} user = {{$alert->user_id}}>Remove Alert</a></td>
                                @endif
                            </tr>
                        @else   
                            <tr style="background-color: #b53838;">
                                <td style="max-width: 750px;">{{ $alert->alert }}</td>
                                <td>High</td>
                                <td>{{ $alert->createdBy->name }}</td>
                                @if (Auth::user()->hasRole('Nurse Notes') == TRUE)
                                    <td class="text-right"><a href="#" class="btn btn-warning btn-sm edit-alert"
                                        data-alert="{{ $alert->alert }}"
                                        data-importance="{{ $alert->importance }}"
                                        data-id="{{ $alert->id }}"
                                        >Edit Alert</a></td>
                                    <td class="text-right"><a href="#" class="btn btn-danger btn-sm deletebtn" alert_id = {{$alert->id}} user = {{$alert->user_id}}>Remove Alert</a></td>
                                @endif
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        @endif
    </div>
</div>

@include('employee_health.modals.editAlerts')
@include('employee_health.modals.editallergies')
@include('employee_health.modals.editgrits')
@include('employee_health.modals.medicalAlerts')

