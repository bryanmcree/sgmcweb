<button type="button" class="btn btn-sm btn-primary mb-2" data-toggle="modal" data-target="#enternote">
    <i class="fas fa-weight"></i> Enter Note
</button>
@if($notes->isEmpty())
    <div class="alert alert-info">No notes for patient.</div>
@else
    @foreach($notes as $note)
        <table class="table table-condensed table-dark table-hover">
            <tbody>
            <tr>
                <td><b>Entered by:</b> {{$note->createdBy->name}}</td>
                <td><b>Follow-up Date:</b> @if($note->followup_date->format('m-d-Y') != '01-01-1900'){{$note->followup_date->format('m-d-Y')}}@endif</td>
                <td><b>Entered At:</b> {{$note->created_at}}</td>
                <td align="right"><a href="#" class="btn-primary btn-sm editNote"
                                     data-followup_date="{{$note->followup_date->format('Y-m-d')}}"
                                     data-note="{{$note->note}}"
                                     data-id="{{$note->id}}"

                    > <i class="fas fa-edit"></i></a> </td>
            </tr>
            <tr>
                <td colspan="4">{{$note->note}}</td>
            </tr>
            </tbody>
        </table>
        <hr class="bg-info">
    @endforeach
@endif
</div>