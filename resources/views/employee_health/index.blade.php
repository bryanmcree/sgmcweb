{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Employee Health') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1

    --}}
@section('content')
<?php
//this will help models to get back to the right page
$page_source = 'index';
?>

    @include('employee_health.includes.demographics')


    <div class="card mb-5 text-white bg-dark">
        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-dashboard-tab" data-toggle="tab" href="#nav-dashboard" role="tab" aria-controls="nav-dashboard" aria-selected="true"><i class="fas fa-notes-medical"></i> Dashboard</a>
                @if (Auth::user()->hasRole('Nurse Notes') == TRUE)<a class="nav-item nav-link" id="nav-notes-tab" data-toggle="tab" href="#nav-notes" role="tab" aria-controls="nav-notes" aria-selected="false">Notes</a> @endif
                <a class="nav-item nav-link" id="nav-vac-tab" data-toggle="tab" href="#nav-vac" role="tab" aria-controls="nav-vac" aria-selected="false">Vaccinations</a>
                <a class="nav-item nav-link" id="nav-rec-tab" data-toggle="tab" href="#nav-rec" role="tab" aria-controls="nav-rec" aria-selected="false">Recent Patients</a>
            </div>
        </nav>
        <div class="tab-content " id="nav-tabContent">
            <div class="tab-pane fade show active mb-4" id="nav-dashboard" role="tabpanel" aria-labelledby="nav-dashboard-tab">
                <br>
                <div class="row mb-4 justify-content-md-center">
                    <div class="card-deck col-lg-12">
                        <div class="card col-4 mb-4 border-info text-white bg-dark">
                            <div class="card-body">
                                <canvas id="bloodpressure"></canvas>
                            </div>


                            <div class="card-footer">
                                <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#enterbp">
                                    <i class="fas fa-user-md"></i> Enter Blood Pressure
                                </button>
                            </div>
                        </div>
                        <div class="card col-4 mb-4 border-info text-white bg-dark">
                            <div class="card-body">
                                <canvas id="weight"></canvas>
                            </div>

                            <div class="card-footer">
                                <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#enterweight">
                                    <i class="fas fa-weight"></i> Enter Weight
                                </button>
                            </div>
                        </div>
                        <div class="card col-4 mb-4 border-info text-white bg-dark" style="height: 375px;overflow-y: auto;">
                            <div class="card-header">Health Screen History <div class="float-right">Next Due: @if($screens->isEmpty()) NOW @elseif(empty($next_screen->next_due_date)) {{$next->format('m-d-Y')}} @else {{$next_screen->next_due_date->format('m-d-Y')}} @endif <a href="#next_due" class="btn btn-primary btn-sm ml-2" data-toggle="modal"><i class="far fa-edit"></i></a></div></div>
                            
                            <div class="card-body">
                                <table class="table">
                                    <tbody>
                                    @foreach($screens as $screen)
                                        <tr>
                                            <td>{{$screen->created_at->format('m-d-Y')}}</td>
                                            <td>{{$screen->createdBy->name}}</td>
                                            <td align="right"><a href="/employee_health/hs/{{$employee->employee_number}}?hs={{$screen->id}}" class="btn btn-sm btn-primary"><i class="fas fa-user-edit"></i></a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                            <div class="card-footer">
                                <button type="button" class="btn btn-sm btn-primary NewHealthScreen" data-allergies="@if(is_null($allergies)) None @else{{$allergies->allergies}}@endif">
                                    <i class="fas fa-file-medical-alt"></i> Start New Health Screen
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="tab-pane fade" id="nav-notes" role="tabpanel" aria-labelledby="nav-notes-tab">
                <div class="card mt-2 text-white bg-dark">
                    <div class="card-header">
                        <b>Nurse Notes</b>
                    </div>
                    <div class="card-body">
                        @include('employee_health.includes.notes')
                </div>
            </div>
            <div class="tab-pane fade" id="nav-vac" role="tabpanel" aria-labelledby="nav-vac-tab">
                <div class="card mt-2 border-info text-white bg-dark">
                    <div class="card-header">
                        <b>Vaccination Records</b>
                    </div>
                    <div class="card-body">
                        @include('employee_health.includes.vaccination')
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="nav-rec" role="tabpanel" aria-labelledby="nav-rec-tab">
                <div class="card mt-2 border-info text-white bg-dark">
                    <div class="card-header">
                        <b>Recent Patients</b>
                    </div>
                    <div class="card-body">
                        <table class="table table-dark table-hover">
                            <thead>
                                <tr>
                                    <td><b>Patient Name</b></td>
                                    <td><b>Accessed By</b></td>
                                    <td><b>Date and Time</b></td>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('employee_health.modals.new_health_screen')
    @include('employee_health.modals.enterweight')
    @include('employee_health.modals.enterbp')
    @include('employee_health.modals.enternote')
    @include('employee_health.modals.editnote')
    @include('employee_health.modals.next_due_date')
    @include('employee_health.modals.editTST')
    @include('employee_health.modals.TST')

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">

    @include('employee_health.includes.javascript.charts') //All charts JS
    @include('employee_health.includes.javascript.tab') //Tab script
    @include('employee_health.includes.javascript.modal') //All Modal JS data passes

    $(document).on("click", ".deletebtn", function () {
            var alert_id = $(this).attr("alert_id");
            var user = $(this).attr("user");
            DeleteAlert(alert_id, user);
        });

        function DeleteAlert(alert_id, user) {
            swal({
                title: "Remove Alert?",
                text: "Removing this Alert cannot be undone.",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                confirmButtonText: "Yes, Remove it!",
                confirmButtonColor: "#ec6c62"
            }, function() {
                $.ajax({
                    url: "/employee_health/remove/" + alert_id,
                    type: "GET"
                })
                    .done(function(data) {
                        swal({
                                title: "Deleted",
                                text: "Alert Removed",
                                type: "success",
                                timer: 1800,
                                showConfirmButton: false

                            }
                        );
                        setTimeout(function(){window.location.replace('/employee_health/' + user)},1900);
                    })
                    .error(function(data) {
                        swal("Oops", "We couldn't connect to the server!", "error");
                    });
            });
        }

</script>

@endsection
@endif