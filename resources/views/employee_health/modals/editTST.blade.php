<div class="modal fade" id="editformTST" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document" style="max-width: 55%">
        <div class="modal-content text-white bg-dark">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">New TST</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="/employee_health/TST/edit">
                <input type="hidden" name="user_id" value="{{$employee->employee_number}}">
                <input type="hidden" name="created_by" class="tst-createdBy">
                <input type="hidden" name="id" class="tst-id">
                {{ csrf_field() }}
            <div class="modal-body">
                <div class="card border-danger text-white bg-dark">
                    <div class="card-body">
                        
                        <div class="form-row">
                            <div class="form-group col-md-8">
                                <label>Name</label>
                                <input type="text" class="form-control tst-name" name="name" placeholder="Patient Name" readonly required>
                            </div>
                            <div class="form-group col-md-4">
                                <label>EE#</label>
                                <input type="number" class="form-control tst-ee" name="EE" placeholder="EE#" readonly required>
                            </div>
                        </div>
                        <hr>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Date Given</label>
                                <input type="date" class="form-control tst-date-given" name="date_given" placeholder="Date Given" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Given By</label>
                                <input type="text" class="form-control tst-given-by" name="given_by" placeholder="Given By" readonly required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-2">
                                <label>Prescription</label>
                                <input type="text" class="form-control tst-prescription" name="prescription" placeholder="Prescription" value="Tubersol" required>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Lot#</label>
                                <input type="number" class="form-control tst-lot" name="lot" placeholder="Lot#" required>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Exp. Date</label>
                                <input type="date" class="form-control tst-exp-date" name="exp_date" placeholder="Expiration Date" required>
                            </div>
                            <div class="form-group col-md-2">
                                <label>Site</label>
                                <select name="site" class="form-control tst-site" required>
                                    <option value="" selected>[Choose Site]</option>
                                    <option value="LF">LF</option>
                                    <option value="RF">RF</option>
                                </select>
                            </div>
                        </div>
                        <hr>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Date Read</label>
                                <input type="date" class="form-control" name="date_read" placeholder="Date Read">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Read By</label>
                                <input type="text" class="form-control" name="read_by" placeholder="Read By" readonly value="{{ Auth::user()->name }}">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Result</label>
                                <select name="result" class="form-control">
                                    <option value="" selected>[Choose Result]</option>
                                    <option value="NEGATIVE">NEGATIVE (0-9mm)</option>
                                    <option value="POSITIVE">POSITIVE (10mm or more)</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Result mm</label>
                                <input type="decimal" class="form-control" name="result_mm" placeholder="Result in mm">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary" value="Submit">
            </div>
            </form>
        </div>
    </div>
</div>