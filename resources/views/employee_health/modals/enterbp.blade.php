<div class="modal fade" id="enterbp" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
        <div class="modal-content text-white bg-dark">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Enter Blood Pressure</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="/employee_health/enter_bp">
                {{ csrf_field() }}
                <input type="hidden" name="employee_number" value="{{$employee->employee_number}}">
                <input type="hidden" name="created_by" value="{{Auth::user()->employee_number}}">
            <div class="modal-body">
                <div class="card border-danger text-white bg-dark">
                    <div class="card-header">Blood Pressure</div>
                    <div class="card-body">
                        <div class="">
                            <div class="form-group ">
                                <label for="exampleFormControlInput1">Systolic</label>
                                <input type="number" name="systolic" class="form-control" id="exampleFormControlInput1">
                            </div>
                            <div class="form-group ">
                                <label for="exampleFormControlInput1">Diastolic</label>
                                <input type="number" name="distolic" class="form-control" id="exampleFormControlInput1">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary" value="Enter BP">
            </div>
            </form>
        </div>
    </div>
</div>