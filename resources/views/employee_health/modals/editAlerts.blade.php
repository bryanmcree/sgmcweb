<div class="modal fade" id="editAlerts" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content text-white bg-dark">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Alert</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="/employee_health/update_medAlert">
                {{ csrf_field() }}
                <input type="hidden" name="user_id" value="{{$employee->employee_number}}">
                <input type="hidden" name="created_by" value="{{Auth::user()->employee_number}}">
                <input type="hidden" name="id" class="med-id">
                <div class="modal-body">
                    <div class="card border-danger text-white bg-dark">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="alert">Alert</label>
                                <textarea class="form-control med-alert" name="alert" rows="4"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="importance">Level of Importance</label>
                                <select name="importance" class="form-control med-importance">
                                    <option value="0">Low</option>
                                    <option value="1">High</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Update Alert">
                </div>
            </form>
        </div>
    </div>
</div>