<div class="modal fade" id="enternote" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content text-white bg-dark">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Enter Nurse Note</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="/employee_health/enter_note">
                {{ csrf_field() }}
                <input type="hidden" name="employee_number" value="{{$employee->employee_number}}">
                <input type="hidden" name="created_by" value="{{Auth::user()->employee_number}}">
                <input type="hidden" name="page_source" value="{{$page_source}}">
                <input type="hidden" name="hs_update_id" value="{{app('request')->input('hs')}}">
                <div class="card text-white bg-dark mb-4">
                    <div class="card-body">
                        <div class="row col-lg-12">
                            <div class="col-lg-8">
                                <div class="form-group">
                                    <textarea class="form-control mt-2" name="note" id="exampleFormControlTextarea1" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Follow-up Date:</label>
                                    <input type="date" name="followup_date" class="form-control" id="exampleFormControlInput1">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Enter Note">
                </div>
            </form>
        </div>
    </div>
</div>