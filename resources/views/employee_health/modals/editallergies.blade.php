<div class="modal fade" id="editallergies" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content text-white bg-dark">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Allergies</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="/employee_health/edit_allergies">
                {{ csrf_field() }}
                <input type="hidden" name="employee_number" value="{{$employee->employee_number}}">
                <input type="hidden" name="page_source" value="{{$page_source}}">
                <input type="hidden" name="hs_update_id" value="{{app('request')->input('hs')}}">
                <div class="modal-body">
                    <div class="card border-danger text-white bg-dark">
                        <div class="card-body">
                            <div class="form-group">
                                <textarea class="form-control emp_allergies" name="allergies" id="exampleFormControlTextarea1" rows="4"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Update Allergies">
                </div>
            </form>
        </div>
    </div>
</div>