<!-- Modal -->
<div class="modal fade " id="new_health_screen" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document" style="max-width: 90%">
        <div class="modal-content text-white bg-dark">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New Health Screen</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="/employee_health/new_screen">
                {{ csrf_field() }}
                <input type="hidden" name="employee_number" value="{{$employee->employee_number}}">
                <input type="hidden" name="created_by" value="{{Auth::user()->employee_number}}">
            <div class="modal-body">
                <div class="card-deck mb-4">
                    <div class="card border-danger text-white bg-dark">
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col">
                                    <label for="exampleFormControlTextarea1">Have you had a <b><i>work related</i></b> back / shoulder injury in the last 12 months?  If so give details.</label>
                                    <textarea class="form-control" name="work_related" id="exampleFormControlTextarea1" rows="2"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card border-danger text-white bg-dark">
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col">
                                    <label for="exampleFormControlTextarea1">Have you had a <b><i>non-work related</i></b> back / shoulder injury in the last 12 months?  If so give details.</label>
                                    <textarea class="form-control" name="non_work_related" id="exampleFormControlTextarea1" rows="2"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card border-danger text-white bg-dark">
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col">
                                    <label for="exampleFormControlSelect1">Are you currently using tobacco products?</label>
                                    <select class="form-control" name="tobacco" id="exampleFormControlSelect1" required>
                                        <option value="">[Select]</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card border-danger text-white bg-dark">
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col">
                                    <label for="exampleFormControlSelect1">What is your pattern of alcohol use?</label>
                                    <select class="form-control" name="alcohol" id="exampleFormControlSelect1">
                                        <option value="">[Select]</option>
                                        <option value="1">Daily</option>
                                        <option value="2">Weekends</option>
                                        <option value="3">Occasional</option>
                                        <option value="0">None</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card border-danger text-white bg-dark">
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col">
                                    <label for="exampleFormControlSelect1">BBP Category?</label>
                                    <select class="form-control" name="bbp_category" id="exampleFormControlSelect1">
                                        <option value="">[Select]</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-deck mb-4">
                    <div class="card border-danger text-white bg-dark">
                        <div class="card-header">Blood Pressure</div>
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group ">
                                    <label for="exampleFormControlInput1">Systolic</label>
                                    <input type="number" name="systolic" class="form-control" id="exampleFormControlInput1">
                                </div>
                                <div class="form-group ">
                                    <label for="exampleFormControlInput1">Diastolic</label>
                                    <input type="number" name="distolic" class="form-control" id="exampleFormControlInput1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card border-danger text-white bg-dark">
                        <div class="card-header">Height / Weight</div>
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group ">
                                    <label for="exampleFormControlInput1">Feet</label>
                                    <input type="number" name="height_feet" class="form-control" id="exampleFormControlInput1">
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Inches</label>
                                    <input type="number" step=".01" name="height_inches" class="form-control" id="exampleFormControlInput1">
                                </div>
                                <div class="form-group ">
                                    <label for="exampleFormControlInput1">Weight</label>
                                    <input type="number" step=".01" name="weight" class="form-control" id="exampleFormControlInput1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card border-danger text-white bg-dark">
                        <div class="card-header">TB Category</div>
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col">
                                    <label for="exampleFormControlSelect1">TB Category</label>
                                    <select class="form-control" name="tb_category" id="exampleFormControlSelect1" required>
                                        <option value="">[Select]</option>
                                        <option value="2">High</option>
                                        <option value="1">Medium</option>
                                        <option value="0">Low</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col">
                                    <label for="exampleFormControlSelect1">TB Mask Size</label>
                                    <select class="form-control" name="tb_mask_size" id="exampleFormControlSelect1" required>
                                        <option value="">[Select]</option>
                                        <option value="0">Regular</option>
                                        <option value="1">Small</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card border-danger text-white bg-dark">
                        <div class="card-header">
                            Have you <b>developed and of the following <b>symptoms in the last year?</b></b>
                        </div>
                        <div class="card-body">
                            <div class="form-check">
                                <input class="form-check-input" name="cough" type="checkbox" value="1" id="defaultCheck1">
                                <label class="form-check-label" for="defaultCheck1">
                                    Persistent cough or coughing up blood
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" name="rash" type="checkbox" value="1" id="defaultCheck2">
                                <label class="form-check-label" for="defaultCheck2">
                                    Persistent skin rashes, abscesses or sores
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" name="sweat" type="checkbox" value="1" id="defaultCheck3">
                                <label class="form-check-label" for="defaultCheck3">
                                    Night sweats
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" name="fatigue" type="checkbox" value="1" id="defaultCheck4">
                                <label class="form-check-label" for="defaultCheck4">
                                    Excessive fatigue
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" name="diarrhea" type="checkbox" value="1" id="defaultCheck5">
                                <label class="form-check-label" for="defaultCheck5">
                                    Diarrhea lasting more than 48 hours or with blood/mucous in stool
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="card border-danger text-white bg-dark">
                        <div class="card-header">
                            Current Allergies
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <textarea class="form-control emp_allergies" name="allergies" id="exampleFormControlTextarea1" rows="4"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="card border-danger text-white bg-dark">
                        <div class="card-header">
                            Do you have any health concerns or problems you would like to discuss with the Employee Health Nurse?
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <textarea class="form-control" name="concerns" id="exampleFormControlTextarea1" rows="2"></textarea>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary">
            </div>
            </form>
        </div>
    </div>
</div>