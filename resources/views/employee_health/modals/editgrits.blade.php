<div class="modal fade" id="editgrits" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content text-white bg-dark">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">GRITS Number</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="/employee_health/edit_grits">
                {{ csrf_field() }}
                <input type="hidden" name="employee_number" value="{{$employee->employee_number}}">
                <input type="hidden" name="page_source" value="{{$page_source}}">
                <input type="hidden" name="hs_update_id" value="{{app('request')->input('hs')}}">
                <div class="modal-body">
                    <div class="card border-danger text-white bg-dark">
                        <div class="card-body">
                            <div class="form-group">
                                <input type="text" name="grits_number" class="form-control grits_number" id="exampleInputEmail1" aria-describedby="emailHelp">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Update GRITS">
                </div>
            </form>
        </div>
    </div>
</div>