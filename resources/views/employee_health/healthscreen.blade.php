{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Employee Health') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1

    --}}
@section('content')
    <?php
    //this will help models to get back to the right page
    $page_source = 'hs';
    ?>

    @include('employee_health.includes.demographics')

    <div class="card mb-4 text-white bg-dark">
        <div class="card-header">Health Screen Detail</div>
        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-dashboard-tab" data-toggle="tab" href="#nav-dashboard" role="tab" aria-controls="nav-dashboard" aria-selected="true"><i class="fas fa-notes-medical"></i> Basic</a>
                 @if (Auth::user()->hasRole('Nurse Notes') == TRUE)<a class="nav-item nav-link" id="nav-notes-tab" data-toggle="tab" href="#nav-notes" role="tab" aria-controls="nav-notes" aria-selected="false">Notes</a>@endif
                <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Contact</a>
                <a class="nav-item nav-link" id="nav-contact-tab" href="/employee_health/{{$employee->employee_number}}" role="tab">Return</a>
            </div>
        </nav>
        <div class="tab-content " id="nav-tabContent">
            <div class="tab-pane fade show active mb-4" id="nav-dashboard" role="tabpanel" aria-labelledby="nav-dashboard-tab">
                <br>
                <form method="post" action="/employee_health/hs/update">
                    {{ csrf_field() }}
                    <input type="hidden" name="hs_update_id" value="{{$hs->id}}">
                    <input type="hidden" name="employee_number" value="{{$employee->employee_number}}">
                    <div class="col-lg-12 mb-4">
                        <input type="submit" class="btn btn-primary btn-sm" value="Update Basic">
                    </div>
                    <div class="row mb-4 justify-content-md-center">
                        <div class="card-deck col-lg-12">
                            <div class="card border-danger text-white bg-dark">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="form-group col">
                                            <label for="exampleFormControlTextarea1">Have you had a <b><i>work related</i></b> back / shoulder injury in the last 12 months?  If so give details.</label>
                                            <textarea class="form-control" name="work_related" id="exampleFormControlTextarea1" rows="2">{{$hs->work_related}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card border-danger text-white bg-dark">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="form-group col">
                                            <label for="exampleFormControlTextarea1">Have you had a <b><i>non-work related</i></b> back / shoulder injury in the last 12 months?  If so give details.</label>
                                            <textarea class="form-control" name="non_work_related" id="exampleFormControlTextarea1" rows="2">{{$hs->non_work_related}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card border-danger text-white bg-dark">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="form-group col">
                                            <label for="exampleFormControlSelect1">Are you currently using tobacco products?</label>
                                            <select class="form-control" name="tobacco" id="exampleFormControlSelect1" required>
                                                <option value="{{$hs->tobacco}}" selected>@if($hs->tobacco == 1) Yes @else No @endif </option>
                                                <option value="1">Yes</option>
                                                <option value="0">No</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card border-danger text-white bg-dark">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="form-group col">
                                            <label for="exampleFormControlSelect1">What is your pattern of alcohol use?</label>
                                            <select class="form-control" name="alcohol" id="exampleFormControlSelect1">
                                                <option value="{{$hs->alcohol}}" selected>@if($hs->alcohol == 1) Daily @elseif($hs->alcohol == 2) Weekends @elseif($hs->alcohol == 3) Occasional @else None @endif </option>
                                                <option value="1">Daily</option>
                                                <option value="2">Weekends</option>
                                                <option value="3">Occasional</option>
                                                <option value="0">None</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card border-danger text-white bg-dark">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="form-group col">
                                            <label for="exampleFormControlSelect1">BBP Category?</label>
                                            <select class="form-control" name="bbp_category" id="exampleFormControlSelect1">
                                                <option value="{{$hs->bbp_category}}" selected>@if($hs->bbp_category == 1) 1 @elseif($hs->bbp_category == 2) 2 @elseif($hs->alcohol == 3) 3  @endif </option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-4 justify-content-md-center">
                        <div class="card-deck col-lg-12">
                            <div class="card border-danger text-white bg-dark">
                                <div class="card-header">Blood Pressure</div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="form-group ">
                                            <label for="exampleFormControlInput1">Systolic</label>
                                            <input type="number" value="{{$hs_bp->systolic}}" name="systolic" class="form-control" id="exampleFormControlInput1">
                                        </div>
                                        <div class="form-group ">
                                            <label for="exampleFormControlInput1">Diastolic</label>
                                            <input type="number" value="{{$hs_bp->distolic}}" name="distolic" class="form-control" id="exampleFormControlInput1">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card border-danger text-white bg-dark">
                                <div class="card-header">Height / Weight</div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="form-group ">
                                            <label for="exampleFormControlInput1">Feet</label>
                                            <input type="decimal" value="{{$hs_weight->height_feet}}" name="height_feet" class="form-control" id="exampleFormControlInput1">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleFormControlInput1">Inches</label>
                                            <input type="decimal" step=".01" value="{{$hs_weight->height_inches}}" name="height_inches" class="form-control" id="exampleFormControlInput1">
                                        </div>
                                        <div class="form-group ">
                                            <label for="exampleFormControlInput1">Weight</label>
                                            <input type="decimal" step=".01" value="{{$hs_weight->weight}}" name="weight" class="form-control" id="exampleFormControlInput1">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card border-danger text-white bg-dark">
                                <div class="card-header">TB Category</div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="form-group col">
                                            <label for="exampleFormControlSelect1">TB Category</label>
                                            <select class="form-control" name="tb_category" id="exampleFormControlSelect1" required>
                                                <option value="{{$hs->tb_category}}" selected>@if($hs->tb_category == 1) Medium @elseif($hs->tb_category == 2) High @else Low @endif </option>
                                                <option value="2">High</option>
                                                <option value="1">Medium</option>
                                                <option value="0">Low</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col">
                                            <label for="exampleFormControlSelect1">TB Mask Size</label>
                                            <select class="form-control" name="tb_mask_size" id="exampleFormControlSelect1" required>
                                                <option value="{{$hs->tb_mask_size}}" selected>@if($hs->tb_mask_size == 0) Regular @elseif($hs->tb_mask_size == 1) Small @endif </option>
                                                <option value="0">Regular</option>
                                                <option value="1">Small</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card border-danger text-white bg-dark">
                                <div class="card-header">
                                    Have you <b>developed any of the following <b>symptoms in the last year?</b></b>
                                </div>
                                <div class="card-body">
                                    <div class="form-check">
                                        <input class="form-check-input" name="cough" type="checkbox" value="1" id="defaultCheck1" @if($hs->cough == 1) checked @endif>
                                        <label class="form-check-label" for="defaultCheck1">
                                            Persistent cough or coughing up blood
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" name="rash" type="checkbox" value="1" id="defaultCheck2" @if($hs->rash == 1) checked @endif>
                                        <label class="form-check-label" for="defaultCheck2">
                                            Persistent skin rashes, abscesses or sores
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" name="sweat" type="checkbox" value="1" id="defaultCheck3" @if($hs->sweat == 1) checked @endif>
                                        <label class="form-check-label" for="defaultCheck3">
                                            Night sweats
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" name="fatigue" type="checkbox" value="1" id="defaultCheck4" @if($hs->fatigue == 1) checked @endif>
                                        <label class="form-check-label" for="defaultCheck4">
                                            Excessive fatigue
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" name="diarrhea" type="checkbox" value="1" id="defaultCheck5" @if($hs->diarrhea == 1) checked @endif>
                                        <label class="form-check-label" for="defaultCheck5">
                                            Diarrhea lasting more than 48 hours or with blood/mucous in stool
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="card border-danger text-white bg-dark">
                                <div class="card-header">
                                    Current Allergies
                                </div>
                                <div class="card-body">
                                    <div class="form-group">
                                        <textarea class="form-control emp_allergies" name="allergies" id="exampleFormControlTextarea1" rows="4">{{$allergies->allergies}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="card border-danger text-white bg-dark">
                                <div class="card-header">
                                    Do you have any health concerns or problems you would like to discuss with the Employee Health Nurse?
                                </div>
                                <div class="card-body">
                                    <div class="form-group">
                                        <textarea class="form-control" name="concerns" id="exampleFormControlTextarea1" rows="2">{{$hs->concerns}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <input type="submit" class="btn btn-primary btn-sm" value="Update Basic">
                    </div>
                </form>


            </div>
            <div class="tab-pane fade" id="nav-notes" role="tabpanel" aria-labelledby="nav-notes-tab">
                <br>
                <div class="card border-info text-white bg-dark">
                    <div class="card-header">
                        Nurse Notes
                    </div>
                    <div class="card-body">
                        @include('employee_health.includes.notes')
                </div>
            </div>
            <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">...</div>
        </div>
    </div>

    @include('employee_health.modals.enternote')
    @include('employee_health.modals.editnote')

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="application/javascript">
        @include('employee_health.includes.javascript.charts') //All charts JS
        @include('employee_health.includes.javascript.tab') //Tab script
        @include('employee_health.includes.javascript.modal') //All Modal JS data passes
    </script>

@endsection
@endif