@extends('layouts.app')

@section('content')

    @if(Auth::user()->hasRole('LE_Admin') == TRUE)
        <div class="card bg-secondary text-white mb-3">
            <div class="card-header">

                <div class="row">
                    <div class="col-lg-6">
                        Labor Efficiency Dashboard
                    </div>
                    <div class="col-lg-6 text-right">
                        <a href="/labor-efficiency/cc-report" class="btn btn-sm btn-primary">Cost Center List</a>
                        <a href="/labor-efficiency" class="btn btn-sm btn-primary">Return to Dashboard</a>
                    </div>
                </div>

            </div>
            <div class="card-body">
            
                <div class="card bg-dark text-white mb-3">
                    <div class="card-header">
                        All Cost Centers Data
                        <span class="float-right text-info">{{ Carbon::parse($startDate)->format('m/d/Y') }} - {{ Carbon::parse($endDate)->format('m/d/Y') }}</span>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-dark table-bordered table-striped table-hover text-center" style="border-radius:10px;" id="allData">
                                <thead>
                                    <th nowrap>Date</th>
                                    <th nowrap>Cost Center</th>
                                    <th nowrap>Division</th>
                                    <th nowrap>Stat</th>
                                    <th nowrap>Target MH/Stat</th>
                                    <th nowrap>Expected Hours</th>
                                    <th nowrap>Actual Hours</th>
                                    <th nowrap>PI %</th>
                                </thead>
                                <tbody>
                                    @foreach ($flexHours as $flex)
                                        <tr>
                                            <td nowrap>{{ Carbon::parse($flex->date)->format('m/d/Y') }}</td>
                                            <td nowrap>
                                                <a href="/labor-efficiency/show/{{ $flex->cost_center_id }}"><i class="fas fa-info-circle text-primary"></i></a>
                                                &nbsp;
                                                {{ $flex->style1 }}
                                            </td>
                                            <td nowrap>{{ $flex->csuite }}</td>
                                            <td nowrap>{{ $flex->uos }}</td>
                                            <td>{{ number_format($flex->benchmark, 2) }}</td>
                                            <td>{{ $flex->expected_work_hours }}</td>
                                            <td>@if(!is_null($flex->worked_hours)) {{ $flex->worked_hours }} @else <span class="text-danger">N/A</span> @endif</td>
                                            <td>@if(!is_null($flex->daily_pi)) {{ number_format($flex->daily_pi, 2) }}% @else <span class="text-danger">N/A</span> @endif</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="card bg-dark text-white col-lg-6 m-auto">
                    <div class="card-header">
                        No Entry
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-dark table-bordered table-striped table-hover" style="border-radius:10px;" id="missing">
                                <thead>
                                    <th class="text-center">Cost Center</th>
                                    <th class="text-center">Date</th>
                                    {{-- <th>Division</th> --}}
                                </thead>
                                <tbody>
                                    @foreach ($procedure as $missing)
                                        <tr>
                                            <td>
                                                <a href="/labor-efficiency/show/{{ $missing->id }}"><i class="fas fa-info-circle text-primary"></i></a>
                                                &nbsp;
                                                {{ $missing->style1 }}
                                            </td>
                                            <td>{{ $missing->date }}</td>
                                            {{-- <td>{{ $missing->csuite }}</td> --}}
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div> <!-- Main Card Body -->
        </div> <!-- Main Card -->

    @endif

@endsection

@section('scripts')

<script>


$(document).ready(function() {
    $('#allData').DataTable( {
        "pageLength": 10,
        "order": [],
        "columnDefs": [ {
            "targets"  : 'no-sort',
            "orderable": false,
        }]
    } );
} );

$(document).ready(function() {
    $('#missing').DataTable( {
        "pageLength": 10,
        "order": [],
        "columnDefs": [ {
            "targets"  : 'no-sort',
            "orderable": false,
        }]
    } );
} );

</script>


   
   
</script>    

@endsection