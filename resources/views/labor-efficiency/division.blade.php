@extends('layouts.app')

@section('content')


<div class="card bg-secondary text-white mb-3">
    <div class="card-header">

        <div class="row">
            <div class="col-lg-6">
                Labor Efficiency Dashboard <i>({{ $division }})</i>
            </div>
            <div class="col-lg-6 text-right">
                <a href="/labor-efficiency" class="btn btn-sm btn-primary">Return to Dashboard</a>
            </div>
        </div>

    </div>
    <div class="card-body">
        
        <div class="card-deck mb-3">
            <div class="card bg-dark text-white col-lg-8">
                <div class="card-header">
                    All Cost Centers Data
                    <span class="float-right">{{ Carbon::now()->subDays(1)->format('m-d-Y') }}</span>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-dark table-bordered table-striped table-hover text-center" style="border-radius:10px;" id="allData">
                            <thead>
                                <th nowrap>Cost Center</th>
                                <th nowrap>Campus</th>
                                <th nowrap>Expected Hours</th>
                                <th nowrap>Actual Hours</th>
                                <th nowrap>Flex Hours</th>
                                <th nowrap>PI %</th>
                            </thead>
                            <tbody>
                                @foreach ($flexHours as $flex)
                                    <tr>
                                        <td nowrap>
                                            <a href="/labor-efficiency/show/{{ $flex->cost_center_id }}"><i class="fas fa-info-circle text-primary"></i></a>
                                            &nbsp;
                                            {{ $flex->style1 }}
                                        </td>
                                        <td nowrap>{{ $flex->campus }}</td>
                                        <td>{{ $flex->expected_work_hours }}</td>
                                        <td>@if(!is_null($flex->worked_hours)) {{ $flex->worked_hours }} @else <span class="text-danger">N/A</span> @endif</td>
                                        <td>@if(!is_null($flex->flex_hours)) {{ $flex->flex_hours }} @else <span class="text-danger">N/A</span> @endif</td>
                                        <td>@if(!is_null($flex->daily_pi)) {{ number_format($flex->daily_pi, 2) }}% @else <span class="text-danger">N/A</span> @endif</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="card bg-dark text-white col-lg-4">
                <div class="card-header">
                    No Entry
                    <span class="float-right">{{ Carbon::now()->subDays(1)->format('m-d-Y') }}</span>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-dark table-bordered table-striped table-hover" style="border-radius:10px;" id="missing">
                            <thead>
                                <th class="text-center">Cost Center</th>
                            </thead>
                            <tbody>
                                @foreach ($missings as $missing)
                                    <tr>
                                        <td>
                                            <a href="/labor-efficiency/show/{{ $missing->id }}"><i class="fas fa-info-circle text-primary"></i></a>
                                            &nbsp;
                                            {{ $missing->style1 }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="card-deck mb-3">

            <div class="card bg-dark text-white col-lg-6">
                <div class="card-header">
                    Top 10 Cost Centers
                    <span class="float-right">{{ Carbon::now()->subDays(1)->format('m-d-Y') }}</span>
                </div>
                <div class="card-body">
                    <table class="table table-dark table-bordered table-striped table-hover text-center">
                        <thead class="bg-success">
                            <th colspan="2">Cost Center</th>
                            <th>Flex Hours</th>
                            <th>Productivity Index</th>
                        </thead>
                        <tbody>
                            @foreach ($topTen as $top)
                                <tr>
                                    <td> <a href="/labor-efficiency/show/{{ $top->cost_center_id }}"><i class="fas fa-info-circle text-success"></i></a> </td>
                                    <td>{{ $top->style1 }}</td>
                                    <td>{{ $top->flex_hours }}</td>
                                    <td>{{ number_format($top->daily_pi, 2) }}%</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="card bg-dark text-white col-lg-6">
                <div class="card-header">
                    Bottom 10 Cost Centers
                    <span class="float-right">{{ Carbon::now()->subDays(1)->format('m-d-Y') }}</span>
                </div>
                <div class="card-body">
                    <table class="table table-dark table-bordered table-striped table-hover text-center">
                        <thead class="bg-danger">
                            <th colspan="2">Cost Center</th>
                            <th>Flex Hours</th>
                            <th>Productivity Index</th>
                        </thead>
                        <tbody>
                            @foreach ($bottomTen as $bottom)
                                <tr>
                                    <td> <a href="/labor-efficiency/show/{{ $bottom->cost_center_id }}"><i class="fas fa-info-circle text-danger"></i></a> </td>
                                    <td>{{ $bottom->style1 }}</td>
                                    <td>{{ $bottom->flex_hours }}</td>
                                    <td>{{ number_format($bottom->daily_pi, 2) }}%</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>


    </div> <!-- Main Card Body -->
</div> <!-- Main Card -->



@endsection

@section('scripts')
<script>

    $(document).ready(function() {
        $('#allData').DataTable( {
            "pageLength": 10,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );

    $(document).ready(function() {
        $('#missing').DataTable( {
            "pageLength": 10,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );

</script>
@endsection 