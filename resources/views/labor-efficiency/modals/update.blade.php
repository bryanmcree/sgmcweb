<!-- Modal -->
<div class="modal fade" id="updateLE" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content text-white bg-dark">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Update Labor Efficiency</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form method="POST" action="/labor-efficiency/update">
                {{ csrf_field() }}
                <input type="hidden" class="id-value" name="id">
                <input type="hidden" class="target-value" name="benchmark">
                
                <div class="modal-body">
                    @if(Auth::user()->hasRole('Manual LE') == TRUE)
                        <div class="form-group">
                            <label>Date</label>
                            <input type="date" name="date" class="form-control date-value" required>
                        </div>
                        <div class="form-group">
                            <label>Volume</label>
                            <input type="decimal" step="0.01" name="expected_volume" class="form-control expected-volume">
                        </div>

                        <hr>

                        {{-- <div class="form-group">
                            <label>Actual Volume</label>
                            <input type="decimal" step="0.01" name="actual_volume" class="form-control actual-volume">
                        </div> --}}
                        <div class="form-group">
                            <label>Actual Worked Hours</label>
                            <input type="decimal" step="0.01" name="worked_hours" class="form-control actual-hours">
                        </div>
                    @else
                        <input type="hidden" name="date" class="form-control date-value" required>
                        <input type="hidden" step="0.01" name="expected_volume" class="form-control expected-volume">
                        <input type="hidden" step="0.01" name="worked_hours" class="form-control actual-hours">
                    @endif

                    <div class="form-group">
                        <label>Comments <small><i>(required if PI is less than 110%)</i></small></label>
                        <textarea name="comments" class="form-control comments" rows="3"></textarea>
                    </div>
                    
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Update">
                </div>

            </form>

        </div>
    </div>
</div>