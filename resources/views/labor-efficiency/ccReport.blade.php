@extends('layouts.app')

@section('content')

<div class="card bg-secondary text-white mb-3 col-lg-6 mx-auto">
    <div class="card-header">
        Choose your Cost Center
        <span class="float-right">
            <a href="/labor-efficiency" class="btn btn-sm btn-primary">Return to Dashboard</a>
        </span>
    </div>
    <div class="card-body">

        <table class="table table-dark table-hover table-striped table-bordered" id="ccSearch">
            <thead>
                <th>Cost Center</th>
                <th class="text-center">View Detail</th>
            </thead>
            <tbody>
                @foreach($ccs as $cc)
                    <tr>
                        <td>{{ $cc->style1 }}</td>
                        <td class="text-center"> <a href="/labor-efficiency/show/{{ $cc->id }}"><i class="fas fa-info-circle text-primary"></i></a> </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    </div>
</div>

@endsection

@section('scripts')

<script type="application/javascript">
    $(document).ready(function() {
        $('#ccSearch').DataTable( {
            "pageLength": 20,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>
    
@endsection