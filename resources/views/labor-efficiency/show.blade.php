@extends('layouts.app')

@section('content')

    <div class="card bg-secondary text-white mb-3">
        <div class="card-header">
            {{ $cc->style1 }}
            <span class="float-right">
                <a href="/labor-efficiency" class="btn btn-sm btn-dark">Return Home</a>
            </span>
        </div>
        <div class="card-body">

            <div class="card-group mb-3">

                <div class="card" style="box-shadow: none !important; background-color:#343a40; border-radius:0px;"> 
                    <div class="card-body">
                        <h4 class="card-title text-center">
                            Avg Work Hours: {{ number_format($avgWork->avg_work, 2) }}
                        </h4>
                    </div>
                </div>

                <div class="card" style="box-shadow: none !important; background-color:#343a40; border-radius:0px;">
                    <div class="card-body">
                        <h4 class="card-title text-center">
                            Avg Flex Hours: {{ number_format($avgFlex->avg_flex, 2) }}
                        </h4>
                    </div>
                </div>

                <div class="card" style="box-shadow: none !important; background-color:#343a40; border-radius:0px;">
                    <div class="card-body">
                        <h4 class="card-title text-center">
                            Avg Productivity Index: {{ number_format($avgProductivity->avg_pi, 1) }}%
                        </h4>
                    </div>
                </div>

                <div class="card" style="box-shadow: none !important; background-color:#343a40; border-radius:0px;">
                    <div class="card-body">
                        <h4 class="card-title text-center">
                            Avg MH/Stat: {{ number_format($avgMHstat->mh_stat, 2) }}
                        </h4>
                    </div>
                </div>

            </div>

                <div class="card-deck mb-3">

                    @if(Auth::user()->hasRole('Manual LE') == TRUE)
                    <div class="card bg-dark text-white col-lg-4">
                        <div class="card-header row">
                            <div class="col-lg-6 text-center border-right">
                                <i>Your Target MH/Stat: <span class="text-info">{{ $cc->worked_target }}</span></i>
                            </div>
                            <div class="col-lg-6 text-center">
                                <i>Your Stat: <span class="text-info">{{ $cc->uos }}</span></i>
                            </div>
                        </div>
                        <div class="card-body">
                            <form action="/labor-efficiency/store" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" value="{{ $cc->id }}" name="cost_center_id">
                                <input type="hidden" value="{{ Auth::user()->employee_number }}" name="created_by">
                                <input type="hidden" value="{{ $cc->worked_target }}" name="benchmark">

                                <div class="form-group">
                                    <label>Date</label>
                                    <input type="date" name="date" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label> @if($cc->uos == 'Fixed') Fixed Work Hours @else Volume @endif</label>
                                    <input type="number" step="0.01" name="expected_volume" class="form-control">
                                </div>

                                <hr>

                                <input type="submit" class="btn btn-primary btn-block" value="Calculate">
                                
                            </form>
                        </div>
                    </div>
                    @endif

                    <div class="card bg-dark text-white col-lg-8 m-auto">
                        <div class="card-body">

                            <canvas id="bar-chart-variance" height="60"></canvas>

                        </div>
                    </div>

                </div>


            <table class="table table-dark table-hover table-striped table-bordered" id="results">
                <thead>
                    <th>Date</th>
                    <th>@if($cc->uos == 'Fixed') Fixed Work Hours @else Volume @endif</th>
                    <th>Expected Work Hours</th>
                    {{-- <th>Actual Volume</th> --}}
                    <th>Actual Work Hours</th>
                    <th>Flex Hours</th>
                    <th>Productivity Index</th>
                    <th>MH/Stat</th>
                    <th>Expected FTEs</th>
                    <th>Worked FTEs</th>
                    <th>Comments</th>
                    <th>Update</th>
                </thead>
                <tbody>
                    @foreach ($recents as $recent)
                        <tr @if(is_null($recent->worked_hours)) class="text-danger" @endif>
                            <td nowrap>{{ Carbon::parse($recent->date)->format('m-d-Y') }}</td>
                            <td nowrap>{{ $recent->expected_volume }}</td>
                            <td nowrap>{{ $recent->expected_work_hours }}</td>
                            {{-- <td>@if(!empty($recent->actual_volume)) {{ $recent->actual_volume }} @else N/A @endif</td> --}}
                            <td nowrap>@if(!is_null($recent->worked_hours)) {{ $recent->worked_hours }} @else N/A @endif</td>
                            <td nowrap>@if(!is_null($recent->flex_hours)) {{ $recent->flex_hours }} @else N/A @endif</td>
                            <td nowrap>@if(!is_null($recent->daily_pi)) {{ number_format($recent->daily_pi, 2) }}% @else N/A @endif</td>
                            <td nowrap>@if(!is_null($recent->MH_stat)) {{ number_format($recent->MH_stat, 2) }} @else N/A @endif</td>
                            <td nowrap>@if($recent->expected_work_hours > 0) {{ number_format($recent->expected_work_hours / $cc->le_denom,2) }} @else N/A @endif</td>
                            <td nowrap>@if($recent->worked_hours > 0) {{ number_format($recent->worked_hours / $cc->le_denom,2) }} @else N/A @endif</td>
                            <td>@if(!empty($recent->comments)) {{ $recent->comments }} @else N/A @endif</td>
                            <td class="text-center">
                                <a href="#" class="btn btn-sm btn-primary updateLEbtn" var_id="{{ $recent->id }}"
                                var_date="{{ Carbon::parse($recent->date)->format('Y-m-d') }}" var_expected_volume="{{ $recent->expected_volume }}" 
                                var_actual_hours="{{ $recent->worked_hours }}" var_actual_volume="{{ $recent->actual_volume }}"
                                var_benchmark="{{ $cc->worked_target }}" var_comments = "{{ $recent->comments }}">
                                <i class="fas fa-edit"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>

    @include('labor-efficiency.modals.update')

@endsection

@section('scripts')

<script>

    //Pass values to modal for editing
    $(document).on("click", ".updateLEbtn", function () {
        $('.id-value').val($(this).attr("var_id"));
        $('.date-value').val($(this).attr("var_date"));
        $('.expected-volume').val($(this).attr("var_expected_volume"));
        $('.actual-hours').val($(this).attr("var_actual_hours"));
        $('.actual-volume').val($(this).attr("var_actual_volume"));
        $('.target-value').val($(this).attr("var_benchmark"));
        $('.comments').val($(this).attr("var_comments"));
        $('#updateLE').modal('show');
    });

    $(document).ready(function() {
        $('#results').DataTable( {
            "pageLength": 10,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );

</script>  

<script>
    
var pointBackgroundColors = [];

var mid = 100;

var myChart = new Chart(document.getElementById("bar-chart-variance"), {
    type: 'line',
    data: {
        labels: [
            @if(!is_null($flexChart))
                @foreach($flexChart as $data)
                    '{{ $data->date }}',
                @endforeach
            @else
                ''
            @endif
        ],
        datasets: [
            {
                label: "PI %",
                // backgroundColor: "rgba(29, 149, 247, .6)",
                pointBackgroundColor: pointBackgroundColors,
                borderWidth: 1,
                fill: false,
                data: [
                    @foreach($flexChart as $values)
                        {{ $values->daily_pi}},
                    @endforeach
                ],
                borderColor: "rgba(0, 0, 0, .8)",
            }
        ]
    },
    options: {
        elements: {
            line: {
                tension: 0
            }
        },
        legend: { display: false },
        title: {
            display: true,
            text: 'Productivity Index % Last 30 Days'
        },
        scales: {
            yAxes: [{
                stacked:false,
            ticks: {
                beginAtZero: false
                }
            }],
            xAxes: [{
                stacked:false,
                display: false,
                ticks: {
                    beginAtZero: true
                }
            }]

        },
        annotation: {
            annotations: [
                {
                    type: 'line',
                    mode: 'horizontal',
                    scaleID: 'y-axis-0',
                    value: mid,
                    borderColor: 'rgba(255, 196, 0, 0.6)',
                    borderWidth: 0,
                    label: {
                        backgroundColor: 'transparent',
                        enabled: true,
                        yAdjust: 12,
                        content: 'Target: 100%'
                    }
                }
            ]
        }
    }
});

for (i = 0; i < myChart.data.datasets[0].data.length; i++) {
    if (myChart.data.datasets[0].data[i] > mid) {
        pointBackgroundColors.push("#00f2b6");
    } else {
        pointBackgroundColors.push("#f50707");
    }
}

myChart.update();

</script>

@endsection