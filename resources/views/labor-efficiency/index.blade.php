@extends('layouts.app')

@section('content')

    @if(Auth::user()->hasRole('LE_Admin') == TRUE)
        <div class="card bg-secondary text-white mb-3">
            <div class="card-header">

                <div class="row">
                    <div class="col-lg-6">
                        Labor Efficiency Dashboard
                    </div>
                    <div class="col-lg-6 text-right">
                        <a href="#reportExcel" class="btn btn-info btn-sm" data-toggle="modal">
                            Report to Excel
                        </a>
                        <a href="#reportExcelDivision" class="btn btn-info btn-sm" data-toggle="modal">
                            Report By Division to Excel
                        </a>
                        <a href="#reportLE" class="btn btn-info btn-sm" data-toggle="modal">
                            Report
                        </a>
                        <button class="btn btn-info btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Filter By Officer
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            @foreach ($divisions as $division)
                                <a href="/labor-efficiency/division/{{ $division->csuite }}" class="dropdown-item">{{ $division->csuite }}</a>
                            @endforeach
                        </div>
                        <a href="/labor-efficiency/cc-report" class="btn btn-sm btn-primary">Cost Center List</a>
                    </div>
                </div>

            </div>
            <div class="card-body">
                
                <div class="card-deck mb-3">
                    <div class="card bg-dark text-white col-lg-8">
                        <div class="card-header">
                            All Cost Centers Data
                            <span class="float-right">{{ Carbon::now()->subDays(1)->format('m-d-Y') }}</span>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-dark table-bordered table-striped table-hover text-center" style="border-radius:10px;" id="allData">
                                    <thead>
                                        <th nowrap>Cost Center</th>
                                        <th nowrap>Campus</th>
                                        <th nowrap>Expected Hours</th>
                                        <th nowrap>Actual Hours</th>
                                        <th nowrap>Flex Hours</th>
                                        <th nowrap>PI %</th>
                                    </thead>
                                    <tbody>
                                        @foreach ($flexHours as $flex)
                                            <tr>
                                                <td nowrap>
                                                    <a href="/labor-efficiency/show/{{ $flex->cost_center_id }}"><i class="fas fa-info-circle text-primary"></i></a>
                                                    &nbsp;
                                                    {{ $flex->style1 }}
                                                </td>
                                                <td nowrap>{{ $flex->campus }}</td>
                                                <td>{{ $flex->expected_work_hours }}</td>
                                                <td>@if(!is_null($flex->worked_hours)) {{ $flex->worked_hours }} @else <span class="text-danger">N/A</span> @endif</td>
                                                <td>@if(!is_null($flex->flex_hours)) {{ $flex->flex_hours }} @else <span class="text-danger">N/A</span> @endif</td>
                                                <td>@if(!is_null($flex->daily_pi)) {{ number_format($flex->daily_pi, 1) }}% @else <span class="text-danger">N/A</span> @endif</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="card bg-dark text-white col-lg-4">
                        <div class="card-header">
                            No Entry
                            <span class="float-right">{{ Carbon::now()->subDays(1)->format('m-d-Y') }}</span>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-dark table-bordered table-striped table-hover" style="border-radius:10px;" id="missing">
                                    <thead>
                                        <th class="text-center">Cost Center</th>
                                    </thead>
                                    <tbody>
                                        @foreach ($missings as $missing)
                                            <tr>
                                                <td>
                                                    <a href="/labor-efficiency/show/{{ $missing->id }}"><i class="fas fa-info-circle text-primary"></i></a>
                                                    &nbsp;
                                                    {{ $missing->style1 }}
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-deck mb-3">

                    <div class="card bg-dark text-white col-lg-6">
                        <div class="card-header">
                            Top 10 Cost Centers
                            <span class="float-right">{{ Carbon::now()->subDays(1)->format('m-d-Y') }}</span>
                        </div>
                        <div class="card-body">
                            <table class="table table-dark table-bordered table-striped table-hover text-center">
                                <thead class="bg-success">
                                    <th colspan="2">Cost Center</th>
                                    <th>Flex Hours</th>
                                    <th>Productivity Index</th>
                                </thead>
                                <tbody>
                                    @foreach ($topTen as $top)
                                        <tr>
                                            <td> <a href="/labor-efficiency/show/{{ $top->cost_center_id }}"><i class="fas fa-info-circle text-success"></i></a> </td>
                                            <td>{{ $top->cost_center->style1 }}</td>
                                            <td>{{ $top->flex_hours }}</td>
                                            <td>{{ number_format($top->daily_pi, 1) }}%</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="card bg-dark text-white col-lg-6">
                        <div class="card-header">
                            Bottom 10 Cost Centers
                            <span class="float-right">{{ Carbon::now()->subDays(1)->format('m-d-Y') }}</span>
                        </div>
                        <div class="card-body">
                            <table class="table table-dark table-bordered table-striped table-hover text-center">
                                <thead class="bg-danger">
                                    <th colspan="2">Cost Center</th>
                                    <th>Flex Hours</th>
                                    <th>Productivity Index</th>
                                </thead>
                                <tbody>
                                    @foreach ($bottomTen as $bottom)
                                        <tr>
                                            <td> <a href="/labor-efficiency/show/{{ $bottom->cost_center_id }}"><i class="fas fa-info-circle text-danger"></i></a> </td>
                                            <td>{{ $bottom->cost_center->style1 }}</td>
                                            <td>{{ $bottom->flex_hours }}</td>
                                            <td>{{ number_format($bottom->daily_pi, 1) }}%</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>

                <div class="card bg-dark text-white mb-3 col-lg-8 m-auto">
                    <div class="card-header">
                        SGMC Trend Last 7 Days
                    </div>
                    <div class="card-body">
                        <canvas id="trendChart"></canvas>
                    </div>
                </div>

            </div> <!-- Main Card Body -->
        </div> <!-- Main Card -->

    @else

    <div class="card bg-secondary text-white mb-3">
        <div class="card-header">
            Labor Efficiency Dashboard <small class="text-black"><i>(This Dashboard Only Shows Cost Centers You Are Director/Manager of. Please Submit A Help Ticket If Any Cost Centers Are Missing.)</i></small>
            <span class="float-right">
                <a href="/labor-efficiency/cc-report" class="btn btn-sm btn-primary">Cost Center List</a>
            </span>
        </div>
        <div class="card-body">
            
            <div class="card-deck mb-3">
                <div class="card bg-dark text-white col-lg-9">
                    <div class="card-header">
                        All Cost Centers Data
                        <span class="float-right">Last 7 Days</span>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-dark table-bordered table-striped table-hover text-center" style="border-radius:10px;" id="dirData">
                                <thead>
                                    <th nowrap>Date</th>
                                    <th nowrap>Cost Center</th>
                                    <th nowrap>Expected Work Hours</th>
                                    <th nowrap>Actual Work Hours</th>
                                    <th nowrap>Flex Hours</th>
                                    <th nowrap>Productivity Index</th>
                                </thead>
                                <tbody>
                                    @foreach ($dirflexHours as $flex)
                                        <tr>
                                            <td>{{ Carbon::parse($flex->date)->format('m-d-Y') }}</td>
                                            <td>
                                                <a href="/labor-efficiency/show/{{ $flex->cost_center_id }}"><i class="fas fa-info-circle text-primary"></i></a>
                                                &nbsp;
                                                {{ $flex->style1 }}
                                            </td>
                                            <td>{{ $flex->expected_work_hours }}</td>
                                            <td>@if(!is_null($flex->worked_hours)) {{ $flex->worked_hours }} @else <span class="text-danger">N/A</span> @endif</td>
                                            <td>@if(!is_null($flex->flex_hours)) {{ $flex->flex_hours }} @else <span class="text-danger">N/A</span> @endif</td>
                                            <td>@if(!is_null($flex->daily_pi)) {{ number_format($flex->daily_pi, 2) }}% @else <span class="text-danger">N/A</span> @endif</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="card bg-dark text-white col-lg-3">
                    <div class="card-header">
                        Awaiting Results
                        <span class="float-right">{{ Carbon::now()->subDays(1)->format('m-d-Y') }}</span>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive" style="max-height:615px; overflow-y:auto">
                            <table class="table table-dark table-bordered table-striped table-hover" style="border-radius:10px;">
                                <thead>
                                    <th class="text-center">Cost Center</th>
                                </thead>
                                <tbody>
                                    @foreach ($dirmissings as $missing)
                                        <tr>
                                            <td>
                                                <a href="/labor-efficiency/show/{{ $missing->cost_center_id }}"><i class="fas fa-info-circle text-primary"></i></a>
                                                &nbsp;
                                                {{ $missing->style1 }}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-deck mb-3">

                <div class="card bg-dark text-white col-lg-6">
                    <div class="card-header">
                        Top 10 Cost Centers
                        <span class="float-right">{{ Carbon::now()->subDays(1)->format('m-d-Y') }}</span>
                    </div>
                    <div class="card-body">
                        <table class="table table-dark table-bordered table-striped table-hover text-center">
                            <thead class="bg-success">
                                <th colspan="2">Cost Center</th>
                                <th>Flex Hours</th>
                                <th>Productivity Index</th>
                            </thead>
                            <tbody>
                                @foreach ($dirtopTen as $top)
                                    <tr>
                                        <td> <a href="/labor-efficiency/show/{{ $top->cost_center_id }}"><i class="fas fa-info-circle text-success"></i></a> </td>
                                        <td>{{ $top->style1 }}</td>
                                        <td>{{ $top->flex_hours }}</td>
                                        <td>{{ number_format($top->daily_pi, 2) }}%</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="card bg-dark text-white col-lg-6">
                    <div class="card-header">
                        Bottom 10 Cost Centers
                        <span class="float-right">{{ Carbon::now()->subDays(1)->format('m-d-Y') }}</span>
                    </div>
                    <div class="card-body">
                        <table class="table table-dark table-bordered table-striped table-hover text-center">
                            <thead class="bg-danger">
                                <th colspan="2">Cost Center</th>
                                <th>Flex Hours</th>
                                <th>Productivity Index</th>
                            </thead>
                            <tbody>
                                @foreach ($dirbottomTen as $bottom)
                                    <tr>
                                        <td> <a href="/labor-efficiency/show/{{ $bottom->cost_center_id }}"><i class="fas fa-info-circle text-danger"></i></a> </td>
                                        <td>{{ $bottom->style1 }}</td>
                                        <td>{{ $bottom->flex_hours }}</td>
                                        <td>{{ number_format($bottom->daily_pi, 2) }}%</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>

        </div> <!-- Main Card Body -->
    </div> <!-- Main Card -->

    {{-- <div class="card bg-secondary text-white mb-3 col-lg-8 mx-auto">
        <div class="card-header">
            Choose your Cost Center
        </div>
        <div class="card-body">

            <table class="table table-dark table-hover table-striped table-bordered" id="ccSearch">
                <thead>
                    <th class="text-center">Clinical</th>
                    <th style="width:55%">Cost Center</th>
                    <th class="text-center">Calculate LE</th>
                </thead>
                <tbody>
                    @foreach($ccs as $cc)
                        <tr>
                            <td class="text-center">@if($cc->clinical == 1) <i class="fas fa-user-md" style="color:deeppink;"></i> @else - @endif</td>
                            <td>{{ $cc->style1 }}</td>
                            <td class="text-center"> <a href="/labor-efficiency/show/{{ $cc->id }}"><i class="fas fa-info-circle text-primary"></i></a> </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div> --}}

    @endif

    @include('labor-efficiency.modals.report')
    @include('labor-efficiency.modals.reportExcel')
    @include('labor-efficiency.modals.reportExcelDivision')

@endsection

@section('scripts')

<script>

$(document).ready(function() {
    $('#ccSearch').DataTable( {
        "pageLength": 20,
        "order": [],
        "columnDefs": [ {
            "targets"  : 'no-sort',
            "orderable": false,
        }]
    } );
} );

$(document).ready(function() {
    $('#allData').DataTable( {
        "pageLength": 10,
        "order": [],
        "columnDefs": [ {
            "targets"  : 'no-sort',
            "orderable": false,
        }]
    } );
} );

$(document).ready(function() {
    $('#dirData').DataTable( {
        "pageLength": 10,
        "order": [],
        "columnDefs": [ {
            "targets"  : 'no-sort',
            "orderable": false,
        }]
    } );
} );

$(document).ready(function() {
    $('#missing').DataTable( {
        "pageLength": 10,
        "order": [],
        "columnDefs": [ {
            "targets"  : 'no-sort',
            "orderable": false,
        }]
    } );
} );

</script>

<script type="application/javascript">

{{-- Trend Seven Days --}}
@if(!empty($weeklyTrend))
new Chart(document.getElementById("trendChart"), {
type: 'line',
data: {
labels: [
@foreach($weeklyTrend as $trend)
    "{{Carbon::parse($trend->date)->format('m-d-Y')}}",
@endforeach
],
datasets: [{
data: [
@foreach($weeklyTrend as $trend)
    {{$trend->avgflex}},
@endforeach
],
label: "Average of Flex Hours",
borderColor: "#8d3dff",
fill: false
}
]
},
options: {
title: {
display: true,
text: '7 Day Trend'
},
scales: {
yAxes: [{
stacked:false,
ticks: {
beginAtZero: true
}
}]
}
}
});
@endif

   
   
</script>    

@endsection