{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Menu') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <form method="post" action="/menu/admin/addmenu">
            <input type="hidden" value="{{Auth::user()->employee_number}}" name="created_by">
            {{ csrf_field() }}
            <div class="card text-black border-dark mb-3 mt-3">
                <div class="card-header">
                    <ul class="nav">
                        <li class="nav-item">
                            <a href="/menu/admin" class="btn btn-info"><b>SGMC Food Services Home</b></a>
                        </li>
                        <p>&nbsp;&nbsp;</p>
                        <li class="nav-item">
                            <a class="btn btn-info" href="/menu/admin/menus">Create new Menu</a>
                        </li>
                        <p>&nbsp;&nbsp;</p>
                        <li class="nav-item">
                            <a class="btn btn-info" href="/menu/admin/items">Create new Food Item</a>
                        </li>
                    </ul>
                </div>
                <div class="card-body">
                    <div class="card-title">
                        <h2>Create a New Menu and Add Food Items</h2>
                        <p>After choosing the date and location of your menu, please remember to populate your menu with food items by clicking on each item you want to display below. To remove an item you will just need to click it again.</p>
                        <hr>
                    </div>
                
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="formGroupExampleInput">Menu Start Date:</label>
                            <input type="date" name="start_date" class="form-control" id="formGroupExampleInput" required>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Menu Location:</label>
                            <select class="form-control" name="location" id="location">
                                <option selected value="">[Select Location]</option>
                                <option  value="Lanier">Lanier</option>
                                <option  value="Main - Cafeteria">Main - Cafeteria</option>
                                <option  value="Main - All Spice">Main - All Spice</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Display Location:</label>
                            <select class="form-control" name="Display" id="display">
                                <option selected value="">[Select Display]</option>
                                <option  value="Breakfast-Grill">Breakfast-Grill</option>
                                <option  value="Breakfast-Salad">Breakfast-Salad</option>
                                <option  value="Breakfast-Deli">Breakfast-Deli</option>
                                <option  value="Salad">Salad</option>
                                <option  value="Grill">Grill</option>
                                <option  value="Deli">Deli</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="items">Items:</label>
                            <select name="item[]" class="form-control" multiple="multiple" id="item_select">

                                    <optgroup label = "Main Dishes">
                                    @foreach($main as $item)
                                        <option value="{{$item->id}}">{{$item->title}}</option>
                                    @endforeach

                                    <optgroup label = "Side Items">
                                    @foreach($side as $item)
                                        <option value="{{$item->id}}">{{$item->title}}</option>
                                    @endforeach

                                    <optgroup label = "Extra Items">
                                    @foreach($extra as $item)
                                        <option value="{{$item->id}}">{{$item->title}}</option>
                                    @endforeach

                                    <optgroup label = "Featured Dishes">
                                    @foreach($feature as $item)
                                        <option value="{{$item->id}}">{{$item->title}}</option>
                                    @endforeach

                            </select>
                        </div>
                    </div>
                    <p><input type="submit" value="Add Items" class="btn btn-primary btn-lg btn-block"></p>
                </div>
            </div>
        </form>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

<script>
    var ingredients = jQuery('#location');
    var select = this.value;
    ingredients.change(function () {
    if ($(this).val() == 'Main - All Spice') {
        $('.display').show();
    }
    else $('.display').hide();
});
</script>


<script>
    var select_element = document.getElementById('item_select');
    multi(select_element), {
        'enable_search': true,
        'search_placeholder': 'Search...',
        'non_selected_header': 'All options',
        'selected_header': 'Selected options'
    };

</script>


    <script type="application/javascript">
        $(document).ready(function() {
            $('#menus').DataTable( {
                "pageLength": 20,
                "order": [],
                "columnDefs": [ {
                    "targets"  : 'no-sort',
                    "orderable": false,
                }]
            } );
        } );
    </script>

@endsection
@endif