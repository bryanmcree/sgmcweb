<meta http-equiv="refresh" content="95">

@extends('layouts.new_nonav')
    {{--Page Design Goes Below--}}
@section('content')

<body>

    {{-- {{Carbon\Carbon::now()->format('H:i:s')}} --}}

    @if(Carbon\Carbon::now()->format('H:i:s') > '05:30:00' AND Carbon\Carbon::now()->format('H:i:s') < '10:30:00' )
        <img src="/img/GoodMorning_Header.png" class="img-fluid" style="width: 100%;" alt="header">
    @else
        <img src="/img/Deli_Header.png" class="img-fluid" style="width: 100%;" alt="header">
    @endif

    @include('menu.admin.layouts.carousel_deli')

</body>

        <div class="card-footer bg-dark text-white">
            <h2 class="text-center">2000 calories a day is used for general nutrition advice, but calorie needs vary.
                <br>Additional nutrition information is avaliable upon request.
            </h2>
        </div>



@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

<script>
    $('.carousel').carousel({
    interval: 15000 // Change to 20000
  })
</script>

<script type="application/javascript">
    $(document).ready(function() {
        $('#show').DataTable( {
            "pageLength": 20,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>

@endsection
