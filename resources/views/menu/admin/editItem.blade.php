{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Menu') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <form method="post" action="/menu/admin/updateItem/{{$item->id}}">
            <input type="hidden" value="{{Auth::user()->employee_number}}" name="created_by">
            {{ csrf_field() }}
            <div class="card text-black border-dark mb-3 mt-3">
                <div class="card-header">
                    <ul class="nav">
                        <li class="nav-item">
                            <a href="/menu/admin" class="btn btn-info"><b>SGMC Food Services Home</b></a>
                        </li>
                        <p>&nbsp;&nbsp;</p>
                        <li class="nav-item">
                            <a class="btn btn-info" href="/menu/admin/menus">Create new Menu</a>
                        </li>
                        <p>&nbsp;&nbsp;</p>
                        <li class="nav-item">
                            <a class="btn btn-info" href="/menu/admin/items">Create new Food Item</a>
                        </li>
                    </ul>
                </div>
                <div class="card-body">
                    <div class="card-title">
                        <h2>Create a New Food Item for Your Menu</h2>
                        <p>Please fill out all of the fields in the form below and click the 'Add Dish' button to save it for later use on any menu. You may also create a 'Featured Dish' by specifying the category as featured in the dropdown below.</p>
                        <hr>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="formGroupExampleInput">Dish Title</label>
                            <input type="text" name="title" class="form-control" id="formGroupExampleInput" value="{{$item->title}}" required>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="formGroupExampleInput">Dish Price</label>
                            <input type="number" name="price" class="form-control" step="0.01" id="formGroupExampleInput" value="{{$item->price}}" required>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="formGroupExampleInput">Dish Calories</label>
                            <input type="number" name="calories" class="form-control" min="0" value="{{$item->calories}}" required>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Dish Category</label>
                            <select class="form-control" name="category" id="category">
                                    <option selected value="{{$item->menuCategory->id}}">{{$item->menuCategory->category}}</option>
                                @foreach($menu_category as $category)
                                    <option value="{{$category->id}}">{{$category->category}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group ingredients" @if($item->menuCategory->id != 4) style=" display: none;" @endif>
                            <label for="formGroupExampleInput">Ingredients <i>(Only if featured)</i> <br>Syntax Ex: Lettuce | Tomato | Pickle </label>
                            <input type="text" name="ingredients" class="form-control" value="{{$item->ingredients}}">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <p><input type="submit" value="Update Dish" class="btn btn-primary btn-block"></p>
                    </div>
                </div>
            </div>
            </form>
        </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

<script>
    var ingredients = jQuery('#category');
    var select = this.value;
    ingredients.change(function () {
    if ($(this).val() == '4') {
        $('.ingredients').show();
    }
    else $('.ingredients').hide(); // hide div if value is not "featured"
});
</script>

    <script type="application/javascript">
        $(document).ready(function() {
            $('#items').DataTable( {
                "pageLength": 20,
                "order": [],
                "columnDefs": [ {
                    "targets"  : 'no-sort',
                    "orderable": false,
                }]
            } );
        } );
    </script>

@endsection
@endif