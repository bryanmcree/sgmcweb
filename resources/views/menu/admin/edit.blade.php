{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Menu') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="">

    </div>

    <div class="col-md-12">
        <form method="post" action="/menu/admin/update/{{ $menu->id }}">
            <input type="hidden" value="{{Auth::user()->employee_number}}" name="created_by">
            {{ csrf_field() }}
            <div class="card text-black border-dark mb-3 mt-3">
                <div class="card-header">
                    <ul class="nav">
                        <li class="nav-item">
                            <a href="/menu/admin" class="btn btn-info"><b>SGMC Food Services Home</b></a>
                        </li>
                        <p>&nbsp;&nbsp;</p>
                        <li class="nav-item">
                            <a class="btn btn-info" href="/menu/admin/menus">Create new Menu</a>
                        </li>
                        <p>&nbsp;&nbsp;</p>
                        <li class="nav-item">
                            <a class="btn btn-info" href="/menu/admin/items">Create new Food Item</a>
                        </li>
                    </ul>
                </div>
            <div class="card-body">
                <div class="card-title">
                    <h2>Edit Food Items For: <span class="text-primary">{{ $menu->start_date->toFormattedDateString() }}</span></h2>
                    <p>Please check off each item you want to update to the current menu and click the 'Update Menu' button below.</p>
                    <hr>
                </div>
                
                <div class="col-lg-12">
                    <div class="col-lg-6 well">
                        <div class="form-group">
                            <label for="formGroupExampleInput">Menu Start Date</label>
                        <input type="date" value="{{ $newFormat }}" name="start_date" class="form-control" id="formGroupExampleInput" required>
                        </div>
                    </div>
                    <div class="col-lg-6 well">
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Menu Location</label>
                            <select class="form-control" name="location" id="exampleFormControlSelect1">
                                <option selected value="{{ $menu->location }}">{{ $menu->location }}</option>
                                <option  value="Lanier">Lanier</option>
                                <option  value="Main - Cafeteria">Main - Cafeteria</option>
                                <option  value="Main - All Spice">Main - All Spice</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Display Location:</label>
                            <select class="form-control" name="Display" id="exampleFormControlSelect1">
                                <option selected value="{{$menu->Display}}">{{$menu->Display}}</option>
                                <option  value="Breakfast-Grill">Breakfast-Grill</option>
                                <option  value="Breakfast-Salad">Breakfast-Salad</option>
                                <option  value="Breakfast-Deli">Breakfast-Deli</option>
                                <option  value="Salad">Salad</option>
                                <option  value="Grill">Grill</option>
                                <option  value="Deli">Deli</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="items">Items:</label>
                            <select name="item[]" class="form-control" id="edit_menu" multiple="multiple">

                                @if(count($chosenMain) > 0)
                                    <optgroup label = "Main Dishes">
                                    @foreach($main as $item)
                                        <option @if($item->id == $chosenMain[$counter]) selected value="{{$chosenMain[$counter]}}" @else value="{{ $item->id }}" @endif>{{$item->title}}</option>
                                    
                                        @if($counter < $maxMain -1 && $item->id == $chosenMain[$counter])
                                            {{$counter++}}
                                        @endif
                                    @endforeach
                                @else 
                                    <optgroup label = "Main Dishes">
                                    @foreach($main as $item)
                                        <option value="{{ $item->id }}">{{$item->title}}</option>
                                    @endforeach
                                @endif
                                {{$counter = 0}}
                                
                                @if(count($chosenSide) > 0)
                                    <optgroup label = "Side Items">
                                    @foreach($side as $item)
                                        <option @if($item->id == $chosenSide[$counter]) selected value="{{$chosenSide[$counter]}}" @else value="{{ $item->id }}" @endif>{{$item->title}}</option>
                                    
                                        @if($counter < $maxSide -1 && $item->id == $chosenSide[$counter])
                                            {{$counter++}}
                                        @endif
                                    @endforeach
                                @else 
                                    <optgroup label = "Side Items">
                                    @foreach($side as $item)
                                        <option value="{{ $item->id }}">{{$item->title}}</option>
                                    @endforeach
                                @endif
                                {{$counter = 0}}

                                @if(count($chosenExtra) > 0)
                                    <optgroup label = "Extra Items">
                                    @foreach($extra as $item)
                                        <option @if($item->id == $chosenExtra[$counter]) selected value="{{$chosenExtra[$counter]}}" @else value="{{ $item->id }}" @endif>{{$item->title}}</option>
                                    
                                        @if($counter < $maxExtra -1 && $item->id == $chosenExtra[$counter])
                                            {{$counter++}}
                                        @endif
                                    @endforeach
                                @else 
                                    <optgroup label = "Extra Items">
                                    @foreach($extra as $item)
                                        <option value="{{ $item->id }}">{{$item->title}}</option>
                                    @endforeach
                                @endif
                                {{$counter = 0}}

                                @if(count($chosenFeature) > 0)
                                    <optgroup label = "Featured Dishes">
                                    @foreach($feature as $item)
                                        <option @if($item->id == $chosenFeature[$counter]) selected value="{{$chosenFeature[$counter]}}" @else value="{{ $item->id }}" @endif>{{$item->title}}</option>
                                    
                                        @if($counter < $maxFeature -1 && $item->id == $chosenFeature[$counter])
                                            {{$counter++}}
                                        @endif
                                    @endforeach
                                @else 
                                    <optgroup label = "Featured Dishes">
                                    @foreach($feature as $item)
                                        <option value="{{ $item->id }}">{{$item->title}}</option>
                                    @endforeach
                                @endif

                            </select>
                        </div>
                    </div>
                <p><input type="submit" value="Update Menu" class="btn btn-primary btn-lg btn-block"></p>
            </div>
        </div>
        </form>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

<script>
    var select_element = document.getElementById('edit_menu');
    multi(select_element), {
        'enable_search': true,
        'search_placeholder': 'Search...',
        'non_selected_header': 'All options',
        'selected_header': 'Selected options'
    };


    $('#edit_menu').multi().val( {!! json_encode($menu->menuItems()->getRelatedIds()) !!} ).trigger('change');
</script>

{{-- <script>
    $(document).ready(function() {
        //$('.js-example-basic-multiple').select2();
        //$('.js-example-basic-multiple').select2().val({!! json_encode($menuPopulated->menuItems()->getRelatedIds()) !!}).trigger('change');

    });
</script>

    <script type="application/javascript">
        $(document).ready(function() {
            $('#items').DataTable( {
                "pageLength": 20,
                "order": [],
                "columnDefs": [ {
                    "targets"  : 'no-sort',
                    "orderable": false,
                }]
            } );

            //$("select[name='edit_menu']").find("option[value='json_encode($menuPopulated->menuItems()->getRelatedIds())']").attr("selected",true);
            //$('#edit_menu').val({!! json_encode($menuPopulated->menuItems()->getRelatedIds()) !!}).trigger('change');
            //$("select[name='edit_menu']").find("option[value='json_encode($menuPopulated->menuItems()->getRelatedIds())']").attr("selected",true);


            
        } );
    </script> --}}

@endsection
@endif