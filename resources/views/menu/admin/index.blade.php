{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Menu') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
    
@section('content')

    <div class="col-md-12">
        <div class="card mb-3 mt-3">
            <div class="card-header">
                <ul class="nav">
                    <li class="nav-item">
                        <a href="/menu/admin" class="btn btn-info"><b>SGMC Food Services Home</b></a>
                    </li>
                    <p>&nbsp;&nbsp;</p>
                    <li class="nav-item">
                        <a class="btn btn-info" href="/menu/admin/menus">Create new Menu</a>
                    </li>
                    <p>&nbsp;&nbsp;</p>
                    <li class="nav-item">
                        <a class="btn btn-info" href="/menu/admin/items">Create new Food Item</a>
                    </li>
                </ul>
            </div>
            <div class="card-body">
                <div class="card-title pl-3">
                    <h3>Current Menus</h3>
                </div>
                <hr>
                <div class="col-lg-12">
                    <table class="table" id="menus">  
                        <thead>
                        <tr>
                            {{-- <td><b>Menu For</b></td> --}}
                            <td><b>Location</b></td>
                            <td><b>Entered By</b></td>
                            <td><b>Display Location</b></td>
                            <td align="right"><b>View Items</b></td>
                            {{-- <td align="right"><b>Edit Items</b></td> --}}
                            {{-- <td align="right"><b>Delete</b></td> --}}
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($menu_daily as $menu)
                            <tr>
                                {{-- <td>{{$menu->start_date->toFormattedDateString()}}</td> --}}
                                <td>{{$menu->location}}</td>
                                <td>{{$menu->createdBy->name}}</td>
                                <td>{{$menu->Display}}</td>
                                <td align="right"><a href="/menu/admin/show/{{$menu->Display}}" class="btn btn-xs btn-primary" target="blank">View Menu</a></td>
                                {{-- <td align="right"><a href="/menu/admin/edit/{{$menu->id}}"  @if($menu->location == 'Main - All Spice') class="btn btn-xs btn-warning" @else class="btn btn-xs btn-warning" @endif>Edit Menu</a></td> --}}
                                {{-- <td align="right"><a href="#" class="btn btn-xs btn-danger deletebtn" menu_id = {{$menu->id}}>Delete Menu</a></td> --}}
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">

$(document).on("click", ".deletebtn", function () {
            //alert($(this).attr("data-cost_center"));
            var menu_id = $(this).attr("menu_id");
            DeleteMenuValues(menu_id);
        });

        function DeleteMenuValues(menu_id) {
            swal({
                title: "Delete Menu?",
                text: "Deleting this Menu cannot be undone.",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                confirmButtonText: "Yes, delete it!",
                confirmButtonColor: "#ec6c62"
            }, function() {
                $.ajax({
                    url: "/menu/admin/destroy/" + menu_id,
                    type: "GET"
                })
                    .done(function(data) {
                        swal({
                                title: "Deleted",
                                text: "Menu Deleted",
                                type: "success",
                                timer: 1800,
                                showConfirmButton: false

                            }
                        );
                        setTimeout(function(){window.location.replace('/menu/admin')},1900);
                    })
                    .error(function(data) {
                        swal("Oops", "We couldn't connect to the server!", "error");
                    });
            });
        }

</script>


<script type="application/javascript">
    $(document).ready(function() {
        $('#menus').DataTable( {
            "pageLength": 20,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>



@endsection
@endif