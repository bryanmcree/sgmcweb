
<div id="grillCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
               
        @if(Carbon\Carbon::now()->format('H:i:s') > '05:30:00' AND Carbon\Carbon::now()->format('H:i:s') < '10:30:00')
    
                {{-- @if(count($mainBreak) > 0)
                <div class="carousel-item p-4 active" id="slide-1">
    
                    <div class="row">
                        @foreach($mainBreak as $mainItem)
                            @if($counter < $half)
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4 text-left">
                                            <h3>{{$mainItem->title}}</h3>
                                        </div>
                                        <div class="col-lg-4 text-center">
                                            <h3>${{$mainItem->price}}</h3>
                                        </div>
                                        <div class="col-lg-4 text-right">
                                            <h3>cal. {{$mainItem->calories}}</h3>
                                        </div>
                                    </div>
                                    <br>
                                </div>
                            @else
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4 text-left">
                                            <h3>{{$mainItem->title}}</h3>
                                        </div>
                                        <div class="col-lg-4 text-center">
                                            <h3>${{$mainItem->price}}</h3>
                                        </div>
                                        <div class="col-lg-4 text-right">
                                            <h3>cal. {{$mainItem->calories}}</h3>
                                        </div>
                                    </div>
                                    <br>
                                </div>
                            @endif
                        @endforeach
                    
                    </div><!-- ROW -->
    
                </div> <!-- SLIDE 1 -->
                @endif
    
                @if(count($sideBreak) > 0 || count($extraBreak) > 0)
                <div class="carousel-item p-4" id="slide-2">
    
                    <div class="row">
    
                        <div class="col-lg-2 text-center mt-4">
                            
                            <button class="btn btn-circle-breakfast">Sides</button>
    
                            <br><br><br><br><br><br><br>
    
                            <button class="btn btn-circle-breakfast">Xtras</button>
    
                        </div>
    
                        <div class="col-lg-6"  style="line-height: 0.4em;">
                            
                             @foreach($sideBreak as $sideItem)
                                <div class="row">
                                    <div class="col-lg-4 text-left">
                                        <h4>{{$sideItem->title}}</h4>
                                    </div>
                                    <div class="col-lg-4 text-center">
                                        <h4>${{$sideItem->price}}</h4>
                                    </div>
                                    <div class="col-lg-4 text-right">
                                        <h4>cal. {{$sideItem->calories}}</h4>
                                    </div>
                                </div>
                                <br>
                            @endforeach
                            @foreach($extraBreak as $extraItem)
                                <div class="row">
                                    <div class="col-lg-4 text-left">
                                        <h4>{{$extraItem->title}}</h4>
                                    </div>
                                    <div class="col-lg-4 text-center">
                                        <h4>${{$extraItem->price}}</h4>
                                    </div>
                                    <div class="col-lg-4 text-right">
                                        <h4>cal. {{$extraItem->calories}}</h4>
                                    </div>
                                </div>
                                <br>
                            @endforeach
                        </div>
    
                        <div class="col-lg-4">
                            <img src="/img/Eggs.png" alt="img">
                        </div>
    
                    </div><!-- ROW -->
    
                </div> <!-- SLIDE 2 -->
                @endif --}}
    
                @if(count($mainBreak) > 0)
                    <div class="carousel-item active" id="slide-3">
    
                        <div class="row">
                            <div class="col-lg-8 pt-3">
                                @foreach($mainBreak as $allitem)
                                <div class="row">
                                        <div class="col-lg-1">
                                            
                                            </div>
                                        <div class="col-lg-6 text-left">
                                            <h3>{{strtolower($allitem->title)}}</h3>
                                        </div>
                                        <div class="col-lg-3 text-center">
                                            <h3>${{$allitem->price}}</h3>
                                        </div>
                                        <div class="col-lg-2 text-left pl-5">
                                            <h3>cal. {{$allitem->calories}}</h3>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="col-lg-4">
                                <img src="/img/Eggs.png" class="img-fluid" style="width: 100%; height: 720px;" alt="Img">
                            </div>
                        </div>
    
                    </div> <!-- SLIDE 3 -->
                @endif
    
                {{-- <div class="carousel-item" id="slide-4">
    
                    <div class="row">
                        <div class="col-lg-8 pt-5 text-center">
                            <br><br><br>
                           <h2>
                               single waffle
                               <br>
                               $2.49 | cal. 200
                            </h2>
                            <br><br>
                            <h2>
                                four mini waffles
                                <br>
                                $2.49 | cal. 200
                            </h2>
                            <br><br>
                            <h2>
                                waffle with toppings
                                <br>
                                $4.29 | cal. 200-400
                            </h2>
                        </div>
                        <div class="col-lg-4">
                            <img src="/img/waffles.png" class="img-fluid" style="width: 100%;" alt="Img">
                        </div>
                    </div>
    
                </div> <!-- SLIDE 4 -->
    
                <div class="carousel-item" id="slide-5">
    
                        <div class="row">
                        <div class="col-lg-8 pt-5 text-center">
                            <br><br><br>
                            <h2>
                                steel cut oatmeal
                                <br>
                                8 oz. | $1.39 | cal. 150
                            </h2>
                            <br><br>
                            <h2>
                                cream of wheat
                                <br>
                                8 oz. | $1.39 | cal. 200
                            </h2>
                            <br><br>
                            <h2>
                                stone ground grits
                                <br>
                                8 oz. | $1.39 | cal. 150
                            </h2>
                            <br><br>
                            <h2>    
                                honey orange quinoa
                                <br>
                                8 oz. | $1.39 | cal. 150
                            </h2>
                        </div>
                        <div class="col-lg-4 text-right">
                            <img src="/img/oats.png" class="img-fluid" style="width: 85%; height: 815px;" alt="Img">
                        </div>
                    </div>
    
                </div> <!-- SLIDE 5 --> --}}
            
    
    <!-- END BREAKFAST -->
    <!-- START GRILL -->
    
        @else
    
            @if(count($main) > 0 || count($side) > 0 || count($extra) > 0)
                <div class="carousel-item p-4 active" style="line-height: 0.3em;" id="slide-1">
                    <div class="row">
                        <div class="col-lg-6">
                            @foreach($main as $mainItem)
                                <div class="row">
                                    <div class="col-lg-1">
                                    
                                    </div>
                                    <div class="col-lg-5 text-left">
                                        <h3>{{strtolower($mainItem->title)}}</h3>
                                    </div>
                                    <div class="col-lg-3 text-center">
                                        <h3>${{$mainItem->price}}</h3>
                                    </div>
                                    <div class="col-lg-3 text-right">
                                        <h3>cal. {{$mainItem->calories}}</h3>
                                    </div>
                                </div>
                                <br>
                            @endforeach
                        </div>
    
                        <div class="col-lg-2 text-right mt-4">
                            <button class="btn btn-circle-grill" style="margin-top:20%;">Sides</button>
                            
                            <button class="btn btn-circle-grill" style="margin-top:40%;">Xtras</button>
                        </div>
    
                        <div class="col-lg-4">
                            @foreach($side as $sideItem)
                                <div class="row">
                                    <div class="col-lg-6 text-left">
                                        <h4>{{strtolower($sideItem->title)}}</h4>
                                    </div>
                                    <div class="col-lg-2 text-left">
                                        <h4>${{$sideItem->price}}</h4>
                                    </div>
                                    <div class="col-lg-3 text-right">
                                        <h4>cal. {{$sideItem->calories}}</h4>
                                    </div>
                                    <div class="col-lg-1">
                                            
                                    </div>
                                </div>
                                <br>
                            @endforeach
                            <hr>
                            @foreach($extra as $extraItem)
                                <div class="row">
                                    <div class="col-lg-6 text-left">
                                        <h4>{{strtolower($extraItem->title)}}</h4>
                                    </div>
                                    <div class="col-lg-2 text-left">
                                        <h4>${{$extraItem->price}}</h4>
                                    </div>
                                    <div class="col-lg-3 text-right">
                                        <h4>cal. {{$extraItem->calories}}</h4>
                                    </div>
                                    <div class="col-lg-1">
                                            
                                    </div>
                                </div>
                                <br>
                            @endforeach
                            <h4>
                            add fries and a 20 oz. fountain beverage | $2.59
                            </h4>
                            <h4>
                                add fries and a 20 oz. bottled beverage | $2.89
                            </h4>
                        </div>
                    </div> <!-- ROW -->
    
                </div> <!-- SLIDE 1 -->
            @endif
    
            {{-- @if(count($feature) > 0)
                <div class="carousel-item" id="slide-2">
    
                    <div class="pt-5">
                        @foreach($feature as $featured)
                            <h1 class="text-center mt-5"><i>{{$featured->title}}</i></h1>
                            <h4 class="text-center mt-3">{{$featured->ingredients}}</h4>
                            <h4 class="text-center mt-3">${{$featured->price}} | cal. {{$featured->calories}}</h4>
                            <br>
                            <hr>
                        @endforeach
                    </div>
    
                </div> <!-- SLIDE 2 -->
            @endif
    
<!-- END GRILL --> --}}

    
        @endif
    
    
    </div> <!-- CAROUSEL INNER -->
</div>