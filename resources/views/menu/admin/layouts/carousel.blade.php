
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
           
        @if($menu->Display == "Breakfast")

            @if(count($main) > 0)
            <div class="carousel-item p-4 active" id="slide-1">

                <div class="row">
                    @foreach($main as $mainItem)
                        @if($counter < $half)
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4 text-left">
                                        <h3>{{$mainItem->title}}</h3>
                                    </div>
                                    <div class="col-lg-4 text-center">
                                        <h3>${{$mainItem->price}}</h3>
                                    </div>
                                    <div class="col-lg-4 text-right">
                                        <h3>cal. {{$mainItem->calories}}</h3>
                                    </div>
                                </div>
                                <br>
                            </div>
                        @else
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4 text-left">
                                        <h3>{{$mainItem->title}}</h3>
                                    </div>
                                    <div class="col-lg-4 text-center">
                                        <h3>${{$mainItem->price}}</h3>
                                    </div>
                                    <div class="col-lg-4 text-right">
                                        <h3>cal. {{$mainItem->calories}}</h3>
                                    </div>
                                </div>
                                <br>
                            </div>
                        @endif
                    @endforeach
                
                </div><!-- ROW -->

            </div> <!-- SLIDE 1 -->
            @endif

            @if(count($side) > 0 || count($extra) > 0)
            <div class="carousel-item p-4" id="slide-2">

                <div class="row">

                    <div class="col-lg-2 text-center mt-4">
                        
                        <button class="btn btn-circle-breakfast">Sides</button>

                        <br><br><br><br><br><br><br>

                        <button class="btn btn-circle-breakfast">Xtras</button>

                    </div>

                    <div class="col-lg-6"  style="line-height: 0.4em;">
                        
                         @foreach($side as $sideItem)
                            <div class="row">
                                <div class="col-lg-4 text-left">
                                    <h4>{{$sideItem->title}}</h4>
                                </div>
                                <div class="col-lg-4 text-center">
                                    <h4>${{$sideItem->price}}</h4>
                                </div>
                                <div class="col-lg-4 text-right">
                                    <h4>cal. {{$sideItem->calories}}</h4>
                                </div>
                            </div>
                            <br>
                        @endforeach
                        @foreach($extra as $extraItem)
                            <div class="row">
                                <div class="col-lg-4 text-left">
                                    <h4>{{$extraItem->title}}</h4>
                                </div>
                                <div class="col-lg-4 text-center">
                                    <h4>${{$extraItem->price}}</h4>
                                </div>
                                <div class="col-lg-4 text-right">
                                    <h4>cal. {{$extraItem->calories}}</h4>
                                </div>
                            </div>
                            <br>
                        @endforeach
                    </div>

                    <div class="col-lg-4">
                        <img src="/img/Eggs.png" alt="img">
                    </div>

                </div><!-- ROW -->

            </div> <!-- SLIDE 2 -->
            @endif

            @if(count($feature) > 0)
                <div class="carousel-item" id="slide-3">

                    <div class="row">
                        <div class="col-lg-8 pt-5">
                            @foreach($feature as $featured)
                                <h1 class="text-center mt-5"><i>{{$featured->title}}</i></h1>
                                <h4 class="text-center mt-3">{{$featured->ingredients}}</h4>
                                <h4 class="text-center mt-3">${{$featured->price}} | cal. {{$featured->calories}}</h4>
                                <hr>
                            @endforeach
                        </div>
                        <div class="col-lg-4">
                            <img src="/img/Muffin_Banana.png" class="img-fluid" style="width: 100%; height: 815px;" alt="Img">
                        </div>
                    </div>

                </div> <!-- SLIDE 3 -->
            @endif

            <div class="carousel-item" id="slide-4">

                <div class="row">
                    <div class="col-lg-8 pt-5 text-center">
                        <br><br><br>
                       <h2>
                           single waffle
                           <br>
                           $2.49 | cal. 200
                        </h2>
                        <br><br>
                        <h2>
                            four mini waffles
                            <br>
                            $2.49 | cal. 200
                        </h2>
                        <br><br>
                        <h2>
                            waffle with toppings
                            <br>
                            $4.29 | cal. 200-400
                        </h2>
                    </div>
                    <div class="col-lg-4">
                        <img src="/img/waffles.png" class="img-fluid" style="width: 100%;" alt="Img">
                    </div>
                </div>

            </div> <!-- SLIDE 4 -->

            <div class="carousel-item" id="slide-5">

                    <div class="row">
                    <div class="col-lg-8 pt-5 text-center">
                        <br><br><br>
                        <h2>
                            steel cut oatmeal
                            <br>
                            8 oz. | $1.39 | cal. 150
                        </h2>
                        <br><br>
                        <h2>
                            cream of wheat
                            <br>
                            8 oz. | $1.39 | cal. 200
                        </h2>
                        <br><br>
                        <h2>
                            stone ground grits
                            <br>
                            8 oz. | $1.39 | cal. 150
                        </h2>
                        <br><br>
                        <h2>    
                            honey orange quinoa
                            <br>
                            8 oz. | $1.39 | cal. 150
                        </h2>
                    </div>
                    <div class="col-lg-4 text-right">
                        <img src="/img/oats.png" class="img-fluid" style="width: 85%; height: 815px;" alt="Img">
                    </div>
                </div>

            </div> <!-- SLIDE 5 -->
        

<!-- END BREAKFAST -->
<!-- START SALAD -->

        @elseif($menu->Display == "Salad")

        @if(count($main) > 0 || count($side) > 0 || count($extra) > 0)
            <div class="carousel-item p-4" id="slide-1">
                <div class="row">
                    <div class="col-lg-4">
                        <h1 class="text-center text-white bg-success mb-5 myBorder">Main Dish</h1>
                        @foreach($main as $mainItem)
                            <div class="row">
                                <div class="col-lg-4 text-left">
                                    <h5>{{$mainItem->title}}</h5>
                                </div>
                                <div class="col-lg-4 text-center">
                                    <h5>${{$mainItem->price}}</h5>
                                </div>
                                <div class="col-lg-4 text-right">
                                    <h5>cal. {{$mainItem->calories}}</h5>
                                </div>
                            </div>
                            <br>
                        @endforeach
                    </div>
                    <div class="col-lg-4">
                        <h1 class="text-center text-white bg-success mb-5 myBorder">Side Items</h1>
                        @foreach($side as $sideItem)
                            <div class="row">
                                <div class="col-lg-4 text-left">
                                    <h5>{{$sideItem->title}}</h5>
                                </div>
                                <div class="col-lg-4 text-center">
                                    <h5>${{$sideItem->price}}</h5>
                                </div>
                                <div class="col-lg-4 text-right">
                                    <h5>cal. {{$sideItem->calories}}</h5>
                                </div>
                            </div>
                            <br>
                        @endforeach
                    </div>
                    <div class="col-lg-4">
                        <h1 class="text-center text-white bg-success mb-5 myBorder">Extra Items</h1>
                        @foreach($extra as $extraItem)
                            <div class="row">
                                <div class="col-lg-4 text-left">
                                    <h5>{{$extraItem->title}}</h5>
                                </div>
                                <div class="col-lg-4 text-center">
                                    <h5>${{$extraItem->price}}</h5>
                                </div>
                                <div class="col-lg-4 text-right">
                                    <h5>cal. {{$extraItem->calories}}</h5>
                                </div>
                            </div>
                            <br>
                        @endforeach
                    </div>

                </div> <!-- ROW -->

            </div> <!-- SLIDE 1 -->
        @endif

        <div class="carousel-item active" id="slide-2">
            <div class="row">
                <div class="col-lg-8 pt-5">
                    <h1 style="font-size: 5em;" class="text-center">build your own salad</h1>
                    <h2 style="font-size: 3em;" class="text-center mb-5">$6.24/lb</h2>
                    <div style="padding-left: 25%;">
                        <p>
                            <button class="btn btn-circle-salad"><span class="salad-text-size">1</span></button>
                            <span class="salad-text-size pl-3">lead with leafy greens</span>
                        </p>
                        <p>
                            <button class="btn btn-circle-salad"><span class="salad-text-size">2</span></button>
                            <span class="salad-text-size pl-3">add colorful vegetables</span>
                        </p>
                        <p>
                            <button class="btn btn-circle-salad"><span class="salad-text-size">3</span></button>
                            <span class="salad-text-size pl-3">pick up some lean protein</span>
                        </p>
                        <p>
                            <button class="btn btn-circle-salad"><span class="salad-text-size">4</span></button>
                            <span class="salad-text-size pl-3">drizzle with dressing</span>
                        </p>
                        <!-- Create an icon wrapped by the fa-stack class -->
    
                    </div>
                </div>
                <div class="col-lg-4">
                    <img src="/img/Salad_Sidebar_Menu.png" class="img-fluid" style="width: 100%;" alt="Img">
                </div>
            </div>
        </div> <!-- SLIDE 2 -->

        @if(count($feature) > 0)
            <div class="carousel-item" id="slide-3">

                <div class="pt-5">
                    @foreach($feature as $featured)
                        <h1 class="text-center mt-5"><i>{{$featured->title}}</i></h1>
                        <h4 class="text-center mt-3">{{$featured->ingredients}}</h4>
                        <h4 class="text-center mt-3">${{$featured->price}} | cal. {{$featured->calories}}</h4>
                        <br>
                        <hr>
                    @endforeach
                </div>

            </div> <!-- SLIDE 3 -->
            @endif

<!-- END SALAD -->
<!-- START GRILL -->

        @elseif($menu->Display == "Grill")

        @if(count($main) > 0 || count($side) > 0 || count($extra) > 0)
            <div class="carousel-item p-4 active" style="line-height: 0.3em;" id="slide-1">
                <div class="row">
                    <div class="col-lg-6">
                        @foreach($main as $mainItem)
                            <div class="row">
                                <div class="col-lg-4 text-left">
                                    <h3>{{$mainItem->title}}</h3>
                                </div>
                                <div class="col-lg-4 text-center">
                                    <h3>${{$mainItem->price}}</h3>
                                </div>
                                <div class="col-lg-4 text-right">
                                    <h3>cal. {{$mainItem->calories}}</h3>
                                </div>
                            </div>
                            <br>
                        @endforeach
                    </div>

                    <div class="col-lg-2 text-right mt-4">
                        <button class="btn btn-circle-grill" style="margin-top:25%;">Sides</button>
                        
                        <button class="btn btn-circle-grill" style="margin-top:60%;">Xtras</button>
                    </div>

                    <div class="col-lg-4">
                        @foreach($side as $sideItem)
                            <div class="row">
                                <div class="col-lg-4 text-left">
                                    <h4>{{$sideItem->title}}</h4>
                                </div>
                                <div class="col-lg-4 text-center">
                                    <h4>${{$sideItem->price}}</h4>
                                </div>
                                <div class="col-lg-4 text-right">
                                    <h4>cal. {{$sideItem->calories}}</h4>
                                </div>
                            </div>
                            <br>
                        @endforeach
                        <hr>
                        @foreach($extra as $extraItem)
                            <div class="row">
                                <div class="col-lg-4 text-left">
                                    <h4>{{$extraItem->title}}</h4>
                                </div>
                                <div class="col-lg-4 text-center">
                                    <h4>${{$extraItem->price}}</h4>
                                </div>
                                <div class="col-lg-4 text-right">
                                    <h4>cal. {{$extraItem->calories}}</h4>
                                </div>
                            </div>
                            <br>
                        @endforeach
                    </div>
                </div> <!-- ROW -->

            </div> <!-- SLIDE 1 -->
        @endif

        @if(count($feature) > 0)
            <div class="carousel-item" id="slide-2">

                <div class="pt-5">
                    @foreach($feature as $featured)
                        <h1 class="text-center mt-5"><i>{{$featured->title}}</i></h1>
                        <h4 class="text-center mt-3">{{$featured->ingredients}}</h4>
                        <h4 class="text-center mt-3">${{$featured->price}} | cal. {{$featured->calories}}</h4>
                        <br>
                        <hr>
                    @endforeach
                </div>

            </div> <!-- SLIDE 2 -->
        @endif

<!-- END GRILL -->
<!-- START DELI -->

        @elseif($menu->Display == "Deli")

            <div class="carousel-item active" id="slide-1">
                <div class="row"  style="line-height: 0.5em;">
                    <div class="col-lg-4 pt-3 text-center">
                        <button class="btn btn-circle-deli">Step 1</button>
                        <span class="pl-4" style="font-size: 2em;"><b>choose your bread</b></span>

                        <div class="row pt-3">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-6 text-left">
                                <h3>nine grain wheat</h3>
                                <h3>honey oat</h3>
                                <h3>italian herbs & cheese</h3>
                                <h3>kaiser roll</h3>
                            </div>
                            <div class="col-lg-4 text-right">
                                <h3>cal. 220</h3>
                                <h3>cal. 278</h3>
                                <h3>cal. 230</h3>
                                <h3>cal. 320</h3>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 pt-3 text-center">
                        <button class="btn btn-circle-deli">Step 2</button>
                        <span class="pl-4" style="font-size: 2em;"><b>choose your protein</b></span>
                        <div class="row pt-3">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-6 text-left">
                                <h3>smoked turkey</h3>
                                <h3>honey ham</h3>
                                <h3>roasted chicken</h3>
                                <h3>pastrami</h3>
                            </div>
                            <div class="col-lg-4 text-right">
                                <h3>cal. 220</h3>
                                <h3>cal. 123</h3>
                                <h3>cal. 230</h3>
                                <h3>cal. 320</h3>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 pt-3 text-center">
                        <button class="btn btn-circle-deli">Step 3</button>
                        <span class="pl-4" style="font-size: 2em;"><b>choose your cheese</b></span>
                        <div class="row pt-3">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-6 text-left">
                                <h3>american</h3>
                                <h3>perpper jack</h3>
                                <h3>swiss</h3>
                                <h3>provolone</h3>
                            </div>
                            <div class="col-lg-4 text-left">
                                <h3>cal. 220</h3>
                                <h3>cal. 230</h3>
                                <h3>cal. 230</h3>
                                <h3>cal. 320</h3>
                            </div>
                        </div>
                    </div>

                </div> <!-- ROW 1 -->
                <div class="row">

                    <div class="col-lg-6 pt-3 text-center">
                        <button class="btn btn-circle-deli">Step 4</button>
                        <span class="pl-4" style="font-size: 2em;"><b>choose your condiments</b></span>
                        <div class="row pt-3">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-6 text-left">
                                <h3>mayonnaise</h3>
                                <h3>specialty spread</h3>
                                <h3>oil & vinegar</h3>
                                <h3>deli mustard</h3>
                            </div>
                            <div class="col-lg-4 text-center">
                                <h3>cal. 220</h3>
                                <h3>cal. 110</h3>
                                <h3>cal. 230</h3>
                                <h3>cal. 320</h3>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 pt-3 text-center">
                        <button class="btn btn-circle-deli">Step 5</button>
                        <span class="pl-4" style="font-size: 2em;"><b>choose your toppings</b></span>
                        <div class="row pt-3">
                            <div class="col-lg-1"></div>
                            <div class="col-lg-3 text-left">
                                <h3>lettuce</h3>
                                <h3>tomato</h3>
                                <h3>red onion</h3>
                                <h3>pickles</h3>
                            </div>
                            <div class="col-lg-2 text-left">
                                <h3>cal. 220</h3>
                                <h3>cal. 110</h3>
                                <h3>cal. 230</h3>
                                <h3>cal. 320</h3>
                            </div>
                            <div class="col-lg-3 text-left">
                                <h3>lettuce</h3>
                                <h3>tomato</h3>
                                <h3>red onion</h3>
                                <h3>pickles</h3>
                            </div>
                            <div class="col-lg-2 text-left">
                                <h3>cal. 220</h3>
                                <h3>cal. 110</h3>
                                <h3>cal. 230</h3>
                                <h3>cal. 320</h3>
                            </div>
                            <div class="col-lg-1"></div>
                        </div>
                    </div>


                </div> <!-- ROW 2 -->

            </div> <!-- SLIDE 1 -->


        
            <div class="carousel-item" id="slide-2">

                <div class="row">
                    <div class="col-lg-6 pt-5 text-center">
                        <h1 style="font-size: 4em;"><b>boars, head wraps, and sandwhiches</b></h1>
                        <br>
                        <div style="font-size: 2.5em;">
                            <p>
                                choice of meat, cheese, dressing, and vegetables
                                <br>
                                421 cal | $4.99 | roast beef $5.99
                            </p>
                            <p>
                                chicken salad or tuna salad by the bowl
                                <br>
                                287 cal | $3.19
                            </p>
                            <p>
                                chicken salad or tuna salad sandwhich
                                <br>
                                417 cal | $4.19
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6 pt-5 text-center">
                        <h1 style="font-size: 4em;"><b>add on's</b></h1>
                        <br>
                        <div style="font-size: 2.5em;">
                            <p>
                                bacon | 112 cal | $0.89
                                <br>
                                guacamole | 139 cal | $0.89
                                <br>
                                double meat | 112 cal | $2.79
                            </p>
                            <p>
                                add fries and a 20 oz. fountain beverage | $2.59
                            </p>
                            <p>
                                add fries and a 20 oz. bottled beverage | $2.89
                            </p>
                        </div>
                    </div>

                </div> <!-- ROW -->

            </div> <!-- SLIDE 2 -->

            @if(count($side) > 0 || count($extra) > 0)
            <div class="carousel-item" id="slide-3">
                <div class="row">

                    <div class="col-lg-2 text-right mt-4" style="font-size:5em;">
                        <span class="fa-stack">
                            <span class="fas fa-circle fa-stack-2x"></span>
                            <strong class="fa-stack-1x text-white" style="font-size:60%;">
                                Sides
                            </strong>
                        </span>
                    </div>

                    <div class="col-lg-4 pt-5">
                        {{-- <h1 class="text-center mb-5 myBorder" style="background-color: #ffd2a3;">Extra Items</h1> --}}
                        @foreach($side as $sideItem)
                            <div class="row">
                                <div class="col-lg-4 text-left">
                                    <h3>{{$sideItem->title}}</h3>
                                </div>
                                <div class="col-lg-4 text-center">
                                    <h3>${{$sideItem->price}}</h3>
                                </div>
                                <div class="col-lg-4 text-left">
                                    <h3>cal. {{$sideItem->calories}}</h3>
                                </div>
                            </div>
                            <br>
                        @endforeach
                    </div>

                    <div class="col-lg-2 text-right mt-4" style="font-size:5em;">
                        <span class="fa-stack">
                            <span class="fas fa-circle fa-stack-2x"></span>
                            <strong class="fa-stack-1x text-white" style="font-size:60%;">
                                Xtras
                            </strong>
                        </span>
                    </div>

                    <div class="col-lg-4 pt-5">
                        {{-- <h1 class="text-center mb-5 myBorder" style="background-color: #ffd2a3;">Extra Items</h1> --}}
                        @foreach($extra as $extraItem)
                            <div class="row">
                                <div class="col-lg-4 text-left">
                                    <h3>{{$extraItem->title}}</h3>
                                </div>
                                <div class="col-lg-4 text-center">
                                    <h3>${{$extraItem->price}}</h3>
                                </div>
                                <div class="col-lg-4 text-left">
                                    <h3>cal. {{$extraItem->calories}}</h3>
                                </div>
                            </div>
                            <br>
                        @endforeach
                    </div>

                </div> <!-- ROW -->
            </div> <!-- SLIDE 3 -->
            @endif

            @if(count($main) > 0)
            <div class="carousel-item p-4" id="slide-4">

                <div class="row">
                    @foreach($main as $mainItem)
                        <div class="col-lg-2"></div>
                        <div class="col-lg-8">
                            <div class="row">
                                <div class="col-lg-4 text-left">
                                    <h3>{{$mainItem->title}}</h3>
                                </div>
                                <div class="col-lg-4 text-center">
                                    <h3>${{$mainItem->price}}</h3>
                                </div>
                                <div class="col-lg-4 text-right">
                                    <h3>cal. {{$mainItem->calories}}</h3>
                                </div>
                            </div>
                            <br>
                        </div>
                        <div class="col-lg-2"></div>
                    @endforeach
                
                </div><!-- ROW -->

            </div> <!-- SLIDE 4 -->
            @endif

            @if(count($feature) > 0)
            <div class="carousel-item" id="slide-5">

                <div class="pt-5">
                    @foreach($feature as $featured)
                        <h1 class="text-center mt-5"><i>{{$featured->title}}</i></h1>
                        <h4 class="text-center mt-3">{{$featured->ingredients}}</h4>
                        <h4 class="text-center mt-3">${{$featured->price}} | cal. {{$featured->calories}}</h4>
                        <br>
                        <hr>
                    @endforeach
                </div>

            </div> <!-- SLIDE 5 -->
            @endif

        @endif


    </div> <!-- CAROUSEL INNER -->
</div>