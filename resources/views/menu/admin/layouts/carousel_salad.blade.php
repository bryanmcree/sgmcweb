
<div id="saladCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
                
        @if(Carbon\Carbon::now()->format('H:i:s') > '05:30:00' AND Carbon\Carbon::now()->format('H:i:s') < '10:30:00')
    
                {{-- @if(count($mainBreak) > 0)
                <div class="carousel-item p-4 active" id="slide-1">
    
                    <div class="row">
                        @foreach($mainBreak as $mainItem)
                            @if($counter < $half)
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4 text-left">
                                            <h3>{{$mainItem->title}}</h3>
                                        </div>
                                        <div class="col-lg-4 text-center">
                                            <h3>${{$mainItem->price}}</h3>
                                        </div>
                                        <div class="col-lg-4 text-right">
                                            <h3>cal. {{$mainItem->calories}}</h3>
                                        </div>
                                    </div>
                                    <br>
                                </div>
                            @else
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4 text-left">
                                            <h3>{{$mainItem->title}}</h3>
                                        </div>
                                        <div class="col-lg-4 text-center">
                                            <h3>${{$mainItem->price}}</h3>
                                        </div>
                                        <div class="col-lg-4 text-right">
                                            <h3>cal. {{$mainItem->calories}}</h3>
                                        </div>
                                    </div>
                                    <br>
                                </div>
                            @endif
                        @endforeach
                    
                    </div><!-- ROW -->
    
                </div> <!-- SLIDE 1 -->
                @endif
    
                @if(count($sideBreak) > 0 || count($extraBreak) > 0)
                <div class="carousel-item p-4" id="slide-2">
    
                    <div class="row">
    
                        <div class="col-lg-2 text-center mt-4">
                            
                            <button class="btn btn-circle-breakfast">Sides</button>
    
                            <br><br><br><br><br><br><br>
    
                            <button class="btn btn-circle-breakfast">Xtras</button>
    
                        </div>
    
                        <div class="col-lg-6"  style="line-height: 0.4em;">
                            
                                @foreach($sideBreak as $sideItem)
                                <div class="row">
                                    <div class="col-lg-4 text-left">
                                        <h4>{{$sideItem->title}}</h4>
                                    </div>
                                    <div class="col-lg-4 text-center">
                                        <h4>${{$sideItem->price}}</h4>
                                    </div>
                                    <div class="col-lg-4 text-right">
                                        <h4>cal. {{$sideItem->calories}}</h4>
                                    </div>
                                </div>
                                <br>
                            @endforeach
                            @foreach($extraBreak as $extraItem)
                                <div class="row">
                                    <div class="col-lg-4 text-left">
                                        <h4>{{$extraItem->title}}</h4>
                                    </div>
                                    <div class="col-lg-4 text-center">
                                        <h4>${{$extraItem->price}}</h4>
                                    </div>
                                    <div class="col-lg-4 text-right">
                                        <h4>cal. {{$extraItem->calories}}</h4>
                                    </div>
                                </div>
                                <br>
                            @endforeach
                        </div>
    
                        <div class="col-lg-4">
                            <img src="/img/Eggs.png" alt="img">
                        </div>
    
                    </div><!-- ROW -->
    
                </div> <!-- SLIDE 2 -->
                @endif
    
                @if(count($featureBreak) > 0)
                    <div class="carousel-item" id="slide-3">
    
                        <div class="row">
                            <div class="col-lg-8 pt-5">
                                @foreach($featureBreak as $featured)
                                    <h1 class="text-center mt-5"><i>{{$featured->title}}</i></h1>
                                    <h4 class="text-center mt-3">{{$featured->ingredients}}</h4>
                                    <h4 class="text-center mt-3">${{$featured->price}} | cal. {{$featured->calories}}</h4>
                                    <hr>
                                @endforeach
                            </div>
                            <div class="col-lg-4">
                                <img src="/img/Muffin_Banana.png" class="img-fluid" style="width: 100%; height: 815px;" alt="Img">
                            </div>
                        </div>
    
                    </div> <!-- SLIDE 3 -->
                @endif
    
                <div class="carousel-item" id="slide-4">
    
                    <div class="row">
                        <div class="col-lg-8 pt-5 text-center">
                            <br><br><br>
                            <h2>
                                single waffle
                                <br>
                                $2.49 | cal. 200
                            </h2>
                            <br><br>
                            <h2>
                                four mini waffles
                                <br>
                                $2.49 | cal. 200
                            </h2>
                            <br><br>
                            <h2>
                                waffle with toppings
                                <br>
                                $4.29 | cal. 200-400
                            </h2>
                        </div>
                        <div class="col-lg-4">
                            <img src="/img/waffles.png" class="img-fluid" style="width: 100%;" alt="Img">
                        </div>
                    </div>
    
                </div> <!-- SLIDE 4 --> --}}
    
                <div class="carousel-item active" id="slide-5">
    
                        <div class="row">
                        <div class="col-lg-8 pt-5 text-center">
                            <br><br><br>
                            <h1>
                                oatmeal
                                <br>
                                8 oz. | $1.39 | cal. 150
                            </h1>
                            <br><br>
                            <h1>
                                bagel
                                <br>
                                1 ea. | $1.39 | cal. 300
                            </h1>
                            <br><br>
                            <h1>
                                yogurt bar
                                <br>
                                $ .39 per oz.
                            </h1>
                        </div>
                        <div class="col-lg-4 text-right">
                            <img src="/img/oats.png" class="img-fluid" style="width: 85%; height: 710px;" alt="Img">
                        </div>
                    </div>
    
                </div> <!-- SLIDE 5 -->
            
    
    <!-- END BREAKFAST -->
    <!-- START SALAD -->

    @else

    {{-- @if(count($main) > 0 || count($side) > 0 || count($extra) > 0)
        <div class="carousel-item p-4" id="slide-1">
            <div class="row">
                <div class="col-lg-4">
                    <h1 class="text-center text-white bg-success mb-5 myBorder">Main Dish</h1>
                    @foreach($main as $mainItem)
                        <div class="row">
                            <div class="col-lg-4 text-left">
                                <h5>{{$mainItem->title}}</h5>
                            </div>
                            <div class="col-lg-4 text-center">
                                <h5>${{$mainItem->price}}</h5>
                            </div>
                            <div class="col-lg-4 text-right">
                                <h5>cal. {{$mainItem->calories}}</h5>
                            </div>
                        </div>
                        <br>
                    @endforeach
                </div>
                <div class="col-lg-4">
                    <h1 class="text-center text-white bg-success mb-5 myBorder">Side Items</h1>
                    @foreach($side as $sideItem)
                        <div class="row">
                            <div class="col-lg-4 text-left">
                                <h5>{{$sideItem->title}}</h5>
                            </div>
                            <div class="col-lg-4 text-center">
                                <h5>${{$sideItem->price}}</h5>
                            </div>
                            <div class="col-lg-4 text-right">
                                <h5>cal. {{$sideItem->calories}}</h5>
                            </div>
                        </div>
                        <br>
                    @endforeach
                </div>
                <div class="col-lg-4">
                    <h1 class="text-center text-white bg-success mb-5 myBorder">Extra Items</h1>
                    @foreach($extra as $extraItem)
                        <div class="row">
                            <div class="col-lg-4 text-left">
                                <h5>{{$extraItem->title}}</h5>
                            </div>
                            <div class="col-lg-4 text-center">
                                <h5>${{$extraItem->price}}</h5>
                            </div>
                            <div class="col-lg-4 text-right">
                                <h5>cal. {{$extraItem->calories}}</h5>
                            </div>
                        </div>
                        <br>
                    @endforeach
                </div>

            </div> <!-- ROW -->

        </div> <!-- SLIDE 1 -->
    @endif --}}

    <div class="carousel-item active" id="slide-2">
        <div class="row">
            <div class="col-lg-8 pt-3">
                <h1 style="font-size: 5em;" class="text-center">build your own salad</h1>
                <h2 style="font-size: 3em;" class="text-center mb-5">$6.24/lb</h2>
                <div style="padding-left: 25%;">
                    <p>
                        <button class="btn btn-circle-salad"><span class="salad-text-size">1</span></button>
                        <span class="salad-text-size pl-3">lead with leafy greens</span>
                    </p>
                    <p>
                        <button class="btn btn-circle-salad"><span class="salad-text-size">2</span></button>
                        <span class="salad-text-size pl-3">add colorful vegetables</span>
                    </p>
                    <p>
                        <button class="btn btn-circle-salad"><span class="salad-text-size">3</span></button>
                        <span class="salad-text-size pl-3">pick up some lean protein</span>
                    </p>
                    <p>
                        <button class="btn btn-circle-salad"><span class="salad-text-size">4</span></button>
                        <span class="salad-text-size pl-3">drizzle with dressing</span>
                    </p>
                    <!-- Create an icon wrapped by the fa-stack class -->

                </div>
            </div>
            <div class="col-lg-4">
                <img src="/img/Salad_Sidebar_Menu.png" class="img-fluid" style="width: 100%;" alt="Img">
            </div>
        </div>
    </div> <!-- SLIDE 2 -->

    {{-- @if(count($feature) > 0)
        <div class="carousel-item" id="slide-3">

            <div class="pt-5">
                @foreach($feature as $featured)
                    <h1 class="text-center mt-5"><i>{{$featured->title}}</i></h1>
                    <h4 class="text-center mt-3">{{$featured->ingredients}}</h4>
                    <h4 class="text-center mt-3">${{$featured->price}} | cal. {{$featured->calories}}</h4>
                    <br>
                    <hr>
                @endforeach
            </div>

        </div> <!-- SLIDE 3 -->
        @endif --}}

<!-- END SALAD -->

    
        @endif
    
    
    </div> <!-- CAROUSEL INNER -->
</div>