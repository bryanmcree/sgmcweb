{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('ExecSummary') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.nonav')
    {{--Page Design Goes Below--}}
@section('content')
    <meta http-equiv="refresh" content="36000">

    @if(is_null($yesterday_summary))

        <div class="alert alert-info"><h3><b>Looks like yesterdays data is not available yet.</b></h3></div>
    @else
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#"><b>{{$yesterday_summary->report_date->format('l jS \\of F, Y')}}</b></a>
                </div>
                <ul class="nav navbar-nav">
                    <li><a href="#" onClick="javascript:goToSlide(0);">Revenue</a></li>
                    <li><a href="#" onClick="javascript:goToSlide(1);">Discharges</a></li>
                    <li><a href="#" onClick="javascript:goToSlide(2);">Emergency Room</a></li>
                    <li><a href="#" onClick="javascript:goToSlide(3);">Surgery</a></li>
                    <li><a href="#" onClick="javascript:goToSlide(4);">Radiology</a></li>
                    <li><a href="#" onClick="javascript:goToSlide(5);">Spreadsheet</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#" id="pauseButton"><i class="fa fa-pause" aria-hidden="true"></i> Pause</a></li>
                    <li><a href="#" id="playButton"><i class="fa fa-play" aria-hidden="true"></i> Play</a></li>
                    <li><a href="#" onClick="javascript:goToSlide(6);">About</a></li>
                </ul>
            </div>
        </nav>

        <div class="col-lg-12">
            <div id="message"></div>
        </div>


        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">
    <div class="col-md-12">
        <div class="col-lg-2">
            <div class="well">
                <div style="text-align: center;">
                    <div style="font-size: 18px; font-weight: bold;">Hospital Revenue</div>
                    <div style="vertical-align: middle; font-size: 30px; color: black; ">${{number_format($yesterday_summary->h_r)}}</div>
                    @if((($yesterday_summary->h_r-$day_before_summary->h_r)/$yesterday_summary->h_r)*100 < 0)
                        <div style="vertical-align: middle; font-size: 15px; color: red; "><i class="fas fa-arrow-down"></i>{{number_format(((($yesterday_summary->h_r-$day_before_summary->h_r)/$yesterday_summary->h_r)*100)/-1,2)}}%</div>
                    @elseif((($yesterday_summary->h_r-$day_before_summary->h_r)/$yesterday_summary->h_r)*100 == 0)
                        <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                    @else
                        <div style="vertical-align: middle; font-size: 15px; color: green; "><i class="fas fa-arrow-up"></i>{{number_format((($yesterday_summary->h_r-$day_before_summary->h_r)/$yesterday_summary->h_r)*100,2)}}%</div>
                    @endif
                </div>
                <canvas id="monthly_hr"></canvas>
                <div style="text-align: center">
                    <div style="font-size: 18px; font-weight: bold;">Month to Date</div>
                    <div style="display:inline-block; font-size: 30px; color: black; ">${{number_format($month_summary->sum('h_r'))}}</div>
                </div>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="well">
                <div style="text-align: center;">
                    <div style="font-size: 18px; font-weight: bold;">Professional Billing</div>
                    <div style="vertical-align: middle; font-size: 30px; color: black; ">${{number_format($yesterday_summary->p_b_r)}}</div>
                    @if((($yesterday_summary->p_b_r-$day_before_summary->p_b_r)/$yesterday_summary->p_b_r)*100 < 0)
                        <div style="vertical-align: middle; font-size: 15px; color: red; "><i class="fas fa-arrow-down"></i>{{number_format(((($yesterday_summary->p_b_r-$day_before_summary->p_b_r)/$yesterday_summary->p_b_r)*100)/-1,2)}}%</div>
                    @elseif((($yesterday_summary->p_b_r-$day_before_summary->p_b_r)/$yesterday_summary->p_b_r)*100 == 0)
                        <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                    @else
                        <div style="vertical-align: middle; font-size: 15px; color: green; "><i class="fas fa-arrow-up"></i>{{number_format((($yesterday_summary->p_b_r-$day_before_summary->p_b_r)/$yesterday_summary->p_b_r)*100,2)}}%</div>
                    @endif
                </div>
                <canvas id="monthly_pb"></canvas>
                <div style="text-align: center">
                    <div style="font-size: 18px; font-weight: bold;">Month to Date</div>
                    <div style="display:inline-block; font-size: 30px; color: black; ">${{number_format($month_summary->sum('p_b_r'))}}</div>
                </div>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="well">
                <div style="text-align: center;">
                    <div style="font-size: 18px; font-weight: bold;">Total Patient Revenue</div>
                    <div style="vertical-align: middle; font-size: 30px; color: black; ">${{number_format($yesterday_summary->total_p_r)}}</div>
                    @if(((($yesterday_summary->total_p_r)-($day_before_summary->total_p_r))/($yesterday_summary->total_p_r))*100 < 0)
                        <div style="vertical-align: middle; font-size: 15px; color: red; "><i class="fas fa-arrow-down"></i>{{number_format((((($yesterday_summary->total_p_r)-($day_before_summary->p_b_r+$day_before_summary->h_r))/($yesterday_summary->total_p_r))*100)/-1,2)}}%</div>
                    @elseif((($yesterday_summary->total_p_r-$day_before_summary->total_p_r)/$yesterday_summary->total_p_r)*100 == 0)
                        <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                    @else
                        <div style="vertical-align: middle; font-size: 15px; color: green; "><i class="fas fa-arrow-up"></i>{{number_format(((($yesterday_summary->total_p_r)-($day_before_summary->total_p_r))/($yesterday_summary->total_p_r))*100,2)}}%</div>
                    @endif
                </div>
                <canvas id="total_rev" height="31"></canvas>
                <div style="text-align: center">
                    <div style="font-size: 18px; font-weight: bold;">Month to Date</div>
                    <div style="display:inline-block; font-size: 30px; color: black; ">${{number_format($month_summary->sum('p_b_r')+$month_summary->sum('h_r'))}}</div>
                </div>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="well">
                <div style="text-align: center;">
                    <div style="font-size: 18px; font-weight: bold;">ADLT/PED Days</div>
                    <div style="vertical-align: middle; font-size: 30px; color: black; ">{{number_format($yesterday_summary->a_p_days)}}</div>
                    @if((($yesterday_summary->a_p_days-$day_before_summary->a_p_days)/$yesterday_summary->a_p_days)*100 < 0)
                        <div style="vertical-align: middle; font-size: 15px; color: red; "><i class="fas fa-arrow-down"></i>{{number_format(((($yesterday_summary->a_p_days-$day_before_summary->a_p_days)/$yesterday_summary->a_p_days)*100)/-1,2)}}%</div>
                    @elseif((($yesterday_summary->a_p_days-$day_before_summary->a_p_days)/$yesterday_summary->a_p_days)*100 == 0)
                        <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                    @else
                        <div style="vertical-align: middle; font-size: 15px; color: green; "><i class="fas fa-arrow-up"></i>{{number_format((($yesterday_summary->a_p_days-$day_before_summary->a_p_days)/$yesterday_summary->a_p_days)*100,2)}}%</div>
                    @endif
                </div>
                <canvas id="a_p_days"></canvas>
                <div style="text-align: center">
                    <div style="font-size: 18px; font-weight: bold;">Month to Date</div>
                    <div style="display:inline-block; font-size: 30px; color: black; ">{{number_format($month_summary->sum('a_p_days'))}}</div>
                </div>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="well">
                <div style="text-align: center;">
                    <div style="font-size: 18px; font-weight: bold;">IRF Days</div>
                    <div style="vertical-align: middle; font-size: 30px; color: black; ">{{number_format($yesterday_summary->irf_days)}}</div>
                    @if((($yesterday_summary->irf_days-$day_before_summary->irf_days)/$yesterday_summary->irf_days)*100 < 0)
                        <div style="vertical-align: middle; font-size: 15px; color: red; "><i class="fas fa-arrow-down"></i>{{number_format(((($yesterday_summary->irf_days-$day_before_summary->irf_days)/$yesterday_summary->irf_days)*100)/-1,2)}}%</div>
                    @elseif((($yesterday_summary->irf_days-$day_before_summary->irf_days)/$yesterday_summary->irf_days)*100 == 0)
                        <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                    @else
                        <div style="vertical-align: middle; font-size: 15px; color: green; "><i class="fas fa-arrow-up"></i>{{number_format((($yesterday_summary->irf_days-$day_before_summary->irf_days)/$yesterday_summary->irf_days)*100,2)}}%</div>
                    @endif
                </div>
                <canvas id="irf_days"></canvas>
                <div style="text-align: center">
                    <div style="font-size: 18px; font-weight: bold;">Month to Date</div>
                    <div style="display:inline-block; font-size: 30px; color: black; ">{{number_format($month_summary->sum('irf_days'))}}</div>
                </div>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="well">
                <div style="text-align: center;">
                    <div style="font-size: 18px; font-weight: bold;">NICU Days</div>
                    <div style="vertical-align: middle; font-size: 30px; color: black; ">{{number_format($yesterday_summary->nicu_days)}}</div>
                    @if($yesterday_summary->nicu_days == 0 )
                        @if($yesterday_summary->nicu_days == 0 AND $day_before_summary->nicu_days == 0)
                            <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                        @else
                            <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                        @endif
                    @else
                        @if((($yesterday_summary->nicu_days-$day_before_summary->nicu_days)/$yesterday_summary->nicu_days)*100 < 0)
                            <div style="vertical-align: middle; font-size: 15px; color: red; "><i class="fas fa-arrow-down"></i>{{number_format(((($yesterday_summary->nicu_days-$day_before_summary->nicu_days)/$yesterday_summary->nicu_days)*100)/-1,2)}}%</div>
                        @elseif((($yesterday_summary->nicu_days-$day_before_summary->nicu_days)/$yesterday_summary->nicu_days)*100 == 0)
                            <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                        @else
                            <div style="vertical-align: middle; font-size: 15px; color: green; "><i class="fas fa-arrow-up"></i>{{number_format((($yesterday_summary->nicu_days-$day_before_summary->nicu_days)/$yesterday_summary->nicu_days)*100,2)}}%</div>
                        @endif
                    @endif
                </div>
                <canvas id="nicu_days"></canvas>
                <div style="text-align: center">
                    <div style="font-size: 18px; font-weight: bold;">Month to Date</div>
                    <div style="display:inline-block; font-size: 30px; color: black; ">{{number_format($month_summary->sum('nicu_days'))}}</div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="well">
                <div style="text-align: center;">
                    <div style="font-size: 18px; font-weight: bold;">Total Days</div>
                    <div style="vertical-align: middle; font-size: 30px; color: black; ">{{number_format($yesterday_summary->a_p_days)}}</div>
                    @if((($yesterday_summary->total_days-$day_before_summary->total_days)/$yesterday_summary->total_days)*100 < 0)
                        <div style="vertical-align: middle; font-size: 15px; color: red; "><i class="fas fa-arrow-down"></i>{{number_format(((($yesterday_summary->total_days-$day_before_summary->total_days)/$yesterday_summary->total_days)*100)/-1,2)}}%</div>
                    @elseif((($yesterday_summary->total_days-$day_before_summary->total_days)/$yesterday_summary->total_days)*100 == 0)
                        <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                    @else
                        <div style="vertical-align: middle; font-size: 15px; color: green; "><i class="fas fa-arrow-up"></i>{{number_format((($yesterday_summary->total_days-$day_before_summary->total_days)/$yesterday_summary->total_days)*100,2)}}%</div>
                    @endif
                </div>
                <canvas id="total_days" height="42"></canvas>
                <div class="row">
                    <div class="col-lg-6">
                        <div style="font-size: 18px; font-weight: bold;">Month to Date</div>
                        <div style="font-size: 30px; color: black; ">{{number_format($month_summary->sum('total_days'))}}</div>
                    </div>
                    <div class="col-lg-6" style="text-align: right">
                        <div>
                            <div style="font-size: 18px; font-weight: bold;">Avg Daily Census</div>
                            <div style="font-size: 30px; color: black; ">{{number_format($month_summary->avg('total_days'),2)}}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

        <div class="item">
            <div class="col-md-12">
                <div class="col-lg-2">
                    <div class="well">
                        <div style="text-align: center;">
                            <div style="font-size: 18px; font-weight: bold;">Adult PED Discharges</div>
                            <div style="vertical-align: middle; font-size: 30px; color: black; ">{{number_format($yesterday_summary->adlt_disc)}}</div>
                            @if((($yesterday_summary->adlt_disc-$day_before_summary->adlt_disc)/$yesterday_summary->adlt_disc)*100 < 0)
                                <div style="vertical-align: middle; font-size: 15px; color: red; "><i class="fas fa-arrow-down"></i>{{number_format(((($yesterday_summary->adlt_disc-$day_before_summary->adlt_disc)/$yesterday_summary->adlt_disc)*100)/-1,2)}}%</div>
                            @elseif((($yesterday_summary->adlt_disc-$day_before_summary->adlt_disc)/$yesterday_summary->adlt_disc)*100 == 0)
                                <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                            @else
                                <div style="vertical-align: middle; font-size: 15px; color: green; "><i class="fas fa-arrow-up"></i>{{number_format((($yesterday_summary->adlt_disc-$day_before_summary->adlt_disc)/$yesterday_summary->adlt_disc)*100,2)}}%</div>
                            @endif
                        </div>
                        <canvas id="adlt_disc"></canvas>
                        <div style="text-align: center">
                            <div style="font-size: 18px; font-weight: bold;">Month to Date</div>
                            <div style="display:inline-block; font-size: 30px; color: black; ">{{number_format($month_summary->sum('adlt_disc'))}}</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="well">
                        <div style="text-align: center;">
                            <div style="font-size: 18px; font-weight: bold;">IRF Discharges</div>
                            <div style="vertical-align: middle; font-size: 30px; color: black; ">{{number_format($yesterday_summary->irf_disc)}}</div>
                            @if($yesterday_summary->irf_disc == 0 )
                                @if($yesterday_summary->irf_disc == 0 AND $day_before_summary->irf_disc == 0)
                                    <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                                @else
                                    <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                                @endif
                            @else
                                @if($yesterday_summary->irf_disc == 0 AND $day_before_summary->irf_disc == 0)
                                    <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                                @else
                                    @if((($yesterday_summary->irf_disc-$day_before_summary->irf_disc)/$yesterday_summary->irf_disc)*100 < 0)
                                        <div style="vertical-align: middle; font-size: 15px; color: red; "><i class="fas fa-arrow-down"></i>{{number_format(((($yesterday_summary->irf_disc-$day_before_summary->irf_disc)/$yesterday_summary->irf_disc)*100)/-1,2)}}%</div>
                                    @elseif((($yesterday_summary->irf_disc-$day_before_summary->irf_disc)/$yesterday_summary->irf_disc)*100 == 0)
                                        <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                                    @else
                                        <div style="vertical-align: middle; font-size: 15px; color: green; "><i class="fas fa-arrow-up"></i>{{number_format((($yesterday_summary->irf_disc-$day_before_summary->irf_disc)/$yesterday_summary->irf_disc)*100,2)}}%</div>
                                    @endif
                                @endif
                            @endif
                        </div>
                        <canvas id="irf_disc"></canvas>
                        <div style="text-align: center">
                            <div style="font-size: 18px; font-weight: bold;">Month to Date</div>
                            <div style="display:inline-block; font-size: 30px; color: black; ">{{number_format($month_summary->sum('irf_disc'))}}</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="well">
                        <div style="text-align: center;">
                            <div style="font-size: 18px; font-weight: bold;">NICU Discharges</div>
                            <div style="vertical-align: middle; font-size: 30px; color: black; ">{{number_format($yesterday_summary->nicu_disc)}}</div>
                            @if($yesterday_summary->nicu_disc == 0 )
                                <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                            @else
                                @if($yesterday_summary->nicu_disc == 0 AND $day_before_summary->nicu_disc == 0)
                                    <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                                @else
                                    @if((($yesterday_summary->nicu_disc-$day_before_summary->nicu_disc)/$yesterday_summary->nicu_disc)*100 < 0)
                                        <div style="vertical-align: middle; font-size: 15px; color: red; "><i class="fas fa-arrow-down"></i>{{number_format(((($yesterday_summary->nicu_disc-$day_before_summary->nicu_disc)/$yesterday_summary->nicu_disc)*100)/-1,2)}}%</div>
                                    @elseif((($yesterday_summary->nicu_disc-$day_before_summary->nicu_disc)/$yesterday_summary->nicu_disc)*100 == 0)
                                        <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                                    @else
                                        <div style="vertical-align: middle; font-size: 15px; color: green; "><i class="fas fa-arrow-up"></i>{{number_format((($yesterday_summary->nicu_disc-$day_before_summary->nicu_disc)/$yesterday_summary->nicu_disc)*100,2)}}%</div>
                                    @endif

                                @endif
                            @endif
                        </div>
                        <canvas id="nicu_disc"></canvas>
                        <div style="text-align: center">
                            <div style="font-size: 18px; font-weight: bold;">Month to Date</div>
                            <div style="display:inline-block; font-size: 30px; color: black; ">{{number_format($month_summary->sum('nicu_disc'))}}</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="well">
                        <div style="text-align: center;">
                            <div style="font-size: 18px; font-weight: bold;">Total Discharges</div>
                            <div style="vertical-align: middle; font-size: 30px; color: black; ">{{number_format($yesterday_summary->total_disc)}}</div>
                            @if(((($yesterday_summary->total_disc)-($day_before_summary->total_disc))/($yesterday_summary->total_disc))*100 < 0)
                                <div style="vertical-align: middle; font-size: 15px; color: red; "><i class="fas fa-arrow-down"></i>{{number_format((((($yesterday_summary->total_disc)-($day_before_summary->total_disc))/($yesterday_summary->total_disc))*100)/-1,2)}}%</div>
                            @elseif((($yesterday_summary->total_disc-$day_before_summary->total_disc)/$yesterday_summary->total_disc)*100 == 0)
                                <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                            @else
                                <div style="vertical-align: middle; font-size: 15px; color: green; "><i class="fas fa-arrow-up"></i>{{number_format(((($yesterday_summary->total_disc)-($day_before_summary->total_disc))/($yesterday_summary->total_disc))*100,2)}}%</div>
                            @endif
                        </div>
                        <canvas id="total_disc" height="42"></canvas>
                        <div style="text-align: center">
                            <div style="font-size: 18px; font-weight: bold;">Month to Date</div>
                            <div style="display:inline-block; font-size: 30px; color: black; ">{{number_format($year_summary->sum('total_disc'))}}</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="well">
                        <div style="text-align: center;">
                            <div style="font-size: 18px; font-weight: bold;">Normal Newborn Discharges</div>
                            <div style="vertical-align: middle; font-size: 30px; color: black; ">{{number_format($yesterday_summary->norm_nb_disc)}}</div>
                            @if($yesterday_summary->norm_nb_disc == 0 )
                                <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                            @else
                                @if((($yesterday_summary->norm_nb_disc-$day_before_summary->norm_nb_disc)/$yesterday_summary->norm_nb_disc)*100 < 0)
                                    <div style="vertical-align: middle; font-size: 15px; color: red; "><i class="fas fa-arrow-down"></i>{{number_format(((($yesterday_summary->norm_nb_disc-$day_before_summary->norm_nb_disc)/$yesterday_summary->norm_nb_disc)*100)/-1,2)}}%</div>
                                @elseif((($yesterday_summary->norm_nb_disc-$day_before_summary->norm_nb_disc)/$yesterday_summary->norm_nb_disc)*100 == 0)
                                    <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                                @else
                                    <div style="vertical-align: middle; font-size: 15px; color: green; "><i class="fas fa-arrow-up"></i>{{number_format((($yesterday_summary->norm_nb_disc-$day_before_summary->norm_nb_disc)/$yesterday_summary->norm_nb_disc)*100,2)}}%</div>
                                @endif
                            @endif
                        </div>
                        <canvas id="norm_nb_disc"  height="30"></canvas>
                        <div style="text-align: center">
                            <div style="font-size: 18px; font-weight: bold;">Month to Date</div>
                            <div style="display:inline-block; font-size: 30px; color: black; ">{{number_format($month_summary->sum('norm_nb_disc'))}}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="item">
            <div class="col-lg-12">
                <div class="col-lg-6">
                    <div class="well">
                        <div style="text-align: center;">
                            <div style="font-size: 18px; font-weight: bold;">Total ER Visits</div>
                            <div style="vertical-align: middle; font-size: 30px; color: black; ">{{number_format($yesterday_summary->total_er_visits)}}</div>
                            @if((($yesterday_summary->total_er_visits-$day_before_summary->total_er_visits)/$yesterday_summary->total_er_visits)*100 < 0)
                                <div style="vertical-align: middle; font-size: 15px; color: red; "><i class="fas fa-arrow-down"></i>{{number_format(((($yesterday_summary->total_er_visits-$day_before_summary->total_er_visits)/$yesterday_summary->total_er_visits)*100)/-1,2)}}%</div>
                            @elseif((($yesterday_summary->total_er_visits-$day_before_summary->total_er_visits)/$yesterday_summary->total_er_visits)*100 == 0)
                                <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                            @else
                                <div style="vertical-align: middle; font-size: 15px; color: green; "><i class="fas fa-arrow-up"></i>{{number_format((($yesterday_summary->total_er_visits-$day_before_summary->total_er_visits)/$yesterday_summary->total_er_visits)*100,2)}}%</div>
                            @endif
                        </div>
                        <canvas id="total_er_visits" height="35"></canvas>
                        <div style="text-align: center">
                            <div style="font-size: 18px; font-weight: bold;">Month to Date</div>
                            <div style="display:inline-block; font-size: 30px; color: black; ">{{number_format($month_summary->sum('total_er_visits'))}}</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="well">
                        <div style="text-align: center;">
                            <div style="font-size: 18px; font-weight: bold;">ER Admits to Bed</div>
                            <div style="vertical-align: middle; font-size: 30px; color: black; ">{{number_format($yesterday_summary->er_admits)}}</div>
                            @if((($yesterday_summary->er_admits-$day_before_summary->er_admits)/$yesterday_summary->er_admits)*100 < 0)
                                <div style="vertical-align: middle; font-size: 15px; color: red; "><i class="fas fa-arrow-down"></i>{{number_format(((($yesterday_summary->er_admits-$day_before_summary->er_admits)/$yesterday_summary->er_admits)*100)/-1,2)}}%</div>
                            @elseif((($yesterday_summary->er_admits-$day_before_summary->er_admits)/$yesterday_summary->er_admits)*100 == 0)
                                <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                            @else
                                <div style="vertical-align: middle; font-size: 15px; color: green; "><i class="fas fa-arrow-up"></i>{{number_format((($yesterday_summary->er_admits-$day_before_summary->er_admits)/$yesterday_summary->er_admits)*100,2)}}%</div>
                            @endif
                        </div>
                        <canvas id="er_admits" height="35"></canvas>
                        <div style="text-align: center">
                            <div style="font-size: 18px; font-weight: bold;">Month to Date</div>
                            <div style="display:inline-block; font-size: 30px; color: black; ">{{number_format($month_summary->sum('er_admits'))}}</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="well">
                        <div style="text-align: center;">
                            <div style="font-size: 18px; font-weight: bold;">ER Admits to Observation</div>
                            <div style="vertical-align: middle; font-size: 30px; color: black; ">{{number_format($yesterday_summary->er_obs)}}</div>
                            @if((($yesterday_summary->er_admits-$day_before_summary->er_obs)/$yesterday_summary->er_obs)*100 < 0)
                                <div style="vertical-align: middle; font-size: 15px; color: red; "><i class="fas fa-arrow-down"></i>{{number_format(((($yesterday_summary->er_obs-$day_before_summary->er_obs)/$yesterday_summary->er_obs)*100)/-1,2)}}%</div>
                            @elseif((($yesterday_summary->er_admits-$day_before_summary->er_obs)/$yesterday_summary->er_obs)*100 == 0)
                                <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                            @else
                                <div style="vertical-align: middle; font-size: 15px; color: green; "><i class="fas fa-arrow-up"></i>{{number_format((($yesterday_summary->er_obs-$day_before_summary->er_obs)/$yesterday_summary->er_obs)*100,2)}}%</div>
                            @endif
                        </div>
                        <canvas id="er_obs" height="35"></canvas>
                        <div style="text-align: center">
                            <div style="font-size: 18px; font-weight: bold;">Month to Date</div>
                            <div style="display:inline-block; font-size: 30px; color: black; ">{{number_format($month_summary->sum('er_obs'))}}</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="well">
                        <div style="text-align: center;">
                            <div style="font-size: 18px; font-weight: bold;">Transfers from ER</div>
                            <div style="vertical-align: middle; font-size: 30px; color: black; ">{{number_format($yesterday_summary->transfers_from_er)}}</div>
                            @if($yesterday_summary->transfers_from_er == 0 )
                                <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                            @else
                                @if((($yesterday_summary->transfers_from_er-$day_before_summary->transfers_from_er)/$yesterday_summary->transfers_from_er)*100 < 0)
                                    <div style="vertical-align: middle; font-size: 15px; color: red; "><i class="fas fa-arrow-down"></i>{{number_format(((($yesterday_summary->transfers_from_er-$day_before_summary->transfers_from_er)/$yesterday_summary->transfers_from_er)*100)/-1,2)}}%</div>
                                @elseif((($yesterday_summary->transfers_from_er-$day_before_summary->transfers_from_er)/$yesterday_summary->transfers_from_er)*100 == 0)
                                    <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                                @else
                                    <div style="vertical-align: middle; font-size: 15px; color: green; "><i class="fas fa-arrow-up"></i>{{number_format((($yesterday_summary->transfers_from_er-$day_before_summary->transfers_from_er)/$yesterday_summary->transfers_from_er)*100,2)}}%</div>
                                @endif
                            @endif
                        </div>
                        <canvas id="transfers_from_er" height="35"></canvas>
                        <div style="text-align: center">
                            <div style="font-size: 18px; font-weight: bold;">Month to Date</div>
                            <div style="display:inline-block; font-size: 30px; color: black; ">{{number_format($month_summary->sum('transfers_from_er'))}}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="col-lg-12">
                <div class="col-lg-2">
                    <div class="well">
                        <div style="text-align: center;">
                            <div style="font-size: 18px; font-weight: bold;">GI Lab Proc</div>
                            <div style="vertical-align: middle; font-size: 30px; color: black; ">{{number_format($yesterday_summary->gi_lab)}}</div>
                            @if($yesterday_summary->gi_lab == 0 )
                                @if((($day_before_summary->gi_lab-$yesterday_summary->gi_lab))*100 > 0)
                                    <div style="vertical-align: middle; font-size: 15px; color: red; "><i class="fas fa-arrow-down"></i>{{number_format(((($yesterday_summary->gi_lab-$day_before_summary->gi_lab))*100)/-1,2)}}%</div>
                                @elseif((($day_before_summary->gi_lab-$yesterday_summary->gi_lab))*100 == 0)
                                    <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                                @else
                                    <div style="vertical-align: middle; font-size: 15px; color: green; "><i class="fas fa-arrow-up"></i>{{number_format(((($yesterday_summary->gi_lab-$day_before_summary->gi_lab))*100),2)}}%</div>
                                @endif
                            @else
                                @if((($yesterday_summary->gi_lab-$day_before_summary->gi_lab)/$yesterday_summary->gi_lab)*100 < 0)
                                    <div style="vertical-align: middle; font-size: 15px; color: red; "><i class="fas fa-arrow-down"></i>{{number_format(((($yesterday_summary->gi_lab-$day_before_summary->gi_lab)/$yesterday_summary->gi_lab)*100)/-1,2)}}%</div>
                                @elseif((($yesterday_summary->gi_lab-$day_before_summary->gi_lab)/$yesterday_summary->gi_lab)*100 == 0)
                                    <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                                @else
                                    <div style="vertical-align: middle; font-size: 15px; color: green; "><i class="fas fa-arrow-up"></i>{{number_format((($yesterday_summary->gi_lab-$day_before_summary->gi_lab)/$yesterday_summary->gi_lab)*100,2)}}%</div>
                                @endif
                            @Endif
                        </div>
                        <canvas id="gi_lab"></canvas>
                        <div style="text-align: center">
                            <div style="font-size: 18px; font-weight: bold;">Month to Date</div>
                            <div style="display:inline-block; font-size: 30px; color: black; ">{{number_format($month_summary->sum('gi_lab'))}}</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="well">
                        <div style="text-align: center;">
                            <div style="font-size: 18px; font-weight: bold;">Cath Lab Proc</div>
                            <div style="vertical-align: middle; font-size: 30px; color: black; ">{{number_format($yesterday_summary->cath_lab)}}</div>
                            @if($yesterday_summary->cath_lab == 0 )
                                <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                            @else
                                @if((($yesterday_summary->cath_lab-$day_before_summary->cath_lab)/$yesterday_summary->cath_lab)*100 < 0)
                                    <div style="vertical-align: middle; font-size: 15px; color: red; "><i class="fas fa-arrow-down"></i>{{number_format(((($yesterday_summary->cath_lab-$day_before_summary->cath_lab)/$yesterday_summary->cath_lab)*100)/-1,2)}}%</div>
                                @elseif((($yesterday_summary->cath_lab-$day_before_summary->cath_lab)/$yesterday_summary->cath_lab)*100 == 0)
                                    <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                                @else
                                    <div style="vertical-align: middle; font-size: 15px; color: green; "><i class="fas fa-arrow-up"></i>{{number_format((($yesterday_summary->cath_lab-$day_before_summary->cath_lab)/$yesterday_summary->cath_lab)*100,2)}}%</div>
                                @endif
                            @endif
                        </div>
                        <canvas id="cath_lab"></canvas>
                        <div style="text-align: center">
                            <div style="font-size: 18px; font-weight: bold;">Month to Date</div>
                            <div style="display:inline-block; font-size: 30px; color: black; ">{{number_format($month_summary->sum('cath_lab'))}}</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="well">
                        <div style="text-align: center;">
                            <div style="font-size: 18px; font-weight: bold;">Main OR</div>
                            <div style="vertical-align: middle; font-size: 30px; color: black; ">{{number_format($yesterday_summary->main_or)}}</div>
                            @if($yesterday_summary->main_or == 0 )
                                <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                            @else
                                @if((($yesterday_summary->main_or-$day_before_summary->main_or)/$yesterday_summary->main_or)*100 < 0)
                                    <div style="vertical-align: middle; font-size: 15px; color: red; "><i class="fas fa-arrow-down"></i>{{number_format(((($yesterday_summary->main_or-$day_before_summary->main_or)/$yesterday_summary->main_or)*100)/-1,2)}}%</div>
                                @elseif((($yesterday_summary->main_or-$day_before_summary->main_or)/$yesterday_summary->main_or)*100 == 0)
                                    <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                                @else
                                    <div style="vertical-align: middle; font-size: 15px; color: green; "><i class="fas fa-arrow-up"></i>{{number_format((($yesterday_summary->main_or-$day_before_summary->main_or)/$yesterday_summary->main_or)*100,2)}}%</div>
                                @endif
                            @endif
                        </div>
                        <canvas id="main_or"></canvas>
                        <div style="text-align: center">
                            <div style="font-size: 18px; font-weight: bold;">Month to Date</div>
                            <div style="display:inline-block; font-size: 30px; color: black; ">{{number_format($month_summary->sum('main_or'))}}</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="well">
                        <div style="text-align: center;">
                            <div style="font-size: 18px; font-weight: bold;">Total OP</div>
                            <div style="vertical-align: middle; font-size: 30px; color: black; ">{{number_format($yesterday_summary->total_op)}}</div>
                            @if($yesterday_summary->total_op == 0 )
                                <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                            @else
                                @if($yesterday_summary->total_op == 0 )
                                    <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                                @else
                                    @if((($yesterday_summary->total_op-$day_before_summary->total_op)/$yesterday_summary->total_op)*100 < 0)
                                        <div style="vertical-align: middle; font-size: 15px; color: red; "><i class="fas fa-arrow-down"></i>{{number_format(((($yesterday_summary->total_op-$day_before_summary->total_op)/$yesterday_summary->total_op)*100)/-1,2)}}%</div>
                                    @elseif((($yesterday_summary->total_op-$day_before_summary->total_op)/$yesterday_summary->total_op)*100 == 0)
                                        <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                                    @else
                                        <div style="vertical-align: middle; font-size: 15px; color: green; "><i class="fas fa-arrow-up"></i>{{number_format((($yesterday_summary->total_op-$day_before_summary->total_op)/$yesterday_summary->total_op)*100,2)}}%</div>
                                    @endif
                                @endif
                            @endif
                        </div>
                        <canvas id="total_op" height="42"></canvas>
                        <div style="text-align: center">
                            <div style="font-size: 18px; font-weight: bold;">Month to Date</div>
                            <div style="display:inline-block; font-size: 30px; color: black; ">{{number_format($month_summary->sum('total_op'))}}</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="well">
                        <div style="text-align: center;">
                            <div style="font-size: 18px; font-weight: bold;">ASC</div>
                            <div style="vertical-align: middle; font-size: 30px; color: black; ">{{number_format($yesterday_summary->asc_or)}}</div>
                            @if($yesterday_summary->asc_or == 0 )
                                <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                            @else
                                @if((($yesterday_summary->asc_or-$day_before_summary->asc_or)/$yesterday_summary->asc_or)*100 < 0)
                                    <div style="vertical-align: middle; font-size: 15px; color: red; "><i class="fas fa-arrow-down"></i>{{number_format(((($yesterday_summary->asc_or-$day_before_summary->asc_or)/$yesterday_summary->asc_or)*100)/-1,2)}}%</div>
                                @elseif((($yesterday_summary->asc_or-$day_before_summary->asc_or)/$yesterday_summary->asc_or)*100 == 0)
                                    <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                                @else
                                    <div style="vertical-align: middle; font-size: 15px; color: green; "><i class="fas fa-arrow-up"></i>{{number_format((($yesterday_summary->asc_or-$day_before_summary->asc_or)/$yesterday_summary->asc_or)*100,2)}}%</div>
                                @endif
                            @endif
                        </div>
                        <canvas id="asc_or"></canvas>
                        <div style="text-align: center">
                            <div style="font-size: 18px; font-weight: bold;">Month to Date</div>
                            <div style="display:inline-block; font-size: 30px; color: black; ">{{number_format($month_summary->sum('asc_or'))}}</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="well">
                        <div style="text-align: center;">
                            <div style="font-size: 18px; font-weight: bold;">Main OP</div>
                            <div style="vertical-align: middle; font-size: 30px; color: black; ">{{number_format($yesterday_summary->main_op)}}</div>
                            @if($yesterday_summary->main_op == 0 )
                                <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                            @else
                                @if((($yesterday_summary->main_op-$day_before_summary->main_op)/$yesterday_summary->main_op)*100 < 0)
                                    <div style="vertical-align: middle; font-size: 15px; color: red; "><i class="fas fa-arrow-down"></i>{{number_format(((($yesterday_summary->main_op-$day_before_summary->main_op)/$yesterday_summary->main_op)*100)/-1,2)}}%</div>
                                @elseif((($yesterday_summary->main_op-$day_before_summary->main_op)/$yesterday_summary->main_op)*100 == 0)
                                    <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                                @else
                                    <div style="vertical-align: middle; font-size: 15px; color: green; "><i class="fas fa-arrow-up"></i>{{number_format((($yesterday_summary->main_op-$day_before_summary->main_op)/$yesterday_summary->main_op)*100,2)}}%</div>
                                @endif
                            @endif
                        </div>
                        <canvas id="main_op"></canvas>
                        <div style="text-align: center">
                            <div style="font-size: 18px; font-weight: bold;">Month to Date</div>
                            <div style="display:inline-block; font-size: 30px; color: black; ">{{number_format($month_summary->sum('main_op'))}}</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="well">
                        <div style="text-align: center;">
                            <div style="font-size: 18px; font-weight: bold;">Main IP</div>
                            <div style="vertical-align: middle; font-size: 30px; color: black; ">{{number_format($yesterday_summary->main_ip)}}</div>
                            @if((($yesterday_summary->main_ip-$day_before_summary->main_ip)/$yesterday_summary->main_ip)*100 < 0)
                                <div style="vertical-align: middle; font-size: 15px; color: red; "><i class="fas fa-arrow-down"></i>{{number_format(((($yesterday_summary->main_ip-$day_before_summary->main_ip)/$yesterday_summary->main_ip)*100)/-1,2)}}%</div>
                            @elseif((($yesterday_summary->main_ip-$day_before_summary->main_ip)/$yesterday_summary->main_ip)*100 == 0)
                                <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                            @else
                                <div style="vertical-align: middle; font-size: 15px; color: green; "><i class="fas fa-arrow-up"></i>{{number_format((($yesterday_summary->main_ip-$day_before_summary->main_ip)/$yesterday_summary->main_ip)*100,2)}}%</div>
                            @endif
                        </div>
                        <canvas id="main_ip"></canvas>
                        <div style="text-align: center">
                            <div style="font-size: 18px; font-weight: bold;">Month to Date</div>
                            <div style="display:inline-block; font-size: 30px; color: black; ">{{number_format($month_summary->sum('main_ip'))}}</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="well">
                        <div style="text-align: center;">
                            <div style="font-size: 18px; font-weight: bold;">Total Surgeries</div>
                            <div style="vertical-align: middle; font-size: 30px; color: black; ">{{number_format($yesterday_summary->total_sur)}}</div>
                            @if((($yesterday_summary->total_sur-$day_before_summary->total_sur)/$yesterday_summary->total_sur)*100 < 0)
                                <div style="vertical-align: middle; font-size: 15px; color: red; "><i class="fas fa-arrow-down"></i>{{number_format(((($yesterday_summary->total_sur-$day_before_summary->total_sur)/$yesterday_summary->total_sur)*100)/-1,2)}}%</div>
                            @elseif((($yesterday_summary->total_sur-$day_before_summary->total_sur)/$yesterday_summary->total_sur)*100 == 0)
                                <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                            @else
                                <div style="vertical-align: middle; font-size: 15px; color: green; "><i class="fas fa-arrow-up"></i>{{number_format((($yesterday_summary->total_sur-$day_before_summary->total_sur)/$yesterday_summary->total_sur)*100,2)}}%</div>
                            @endif
                        </div>
                        <canvas id="total_sur" height="42"></canvas>
                        <div style="text-align: center">
                            <div style="font-size: 18px; font-weight: bold;">Month to Date</div>
                            <div style="display:inline-block; font-size: 30px; color: black; ">{{number_format($month_summary->sum('total_sur'))}}</div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="item">
            <div class="col-lg-6">
                <div class="well">
                    <div style="text-align: center;">
                        <div style="font-size: 18px; font-weight: bold;">MRI Procedures</div>
                        <div style="vertical-align: middle; font-size: 30px; color: black; ">{{number_format($yesterday_summary->mri_proc)}}</div>
                        @if((($yesterday_summary->mri_proc-$day_before_summary->mri_proc)/$yesterday_summary->mri_proc)*100 < 0)
                            <div style="vertical-align: middle; font-size: 15px; color: red; "><i class="fas fa-arrow-down"></i>{{number_format(((($yesterday_summary->mri_proc-$day_before_summary->mri_proc)/$yesterday_summary->mri_proc)*100)/-1,2)}}%</div>
                        @elseif((($yesterday_summary->mri_proc-$day_before_summary->mri_proc)/$yesterday_summary->mri_proc)*100 == 0)
                            <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                        @else
                            <div style="vertical-align: middle; font-size: 15px; color: green; "><i class="fas fa-arrow-up"></i>{{number_format((($yesterday_summary->mri_proc-$day_before_summary->mri_proc)/$yesterday_summary->mri_proc)*100,2)}}%</div>
                        @endif
                    </div>
                    <canvas id="mri_proc" height="42"></canvas>
                    <div style="text-align: center">
                        <div style="font-size: 18px; font-weight: bold;">Month to Date</div>
                        <div style="display:inline-block; font-size: 30px; color: black; ">{{number_format($month_summary->sum('mri_proc'))}}</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="well">
                    <div style="text-align: center;">
                        <div style="font-size: 18px; font-weight: bold;">CT Procedures</div>
                        <div style="vertical-align: middle; font-size: 30px; color: black; ">{{number_format($yesterday_summary->ct_proc)}}</div>
                        @if((($yesterday_summary->ct_proc-$day_before_summary->ct_proc)/$yesterday_summary->ct_proc)*100 < 0)
                            <div style="vertical-align: middle; font-size: 15px; color: red; "><i class="fas fa-arrow-down"></i>{{number_format(((($yesterday_summary->ct_proc-$day_before_summary->ct_proc)/$yesterday_summary->ct_proc)*100)/-1,2)}}%</div>
                        @elseif((($yesterday_summary->ct_proc-$day_before_summary->ct_proc)/$yesterday_summary->ct_proc)*100 == 0)
                            <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                        @else
                            <div style="vertical-align: middle; font-size: 15px; color: green; "><i class="fas fa-arrow-up"></i>{{number_format((($yesterday_summary->ct_proc-$day_before_summary->ct_proc)/$yesterday_summary->ct_proc)*100,2)}}%</div>
                        @endif
                    </div>
                    <canvas id="ct_proc" height="42"></canvas>
                    <div style="text-align: center">
                        <div style="font-size: 18px; font-weight: bold;">Month to Date</div>
                        <div style="display:inline-block; font-size: 30px; color: black; ">{{number_format($month_summary->sum('ct_proc'))}}</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="well">
                    <div style="text-align: center;">
                        <div style="font-size: 18px; font-weight: bold;">Mammo Procedures</div>
                        <div style="vertical-align: middle; font-size: 30px; color: black; ">{{number_format($yesterday_summary->mammo_proc)}}</div>
                        @if($yesterday_summary->mammo_proc == 0 )
                            <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                        @else
                            @if((($yesterday_summary->mammo_proc-$day_before_summary->mammo_proc)/$yesterday_summary->mammo_proc)*100 < 0)
                                <div style="vertical-align: middle; font-size: 15px; color: red; "><i class="fas fa-arrow-down"></i>{{number_format(((($yesterday_summary->mammo_proc-$day_before_summary->mammo_proc)/$yesterday_summary->mammo_proc)*100)/-1,2)}}%</div>
                            @elseif((($yesterday_summary->mammo_proc-$day_before_summary->mammo_proc)/$yesterday_summary->mammo_proc)*100 == 0)
                                <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                            @else
                                <div style="vertical-align: middle; font-size: 15px; color: green; "><i class="fas fa-arrow-up"></i>{{number_format((($yesterday_summary->mammo_proc-$day_before_summary->mammo_proc)/$yesterday_summary->mammo_proc)*100,2)}}%</div>
                            @endif
                        @endif
                    </div>
                    <canvas id="mammo_proc" height="42"></canvas>
                    <div style="text-align: center">
                        <div style="font-size: 18px; font-weight: bold;">Month to Date</div>
                        <div style="display:inline-block; font-size: 30px; color: black; ">{{number_format($month_summary->sum('mammo_proc'))}}</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="well">
                    <div style="text-align: center;">
                        <div style="font-size: 18px; font-weight: bold;">PET Procedures</div>
                        <div style="vertical-align: middle; font-size: 30px; color: black; ">{{number_format($yesterday_summary->pet_proc)}}</div>
                        @if($yesterday_summary->pet_proc == 0 )
                            <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                        @else
                            @if((($yesterday_summary->pet_proc-$day_before_summary->pet_proc)/$yesterday_summary->pet_proc)*100 < 0)
                                <div style="vertical-align: middle; font-size: 15px; color: red; "><i class="fas fa-arrow-down"></i>{{number_format(((($yesterday_summary->pet_proc-$day_before_summary->pet_proc)/$yesterday_summary->pet_proc)*100)/-1,2)}}%</div>
                            @elseif((($yesterday_summary->pet_proc-$day_before_summary->pet_proc)/$yesterday_summary->pet_proc)*100 == 0)
                                <div style="vertical-align: middle; font-size: 15px; color: black; "> No change</div>
                            @else
                                <div style="vertical-align: middle; font-size: 15px; color: green; "><i class="fas fa-arrow-up"></i>{{number_format((($yesterday_summary->pet_proc-$day_before_summary->pet_proc)/$yesterday_summary->pet_proc)*100,2)}}%</div>
                            @endif
                        @endif
                    </div>
                    <canvas id="pet_proc" height="42"></canvas>
                    <div style="text-align: center">
                        <div style="font-size: 18px; font-weight: bold;">Month to Date</div>
                        <div style="display:inline-block; font-size: 30px; color: black; ">{{number_format($month_summary->sum('pet_proc'))}}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body" style="height:750px; width:100%; overflow-y: auto;">
                        <table class="table table-condensed table-hover table-bordered">
                            <thead>
                            <tr>
                                <td><b>Date</b></td>
                                <td><b>DOW</b></td>
                                <td align="center"><b>Professional Revenue</b></td>
                                <td align="center"><b>Hospital Revenue</b></td>
                                <td align="center" bgcolor="#c9d3ee"><b>Total Patient Revenue</b></td>
                                <td align="center"><b>ADLT/PED Days</b></td>
                                <td align="center"><b>IRF Days</b></td>
                                <td align="center"><b>NICU Days</b></td>
                                <td align="center" bgcolor="#c9d3ee"><b>Total Days</b></td>
                                <td align="center"><b>ADLT/PED Disc</b></td>
                                <td align="center"><b>IFR Disc</b></td>
                                <td align="center"><b>NICU Disc</b></td>
                                <td align="center"><b>Newborn Disc</b></td>
                                <td align="center" bgcolor="#c9d3ee"><b>Total Disc</b></td>
                                <td align="center" bgcolor="#c9d3ee"><b>Total ER Visits</b></td>
                                <td align="center"><b>ER Admits to Bed</b></td>
                                <td align="center"><b>ER Admits to OBS</b></td>
                                <td align="center"><b>Transfers from ER</b></td>
                                <td align="center"><b>GI Lab Proc</b></td>
                                <td align="center"><b>Cath Lab Proc</b></td>
                                <td align="center"><b>SNH Main OR</b></td>
                                <td align="center"><b>ASC</b></td>
                                <td align="center"><b>SGMC Main OP</b></td>
                                <td align="center"><b>SGMC Main IP</b></td>
                                <td align="center" bgcolor="#c9d3ee"><b>Total OP</b></td>
                                <td align="center" bgcolor="#c9d3ee"><b>Total SURG</b></td>
                                <td align="center"><b>MRI Proc</b></td>
                                <td align="center"><b>CT Proc</b></td>
                                <td align="center"><b>Mammo PROC</b></td>
                                <td align="center"><b>PET Proc</b></td>
                            </tr>
                            </thead>
                            @foreach($month_summary as $monthly)
                                <tr>
                                    <td nowrap="">{{$monthly->report_date->format("m-d-Y")}}</td>
                                    <td>
                                        @if($monthly->day_of_week == 7)
                                            Sun
                                        @elseif($monthly->day_of_week == 8)
                                            Mon
                                        @elseif($monthly->day_of_week == 9)
                                            Tue
                                        @elseif($monthly->day_of_week == 10)
                                            Wed
                                        @elseif($monthly->day_of_week == 11)
                                            Thr
                                        @elseif($monthly->day_of_week == 12)
                                            Fri
                                        @elseif($monthly->day_of_week == 13)
                                            Sat
                                        @endif
                                    </td>
                                    <td align="center">${{number_format($monthly->p_b_r,0)}}</td>
                                    <td align="center">${{number_format($monthly->h_r,0)}}</td>
                                    <td align="center"  bgcolor="#c9d3ee">${{number_format($monthly->h_r+$monthly->p_b_r,0)}}</td>
                                    <td align="center">{{$monthly->a_p_days}}</td>
                                    <td align="center">{{$monthly->irf_days}}</td>
                                    <td align="center">{{$monthly->nicu_days}}</td>
                                    <td align="center"  bgcolor="#c9d3ee">{{$monthly->nicu_days+$monthly->irf_days+$monthly->a_p_days}}</td>
                                    <td align="center">{{$monthly->adlt_disc}}</td>
                                    <td align="center">{{$monthly->irf_disc}}</td>
                                    <td align="center">{{$monthly->nicu_disc}}</td>
                                    <td align="center">{{$monthly->norm_nb_disc}}</td>
                                    <td align="center" bgcolor="#c9d3ee">{{$monthly->total_disc}}</td>
                                    <td align="center" bgcolor="#c9d3ee">{{$monthly->total_er_visits}}</td>
                                    <td align="center">{{$monthly->er_admits}}</td>
                                    <td align="center">{{$monthly->er_obs}}</td>
                                    <td align="center">{{$monthly->transfers_from_er}}</td>
                                    <td align="center">{{$monthly->gi_lab}}</td>
                                    <td align="center">{{$monthly->cath_lab}}</td>
                                    <td align="center">{{$monthly->main_or}}</td>
                                    <td align="center">{{$monthly->asc_or}}</td>
                                    <td align="center">{{$monthly->main_op}}</td>
                                    <td align="center">{{$monthly->main_ip}}</td>
                                    <td align="center" bgcolor="#c9d3ee">{{$monthly->total_op}}</td>
                                    <td align="center" bgcolor="#c9d3ee">{{$monthly->total_sur}}</td>
                                    <td align="center">{{$monthly->mri_proc}}</td>
                                    <td align="center">{{$monthly->ct_proc}}</td>
                                    <td align="center">{{$monthly->mammo_proc}}</td>
                                    <td align="center">{{$monthly->pet_proc}}</td>
                                </tr>
                            @endforeach
                            <tr bgcolor="#a3d6ff">
                                <td align="center" colspan="2"><b>MTD TOTAL</b></td>
                                <td align="center"><b>${{number_format($month_summary->sum('p_b_r'))}}</b></td>
                                <td align="center"><b>${{number_format($month_summary->sum('h_r'))}}</b></td>
                                <td align="center"  bgcolor="#c9d3ee"><b>${{number_format($month_summary->sum('h_r')+$month_summary->sum('p_b_r'),0)}}</b></td>
                                <td align="center"><b>{{$month_summary->sum('a_p_days')}}</b></td>
                                <td align="center"><b>{{$month_summary->sum('irf_days')}}</b></td>
                                <td align="center"><b>{{$month_summary->sum('nicu_days')}}</b></td>
                                <td align="center"  bgcolor="#c9d3ee"><b>{{$month_summary->sum('nicu_days')+$month_summary->sum('irf_days')+$month_summary->sum('a_p_days')}}</b></td>
                                <td align="center"><b>{{$month_summary->sum('adlt_disc')}}</b></td>
                                <td align="center"><b>{{$month_summary->sum('irf_disc')}}</b></td>
                                <td align="center"><b>{{$month_summary->sum('nicu_disc')}}</b></td>
                                <td align="center"><b>{{$month_summary->sum('norm_nb_disc')}}</b></td>
                                <td align="center" bgcolor="#c9d3ee"><b>{{$month_summary->sum('total_disc')}}</b></td>
                                <td align="center" bgcolor="#c9d3ee"><b>{{$month_summary->sum('total_er_visits')}}</b></td>
                                <td align="center"><b>{{$month_summary->sum('er_admits')}}</b></td>
                                <td align="center"><b>{{$month_summary->sum('er_obs')}}</b></td>
                                <td align="center"><b>{{$month_summary->sum('transfers_from_er')}}</b></td>
                                <td align="center"><b>{{$month_summary->sum('gi_lab')}}</b></td>
                                <td align="center"><b>{{$month_summary->sum('cath_lab')}}</b></td>
                                <td align="center"><b>{{$month_summary->sum('main_or')}}</b></td>
                                <td align="center"><b>{{$month_summary->sum('asc_or')}}</b></td>
                                <td align="center"><b>{{$month_summary->sum('main_op')}}</b></td>
                                <td align="center"><b>{{$month_summary->sum('main_ip')}}</b></td>
                                <td align="center" bgcolor="#c9d3ee"><b>{{$month_summary->sum('total_op')}}</b></td>
                                <td align="center" bgcolor="#c9d3ee"><b>{{$month_summary->sum('total_sur')}}</b></td>
                                <td align="center"><b>{{$month_summary->sum('mri_proc')}}</b></td>
                                <td align="center"><b>{{$month_summary->sum('ct_proc')}}</b></td>
                                <td align="center"><b>{{$month_summary->sum('mammo_proc')}}</b></td>
                                <td align="center"><b>{{$month_summary->sum('pet_proc')}}</b></td>
                            </tr>
                            <tr>
                                <td colspan="30">&nbsp;</td>
                            </tr>
                            @if(!empty($budget))
                            <tr>
                                <td colspan="4"><b>Months Budget</b></td>
                                <td align="center">${{number_format($budget->total_p_r,0)}}</td>
                                <td align="center">{{number_format($budget->a_p_days,0)}}</td>
                                <td align="center">{{number_format($budget->irf_days,0)}}</td>
                                <td align="center">{{number_format($budget->nicu_days,0)}}</td>
                                <td align="center">{{number_format($budget->nicu_days+$budget->irf_days+$budget->a_p_days,0)}}</td>
                                <td align="center">{{number_format($budget->adlt_disc,0)}}</td>
                                <td align="center">{{number_format($budget->irf_disc,0)}}</td>
                                <td align="center">{{number_format($budget->nicu_disc),0}}</td>
                                <td align="center">{{number_format($budget->norm_nb_disc,0)}}</td>
                                <td align="center">{{number_format($budget->total_disc,0)}}</td>
                                <td align="center">{{number_format($budget->total_er_visits,0)}}</td>
                                <td align="center">{{number_format($budget->er_admits,0)}}</td>
                                <td align="center">{{number_format($budget->er_obs,0)}}</td>
                                <td align="center">{{number_format($budget->transfers_from_er,0)}}</td>
                                <td align="center">{{number_format($budget->gi_lab,0)}}</td>
                                <td align="center">{{number_format($budget->cath_lab,0)}}</td>
                                <td align="center">{{number_format($budget->main_or,0)}}</td>
                                <td align="center">{{number_format($budget->asc_or,0)}}</td>
                                <td align="center">{{number_format($budget->main_op,0)}}</td>
                                <td align="center">{{number_format($budget->main_ip,0)}}</td>
                                <td align="center">{{number_format($budget->total_op,0)}}</td>
                                <td align="center">{{number_format($budget->total_sur,0)}}</td>
                                <td align="center">{{number_format($budget->mri_proc,0)}}</td>
                                <td align="center">{{number_format($budget->ct_proc,0)}}</td>
                                <td align="center">{{number_format($budget->mammo_proc,0)}}</td>
                                <td align="center">{{number_format($budget->pet_proc,0)}}</td>
                            </tr>
                            <tr>
                                <td colspan="4"><b>MTD Budget</b></td>
                                <td align="center">${{number_format(($budget->total_p_r/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($budget->a_p_days/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($budget->irf_days/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($budget->nicu_days/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format((($budget->nicu_days+$budget->irf_days+$budget->a_p_days)/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($budget->adlt_disc/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($budget->irf_disc/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($budget->nicu_disc/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($budget->norm_nb_disc/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($budget->total_disc/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($budget->total_er_visits/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($budget->er_admits/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($budget->er_obs/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($budget->transfers_from_er/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($budget->gi_lab/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($budget->cath_lab/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($budget->main_or/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($budget->asc_or/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($budget->main_op/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($budget->main_ip/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($budget->total_op/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($budget->total_sur/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($budget->mri_proc/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($budget->ct_proc/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($budget->mammo_proc/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($budget->pet_proc/$days_in_month)*$day_of_month,0)}}</td>
                            </tr>
                            <tr>
                                <td colspan="4"><b>Variance from Budget</b></td>

                                <td align="center">${{number_format(($month_summary->sum('h_r')+$month_summary->sum('p_b_r'))-(($budget->total_p_r/$days_in_month)*$day_of_month),0)}}</td>
                                <td align="center">{{number_format($month_summary->sum('a_p_days')-($budget->a_p_days/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format($month_summary->sum('irf_days')-($budget->irf_days/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format($month_summary->sum('nicu_days')-($budget->nicu_days/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($month_summary->sum('nicu_days')+$month_summary->sum('irf_days')+$month_summary->sum('a_p_days'))-(($budget->nicu_days+$budget->irf_days+$budget->a_p_days)/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($month_summary->sum('adlt_disc')-($budget->adlt_disc/$days_in_month)*$day_of_month),0)}}</td>
                                <td align="center">{{number_format(($month_summary->sum('irf_disc'))-($budget->irf_disc/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($month_summary->sum('nicu_disc'))-($budget->nicu_disc/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($month_summary->sum('norm_nb_disc'))-($budget->norm_nb_disc/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($month_summary->sum('total_disc'))-($budget->total_disc/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($month_summary->sum('total_er_visits'))-($budget->total_er_visits/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($month_summary->sum('er_admits'))-($budget->er_admits/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($month_summary->sum('er_obs'))-($budget->er_obs/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($month_summary->sum('transfers_from_er'))-($budget->transfers_from_er/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($month_summary->sum('gi_lab'))-($budget->gi_lab/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($month_summary->sum('cath_lab'))-($budget->cath_lab/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($month_summary->sum('main_or'))-($budget->main_or/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($month_summary->sum('asc_or'))-($budget->asc_or/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($month_summary->sum('main_op'))-($budget->main_op/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($month_summary->sum('main_ip'))-($budget->main_ip/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($month_summary->sum('total_op'))-($budget->total_op/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($month_summary->sum('total_sur'))-($budget->total_sur/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($month_summary->sum('mri_proc'))-($budget->mri_proc/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($month_summary->sum('ct_proc'))-($budget->ct_proc/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($month_summary->sum('mammo_proc'))-($budget->mammo_proc/$days_in_month)*$day_of_month,0)}}</td>
                                <td align="center">{{number_format(($month_summary->sum('pet_proc'))-($budget->pet_proc/$days_in_month)*$day_of_month,0)}}</td>
                            </tr>

                            <tr>
                                <td colspan="4"><b>% Variance from Budget</b></td>

                                <td align="center"
                                    @if(((($month_summary->sum('h_r')+$month_summary->sum('p_b_r'))-(($budget->total_p_r/$days_in_month)*$day_of_month))/(($budget->total_p_r/$days_in_month)*$day_of_month))*100 <0) bgcolor="#ff9e9e"
                                    @else bgcolor="#9effa1"
                                    @endif>
                                    {{number_format(((($month_summary->sum('h_r')+$month_summary->sum('p_b_r'))-(($budget->total_p_r/$days_in_month)*$day_of_month))/(($budget->total_p_r/$days_in_month)*$day_of_month))*100,1)}}%
                                </td>

                                <td align="center"
                                    @if((($month_summary->sum('a_p_days')-($budget->a_p_days/$days_in_month)*$day_of_month)/(($budget->a_p_days/$days_in_month)*$day_of_month))*100 <0) bgcolor="#ff9e9e"
                                    @else bgcolor="#9effa1"
                                    @endif
                                >{{number_format((($month_summary->sum('a_p_days')-($budget->a_p_days/$days_in_month)*$day_of_month)/(($budget->a_p_days/$days_in_month)*$day_of_month))*100,1)}}%
                                </td>

                                <td align="center"
                                    @if((($month_summary->sum('irf_days')-($budget->irf_days/$days_in_month)*$day_of_month)/(($budget->irf_days/$days_in_month)*$day_of_month))*100 <0) bgcolor="#ff9e9e"
                                    @else bgcolor="#9effa1"
                                    @endif
                                >{{number_format((($month_summary->sum('irf_days')-($budget->irf_days/$days_in_month)*$day_of_month)/(($budget->irf_days/$days_in_month)*$day_of_month))*100,1)}}%
                                </td>

                                <td align="center"
                                    @if((($month_summary->sum('nicu_days')-($budget->nicu_days/$days_in_month)*$day_of_month)/(($budget->nicu_days/$days_in_month)*$day_of_month))*100 <0) bgcolor="#ff9e9e"
                                    @else bgcolor="#9effa1"
                                    @endif
                                >{{number_format((($month_summary->sum('nicu_days')-($budget->nicu_days/$days_in_month)*$day_of_month)/(($budget->nicu_days/$days_in_month)*$day_of_month))*100,1)}}%
                                </td>

                                <td align="center"
                                    @if(((($month_summary->sum('nicu_days')+$month_summary->sum('irf_days')+$month_summary->sum('a_p_days'))-(($budget->nicu_days+$budget->irf_days+$budget->a_p_days)/$days_in_month)*$day_of_month)/((($budget->nicu_days+$budget->irf_days+$budget->a_p_days)/$days_in_month)*$day_of_month))*100 <0) bgcolor="#ff9e9e"
                                    @else bgcolor="#9effa1"
                                    @endif
                                >{{number_format(((($month_summary->sum('nicu_days')+$month_summary->sum('irf_days')+$month_summary->sum('a_p_days'))-(($budget->nicu_days+$budget->irf_days+$budget->a_p_days)/$days_in_month)*$day_of_month)/((($budget->nicu_days+$budget->irf_days+$budget->a_p_days)/$days_in_month)*$day_of_month))*100,1)}}%
                                </td>

                                <td align="center"
                                    @if(((($month_summary->sum('adlt_disc')-($budget->adlt_disc/$days_in_month)*$day_of_month))/(($budget->adlt_disc/$days_in_month)*$day_of_month))*100 <0) bgcolor="#ff9e9e"
                                    @else bgcolor="#9effa1"
                                    @endif
                                >{{number_format(((($month_summary->sum('adlt_disc')-($budget->adlt_disc/$days_in_month)*$day_of_month))/(($budget->adlt_disc/$days_in_month)*$day_of_month))*100,1)}}%
                                </td>

                                <td align="center"
                                    @if(((($month_summary->sum('irf_disc'))-($budget->irf_disc/$days_in_month)*$day_of_month)/(($budget->irf_disc/$days_in_month)*$day_of_month))*100 <0) bgcolor="#ff9e9e"
                                    @else bgcolor="#9effa1"
                                    @endif
                                >{{number_format(((($month_summary->sum('irf_disc'))-($budget->irf_disc/$days_in_month)*$day_of_month)/(($budget->irf_disc/$days_in_month)*$day_of_month))*100,1)}}%
                                </td>

                                <td align="center"
                                    @if(((($month_summary->sum('nicu_disc'))-($budget->nicu_disc/$days_in_month)*$day_of_month)/(($budget->nicu_disc/$days_in_month)*$day_of_month))*100 <0) bgcolor="#ff9e9e"
                                    @else bgcolor="#9effa1"
                                    @endif
                                >{{number_format(((($month_summary->sum('nicu_disc'))-($budget->nicu_disc/$days_in_month)*$day_of_month)/(($budget->nicu_disc/$days_in_month)*$day_of_month))*100,1)}}%
                                </td>

                                <td align="center"
                                    @if((($month_summary->sum('norm_nb_disc'))-($budget->norm_nb_disc/$days_in_month)*$day_of_month)/(($budget->norm_nb_disc/$days_in_month)*$day_of_month)*100 <0) bgcolor="#ff9e9e"
                                    @else bgcolor="#9effa1"
                                    @endif
                                >{{number_format((($month_summary->sum('norm_nb_disc'))-($budget->norm_nb_disc/$days_in_month)*$day_of_month)/(($budget->norm_nb_disc/$days_in_month)*$day_of_month)*100,1)}}%
                                </td>

                                <td align="center"
                                    @if((($month_summary->sum('total_disc'))-($budget->total_disc/$days_in_month)*$day_of_month)/(($budget->total_disc/$days_in_month)*$day_of_month)*100 <0) bgcolor="#ff9e9e"
                                    @else bgcolor="#9effa1"
                                    @endif
                                >{{number_format((($month_summary->sum('total_disc'))-($budget->total_disc/$days_in_month)*$day_of_month)/(($budget->total_disc/$days_in_month)*$day_of_month)*100,1)}}%
                                </td>

                                <td align="center"
                                    @if((($month_summary->sum('total_er_visits'))-($budget->total_er_visits/$days_in_month)*$day_of_month)/(($budget->total_er_visits/$days_in_month)*$day_of_month)*100 <0) bgcolor="#ff9e9e"
                                    @else bgcolor="#9effa1"
                                    @endif
                                >{{number_format((($month_summary->sum('total_er_visits'))-($budget->total_er_visits/$days_in_month)*$day_of_month)/(($budget->total_er_visits/$days_in_month)*$day_of_month)*100,1)}}%
                                </td>

                                <td align="center"
                                    @if(((($month_summary->sum('er_admits'))-($budget->er_admits/$days_in_month)*$day_of_month)/(($budget->er_admits/$days_in_month)*$day_of_month))*100 <0) bgcolor="#ff9e9e"
                                    @else bgcolor="#9effa1"
                                    @endif
                                >{{number_format(((($month_summary->sum('er_admits'))-($budget->er_admits/$days_in_month)*$day_of_month)/(($budget->er_admits/$days_in_month)*$day_of_month))*100,1)}}%
                                </td>

                                <td align="center"
                                    @if(((($month_summary->sum('er_obs'))-($budget->er_obs/$days_in_month)*$day_of_month)/(($budget->er_obs/$days_in_month)*$day_of_month))*100 <0) bgcolor="#ff9e9e"
                                    @else bgcolor="#9effa1"
                                    @endif
                                >{{number_format(((($month_summary->sum('er_obs'))-($budget->er_obs/$days_in_month)*$day_of_month)/(($budget->er_obs/$days_in_month)*$day_of_month))*100,1)}}%
                                </td>

                                <td align="center"
                                    @if((($month_summary->sum('transfers_from_er'))-($budget->transfers_from_er/$days_in_month)*$day_of_month)-(($budget->transfers_from_er/$days_in_month)*$day_of_month)*100 <0) bgcolor="#ff9e9e"
                                    @else bgcolor="#9effa1"
                                    @endif
                                >{{number_format((($month_summary->sum('transfers_from_er'))-($budget->transfers_from_er/$days_in_month)*$day_of_month)/(($budget->transfers_from_er/$days_in_month)*$day_of_month)*100,1)}}%
                                </td>

                                <td align="center"
                                    @if(((($month_summary->sum('gi_lab'))-($budget->gi_lab/$days_in_month)*$day_of_month)/(($budget->gi_lab/$days_in_month)*$day_of_month))*100 <0) bgcolor="#ff9e9e"
                                    @else bgcolor="#9effa1"
                                    @endif
                                >{{number_format(((($month_summary->sum('gi_lab'))-($budget->gi_lab/$days_in_month)*$day_of_month)/(($budget->gi_lab/$days_in_month)*$day_of_month))*100,1)}}%
                                </td>

                                <td align="center"
                                    @if(((($month_summary->sum('cath_lab'))-($budget->cath_lab/$days_in_month)*$day_of_month)/(($budget->cath_lab/$days_in_month)*$day_of_month))*100 <0) bgcolor="#ff9e9e"
                                    @else bgcolor="#9effa1"
                                    @endif
                                >{{number_format(((($month_summary->sum('cath_lab'))-($budget->cath_lab/$days_in_month)*$day_of_month)/(($budget->cath_lab/$days_in_month)*$day_of_month))*100,1)}}%
                                </td>

                                <td align="center"
                                    @if((($month_summary->sum('main_or'))-($budget->main_or/$days_in_month)*$day_of_month)/(($budget->main_or/$days_in_month)*$day_of_month)*100 <0) bgcolor="#ff9e9e"
                                    @else bgcolor="#9effa1"
                                    @endif
                                >{{number_format((($month_summary->sum('main_or'))-($budget->main_or/$days_in_month)*$day_of_month)/(($budget->main_or/$days_in_month)*$day_of_month)*100,1)}}%
                                </td>

                                <td align="center"

                                >
                                </td>

                                <td align="center"
                                    @if((($month_summary->sum('main_op'))-($budget->main_op/$days_in_month)*$day_of_month)/(($budget->main_op/$days_in_month)*$day_of_month)*100 <0) bgcolor="#ff9e9e"
                                    @else bgcolor="#9effa1"
                                    @endif
                                >{{number_format((($month_summary->sum('main_op'))-($budget->main_op/$days_in_month)*$day_of_month)/(($budget->main_op/$days_in_month)*$day_of_month)*100,1)}}%
                                </td>

                                <td align="center"
                                    @if((($month_summary->sum('main_ip'))-($budget->main_ip/$days_in_month)*$day_of_month)/(($budget->main_ip/$days_in_month)*$day_of_month)*100 <0) bgcolor="#ff9e9e"
                                    @else bgcolor="#9effa1"
                                    @endif
                                >{{number_format((($month_summary->sum('main_ip'))-($budget->main_ip/$days_in_month)*$day_of_month)/(($budget->main_ip/$days_in_month)*$day_of_month)*100,1)}}%
                                </td>

                                <td align="center"
                                    @if((($month_summary->sum('total_op'))-($budget->total_op/$days_in_month)*$day_of_month)/(($budget->total_op/$days_in_month)*$day_of_month)*100 <0) bgcolor="#ff9e9e"
                                    @else bgcolor="#9effa1"
                                    @endif
                                >{{number_format((($month_summary->sum('total_op'))-($budget->total_op/$days_in_month)*$day_of_month)/(($budget->total_op/$days_in_month)*$day_of_month)*100,1)}}%
                                </td>

                                <td align="center"
                                    @if((($month_summary->sum('total_sur'))-($budget->total_sur/$days_in_month)*$day_of_month)/(($budget->total_sur/$days_in_month)*$day_of_month)*100 <0) bgcolor="#ff9e9e"
                                    @else bgcolor="#9effa1"
                                    @endif
                                >{{number_format((($month_summary->sum('total_sur'))-($budget->total_sur/$days_in_month)*$day_of_month)/(($budget->total_sur/$days_in_month)*$day_of_month)*100,1)}}%
                                </td>

                                <td align="center"
                                    @if((($month_summary->sum('mri_proc'))-($budget->mri_proc/$days_in_month)*$day_of_month)/(($budget->mri_proc/$days_in_month)*$day_of_month)*100 <0) bgcolor="#ff9e9e"
                                    @else bgcolor="#9effa1"
                                    @endif
                                >{{number_format((($month_summary->sum('mri_proc'))-($budget->mri_proc/$days_in_month)*$day_of_month)/(($budget->mri_proc/$days_in_month)*$day_of_month)*100,1)}}%
                                </td>

                                <td align="center"
                                    @if((($month_summary->sum('mri_proc'))-($budget->mri_proc/$days_in_month)*$day_of_month)/(($budget->mri_proc/$days_in_month)*$day_of_month)*100 <0) bgcolor="#ff9e9e"
                                    @else bgcolor="#9effa1"
                                    @endif
                                >{{number_format((($month_summary->sum('ct_proc'))-($budget->ct_proc/$days_in_month)*$day_of_month)/(($budget->ct_proc/$days_in_month)*$day_of_month)*100,1)}}%
                                </td>

                                <td align="center"
                                    @if((($month_summary->sum('mammo_proc'))-($budget->mammo_proc/$days_in_month)*$day_of_month)/(($budget->mammo_proc/$days_in_month)*$day_of_month)*100 <0) bgcolor="#ff9e9e"
                                    @else bgcolor="#9effa1"
                                    @endif
                                >{{number_format((($month_summary->sum('mammo_proc'))-($budget->mammo_proc/$days_in_month)*$day_of_month)/(($budget->mammo_proc/$days_in_month)*$day_of_month)*100,1)}}%
                                </td>

                                <td align="center"
                                    @if((($month_summary->sum('pet_proc'))-($budget->pet_proc/$days_in_month)*$day_of_month)/(($budget->pet_proc/$days_in_month)*$day_of_month)*100 <0) bgcolor="#ff9e9e"
                                    @else bgcolor="#9effa1"
                                    @endif
                                >{{number_format((($month_summary->sum('pet_proc'))-($budget->pet_proc/$days_in_month)*$day_of_month)/(($budget->pet_proc/$days_in_month)*$day_of_month)*100,1)}}%</td>
                            </tr>
                            @endif
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="4"></td>
                                <td align="center" bgcolor="#c9d3ee"><b>Total Patient Revenue</b></td>
                                <td align="center"><b>ADLT/PED Days</b></td>
                                <td align="center"><b>IRF Days</b></td>
                                <td align="center"><b>NICU Days</b></td>
                                <td align="center" bgcolor="#c9d3ee"><b>Total Days</b></td>
                                <td align="center"><b>ADLT/PED Disc</b></td>
                                <td align="center"><b>IFR Disc</b></td>
                                <td align="center"><b>NICU Disc</b></td>
                                <td align="center"><b>Newborn Disc</b></td>
                                <td align="center" bgcolor="#c9d3ee"><b>Total Disc</b></td>
                                <td align="center" bgcolor="#c9d3ee"><b>Total ER Visits</b></td>
                                <td align="center"><b>ER Admits to Bed</b></td>
                                <td align="center"><b>ER Admits to OBS</b></td>
                                <td align="center"><b>Transfers from ER</b></td>
                                <td align="center"><b>GI Lab Proc</b></td>
                                <td align="center"><b>Cath Lab Proc</b></td>
                                <td align="center"><b>SNH Main OR</b></td>
                                <td align="center"><b>ASC</b></td>
                                <td align="center"><b>SGMC Main OP</b></td>
                                <td align="center"><b>SGMC Main IP</b></td>
                                <td align="center" bgcolor="#c9d3ee"><b>Total OP</b></td>
                                <td align="center" bgcolor="#c9d3ee"><b>Total SURG</b></td>
                                <td align="center"><b>MRI Proc</b></td>
                                <td align="center"><b>CT Proc</b></td>
                                <td align="center"><b>Mammo PROC</b></td>
                                <td align="center"><b>PET Proc</b></td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>

                </div>
            </div>
        </div>
        <div class="item">
            <div class="col-lg-12">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <td><b>Date</b></td>
                        <td><b>Editor</b></td>
                        <td><b>Comments</b></td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>9/10/2018</td>
                        <td>Bryan McRee</td>
                        <td>Finished implementing the budget into the spreadsheet.</td>
                    </tr>
                    <tr>
                        <td>9/11/2018</td>
                        <td>Bryan McRee</td>
                        <td>Adding conditional formatting to prevent division by zero errors and correctly calculate the % change.  Tested with IRF discharges however none of the data landed where an error could be thrown.</td>
                    </tr>
                    <tr>
                        <td>9/13/2018</td>
                        <td>Bryan McRee</td>
                        <td>Math error in adult/ped discharges.  Fixed</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

            <!-- Left and right controls -->

</div>
    @endif


@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">

//This is the function that allows to click to screen
    function goToSlide(number) {
        $("#myCarousel").carousel(number);
    }
//speed of the scroll
    $('#myCarousel').carousel({
        interval: 20000
    });

    $('#playButton').click(function () {
        $('#myCarousel').carousel('cycle');
        $('#message').html('<div class="alert alert-success fade">Scrolling Resumed</div>');
        $(".alert").delay(200).addClass("in").fadeOut(2000);
    });
    $('#pauseButton').click(function () {
        $('#myCarousel').carousel('pause');
        $('#message').html('<div class="alert alert-danger fade">Scrolling Paused</div>');
        $(".alert").delay(200).addClass("in").fadeOut(2000);
    });



    new Chart(document.getElementById("monthly_hr"), {
        type: 'line',
        data: {
            labels: [
                @foreach($month_summary as $month)
                "{{$month->report_date->format('m-d-Y')}}",
                @endforeach

            ],
            datasets: [{
                data: [

                    @foreach($month_summary as $month)
                        {{$month->h_r}},
                    @endforeach

                ],
                label: "Dollars",
                borderColor: "#3e95cd",
                borderWidth: 1,
                fill: true,
                //backgroundColor: "rgba(200,132,153,.5)",
                //backgroundColor: "rgba(83,132,153,.5)",
                pointHoverRadius: 5,
                pointHoverBackgroundColor: 'black'
            }
            ]
        },
        options: {
            title: {
                display: false,
                text: 'World population per region (in millions)'
            },
            legend: {
                display: false
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem) {
                        return tooltipItem.yLabel;
                    }
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        display: false
                    },
                }],
                xAxes: [{
                    ticks: {
                        display: false
                    }
                }],
            }
        }
    });

    new Chart(document.getElementById("monthly_pb"), {
        type: 'line',
        data: {
            labels: [
                @foreach($month_summary as $month_pb)
                    "{{$month_pb->report_date->format('m-d-Y')}}",
                @endforeach

            ],
            datasets: [{
                data: [

                    @foreach($month_summary as $month_pb)
                    {{$month_pb->p_b_r}},
                    @endforeach

                ],
                label: "Dollars",
                borderColor: "#3e95cd",
                borderWidth: 1,
                fill: true,
                //backgroundColor: "rgba(200,132,153,.5)",
                //backgroundColor: "rgba(83,132,153,.5)",
                pointHoverRadius: 5,
                pointHoverBackgroundColor: 'black'
            }
            ]
        },
        options: {
            title: {
                display: false,
                text: 'World population per region (in millions)'
            },
            legend: {
                display: false
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem) {
                        return tooltipItem.yLabel;
                    }
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        display: false
                    }
                }],
                xAxes: [{
                    ticks: {
                        display: false
                    }
                }],
            }
        }
    });

    new Chart(document.getElementById("total_rev"), {
        type: 'line',
        data: {
            labels: [
                @foreach($month_summary as $month_pb)
                    "{{$month_pb->report_date->format('m-d-Y')}}",
                @endforeach

            ],
            datasets: [{
                data: [

                    @foreach($month_summary as $month_pb)
                    {{$month_pb->total_p_r}},
                    @endforeach

                ],
                label: "Dollars",
                borderColor: "#3e95cd",
                borderWidth: 1,
                fill: true,
                backgroundColor: "rgba(83,132,153,.5)",
                pointHoverRadius: 5,
                pointHoverBackgroundColor: 'black'
            }
            ]
        },
        options: {
            title: {
                display: false,
                text: 'World population per region (in millions)'
            },
            legend: {
                display: false
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem) {
                        return tooltipItem.yLabel;
                    }
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        display: false
                    }
                }],
                xAxes: [{
                    ticks: {
                        display: false
                    }
                }],
            }
        }
    });


    new Chart(document.getElementById("a_p_days"), {
        type: 'line',
        data: {
            labels: [
                @foreach($month_summary as $month_pb)
                    "{{$month_pb->report_date->format('m-d-Y')}}",
                @endforeach

            ],
            datasets: [{
                data: [

                    @foreach($month_summary as $month_pb)
                    {{$month_pb->a_p_days}},
                    @endforeach

                ],
                label: "Dollars",
                borderColor: "#3e95cd",
                borderWidth: 1,
                fill: true,
               //backgroundColor: "rgba(83,132,153,.5)",
                pointHoverRadius: 5,
                pointHoverBackgroundColor: 'black'
            }
            ]
        },
        options: {
            title: {
                display: false,
                text: 'World population per region (in millions)'
            },
            legend: {
                display: false
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem) {
                        return tooltipItem.yLabel;
                    }
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        display: false
                    }
                }],
                xAxes: [{
                    ticks: {
                        display: false
                    }
                }],
            }
        }
    });


    new Chart(document.getElementById("irf_days"), {
        type: 'line',
        data: {
            labels: [
                @foreach($month_summary as $month_pb)
                    "{{$month_pb->report_date->format('m-d-Y')}}",
                @endforeach

            ],
            datasets: [{
                data: [

                    @foreach($month_summary as $month_pb)
                    {{$month_pb->irf_days}},
                    @endforeach

                ],
                label: "Dollars",
                borderColor: "#3e95cd",
                borderWidth: 1,
                fill: true,
                //backgroundColor: "rgba(83,132,153,.5)",
                pointHoverRadius: 5,
                pointHoverBackgroundColor: 'black'
            }
            ]
        },
        options: {
            title: {
                display: false,
                text: 'World population per region (in millions)'
            },
            legend: {
                display: false
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem) {
                        return tooltipItem.yLabel;
                    }
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        display: false
                    }
                }],
                xAxes: [{
                    ticks: {
                        display: false
                    }
                }],
            }
        }
    });

    new Chart(document.getElementById("nicu_days"), {
        type: 'line',
        data: {
            labels: [
                @foreach($month_summary as $month_pb)
                    "{{$month_pb->report_date->format('m-d-Y')}}",
                @endforeach

            ],
            datasets: [{
                data: [

                    @foreach($month_summary as $month_pb)
                    {{$month_pb->nicu_days}},
                    @endforeach

                ],
                label: "Dollars",
                borderColor: "#3e95cd",
                borderWidth: 1,
                fill: true,
                //backgroundColor: "rgba(83,132,153,.5)",
                pointHoverRadius: 5,
                pointHoverBackgroundColor: 'black'
            }
            ]
        },
        options: {
            title: {
                display: false,
                text: 'World population per region (in millions)'
            },
            legend: {
                display: false
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem) {
                        return tooltipItem.yLabel;
                    }
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        display: false
                    }
                }],
                xAxes: [{
                    ticks: {
                        display: false
                    }
                }],
            }
        }
    });

    new Chart(document.getElementById("total_days"), {
        type: 'line',
        data: {
            labels: [
                @foreach($month_summary as $month_pb)
                    "{{$month_pb->report_date->format('m-d-Y')}}",
                @endforeach

            ],
            datasets: [{
                data: [

                    @foreach($month_summary as $month_pb)
                    {{$month_pb->total_days}},
                    @endforeach

                ],
                label: "Dollars",
                borderColor: "#3e95cd",
                borderWidth: 1,
                fill: true,
                backgroundColor: "rgba(200,132,153,.5)",
                //backgroundColor: "rgba(83,132,153,.5)",
                pointHoverRadius: 5,
                pointHoverBackgroundColor: 'black'
            }
            ]
        },
        options: {
            title: {
                display: false,
                text: 'World population per region (in millions)'
            },
            legend: {
                display: false
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem) {
                        return tooltipItem.yLabel;
                    }
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        display: false
                    }
                }],
                xAxes: [{
                    ticks: {
                        display: false
                    }
                }],
            }
        }
    });


    new Chart(document.getElementById("adlt_disc"), {
        type: 'line',
        data: {
            labels: [
                @foreach($month_summary as $month_pb)
                    "{{$month_pb->report_date->format('m-d-Y')}}",
                @endforeach

            ],
            datasets: [{
                data: [

                    @foreach($month_summary as $month_pb)
                    {{$month_pb->adlt_disc}},
                    @endforeach

                ],
                label: "Dollars",
                borderColor: "#3e95cd",
                borderWidth: 1,
                fill: true,
                //backgroundColor: "rgba(83,132,153,.5)",
                pointHoverRadius: 5,
                pointHoverBackgroundColor: 'black'
            }
            ]
        },
        options: {
            title: {
                display: false,
                text: 'World population per region (in millions)'
            },
            legend: {
                display: false
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem) {
                        return tooltipItem.yLabel;
                    }
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        display: false
                    }
                }],
                xAxes: [{
                    ticks: {
                        display: false
                    }
                }],
            }
        }
    });


    new Chart(document.getElementById("irf_disc"), {
        type: 'line',
        data: {
            labels: [
                @foreach($month_summary as $month_pb)
                    "{{$month_pb->report_date->format('m-d-Y')}}",
                @endforeach

            ],
            datasets: [{
                data: [

                    @foreach($month_summary as $month_pb)
                    {{$month_pb->irf_disc}},
                    @endforeach

                ],
                label: "Dollars",
                borderColor: "#3e95cd",
                borderWidth: 1,
                fill: true,
                //backgroundColor: "rgba(83,132,153,.5)",
                pointHoverRadius: 5,
                pointHoverBackgroundColor: 'black'
            }
            ]
        },
        options: {
            title: {
                display: false,
                text: 'World population per region (in millions)'
            },
            legend: {
                display: false
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem) {
                        return tooltipItem.yLabel;
                    }
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        display: false
                    }
                }],
                xAxes: [{
                    ticks: {
                        display: false
                    }
                }],
            }
        }
    });

    new Chart(document.getElementById("nicu_disc"), {
        type: 'line',
        data: {
            labels: [
                @foreach($month_summary as $month_pb)
                    "{{$month_pb->report_date->format('m-d-Y')}}",
                @endforeach

            ],
            datasets: [{
                data: [

                    @foreach($month_summary as $month_pb)
                    {{$month_pb->nicu_disc}},
                    @endforeach

                ],
                label: "Dollars",
                borderColor: "#3e95cd",
                borderWidth: 1,
                fill: true,
                //backgroundColor: "rgba(83,132,153,.5)",
                pointHoverRadius: 5,
                pointHoverBackgroundColor: 'black'
            }
            ]
        },
        options: {
            title: {
                display: false,
                text: 'World population per region (in millions)'
            },
            legend: {
                display: false
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem) {
                        return tooltipItem.yLabel;
                    }
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        display: false
                    }
                }],
                xAxes: [{
                    ticks: {
                        display: false
                    }
                }],
            }
        }
    });

    new Chart(document.getElementById("total_disc"), {
        type: 'line',
        data: {
            labels: [
                @foreach($month_summary as $month_pb)
                    "{{$month_pb->report_date->format('m-d-Y')}}",
                @endforeach

            ],
            datasets: [{
                data: [

                    @foreach($month_summary as $month_pb)
                    {{$month_pb->total_disc}},
                    @endforeach

                ],
                label: "Dollars",
                borderColor: "#3e95cd",
                borderWidth: 1,
                fill: true,
                //backgroundColor: "rgba(83,132,153,.5)",
                pointHoverRadius: 5,
                pointHoverBackgroundColor: 'black'
            }
            ]
        },
        options: {
            title: {
                display: false,
                text: 'World population per region (in millions)'
            },
            legend: {
                display: false
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem) {
                        return tooltipItem.yLabel;
                    }
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        display: false
                    }
                }],
                xAxes: [{
                    ticks: {
                        display: false
                    }
                }],
            }
        }
    });

    new Chart(document.getElementById("norm_nb_disc"), {
        type: 'line',
        data: {
            labels: [
                @foreach($month_summary as $month_pb)
                    "{{$month_pb->report_date->format('m-d-Y')}}",
                @endforeach

            ],
            datasets: [{
                data: [

                    @foreach($month_summary as $month_pb)
                    {{$month_pb->norm_nb_disc}},
                    @endforeach

                ],
                label: "Dollars",
                borderColor: "#3e95cd",
                borderWidth: 1,
                fill: true,
                //backgroundColor: "rgba(83,132,153,.5)",
                pointHoverRadius: 5,
                pointHoverBackgroundColor: 'black'
            }
            ]
        },
        options: {
            title: {
                display: false,
                text: 'World population per region (in millions)'
            },
            legend: {
                display: false
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem) {
                        return tooltipItem.yLabel;
                    }
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        display: false
                    }
                }],
                xAxes: [{
                    ticks: {
                        display: false
                    }
                }],
            }
        }
    });

new Chart(document.getElementById("total_er_visits"), {
    type: 'line',
    data: {
        labels: [
            @foreach($month_summary as $month_pb)
                "{{$month_pb->report_date->format('m-d-Y')}}",
            @endforeach

        ],
        datasets: [{
            data: [

                @foreach($month_summary as $month_pb)
                {{$month_pb->total_er_visits}},
                @endforeach

            ],
            label: "Dollars",
            borderColor: "#3e95cd",
            borderWidth: 1,
            fill: true,
            //backgroundColor: "rgba(83,132,153,.5)",
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'black'
        }
        ]
    },
    options: {
        title: {
            display: false,
            text: 'World population per region (in millions)'
        },
        legend: {
            display: false
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem) {
                    return tooltipItem.yLabel;
                }
            }
        },
        scales: {
            yAxes: [{
                ticks: {
                    display: false
                }
            }],
            xAxes: [{
                ticks: {
                    display: false
                }
            }],
        }
    }
});


new Chart(document.getElementById("er_admits"), {
    type: 'line',
    data: {
        labels: [
            @foreach($month_summary as $month_pb)
                "{{$month_pb->report_date->format('m-d-Y')}}",
            @endforeach

        ],
        datasets: [{
            data: [

                @foreach($month_summary as $month_pb)
                {{$month_pb->er_admits}},
                @endforeach

            ],
            label: "Dollars",
            borderColor: "#3e95cd",
            borderWidth: 1,
            fill: true,
            //backgroundColor: "rgba(83,132,153,.5)",
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'black'
        }
        ]
    },
    options: {
        title: {
            display: false,
            text: 'World population per region (in millions)'
        },
        legend: {
            display: false
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem) {
                    return tooltipItem.yLabel;
                }
            }
        },
        scales: {
            yAxes: [{
                ticks: {
                    display: false
                }
            }],
            xAxes: [{
                ticks: {
                    display: false
                }
            }],
        }
    }
});

new Chart(document.getElementById("er_obs"), {
    type: 'line',
    data: {
        labels: [
            @foreach($month_summary as $month_pb)
                "{{$month_pb->report_date->format('m-d-Y')}}",
            @endforeach

        ],
        datasets: [{
            data: [

                @foreach($month_summary as $month_pb)
                {{$month_pb->er_obs}},
                @endforeach

            ],
            label: "Dollars",
            borderColor: "#3e95cd",
            borderWidth: 1,
            fill: true,
            //backgroundColor: "rgba(83,132,153,.5)",
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'black'
        }
        ]
    },
    options: {
        title: {
            display: false,
            text: 'World population per region (in millions)'
        },
        legend: {
            display: false
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem) {
                    return tooltipItem.yLabel;
                }
            }
        },
        scales: {
            yAxes: [{
                ticks: {
                    display: false
                }
            }],
            xAxes: [{
                ticks: {
                    display: false
                }
            }],
        }
    }
});

new Chart(document.getElementById("transfers_from_er"), {
    type: 'line',
    data: {
        labels: [
            @foreach($month_summary as $month_pb)
                "{{$month_pb->report_date->format('m-d-Y')}}",
            @endforeach

        ],
        datasets: [{
            data: [

                @foreach($month_summary as $month_pb)
                {{$month_pb->transfers_from_er}},
                @endforeach

            ],
            label: "Dollars",
            borderColor: "#3e95cd",
            borderWidth: 1,
            fill: true,
            //backgroundColor: "rgba(83,132,153,.5)",
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'black'
        }
        ]
    },
    options: {
        title: {
            display: false,
            text: 'World population per region (in millions)'
        },
        legend: {
            display: false
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem) {
                    return tooltipItem.yLabel;
                }
            }
        },
        scales: {
            yAxes: [{
                ticks: {
                    display: false
                }
            }],
            xAxes: [{
                ticks: {
                    display: false
                }
            }],
        }
    }
});

new Chart(document.getElementById("gi_lab"), {
    type: 'line',
    data: {
        labels: [
            @foreach($month_summary as $month_pb)
                "{{$month_pb->report_date->format('m-d-Y')}}",
            @endforeach

        ],
        datasets: [{
            data: [

                @foreach($month_summary as $month_pb)
                {{$month_pb->gi_lab}},
                @endforeach

            ],
            label: "Dollars",
            borderColor: "#3e95cd",
            borderWidth: 1,
            fill: true,
            //backgroundColor: "rgba(83,132,153,.5)",
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'black'
        }
        ]
    },
    options: {
        title: {
            display: false,
            text: 'World population per region (in millions)'
        },
        legend: {
            display: false
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem) {
                    return tooltipItem.yLabel;
                }
            }
        },
        scales: {
            yAxes: [{
                ticks: {
                    display: false
                }
            }],
            xAxes: [{
                ticks: {
                    display: false
                }
            }],
        }
    }
});

new Chart(document.getElementById("cath_lab"), {
    type: 'line',
    data: {
        labels: [
            @foreach($month_summary as $month_pb)
                "{{$month_pb->report_date->format('m-d-Y')}}",
            @endforeach

        ],
        datasets: [{
            data: [

                @foreach($month_summary as $month_pb)
                {{$month_pb->cath_lab}},
                @endforeach

            ],
            label: "Dollars",
            borderColor: "#3e95cd",
            borderWidth: 1,
            fill: true,
            //backgroundColor: "rgba(83,132,153,.5)",
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'black'
        }
        ]
    },
    options: {
        title: {
            display: false,
            text: 'World population per region (in millions)'
        },
        legend: {
            display: false
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem) {
                    return tooltipItem.yLabel;
                }
            }
        },
        scales: {
            yAxes: [{
                ticks: {
                    display: false
                }
            }],
            xAxes: [{
                ticks: {
                    display: false
                }
            }],
        }
    }
});

new Chart(document.getElementById("main_or"), {
    type: 'line',
    data: {
        labels: [
            @foreach($month_summary as $month_pb)
                "{{$month_pb->report_date->format('m-d-Y')}}",
            @endforeach

        ],
        datasets: [{
            data: [

                @foreach($month_summary as $month_pb)
                {{$month_pb->main_or}},
                @endforeach

            ],
            label: "Dollars",
            borderColor: "#3e95cd",
            borderWidth: 1,
            fill: true,
            //backgroundColor: "rgba(83,132,153,.5)",
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'black'
        }
        ]
    },
    options: {
        title: {
            display: false,
            text: 'World population per region (in millions)'
        },
        legend: {
            display: false
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem) {
                    return tooltipItem.yLabel;
                }
            }
        },
        scales: {
            yAxes: [{
                ticks: {
                    display: false
                }
            }],
            xAxes: [{
                ticks: {
                    display: false
                }
            }],
        }
    }
});

new Chart(document.getElementById("total_op"), {
    type: 'line',
    data: {
        labels: [
            @foreach($month_summary as $month_pb)
                "{{$month_pb->report_date->format('m-d-Y')}}",
            @endforeach

        ],
        datasets: [{
            data: [

                @foreach($month_summary as $month_pb)
                {{$month_pb->total_op}},
                @endforeach

            ],
            label: "Dollars",
            borderColor: "#3e95cd",
            borderWidth: 1,
            fill: true,
            backgroundColor: "rgba(83,132,153,.5)",
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'black'
        }
        ]
    },
    options: {
        title: {
            display: false,
            text: 'World population per region (in millions)'
        },
        legend: {
            display: false
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem) {
                    return tooltipItem.yLabel;
                }
            }
        },
        scales: {
            yAxes: [{
                ticks: {
                    display: false
                }
            }],
            xAxes: [{
                ticks: {
                    display: false
                }
            }],
        }
    }
});

new Chart(document.getElementById("asc_or"), {
    type: 'line',
    data: {
        labels: [
            @foreach($month_summary as $month_pb)
                "{{$month_pb->report_date->format('m-d-Y')}}",
            @endforeach

        ],
        datasets: [{
            data: [

                @foreach($month_summary as $month_pb)
                {{$month_pb->asc_or}},
                @endforeach

            ],
            label: "Dollars",
            borderColor: "#3e95cd",
            borderWidth: 1,
            fill: true,
            //backgroundColor: "rgba(83,132,153,.5)",
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'black'
        }
        ]
    },
    options: {
        title: {
            display: false,
            text: 'World population per region (in millions)'
        },
        legend: {
            display: false
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem) {
                    return tooltipItem.yLabel;
                }
            }
        },
        scales: {
            yAxes: [{
                ticks: {
                    display: false
                }
            }],
            xAxes: [{
                ticks: {
                    display: false
                }
            }],
        }
    }
});

new Chart(document.getElementById("main_op"), {
    type: 'line',
    data: {
        labels: [
            @foreach($month_summary as $month_pb)
                "{{$month_pb->report_date->format('m-d-Y')}}",
            @endforeach

        ],
        datasets: [{
            data: [

                @foreach($month_summary as $month_pb)
                {{$month_pb->main_op}},
                @endforeach

            ],
            label: "Dollars",
            borderColor: "#3e95cd",
            borderWidth: 1,
            fill: true,
            //backgroundColor: "rgba(83,132,153,.5)",
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'black'
        }
        ]
    },
    options: {
        title: {
            display: false,
            text: 'World population per region (in millions)'
        },
        legend: {
            display: false
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem) {
                    return tooltipItem.yLabel;
                }
            }
        },
        scales: {
            yAxes: [{
                ticks: {
                    display: false
                }
            }],
            xAxes: [{
                ticks: {
                    display: false
                }
            }],
        }
    }
});

new Chart(document.getElementById("main_ip"), {
    type: 'line',
    data: {
        labels: [
            @foreach($month_summary as $month_pb)
                "{{$month_pb->report_date->format('m-d-Y')}}",
            @endforeach

        ],
        datasets: [{
            data: [

                @foreach($month_summary as $month_pb)
                {{$month_pb->main_ip}},
                @endforeach

            ],
            label: "Dollars",
            borderColor: "#3e95cd",
            borderWidth: 1,
            fill: true,
            //backgroundColor: "rgba(83,132,153,.5)",
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'black'
        }
        ]
    },
    options: {
        title: {
            display: false,
            text: 'World population per region (in millions)'
        },
        legend: {
            display: false
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem) {
                    return tooltipItem.yLabel;
                }
            }
        },
        scales: {
            yAxes: [{
                ticks: {
                    display: false
                }
            }],
            xAxes: [{
                ticks: {
                    display: false
                }
            }],
        }
    }
});

new Chart(document.getElementById("total_sur"), {
    type: 'line',
    data: {
        labels: [
            @foreach($month_summary as $month_pb)
                "{{$month_pb->report_date->format('m-d-Y')}}",
            @endforeach

        ],
        datasets: [{
            data: [

                @foreach($month_summary as $month_pb)
                {{$month_pb->total_sur}},
                @endforeach

            ],
            label: "Dollars",
            borderColor: "#3e95cd",
            borderWidth: 1,
            fill: true,
            //backgroundColor: "rgba(83,132,153,.5)",
            backgroundColor: "rgba(200,132,153,.5)",
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'black'
        }
        ]
    },
    options: {
        title: {
            display: false,
            text: 'World population per region (in millions)'
        },
        legend: {
            display: false
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem) {
                    return tooltipItem.yLabel;
                }
            }
        },
        scales: {
            yAxes: [{
                ticks: {
                    display: false
                }
            }],
            xAxes: [{
                ticks: {
                    display: false
                }
            }],
        }
    }
});

new Chart(document.getElementById("mri_proc"), {
    type: 'line',
    data: {
        labels: [
            @foreach($month_summary as $month_pb)
                "{{$month_pb->report_date->format('m-d-Y')}}",
            @endforeach

        ],
        datasets: [{
            data: [

                @foreach($month_summary as $month_pb)
                {{$month_pb->mri_proc}},
                @endforeach

            ],
            label: "Dollars",
            borderColor: "#3e95cd",
            borderWidth: 1,
            fill: true,
            //backgroundColor: "rgba(83,132,153,.5)",
            //backgroundColor: "rgba(200,132,153,.5)",
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'black'
        }
        ]
    },
    options: {
        title: {
            display: false,
            text: 'World population per region (in millions)'
        },
        legend: {
            display: false
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem) {
                    return tooltipItem.yLabel;
                }
            }
        },
        scales: {
            yAxes: [{
                ticks: {
                    display: false
                }
            }],
            xAxes: [{
                ticks: {
                    display: false
                }
            }],
        }
    }
});

new Chart(document.getElementById("ct_proc"), {
    type: 'line',
    data: {
        labels: [
            @foreach($month_summary as $month_pb)
                "{{$month_pb->report_date->format('m-d-Y')}}",
            @endforeach

        ],
        datasets: [{
            data: [

                @foreach($month_summary as $month_pb)
                {{$month_pb->ct_proc}},
                @endforeach

            ],
            label: "Dollars",
            borderColor: "#3e95cd",
            borderWidth: 1,
            fill: true,
            //backgroundColor: "rgba(83,132,153,.5)",
            //backgroundColor: "rgba(200,132,153,.5)",
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'black'
        }
        ]
    },
    options: {
        title: {
            display: false,
            text: 'World population per region (in millions)'
        },
        legend: {
            display: false
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem) {
                    return tooltipItem.yLabel;
                }
            }
        },
        scales: {
            yAxes: [{
                ticks: {
                    display: false
                }
            }],
            xAxes: [{
                ticks: {
                    display: false
                }
            }],
        }
    }
});

new Chart(document.getElementById("mammo_proc"), {
    type: 'line',
    data: {
        labels: [
            @foreach($month_summary as $month_pb)
                "{{$month_pb->report_date->format('m-d-Y')}}",
            @endforeach

        ],
        datasets: [{
            data: [

                @foreach($month_summary as $month_pb)
                {{$month_pb->mammo_proc}},
                @endforeach

            ],
            label: "Dollars",
            borderColor: "#3e95cd",
            borderWidth: 1,
            fill: true,
            //backgroundColor: "rgba(83,132,153,.5)",
            //backgroundColor: "rgba(200,132,153,.5)",
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'black'
        }
        ]
    },
    options: {
        title: {
            display: false,
            text: 'World population per region (in millions)'
        },
        legend: {
            display: false
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem) {
                    return tooltipItem.yLabel;
                }
            }
        },
        scales: {
            yAxes: [{
                ticks: {
                    display: false
                }
            }],
            xAxes: [{
                ticks: {
                    display: false
                }
            }],
        }
    }
});

new Chart(document.getElementById("pet_proc"), {
    type: 'line',
    data: {
        labels: [
            @foreach($month_summary as $month_pb)
                "{{$month_pb->report_date->format('m-d-Y')}}",
            @endforeach

        ],
        datasets: [{
            data: [

                @foreach($month_summary as $month_pb)
                {{$month_pb->pet_proc}},
                @endforeach

            ],
            label: "Dollars",
            borderColor: "#3e95cd",
            borderWidth: 1,
            fill: true,
            //backgroundColor: "rgba(83,132,153,.5)",
            //backgroundColor: "rgba(200,132,153,.5)",
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'black'
        }
        ]
    },
    options: {
        title: {
            display: false,
            text: 'World population per region (in millions)'
        },
        legend: {
            display: false
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem) {
                    return tooltipItem.yLabel;
                }
            }
        },
        scales: {
            yAxes: [{
                ticks: {
                    display: false
                }
            }],
            xAxes: [{
                ticks: {
                    display: false
                }
            }],
        }
    }
});
</script>

@endsection
@endif