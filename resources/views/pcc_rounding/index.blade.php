{{--New file Template--}}

{{--Add Security for this page below--}}

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}
    
@section('content')

    <div class="card bg-dark text-white mb-3">
        <div class="card-body">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Department</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="dashboard-tab" data-toggle="tab" href="#dashboard" role="tab" aria-controls="dashboard" aria-selected="false">Dashboard</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="show-tab" data-toggle="tab" href="#today" role="tab" aria-controls="show" aria-selected="false">Today</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="history-tab" data-toggle="modal" href="#pccReport" role="tab" aria-controls="history" aria-selected="false">Generate Report</a>
                </li>
            </ul>
        </div>
    </div>

    <div class="tab-content" id="myTabContent">

        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="dashboard-tab">

            <div class="card mb-4 text-white bg-dark">
                <div class="card-header">
                    Current Patient List: (Total {{ count($current) }})
                </div>
                <div class="card-body">
                    <div class="card-title">
                        <h5>Please Select Your Cost Center</h5>
                    </div>
                    
                    <div class="list-group col-3">
                        @foreach($units as $unit)
                            <a href="/pcc/unit/{{$unit->unit}}" class="list-group-item list-group-item-action list-group-item-dark"> {{ $unit->unit }} </a>
                        @endforeach
                    </div>

                </div>
            </div>

        </div> {{--Home--}}

        <div class="tab-pane fade show" id="dashboard" role="tabpanel" aria-labelledby="dashboard-tab">

            <div class="card mb-4 text-white bg-dark">
                <div class="card-header">
                    Current Patient List: (Total {{ count($current) }})
                </div>
                <div class="card-body">

                    <div class="card-deck">
                        
                        <div class="card mb-4 text-white bg-dark border-success">
                            <div class="card-header border-success">
                                Complete
                            </div>
                            <div class="card-body">
                                {{ $comp_count }}
                            </div>
                        </div>
                        <div class="card mb-4 text-white bg-dark border-danger">
                            <div class="card-header border-danger">
                                Incomplete
                            </div>
                            <div class="card-body">
                                {{ count($current) - $comp_count }}
                            </div>
                        </div>

                    </div>

                    
                    <div class="card-deck">
                        <div class="card mb-4 bg-dark text-white border-info">
                            <div class="card-header border-info">
                                Foley Compliance %
                                <span class="float-right text-warning">SGMC ALL TIME %</span>
                            </div>
                            <div class="card-body">

                                <div class="progress">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated @if($complianceF < 80) bg-danger @elseif($complianceF < 90 && $complianceF >= 80) bg-warning @else bg-success @endif" role="progressbar" aria-valuenow="{{number_format($complianceF, 1)}}" aria-valuemin="0" aria-valuemax="100" style="width: {{number_format($complianceF, 1)}}%"> {{number_format($complianceF, 1)}}%</div>
                                </div>

                            </div>
                        </div>

                        <div class="card mb-4 bg-dark text-white border-info">
                            <div class="card-header border-info">
                                Central Line Compliance %
                                <span class="float-right text-warning">SGMC ALL TIME %</span>
                            </div>
                            <div class="card-body">

                                <div class="progress">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated @if($complianceCL < 80) bg-danger @elseif($complianceCL < 90 && $complianceCL >= 80) bg-warning @else bg-success @endif" role="progressbar" aria-valuenow="{{number_format($complianceCL, 1)}}" aria-valuemin="0" aria-valuemax="100" style="width: {{number_format($complianceCL, 1)}}%"> {{number_format($complianceCL, 1)}}%</div>
                                </div>
                                
                            </div>
                        </div>
                    </div>

                    
                    <div class="table-responsive">
                        <table class="table table-dark table-bordered table-hover" id="pcc">
                            <thead>
                                <th>Unit</th>
                                <th>Patient Name</th>
                                <th>Room #</th>
                                <th>Status</th>
                                <th>Start Survey</th>
                            </thead>
                            <tbody>
                                @foreach($current as $curr)
                                    <tr>
                                        <td> {{ $curr->unit }} </td>
                                        <td> {{ $curr->patient_name }} </td>
                                        <td> {{ $curr->bed }} </td>   
                                        <td>
                                            @foreach($complete as $comp)
                                                @if($comp->patient == $curr->patient_name && $comp->room_num == $curr->bed)
                                                    <i class="fa fa-check text-success" aria-hidden="true"></i> Completed
                                                @endif
                                            @endforeach
                                        </td>
                                        <td> <a href="/pcc/create/{{ $curr->id }}" class="btn btn-primary btn-block btn-sm">Start Survey</a> </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>

        </div> {{-- Dashboard --}}

        <div class="tab-pane fade show" id="today" role="tabpanel" aria-labelledby="home-tab">

            <div class="card bg-dark text-white mb-3">
                <div class="card-header">
                    Completed Today
                </div>
                <div class="card-body">
                    <div class="jumbotron bg-secondary">
                        <h1>Total: <span class="text-warning"> {{ count($complete) }} </span> </h1>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-dark table-bordered table-hover" id="comp_today">
                            <thead>
                                <th>Unit</th>
                                <th>Patient Name</th>
                                <th>Room #</th>
                                <th>View</th>
                                <th>Edit Survey</th>
                                <th>Delete Survey</th>
                            </thead>
                            <tbody>
                                @foreach($complete as $comp)
                                    <tr>
                                        <td> {{ $comp->unit }} </td>
                                        <td> {{ $comp->patient }} </td>
                                        <td> {{ $comp->room_num }} </td>
                                        <td> <a href="/pcc/show/{{$comp->id}}" class="btn btn-sm btn-info btn-block">View</a> </td>
                                        <td> <a href="/pcc/edit/{{$comp->id}}" class="btn btn-sm btn-warning btn-block">Edit</a> </td>
                                        <td> <a href="#" class="btn btn-sm btn-danger btn-block deleteSurvey" survey_id = {{$comp->id}} >Delete</a> </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>

        </div> {{-- Today --}}

    </div> {{-- Nav Tabs End --}}

    @include('pcc_rounding.modals.report')

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">
    $(document).ready(function() {
        $('#pcc').DataTable( {
            "pageLength": 15,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>

<script type="application/javascript">
    $(document).ready(function() {
        $('#comp_today').DataTable( {
            "pageLength": 15,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>

<script type="application/javascript">

$(document).on("click", ".deleteSurvey", function () {
            //alert($(this).attr("data-cost_center"));
            var survey_id = $(this).attr("survey_id");
            DeleteSurvey(survey_id);
        });

        function DeleteSurvey(survey_id) {
            swal({
                title: "Delete survey?",
                text: "Deleting this survey cannot be undone.",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                confirmButtonText: "Yes, delete it!",
                confirmButtonColor: "#ec6c62"
            }, function() {
                $.ajax({
                    url: "/pcc/destroy/" + survey_id,
                    type: "GET"
                })
                    .done(function(data) {
                        swal({
                                title: "Deleted",
                                text: "Survey Deleted",
                                type: "success",
                                timer: 1800,
                                showConfirmButton: false

                            }
                        );
                        setTimeout(function(){window.location.replace('/pcc')},1900);
                    })
                    .error(function(data) {
                        swal("Oops", "We couldn't connect to the server!", "error");
                    });
            });
        }

</script>

@endsection
