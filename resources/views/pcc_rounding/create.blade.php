{{--New file Template--}}

{{--Add Security for this page below--}}


    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}
    
@section('content')


    <div class="card mb-4 text-white bg-dark">
        <div class="card-header">
            Survey For: &nbsp;{{ $patient->patient_name }}
            <span class="float-right"><a href="/pcc" class="btn btn-primary btn-sm">Return To Dashboard</a></span>
        </div>
        <div class="card-body">
            
            <form action="/pcc/store" method="POST">
                {{ csrf_field() }}
                <input type="hidden" value="{{$patient->patient_name}}" name="patient">
                <input type="hidden" value="{{$patient->unit}}" name="unit">
                <input type="hidden" value="{{$patient->bed}}" name="room_num">
                <input type="hidden" value={{Carbon::now()}} name="date">

                <div class="form-group">
                    <label for="nurse">Nurse Leader</label>
                    <input type="text" class="form-control" name="nurse_leader" value="{{Auth::user()->name}}" readonly>
                </div>

                <hr style="background-color:white;">{{-- row --}}
                <br>

            @if($patient->unit != 'SGANICU' && $patient->unit != 'SGANURSERY')

                <div class="form-row text-center">

                    <div class="col-lg-1"></div>

                    <div class="form-group col-lg-1">
                        <label for="room_clean">Room Clean?</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="room_clean" value="Y" required>
                            <label class="form-check-label" for="room_clean">
                                Y
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="room_clean" value="N">
                            <label class="form-check-label" for="room_clean">
                                N
                            </label>
                        </div>
                    </div>

                    <div class="form-group col-lg-1">
                        <label for="discharge_folder">Discharge Folder?</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="discharge_folder" value="Y" required>
                            <label class="form-check-label" for="discharge_folder">
                                Y
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="discharge_folder" value="N">
                            <label class="form-check-label" for="discharge_folder">
                                N
                            </label>
                        </div>
                    </div>

                    <div class="form-group col-lg-1">
                        <label for="poc_board">POC Board?</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="poc_board" value="Y" required>
                            <label class="form-check-label" for="poc_board">
                                Y
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="poc_board" value="N">
                            <label class="form-check-label" for="poc_board">
                                N
                            </label>
                        </div>
                    </div>

                    <div class="form-group col-lg-1">
                        <label for="side_effects">Med Side Effects?</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="side_effects" value="Y" required>
                            <label class="form-check-label" for="side_effects">
                                Y
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="side_effects" value="N">
                            <label class="form-check-label" for="side_effects">
                                N
                            </label>
                        </div>
                    </div>

                    <div class="col-lg-2"></div>

                    <div class="form-group col-lg-1">
                        <label for="fall_bundle">Fall Bundle?</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="fall_bundle" value="Y" required>
                            <label class="form-check-label" for="fall_bundle">
                                Y
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="fall_bundle" value="N">
                            <label class="form-check-label" for="fall_bundle">
                                N
                            </label>
                        </div>
                    </div>

                    <div class="form-group col-lg-1">
                        <label for="daily_bath">Daily Bath?</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="daily_bath" value="Y" required>
                            <label class="form-check-label" for="daily_bath">
                                Y
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="daily_bath" value="N">
                            <label class="form-check-label" for="daily_bath">
                                N
                            </label>
                        </div>
                    </div>

                    <div class="form-group col-lg-1">
                        <label for="belonging_box">Patient Belonging Box?</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="belonging_box" value="Y" required>
                            <label class="form-check-label" for="belonging_box">
                                Y
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="belonging_box" value="N">
                            <label class="form-check-label" for="belonging_box">
                                N
                            </label>
                        </div>
                    </div>

                    <div class="form-group col-lg-1">
                        <label for="hand_sanitizer">Hand Sanitizer?</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="hand_sanitizer" value="Y" required>
                            <label class="form-check-label" for="hand_sanitizer">
                                Y
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="hand_sanitizer" value="N">
                            <label class="form-check-label" for="hand_sanitizer">
                                N
                            </label>
                        </div>
                    </div>

                    <div class="col-lg-1"></div>

                </div>

                <br>
                <hr style="background-color:white;">{{-- row --}}
                <br>

                <div class="form-row text-center">

                    <div class="form-group col-lg-4">
                        <label for="foley">Foley:</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="foley" value="Y" required id="foley_yes">
                            <label class="form-check-label" for="foley">
                                Y
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="foley" value="N" id="foley_no">
                            <label class="form-check-label" for="foley">
                                N
                            </label>
                        </div>
                    </div>

                    <div class="form-group col-lg-4 text-left">
                        <label for="foley_removal">Meets Criteria For Removal? (Foley)</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="foley_removal" value="Y" id="foley_req">
                            <label class="form-check-label" for="foley_removal">
                                Y
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="foley_removal" value="N">
                            <label class="form-check-label" for="foley_removal">
                                N
                            </label>
                        </div>
                    </div>

                    <div class="form-group col-lg-4 text-left">
                        <label><b><u> Foley Bundle Criteria </u></b></label>

                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="foley_bundle[]" value="Secure w/Statlock">
                            <label class="form-check-label" for="">
                                Secure w/Statlock
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="foley_bundle[]" value="No Kinks">
                            <label class="form-check-label" for="">
                                No Kinks
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="foley_bundle[]" value="Not Touching Floor/Below Hips">
                            <label class="form-check-label" for="">
                                Not Touching Floor/Below Hips
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="foley_bundle[]" value="< 3/4 Full">
                            <label class="form-check-label" for="">
                                < 3/4 Full
                            </label>
                        </div>
                    </div>

                </div>
                {{-- row --}}

                <hr style="background-color:white;">
                <br>
            @endif
                <div class="form-row text-center">

                    <div class="form-group col-lg-2">
                        <label for="central_line">Central Line:</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="central_line" value="Y" required id="central_yes">
                            <label class="form-check-label" for="central_line">
                                Y
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="central_line" value="N" id="central_no">
                            <label class="form-check-label" for="central_line">
                                N
                            </label>
                        </div>
                    </div>

                    <div class="form-group col-lg-2">
                        <label for="central_line_removal">Meets Criteria For Removal? (CL)</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="central_line_removal" value="Y" id="central_req"> 
                            <label class="form-check-label" for="central_line_removal">
                                Y
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="central_line_removal" value="N">
                            <label class="form-check-label" for="central_line_removal">
                                N
                            </label>
                        </div>
                    </div>


                    <div class="form-group col-lg-2">
                        <select name="location" class="form-control" id="site">
                            <option value="">[Choose Site]</option>
                            <option value="R/arm">R/arm</option>
                            <option value="L/arm">L/arm</option>
                            <option value="R/chest">R/chest</option>
                            <option value="L/chest">L/chest</option>
                            <option value="R/IJ">R/IJ</option>
                            <option value="L/IJ">L/IJ</option>
                            <option value="Femoral">Femoral</option>
                        </select>
                    </div>

                    <div class="form-group col-lg-2">
                        <select name="device_type" class="form-control">
                            <option value="" selected>[Choose Device Type]</option>
                            <option value="CVL">CVL</option>
                            <option value="PORT">PORT</option>
                            <option value="PICC">PICC (Not Midline)</option>
                            <option value="Dialysis Cath">Dialysis Cath</option>
                            <option value="Cordis">Cordis</option>
                        </select>
                    </div>

                    <div class="form-group col-lg-3 text-left ml-5">
                        <label><b><u> Central Line Bundle Criteria </u></b></label>

                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="cl_bundle[]" value="No S/S of Infection (Site)">
                            <label class="form-check-label" for="">
                                No S/S of Infection (<b>Site</b>)
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="cl_bundle[]" value="Dry, Intact, No Gauze, & Dated (Dressing)">
                            <label class="form-check-label" for="">
                                Dry, Intact, No Gauze, & Dated (<b>Dressing</b>)
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="cl_bundle[]" value="Correct (Biopatch)">
                            <label class="form-check-label" for="">
                                Correct (<b>Biopatch</b>)
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="cl_bundle[]" value="Date/Time (Tubing)">
                            <label class="form-check-label" for="">
                                Date/Time (<b>Tubing</b>)
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="cl_bundle[]" value="Capped Correctly (Tubing)">
                            <label class="form-check-label" for="">
                                Capped Correctly (<b>Tubing</b>)
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="cl_bundle[]" value="Alcohol Cap (Hub)">
                            <label class="form-check-label" for="">
                                Alcohol Cap (<b>Hub</b>)
                            </label>
                        </div>

                    </div>

                </div> 

                <hr style="background-color:white;">{{-- row --}}

                <input type="submit" class="btn btn-primary btn-block mt-3" value="Submit">

            </form>

        </div>
    </div>


@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

<script>
    
    $("#foley_yes").click(function() {
        $("#foley_req").prop("required", true);
    });
    $("#foley_no").click(function() {
        $("#foley_req").prop("required", false);
    });


    $("#central_yes").click(function() {
        $("#central_req").prop("required", true);
        $("#site").prop("required", true);
    });
    $("#central_no").click(function() {
        $("#central_req").prop("required", false);
        $("#site").prop("required", false);
    });

</script>

@endsection
