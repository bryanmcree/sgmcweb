{{--New file Template--}}

{{--Add Security for this page below--}}

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}
    
@section('content')

    <div class="card bg-dark text-white mb-3">
        <div class="card-header">
            Report For: {{ Carbon::parse($startDate)->format('n/j/Y') }} - {{ Carbon::parse($endDate)->format('n/j/Y') }}
            <span class="float-right"><a href="/pcc" class="btn btn-primary btn-sm">Return To Dashboard</a></span>
        </div>
        <div class="card-body">

            <div class="jumbotron bg-secondary">
                <h1>Total @if(!empty($leader))  for {{ $leader->nurse_leader }}@endif: <span class="text-warning"> {{ count($survey) }} </span> </h1>
            </div>

                    
            <div class="card-deck">
                <div class="card mb-4 bg-dark text-white border-info">
                    <div class="card-header border-info">
                        Foley Compliance %
                        <span class="float-right text-warning">@if(!empty($leader)) {{ $leader->nurse_leader }} @else SGMC @endif WITHIN DATE RANGE %</span>
                    </div>
                    <div class="card-body">

                        <div class="progress">
                            <div class="progress-bar progress-bar-striped progress-bar-animated @if($complianceF < 80) bg-danger @elseif($complianceF < 90 && $complianceF >= 80) bg-warning @else bg-success @endif" role="progressbar" aria-valuenow="{{number_format($complianceF, 1)}}" aria-valuemin="0" aria-valuemax="100" style="width: {{number_format($complianceF, 1)}}%"> {{number_format($complianceF, 1)}}%</div>
                        </div>
                        
                    </div>
                </div>

                <div class="card mb-4 bg-dark text-white border-info">
                    <div class="card-header border-info">
                        Central Line Compliance %
                        <span class="float-right text-warning">@if(!empty($leader)) {{ $leader->nurse_leader }} @else SGMC @endif WITHIN DATE RANGE %</span>
                    </div>
                    <div class="card-body">
                        
                        <div class="progress">
                            <div class="progress-bar progress-bar-striped progress-bar-animated @if($complianceCL < 80) bg-danger @elseif($complianceCL < 90 && $complianceCL >= 80) bg-warning @else bg-success @endif" role="progressbar" aria-valuenow="{{number_format($complianceCL, 1)}}" aria-valuemin="0" aria-valuemax="100" style="width: {{number_format($complianceCL, 1)}}%"> {{number_format($complianceCL, 1)}}%</div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-dark table-bordered table-hover" id="pcc">
                    <thead>
                        <th>Unit</th>
                        <th>Patient Name</th>
                        <th>Room #</th>
                        <th>View</th>
                        <th>Edit Survey</th>
                        <th>Delete Survey</th>
                    </thead>
                    <tbody>
                        @foreach($survey as $s)
                            <tr>
                                <td> {{ $s->unit }} </td>
                                <td> {{ $s->patient }} </td>
                                <td> {{ $s->room_num }} </td>
                                <td> <a href="/pcc/show/{{$s->id}}" class="btn btn-sm btn-info btn-block">View</a> </td>
                                <td> <a href="/pcc/edit/{{$s->id}}" class="btn btn-sm btn-warning btn-block">Edit</a> </td>
                                <td> <a href="#" class="btn btn-sm btn-danger btn-block deleteSurvey" survey_id = {{$s->id}} >Delete</a> </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')


<script type="application/javascript">
    $(document).ready(function() {
        $('#pcc').DataTable( {
            "pageLength": 15,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>

<script type="application/javascript">

$(document).on("click", ".deleteSurvey", function () {
            //alert($(this).attr("data-cost_center"));
            var survey_id = $(this).attr("survey_id");
            DeleteSurvey(survey_id);
        });

        function DeleteSurvey(survey_id) {
            swal({
                title: "Delete survey?",
                text: "Deleting this survey cannot be undone.",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                confirmButtonText: "Yes, delete it!",
                confirmButtonColor: "#ec6c62"
            }, function() {
                $.ajax({
                    url: "/pcc/destroy/" + survey_id,
                    type: "GET"
                })
                    .done(function(data) {
                        swal({
                                title: "Deleted",
                                text: "Survey Deleted",
                                type: "success",
                                timer: 1800,
                                showConfirmButton: false

                            }
                        );
                        setTimeout(function(){window.location.replace('/pcc')},1900);
                    })
                    .error(function(data) {
                        swal("Oops", "We couldn't connect to the server!", "error");
                    });
            });
        }

</script>

@endsection