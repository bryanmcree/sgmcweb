{{--New file Template--}}

{{--Add Security for this page below--}}

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}
    
@section('content')


 <div class="card mb-4 text-white bg-dark">
    <div class="card-header">
        Current Patient List: (Total {{ count($patients) }})
        <span class="float-right"><a href="/pcc" class="btn btn-primary btn-sm">Return Home</a></span>
    </div>
    <div class="card-body">

        <div class="card-deck">
                        
            <div class="card mb-4 text-white bg-dark border-success">
                <div class="card-header border-success">
                    Complete
                </div>
                <div class="card-body">
                    {{ $comp_count }}
                </div>
            </div>
            <div class="card mb-4 text-white bg-dark border-danger">
                <div class="card-header border-danger">
                    Incomplete
                </div>
                <div class="card-body">
                    {{ count($patients) - $comp_count }}
                </div>
            </div>

        </div>

        <div class="card-deck">
            <div class="card mb-4 bg-dark text-white border-info">
                <div class="card-header border-info">
                    Foley Compliance %
                    <span class="float-right text-warning">ALL TIME DEPARTMENT %</span>
                </div>
                <div class="card-body">
                    
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated @if($complianceF < 80) bg-danger @elseif($complianceF < 90 && $complianceF >= 80) bg-warning @else bg-success @endif" role="progressbar" aria-valuenow="{{number_format($complianceF, 1)}}" aria-valuemin="0" aria-valuemax="100" style="width: {{number_format($complianceF, 1)}}%"> {{number_format($complianceF, 1)}}%</div>
                    </div>

                </div>
            </div>

            <div class="card mb-4 bg-dark text-white border-info">
                <div class="card-header border-info">
                    Central Line Compliance %
                    <span class="float-right text-warning">ALL TIME DEPARTMENT %</span>
                </div>
                <div class="card-body">
                    
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated @if($complianceCL < 80) bg-danger @elseif($complianceCL < 90 && $complianceCL >= 80) bg-warning @else bg-success @endif" role="progressbar" aria-valuenow="{{number_format($complianceCL, 1)}}" aria-valuemin="0" aria-valuemax="100" style="width: {{number_format($complianceCL, 1)}}%"> {{number_format($complianceCL, 1)}}%</div>
                    </div>

                </div>
            </div>
        </div>
    
        <div class="table-responsive">
            <table class="table table-dark table-bordered table-hover" id="unit">
                <thead>
                    <th>Unit</th>
                    <th>Patient Name</th>
                    <th>Room #</th>
                    <th>Status</th>
                    <th>Start Survey</th>
                </thead>
                <tbody>
                    @foreach($patients as $curr)
                        <tr>
                            <td> {{ $curr->unit }} </td>
                            <td> {{ $curr->patient_name }} </td>
                            <td> {{ $curr->bed }} </td>   
                            <td>
                                @foreach($complete as $comp)
                                    @if($comp->patient == $curr->patient_name && $comp->room_num == $curr->bed)
                                        <i class="fa fa-check text-success" aria-hidden="true"></i> Completed
                                    @endif
                                @endforeach
                            </td>
                            <td> <a href="/pcc/create/{{ $curr->id }}" class="btn btn-primary btn-block btn-sm">Start Survey</a> </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
</div>


@endsection

@section('scripts')

<script type="application/javascript">
    $(document).ready(function() {
        $('#unit').DataTable( {
            "pageLength": 15,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>

@endsection
