<!-- Modal -->
<div class="modal fade" id="pccReport" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content text-white bg-dark">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">PCC Rounding Report</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="/pcc/report">
                {{ csrf_field() }}
                <div class="modal-body">

                    <div class="form-group">
                        <label>Nurse Leader</label>
                        <select name="nurse" class="form-control">
                            <option value="">[Select Nurse]</option>
                            @foreach($nurse_leaders as $nurse)
                                <option value="{{$nurse->nurse_leader}}">{{$nurse->nurse_leader}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Start Date / Time</label>
                        <input type="date" name="startDate" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1">End Date / Time</label>
                        <input type="date" name="endDate" class="form-control" required value= {{ \Carbon::now() }}>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Run Report">
                </div>

            </form>
        </div>
    </div>
</div>