{{--New file Template--}}

{{--Add Security for this page below--}}

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}
    
@section('content')


    <div class="card mb-4 text-white bg-dark">
        <div class="card-header">
            Edit Survey For: &nbsp;{{ $survey->patient }}
            <span class="float-right"><a href="/pcc" class="btn btn-primary btn-sm">Return To Dashboard</a></span>
        </div>
        <div class="card-body">
            
            <form action="/pcc/update/{{$survey->id}}" method="POST">
                {{ csrf_field() }}
                {{-- <input type="hidden" value="{{$survey->patient}}" name="patient">
                <input type="hidden" value="{{$survey->unit}}" name="unit">
                <input type="hidden" value="{{$survey->room_num}}" name="room_num"> 
                <input type="hidden" value={{Carbon::now()}} name="date"> --}}

                <div class="form-group">
                    <label for="nurse">Nurse Leader</label>
                    <input type="text" class="form-control" name="nurse_leader" value="{{$survey->nurse_leader}}" readonly>
                </div>
                {{-- row --}}
                {{-- <div class="form-row">
                    <div class="form-group col-lg-6">
                        <label for="discharge_concerns">
                            We want to make sure you are truly prepared for discharge or the next stage of care when that time comes.
                            What questions/concerns do you have about managing your health once you've left our unit?
                            What questions/concerns do you have about your medications?
                        </label>
                        <textarea name="discharge_concerns" rows="5" class="form-control">{{$survey->discharge_concerns}}</textarea>
                    </div>
                    <div class="form-group col-lg-6">
                        <label for="discharge_concerns">
                            <br><br>
                            Tell me how our staff is doing in responding to your calls and addressing your needs.
                        </label>
                        <textarea name="staff_response" rows="5" class="form-control">{{$survey->staff_response}}</textarea>
                    </div>
                </div>
                {{-- row -}}
                <div class="form-row">
                    <div class="form-group col-lg-6">
                        <label for="staff_recognition">
                            Is there a staff member you would like for me to recognize for going above and beyond?
                        </label>
                        <textarea name="staff_recognition" rows="5" class="form-control">{{$survey->staff_recognition}}</textarea>
                    </div>
                    <div class="form-group col-lg-6">
                        <label for="questions">
                            What questions or concerns do you have for me? What can I do for you before I leave?
                        </label>
                        <textarea name="questions" rows="5" class="form-control">{{$survey->questions}}</textarea>
                    </div>
                </div> --}}

                <hr style="background-color:white;">{{-- row --}}
                <br>

                <div class="form-row text-center">

                    <div class="col-lg-1"></div>

                    <div class="form-group col-lg-1">
                        <label for="room_clean">Room Clean?</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="room_clean" value="Y" @if($survey->room_clean == 'Y') checked @endif>
                            <label class="form-check-label" for="room_clean">
                                Y
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="room_clean" value="N" @if($survey->room_clean == 'N') checked @endif>
                            <label class="form-check-label" for="room_clean">
                                N
                            </label>
                        </div>
                    </div>

                    <div class="form-group col-lg-1">
                        <label for="discharge_folder">Discharge Folder?</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="discharge_folder" value="Y" @if($survey->discharge_folder == 'Y') checked @endif>
                            <label class="form-check-label" for="discharge_folder">
                                Y
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="discharge_folder" value="N" @if($survey->discharge_folder == 'N') checked @endif>
                            <label class="form-check-label" for="discharge_folder">
                                N
                            </label>
                        </div>
                    </div>

                    <div class="form-group col-lg-1">
                        <label for="poc_board">POC Board?</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="poc_board" value="Y" @if($survey->poc_board == 'Y') checked @endif>
                            <label class="form-check-label" for="poc_board">
                                Y
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="poc_board" value="N" @if($survey->poc_board == 'N') checked @endif>
                            <label class="form-check-label" for="poc_board">
                                N
                            </label>
                        </div>
                    </div>

                    <div class="form-group col-lg-1">
                        <label for="side_effects">Med Side Effects?</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="side_effects" value="Y" @if($survey->side_effects == 'Y') checked @endif>
                            <label class="form-check-label" for="side_effects">
                                Y
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="side_effects" value="N" @if($survey->side_effects == 'N') checked @endif>
                            <label class="form-check-label" for="side_effects">
                                N
                            </label>
                        </div>
                    </div>

                    <div class="col-lg-2"></div>

                    <div class="form-group col-lg-1">
                        <label for="fall_bundle">Fall Bundle?</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="fall_bundle" value="Y" @if($survey->fall_bundle == 'Y') checked @endif>
                            <label class="form-check-label" for="fall_bundle">
                                Y
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="fall_bundle" value="N" @if($survey->fall_bundle == 'N') checked @endif>
                            <label class="form-check-label" for="fall_bundle">
                                N
                            </label>
                        </div>
                    </div>

                    <div class="form-group col-lg-1">
                        <label for="daily_bath">Daily Bath</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="daily_bath" value="Y" @if($survey->daily_bath == 'Y') checked @endif>
                            <label class="form-check-label" for="daily_bath">
                                Y
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="daily_bath" value="N" @if($survey->daily_bath == 'N') checked @endif>
                            <label class="form-check-label" for="daily_bath">
                                N
                            </label>
                        </div>
                    </div>

                    <div class="form-group col-lg-1">
                        <label for="belonging_box">Patient Belonging Box?</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="belonging_box" value="Y" required @if($survey->belonging_box == 'Y') checked @endif>
                            <label class="form-check-label" for="belonging_box">
                                Y
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="belonging_box" value="N" @if($survey->belonging_box == 'N') checked @endif>
                            <label class="form-check-label" for="belonging_box">
                                N
                            </label>
                        </div>
                    </div>

                    <div class="form-group col-lg-1">
                        <label for="hand_sanitizer">Hand Sanitizer?</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="hand_sanitizer" value="Y" required @if($survey->hand_sanitizer == 'Y') checked @endif>
                            <label class="form-check-label" for="hand_sanitizer">
                                Y
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="hand_sanitizer" value="N" @if($survey->hand_sanitizer == 'N') checked @endif>
                            <label class="form-check-label" for="hand_sanitizer">
                                N
                            </label>
                        </div>
                    </div>

                    <div class="col-lg-1"></div>

                </div>

                <br>
                <hr style="background-color:white;">{{-- row --}}
                <br>

                <div class="form-row text-center">

                    <div class="form-group col-lg-4">
                        <label for="foley">Foley:</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="foley" value="Y" @if($survey->foley == 'Y') checked @endif>
                            <label class="form-check-label" for="foley">
                                Y
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="foley" value="N" @if($survey->foley == 'N') checked @endif>
                            <label class="form-check-label" for="foley">
                                N
                            </label>
                        </div>
                    </div>

                    <div class="form-group col-lg-4 text-left">
                        <label for="foley_removal">Meets Criteria For Removal? (Foley)</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="foley_removal" value="Y" @if($survey->foley_removal == 'Y') checked @endif>
                            <label class="form-check-label" for="foley_removal">
                                Y
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="foley_removal" value="N" @if($survey->foley_removal == 'N') checked @endif>
                            <label class="form-check-label" for="foley_removal">
                                N
                            </label>
                        </div>
                    </div>

                    <div class="form-group col-lg-4 text-left">
                        <label><b><u> Foley Bundle Criteria </u></b></label>

                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="foley_bundle[]" value="Secure w/Statlock"  @foreach($survey->foleys as $f) @if($f->criteria == 'Secure w/Statlock') checked @endif @endforeach>
                            <label class="form-check-label" for="">
                                Secure w/Statlock
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="foley_bundle[]" value="No Kinks" @foreach($survey->foleys as $f) @if($f->criteria == 'No Kinks') checked @endif @endforeach>
                            <label class="form-check-label" for="">
                                No Kinks
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="foley_bundle[]" value="Not Touching Floor/Below Hips" @foreach($survey->foleys as $f) @if($f->criteria == 'Not Touching Floor/Below Hips') checked @endif @endforeach>
                            <label class="form-check-label" for="">
                                Not Touching Floor/Below Hips
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="foley_bundle[]" value="< 3/4 Full" @foreach($survey->foleys as $f) @if($f->criteria == '< 3/4 Full') checked @endif @endforeach>
                            <label class="form-check-label" for="">
                                < 3/4 Full
                            </label>
                        </div>
                    </div>

                </div>
                {{-- row --}}

                <br>
                <hr style="background-color:white;">
                <br>
                
                <div class="form-row text-center">

                     <div class="form-group col-lg-2">
                        <label for="central_line">Central Line:</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="central_line" value="Y" @if($survey->central_line == 'Y') checked @endif>
                            <label class="form-check-label" for="central_line">
                                Y
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="central_line" value="N" @if($survey->central_line == 'N') checked @endif>
                            <label class="form-check-label" for="central_line">
                                N
                            </label>
                        </div>
                    </div>

                    <div class="form-group col-lg-2">
                        <label for="central_line_removal">Meets Criteria For Removal? (CL)</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="central_line_removal" value="Y" @if($survey->central_line_removal == 'Y') checked @endif>
                            <label class="form-check-label" for="central_line_removal">
                                Y
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="central_line_removal" value="N" @if($survey->central_line_removal == 'N') checked @endif>
                            <label class="form-check-label" for="central_line_removal">
                                N
                            </label>
                        </div>
                    </div> 

                    <div class="form-group col-lg-2">
                        <select name="location" class="form-control">
                            <option value="{{$survey->location}}">{{$survey->location}}</option>
                            <option value="R/arm">R/arm</option>
                            <option value="L/arm">L/arm</option>
                            <option value="R/leg">R/leg</option>
                            <option value="L/Leg">L/leg</option>
                            <option value="R/IJ">R/IJ</option>
                            <option value="L/IJ">L/IJ</option>
                            <option value="Femoral">Femoral</option>
                        </select>
                    </div>

                    <div class="form-group col-lg-2">
                        <select name="device_type" class="form-control">
                            <option value="{{$survey->device_type}}" selected>{{$survey->device_type}}</option>
                            <option value="CVL">CVL</option>
                            <option value="PORT">PORT</option>
                            <option value="PICC">PICC (Not Midline)</option>
                            <option value="Dialysis Cath">Dialysis Cath</option>
                            <option value="Cordis">Cordis</option>
                        </select>
                    </div>

                    <div class="form-group col-lg-3 ml-5 text-left">
                        <label><b><u> Central Line Bundle Criteria </u></b></label>

                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="cl_bundle[]" value="No S/S of Infection (Site)"  @foreach($survey->centrals as $c) @if($c->criteria == 'No S/S of Infection (Site)') checked @endif @endforeach>
                            <label class="form-check-label" for="">
                                No S/S of Infection (<b>Site</b>)
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="cl_bundle[]" value="Dry, Intact, No Gauze, & Dated (Dressing)"  @foreach($survey->centrals as $c) @if($c->criteria == 'Dry, Intact, No Gauze, & Dated (Dressing)') checked @endif @endforeach>
                            <label class="form-check-label" for="">
                                Dry, Intact, No Gauze, & Dated (<b>Dressing</b>)
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="cl_bundle[]" value="Correct (Biopatch)"  @foreach($survey->centrals as $c) @if($c->criteria == 'Correct (Biopatch)') checked @endif @endforeach>
                            <label class="form-check-label" for="">
                                Correct (<b>Biopatch</b>)
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="cl_bundle[]" value="Date/Time (Tubing)"  @foreach($survey->centrals as $c) @if($c->criteria == 'Date/Time (Tubing)') checked @endif @endforeach>
                            <label class="form-check-label" for="">
                                Date/Time (<b>Tubing</b>)
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="cl_bundle[]" value="Capped Correctly (Tubing)"  @foreach($survey->centrals as $c) @if($c->criteria == 'Capped Correctly (Tubing)') checked @endif @endforeach>
                            <label class="form-check-label" for="">
                                Capped Correctly (<b>Tubing</b>)
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="cl_bundle[]" value="Alcohol Cap (Hub)"  @foreach($survey->centrals as $c) @if($c->criteria == 'Alcohol Cap (Hub)') checked @endif @endforeach>
                            <label class="form-check-label" for="">
                                Alcohol Cap (<b>Hub</b>)
                            </label>
                        </div>

                    </div>

                </div> 

                <hr style="background-color:white;">{{-- row --}}

                <input type="submit" class="btn btn-primary btn-block mt-3" value="Update">

            </form>

        </div>
    </div>


@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
