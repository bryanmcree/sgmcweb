{{--New file Template--}}

{{--Add Security for this page below--}}
    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}
    
@section('content')

    <div class="card mb-3">
        <div class="card-header">
            Details for: {{$survey->patient}}
            <span class="float-right"><a href="/pcc" class="btn btn-primary btn-sm">Return To Dashboard</a></span>
        </div>
        <div class="card-body">

            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <th style="width: 25%;">Category</th>
                        <th>Data</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Patient Name</td>
                            <td> {{ $survey->patient }} </td>
                        </tr>
                        <tr>
                            <td>Unit</td>
                            <td> {{ $survey->unit }} </td>
                        </tr>
                        <tr>
                            <td>Room #</td>
                            <td> {{ $survey->room_num }} </td>
                        </tr>
                        <tr>
                            <td>Date</td>
                            <td> {{ Carbon::parse($survey->date)->format('n-j-Y') }} </td>
                        </tr>
                        <tr>
                            <td>Nurse Leader</td>
                            <td> {{ $survey->nurse_leader }} </td>
                        </tr>
                        {{-- <tr>
                            <td>Discharge Preparation Questions/Concerns</td>
                            <td> {{ $survey->discharge_concerns }} </td>
                        </tr>
                        <tr>
                            <td>Staff Response</td>
                            <td> {{ $survey->staff_response }} </td>
                        </tr>
                        <tr>
                            <td>Staff Recognition</td>
                            <td> {{ $survey->staff_recognition }} </td>
                        </tr>
                        <tr>
                            <td>Questions/Concerns</td>
                            <td> {{ $survey->questions }} </td>
                        </tr> --}}
                        <tr>
                            <td>Room Clean?</td>
                            <td> {{ $survey->room_clean }} </td>
                        </tr>
                        <tr>
                            <td>Discharge Folder?</td>
                            <td> {{ $survey->discharge_folder }} </td>
                        </tr>
                        <tr>
                            <td>POC Board?</td>
                            <td> {{ $survey->poc_board }} </td>
                        </tr>
                        <tr>
                            <td>Med Side Effects?</td>
                            <td> {{ $survey->side_effects }} </td>
                        </tr>
                        <tr>
                            <td>Fall Bundle?</td>
                            <td> {{ $survey->fall_bundle }} </td>
                        </tr>
                        <tr>
                            <td>Daily Bath?</td>
                            <td> {{ $survey->daily_bath }} </td>
                        </tr>
                        <tr>
                            <td>Patient Belonging Box?</td>
                            <td> {{ $survey->belonging_box }} </td>
                        </tr>
                        <tr>
                            <td>Hand Sanitizer?</td>
                            <td> {{ $survey->hand_sanitizer }} </td>
                        </tr>
                        <tr>
                            <td>Foley?</td>
                            <td> {{ $survey->foley }} </td>
                        </tr>
                        <tr>
                            <td>Meets Criteria for Removal? (Foley)</td>
                            <td> {{ $survey->foley_removal }} </td>
                        </tr>
                        <tr>
                            <td>Central Line?</td>
                            <td> {{ $survey->central_line }} </td>
                        </tr>
                        <tr>
                            <td>Meets Criteria for Removal? (CL)</td>
                            <td> {{ $survey->central_line_removal }} </td>
                        </tr>
                        <tr>
                            <td>Site</td>
                            <td> {{ $survey->location }} </td>
                        </tr>
                        <tr>
                            <td>Device Type</td>
                            <td> {{ $survey->device_type }} </td>
                        </tr>
                        <tr>
                            <td>Foley Bundle</td>
                            <td> 
                                @foreach($survey->foleys as $f)
                                    {{ $f->criteria }}
                                    <br>
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <td>Central Line Bundle</td>
                            <td> 
                                @foreach($survey->centrals as $c)
                                    {{ $c->criteria }}
                                    <br>
                                @endforeach
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
        <div class="card-footer">
            <a href="/pcc" class="btn btn-primary">Return To Dashboard</a>
        </div>
    </div>

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
