@if ( Auth::user()->hasRole('SGMC Safety & Security') == FALSE)
    @include('layouts.unauthorized')
@Else

@extends('layouts.app')

    <style>

        /* table.table-bordered{
            border:1px solid #8d3dff;
        }

        table.table-bordered > thead > tr > th{
            border:1px solid #8d3dff;
        }

        table.table-bordered > tbody > tr > td{
            border:1px solid #8d3dff;
        }

        table.table-bordered > tbody > tr > th{
            border:1px solid #8d3dff;
        } */

    </style>


    {{-- 2 --}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

@section('content')

    @include('sgmcsecurity.nav.nav')

<div class="card bg-dark text-white mb-3 mx-auto" style="width:50%">
    <div class="card-header">
        <h4>Rounds Checklist</h4>
    </div>

    <form action="/security/rounds/create" method="POST">
        <div class="card-body">
            {{ csrf_field() }}
            <input type="hidden" name="user_id" value="{{$user->employee_number}}">

            <table class="table table-dark table-hover table-bordered">
                <thead class="text-warning">
                    <th>Officer</th>
                    <th>Date & Time</th>
                </thead>
                <tbody>
                    <tr>
                        <td> 
                            <select name="name" class="form-control" required>
                                <option value="{{$user->name}}" selected>{{$user->name}}</option>
                                @foreach($officers as $off)
                                    <option value="{{ $off->name }}">{{ $off->name }}</option>
                                @endforeach
                            </select>
                        </td>
                        <td>
                            <input type="datetime-local" name="date_time" class="form-control" required value="{{Carbon\Carbon::now()->format('Y-m-d')."T".Carbon\Carbon::now()->format('H:i:s')}}">
                        </td>
                    </tr>
                    <tr>
                        <th class="text-warning" colspan="2">Location</th>
                    </tr>
                    <tr>
                        <td colspan="2"> 
                            <select name="location" class="form-control basic" required>
                                <option value="" selected>[Choose Location]</option>
                                @foreach($locations as $loc)
                                    <option value="{{ $loc->location }}">{{ $loc->location }}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th class="text-warning" colspan="2">Remarks</th>
                    </tr>
                    <tr>
                        <td colspan="2"><textarea name="remarks" class="form-control" cols="30" rows="4"></textarea></td>
                    </tr>
                </tbody>
            </table>

        </div>

        <div class="card-footer">
            <input type="submit" class="btn btn-block btn-primary">
        </div>

    </form>
</div>

<div class="card bg-dark text-white mb-3">
    <div class="card-header">
        SGMC Weekly Rounds Made Goal for: &nbsp;&nbsp; {{$start->format('n/j/y')}} - {{$end->format('n/j/y')}}
    </div>
    <div class="card-body">

        <div class="row mb-2">
            <div class="col-lg-6">
                <h6>Total: {{$goal}}</h6>                
            </div>
            <div class="col-lg-6 text-right">
                <h6>Goal: 1000</h6>
            </div>
        </div>

        <div class="progress">
            <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="{{$goal / 10}}" aria-valuemin="0" aria-valuemax="1000" style="width: {{$goal / 10}}%"> {{$goal / 10}}%</div>
        </div>
        
    </div>
</div>

@endsection

@section('scripts')

    {{-- 2 --}}
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script type="text/javascript">

    $(".basic").select2({
        allowClear: true
    });

</script>


@endsection


@endif