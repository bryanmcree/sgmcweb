@if ( Auth::user()->hasRole('SGMC Safety & Security') == FALSE)
    @include('layouts.unauthorized')
@Else

@extends('layouts.app')

    {{-- 2 --}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

@section('content')

    @include('sgmcsecurity.nav.nav')

    <div class="card bg-dark text-white mb-3 mx-auto" style="width:50%">
        <div class="card-header">
            <h4>Dispatcher Daily Log</h4>
        </div>
        <div class="card-body">
    
            <form action="/security/dispatch/create" method="POST">
                {{ csrf_field() }}


                <table class="table table-dark table-hover table-bordered">
                <thead>
                    <th style="width:30%;">Description</th>
                    <th>Input</th>
                </thead>
                <tbody>
                    <tr>
                        <td>Date</td>
                        <td> 
                            <input type="date" name="date" class="form-control" required value= {{ \Carbon\Carbon::now() }}>
                        </td>
                    </tr>
                    <tr>
                        <td>Dispatched</td>
                        <td> 
                            <input type="time" name="dispatched" class="form-control" required>
                        </td>
                    </tr>
                    <tr>
                        <td>Arrived</td>
                        <td> 
                            <input type="time" name="arrived" class="form-control" required>
                        </td>
                    </tr>
                    <tr>
                        <td>Activity</td>
                        <td> 
                            <select name="activity" class="form-control basic" required>
                                <option value="" selected>[Choose Activity]</option>
                                @foreach($activities as $act)
                                    <option value="{{ $act->activity }}">{{ $act->activity }}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Location</td>
                        <td> 
                            <select name="location" class="form-control basic" required>
                                <option value="" selected>[Choose Location]</option>
                                @foreach($locations as $loc)
                                    <option value="{{ $loc->location }}">{{ $loc->location }}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Room #</td>
                        <td> 
                            <input type="text" class="form-control" name="room_num">
                        </td>
                    </tr>
                    <tr>
                        <td>Workplace Violence?</td>
                        <td> 
                            <select name="wpv" class="form-control" required>
                                <option value="" selected>[Y/N]</option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                                <option value="N/A">N/A</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Officer or Employee</td>
                        <td> 
                            <select name="officer" class="form-control basic" required>
                                <option value="" selected>[Choose Officer]</option>
                                @foreach($officers as $off)
                                    <option value="{{ $off->name }}">{{ $off->name }}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Dispatcher</td>
                        <td> 
                            <select name="dispatcher" class="form-control basic" required>
                                <option value="" selected>[Choose Dispatcher]</option>
                                @foreach($officers as $off)
                                    <option value="{{ $off->name }}">{{ $off->name }}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Remarks</td>
                        <td> 
                            <textarea name="remarks" class="form-control" cols="30" rows="5"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>Completed</td>
                        <td> 
                            <input type="time" class="form-control" name="completed">
                        </td>
                    </tr>

                </tbody>
            </table>

            <hr>

            <input type="submit" class="btn btn-primary btn-block">

            </form>
        </div>
    </div>

@endsection

@section('scripts')

    {{-- 2 --}}
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script type="text/javascript">

    $(".basic").select2({
        allowClear: true
    });

</script>


@endsection


@endif