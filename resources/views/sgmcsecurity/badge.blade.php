@if ( Auth::user()->hasRole('SGMC Safety & Security') == FALSE)
    @include('layouts.unauthorized')
@Else

@extends('layouts.app')

@section('content')

    @include('sgmcsecurity.nav.nav')

<div class="card bg-dark text-white mb-3 m-auto" style="width: 60%">
    <div class="card-header">
        <h3>After Hours Visitor Log</h3>
    </div>
    <div class="card-body">
    
        <form action="/security/badge/issue" method="POST">
            <input type="hidden" name="user_id" value="{{ $user->employee_number }}">
            {{ csrf_field() }}

            <div class="form-row">
                <div class="form-group col-lg-6">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control" readonly required value="{{ $user->name }}">
                </div>
                <div class="form-group col-lg-6">
                    <label for="date">Date</label>
                    <input type="date" name="date" class="form-control" required>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-lg-6">
                    <label for="unit">Unit #</label>
                    <input type="text" name="unit" class="form-control" required>
                </div>
                <div class="form-group col-lg-6">
                    <label for="radio">Radio #</label>
                    <input type="text" name="radio" class="form-control" required>
                </div>
            </div>

            <div class="form-row text-center">
                <div class="form-group col-lg-4">
                    <label for="">Subtract</label>
                    <input type="Button" class="btn btn-danger btn-block" id='minusButton' value="Subtract -"> 
                </div>
                <div class="form-group col-lg-4">
                    <label for="badges">TOTAL badges issued</label>
                    <input type="text" class="form-control text-center" name="total" id="TextBox" value="0" required>
                </div>
                <div class="form-group col-lg-4">
                    <label for="">Add</label>
                     <input type="Button" class="btn btn-success btn-block" id='AddButton' value="Add +">
                </div>
            </div>

            <hr>
            <input type="submit" class="btn btn-primary btn-block" value="Submit">

        </form>
    </div>
</div>

<script src="http://code.jquery.com/jquery-1.4.3.min.js"></script>
<script>
    $(document).ready(function(){
        $('#AddButton').click( function() {
            var counter = $('#TextBox').val();
            counter++ ;
            $('#TextBox').val(counter);
        });

        $('#minusButton').click( function() {
            var counter = $('#TextBox').val();
            counter-- ;
            $('#TextBox').val(counter);
        });
    });
</script>

@endsection

@endif