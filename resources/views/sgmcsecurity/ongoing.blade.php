@if ( Auth::user()->hasRole('SGMC Safety & Security') == FALSE)
    @include('layouts.unauthorized')
@Else

@extends('layouts.app')

@section('content')

    @include('sgmcsecurity.nav.nav')
    
<div class="card bg-dark text-white mb-3">
    <div class="card-header">
        OPEN DISPATCHES
    </div>
    <div class="card-body">

        <table class="table table-dark table-hover table-bordered">
            <thead>
                <th>Category</th>
                <th>Date</th>
                <th>Location</th>
                <th>Officer</th>
                <th>Edit</th>
            </thead>
            <tbody>
                @foreach($on as $data)
                    <tr>
                        @if($data->activity == 'AT RISK PATIENT')
                            <td class="bg-danger">{{$data->activity}}</td>
                        @elseif($data->activity == 'FORENSIC PATIENT')
                            <td class="bg-info">{{$data->activity}}</td>
                        @elseif($data->activity == 'CODE F PATIENT')
                            <td class="bg-primary">{{$data->activity}}</td>
                        @elseif($data->activity == 'NO INFO PATIENT')
                            <td class="bg-warning">{{$data->activity}}</td>
                        @else
                            <td>{{$data->activity}}</td>
                        @endif
                        <td>{{ Carbon::parse($data->date)->format('n-j-Y') }}</td>
                        <td>{{ $data->location }} @if(!empty($data->room_num)) | Room: #{{ $data->room_num }} @endif </td>
                        <td>{{ $data->officer }}</td>
                        <td><a href="/security/dispatch/edit/{{$data->id}}" class="btn btn-sm btn-block btn-warning">Complete</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    </div>
    <div class="card-footer">
        *These dispatches have not been completed. Use the edit button to complete and submit these dispatches.
    </div>
</div>

<hr class="bg-warning">

<div class="card bg-dark text-white mb-3">
    <div class="card-header">
        COMPLETED DISPATCHES LAST 10 DAYS
    </div>
    <div class="card-body">

        <table class="table table-dark table-hover table-bordered" id="last10">
            <thead>
                <th>Category</th>
                <th>Date</th>
                <th>Location</th>
                <th>Officer</th>
                <th>Edit</th>
            </thead>
            <tbody>
                @foreach($allDis as $data)
                    <tr>
                        <td>{{ $data->activity }}</td>
                        <td>{{ Carbon::parse($data->date)->format('n-j-Y') }}</td>
                        <td>{{ $data->location }} @if(!empty($data->room_num)) {{ $data->room_num }} @endif </td>
                        <td>{{ $data->officer }}</td>
                        <td><a href="/security/dispatch/edit/{{$data->id}}" class="btn btn-sm btn-block btn-warning">Edit</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    </div>
    <div class="card-footer">
        *All dispatches for the last 10 days. Avaliable for editing.
    </div>
</div>


@endsection

@section('scripts')

<script type="application/javascript">
    $(document).ready(function() {
        $('#last10').DataTable( {
            "pageLength": 20,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>

@endsection

@endif