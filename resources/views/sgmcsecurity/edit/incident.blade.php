@if ( Auth::user()->hasRole('SGMC Safety & Security') == FALSE)
    @include('layouts.unauthorized')
@Else

@extends('layouts.app')

    {{-- 2 --}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

@section('content')

@include('sgmcsecurity.nav.nav')

    <div class="card bg-dark text-white mb-3">
        <div class="card-header">
            Edit Incident Report
        </div>
        <div class="card-body">

            <form action="/security/incident/update/{{$incident->id}}" method="POST">
                {{ csrf_field() }}

                <div class="form-row">

                    <div class="form-group col-lg-6">
                        <label for="reported_by">Reported By</label>
                        <input type="text" class="form-control" name="reported_by" value="{{$incident->reported_by}}">
                    </div>
                    <div class="form-group col-lg-6">
                        <label for="datetime">Date & Time</label>
                        <input type="datetime-local" name="datetime" class="form-control" value={{Carbon::parse($incident->datetime)->format('Y-m-d').'T'.Carbon::parse($incident->datetime)->format('H:i') }}>
                    </div>

                </div>

                <div class="form-row">

                    <div class="form-group col-lg-6">
                        <label for="incident">Incident</label>
                        <select name="incident" class="form-control basic" required>
                            <option value="{{$incident->incident}}" selected>{{$incident->incident}}</option>
                            @foreach($incidents as $inc)
                                <option value="{{$inc->incident}}">{{$inc->incident}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-lg-6">
                        <label for="location">Location</label>
                        <select name="location" class="form-control basic" required>
                            <option value="{{$incident->location}}" selected>{{$incident->location}}</option>
                            @foreach($locations as $loc)
                                <option value="{{ $loc->location }}">{{ $loc->location }}</option>
                            @endforeach
                        </select>
                    </div>

                </div>

                <div class="form-group">
                    <label for="comments">Comments</label>
                    <textarea name="comments" rows="5" class="form-control">{{$incident->comments}}</textarea>
                </div>

                <hr>

                <input type="submit" class="btn btn-primary btn-block" value="Update">

            </form>

        </div>
    </div>

@endsection

@section('scripts')

    {{-- 2 --}}
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script type="text/javascript">

    $(".basic").select2({
        allowClear: true
    });

</script>


@endsection


@endif