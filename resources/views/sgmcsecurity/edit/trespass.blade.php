

@if ( Auth::user()->hasRole('SGMC Safety & Security') == FALSE)
@include('layouts.unauthorized')
@Else

@extends('layouts.app')


@section('content')

@include('sgmcsecurity.nav.nav')

<div class="card bg-dark text-white mb-3">
    <div class="card-header">
        Edit Trespassing Incident
    </div>
    <div class="card-body">

        <div class="card bg-secondary mb-4">

            <form method="POST" action="/security/trespass/update/{{ $trespass->id }}">
                {{ csrf_field() }}

                <div class="card-body">

                    <div class="form-row">
                        <div class="form-group col-6">
                            <label for="">Trespasser's First Name</label>
                            <input type="text" class="form-control" name="Fname" value="{{ $trespass->Fname }}">
                        </div>
                        <div class="form-group col-6">
                            <label for="">Trespasser's Last Name</label>
                            <input type="text" class="form-control" name="Lname" value="{{ $trespass->Lname }}">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-4">
                            <label>Date & Time of the Trespassing</label>
                            <input type="datetime-local" class="form-control" name="date_time" value="{{ Carbon::parse($trespass->date_time)->format('Y-m-d')."T".Carbon::parse($trespass->date_time)->format('H:i') }}">
                        </div>
                        <div class="form-group col-4">
                            <label>Location of the Trespassing</label>
                            <select name="location" class="form-control">
                                <option value="{{ $trespass->location }}" selected>{{ $trespass->location }}</option>
                                @foreach ($locations as $location)
                                    <option value="{{ $location->location }}">{{ $location->location }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-4">
                            <label>Date Issued</label>
                            <input type="date" class="form-control" name="date_issued" value={{ $trespass->date_issued }}>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Comments</label>
                        <textarea name="comments" class="form-control" rows="5">{{ $trespass->comments }}</textarea>
                    </div>

                </div>
                <div class="card-footer">
                    <input type="submit" value="Update" class="btn btn-primary btn-block">
                </div>
            </form>
        </div>
    </div>
    
</div>

@endsection


@section('scripts')



@endsection

@endif