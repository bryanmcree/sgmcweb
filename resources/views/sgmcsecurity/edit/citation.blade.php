@if ( Auth::user()->hasRole('SGMC Safety & Security') == FALSE)
    @include('layouts.unauthorized')
@Else

@extends('layouts.app')

    {{-- 2 --}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

@section('content')

@include('sgmcsecurity.nav.nav')

<div class="card bg-dark text-white mb-3 m-auto">
    <div class="card-header">
        <h4>Edit Parking Citation</h4>
    </div>
    <div class="card-body">
    
        <form action="/security/citation/update/{{$citation->id}}" method="POST">
            {{ csrf_field() }}

            <div class="form-row">
                <div class="form-group col-lg-3">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control" readonly required value="{{ $citation->name }}">
                </div>
                <div class="form-group col-lg-3">
                    <label for="license_plate">License Plate #</label>
                    <input type="text" name="license_plate" class="form-control" required value="{{$citation->license_plate}}">
                </div>
                <div class="form-group col-lg-3">
                    <label for="citation_num">Citation #</label>
                    <input type="text" name="citation_num" class="form-control" required value="{{$citation->citation_num}}">
                </div>
                <div class="form-group col-lg-3">
                    <label for="date">Date</label>
                    <input type="date" name="date" class="form-control" value={{$citation->date}} required>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-lg-6">
                    <label for="Violation">Violation</label>
                    <select name="violation[]" class="form-control multi-2" multiple="multiple" required>
                        @foreach($violations as $violation)
                            <option value="{{$violation->violation}}" selected>{{$violation->violation}}</option>
                        @endforeach
                        <option value="no parking decal">NO PARKING DECAL</option>
                        <option value="exceeded authorized time limit">EXCEEDED AUTHORIZED TIME LIMIT</option>
                        <option value="parking in unauthorized area">PARKING IN UNAUTHORIZED AREA</option>
                        <option value="excessive speed for conditions">EXCESSIVE SPEED FOR CONDITIONS</option>
                        <option value="incorrect parking">INCORRECT PARKING</option>
                        <option value="handicapped parking">HANDICAPPED PARKING</option>
                        <option value="vehicle subject to be towed">VEHICLE SUBJECT TO BE TOWED</option>
                    </select>
                </div>
                <div class="form-group col-lg-6">
                    <label for="Location">Violator</label>
                    <select name="location" class="form-control" required>
                        <option value="{{$citation->location}}" selected>{{$citation->location}}</option>
                        <option value="sgmc">SGMC Employee</option>
                        <option value="vsu">VSU Student</option>
                        <option value="visitor">Visitor</option>
                        <option value="vendor">Vendor</option>
                        <option value="N/A">N/A</option>
                    </select>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-6">
                    <label>Violator First Name</label>
                    <input type="text" class="form-control" name="violator_Fname" value="{{$citation->violator_Fname}}">
                </div>
                <div class="form-group col-6">
                    <label>Violator Last Name</label>
                    <input type="text" class="form-control" name="violator_Lname" value="{{$citation->violator_Lname}}">
                </div>
            </div>

            <div class="form-group">
                <label for="comments">Comments</label>
                <textarea name="comments" rows="5" class="form-control">{{$citation->comments}}</textarea>
            </div>


            <hr>
            <input type="submit" class="btn btn-primary btn-block" value="Update Citation">

        </form>
    </div>
</div>

@endsection

@section('scripts')
    {{-- 2 --}}
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    <script>
        $(document).ready(function() {
            $(".multi-2").select2();
        });
    </script>

@endsection

@endif