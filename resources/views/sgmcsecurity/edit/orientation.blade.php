@if ( Auth::user()->hasRole('SGMC Safety & Security') == FALSE)
    @include('layouts.unauthorized')
@Else

@extends('layouts.app')

@section('content')
 
@include('sgmcsecurity.nav.nav')

<div class="card bg-dark text-white mb-3 mx-auto" style="width:60%;">
    <div class="card-header">
        Edit Forensic Officer Orientated
    </div>
    
    <form action="/security/orientation/update/{{ $orientation->id }}" method="POST">
        {{ csrf_field() }}

        <div class="card-body">

            <div class="form-row">
                <div class="form-group col-6">
                    <label for="Fname">Officer's First Name</label>
                    <input type="text" name="Fname" class="form-control" value="{{ $orientation->Fname }}">
                </div>
                <div class="form-group col-6">
                    <label for="Lname">Officer's Last Name</label>
                    <input type="text" name="Lname" class="form-control" value="{{ $orientation->Lname }}">
                </div>
            </div>
            <div class="form-group">
                <label for="date">Date</label>
                <input type="date" class="form-control" name="date" value={{ $orientation->date }}>
            </div>
            <div class="form-row">
                <div class="form-group col-6">
                    <label for="patient_name">Patient Name</label>
                    <input type="text" name="patient_name" class="form-control" value="{{ $orientation->patient_name }}">
                </div>
                <div class="form-group col-6">
                    <label for="correctional_facility">Correctional Facility</label>
                    <input type="text" name="correctional_facility" class="form-control" value="{{ $orientation->correctional_facility }}">
                </div>
            </div>
            <div class="form-group">
                <label for="comments">Comments</label>
                <textarea name="comments" rows="5" class="form-control">{{ $orientation->comments }}</textarea>
            </div>

        </div>

        <div class="card-footer">
            <input type="submit" class="btn btn-primary btn-block" value="Update">
        </div>

    </form>

</div>

@endsection

@endif