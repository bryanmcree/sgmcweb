@if ( Auth::user()->hasRole('SGMC Safety & Security') == FALSE)
    @include('layouts.unauthorized')
@Else

@extends('layouts.app')

    {{-- 2 --}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

@section('content')

    @include('sgmcsecurity.nav.nav')

<div class="card bg-dark text-white mb-3">
    <div class="card-header">
        <h4>Edit Round</h4>
    </div>

    <form action="/security/rounds/update/{{$round->id}}" method="POST">
        <div class="card-body">
            {{ csrf_field() }}

            <table class="table table-dark table-hover table-bordered">
                <thead class="text-warning">
                    <th>Officer</th>
                    <th>Date & Time</th>
                </thead>
                <tbody>
                    <tr>
                        <td> 
                            <select name="name" class="form-control" required>
                                <option value="{{$round->name}}" selected>{{$round->name}}</option>
                                @foreach($officers as $off)
                                    <option value="{{ $off->name }}">{{ $off->name }}</option>
                                @endforeach
                            </select>
                        </td>
                        <td>
                            <input type="datetime-local" name="date_time" class="form-control" required value="{{Carbon\Carbon::parse($round->date_time)->format('Y-m-d')."T".Carbon\Carbon::parse($round->date_time)->format('H:i:s')}}">
                        </td>
                    </tr>
                    <tr>
                        <th class="text-warning" colspan="2">Location</th>
                    </tr>
                    <tr>
                        <td colspan="2"> 
                            <select name="location" class="form-control basic" required>
                                <option value="{{ $round->location }}" selected>{{ $round->location }}</option>
                                @foreach($locations as $loc)
                                    <option value="{{ $loc->location }}">{{ $loc->location }}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th class="text-warning" colspan="2">Remarks</th>
                    </tr>
                    <tr>
                        <td colspan="2"><textarea name="remarks" class="form-control" cols="30" rows="4">{{ $round->remarks }}</textarea></td>
                    </tr>
                </tbody>
            </table>

        </div>

        <div class="card-footer">
            <input type="submit" class="btn btn-block btn-primary" value="Update">
        </div>

    </form>
</div>

@endsection

@section('scripts')

    {{-- 2 --}}
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script type="text/javascript">

    $(".basic").select2({
        allowClear: true
    });

</script>


@endsection


@endif