@if ( Auth::user()->hasRole('SGMC Safety & Security') == FALSE)
    @include('layouts.unauthorized')
@Else

@extends('layouts.new_nonav')

    {{-- 2 --}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

@section('content')

    @include('sgmcsecurity.nav.nav')

<div class="card bg-dark text-white mb-3">
    <div class="card-header">
        <h4>Dispatcher Daily Log Edit</h4>
    </div>
    <div class="card-body">

        <form action="/security/dispatch/update/{{$dis->id}}" method="POST">
            {{ csrf_field() }}

        <div class="form-row">
            <div class="form-group col-lg-4">
                <label for="date">Date</label>
                <input type="date" name="date" class="form-control" required value= {{ $dis->date }}>
            </div>
            <div class="form-group col-lg-4">
                <label for="dispatched">Dispatched</label>
                <input type="time" name="dispatched" class="form-control" required value= {{ Carbon::parse($dis->dispatched)->format('H:i:s') }}>
            </div>
            <div class="form-group col-lg-4">
                <label for="arrived">Arrived</label>
                <input type="time" name="arrived" class="form-control" required value= {{ Carbon::parse($dis->arrived)->format('H:i:s') }}>
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-lg-4">
                <label for="activity">Activity</label>
                <select name="activity" class="form-control basic" required>
                    <option value="{{$dis->activity}}" selected>{{$dis->activity}}</option>
                    @foreach($activities as $act)
                        <option value="{{ $act->activity }}">{{ $act->activity }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-4">
                <label for="location">Location</label>
                <select name="location" class="form-control basic" required>
                    <option value="{{$dis->location}}" selected>{{$dis->location}}</option>
                    @foreach($locations as $loc)
                        <option value="{{ $loc->location }}">{{ $loc->location }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-4">
                <label for="room_num">Room Number</label>
                <input type="text" class="form-control" name="room_num" value="{{ $dis->room_num }}">
            </div>
            
        </div>

        <div class="form-row">
            <div class="form-group col-lg-4">
                <label for="officer">Officer or Employee</label>
                <select name="officer" class="form-control basic" required>
                <option value="{{$dis->officer}}" selected>{{$dis->officer}}</option>
                    @foreach($officers as $off)
                        <option value="{{ $off->name }}">{{ $off->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-4">
                <label for="completed">Completed</label>
                <input type="time" class="form-control" name="completed" value={{ Carbon::parse($dis->completed)->format('H:i:s') }}>
            </div>            
            <div class="form-group col-lg-4">
                <label for="dispatcher">Dispatcher</label>
                <select name="dispatcher" class="form-control basic" required>
                    <option value="{{$dis->dispatcher}}" selected>{{$dis->dispatcher}}</option>
                    @foreach($officers as $off)
                        <option value="{{ $off->name }}">{{ $off->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-lg-12">
                <label for="remarks">Remarks</label>
                <textarea name="remarks" class="form-control" cols="30" rows="5">{{ $dis->remarks }}</textarea>
            </div>
        </div>

        <input type="submit" class="btn btn-success btn-block" value="Update">

        </form>
    </div>
</div>

@endsection

@section('scripts')

    {{-- 2 --}}
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script type="text/javascript">

    $(".basic").select2({
        allowClear: true
    });

</script>


@endsection

@endif