@if ( Auth::user()->hasRole('SGMC Safety & Security') == FALSE)
    @include('layouts.unauthorized')
@Else

@extends('layouts.app')

@section('content')
 
@include('sgmcsecurity.nav.nav')

<div class="card bg-dark text-white mb-3 mx-auto" style="width:60%;">
    <div class="card-header">
        Forensic Officer Orientated
    </div>
    
    <form action="/security/orientation/store" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="created_by" value="{{ Auth::user()->employee_number }}">

        <div class="card-body">

            <div class="form-row">
                <div class="form-group col-6">
                    <label for="Fname">Officer's First Name</label>
                    <input type="text" name="Fname" class="form-control" placeholder="First Name">
                </div>
                <div class="form-group col-6">
                    <label for="Lname">Officer's Last Name</label>
                    <input type="text" name="Lname" class="form-control" placeholder="Last Name">
                </div>
            </div>
            <div class="form-group">
                <label for="date">Date</label>
                <input type="date" class="form-control" name="date">
            </div>
            <div class="form-row">
                <div class="form-group col-6">
                    <label for="patient_name">Patient Name</label>
                    <input type="text" name="patient_name" class="form-control" placeholder="Patient Name">
                </div>
                <div class="form-group col-6">
                    <label for="correctional_facility">Correctional Facility</label>
                    <input type="text" name="correctional_facility" class="form-control" placeholder="Correctional Facility">
                </div>
            </div>
            <div class="form-group">
                <label for="comments">Comments</label>
                <textarea name="comments" rows="5" class="form-control"></textarea>
            </div>

        </div>

        <div class="card-footer">
            <input type="submit" class="btn btn-primary btn-block">
        </div>

    </form>

</div>

<div class="card bg-secondary mb-3">
    <div class="card-body">
        <table class="table table-dark table-hover table-bordered mb-0" id="orientationT">
            <thead>
                <th nowrap>Date & Time</th>
                <th nowrap>Officer Name</th>
                <th nowrap>Patient Name</th>
                <th nowrap>Correctional Facility</th>
                <th>Comments</th>
            </thead>
            <tbody>
                @foreach ($orientations as $orientation)
                    <tr>
                        <td nowrap>{{ Carbon::parse($orientation->date)->format('m-d-Y') }}</td>
                        <td nowrap>{{ $orientation->Fname . ' ' . $orientation->Lname }}</td>
                        <td nowrap>{{ $orientation->patient_name }}</td>
                        <td nowrap>{{ $orientation->correctional_facility }}</td>
                        <td>{{ $orientation->comments }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection

@section('scripts')

<script type="application/javascript">
    $(document).ready(function() {
        $('#orientationT').DataTable( {
            "pageLength": 10,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>

@endsection

@endif