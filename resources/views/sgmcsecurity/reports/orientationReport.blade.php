@if ( Auth::user()->hasRole('SGMC Safety & Security') == FALSE)
    @include('layouts.unauthorized')
@Else

@extends('layouts.new_nonav')

@section('content')

<div class="card bg-dark text-white mb-3">
    <div class="card-header">
        <h4>Officer Orientation Report for: <span class="text-primary">{{Carbon::parse($startDate)->format('M jS, Y')}} - {{Carbon::parse($endDate)->format('M jS, Y')}}</span></h4>
    </div>
    <div class="card-body">

        <div class="jumbotron bg-secondary">
            <h2>Total Orientations: @if(count($reportData) > 0) <span class="text-warning">{{count($reportData)}}</span> @else <span class="text-danger">NONE</span> @endif</h2>
        </div>

        <table class="table table-dark table-hover table-bordered" id="orientationT">
            <thead>
                <th nowrap>Name</th>
                <th nowrap>Patient</th>
                <th nowrap>Facility</th>
                <th nowrap>Date</th>
                <th>Comments</th>
                <th>Edit</th>
                <th>Delete</th>
            </thead>
            <tbody>
                @foreach($reportData as $data)
                    <tr>
                        <td nowrap>{{ $data->Fname }} {{ $data->Lname }}</td>
                        <td nowrap>{{ $data->patient_name }}</td>
                        <td nowrap>{{ $data->correctional_facility }}</td>
                        <td nowrap>{{ Carbon::parse($data->date)->format('n-j-Y') }}</td>
                        <td>{{ $data->comments }}</td>
                        <td><a href="/security/orientation/edit/{{$data->id}}" class="btn btn-sm btn-block btn-warning">Edit</a></td>
                        <td><a href="#" class="btn btn-danger btn-sm btn-block deletebtn" orientation_id={{$data->id}}>Delete</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <a href="/security" class="btn btn-primary">Return to Dashboard</a>

    </div>
</div>

@endsection

@section('scripts')

<script type="application/javascript">
    $(document).ready(function() {
        $('#orientationT').DataTable( {
            "pageLength": 20,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>

<script type="application/javascript">

$(document).on("click", ".deletebtn", function () {
            //alert($(this).attr("data-cost_center"));
            var orientation_id = $(this).attr("orientation_id");
            DeleteOrientation(orientation_id);
        });

        function DeleteOrientation(orientation_id) {
            swal({
                title: "Delete Officer Orientation?",
                text: "Deleting this Officer Orientation cannot be undone.",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                confirmButtonText: "Yes, delete it!",
                confirmButtonColor: "#ec6c62"
            }, function() {
                $.ajax({
                    url: "/security/orientation/destroy/" + orientation_id,
                    type: "GET"
                })
                    .done(function(data) {
                        swal({
                                title: "Deleted",
                                text: "Officer Orientation Deleted",
                                type: "success",
                                timer: 1800,
                                showConfirmButton: false

                            },function() {
                                location.reload();
                            });
                    })
                    .error(function(data) {
                        swal("Oops", "We couldn't connect to the server!", "error");
                    });
            });
        }

</script>

@endsection

@endif