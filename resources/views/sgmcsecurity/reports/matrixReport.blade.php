@if ( Auth::user()->hasRole('SGMC Safety & Security') == FALSE)
    @include('layouts.unauthorized')
@Else

@extends('layouts.new_nonav')

@section('content')

<div class="card mb-3">
    <div class="card-header">
        <h4>Matrix Report for: <span class="text-primary">{{Carbon::parse($startDate)->format('M jS, Y')}} - {{Carbon::parse($endDate)->format('M jS, Y')}}</span></h4>
    </div>
    <div class="card-body">

        <table class="table table-hover table-bordered" style="font-size: 20px;">
            <thead>
                <th style="width: 30%;">Category</th>
                <th>Total</th>
            </thead>
            <tbody>
                <tr>
                    <td>Request Door Unlock</td>
                    <td>{{ count($doors) }}</td>
                </tr>
                <tr>
                    <td>Weapons</td>
                    <td>{{ $weapon }}</td>
                </tr>
                <tr>
                    <td>At Risk Patients</td>
                    <td>{{ count($riskPT) }}</td>
                </tr>
                <tr>
                    <td>At Risk Patients Hrs</td>
                    <td>{{ $risk }} hours, {{ $min }} minutes</td>
                </tr>
                <tr>
                    <td>Crowd Control</td>
                    <td>{{ $cc }}</td>
                </tr>
                <tr>
                    <td>Safety Violations</td>
                    <td>{{ count($safety) }}</td>
                </tr>
                <tr>
                    <td>Parking Violations</td>
                    <td>{{ count($parking) }}</td>
                </tr>
                <tr>
                    <td>Escorts</td>
                    <td>{{ $escort }}</td>
                </tr>
                <tr>
                    <td>Code Grey</td>
                    <td>{{ $grey }}</td>
                </tr>
                <tr>
                    <td>Missing Property</td>
                    <td>{{ count($missing_item) }}</td>
                </tr>
                <tr>
                    <td>After Hours Visitor Screened</td>
                    <td>{{ $screened }}</td>
                </tr>
                <tr>
                    <td>Assist with Arrests</td>
                    <td>{{ count($arrests) }}</td>
                </tr>
                <tr>
                    <td>Break Ins</td>
                    <td>{{ count($breakIn) }}</td>
                </tr>
                <tr>
                    <td>Incident Reports</td>
                    <td> {{ $incidents }} </td>
                </tr>
                <tr>
                    <td>Forensic Patient</td>
                    <td>{{ count($forenPT) }}</td>
                </tr>
                <tr>
                    <td>Forensic Officer Oriented</td>
                    <td>{{ count($forenOFF) }}</td>
                </tr>
                <tr>
                    <td>Patient Valuable Pickups</td>
                    <td>{{ count($ptValUp) }}</td>
                </tr>
                <tr>
                    <td>Patient Valuable Returns</td>
                    <td>{{ count($ptValReturn) }}</td>
                </tr>
                <tr>
                    <td>Smoking Violation</td>
                    <td>{{ count($smoking) }}</td>
                </tr>
                {{-- <tr>
                    <td>Harassment (WPV)</td>
                    <td>{{ count($harass) }}</td>
                </tr>
                <tr>
                    <td>Threats (WPV)</td>
                    <td>{{ count($threat) }}</td>
                </tr>
                <tr>
                    <td>Disorder/Fight (WPV)</td>
                    <td>{{ count($disorderFight) }}</td>
                </tr> --}}
                <tr>
                    <td>Unauthorized Person</td>
                    <td>{{ count($unauthorized) }}</td>
                </tr>


            </tbody>
        </table>

        <a href="/security" class="btn btn-primary">Return to Dashboard</a>

    </div>
</div>

@endsection

@endif