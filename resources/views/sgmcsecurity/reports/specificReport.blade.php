@if ( Auth::user()->hasRole('SGMC Safety & Security') == FALSE)
    @include('layouts.unauthorized')
@Else

@extends('layouts.new_nonav')

@section('content')

<div class="card bg-dark text-white mb-3">
    <div class="card-header">
        Dispatch Report for: <span class="text-primary">{{Carbon::parse($startDate)->format('M jS, Y')}} - {{Carbon::parse($endDate)->format('M jS, Y')}}</span>
        <span class="float-right">
            <a href="/security/specific/export/{{ $startDate }}/{{ $endDate }}/{{ $activity }}" class="btn btn-success btn-sm">Export</a>
            <a href="/security" class="btn btn-primary btn-sm">Return to Dashboard</a>
        </span>
    </div>
    <div class="card-body">

        <div class="jumbotron bg-secondary">
            <h2>Total Dispatches: @if(count($reportData) > 0) <span class="text-warning">{{count($reportData)}}</span> @else <span class="text-danger">NONE</span> @endif</h2>
        </div>

        <div class="table-responsive">
            <table class="table table-dark table-hover table-bordered" id="dispatchT">
                <thead>
                    <th>Activity</th>
                    <th>Date</th>
                    <th>Location</th>
                    <th>Room Number</th>
                    <th>Officer</th>
                    <th>Dispatched</th>
                    <th>Completed</th>
                    <th>Time To Respond</th>
                    <th>Time To Complete</th>
                    <th>Edit</th>
                </thead>
                <tbody>
                    @if(is_null($reportData))
                        <tr>
                            <td colspan="9">No Data</td>
                        </tr>
                    @else 
                        @foreach($reportData as $data)
                            <tr>
                                <td>{{ $data->activity }}</td>
                                <td>{{ Carbon::parse($data->date)->format('n-j-Y') }}</td>
                                <td>{{ $data->location }}</td>
                                <td>@if(!empty($data->room_num)) {{ $data->room_num }} @else N/A @endif</td>
                                <td>{{ $data->officer }}</td>
                                <td>{{ Carbon::parse($data->dispatched)->format('H:i:s') }}</td>
                                <td>{{ Carbon::parse($data->completed)->format('H:i:s') }}</td>
                                <td>{{ Carbon::parse($data->response)->format('H:i:s') }}</td>
                                <td>{{ Carbon::parse($data->total)->format('H:i:s') }}</td>
                                <td><a href="/security/dispatch/edit/{{$data->id}}" class="btn btn-sm btn-block btn-warning">Edit</a></td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>

        <div class="table-responsive">
            <table class="table table-dark table-hover table-bordered">
                <thead>
                    <th>Activity</th>
                    <th>Total Time To Respond</th>
                </thead>
                <tbody>
                    <tr>
                        <td>@if(!is_null($reportData->first())) {{ $reportData->first()->activity }} @else N/A @endif</td>
                        <td>
                            @if($hour != 0) {{ $hour }} hour(s) & {{ $min }} minutes
                            @elseif($min != 0) {{ $min }} minutes
                            @else N/A
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <a href="/security" class="btn btn-primary">Return to Dashboard</a>

    </div>
</div>

@endsection

@section('scripts')

<script type="application/javascript">
    $(document).ready(function() {
        $('#dispatchT').DataTable( {
            "pageLength": 20,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>

@endsection

@endif