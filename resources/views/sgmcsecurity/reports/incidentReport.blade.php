@if ( Auth::user()->hasRole('SGMC Safety & Security') == FALSE)
    @include('layouts.unauthorized')
@Else

@extends('layouts.new_nonav')

@section('content')

<div class="card bg-dark text-white mb-3">
    <div class="card-header">
        <h4>Incident Report for: <span class="text-primary">{{Carbon::parse($startDate)->format('M jS, Y')}} - {{Carbon::parse($endDate)->format('M jS, Y')}}</span></h4>
    </div>
    <div class="card-body">

        <div class="jumbotron bg-secondary">
            <h2>Total Incidents: @if(count($reportData) > 0) <span class="text-warning">{{count($reportData)}}</span> @else <span class="text-danger">NONE</span> @endif</h2>
        </div>

        <table class="table table-dark table-hover table-bordered" id="incidentT">
            <thead>
                <th>File Number</th>
                <th>Incident</th>
                <th>Date & Time</th>
                <th>Location</th>
                <th>WPV?</th>
                <th>Entered by</th>
                <th>View</th>
                <th>Edit</th>
                <th>Delete</th>
            </thead>
            <tbody>
                @foreach($reportData as $data)
                    <tr>
                        <td>{{ $data->file_num }}</td>
                        <td>{{ $data->incident }}</td>
                        <td>{{ Carbon::parse($data->datetime)->format('n-j-Y  H:i') }}</td>
                        <td>{{ $data->location }}</td>
                        <td>{{ $data->wpv }}</td>
                        <td>{{ $data->user_name}}</td>
                        <td><a href="/security/incident/view/{{$data->id}}" class="btn btn-sm btn-block btn-info">View</a></td>
                        <td><a href="/security/incident/edit/{{$data->id}}" class="btn btn-sm btn-block btn-warning">Edit</a></td>
                        <td><a href="#" class="btn btn-danger btn-sm btn-block deletebtn" incident_id={{$data->id}}>Delete</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <a href="/security" class="btn btn-primary">Return to Dashboard</a>

    </div>
</div>

@endsection

@section('scripts')

<script type="application/javascript">
    $(document).ready(function() {
        $('#incidentT').DataTable( {
            "pageLength": 20,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>

<script type="application/javascript">

$(document).on("click", ".deletebtn", function () {
            //alert($(this).attr("data-cost_center"));
            var incident_id = $(this).attr("incident_id");
            DeleteIncident(incident_id);
        });

        function DeleteIncident(incident_id) {
            swal({
                title: "Delete Incident?",
                text: "Deleting this Incident cannot be undone.",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                confirmButtonText: "Yes, delete it!",
                confirmButtonColor: "#ec6c62"
            }, function() {
                $.ajax({
                    url: "/security/incidentReport/destroy/" + incident_id,
                    type: "GET"
                })
                    .done(function(data) {
                        swal({
                                title: "Deleted",
                                text: "Incident Deleted",
                                type: "success",
                                timer: 1800,
                                showConfirmButton: false

                            },function() {
                                location.reload();
                            });
                    })
                    .error(function(data) {
                        swal("Oops", "We couldn't connect to the server!", "error");
                    });
            });
        }

</script>

@endsection

@endif