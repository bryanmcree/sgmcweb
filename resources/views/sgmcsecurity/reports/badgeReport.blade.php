@if ( Auth::user()->hasRole('SGMC Safety & Security') == FALSE)
    @include('layouts.unauthorized')
@Else

@extends('layouts.new_nonav')

@section('content')

<div class="card bg-dark text-white mb-3">
    <div class="card-header">
        <h4>Badge Report for: <span class="text-primary">{{Carbon::parse($startDate)->format('M jS, Y')}} - {{Carbon::parse($endDate)->format('M jS, Y')}}</span></h4>
    </div>
    <div class="card-body">

        <div class="jumbotron bg-secondary">
            <h2>Total Badges Issued: @if($total > 0) <span class="text-warning">{{$total}}</span> @else <span class="text-danger">NONE</span> @endif</h2>
        </div>

        <table class="table table-dark table-hover" id="badgeT">
            <thead>
                <th>Date</th>
                <th>Name</th>
                <th>Issued</th>
                <th>Edit</th>
                <th>Delete</th>
            </thead>
            <tbody>
                @foreach($reportData as $data)
                <tr>
                    <td>{{ Carbon::parse($data->date)->format('n-j-Y') }}</td>
                    <td>{{ $data->name }}</td>
                    <td>{{ $data->total }}</td>
                    <td><a href="#" class="btn btn-sm btn-warning btn-block editBadge" 
                        data-id = {{$data->id}}
                        data-date = {{$data->date}}
                        data-name = "{{$data->name}}"
                        data-total = {{$data->total}} >Edit</a>
                    </td>
                    <td><a href="#" class="btn btn-danger btn-sm btn-block deleteBadge" badge_id = {{$data->id}}>Delete</a></td>
                </tr>
                @endforeach
            </tbody>
        </table>

        <a href="/security" class="btn btn-primary">Return to Dashboard</a>

    </div>
</div>

@include('sgmcsecurity.modals.badgeEdit')

@endsection

@section('scripts')

<script>

    //Pass medical alerts to modal
    $(document).on("click", ".editBadge", function () {
    $('.badge_id').val($(this).attr("data-id"));
    $('.badge_date').val($(this).attr("data-date"));
    $('.badge_name').val($(this).attr("data-name"));
    $('.badge_total').val($(this).attr("data-total"));
    $('#badgeModal').modal('show');
    });

</script>


<script type="application/javascript">

$(document).on("click", ".deleteBadge", function () {
        var badge_id = $(this).attr("badge_id");
        DeleteBadge(badge_id);
    });

function DeleteBadge(badge_id) {
    swal({
        title: "Delete badge?",
        text: "Deleting this badge cannot be undone.",
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "Yes, delete it!",
        confirmButtonColor: "#ec6c62"
    }, function() {
        $.ajax({
            url: "/security/badge/destroy/" + badge_id,
            type: "GET"
        })
            .done(function(data) {
                swal({
                        title: "Deleted",
                        text: "Badge Deleted",
                        type: "success",
                        timer: 1800,
                        showConfirmButton: false

                    },function() {
                        location.reload();
                    });
            })
            .error(function(data) {
                swal("Oops", "We couldn't connect to the server!", "error");
            });
    });
}

</script>

<script type="application/javascript">
    $(document).ready(function() {
        $('#badgeT').DataTable( {
            "pageLength": 20,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>

@endsection

@endif