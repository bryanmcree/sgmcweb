@if ( Auth::user()->hasRole('SGMC Safety & Security') == FALSE)
    @include('layouts.unauthorized')
@Else

@extends('layouts.new_nonav')

@section('content')

<div class="card bg-dark text-white mb-3">
    <div class="card-header">
        Dispatch Report for: <span class="text-primary">{{Carbon::parse($startDate)->format('M jS, Y')}} - {{Carbon::parse($endDate)->format('M jS, Y')}}</span>
        <span class="float-right"> <a href="/security" class="btn btn-sm btn-primary">Return to Dashboard</a> </span>
    </div>
    <div class="card-body">

        <div class="jumbotron bg-secondary">
            <h2>Total WPV: {{ count($dispatchData) + count($incidentData) }}</h2>
        </div>

        <table class="table table-dark table-hover table-bordered" id="dispatchT">
            <thead>
                <th>Activity</th>
                <th>Date</th>
                <th>Location</th>
                <th>Room Number</th>
                <th>WPV?</th>
                <th>Officer</th>
                <th>Edit</th>
                <th>View Details</th>
                <th>Delete</th>
            </thead>
            <tbody>
                @foreach($dispatchData as $data)
                    <tr>
                        <td nowrap>{{ $data->activity }}</td>
                        <td nowrap>{{ Carbon::parse($data->date)->format('n-j-Y') }}</td>
                        <td>{{ $data->location }}</td>
                        <td>@if(!empty($data->room_num)) {{ $data->room_num }} @else N/A @endif</td>
                        <td>{{ $data->wpv }}</td>
                        <td nowrap>{{ $data->officer }}</td>
                        <td><a href="/security/dispatch/edit/{{$data->id}}" class="btn btn-sm btn-block btn-warning">Edit</a></td>
                        <td><a href="/security/dispatch/show/{{$data->id}}" class="btn btn-sm btn-block btn-info">View</a></td>
                        <td><a href="#" dispatch_id="{{$data->id}}" class="btn btn-sm btn-block btn-danger deleteDispatch">Delete</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <table class="table table-dark table-hover table-bordered" id="incidentT">
            <thead>
                <th>File Number</th>
                <th>Incident</th>
                <th>Date & Time</th>
                <th>Location</th>
                <th>WPV?</th>
                <th>Verbal?</th>
                <th>Person Involved</th>
                <th>Entered by</th>
                <th>View</th>
                <th>Edit</th>
                <th>Delete</th>
            </thead>
            <tbody>
                @foreach($incidentData as $data)
                    <tr>
                        <td>{{ $data->file_num }}</td>
                        <td>{{ $data->incident }}</td>
                        <td>{{ Carbon::parse($data->datetime)->format('n-j-Y  H:i') }}</td>
                        <td>{{ $data->location }}</td>
                        <td>{{ $data->wpv }}</td>
                        <td>{{ $data->verbal }}</td>
                        <td>{{ $data->involved }}</td>
                        <td>{{ $data->user_name}}</td>
                        <td><a href="/security/incident/view/{{$data->id}}" class="btn btn-sm btn-block btn-info">View</a></td>
                        <td><a href="/security/incident/edit/{{$data->id}}" class="btn btn-sm btn-block btn-warning">Edit</a></td>
                        <td><a href="#" class="btn btn-danger btn-sm btn-block deletebtn" incident_id={{$data->id}}>Delete</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    </div>
</div>

@endsection

@section('scripts')

<script type="application/javascript">
    $(document).ready(function() {
        $('#dispatchT').DataTable( {
            "pageLength": 20,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>

<script type="application/javascript">
    $(document).ready(function() {
        $('#incidentT').DataTable( {
            "pageLength": 20,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>

<script type="application/javascript">

$(document).on("click", ".deleteDispatch", function () {
    //alert($(this).attr("data-cost_center"));
    var dispatch_id = $(this).attr("dispatch_id");
    DeleteDispatch(dispatch_id);
});

function DeleteDispatch(dispatch_id) {
    swal({
        title: "Delete Dispatch?",
        text: "Deleting this Dispatch cannot be undone.",
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "Yes, delete it!",
        confirmButtonColor: "#ec6c62"
    }, function() {
        $.ajax({
            url: "/security/dispatch/destroy/" + dispatch_id,
            type: "GET"
        })
            .done(function(data) {
                swal({
                        title: "Deleted",
                        text: "Dispatch Deleted",
                        type: "success",
                        timer: 1800,
                        showConfirmButton: false

                    },function() {
                        location.reload();
                    });
            })
            .error(function(data) {
                swal("Oops", "We couldn't connect to the server!", "error");
            });
    });
}

</script>


@endsection

@endif