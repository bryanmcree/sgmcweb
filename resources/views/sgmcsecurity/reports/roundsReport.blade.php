@if ( Auth::user()->hasRole('SGMC Safety & Security') == FALSE)
    @include('layouts.unauthorized')
@Else

@extends('layouts.new_nonav')

@section('content')

<div class="card bg-dark text-white mb-3">
    <div class="card-header">
        <h4>Rounds Report for: <span class="text-primary">{{Carbon::parse($startDate)->format('M jS, Y')}} - {{Carbon::parse($endDate)->format('M jS, Y')}}</span></h4>
    </div>
    <div class="card-body">

        <div class="jumbotron bg-secondary">
            <h2>Total Rounds Made: @if(count($reportData) > 0) <span class="text-warning">{{count($reportData)}}</span> @else <span class="text-danger">NONE</span> @endif</h2>
        </div>

        <table class="table table-dark table-hover table-bordered" id="roundsT">
            <thead>
                <th>Officer</th>
                <th>Location</th>
                <th>DateTime</th>
                <th>Remarks</th>
                <th>Edit</th>
                <th>Delete</th>
            </thead>
            <tbody>
                @foreach($reportData as $data)
                    <tr>
                        <td nowrap>{{ $data->name }}</td>
                        <td>{{ $data->location }}</td>
                        <td nowrap>{{ Carbon::parse($data->date_time)->format('n-j-Y H:i') }}</td>
                        <td>{{ $data->remarks }}</td>
                        <td> <a href="/security/rounds/edit/{{$data->id}}" class="btn btn-warning btn-sm">Edit</a> </td>
                        <td> <a href="#" class="btn btn-danger btn-sm deleteRound" round_id={{$data->id}}>Delete</a> </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <a href="/security" class="btn btn-primary">Return to Dashboard</a>

    </div>
</div>

@endsection

@section('scripts')

<script type="application/javascript">
    $(document).ready(function() {
        $('#roundsT').DataTable( {
            "pageLength": 20,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>

<script type="application/javascript">

$(document).on("click", ".deleteRound", function () {
        var round_id = $(this).attr("round_id");
        DeleteCitation(round_id);
    });

function DeleteCitation(round_id) {
    swal({
        title: "Delete Round?",
        text: "Deleting this Round cannot be undone.",
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "Yes, delete it!",
        confirmButtonColor: "#ec6c62"
    }, function() {
        $.ajax({
            url: "/security/rounds/destroy/" + round_id,
            type: "GET"
        })
            .done(function(data) {
                swal({
                        title: "Deleted",
                        text: "Round Deleted",
                        type: "success",
                        timer: 1800,
                        showConfirmButton: false

                    },function() {
                        location.reload();
                    });
            })
            .error(function(data) {
                swal("Oops", "We couldn't connect to the server!", "error");
            });
    });
}

</script>

@endsection

@endif