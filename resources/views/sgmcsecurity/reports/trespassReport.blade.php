@if ( Auth::user()->hasRole('SGMC Safety & Security') == FALSE)
    @include('layouts.unauthorized')
@Else

@extends('layouts.new_nonav')

@section('content')

<div class="card bg-dark text-white mb-3">
    <div class="card-header">
        <h4>Trespassing Report for: <span class="text-primary">{{Carbon::parse($startDate)->format('M jS, Y')}} - {{Carbon::parse($endDate)->format('M jS, Y')}}</span></h4>
    </div>
    <div class="card-body">

        <div class="jumbotron bg-secondary">
            <h2>Total Trespasses: @if(count($reportData) > 0) <span class="text-warning">{{count($reportData)}}</span> @else <span class="text-danger">NONE</span> @endif</h2>
        </div>

        <table class="table table-dark table-hover table-bordered" id="trespassT">
            <thead>
                <th>Date & Time</th>
                <th>Trespasser</th>
                <th>Date Issued</th>
                <th>Location</th>
                <th>View</th>
                <th>Edit</th>
                <th>Delete</th>
            </thead>
            <tbody>
                @foreach ($reportData as $trespass)
                    <tr>
                        <td>{{ Carbon::parse($trespass->date_time)->format('m-d-Y H:i') }}</td>
                        <td>{{ $trespass->Fname . ' ' . $trespass->Lname }}</td>
                        <td>{{ Carbon::parse($trespass->date_issued)->format('m-d-Y') }}</td>
                        <td>{{ $trespass->location }}</td>
                        <td> <a href="/security/trespass/show/{{ $trespass->id }}" class="btn btn-sm btn-info btn-block">Details</a> </td>
                        <td> <a href="/security/trespass/edit/{{ $trespass->id }}" class="btn btn-sm btn-warning btn-block">Edit</a> </td>
                        <td> <a href="#" class="btn btn-danger btn-sm btn-block deleteTrespass" trespass_id="{{ $trespass->id }}">Delete</a> </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <a href="/security" class="btn btn-primary">Return to Dashboard</a>

    </div>
</div>

@endsection

@section('scripts')

<script type="application/javascript">
    $(document).ready(function() {
        $('#trespassT').DataTable( {
            "pageLength": 20,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>

<script type="application/javascript">

$(document).on("click", ".deleteTrespass", function () {
            //alert($(this).attr("data-cost_center"));
            var trespass_id = $(this).attr("trespass_id");
            DeleteTrespass(trespass_id);
        });

        function DeleteTrespass(trespass_id) {
            swal({
                title: "Delete Trespass?",
                text: "Deleting this Trespass cannot be undone.",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                confirmButtonText: "Yes, delete it!",
                confirmButtonColor: "#ec6c62"
            }, function() {
                $.ajax({
                    url: "/security/trespass/destroy/" + trespass_id,
                    type: "GET"
                })
                    .done(function(data) {
                        swal({
                                title: "Deleted",
                                text: "Trespassing Deleted",
                                type: "success",
                                timer: 1800,
                                showConfirmButton: false

                            },function() {
                                location.reload();
                            });
                    })
                    .error(function(data) {
                        swal("Oops", "We couldn't connect to the server!", "error");
                    });
            });
        }

</script>

@endsection

@endif