@if ( Auth::user()->hasRole('SGMC Safety & Security') == FALSE)
    @include('layouts.unauthorized')
@Else

@extends('layouts.new_nonav')

@section('content')

<div class="card bg-dark text-white mb-3">
    <div class="card-body">

        <table class="table table-dark table-hover table-bordered" id="dispatchT">
            <thead>
                <th>Category</th>
                <th>Data</th>
            </thead>
            <tbody>
                <tr>
                    <td>Activity</td>
                    <td>{{ $dispatch->activity }}</td>
                </tr>
                <tr>
                    <td>Date</td>
                    <td>{{ Carbon::parse($dispatch->date)->format('n-j-Y') }}</td>
                </tr>
                <tr>
                    <td>Dispatched</td>
                    <td>{{ Carbon::parse($dispatch->dispatched)->format('H:i:s') }}</td>
                </tr>
                <tr>
                    <td>Arrived</td>
                    <td>{{ Carbon::parse($dispatch->arrived)->format('H:i:s') }}</td>
                </tr>
                <tr>
                    <td>Location</td>
                    <td>{{ $dispatch->location }}</td>
                </tr>
                <tr>
                    <td>Room Number</td>
                    <td>@if(!empty($dispatch->room_num)) {{ $dispatch->room_num }} @else N/A @endif</td>
                </tr>
                <tr>
                    <td>Officer</td>
                    <td>{{ $dispatch->officer }}</td>
                </tr>
                <tr>
                    <td>Completed</td>
                    <td>{{ Carbon::parse($dispatch->completed)->format('H:i:s') }}</td>
                </tr>
                <tr>
                    <td>Response</td>
                    <td>{{ Carbon::parse($dispatch->response)->format('H:i:s') }}</td>
                </tr>
                <tr>
                    <td>Total</td>
                    <td>{{ Carbon::parse($dispatch->total)->format('H:i:s') }}</td>
                </tr>
                <tr>
                    <td>Dispatcher</td>
                    <td>{{ $dispatch->dispatcher }}</td>
                </tr>
                <tr>
                    <td>Remarks</td>
                    <td>{{ $dispatch->remarks }}</td>
                </tr>

            </tbody>
        </table>

        <a href="/security" class="btn btn-primary">Return to Dashboard</a>

    </div>
</div>

@endsection

@section('scripts')

<script type="application/javascript">
    $(document).ready(function() {
        $('#dispatchT').DataTable( {
            "pageLength": 20,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>

@endsection

@endif