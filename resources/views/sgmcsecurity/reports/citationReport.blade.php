@if ( Auth::user()->hasRole('SGMC Safety & Security') == FALSE)
    @include('layouts.unauthorized')
@Else

@extends('layouts.new_nonav')

@section('content')

<div class="card bg-dark text-white mb-3">
    <div class="card-header">
        <h4>Citation Report for: <span class="text-primary">{{Carbon::parse($startDate)->format('M jS, Y')}} - {{Carbon::parse($endDate)->format('M jS, Y')}}</span></h4>
    </div>
    <div class="card-body">

        <div class="jumbotron bg-secondary">
            <h2>Total Citations Issued: @if(count($reportData) > 0) <span class="text-warning">{{count($reportData)}}</span> @else <span class="text-danger">NONE</span> @endif</h2>
        </div>

        <div class="jumbotron bg-secondary">
            <h3>SGMC Citations Issued: @if(count($sgmcData) > 0) <span class="text-warning">{{count($sgmcData)}}</span> @else <span class="text-danger">NONE</span> @endif</h3>
        </div>

        <div class="jumbotron bg-secondary">
            <h3>VSU Citations Issued: @if(count($vsuData) > 0) <span class="text-warning">{{count($vsuData)}}</span> @else <span class="text-danger">NONE</span> @endif</h3>
        </div>

        <div class="table-responsive">
            {{-- <table class="table table-dark table-bordered">
                <thead>
                    <th>No Parking Decal</th>
                    <th>Exceeded Authorized Time Limit</th>
                    <th>Parking In Unauthorized Area</th>
                    <th>Excessive Speed For Conditions</th>
                    <th>Incorrect Parking</th>
                    <th>Handicapped Parking</th>
                    <th>Vehicle Subject To Be Towed</th>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ count($noDecal) }}</td>
                        <td>{{ count($timeLimit) }}</td>
                        <td>{{ count($unauthorized) }}</td>
                        <td>{{ count($speeding) }}</td>
                        <td>{{ count($badParking) }}</td>
                        <td>{{ count($handicappedParking) }}</td>
                        <td>{{ count($tow) }}</td>
                    </tr>
                </tbody>
            </table> --}}
        </div>

        <div class="table-responsive">
            <table class="table table-dark table-hover table-bordered" id="citationT">
                <thead>
                    <th class="text-center">Citation #</th>
                    <th>Officer</th>
                    <th>Date</th>
                    <th>Violator</th>
                    <th>Violation</th>
                    <th>Comments</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </thead>
                <tbody>
                    @forelse($reportData as $data)
                        <tr>
                            <td class="text-center">{{$data->citation_num}}</td>
                            <td>{{$data->name}}</td>
                            <td>{{Carbon::parse($data->date)->format('n-j-Y')}}</td>
                            <td>{{$data->location}}</td>
                            <td>
                                @foreach($data->violations as $violation)
                                    - {{$violation->violation}}
                                    <br>
                                @endforeach
                            </td>
                            <td>{{$data->comments}}</td>
                            <td><a href="/security/citation/edit/{{$data->id}}" class="btn btn-block btn-warning btn-sm">Edit</a></td>
                            <td><a href="#" class="btn btn-danger btn-block btn-sm deleteCitation" citation_id = {{$data->id}}>Delete</a></td>
                        </tr>
                    @empty
                        <tr>
                            <td class="text-center" colspan="8"> No Data Available</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>

        <a href="/security" class="btn btn-primary">Return to Dashboard</a>

    </div>
</div>

@endsection


@section('scripts')

<script type="application/javascript">

$(document).on("click", ".deleteCitation", function () {
        var citation_id = $(this).attr("citation_id");
        DeleteCitation(citation_id);
    });

function DeleteCitation(citation_id) {
    swal({
        title: "Delete citation?",
        text: "Deleting this citation cannot be undone.",
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "Yes, delete it!",
        confirmButtonColor: "#ec6c62"
    }, function() {
        $.ajax({
            url: "/security/citation/destroy/" + citation_id,
            type: "GET"
        })
            .done(function(data) {
                swal({
                        title: "Deleted",
                        text: "Citation Deleted",
                        type: "success",
                        timer: 1800,
                        showConfirmButton: false

                    },function() {
                        location.reload();
                    });
            })
            .error(function(data) {
                swal("Oops", "We couldn't connect to the server!", "error");
            });
    });
}

</script>

<script type="application/javascript">
    $(document).ready(function() {
        $('#citationT').DataTable( {
            "pageLength": 20,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>

@endsection


@endif