<div class="modal fade" id="badgeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content text-white bg-dark">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Badges</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="/security/badge/update">
                {{ csrf_field() }}
                <input type="hidden" name="id" class="badge_id">
                <div class="modal-body">
                    <div class="card border-danger text-white bg-dark">
                        <div class="card-body">

                            <div class="form-group">
                                <label for="date">Date</label>
                                <input type="date" name="date" class="form-control badge_date">
                            </div>
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" name="name" class="form-control badge_name">
                            </div>
                            <div class="form-group">
                                <label for="total">Total</label>
                                <input type="number" name="total" class="form-control badge_total">
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Update Badge">
                </div>
            </form>
        </div>
    </div>
</div>