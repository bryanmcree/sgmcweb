<!-- Modal -->
<div class="modal fade" id="securityReport" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content text-white bg-dark">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Security Report</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="/security/report">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Select Report Type</label>
                        <select class="form-control" name="report" id="exampleFormControlSelect1" required>
                            <option value="" selected>[Select Type]</option>
                            <option value="badge">Badge Issued Report</option>
                            <option value="citation">Citations Issued Report</option>
                            <option value="dispatch">Dispatch Report</option>
                            <option value="rounds">Rounds Report</option>
                            <option value="incident">Incident Report</option>
                            <option value="trespass">Trespassing Report</option>
                            <option value="orientation">Officer Orientation Report</option>
                            <option value="wpv">WPV Report</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Select Location</label>
                        <select class="form-control" name="location" id="exampleFormControlSelect1">
                            <option value="" selected>[Optional]</option>
                            @foreach ($locations as $location)
                                <option value="{{ $location->location }}">{{ $location->location }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Start Date / Time</label>
                        <input type="date" name="startDate" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1">End Date / Time</label>
                        <input type="date" name="endDate" class="form-control" required value= {{ \Carbon\Carbon::now() }}>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Run Report">
                </div>
            </form>
        </div>
    </div>
</div>