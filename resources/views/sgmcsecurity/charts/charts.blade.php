
{{-- badges --}}
@if(!empty($chartBadges))
new Chart(document.getElementById("badgeChart"), {
type: 'line',
data: {
labels: [
@foreach($chartBadges as $badge)
    "{{Carbon::parse($badge->date)->format('m-d-Y')}}",
@endforeach
],
datasets: [{
data: [
@foreach($chartBadges as $badge)
    {{$badge->total}},
@endforeach
],
label: "Number of Badges Issued",
borderColor: "#8d3dff",
fill: false
}
]
},
options: {
title: {
display: true,
text: 'Badges Issued Last 5 Days'
},
scales: {
yAxes: [{
stacked:false,
ticks: {
beginAtZero: true
}
}]
}
}
});
@endif

{{-- citations --}}
new Chart(document.getElementById("citationChart"), {
type: 'line',
data: {
labels: [
    '6 weeks ago','5 weeks ago','4 weeks ago','3 weeks ago','2 weeks ago','Last 7 Days'
],
datasets: [{
data: [
    {{$chartCitations6}},{{$chartCitations5}},{{$chartCitations4}},{{$chartCitations3}},{{$chartCitations2}},{{$chartCitations1}}
],
label: "Number of Citations Issued",
borderColor: "#8d3dff",
fill: false
}
]
},
options: {
title: {
display: true,
text: 'Citations Issued Last 6 Weeks'
},
scales: {
yAxes: [{
stacked:false,
ticks: {
beginAtZero: true
}
}]
}
}
});

 {{-- dispatch --}}
new Chart(document.getElementById("dispatchChart"), {
type: 'line',
data: {
labels: [
    '6 weeks ago','5 weeks ago','4 weeks ago','3 weeks ago','2 weeks ago','Last 7 Days'
],
datasets: [{
data: [
    {{$chartDispatches6}},{{$chartDispatches5}},{{$chartDispatches4}},{{$chartDispatches3}},{{$chartDispatches2}},{{$chartDispatches1}}
],
label: "Number of Dispatches",
borderColor: "#8d3dff",
fill: false
}
]
},
options: {
title: {
display: true,
text: 'Dispatches Last 6 Weeks'
},
scales: {
yAxes: [{
stacked:false,
ticks: {
beginAtZero: true
}
}]
}
}
});

 {{-- rounds --}}
new Chart(document.getElementById("roundsChart"), {
type: 'line',
data: {
labels: [
    '5 days ago','4 days ago','3 days ago','2 days ago','Last 24 hours'
],
datasets: [{
data: [
    {{$rounds5}},{{$rounds4}},{{$rounds3}},{{$rounds2}},{{$rounds1}}
],
label: "Number of Rounds",
borderColor: "#8d3dff",
fill: false
}
]
},
options: {
title: {
display: true,
text: 'Rounds Last 5 days'
},
scales: {
yAxes: [{
stacked:false,
ticks: {
beginAtZero: true
}
}]
}
}
});


{{-- incidents --}}
new Chart(document.getElementById("incidentChart"), {
type: 'line',
data: {
labels: [
    '6 weeks ago','5 weeks ago','4 weeks ago','3 weeks ago','2 weeks ago','Last 7 Days'
],
datasets: [{
data: [
    {{$chartIncidents6}},{{$chartIncidents5}},{{$chartIncidents4}},{{$chartIncidents3}},{{$chartIncidents2}},{{$chartIncidents1}}
],
label: "Number of Incidents Issued",
borderColor: "#8d3dff",
fill: false
}
]
},
options: {
title: {
display: true,
text: 'Incidents Issued Last 6 Weeks'
},
scales: {
yAxes: [{
stacked:false,
ticks: {
beginAtZero: true
}
}]
}
}
});