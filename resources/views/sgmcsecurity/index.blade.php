@if ( Auth::user()->hasRole('SGMC Safety & Security') == FALSE)
    @include('layouts.unauthorized')
@Else

@extends('layouts.app')

@section('content')

@if(count($on) > 0)
    <div class="alert bg-danger col-6 mx-auto">There are ongoing dispatches active! Please review the <a href="/security/ongoing" class="text-white">'Ongoing Dispatches'</a> tab to complete and submit these dispatches!</div>
@endif

    @include('sgmcsecurity.nav.nav')

<div class="card bg-dark text-white mb-3">
    
    <div class="card-header">
        Security Dashboard 

        @if(Auth::user()->hasRole('SGMC Safety & Security Admin'))
            <div class="float-right">
                <a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#securityReport" aria-selected="false">Generate Report</a>
                <a class="btn btn-info btn-sm" data-toggle="modal" data-target="#securitySpecific" aria-selected="false">Specific Report</a>
                <a class="btn btn-success btn-sm" data-toggle="modal" data-target="#securityMatrix" aria-selected="false">Security Matrix</a>
            </div>
        @endif
    </div>

    <div class="card-body">

    <div class="card-deck mb-3">
        <div class="card bg-dark text-white col-lg-6 border-primary">
            <div class="card-header border-primary">
                Dispatches Last 7 days
            </div>
            <div class="card-body">
                <span style="font-size: 26px;">@if(count($dispatches) > 0) {{count($dispatches)}} @else NONE @endif</span>
            </div>
        </div>
        <div class="card bg-dark text-white col-lg-6 border-primary">
            <div class="card-header border-primary">
                Rounds Made Last 5 Days
            </div>
            <div class="card-body">
                <span style="font-size: 26px;">@if(count($rounds) > 0) {{count($rounds)}} @else NONE @endif</span>
            </div>
        </div>
    </div>

    <div class="card-deck mb-3">
        <div class="card bg-dark text-white col-lg-6 border-info">
            <div class="card-body">
                <canvas id="dispatchChart"></canvas>
            </div>
        </div>
        <div class="card bg-dark text-white col-lg-6 border-info">
            <div class="card-body">
                <canvas id="roundsChart"></canvas>
            </div>
        </div>
    </div>
    
    {{-- Bottom charts --}}

    <div class="card-deck mb-3">
        <div class="card bg-dark text-white col-lg-4 border-primary">
            <div class="card-header border-primary">
                Badges Issued Last Night
            </div>
            <div class="card-body">
                <span style="font-size: 26px;">@if(empty($badges->total)) NONE @else{{$badges->total}} @endif</span>
            </div>
        </div>
        <div class="card bg-dark text-white col-lg-4 border-primary">
            <div class="card-header border-primary">
                Citations Issued Last 30 days
            </div>
            <div class="card-body">
                <span style="font-size: 26px;">@if(count($citations) > 0) {{count($citations)}} @else NONE @endif</span>
            </div>
        </div>
        <div class="card bg-dark text-white col-lg-4 border-primary">
            <div class="card-header border-primary">
                Incidents Last 30 Days
            </div>
            <div class="card-body">
                <span style="font-size: 26px;">@if(count($incidents) > 0) {{count($incidents)}} @else NONE @endif</span>
            </div>
        </div>
    </div>

    <div class="card-deck mb-3">
        <div class="card bg-dark text-white col-lg-4 border-info">
            <div class="card-body">
                <canvas id="badgeChart"></canvas>
            </div>
        </div>
        <div class="card bg-dark text-white col-lg-4 border-info">
            <div class="card-body">
                <canvas id="citationChart"></canvas>
            </div>
        </div>
        <div class="card bg-dark text-white col-lg-4 border-info">
            <div class="card-body">
                <canvas id="incidentChart"></canvas>
            </div>
        </div>
    </div>

    </div>
</div>

@include('sgmcsecurity.modals.securityReport')
@include('sgmcsecurity.modals.securitySpecific')
@include('sgmcsecurity.modals.securityMatrix')

@endsection

@section('scripts')

<script type="application/javascript">

    @include('sgmcsecurity.charts.charts')

</script>

@endsection

@endif