@if ( Auth::user()->hasRole('SGMC Safety & Security') == FALSE)
    @include('layouts.unauthorized')
@Else

@extends('layouts.new_nonav')

@section('content')

<div class="card mb-3">
    <div class="card-header">
        <h4>Report for: <span class="text-primary"> {{ $incident->file_num }} </h4>
    </div>
    <div class="card-body">

        <table class="table table-hover" style="font-size: 20px;">
            <thead>
                <th style="width: 30%;">Category</th>
                <th>Information</th>
            </thead>
            <tbody>
                <tr>
                    <td>File Number</td>
                    <td> {{ $incident->file_num }} </td>
                </tr>
                <tr>
                    <td>Incident</td>
                    <td> {{ $incident->incident }} </td>
                </tr>
                <tr>
                    <td>Date & Time</td>
                    <td> {{ $incident->datetime }} </td>
                </tr>
                <tr>
                    <td>Location</td>
                    <td> {{ $incident->location }} </td>
                </tr>
                <tr>
                    <td>Reported By</td>
                    <td>{{ $incident->reported_by }}</td>
                </tr>
                <tr>
                    <td>Officer</td>
                    <td>{{ $incident->user_name }}</td>
                </tr>

            </tbody>
        </table>

        <hr>

        <h5>Description</h5>
        <p> {{ $incident->comments }} </p>

        <br><br><br>

        <hr>
        <p class="text-center"> {{ $incident->user_name }} </p>

    </div>

    <div class="card-footer">
        <a href="/security" class="btn btn-primary">Return to Dashboard</a>
    </div>

</div>

@endsection

@endif