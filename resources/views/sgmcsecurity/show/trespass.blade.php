@if ( Auth::user()->hasRole('SGMC Safety & Security') == FALSE)
    @include('layouts.unauthorized')
@Else

@extends('layouts.new_nonav')

@section('content')

<div class="card bg-dark text-white mb-3">
    <div class="card-body">
        <table class="table table-dark table-hover table-bordered" id="trespassT">
            <thead>
                <th>Date & Time</th>
                <th>Trespasser</th>
                <th>Date Issued</th>
                <th>Location</th>
            </thead>
            <tbody>
                <td>{{ Carbon::parse($trespass->date_time)->format('m-d-Y H:i') }}</td>
                <td>{{ $trespass->Fname . ' ' . $trespass->Lname }}</td>
                <td>{{ Carbon::parse($trespass->date_issued)->format('m-d-Y') }}</td>
                <td>{{ $trespass->location }}</td>
            </tbody>
        </table>

        <table class="table table-dark table-hover table-bordered" id="trespassT">
                <thead>
                    <th>Comments</th>
                </thead>
                <tbody>
                    <td>{{ $trespass->comments }}</td>
                </tbody>
            </table>

        <a href="/security" class="btn btn-primary">Return to Dashboard</a>

    </div>
</div>

@endsection

@section('scripts')


@endsection

@endif