@if ( Auth::user()->hasRole('SGMC Safety & Security') == FALSE)
    @include('layouts.unauthorized')
@Else

@extends('layouts.app')

    {{-- 2 --}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

@section('content')

@include('sgmcsecurity.nav.nav')

    <div class="card bg-dark text-white mb-3 mx-auto" style="width:60%;">
        <div class="card-header">
            Incident Report
        </div>
        <div class="card-body">

            <form action="/security/incident/store" method="POST" class="mb-0">
                {{ csrf_field() }}
                <input type="hidden" name="user_name" value="{{Auth::user()->name}}">
                <input type="hidden" name="file_num">
                
                <div class="form-row">
                    <div class="form-group col-lg-6">
                        <label for="reported_by">Complainant / Reported By</label>
                        <input type="text" class="form-control" name="reported_by">
                    </div>
                    <div class="form-group col-lg-6">
                        <label for="datetime">Date & Time</label>
                        <input type="datetime-local" name="datetime" class="form-control">
                    </div>

                </div>

                <div class="form-row">

                    <div class="form-group col-lg-6">
                        <label for="incident">Incident</label>
                        <select name="incident" class="form-control basic" required>
                            <option value="">[Choose Incident]</option>
                            @foreach($incidents as $incident)
                                <option value="{{$incident->incident}}">{{$incident->incident}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-lg-6">
                        <label for="location">Location</label>
                        <select name="location" class="form-control basic" required>
                            <option value="" selected>[Choose Location]</option>
                            @foreach($locations as $loc)
                                <option value="{{ $loc->location }}">{{ $loc->location }}</option>
                            @endforeach
                        </select>
                    </div>

                </div>

                <div class="form-row">
                    <div class="form-group col-lg-4">
                        <label for="wpv">Workplace Violence?</label>
                        <select name="wpv" class="form-control" required>
                            <option value="" selected>[Y/N]</option>
                            <option value="Yes">Yes</option>
                            <option value="No">No</option>
                            <option value="N/A">N/A</option>
                        </select>
                    </div>
                    <div class="form-group col-lg-4">
                        <label for="verbal">Verbal or Nonverbal</label>
                        <select name="verbal" class="form-control" required>
                            <option value="" selected>[Choose Answer]</option>
                            <option value="Verbal">Verbal</option>
                            <option value="Nonverbal">Nonverbal</option>
                            <option value="N/A">N/A</option>
                        </select>
                    </div>
                    <div class="form-group col-lg-4">
                        <label for="">Person Involved</label>
                        <select name="involved" class="form-control" required>
                            <option value="" selected>[Choose Answer]</option>
                            <option value="Employee">Employee</option>
                            <option value="Visitor">Visitor</option>
                            <option value="Patient">Patient</option>
                            <option value="Other">Other</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="comments">Comments</label>
                    <textarea name="comments" rows="5" class="form-control"></textarea>
                </div>

                <hr>

                <input type="submit" class="btn btn-primary btn-block">

            </form>

        </div>
    </div>

    <div class="card-deck">
        <div class="card bg-dark text-white mb-3">
            <div class="card-header">
                Recent Incidents
            </div>
            <div class="card-body">
                <table class="table table-hover table-dark table-striped" id="userIncidents">
                    <thead>
                        <th>Incident</th>
                        <th>Date & Time</th>
                        <th>Location</th>
                        <th>Edit</th>
                    </thead>
                    <tbody>
                        @forelse($userIncidents as $inc)
                            <tr>
                                <td>{{ $inc->incident }}</td>
                                <td>{{ Carbon::parse($inc->datetime)->format('n-j-Y  H:i') }}</td>
                                <td>{{ $inc->location }}</td>
                                <td><a href="/security/incident/edit/{{$inc->id}}" class="btn btn-sm btn-block btn-warning">Edit</a></td>
                            </tr>
                        @empty
                            <tr>
                                <td class="text-center" colspan="4">No Data in Table</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                <i class="fas fa-exclamation-circle text-danger"></i> This only shows the incidents submitted by YOU available for editing.
            </div>
        </div>

        <div class="card bg-dark text-white mb-3">
            <div class="card-header">
                All Incidents Recorded
            </div>
            <div class="card-body">

                <table class="table table-hover table-dark table-striped" id="allIncidents">
                    <thead>
                        <th>Incident</th>
                        <th>Date & Time</th>
                        <th>Location</th>
                        <th>View</th>
                    </thead>
                    <tbody>
                        @forelse($allIncidents as $inc)
                            <tr>
                                <td>{{ $inc->incident }}</td>
                                <td>{{ Carbon::parse($inc->datetime)->format('n-j-Y  H:i') }}</td>
                                <td>{{ $inc->location }}</td>
                                <td><a href="/security/incident/view/{{$inc->id}}" target="_blank" class="btn btn-sm btn-block btn-info">View</a></td>
                            </tr>
                        @empty
                            <tr>
                                <td class="text-center" colspan="4">No Data in Table</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>

            </div>
            <div class="card-footer">
                <i class="fas fa-exclamation-circle text-warning"></i> This shows all the incidents recorded for viewing.
            </div>
        </div>
    </div>


@endsection

@section('scripts')

    {{-- 2 --}}
{{-- <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script type="application/javascript">
    $(document).ready(function() {
        $('#allIncidents').DataTable( {
            "pageLength": 10,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );

    $(document).ready(function() {
        $('#userIncidents').DataTable( {
            "pageLength": 10,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>

<script type="text/javascript">

    $(".basic").select2({
        allowClear: true
    });

</script>


@endsection


@endif