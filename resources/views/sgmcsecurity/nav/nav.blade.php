
<style>

    .nav-link{
        color: white;
    }

    .nav-link:hover{
        color: black;
    }

</style>

<div class="card bg-secondary mb-3 mx-auto" style="border-radius:0px; width:75%">
    <div class="card-header mx-auto">
            <ul class="nav">
                <li class="nav-item">
                    <a class="nav-link" href="/security">Dashboard</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/security/rounds">Rounds</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/security/dispatch">Dispatch</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/security/citation">Citation</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/security/badge">Badge</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/security/incident">Incident Report</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/security/trespass">Trespass</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/security/orientation">Officer Orientated</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/security/ongoing">Ongoing Dispatches &nbsp;<span class="badge badge-danger">{{count($on)}}</a>
                </li>

                @if(Auth::user()->hasRole('SGMC Safety & Security Admin'))
                    <li class="nav-item">
                        <a class="nav-link" href="/security/location_activity">Add Location/Activity/Incident</a>
                    </li>
                @endif
            </ul>
    </div>
</div>