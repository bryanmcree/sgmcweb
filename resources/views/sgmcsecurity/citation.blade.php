@if ( Auth::user()->hasRole('SGMC Safety & Security') == FALSE)
    @include('layouts.unauthorized')
@Else

@extends('layouts.app')

    {{-- 2 --}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

@section('content')

    @include('sgmcsecurity.nav.nav')

<div class="card bg-dark text-white mb-4 mx-auto" style="width:80%;">
    <div class="card-header">
        <h3>Citation</h3>
    </div>
    <div class="card-body">
    
        <form action="/security/citation/issue" method="POST">
            <input type="hidden" name="user_id" value="{{ $user->employee_number }}">
            {{ csrf_field() }}

            <table class="table table-dark table-dark table-bordered">
                <thead class="text-warning">
                    <th>Name</th>
                    <th>License Plate #</th>
                    <th>Citation #</th>
                    <th>Date</th>
                </thead>
                <tbody>
                    <tr>
                        <td><input type="text" name="name" class="form-control" readonly required value="{{ $user->name }}"></td>
                        <td><input type="text" name="license_plate" class="form-control" required></td>
                        <td><input type="text" name="citation_num" class="form-control" required></td>
                        <td><input type="date" name="date" class="form-control" required></td>
                    </tr>
                    <tr class="text-warning">
                        <th colspan="4">Violation(s)</th>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <select name="violation[]" class="form-control multi-2" multiple="multiple" required>
                                <option value="no parking decal">NO PARKING DECAL</option>
                                <option value="exceeded authorized time limit">EXCEEDED AUTHORIZED TIME LIMIT</option>
                                <option value="parking in unauthorized area">PARKING IN UNAUTHORIZED AREA</option>
                                <option value="excessive speed for conditions">EXCESSIVE SPEED FOR CONDITIONS</option>
                                <option value="incorrect parking">INCORRECT PARKING</option>
                                <option value="handicapped parking">HANDICAPPED PARKING</option>
                                <option value="vehicle subject to be towed">VEHICLE SUBJECT TO BE TOWED</option>
                            </select>
                        </td>
                    </tr>
                    <tr class="text-warning">
                        <th colspan="2">Violator</th>
                        <th>Violator First Name</th>
                        <th>Violator Last Name</th>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <select name="location" class="form-control" required>
                                <option value="" selected>[Choose Violator]</option>
                                <option value="sgmc">SGMC Employee</option>
                                <option value="vsu">VSU Student</option>
                                <option value="visitor">Visitor</option>
                                <option value="vendor">Vendor</option>
                                <option value="N/A">N/A</option>
                            </select>
                        </td>
                        <td>
                            <input type="text" class="form-control" name="violator_Fname">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="violator_Lname">
                        </td>
                    </tr>
                    <tr class="text-warning">
                        <th colspan="4">Comments</th>
                    </tr>
                    <tr>
                        <td colspan="4"><textarea name="comments" rows="5" class="form-control"></textarea></td>
                    </tr>
                </tbody>
            </table>

            {{-- <div class="form-row">
                <div class="form-group col-lg-3">
                    <label for="name">Name</label>
                    
                </div>
                <div class="form-group col-lg-3">
                    <label for="license_plate">License Plate #</label>
                    
                </div>
                <div class="form-group col-lg-3">
                    <label for="citation_num">Citation #</label>
                    
                </div>
                <div class="form-group col-lg-3">
                    <label for="date">Date</label>
                    
                </div>
            </div> --}}

            {{-- <div class="form-row">
                <div class="form-group col-lg-6">
                    <label for="Violation">Violation(s)</label>
                    
                </div>
                <div class="form-group col-lg-6">
                    <label for="Location">Violator</label>
                    
                </div>
            </div> --}}
{{-- 
            <div class="form-group">
                <label for="comments">Comments</label>
                
            </div> --}}

            <hr>
            <input type="submit" class="btn btn-primary btn-block" value="Submit">

        </form>
    </div>
</div>

    <div class="alert bg-info">Use the search bar below to check if a license plate has previously been issued a citation. If they appear in the search then they are a repeat offender.</div>

<div class="card bg-dark text-white mb-3">
    <div class="card-header">
        Citation History
    </div>
    <div class="card-body">

        <div class="table-responsive">
            <table class="table table-dark table-hover table-bordered" id="offenders">
                <thead>
                    <th class="text-center">Citation #</th>
                    <th>Date</th>
                    <th>License Plate</th>
                </thead>
                <tbody>
                    @forelse($offenders as $data)
                        <tr>
                            <td class="text-center">{{$data->citation_num}}</td>
                            <td>{{Carbon::parse($data->date)->format('n-j-Y')}}</td>
                            <td>{{$data->license_plate}}</td>
                        </tr>
                    @empty
                        <tr>
                            <td class="text-center" colspan="4"> No Data Available</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>

    </div>
</div>


@endsection




@section('scripts')


<script type="application/javascript">
    $(document).ready(function() {
        $('#offenders').DataTable( {
            "pageLength": 10,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>


    2
{{-- <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script>
    $(document).ready(function() {
        $(".multi-2").select2();
    });
</script>



@endsection

@endif