@if ( Auth::user()->hasRole('SGMC Safety & Security') == FALSE)
    @include('layouts.unauthorized')
@Else

@extends('layouts.app')

@section('content')

{{-- <div class="alert bg-warning"><h4>Please be sure to check that you are using the correct form. The top form is for activities. The bottom is for locations.</h4></div> --}}

    @include('sgmcsecurity.nav.nav')  

<div class="container">
    <div class="card bg-dark text-white mb-4">
        <div class="card-header">
            <h3>ADD NEW ACTIVITY</h3>
        </div>
        <div class="card-body">

            <form action="/security/activity/create" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="activity">Activity</label>
                    <input type="text" class="form-control" name="activity" required>
                </div>

                <input type="submit" class="btn btn-primary" value="Add Activity">
            </form>
        </div>
    </div>

    <hr class="bg-warning">

    <div class="card bg-dark text-white mb-4">
        <div class="card-header">
            <h3>ADD NEW LOCATION</h3>
        </div>
        <div class="card-body">
            <form action="/security/location/create" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="location">Location</label>
                    <input type="text" class="form-control" name="location" required>
                </div>

                <input type="submit" class="btn btn-primary" value="Add Location">
            </form>
        </div>
    </div>

    <hr class="bg-warning">

    <div class="card bg-dark text-white mb-4">
        <div class="card-header">
            <h3>ADD NEW INCIDENT</h3>
        </div>
        <div class="card-body">
            <form action="/security/incident/create" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="incident">Incident</label>
                    <input type="text" class="form-control" name="incident" required>
                </div>

                <input type="submit" class="btn btn-primary" value="Add Incident">
            </form>
        </div>
    </div>

    <hr class="bg-danger">

    <div class="card bg-dark text-white mb-3">
        <div class="card-header">
            Remove Activity
        </div>
        <div class="card-body">
            <table class="table table-dark table-hover" id="activity">
                <thead>
                    <th>Activity</th>
                    <th class="text-center">Action</th>
                </thead>
                <tbody>
                    @foreach($activities as $activity)
                        <tr>
                            <td>{{$activity->activity}}</td>
                            <td><a href="#" class="btn btn-sm btn-danger btn-block deleteActivity" activity_id = {{$activity->id}}>Delete</a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="card bg-dark text-white mb-3">
        <div class="card-header">
            Remove Location
        </div>
        <div class="card-body">
            <table class="table table-dark table-hover" id="location">
                <thead>
                    <th>Location</th>
                    <th class="text-center">Action</th>
                </thead>
                <tbody>
                    @foreach($locations as $location)
                        <tr>
                            <td>{{$location->location}}</td>
                            <td><a href="#" class="btn btn-sm btn-danger btn-block deleteLocation" location_id = {{$location->id}}>Delete</a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="card bg-dark text-white mb-3">
        <div class="card-header">
            Remove Incident
        </div>
        <div class="card-body">
            <table class="table table-dark table-hover" id="incident">
                <thead>
                    <th>Incident</th>
                    <th class="text-center">Action</th>
                </thead>
                <tbody>
                    @foreach($incidents as $incident)
                        <tr>
                            <td>{{$incident->incident}}</td>
                            <td><a href="#" class="btn btn-sm btn-danger btn-block deleteIncident" incident_id = {{$incident->id}}>Delete</a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection


@section('scripts')

<script type="application/javascript">
    $(document).ready(function() {
        $('#activity').DataTable( {
            "pageLength": 10,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>

<script type="application/javascript">
    $(document).ready(function() {
        $('#location').DataTable( {
            "pageLength": 10,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>

<script type="application/javascript">
    $(document).ready(function() {
        $('#incident').DataTable( {
            "pageLength": 10,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>

<script type="application/javascript">

$(document).on("click", ".deleteActivity", function () {
            //alert($(this).attr("data-cost_center"));
            var activity_id = $(this).attr("activity_id");
            DeleteActivity(activity_id);
        });

        function DeleteActivity(activity_id) {
            swal({
                title: "Delete activity?",
                text: "Deleting this activity cannot be undone.",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                confirmButtonText: "Yes, delete it!",
                confirmButtonColor: "#ec6c62"
            }, function() {
                $.ajax({
                    url: "/security/activity/destroy/" + activity_id,
                    type: "GET"
                })
                    .done(function(data) {
                        swal({
                                title: "Deleted",
                                text: "Activity Deleted",
                                type: "success",
                                timer: 1800,
                                showConfirmButton: false

                            }
                        );
                        setTimeout(function(){window.location.replace('/security/location_activity')},1900);
                    })
                    .error(function(data) {
                        swal("Oops", "We couldn't connect to the server!", "error");
                    });
            });
        }

</script>

<script type="application/javascript">

$(document).on("click", ".deleteLocation", function () {
            var location_id = $(this).attr("location_id");
            DeleteLocation(location_id);
        });

        function DeleteLocation(location_id) {
            swal({
                title: "Delete Location?",
                text: "Deleting this location cannot be undone.",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                confirmButtonText: "Yes, delete it!",
                confirmButtonColor: "#ec6c62"
            }, function() {
                $.ajax({
                    url: "/security/location/destroy/" + location_id,
                    type: "GET"
                })
                    .done(function(data) {
                        swal({
                                title: "Deleted",
                                text: "Location Deleted",
                                type: "success",
                                timer: 1800,
                                showConfirmButton: false

                            }
                        );
                        setTimeout(function(){window.location.replace('/security/location_activity')},1900);
                    })
                    .error(function(data) {
                        swal("Oops", "We couldn't connect to the server!", "error");
                    });
            });
        }

</script>

<script type="application/javascript">

$(document).on("click", ".deleteIncident", function () {
            //alert($(this).attr("data-cost_center"));
            var incident_id = $(this).attr("incident_id");
            DeleteIncident(incident_id);
        });

        function DeleteIncident(incident_id) {
            swal({
                title: "Delete Incident?",
                text: "Deleting this Incident cannot be undone.",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                confirmButtonText: "Yes, delete it!",
                confirmButtonColor: "#ec6c62"
            }, function() {
                $.ajax({
                    url: "/security/incident/destroy/" + incident_id,
                    type: "GET"
                })
                    .done(function(data) {
                        swal({
                                title: "Deleted",
                                text: "Incident Deleted",
                                type: "success",
                                timer: 1800,
                                showConfirmButton: false

                            }
                        );
                        setTimeout(function(){window.location.replace('/security/location_activity')},1900);
                    })
                    .error(function(data) {
                        swal("Oops", "We couldn't connect to the server!", "error");
                    });
            });
        }

</script>

@endsection

@endif