@if ( Auth::user()->hasRole('SGMC Safety & Security') == FALSE)
    @include('layouts.unauthorized')
@Else

@extends('layouts.app')


@section('content')

@include('sgmcsecurity.nav.nav')

<div class="card bg-dark text-white mb-3">
    <div class="card-header">
        Trespassing Incidents
    </div>
    <div class="card-body">

        <div class="card bg-secondary mx-auto mb-4" style="width:60%">

            <form method="POST" action="/security/trespass/store">
                {{ csrf_field() }}
                <input type="hidden" name="created_by" value="{{ Auth::user()->employee_number }}">

                <div class="card-body">

                    <div class="form-row">
                        <div class="form-group col-6">
                            <label for="">Trespasser's First Name</label>
                            <input type="text" class="form-control" name="Fname">
                        </div>
                        <div class="form-group col-6">
                            <label for="">Trespasser's Last Name</label>
                            <input type="text" class="form-control" name="Lname">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-4">
                            <label>Date & Time of the Trespassing</label>
                            <input type="datetime-local" class="form-control" name="date_time">
                        </div>
                        <div class="form-group col-4">
                            <label>Location of the Trespassing</label>
                            <select name="location" class="form-control">
                                <option value="">[Choose Location]</option>
                                @foreach ($locations as $location)
                                    <option value="{{ $location->location }}">{{ $location->location }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-4">
                            <label>Date Issued</label>
                            <input type="date" class="form-control" name="date_issued">
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Comments</label>
                        <textarea name="comments" class="form-control" rows="5"></textarea>
                    </div>

                </div>
                <div class="card-footer">
                    <input type="submit" value="Submit" class="btn btn-primary btn-block">
                </div>
            </form>
        </div>

        <div class="card bg-secondary mb-3">
            <div class="card-body">
                
                <table class="table table-dark table-hover table-bordered mb-0" id="trespassT">
                    <thead>
                        <th>Date & Time</th>
                        <th>Trespasser</th>
                        <th>Date Issued</th>
                        <th>Location</th>
                        <th>View</th>
                    </thead>
                    <tbody>
                        @foreach ($trespassing as $trespass)
                            <tr>
                                <td>{{ Carbon::parse($trespass->date_time)->format('m-d-Y H:i') }}</td>
                                <td>{{ $trespass->Fname . ' ' . $trespass->Lname }}</td>
                                <td>{{ Carbon::parse($trespass->date_issued)->format('m-d-Y') }}</td>
                                <td>{{ $trespass->location }}</td>
                                <td> <a href="/security/trespass/show/{{ $trespass->id }}" class="btn btn-sm btn-info btn-block">Details</a> </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>

    </div>
</div>

@endsection


@section('scripts')

<script type="application/javascript">
    $(document).ready(function() {
        $('#trespassT').DataTable( {
            "pageLength": 10,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>

@endsection

@endif