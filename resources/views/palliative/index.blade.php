{{--New file Template--}}

{{--Add Security for this page below--}}
@if( Auth::user()->hasRole('Palliative Care') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    <style>
        .buttonCLass{ visibility: hidden;}
        tr:hover .buttonCLass{ visibility: visible; transition: 5.0s;}
    </style>


    <div class="card bg-dark text-white mt-2">
        <div class="card-header">
            <h4><b>Patients</b> <a href="#" class="btn btn-success btn-sm float-right" data-toggle="modal" data-target="#exampleModalCenter"><i class="fad fa-hospital-user"></i> New Patient</a></h4>
        </div>
        <div class="card-body">
            @if($patients->isEmpty())
                <div class="alert-warning alert ">There are no records</div>
            @else
                <table class="table table-hover" id="patients">
                    <thead>
                        <tr>
                            <td><b>Encounter</b></td>
                            <td><b>Unit</b></td>
                            <td><b>Age</b></td>
                            <td><b>Gender</b></td>
                            <td><b>Marital</b></td>
                            <td><b>DOA</b></td>
                            <td><b>DOC</b></td>
                            <td><b>DOD</b></td>
                            <td><b># Visits</b></td>
                            <td><b>Total Time</b></td>
                            <td><b>MD</b></td>
                            <td><b>Entered By</b></td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($patients as $patient)
                        <tr>
                            <td>{{$patient->encounter_number}}</td>
                            <td>{{$patient->unit_number}}</td>
                            <td>{{$patient->patient_age}}</td>
                            <td>{{$patient->patient_gender}}</td>
                            <td>{{$patient->patient_marital_status}}</td>
                            <td>{{$patient->date_of_admission->format('m-d-Y')}}</td>
                            <td>{{$patient->date_of_consult->format('m-d-Y')}}</td>
                            <td>{{$patient->date_of_discharge->format('m-d-Y')}}</td>
                            <td>{{$patient->visits->count()}}</td>
                            <td>{{$patient->visits->sum('total_min')}}</td>
                            <td>{{$patient->reffering_md}}</td>
                            <td>{{$patient->createdBy->name}}</td>
                            <td align="right"><a href="/palliative/patient/{{$patient->id}}" class="btn btn-secondary btn-sm buttonCLass" title="View Patient Detail"><i class="fad fa-procedures"></i> Detail</a> </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>



    @include('palliative.modals.add_patient')

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">
    $(function(){
        $('tr').hover(function(){ /* hover first argument is mouseenter*/
            $(this).find('button').css({"visibility": "visible"});
        },function(){  /* hover second argument is mouseleave*/
            $(this).find('button').css({"visibility": "hidden"});
        });

    });


    $(document).ready(function() {
        $('#patients').DataTable();
    } );


</script>

@endsection

@endif