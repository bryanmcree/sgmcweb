<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content bg-dark text-white">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Add Patient</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="/palliative/patient/add">
                        {{ csrf_field() }}
                        <input type="hidden" name="created_by" value="{{ Auth::user()->employee_number }}">
                        <div class="card mb-4 text-white bg-dark">
                            <div class="card-header">
                                <b>Patient Demographics</b>
                            </div>
                            <div class="card-body">
                                    <div class="form-row">
                                        <div class="col-4">
                                            <div class="card bg-dark text-white">
                                                <div class="card-header">
                                                    <b>Encounter #</b>
                                                </div>
                                                <div class="card-body">
                                                    <input type="text" name="encounter_number" class="form-control" id="inputEncounterNumber" aria-describedby="emailHelp" placeholder="Encounter" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="card bg-dark text-white">
                                                <div class="card-header">
                                                    <b>Unit</b>
                                                </div>
                                                <div class="card-body">
                                                    <select name="unit_number" class="form-control" id="exampleFormControlSelect1" required>
                                                        <option value="">[Select]</option>
                                                        <option value="2 Tower">2 Tower</option>
                                                        <option value="3 West">3 West</option>
                                                        <option value="3 Tower">3 Tower</option>
                                                        <option value="4 West">4 West</option>
                                                        <option value="4 South">4 South</option>
                                                        <option value="4 Tower">4 Tower</option>
                                                        <option value="5 West">5 West</option>
                                                        <option value="5 Tower">5 Tower</option>
                                                        <option value="5 South">5 South</option>
                                                        <option value="ED">ED</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="card bg-dark text-white">
                                                <div class="card-header">
                                                    <b>Race</b>
                                                </div>
                                                <div class="card-body">
                                                    <select name="patient_race" class="form-control" id="exampleFormControlSelect1" required>
                                                        <option>[Select]</option>
                                                        <option value="White">White</option>
                                                        <option value="African American">African American</option>
                                                        <option value="Hispanic">Hispanic</option>
                                                        <option value="Asian">Asian</option>
                                                        <option value="Other">Other</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-4">
                                            <div class="card bg-dark text-white">
                                                <div class="card-header">
                                                    <b>Marital Status</b>
                                                </div>
                                                <div class="card-body">
                                                    <select name="patient_marital_status" class="form-control" id="exampleFormControlSelect1">
                                                        <option>[Select]</option>
                                                        <option value="Single">Single</option>
                                                        <option value="Married">Married</option>
                                                        <option value="Divorced">Divorced</option>
                                                        <option value="Separated">Separated</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="card bg-dark text-white">
                                                <div class="card-header">
                                                    <b>Gender</b>
                                                </div>
                                                <div class="card-body">
                                                    <select name="patient_gender" class="form-control" id="exampleFormControlSelect1">
                                                        <option>[Select]</option>
                                                        <option value="Male">Male</option>
                                                        <option value="Female">Female</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="card bg-dark text-white">
                                                <div class="card-header">
                                                    <b>Date of Birth</b>
                                                </div>
                                                <div class="card-body">
                                                    <input type="date" name="date_of_birth" class="form-control" name="patient_dob" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>

                        <div class="card mb-4 text-white bg-dark">
                            <div class="card-header">
                                <b>Admission Information</b>
                            </div>
                            <div class="card-body">
                                    <div class="form-row">
                                        <div class="col-4">
                                            <div class="card bg-dark text-white">
                                                <div class="card-header">
                                                    <b>Date of Admission</b>
                                                </div>
                                                <div class="card-body">
                                                    <input type="date" class="form-control" name="date_of_admission" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="card bg-dark text-white">
                                                <div class="card-header">
                                                    <b>Date of Consult</b>
                                                </div>
                                                <div class="card-body">
                                                    <input type="date" class="form-control" name="date_of_consult">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="card bg-dark text-white">
                                                <div class="card-header">
                                                    <b>Date of Discharge</b>
                                                </div>
                                                <div class="card-body">
                                                    <input type="date" class="form-control" name="date_of_discharge">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row mt-2">
                                        <div class="col-6">
                                            <div class="card bg-dark text-white">
                                                <div class="card-header">
                                                    <b>Dx 1</b>
                                                </div>
                                                <div class="card-body">
                                                    <input type="text" name="dx1" class="form-control" id="inputEncounterNumber" aria-describedby="emailHelp" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="card bg-dark text-white">
                                                <div class="card-header">
                                                    <b>Dx 2</b>
                                                </div>
                                                <div class="card-body">
                                                    <input type="text" name="dx2" class="form-control" id="inputEncounterNumber" aria-describedby="emailHelp" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row mt-2">
                                        <div class="col-6">
                                            <div class="card bg-dark text-white">
                                                <div class="card-header">
                                                    <b>Insurance</b>
                                                </div>
                                                <div class="card-body">
                                                    <select name="insurance" class="form-control" id="exampleFormControlSelect1">
                                                        <option value="">--</option>
                                                        <option value="Medicare">Medicare</option>
                                                        <option value="Medicaid">Medicaid</option>
                                                        <option value="PI">PI</option>
                                                        <option value="VA/Tricare">VA/Tricare</option>
                                                        <option value="Self Pay">Self Pay</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="card bg-dark text-white">
                                                <div class="card-header">
                                                    <b>Referring MD</b>
                                                </div>
                                                <div class="card-body">
                                                    <input type="text" name="reffering_md" class="form-control" id="inputEncounterNumber" aria-describedby="emailHelp" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row mt-2">
                                        <div class="col-3">
                                            <div class="card bg-dark text-white">
                                                <div class="card-header">
                                                    <b>Vent</b>
                                                </div>
                                                <div class="card-body">
                                                    <select name="vent" class="form-control" id="exampleFormControlSelect1">
                                                        <option value="">--</option>
                                                        <option value="1">Yes</option>
                                                        <option value="0">No</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="card bg-dark text-white">
                                                <div class="card-header">
                                                    <b>DNR</b>
                                                </div>
                                                <div class="card-body">
                                                    <select name="dnr" class="form-control" id="exampleFormControlSelect1">
                                                        <option value="">--</option>
                                                        <option value="1">Yes</option>
                                                        <option value="0">No</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="card bg-dark text-white">
                                                <div class="card-header">
                                                    <b>Cancer</b>
                                                </div>
                                                <div class="card-body">
                                                    <select name="cancer" class="form-control" id="exampleFormControlSelect1">
                                                        <option value="">--</option>
                                                        <option value="1">Yes</option>
                                                        <option value="0">No</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="card bg-dark text-white">
                                                <div class="card-header">
                                                    <b>Disposition</b>
                                                </div>
                                                <div class="card-body">
                                                    <select name="patient_disposition" class="form-control" id="exampleFormControlSelect1">
                                                        <option>[Select]</option>
                                                        <option>Home</option>
                                                        <option>Home with home health</option>
                                                        <option>Home with hospice</option>
                                                        <option>Nursing Home</option>
                                                        <option>Nursing Home with Hospice</option>
                                                        <option>Swing Bed</option>
                                                        <option>Tertiary Care Center</option>
                                                        <option>LTAC</option>
                                                        <option>Hospice Facility</option>
                                                        <option>Expired</option>
                                                        <option>PRN</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                        <input type="submit" class="btn btn-primary" value="Add Patient">
                    </form>
                </div>

        </div>
    </div>
</div>