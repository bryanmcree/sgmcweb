<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content bg-dark text-white">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"><b>Enter Patient Visit</b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="/palliative/patient/visit/add">
                    {{ csrf_field() }}
                    <input type="hidden" name="patient_id" value="{{$patient->id}}">
                    <input type="hidden" name="created_by" value="{{ Auth::user()->employee_number }}">
                    <div class="row">
                        <div class="col-3">
                            <div class="card bg-dark text-white">
                                <div class="card-header">
                                    <b>Minutes</b>
                                </div>
                                <div class="card-body">
                                    <input type="text" name="total_min" class="form-control" id="inputEncounterNumber" aria-describedby="emailHelp" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="col-9">
                            <div class="card bg-dark text-white">
                                <div class="card-header">
                                    <b>Visit Notes</b>
                                </div>
                                <div class="card-body">
                                    <textarea class="form-control" rows="1" name="visit_notes"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="submit" class="btn btn-sm btn-primary mt-2 float-right" value="Enter Visit">
                </form>
            </div>
        </div>
    </div>
</div>