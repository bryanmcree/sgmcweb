{{--New file Template--}}

{{--Add Security for this page below--}}
@if( Auth::user()->hasRole('Palliative Care') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')


    <div class="card mb-4 text-white bg-dark">
        <div class="card-header">
            <b>Patient Demographics</b>
            <div class="btn-group btn-group-sm float-right" role="group" aria-label="Basic example">
                <a href="/palliative" class="btn bt-sm btn-secondary" >Return to Patients</a>
                <a href="#" class="btn btn-warning bt-sm disabled" data-toggle="modal" data-target="#exampleModalCenter">Edit Patient</a>
                <a href="#" class="btn bt-sm btn-danger DeletePatient" data-id="{{$patient->id}}">Delete Patient</a>
            </div>
        </div>
        <div class="card-body">
            <div class="card-deck">
                <div class="card bg-dark text-white">
                    <div class="card-body">
                        <div class="card-title"><b>Encounter</b></div>
                        {{$patient->encounter_number}}
                    </div>
                </div>
                <div class="card bg-dark text-white">
                    <div class="card-body">
                        <div class="card-title"><b>Unit</b></div>
                        {{$patient->unit_number}}
                    </div>
                </div>
                <div class="card bg-dark text-white">
                    <div class="card-body">
                        <div class="card-title"><b>Date of Birth</b></div>
                        {{$patient->date_of_birth->format('m-d-Y')}} ({{$patient->patient_age}})
                    </div>
                </div>
                <div class="card bg-dark text-white">
                    <div class="card-body">
                        <div class="card-title"><b>Gender</b></div>
                        {{$patient->patient_gender}}
                    </div>
                </div>
                <div class="card bg-dark text-white">
                    <div class="card-body">
                        <div class="card-title"><b>Marital Status</b></div>
                        {{$patient->patient_marital_status}}
                    </div>
                </div>
                <div class="card bg-dark text-white">
                    <div class="card-body">
                        <div class="card-title"><b>Race</b></div>
                        {{$patient->patient_race}}
                    </div>
                </div>
                <div class="card bg-dark text-white">
                    <div class="card-body">
                        <div class="card-title"><b>Reffering MD</b></div>
                        {{$patient->reffering_md}}
                    </div>
                </div>
            </div>
            <div class="card-deck mt-2">
                <div class="card bg-dark text-white">
                    <div class="card-body">
                        <div class="card-title"><b>Admit Date</b></div>
                        {{$patient->date_of_admission->format('m-d-Y')}}
                    </div>
                </div>
                <div class="card bg-dark text-white">
                    <div class="card-body">
                        <div class="card-title"><b>Consult Date</b></div>
                        {{$patient->date_of_consult->format('m-d-Y')}}
                    </div>
                </div>
                <div class="card bg-dark text-white">
                    <div class="card-body">
                        <div class="card-title"><b>Discharge Date</b></div>
                        {{$patient->date_of_discharge->format('m-d-Y')}}
                    </div>
                </div>
                <div class="card bg-dark text-white">
                    <div class="card-body">
                        <div class="card-title"><b>Cancer</b></div>
                        @if($patient->cancer ==1) Yes @else No @endif
                    </div>
                </div>
                <div class="card bg-dark text-white">
                    <div class="card-body">
                        <div class="card-title"><b>DNR</b></div>
                        @if($patient->dnr ==1) Yes @else No @endif
                    </div>
                </div>
                <div class="card bg-dark text-white">
                    <div class="card-body">
                        <div class="card-title"><b>Vent</b></div>
                        @if($patient->vent ==1) Yes @else No @endif
                    </div>
                </div>
            </div>
            <div class="card-deck mt-2">
                <div class="card bg-dark text-white">
                    <div class="card-body">
                        <div class="card-title"><b>Dx1</b></div>
                        {{$patient->dx1}}
                    </div>
                </div>
                <div class="card bg-dark text-white">
                    <div class="card-body">
                        <div class="card-title"><b>Dx2</b></div>
                        {{$patient->dx2}}
                    </div>
                </div>
                <div class="card bg-dark text-white">
                    <div class="card-body">
                        <div class="card-title"><b>Insurance</b></div>
                        {{$patient->insurance}}
                    </div>
                </div>
                <div class="card bg-dark text-white">
                    <div class="card-body">
                        <div class="card-title"><b>Disposition</b></div>
                        {{$patient->patient_disposition}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card bg-dark text-white">
        <div class="card-header">
            <b>Visits</b> (<i>Total Time: {{$patient->visits->sum('total_min')}}</i> Minutes) <a href="#" class="btn btn-success float-right btn-sm" data-toggle="modal" data-target="#exampleModalCenter">Add Visit</a>
        </div>
        <div class="card-body">
            @if($patient->visits->isEmpty())
                <div class="alert alert-warning">There are no visits for this patient.</div>
            @else
            <table class="table table-hover" id="visits">
                <thead>
                    <tr>
                        <td><b>Date</b></td>
                        <td><b>Minutes</b></td>
                        <td><b>Employee</b></td>
                        <td><b>Notes</b></td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                @foreach($patient->visits as $visit)
                    <tr>
                        <td>{{$visit->created_at}}</td>
                        <td>{{$visit->total_min}}</td>
                        <td>{{$visit->createdBy->name}}</td>
                        <td>{{$visit->visit_notes}}</td>
                        <td align="right">
                            <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                                <a href="#" class="btn btn-warning disabled" title="Edit Visit"><i class="fad fa-edit"></i></a>
                                <a href="#" class="btn btn-danger DeleteVisit" title="Delete Visit" data-id="{{$visit->id}}" data-patient_id="{{$visit->patient_id}}"><i class="fad fa-trash-alt"></i></a>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @endif
        </div>
    </div>

@include('palliative.modals.add_visit')

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">
    $(document).ready(function() {
        $('#visits').DataTable();
    } );



    $(document).on("click", ".DeleteVisit", function () {
        //alert($(this).attr("data-cost_center"));
        var id = $(this).attr("data-id");
        var patient_id = $(this).attr("data-patient_id");
        deleteVisit(id, patient_id);
    });

    function deleteVisit(id, patient_id) {
        swal({
            title: "Delete Visit?",
            text: "Are you sure that you want to delete this visit?",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            confirmButtonText: "Yes, delete it!",
            confirmButtonColor: "#ec6c62"
        }, function() {
            $.ajax({
                url: "/palliative/patient/delete/" + id + "?patient_id=" + patient_id,
                type: "GET"
            })
                .done(function(data) {
                    swal({
                            title: "Deleted",
                            text: "Visit Deleted",
                            type: "success",
                            timer: 1800,
                            showConfirmButton: false

                        }
                    );
                    setTimeout(function(){window.location.replace('/palliative/patient/'+ patient_id)},1900);
                })
                .error(function(data) {
                    swal("Oops", "We couldn't connect to the server!", "error");
                });
        });
    }


    $(document).on("click", ".DeletePatient", function () {
        //alert($(this).attr("data-cost_center"));
        var id = $(this).attr("data-id");
        deletePatient(id);
    });

    function deletePatient(id) {
        swal({
            title: "Delete Patient?",
            text: "Are you sure that you want to delete this Patient?",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            confirmButtonText: "Yes, delete it!",
            confirmButtonColor: "#ec6c62"
        }, function() {
            $.ajax({
                url: "/palliative/patient/delete/" + id,
                type: "GET"
            })
                .done(function(data) {
                    swal({
                            title: "Deleted",
                            text: "Patient Deleted",
                            type: "success",
                            timer: 1800,
                            showConfirmButton: false

                        }
                    );
                    setTimeout(function(){window.location.replace('/palliative')},1900);
                })
                .error(function(data) {
                    swal("Oops", "We couldn't connect to the server!", "error");
                });
        });
    }
</script>

@endsection

@endif