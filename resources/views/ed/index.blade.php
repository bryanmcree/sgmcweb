{{--New file Template--}}

{{--Add Security for this page below--}}
<script>
    'use strict';

    window.chartColors = {
        red: 'rgb(255, 99, 132)',
        orange: 'rgb(255, 159, 64)',
        yellow: 'rgb(255, 205, 86)',
        green: 'rgb(75, 192, 192)',
        blue: 'rgb(54, 162, 235)',
        purple: 'rgb(153, 102, 255)',
        grey: 'rgb(201, 203, 207)'
    };

    (function(global) {
        var MONTHS = [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December'
        ];

        var COLORS = [
            '#4dc9f6',
            '#f67019',
            '#f53794',
            '#537bc4',
            '#acc236',
            '#166a8f',
            '#00a950',
            '#58595b',
            '#8549ba'
        ];

        var Samples = global.Samples || (global.Samples = {});
        var Color = global.Color;

        Samples.utils = {
            // Adapted from http://indiegamr.com/generate-repeatable-random-numbers-in-js/
            srand: function(seed) {
                this._seed = seed;
            },

            rand: function(min, max) {
                var seed = this._seed;
                min = min === undefined ? 0 : min;
                max = max === undefined ? 1 : max;
                this._seed = (seed * 9301 + 49297) % 233280;
                return min + (this._seed / 233280) * (max - min);
            },

            numbers: function(config) {
                var cfg = config || {};
                var min = cfg.min || 0;
                var max = cfg.max || 1;
                var from = cfg.from || [];
                var count = cfg.count || 8;
                var decimals = cfg.decimals || 8;
                var continuity = cfg.continuity || 1;
                var dfactor = Math.pow(10, decimals) || 0;
                var data = [];
                var i, value;

                for (i = 0; i < count; ++i) {
                    value = (from[i] || 0) + this.rand(min, max);
                    if (this.rand() <= continuity) {
                        data.push(Math.round(dfactor * value) / dfactor);
                    } else {
                        data.push(null);
                    }
                }

                return data;
            },

            labels: function(config) {
                var cfg = config || {};
                var min = cfg.min || 0;
                var max = cfg.max || 100;
                var count = cfg.count || 8;
                var step = (max - min) / count;
                var decimals = cfg.decimals || 8;
                var dfactor = Math.pow(10, decimals) || 0;
                var prefix = cfg.prefix || '';
                var values = [];
                var i;

                for (i = min; i < max; i += step) {
                    values.push(prefix + Math.round(dfactor * i) / dfactor);
                }

                return values;
            },

            months: function(config) {
                var cfg = config || {};
                var count = cfg.count || 12;
                var section = cfg.section;
                var values = [];
                var i, value;

                for (i = 0; i < count; ++i) {
                    value = MONTHS[Math.ceil(i) % 12];
                    values.push(value.substring(0, section));
                }

                return values;
            },

            color: function(index) {
                return COLORS[index % COLORS.length];
            },

            transparentize: function(color, opacity) {
                var alpha = opacity === undefined ? 0.5 : 1 - opacity;
                return Color(color).alpha(alpha).rgbString();
            }
        };

        // DEPRECATED
        window.randomScalingFactor = function() {
            return Math.round(Samples.utils.rand(-100, 100));
        };

        // INITIALIZATION

        Samples.utils.srand(Date.now());

        // Google Analytics
        /* eslint-disable */
        if (document.location.hostname.match(/^(www\.)?chartjs\.org$/)) {
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
            ga('create', 'UA-28909194-3', 'auto');
            ga('send', 'pageview');
        }
        /* eslint-enable */

    }(this));
</script>

@if ( Auth::user()->hasRole('ED Report') == FALSE)
    @include('layouts.unauthorized')
@Else
    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="card bg-dark text-white mb-3">
            <div class="card-header">
                <b>Emergency Department - Average Minuets from Admission to Disposition</b>
                <i>Chart displays last 30 days of the average time in minuets from admission to disposition.</i>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <p><b>Running Average:</b> {{$ed_running_adv}}</p>
                    </div>
                    <div class="col-md-4 text-center">
                        <p><b>Yearly Average:</b> {{$ed_yearly_adv}}</p>
                    </div>
                    <div class="col-md-4 text-right">
                        <p><b>Monthly Average:</b> @if(is_null($ed_monthly_adv)) *--- @else{{$ed_monthly_adv}}@endif</p>
                    </div>
                </div>

                <div class="row">
                    <canvas id="ed_trend" width="100%" height="30"></canvas>
                </div>
            </div>
            <div class="card-footer">
                <i>*--- Monthly averages will be calculated after the second day of the month.</i>
            </div>
        </div>
    </div>


@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">
    //Acute Adv LOS
    new Chart(document.getElementById("ed_trend").getContext('2d'), {
        type: 'line',
        responsive:true,
        plugins: [{
            afterDatasetsDraw: function(chart) {
                var ctx = chart.ctx;
                chart.data.datasets.forEach(function(dataset, index) {
                    var datasetMeta = chart.getDatasetMeta(index);
                    if (datasetMeta.hidden) return;
                    datasetMeta.data.forEach(function(point, index) {
                        var value = dataset.data[index],
                            x = point.getCenterPoint().x,
                            y = point.getCenterPoint().y,
                            radius = point._model.radius,
                            fontSize = 12,
                            fontFamily = 'Verdana',
                            fontColor = 'gray',
                            fontStyle = 'normal';
                        ctx.save();
                        ctx.textBaseline = 'middle';
                        ctx.textAlign = 'center';
                        ctx.font = fontStyle + ' ' + fontSize + 'px' + ' ' + fontFamily;
                        ctx.fillStyle = fontColor;
                        ctx.fillText(value, x, y - radius - fontSize);
                        ctx.restore();
                    });
                });
            }
        }],
        data: {
            labels: [

                @foreach($chart_lables as $lable)
                    '{{$lable->rpt_date->format('m-d-Y')}}',
                @endforeach

            ],
            datasets: [{
                data: [
                    @foreach($ed_adv as $ber_value)
                    {{$ber_value->AVG_Min}},
                    @endforeach
                ],
                label: "Average Minuets from Admission to Disposition",
                borderColor: "#3e95cd",
                fill: true
            }
            ]
        },
        options: {
            animation: {
                duration: 750
            },
            trendlineLinear: {
                style: "rgba(255,105,180, .8)",
                width: 2
            }
        }
    });


    var color = Chart.helpers.color;
    function generateData() {
        var data = [];
        for (var i = 0; i < 7; i++) {
            data.push({
                x: randomScalingFactor(),
                y: randomScalingFactor()
            });
        }
        return data;
    }




    //PAS


    new Chart(document.getElementById("bar-chart-grouped"), {
        type: 'bar',
        data: {
            labels: ["12 am", "1 am", "2 am", "3 am", "4 am", "5 am",'6 am', '7 am'],
            datasets: [
                {
                    label: "BER Revenue Location",
                    backgroundColor: "#3e95cd",
                    data: [2,8,3,9,3,5, 3, 7, 3]
                }, {
                    label: "LAN Revenue Location",
                    backgroundColor: "#8e5ea2",
                    data: [2,8,3,9,3,5, 3, 7, 3]
                }
            ]
        },
        options: {
            title: {
                display: true,
                text: 'Registrations for Yesterday'
            }
        }
    });
</script>

@endsection
@endif