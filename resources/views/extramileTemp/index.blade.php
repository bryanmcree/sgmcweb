@extends('layouts.app')

@section('content')
    <div class="col-md-12">
        <div class="panel panel-success">
            <div class="panel-heading"><i class="fa fa-question-circle"  data-toggle="popover" data-placement="bottom"
                                          title="Active Employees"
                                          data-content="Searchable list of active employees."></i> -><b>Authorized Users ({{$employee->count()}})</b></div>
            <div class="panel-body">
                @if ($employee->isEmpty())
                    <div class="alert alert-info" role="alert">No current employees match your search requirements.</div>
                @else
                    <div class="container-fluid">
                        <div class="row">

                                {!! Form::text('search', null,array('class'=>'form-control','id'=>'search','placeholder'=>'Search for a person...', 'autofocus'=>'autofocus')) !!}

                            </div>
                        </div>
                    </div>
                    <BR>
                    <table class="table table-hover table-responsive table-bordered" id="table">
                        <thead>
                        <tr class="success">
                            <td><b>Name</b></td>

                            <td class="hidden-xs hidden-sm hidden-md"><b>Title</b></td>

                            <td class="hidden-xs"><b>Unit</b></td>
                            <td class="hidden-xs"><b>Emp#</b></td>
                            <td class="hidden-xs"><b>Email</b></td>
                            <td class="hidden-xs"><b>Manager</b></td>
                            <td align="center" colspan="4"></td>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach ($employee as $employees)

                            <tr >
                                <td>{{$employees->last_name}}, {{$employees->first_name}}</td>

                                <td class="hidden-sm hidden-md hidden-xs">{{$employees->title}}</td>

                                <td class="hidden-xs hidden-sm">{{$employees->unit_description}}</td>
                                <td class="hidden-xs hidden-sm">{{$employees->emp_number}}</td>
                                <td class="hidden-xs hidden-sm">{{$employees->email}}</td>
                                <td class="hidden-xs hidden-sm">{{$employees->Reviewer}}</td>
                                <td align="center" style="vertical-align:middle"><a href="/extramileTemp/create/{{$employees->emp_number}}" role="button" class="btn btn-warning btn-xs"><span class="fa fa-btn fa-trophy" aria-hidden="true"></span> Select</a></td>
                            </tr>

                        @endforeach
                        </tbody>
                    </table>



                @endif

            </div>
        </div>
    </div>
@endsection
