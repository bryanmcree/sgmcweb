@extends('layouts.app')

@section('content')
    {!! Form::open(array('action' => ['ExtraMileController@sendRequest'], 'class' => 'form_control')) !!}

<div class="col-md-12">
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
</div>

    <div class="col-md-3 col-md-offset-4">
        <div class="panel panel-success">
            <div class="panel-heading"><b>Request Access to ExtraMile</b></div>
            <div class="panel-body">


                <div class="form-group">
                    {!! Form::label('first_name', 'First Name:') !!}
                    {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('last_name', 'Last Name:') !!}
                    {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('username', 'SGMC User Name:') !!}
                    {!! Form::text('username', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('title', 'Title:') !!}
                    {!! Form::text('title', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('email', 'Email:') !!}
                    {!! Form::text('email', null, ['class' => 'form-control']) !!}
                </div>
                Access to ExtraMile is for supervisors and managers.
                <br><br>
                {!! Form::submit('Submit Request', ['class'=>'btn btn-success btn-block']) !!}
                {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection