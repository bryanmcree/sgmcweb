<div class="col-md-4 col-md-offset-4">
    <div class="panel panel-default">
        <div class="panel panel-heading"><b>Presented To:</b></div>
            <div class="panel-body">
                <div class="form-group">
                    {!! Form::label('presented_to_first_name', 'First Name:') !!}
                    {!! Form::text('presented_to_first_name', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('presented_to_last_name', 'Last Name:') !!}
                    {!! Form::text('presented_to_last_name', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('presented_to_employee_number', 'Employee Number:') !!}
                    {!! Form::text('presented_to_employee_number', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('presented_to_department_number', 'Department Number:') !!}
                    {!! Form::text('presented_to_department_number', null, ['class' => 'form-control','maxlength' => '10']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('presented_to_email', 'Email:') !!}
                    {!! Form::text('presented_to_email', null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('presented_to_campus', 'Status:') !!}
                    {!! Form::select('presented_to_campus', [
                                        'SGMC Main Campus' => 'SGMC Main Campus',
                                        'SGMC Berrien Campus' => 'SGMC Berrien Campus',
                                        'SGMC Lanier Campus' => 'SGMC Lanier Campus',
                                        'SGMC Outpatient Plaza' => 'SGMC Outpatient Plaza',
                                         ], null, ['class' => 'form-control']) !!}
                </div>
            </div>
    </div>
</div>


<div class="col-md-4 col-md-offset-4">
    <div class="panel panel-info">
        <div class="panel panel-heading"><b>Presented By:</b></div>
        <div class="panel-body">
            <div class="form-group">
                {!! Form::label('authorized_by_first_name', 'First Name:') !!}
                {!! Form::text('authorized_by_first_name', Auth::user()->adldapUser->givenname['0'], ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('authorized_by_last_name', 'Last Name:') !!}
                {!! Form::text('authorized_by_last_name', Auth::user()->adldapUser->sn['0'], ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('authorized_by_employee_number', 'Employee Number:') !!}
                {!! Form::text('authorized_by_employee_number', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('authorized_by_email', 'Email:') !!}
                {!! Form::text('authorized_by_email', Auth::user()->adldapUser->mail['0'], ['class' => 'form-control']) !!}
            </div>
        </div>
    </div>
</div>


<div class="col-md-4 col-md-offset-4">
    <div class="panel panel-primary">
        <div class="panel panel-heading"><b>Reason:</b></div>
        <div class="panel-body">
            <div class="form-group">
                {!! Form::checkbox('reason', 'Customer satisfaction (named by patient, visitor, or physician)', null, ['id'=>'reason']) !!}
                {!! Form::label('reason', 'Customer satisfaction (named by patient, visitor, or physician)') !!}
            </div>
            <div class="form-group">
                {!! Form::checkbox('reason', 'Handling a difficult situation involving a patient or family', null, ['id'=>'reason']) !!}
                {!! Form::label('reason', 'Handling a difficult situation involving a patient or family') !!}
            </div>
            <div class="form-group">
                {!! Form::checkbox('reason', 'Demonstrating commitment to Mission and/or Values Statements', null, ['id'=>'reason']) !!}
                {!! Form::label('reason', 'Demonstrating commitment to Mission and/or Values Statements') !!}
            </div>
            <div class="form-group">
                {!! Form::checkbox('reason', 'Performance above and beyond expectations as outlined in the job description', null, ['id'=>'reason']) !!}
                {!! Form::label('reason', 'Performance above and beyond expectations as outlined in the job description') !!}
            </div>
            <div class="form-group">
                {!! Form::checkbox('reason', 'Teamwork', null, ['id'=>'reason']) !!}
                {!! Form::label('reason', 'Teamwork') !!}
            </div>
            <div class="form-group">
                {!! Form::label('reason', 'Other:') !!}
                {!! Form::text('reason', null, ['class' => 'form-control']) !!}
            </div>
        </div>
    </div>
</div>

<div class="col-md-4 col-md-offset-4">
    <div class="panel panel-primary">
        <div class="panel panel-heading"><b>Reason:</b></div>
        <div class="panel-body">
            <div class="form-group">
                {!! Form::label('ticket_number', 'Ticket Number:') !!}
                {!! Form::text('ticket_number', null, ['class' => 'form-control', 'placeholder'=>'Number from meal card.']) !!}
            </div>

        </div>
    </div>
</div>
