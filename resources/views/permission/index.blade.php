@extends('layouts.app')

@section('content')
    <div class="panel panel-success">
        <div class="panel-heading"><b>User Permissions ({{$permissions->count()}})</b></div>
        <div class="panel-body">
    @if ($permissions->isEmpty())
        <div class="alert alert-info" role="alert">You do not have any permissions entered. <a href="/permission/create" role="button" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Permission</a></div>
    @else
    <div class="container-fluid">
                    <div class="row">
                        <div class="input-group">
                            {!! Form::text('search', null,array('class'=>'form-control','id'=>'search','placeholder'=>'Search for a permission...')) !!}
                            <span class="input-group-btn">
                <a href = "/permission/create" class = "btn btn-success" role = "button">Add Permission</a>
                </span>
                        </div>
                    </div>
    </div>
            <BR>
        <table class="table table-hover table-responsive table-bordered table-striped" id="table">

            <thead>
            <tr class="success">
                <td><b>Name</b></td>
                <td class="hidden-xs hidden-sm hidden-md"><b>Label</b></td>
                <td class="hidden-xs"><b>Created</b></td>
                <td align="center" colspan="3"></td>
            </tr>
            </thead>


            @foreach ($permissions as $permission)
                <tbody>
                <tr>
                    <td>{{$permission->name}}</td>
                    <td class="hidden-xs hidden-sm hidden-md">{{$permission->label}}</td>
                    <td class="hidden-xs hidden-sm"> {{$permission->created_at}}</td>
                    <td align="center"><a href="/permission/{{$permission->id}}/edit" role="button" class="btn btn-info btn-xs"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
                    <td align="center"><a href="/permission/del/{{$permission->id}}" role="button" class="btn btn-danger btn-xs" onclick="return ConfirmDelete()"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Delete</a></td>
                </tr>
                </tbody>
            @endforeach
        </table>



    @endif

    </div>
    </div>

@endsection
