{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('anesthesia_admin') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')


    <div class="card mb-4 text-white bg-dark">
        <div class="card-header">
            Anesthesia Monitoring Tool Dashboard
            
            <span class="float-right">
                <a href="/anesthesia/create" class="btn btn-primary btn-sm">New Anesthesia Form</a>
                <a href="#report" data-toggle="modal" class="btn btn-success btn-sm">Generate Report</a>
            </span>
        </div>
        <div class="card-body">
            
            <div class="card bg-secondary mb-3">
                <div class="card-header">
                    Overall Compliance %
                </div>
                <div class="card-body">
                    
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-dark" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: {{ $actual }}%">{{ number_format($actual, 2) }}%</div>
                    </div>

                </div>
            </div>

            <hr class="bg-info">

            <h3>Anesthesiologists Compliance %</h3> <br>

            <div class="card-deck">

                @foreach($doctors as $doctor)

                <?php $doc_total = $doctor->yes + $doctor->no ?>

                @if($doc_total > 0)
                    <?php $doc_actual = ($doctor->yes / $doc_total) * 100 ?>
                @endif

                    <div class="col-lg-4 m-auto">
                        <div class="card bg-secondary mb-3">
                            <div class="card-header">
                                {{ $doctor->anesthesiologist }} Compliance %
                            </div>
                            <div class="card-body">
                                
                                <div class="progress">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:{{ $doc_actual }}%">{{ number_format($doc_actual, 2) }}%</div>
                                </div>
            
                            </div>
                        </div>
                    </div>

                @endforeach

            </div>

            <hr class="bg-info">

            <div class="row">
                <div class="col-7">
                    <h3>Questions Compliance %</h3>
                    <hr>
                </div>
                <div class="col-5">
                    <h3>Detailed Entry</h3>
                    <hr>
                </div>
            </div>


            <div class="row">
                <div class="col-7">
                    <table class="table table-bordered table-secondary table-striped table-hover text-dark">
                        <thead>
                            <th>Question</th>
                            <th nowrap>Compliance %</th>
                        </thead>
                        <tbody>
                            @if($rows > 0)
                                <tr>
                                    <td>Patient was seen preoperative by an anesthesia provider?</td>
                                    <td class="@if(($preop / $preop_rows) * 100 >= 90) bg-success @elseif(($preop / $preop_rows) * 100 >= 80) bg-warning @else bg-danger @endif">
                                        {{ number_format(($preop / $preop_rows) * 100, 2) }} %
                                        ||
                                        {{ $preop }} / {{ $preop_rows }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Anesthesia Preop Evaluation completed within 30 days prior to anesthesia induction?</td>
                                    <td class="@if(($preop_30 / $preop_30_rows) * 100 >= 90) bg-success @elseif(($preop_30 / $preop_30_rows) * 100 >= 80) bg-warning @else bg-danger @endif">
                                        {{ number_format(($preop_30 / $preop_30_rows) * 100, 2) }} %
                                        ||
                                        {{ $preop_30 }} / {{ $preop_30_rows }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>ASA Class?</td>
                                    <td class="@if(($ASA / $ASA_rows) * 100 >= 90) bg-success @elseif(($ASA / $ASA_rows) * 100 >= 80) bg-warning @else bg-danger @endif">
                                        {{ number_format(($ASA / $ASA_rows) * 100, 2) }} %
                                        ||
                                        {{ $ASA }} / {{ $ASA_rows }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Anesthesia Providers' Names?</td>
                                    <td class="@if(($providers / $providers_rows) * 100 >= 90) bg-success @elseif(($providers / $providers_rows) * 100 >= 80) bg-warning @else bg-danger @endif">
                                        {{ number_format(($providers / $providers_rows) * 100, 2) }} %
                                        ||
                                        {{ $providers }} / {{ $providers_rows }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Review of medical record and patient evaluated within 48 hours prior to delivery of anesthesia</td>
                                    <td class="@if(($review_48 / $review_48_rows) * 100 >= 90) bg-success @elseif(($review_48 / $review_48_rows) * 100 >= 80) bg-warning @else bg-danger @endif">
                                        {{ number_format(($review_48 / $review_48_rows) * 100, 2) }} %
                                        ||
                                        {{ $review_48 }} / {{ $review_48_rows }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Airway equipment and anesthesia drugs and resuscitation drugs available prior to induction</td>
                                    <td class="@if(($airway / $airway_rows) * 100 >= 90) bg-success @elseif(($airway / $airway_rows) * 100 >= 80) bg-warning @else bg-danger @endif">
                                        {{ number_format(($airway / $airway_rows) * 100, 2) }} %
                                        ||
                                        {{ $airway }} / {{ $airway_rows }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Anesthesia machine and monitors checked, alarms on and functioning prior to induction</td>
                                    <td class="@if(($machine_monitors / $machine_monitors_rows) * 100 >= 90) bg-success @elseif(($machine_monitors / $machine_monitors_rows) * 100 >= 80) bg-warning @else bg-danger @endif">
                                        {{ number_format(($machine_monitors / $machine_monitors_rows) * 100, 2) }} %
                                        ||
                                        {{ $machine_monitors }} / {{ $machine_monitors_rows }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Documentation of staff MD for all cases utilizing CRNA or Anesthesia Assistants</td>
                                    <td class="@if(($doc_CRNA / $doc_CRNA_rows) * 100 >= 90) bg-success @elseif(($doc_CRNA / $doc_CRNA_rows) * 100 >= 80) bg-warning @else bg-danger @endif">
                                        {{ number_format(($doc_CRNA / $doc_CRNA_rows) * 100, 2) }} %
                                        ||
                                        {{ $doc_CRNA }} / {{ $doc_CRNA_rows }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Documentation of staff MD present for induction, emergence, and all critical points of case</td>
                                    <td class="@if(($doc_MDstaff / $doc_MDstaff_rows) * 100 >= 90) bg-success @elseif(($doc_MDstaff / $doc_MDstaff_rows) * 100 >= 80) bg-warning @else bg-danger @endif">
                                        {{  number_format(($doc_MDstaff / $doc_MDstaff_rows) * 100, 2) }} %
                                        ||
                                        {{ $doc_MDstaff }} / {{ $doc_MDstaff_rows }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Anesthesia Induction Time </td>
                                    <td class="@if(($induction_time / $induction_time_rows) * 100 >= 90) bg-success @elseif(($induction_time / $induction_time_rows) * 100 >= 80) bg-warning @else bg-danger @endif">
                                        {{ number_format(($induction_time / $induction_time_rows) * 100, 2) }} %
                                        ||
                                        {{ $induction_time }} / {{ $induction_time_rows }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Positioning documented (pressure points, head/neck aligned, etc)</td>
                                    <td class="@if(($doc_position / $doc_position_rows) * 100 >= 90) bg-success @elseif(($doc_position / $doc_position_rows) * 100 >= 80) bg-warning @else bg-danger @endif">
                                        {{ number_format(($doc_position / $doc_position_rows) * 100, 2) }} %
                                        ||
                                        {{ $doc_position }} / {{ $doc_position_rows }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Intubation or insertion of airways documented</td>
                                    <td class="@if(($doc_intubation / $doc_intubation_rows) * 100 >= 90) bg-success @elseif(($doc_intubation / $doc_intubation_rows) * 100 >= 80) bg-warning @else bg-danger @endif">
                                        {{ number_format(($doc_intubation / $doc_intubation_rows) * 100, 2) }} %
                                        ||
                                        {{ $doc_intubation }} / {{ $doc_intubation_rows }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Flow rates for gases and oxygen documented</td>
                                    <td class="@if(($doc_flowrate / $doc_flowrate_rows) * 100 >= 90) bg-success @elseif(($doc_flowrate / $doc_flowrate_rows) * 100 >= 80) bg-warning @else bg-danger @endif">
                                        {{ number_format(($doc_flowrate / $doc_flowrate_rows) * 100, 2) }} %
                                        ||
                                        {{ $doc_flowrate }} / {{ $doc_flowrate_rows }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Vital signs, including EtCo2, and vent settings documented at least every 5 minutes throughout the case</td>
                                    <td class="@if(($doc_vital_5 / $doc_vital_5_rows) * 100 >= 90) bg-success @elseif(($doc_vital_5 / $doc_vital_5_rows) * 100 >= 80) bg-warning @else bg-danger @endif">
                                        {{ number_format(($doc_vital_5 / $doc_vital_5_rows) * 100, 2) }} %
                                        ||
                                        {{ $doc_vital_5 }} / {{ $doc_vital_5_rows }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Name, dosage, route and time of administration of drugs and anesthesia agents documented</td>
                                    <td class="@if(($doc_name_time / $doc_name_time_rows) * 100 >= 90) bg-success @elseif(($doc_name_time / $doc_name_time_rows) * 100 >= 80) bg-warning @else bg-danger @endif">
                                        {{ number_format(($doc_name_time / $doc_name_time_rows) * 100, 2) }} %
                                        ||
                                        {{ $doc_name_time }} / {{ $doc_name_time_rows }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Names and amounts of IV fluids administered during the case documented</td>
                                    <td class="@if(($doc_IV_amount / $doc_IV_amount_rows) * 100 >= 90) bg-success @elseif(($doc_IV_amount / $doc_IV_amount_rows) * 100 >= 80) bg-warning @else bg-danger @endif">
                                        {{ number_format(($doc_IV_amount / $doc_IV_amount_rows) * 100, 2) }} %
                                        ||
                                        {{ $doc_IV_amount }} / {{ $doc_IV_amount_rows }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Names and amounts of any blood products administered during the case documented</td>
                                    <td class="@if(($doc_blood_amount / $doc_blood_amount_rows) * 100 >= 90) bg-success @elseif(($doc_blood_amount / $doc_blood_amount_rows) * 100 >= 80) bg-warning @else bg-danger @endif">
                                        {{ number_format(($doc_blood_amount / $doc_blood_amount_rows) * 100, 2) }} %
                                        ||
                                        {{ $doc_blood_amount }} / {{ $doc_blood_amount_rows }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Estimated Blood Loss during the case documented</td>
                                    <td class="@if(($doc_blood_loss / $doc_blood_loss_rows) * 100 >= 90) bg-success @elseif(($doc_blood_loss / $doc_blood_loss_rows) * 100 >= 80) bg-warning @else bg-danger @endif">
                                        {{ number_format(($doc_blood_loss / $doc_blood_loss_rows) * 100, 2) }} %
                                        ||
                                        {{ $doc_blood_loss }} / {{ $doc_blood_loss_rows }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Urine Output during the case documented</td>
                                    <td class="@if(($urine_output / $urine_output_rows) * 100 >= 90) bg-success @elseif(($urine_output / $urine_output_rows) * 100 >= 80) bg-warning @else bg-danger @endif">
                                        {{ number_format(($urine_output / $urine_output_rows) * 100, 2) }} %
                                        ||
                                        {{ $urine_output }} / {{ $urine_output_rows }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Documentation that patient meets transfer to PACU criteria</td>
                                    <td class="@if(($doc_PACU_transfer / $doc_PACU_transfer_rows) * 100 >= 90) bg-success @elseif(($doc_PACU_transfer / $doc_PACU_transfer_rows) * 100 >= 80) bg-warning @else bg-danger @endif">
                                        {{ number_format(($doc_PACU_transfer / $doc_PACU_transfer_rows) * 100, 2) }} %
                                        ||
                                        {{ $doc_PACU_transfer }} / {{ $doc_PACU_transfer_rows }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Normothermia documented at end of anesthesia (96.8-100.4 F)</td>
                                    <td class="@if(($doc_normothermia / $doc_normothermia_rows) * 100 >= 90) bg-success @elseif(($doc_normothermia / $doc_normothermia_rows) * 100 >= 80) bg-warning @else bg-danger @endif">
                                        {{ number_format(($doc_normothermia / $doc_normothermia_rows) * 100, 2) }} %
                                        ||
                                        {{ $doc_normothermia }} / {{ $doc_normothermia_rows }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Documentation of report from Anesthesia to PACU/Recovery Area nurse (including ICU)</td>
                                    <td class="@if(($doc_PACU_ICU / $doc_PACU_ICU_rows) * 100 >= 90) bg-success @elseif(($doc_PACU_ICU / $doc_PACU_ICU_rows) * 100 >= 80) bg-warning @else bg-danger @endif">
                                        {{ number_format(($doc_PACU_ICU / $doc_PACU_ICU_rows) * 100, 2) }} %
                                        ||
                                        {{ $doc_PACU_ICU }} / {{ $doc_PACU_ICU_rows }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Postanesthesia evaluation within 48 hours of move into recovery area (PACU, OPNU, Med-Surg, ICU, etc)</td>
                                    <td class="@if(($post_anesthesia / $post_anesthesia_rows) * 100 >= 90) bg-success @elseif(($post_anesthesia / $post_anesthesia_rows) * 100 >= 80) bg-warning @else bg-danger @endif">
                                        {{ number_format(($post_anesthesia / $post_anesthesia_rows) * 100, 2) }} %
                                        ||
                                        {{ $post_anesthesia }} / {{ $post_anesthesia_rows }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Narcan administration</td>
                                    <td>
                                        {{ number_format(($narcan / $rows) * 100, 2) }} %
                                        ||
                                        {{ $narcan }} / {{ $rows }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Other reversal agent administration</td>
                                    <td>
                                        {{ number_format(($reversal_agent / $rows) * 100, 2) }} %
                                        ||
                                        {{ $reversal_agent }} / {{ $rows }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Severe postop nausea/vomiting</td>
                                    <td>
                                        {{ number_format(($postop_nausea / $rows) * 100, 2) }} %
                                        ||
                                        {{ $postop_nausea }} / {{ $rows }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Cardiac Arrest</td>
                                    <td>
                                        {{ number_format(($cardiac_arrest / $rows) * 100, 2) }} %
                                        ||
                                        {{ $cardiac_arrest }} / {{ $rows }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Anaphylaxis</td>
                                    <td>
                                        {{ number_format(($anaphylaxis / $rows) * 100, 2) }} %
                                        ||
                                        {{ $anaphylaxis }} / {{ $rows }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Malignant Hypothermia</td>
                                    <td>
                                        {{ number_format(($malignant_hypo / $rows) * 100, 2) }} %
                                        ||
                                        {{ $malignant_hypo }} / {{ $rows }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Incorrect site/procedure</td>
                                    <td>
                                        {{ number_format(($incorrect_site / $rows) * 100, 2) }} %
                                        ||
                                        {{ $incorrect_site }} / {{ $rows }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Medication error</td>
                                    <td>
                                        {{ number_format(($med_error / $rows) * 100, 2) }} %
                                        ||
                                        {{ $med_error }} / {{ $rows }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Intraoperative Awareness</td>
                                    <td>
                                        {{ number_format(($intraoperative / $rows) * 100, 2) }} %
                                        ||
                                        {{ $intraoperative }} / {{ $rows }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Unexpected difficult airway</td>
                                    <td>
                                        {{ number_format(($difficult_air / $rows) * 100, 2) }} %
                                        ||
                                        {{ $difficult_air }} / {{ $rows }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Unplanned reintubation</td>
                                    <td>
                                        {{ number_format(($unplanned_reintubation / $rows) * 100, 2) }} %
                                        ||
                                        {{ $unplanned_reintubation }} / {{ $rows }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Dental trauma (during intubation, extubation, etc)</td>
                                    <td>
                                        {{ number_format(($dental / $rows) * 100, 2) }} %
                                        ||
                                        {{ $dental }} / {{ $rows }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>High Spinal</td>
                                    <td>
                                        {{ number_format(($spinal / $rows) * 100, 2) }} %
                                        ||
                                        {{ $spinal }} / {{ $rows }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Flash Pulmonary Edema</td>
                                    <td>
                                        {{ number_format(($flash_edema / $rows) * 100, 2) }} %
                                        ||
                                        {{ $flash_edema }} / {{ $rows }}
                                    </td>
                                </tr>
                            @else
                                <td colspan="2">
                                    No Data for this Date Range
                                </td>
                            @endif
                        </tbody>
        
                    </table>
                </div>
            

                <div class="col-5">
                    <table class="table table-dark table-hover table-striped" id="anesthesiaT">
                        <thead>
                            <th>Date</th>
                            <th>MRN</th>
                            <th>Anesthesiologist</th>
                            <th>Type</th>
                            <th class="text-center">Emergency Procedure</th>
                            <th>Details</th>
                        </thead>
                        <tbody>
                            @foreach($data as $info)
                                <tr>
                                    <td nowrap>{{ Carbon::parse($info->date)->format('m-d-Y') }}</td>
                                    <td>{{ $info->MRN }}</td>
                                    <td>{{ $info->anesthesiologist }}</td>
                                    <td>{{ $info->type }}</td>
                                    <td align="center" class="pt-3"> @if($info->emergency_procedure == 'Yes') <i class="fas fa-check text-success"></i> @else <i class="fas fa-ban text-danger"></i> @endif </td>
                                    <td> <a href="/anesthesia/show/{{ $info->id }}" class="btn btn-info btn-sm">Details</a> </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>

        </div>
        <div class="card-footer">
            <a href="/anesthesia/create" class="btn btn-primary">New Anesthesia Form</a>
            <span class="float-right"> <a href="#report" data-toggle="modal" class="btn btn-success">Generate Report</a> </span>
        </div>
    </div>

    @include('anesthesia.modals.report')

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">
    $(document).ready(function() {
        $('#anesthesiaT').DataTable( {
            "pageLength": 30,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>

@endsection

@endif