<!-- Modal -->
<div class="modal fade" id="report" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content text-white bg-dark">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Anesthesia Monitoring Report</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="/anesthesia/report">
                    {{ csrf_field() }}
    
                    <div class="modal-body">
    
                        <div class="form-group">
                            <label>Doctor (<i>Leave blank for overall compliance within date range</i>)</label>
                            <select name="doctor" class="form-control">
                                <option value="">[Select Doctor]</option>
                                <option value="2006 - Bowers, Emmett">2006 - Bowers, Emmett</option>
                                <option value="3503 - Engel, Matthew">3503 - Engel, Matthew</option>
                                <option value="1339 - Feese, Richard">1339 - Feese, Richard</option>
                                <option value="1386 - Kenworthy, Kevin">1386 - Kenworthy, Kevin</option>
                                <option value="3439 - Megargel, James">3439 - Megargel, James</option>
                                <option value="14576 - Brazil">14576 - Brazil</option>
                                <option value="3482 - Shen, Qiheng">3482 - Shen, Qiheng</option>
                                <option value="1754 - Warren, James">1754 - Warren, James</option>
                                <option value="1280 - Wright, John">1280 - Wright, John</option>
                                <option value="8925 - Hancock">8925 - Hancock</option>
                                <option value="1000075 - Bailey, Travis">1000075 - Bailey, Travis</option>
                                <option value="18125 - Parimucha, Michael">18125 - Parimucha, Michael</option>
                                <option value="Pritchett, Jennifer">Pritchett, Jennifer</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Start Date / Time</label>
                            <input type="date" name="startDate" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>End Date / Time</label>
                            <input type="date" name="endDate" class="form-control" required>
                        </div>
                    </div>
    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" value="Run Report">
                    </div>
    
                </form>
            </div>
        </div>
    </div>