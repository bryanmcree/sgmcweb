{{--New file Template--}}

{{--Add Security for this page below--}}

@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')


<div class="card mb-4 text-white bg-dark">
    <div class="card-header">
        Anesthesia Report
        @if(Auth::user()->hasRole('anesthesia_admin') == TRUE) <span class="float-right"> <a href="/anesthesia" class="btn btn-sm btn-primary">Return To Dashboard</a> </span> @endif
    </div>
    <div class="card-body">
        
        <div class="card bg-secondary mb-3">
            <div class="card-header">
                Please answer all of the following questions to the best of your ability.
            </div>
            <div class="card-body">

                <form action="/anesthesia/update/{{ $anesthesia->id }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="created_by" value="{{ Auth::user()->employee_number }}">
                    <input type="hidden" name="comp_yes">
                    <input type="hidden" name="comp_no">
                    <input type="hidden" name="comp_NA">
                
                    <table class="table table-bordered table-dark">
                        <thead>
                            <th></th>
                            <th>Question</th>
                        </thead>
                        <tbody>

                            <tr>
                                <td>1.</td>
                                <td>
                                    What is the date the surgery was done?
                                    <hr>
                                    <input type="datetime-local" class="form-control" name="date" style="width:50%;" value="{{ Carbon::parse($anesthesia->date)->format('Y-m-d') . 'T' . Carbon::parse($anesthesia->date)->format('H:i:s') }}" required>
                                </td>
                            </tr>
                            <tr>
                                <td>2.</td>
                                <td>
                                    MRN number?
                                    <hr>
                                    <input type="text" class="form-control" name="MRN" style="width:50%;" value="{{ $anesthesia->MRN }}" required>
                                </td>
                            </tr>
                            <tr>
                                <td>3.</td>
                                <td>
                                    Choose Anesthesiologist ID
                                    <hr>
                                    <select class="form-control" name="anesthesiologist" style="width:50%;" required>
                                        <option value="{{ $anesthesia->anesthesiologist }}" selected>{{ $anesthesia->anesthesiologist }}</option>
                                        <option value="2006 - Bowers, Emmett">2006 - Bowers, Emmett</option>
                                        <option value="3503 - Engel, Matthew">3503 - Engel, Matthew</option>
                                        <option value="1339 - Feese, Richard">1339 - Feese, Richard</option>
                                        <option value="1386 - Kenworthy, Kevin">1386 - Kenworthy, Kevin</option>
                                        <option value="3439 - Megargel, James">3439 - Megargel, James</option>
                                        <option value="14576 - Brazil">14576 - Brazil</option>
                                        <option value="3482 - Shen, Qiheng">3482 - Shen, Qiheng</option>
                                        <option value="1754 - Warren, James">1754 - Warren, James</option>
                                        <option value="1280 - Wright, John">1280 - Wright, John</option>
                                        <option value="8925 - Hancock">8925 - Hancock</option>
                                        <option value="1000075 - Bailey, Travis">1000075 - Bailey, Travis</option>
                                        <option value="18125 - Parimucha, Michael">18125 - Parimucha, Michael</option>
                                        <option value="Pritchett, Jennifer">Pritchett, Jennifer</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>4.</td>
                                <td>
                                    Emergency Procedure      
                                    <hr>
                                    <div class="col">
                                        <div class="form-check form-check-inline">
                                            <input style="width:30px; height:30px;" class="form-check-input" type="radio" name="emergency_procedure" value="Yes" @if($anesthesia->emergency_procedure == 'Yes') checked @endif required>
                                            <label style="font-size: 30px;" class="form-check-label">Yes</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input style="width:30px; height:30px;" class="form-check-input" type="radio" name="emergency_procedure" value="No" @if($anesthesia->emergency_procedure == 'No') checked @endif>
                                            <label style="font-size: 30px;" class="form-check-label">No</label>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>5.</td>
                                <td>
                                    Anesthesia Type
                                    <hr>
                                    <select class="form-control" name="type" style="width:50%;" required>
                                        <option value="{{ $anesthesia->type }}">{{ $anesthesia->type }}</option>
                                        <option value="General">General</option>
                                        <option value="Regional">Regional</option>
                                        <option value="MAC">MAC</option>
                                        <option value="Local">Local</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>6.</td>
                                <td>
                                    Patient was seen preoperative by an anesthesia provider? 
                                    <hr>
                                    <select class="form-control" name="preop" style="width:50%;" required>
                                        <option value="{{ $anesthesia->preop }}">{{ $anesthesia->preop }}</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                        <option value="NA">NA</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>7.</td>
                                <td>
                                    Anesthesia Preop Evaluation completed within 30 days prior to anesthesia induction? 
                                    <hr>
                                    <select class="form-control" name="preop_30" style="width:50%;" required>
                                        <option value="{{ $anesthesia->preop_30 }}">{{ $anesthesia->preop_30 }}</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                        <option value="NA">NA</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>8.</td>
                                <td>
                                    ASA Class? 
                                    <hr>
                                    <select class="form-control" name="ASA" style="width:50%;" required>
                                        <option value="{{ $anesthesia->ASA }}">{{ $anesthesia->ASA }}</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                        <option value="NA">NA</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>9.</td>
                                <td>
                                    Anesthesia Providers' Names? 
                                    <hr>
                                    <select class="form-control" name="providers" style="width:50%;" required>
                                        <option value="{{ $anesthesia->providers }}">{{ $anesthesia->providers }}</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                        <option value="NA">NA</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>10.</td>
                                <td>
                                    Review of medical record and patient evaluated within 48 hours prior to delivery of anesthesia 
                                    <hr>
                                    <select class="form-control" name="review_48" style="width:50%;" required>
                                        <option value="{{ $anesthesia->review_48 }}">{{ $anesthesia->review_48 }}</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                        <option value="NA">NA</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>11.</td>
                                <td>
                                    Airway equipment and anesthesia drugs and resuscitation drugs available prior to induction 
                                    <hr>
                                    <select class="form-control" name="airway" style="width:50%;" required>
                                        <option value="{{ $anesthesia->airway }}">{{ $anesthesia->airway }}</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                        <option value="NA">NA</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>12.</td>
                                <td>
                                    Anesthesia machine and monitors checked, alarms on and functioning prior to induction
                                    <hr>
                                    <select class="form-control" name="machine_monitors" style="width:50%;" required>
                                        <option value="{{ $anesthesia->machine_monitors }}">{{ $anesthesia->machine_monitors }}</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                        <option value="NA">NA</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>13.</td>
                                <td>
                                    Documentation of staff MD for all cases utilizing CRNA or Anesthesia Assistants 
                                    <hr>
                                    <select class="form-control" name="doc_CRNA" style="width:50%;" required>
                                        <option value="{{ $anesthesia->doc_CRNA }}">{{ $anesthesia->doc_CRNA }}</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                        <option value="NA">NA</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>14.</td>
                                <td>
                                    Documentation of staff MD present for induction, emergence, and all critical points of case 
                                    <hr>
                                    <select class="form-control" name="doc_MDstaff" style="width:50%;" required>
                                        <option value="{{ $anesthesia->doc_MDstaff }}">{{ $anesthesia->doc_MDstaff }}</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                        <option value="NA">NA</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>15.</td>
                                <td>
                                    Anesthesia Induction Time 
                                    <hr>
                                    <select class="form-control" name="induction_time" style="width:50%;" required>
                                        <option value="{{ $anesthesia->induction_time }}">{{ $anesthesia->induction_time }}</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                        <option value="NA">NA</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>16.</td>
                                <td>
                                    Positioning documented (pressure points, head/neck aligned, etc) 
                                    <hr>
                                    <select class="form-control" name="doc_position" style="width:50%;" required>
                                        <option value="{{ $anesthesia->doc_position }}">{{ $anesthesia->doc_position }}</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                        <option value="NA">NA</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>17.</td>
                                <td>
                                    Intubation or insertion of airways documented 
                                    <hr>
                                    <select class="form-control" name="doc_intubation" style="width:50%;" required>
                                        <option value="{{ $anesthesia->doc_intubation }}">{{ $anesthesia->doc_intubation }}</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                        <option value="NA">NA</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>18.</td>
                                <td>
                                    Flow rates for gases and oxygen documented  
                                    <hr>
                                    <select class="form-control" name="doc_flowrate" style="width:50%;" required>
                                        <option value="{{ $anesthesia->doc_flowrate }}">{{ $anesthesia->doc_flowrate }}</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                        <option value="NA">NA</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>19.</td>
                                <td>
                                    Vital signs, including EtCo2, and vent settings documented at least every 5 minutes throughout the case 
                                    <hr>
                                    <select class="form-control" name="doc_vital_5" style="width:50%;" required>
                                        <option value="{{ $anesthesia->doc_vital_5 }}">{{ $anesthesia->doc_vital_5 }}</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                        <option value="NA">NA</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>20.</td>
                                <td>
                                    Name, dosage, route and time of administration of drugs and anesthesia agents documented 
                                    <hr>
                                    <select class="form-control" name="doc_name_time" style="width:50%;" required>
                                        <option value="{{ $anesthesia->doc_name_time }}">{{ $anesthesia->doc_name_time }}</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                        <option value="NA">NA</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>21.</td>
                                <td>
                                    Names and amounts of IV fluids administered during the case documented 
                                    <hr>
                                    <select class="form-control" name="doc_IV_amount" style="width:50%;" required>
                                        <option value="{{ $anesthesia->doc_IV_amount }}">{{ $anesthesia->doc_IV_amount }}</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                        <option value="NA">NA</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>22.</td>
                                <td>
                                    Names and amounts of any blood products administered during the case documented 
                                    <hr>
                                    <select class="form-control" name="doc_blood_amount" style="width:50%;" required>
                                        <option value="{{ $anesthesia->doc_blood_amount }}">{{ $anesthesia->doc_blood_amount }}</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                        <option value="NA">NA</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>23.</td>
                                <td>
                                    Estimated Blood Loss during the case documented 
                                    <hr>
                                    <select class="form-control" name="doc_blood_loss" style="width:50%;" required>
                                        <option value="{{ $anesthesia->doc_blood_loss }}">{{ $anesthesia->doc_blood_loss }}</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                        <option value="NA">NA</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>24.</td>
                                <td>
                                    Urine Output during the case documented  
                                    <hr>
                                    <select class="form-control" name="urine_output" style="width:50%;" required>
                                        <option value="{{ $anesthesia->urine_output }}">{{ $anesthesia->urine_output }}</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                        <option value="NA">NA</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>25.</td>
                                <td>
                                    Documentation that patient meets transfer to PACU criteria  
                                    <hr>
                                    <select class="form-control" name="doc_PACU_transfer" style="width:50%;" required>
                                        <option value="{{ $anesthesia->doc_PACU_transfer }}">{{ $anesthesia->doc_PACU_transfer }}</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                        <option value="NA">NA</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>26.</td>
                                <td>
                                    Normothermia documented at end of anesthesia (96.8-100.4 F)   
                                    <hr>
                                    <select class="form-control" name="doc_normothermia" style="width:50%;" required>
                                        <option value="{{ $anesthesia->doc_normothermia }}">{{ $anesthesia->doc_normothermia }}</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                        <option value="NA">NA</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>27.</td>
                                <td>
                                    Documentation of report from Anesthesia to PACU/Recovery Area nurse (including ICU)   
                                    <hr>
                                    <select class="form-control" name="doc_PACU_ICU" style="width:50%;" required>
                                        <option value="{{ $anesthesia->doc_PACU_ICU }}">{{ $anesthesia->doc_PACU_ICU }}</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                        <option value="NA">NA</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>28.</td>
                                <td>
                                    Postanesthesia evaluation within 48 hours of move into recovery area (PACU, OPNU, Med-Surg, ICU, etc)    
                                    <hr>
                                    <select class="form-control" name="post_anesthesia" style="width:50%;" required>
                                        <option value="{{ $anesthesia->post_anesthesia }}">{{ $anesthesia->post_anesthesia }}</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                        <option value="NA">NA</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>29.</td>
                                <td>
                                    Narcan administration    
                                    <hr>
                                    <div class="col">
                                        <div class="form-check form-check-inline">
                                            <input style="width:30px; height:30px;" class="form-check-input" type="radio" name="narcan" value="Yes" @if($anesthesia->narcan == 'Yes') checked @endif required>
                                            <label style="font-size: 30px;" class="form-check-label">Yes</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input style="width:30px; height:30px;" class="form-check-input" type="radio" name="narcan" value="No" @if($anesthesia->narcan == 'No') checked @endif>
                                            <label style="font-size: 30px;" class="form-check-label">No</label>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>30.</td>
                                <td>
                                    Other reversal agent administration     
                                    <hr>
                                    <div class="col">
                                        <div class="form-check form-check-inline">
                                            <input style="width:30px; height:30px;" class="form-check-input" type="radio" name="reversal_agent" value="Yes" @if($anesthesia->reversal_agent == 'Yes') checked @endif required>
                                            <label style="font-size: 30px;" class="form-check-label">Yes</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input style="width:30px; height:30px;" class="form-check-input" type="radio" name="reversal_agent" value="No" @if($anesthesia->reversal_agent == 'No') checked @endif>
                                            <label style="font-size: 30px;" class="form-check-label">No</label>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>31.</td>
                                <td>
                                    Severe postop nausea/vomiting      
                                    <hr>
                                    <div class="col">
                                        <div class="form-check form-check-inline">
                                            <input style="width:30px; height:30px;" class="form-check-input" type="radio" name="postop_nausea" value="Yes" @if($anesthesia->postop_nausea == 'Yes') checked @endif required>
                                            <label style="font-size: 30px;" class="form-check-label">Yes</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input style="width:30px; height:30px;" class="form-check-input" type="radio" name="postop_nausea" value="No" @if($anesthesia->postop_nausea == 'No') checked @endif>
                                            <label style="font-size: 30px;" class="form-check-label">No</label>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>32.</td>
                                <td>
                                    Cardiac Arrest     
                                    <hr>
                                    <div class="col">
                                        <div class="form-check form-check-inline">
                                            <input style="width:30px; height:30px;" class="form-check-input" type="radio" name="cardiac_arrest" value="Yes" @if($anesthesia->cardiac_arrest == 'Yes') checked @endif required>
                                            <label style="font-size: 30px;" class="form-check-label">Yes</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input style="width:30px; height:30px;" class="form-check-input" type="radio" name="cardiac_arrest" value="No" @if($anesthesia->cardiac_arrest == 'No') checked @endif>
                                            <label style="font-size: 30px;" class="form-check-label">No</label>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>33.</td>
                                <td>
                                    Anaphylaxis     
                                    <hr>
                                    <div class="col">
                                        <div class="form-check form-check-inline">
                                            <input style="width:30px; height:30px;" class="form-check-input" type="radio" name="anaphylaxis" value="Yes" @if($anesthesia->anaphylaxis == 'Yes') checked @endif required>
                                            <label style="font-size: 30px;" class="form-check-label">Yes</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input style="width:30px; height:30px;" class="form-check-input" type="radio" name="anaphylaxis" value="No" @if($anesthesia->anaphylaxis == 'No') checked @endif>
                                            <label style="font-size: 30px;" class="form-check-label">No</label>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>34.</td>
                                <td>
                                    Malignant Hyperthermia      
                                    <hr>
                                    <div class="col">
                                        <div class="form-check form-check-inline">
                                            <input style="width:30px; height:30px;" class="form-check-input" type="radio" name="malignant_hypo" value="Yes" @if($anesthesia->malignant_hypo == 'Yes') checked @endif required>
                                            <label style="font-size: 30px;" class="form-check-label">Yes</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input style="width:30px; height:30px;" class="form-check-input" type="radio" name="malignant_hypo" value="No" @if($anesthesia->malignant_hypo == 'No') checked @endif>
                                            <label style="font-size: 30px;" class="form-check-label">No</label>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>35.</td>
                                <td>
                                    Incorrect site/procedure      
                                    <hr>
                                    <div class="col">
                                        <div class="form-check form-check-inline">
                                            <input style="width:30px; height:30px;" class="form-check-input" type="radio" name="incorrect_site" value="Yes" @if($anesthesia->incorrect_site == 'Yes') checked @endif required>
                                            <label style="font-size: 30px;" class="form-check-label">Yes</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input style="width:30px; height:30px;" class="form-check-input" type="radio" name="incorrect_site" value="No" @if($anesthesia->incorrect_site == 'No') checked @endif>
                                            <label style="font-size: 30px;" class="form-check-label">No</label>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>36.</td>
                                <td>
                                    Medication error      
                                    <hr>
                                    <div class="col">
                                        <div class="form-check form-check-inline">
                                            <input style="width:30px; height:30px;" class="form-check-input" type="radio" name="med_error" value="Yes" @if($anesthesia->med_error == 'Yes') checked @endif required>
                                            <label style="font-size: 30px;" class="form-check-label">Yes</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input style="width:30px; height:30px;" class="form-check-input" type="radio" name="med_error" value="No" @if($anesthesia->med_error == 'No') checked @endif>
                                            <label style="font-size: 30px;" class="form-check-label">No</label>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>37.</td>
                                <td>
                                    Intraoperative Awareness      
                                    <hr>
                                    <div class="col">
                                        <div class="form-check form-check-inline">
                                            <input style="width:30px; height:30px;" class="form-check-input" type="radio" name="intraoperative" value="Yes" @if($anesthesia->intraoperative == 'Yes') checked @endif required>
                                            <label style="font-size: 30px;" class="form-check-label">Yes</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input style="width:30px; height:30px;" class="form-check-input" type="radio" name="intraoperative" value="No" @if($anesthesia->intraoperative == 'No') checked @endif>
                                            <label style="font-size: 30px;" class="form-check-label">No</label>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>38.</td>
                                <td>
                                    Unexpected difficult airway     
                                    <hr>
                                    <div class="col">
                                        <div class="form-check form-check-inline">
                                            <input style="width:30px; height:30px;" class="form-check-input" type="radio" name="difficult_air" value="Yes" @if($anesthesia->difficult_air == 'Yes') checked @endif required>
                                            <label style="font-size: 30px;" class="form-check-label">Yes</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input style="width:30px; height:30px;" class="form-check-input" type="radio" name="difficult_air" value="No" @if($anesthesia->difficult_air == 'No') checked @endif>
                                            <label style="font-size: 30px;" class="form-check-label">No</label>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>39.</td>
                                <td>
                                    Unplanned reintubation     
                                    <hr>
                                    <div class="col">
                                        <div class="form-check form-check-inline">
                                            <input style="width:30px; height:30px;" class="form-check-input" type="radio" name="unplanned_reintubation" value="Yes" @if($anesthesia->unplanned_reintubation == 'Yes') checked @endif required>
                                            <label style="font-size: 30px;" class="form-check-label">Yes</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input style="width:30px; height:30px;" class="form-check-input" type="radio" name="unplanned_reintubation" value="No" @if($anesthesia->unplanned_reintubation == 'No') checked @endif>
                                            <label style="font-size: 30px;" class="form-check-label">No</label>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>40.</td>
                                <td>
                                    Dental trauma (during intubation, extubation, etc)      
                                    <hr>
                                    <div class="col">
                                        <div class="form-check form-check-inline">
                                            <input style="width:30px; height:30px;" class="form-check-input" type="radio" name="dental" value="Yes" @if($anesthesia->dental == 'Yes') checked @endif required>
                                            <label style="font-size: 30px;" class="form-check-label">Yes</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input style="width:30px; height:30px;" class="form-check-input" type="radio" name="dental" value="No" @if($anesthesia->dental == 'No') checked @endif>
                                            <label style="font-size: 30px;" class="form-check-label">No</label>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>41.</td>
                                <td>
                                    High Spinal      
                                    <hr>
                                    <div class="col">
                                        <div class="form-check form-check-inline">
                                            <input style="width:30px; height:30px;" class="form-check-input" type="radio" name="spinal" value="Yes" @if($anesthesia->spinal == 'Yes') checked @endif required>
                                            <label style="font-size: 30px;" class="form-check-label">Yes</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input style="width:30px; height:30px;" class="form-check-input" type="radio" name="spinal" value="No" @if($anesthesia->spinal == 'No') checked @endif>
                                            <label style="font-size: 30px;" class="form-check-label">No</label>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>42.</td>
                                <td>
                                    Flash Pulmonary Edema       
                                    <hr>
                                    <div class="col">
                                        <div class="form-check form-check-inline">
                                            <input style="width:30px; height:30px;" class="form-check-input" type="radio" name="flash_edema" value="Yes" @if($anesthesia->flash_edema == 'Yes') checked @endif required>
                                            <label style="font-size: 30px;" class="form-check-label">Yes</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input style="width:30px; height:30px;" class="form-check-input" type="radio" name="flash_edema" value="No" @if($anesthesia->flash_edema == 'No') checked @endif>
                                            <label style="font-size: 30px;" class="form-check-label">No</label>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                        </tbody>

                        <tfoot>
                            <td colspan="2"> <input type="submit" class="btn btn-primary btn-block"> </td>
                        </tfoot>

                    </table>
                
                </form>

            </div>

        </div> {{-- card --}}

    </div>
</div>


@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
