{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('anesthesia_admin') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')


    <div class="card mb-4 text-white bg-dark">
        <div class="card-header">
            Anesthesia Monitoring Details
            <span class="float-right"> 
                <a href="/anesthesia/edit/{{ $anesthesia->id }}" class="btn btn-warning btn-sm">Edit</a>
                <a href="/anesthesia" class="btn btn-sm btn-primary">Return To Dashboard</a> 
            </span>
        </div>
        <div class="card-body">

            <table class="table table-dark table-hover">
                <thead>
                    <th>Date</th>
                    <th>MRN</th>
                    <th>Anesthesiologist</th>
                    <th>Type</th>
                    <th class="text-center">Emergency Procedure</th>
                </thead>
                <tbody>
                    <td>{{ Carbon::parse($anesthesia->date)->format('m-d-Y') }}</td>
                    <td>{{ $anesthesia->MRN }}</td>
                    <td>{{ $anesthesia->anesthesiologist }}</td>
                    <td>{{ $anesthesia->type }}</td>
                    <td align="center"> @if($anesthesia->emergency_procedure == 'Yes') <i class="fas fa-check text-success"></i> @else <i class="fas fa-ban text-danger"></i> @endif </td>
                </tbody>
            </table>

            <table class="table table-bordered table-dark">
                <thead>
                    <th>Question</th>
                    <th style="width:50%;">Answer</th>
                </thead>
                <tbody>

                    <tr>
                        <td>1. What is the date the surgery was done?</td>
                        <td>
                            {{ Carbon::parse($anesthesia->date)->format('m-d-Y') }}
                        </td>
                    </tr>
                    <tr>
                        <td>2. MRN number?</td>
                        <td>
                            {{ $anesthesia->MRN }}
                        </td>
                    </tr>
                    <tr>
                        <td>3. Choose Anesthesiologist ID</td>
                        <td>
                            {{ $anesthesia->anesthesiologist }}
                        </td>
                    </tr>
                    <tr>
                        <td>4. Anesthesia Type</td>
                        <td>
                            {{ $anesthesia->type }}
                        </td>
                    </tr>
                    <tr>
                        <td>5. Patient was seen preoperative by an anesthesia provider?</td>
                        <td>
                            {{ $anesthesia->preop }}
                        </td>
                    </tr>
                    <tr>
                        <td>6. Anesthesia Preop Evaluation completed within 30 days prior to anesthesia induction?</td>
                        <td>
                            {{ $anesthesia->preop_30 }}
                        </td>
                    </tr>
                    <tr>
                        <td>7. ASA Class?</td>
                        <td>
                            {{ $anesthesia->ASA }}
                        </td>
                    </tr>
                    <tr>
                        <td>8. Anesthesia Providers' Names?</td>
                        <td>
                            {{ $anesthesia->providers }}
                        </td>
                    </tr>
                    <tr>
                        <td>9. Review of medical record and patient evaluated within 48 hours prior to delivery of anesthesia</td>
                        <td>
                            {{ $anesthesia->review_48 }}
                        </td>
                    </tr>
                    <tr>
                        <td>10. Airway equipment and anesthesia drugs and resuscitation drugs available prior to induction</td>
                        <td>
                            {{ $anesthesia->airway }}
                        </td>
                    </tr>
                    <tr>
                        <td>11. Anesthesia machine and monitors checked, alarms on and functioning prior to induction</td>
                        <td>
                            {{ $anesthesia->machine_monitors }}
                        </td>
                    </tr>
                    <tr>
                        <td>12. Documentation of staff MD for all cases utilizing CRNA or Anesthesia Assistants</td>
                        <td>
                            {{ $anesthesia->doc_CRNA }}
                        </td>
                    </tr>
                    <tr>
                        <td>13. Documentation of staff MD present for induction, emergence, and all critical points of case</td>
                        <td>
                            {{ $anesthesia->doc_MDstaff }}
                        </td>
                    </tr>
                    <tr>
                        <td>14. Anesthesia Induction Time </td>
                        <td>
                            {{ $anesthesia->induction_time }}
                        </td>
                    </tr>
                    <tr>
                        <td>15. Positioning documented (pressure points, head/neck aligned, etc)</td>
                        <td>
                            {{ $anesthesia->doc_position }}
                        </td>
                    </tr>
                    <tr>
                        <td>16. Intubation or insertion of airways documented</td>
                        <td>
                            {{ $anesthesia->doc_intubation }}
                        </td>
                    </tr>
                    <tr>
                        <td>17. Flow rates for gases and oxygen documented</td>
                        <td>
                            {{ $anesthesia->doc_flowrate }}
                        </td>
                    </tr>
                    <tr>
                        <td>18. Vital signs, including EtCo2, and vent settings documented at least every 5 minutes throughout the case</td>
                        <td>
                            {{ $anesthesia->doc_vital_5 }}
                        </td>
                    </tr>
                    <tr>
                        <td>19. Name, dosage, route and time of administration of drugs and anesthesia agents documented</td>
                        <td>
                            {{ $anesthesia->doc_name_time }}
                        </td>
                    </tr>
                    <tr>
                        <td>20. Names and amounts of IV fluids administered during the case documented</td>
                        <td>
                            {{ $anesthesia->doc_IV_amount }}
                        </td>
                    </tr>
                    <tr>
                        <td>21. Names and amounts of any blood products administered during the case documented</td>
                        <td>
                            {{ $anesthesia->doc_blood_amount }}
                        </td>
                    </tr>
                    <tr>
                        <td>22. Estimated Blood Loss during the case documented</td>
                        <td>
                            {{ $anesthesia->doc_blood_loss }}
                        </td>
                    </tr>
                    <tr>
                        <td>23. Urine Output during the case documented</td>
                        <td>
                            {{ $anesthesia->urine_output }}
                        </td>
                    </tr>
                    <tr>
                        <td>24. Documentation that patient meets transfer to PACU criteria</td>
                        <td>
                            {{ $anesthesia->doc_PACU_transfer }}
                        </td>
                    </tr>
                    <tr>
                        <td>25. Normothermia documented at end of anesthesia (96.8-100.4 F)</td>
                        <td>
                            {{ $anesthesia->doc_normothermia }}
                        </td>
                    </tr>
                    <tr>
                        <td>26. Documentation of report from Anesthesia to PACU/Recovery Area nurse (including ICU)</td>
                        <td>
                            {{ $anesthesia->doc_PACU_ICU }}
                        </td>
                    </tr>
                    <tr>
                        <td>27. Postanesthesia evaluation within 48 hours of move into recovery area (PACU, OPNU, Med-Surg, ICU, etc)</td>
                        <td>
                            {{ $anesthesia->post_anesthesia }}
                        </td>
                    </tr>
                    <tr>
                        <td>28. Narcan administration</td>
                        <td>
                            {{ $anesthesia->narcan }}
                        </td>
                    </tr>
                    <tr>
                        <td>29. Other reversal agent administration</td>
                        <td>
                            {{ $anesthesia->reversal_agent }}
                        </td>
                    </tr>
                    <tr>
                        <td>30. Severe postop nausea/vomiting</td>
                        <td>
                            {{ $anesthesia->postop_nausea }}
                        </td>
                    </tr>
                    <tr>
                        <td>31. Cardiac Arrest</td>
                        <td>
                            {{ $anesthesia->cardiac_arrest }}
                        </td>
                    </tr>
                    <tr>
                        <td>32. Anaphylaxis</td>
                        <td>
                            {{ $anesthesia->anaphylaxis }}
                        </td>
                    </tr>
                    <tr>
                        <td>33. Malignant Hypothermia</td>
                        <td>
                            {{ $anesthesia->malignant_hypo }}
                        </td>
                    </tr>
                    <tr>
                        <td>34. Incorrect site/procedure</td>
                        <td>
                            {{ $anesthesia->incorrect_site }}
                        </td>
                    </tr>
                    <tr>
                        <td>35. Medication error</td>
                        <td>
                            {{ $anesthesia->med_error }}
                        </td>
                    </tr>
                    <tr>
                        <td>36. Intraoperative Awareness</td>
                        <td>
                            {{ $anesthesia->intraoperative }}
                        </td>
                    </tr>
                    <tr>
                        <td>37. Unexpected difficult airway</td>
                        <td>
                            {{ $anesthesia->difficult_air }}
                        </td>
                    </tr>
                    <tr>
                        <td>38. Unplanned reintubation</td>
                        <td>
                            {{ $anesthesia->unplanned_reintubation }}
                        </td>
                    </tr>
                    <tr>
                        <td>39. Dental trauma (during intubation, extubation, etc)</td>
                        <td>
                            {{ $anesthesia->dental }}
                        </td>
                    </tr>
                    <tr>
                        <td>40. High Spinal</td>
                        <td>
                            {{ $anesthesia->spinal }}
                        </td>
                    </tr>
                    <tr>
                        <td>41. Flash Pulmonary Edema</td>
                        <td>
                            {{ $anesthesia->flash_edema }}
                        </td>
                    </tr>

                </tbody>

                <tfoot>
                    <td colspan="2"> <input type="submit" class="btn btn-primary btn-block"> </td>
                </tfoot>

            </table>

        </div>
    </div>

    @include('anesthesia.modals.report')

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection

@endif