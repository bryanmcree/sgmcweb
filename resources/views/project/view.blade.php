{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('PM') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <b>{{$project->project_name}}</b>
            </div>
            <div class="panel-body">

            @include("project.includes.view")

            </div>
        </div>
    </div>


    @include("project.modals.add_task")

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">



    </script>

@endsection
@endif