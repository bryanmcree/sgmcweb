{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('PM') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <b>Project Management Dashboard</b>
            </div>
            <div class="panel-body">
                <div class="col-md-3">
                    <div class="panel panel-default">
                        <div class="panel panel-heading">
                            <b>Projects</b>
                        </div>
                        <div class="panel-body">

                            <ul class="nav nav-pills btn-sm">
                                <li class="active"><a data-toggle="pill" href="#home">Projects</a></li>
                                <li><a data-target=".AddProject" data-toggle="modal" href="#">New Project</a></li>
                                <li><a data-toggle="pill" href="#menu1">Completed Projects</a></li>
                            </ul>

                            <div class="tab-content">
                                <div id="home" class="tab-pane fade in active">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <td><b>My Projects</b></td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($projects as $project)
                                            <tr>
                                                <td>{{$project->project_name}}</td>
                                                <td align="right"><a href="/project/{{$project->id}}" class="btn btn-sm btn-primary"><i class="fas fa-images"></i></a> </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div id="menu1" class="tab-pane fade">
                                    @if($completed_projects->isEmpty())
                                        <div class="alert alert-info">You don't have any completed projects yet!</div>
                                    @else
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <td><b>Completed Projects</b></td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($completed_projects as $project)
                                                <tr>
                                                    <td>{{$project->project_name}}</td>
                                                    <td align="right"><a href="/project/{{$project->id}}" class="btn btn-sm btn-primary"><i class="fas fa-images"></i></a> </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @endif
                                </div>
                                <div id="menu2" class="tab-pane fade">
                                    <h3>Menu 2</h3>
                                    <p>Some content in menu 2.</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="panel panel-default">
                        <div class="panel panel-heading">
                            <b>To Do..</b>
                        </div>
                        <div class="panel-body">
                            <ul class="list-unstyled">
                                @foreach($projects as $project)
                                <li><h4><b>{{$project->project_name}}</b></h4>
                                    <ul>
                                        @foreach($project->projectTask as $task)
                                        <li>{{$task->task_name}}</li>
                                        @endforeach
                                    </ul>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include("project.modals.add_project")

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
@endif