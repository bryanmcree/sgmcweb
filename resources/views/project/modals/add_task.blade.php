<div class="modal fade AddTask" id="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">
                <button type="button" class="close btn-sgmc" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-file-o" aria-hidden="true"></i> <b>New Task</b></h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/project/task">
                    {{ csrf_field() }}
                    <input type="hidden" value="{{Auth::user()->employee_number}}" name="created_by">
                    <input type="hidden" value="{{Auth::user()->employee_number}}" name="assigned_to">
                    <input type="hidden" value="{{$project->id}}" name="project_id">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label for="task_name_id">Task Name:</label>
                                        <input type="text" class="form-control" id="task_name_id" name="task_name" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="task_description_id">Task Description:</label>
                                        <textarea class="form-control" id="task_description_id" name="task_description"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <div class="RightLeft">
                    <input type="submit" class="btn btn-sgmc" value="Create Task">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                </div>
            </div>
            </form>
        </div>
    </div>
</div>