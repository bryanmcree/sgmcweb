<div class="modal fade AddProject" id="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">
                <button type="button" class="close btn-sgmc" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-file-o" aria-hidden="true"></i> <b>New Project</b></h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/project/new">
                    {{ csrf_field() }}
                    <input type="hidden" value="{{Auth::user()->employee_number}}" name="created_by">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label for="job_code_id">Project Name:</label>
                                        <input type="text" class="form-control" id="project_name_id" name="project_name" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <div class="RightLeft">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-sgmc" value="Create Project">
                </div>
            </div>
            </form>
        </div>
    </div>
</div>