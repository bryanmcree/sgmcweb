<div class="col-md-6">
    <div class="panel panel-default">
        <div class="panel-heading panel-heading-custom">
            <b>{{$selected_task->task_name}}</b>
        </div>
        <div class="panel-body">
            <ul class="nav nav-pills btn-sm">
                <li><a class="disabled" href="#">Mark as Complete</a></li>
                <li><a href="#">Add Sub-Task</a></li>
            </ul>

            {{$selected_task->task_description}}




            @if($sub_task->isEmpty())

            @else
                <table class="table">
                    <thead>
                    <tr>
                        <td><b>Completed Projects</b></td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($sub_task as $task)
                        <tr>
                            <td>{{$task->sub_task_name}}</td>
                            <td align="right"><a href="/project/{{$project->id}}" class="btn btn-sm btn-primary"><i class="fas fa-images"></i></a> </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
</div>