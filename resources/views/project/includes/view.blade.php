<div class="col-md-6">
    <div class="panel panel-default">
        <div class="panel-heading">
            <b>Task</b>

        </div>
        <div class="panel-body">

            <ul class="nav nav-pills btn-sm">
                <li class="active"><a data-toggle="pill" href="#home">Task</a></li>
                <li><a href="#" data-target=".AddTask" data-toggle="modal">Add Task</a></li>
                <li><a data-toggle="pill" href="#menu1">Completed Task</a></li>

                <li><a href="/project" >Back to Projects</a></li>
            </ul>

            <div class="tab-content">
                <div id="home" class="tab-pane fade in active">
                    <br>
                    <table class="table">
                        <tbody>
                        @foreach($tasks as $task)
                            <tr>
                                <td><i class="fas fa-check" title="Mark as Complete" style="color:Tomato"></i></td>
                                <td>{{$task->task_name}}</td>
                                <td>{{\Carbon\Carbon::createFromTimeStamp(strtotime($task->created_at))->diffForHumans()}}</td>
                                <td>{{$task->assignedTo->last_name}}, {{$task->assignedTo->first_name}}</td>
                                <td align="right"><a href="/project/{{$project->id}}?task={{$task->id}}" class="btn btn-sm btn-sgmc"><i class="fas fa-images"></i></a> </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div id="menu1" class="tab-pane fade">
                    <h3>Completed Task</h3>
                    <p>Some content in menu 1.</p>
                </div>
            </div>


        </div>
    </div>
</div>

@if(!is_null($selected_task))
    @include("project.includes.task")
@endif