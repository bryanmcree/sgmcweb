{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('PM') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <b>{{$project->project_name}}</b> <a href="/project" class="btn btn-primary pull-right btn-xs">Back to Projects</a>
            </div>
            <div class="panel-body">

                @include("project.includes.view")

                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2><b>Share Project With...</b></h2>
                            <div class="panel-sub-title">
                                <i>Find users below to share project with.  Anyone you share this project with will be allowed to participate.</i>
                            </div>
                        </div>
                        <div class="panel-body">
                            Task details
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @include("project.modals.add_task")

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">



    </script>

@endsection
@endif