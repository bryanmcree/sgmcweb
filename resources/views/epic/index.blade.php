{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Epic Security') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="card bg-dark text-white mb-3">
            <div class="card-header">
                <b>Download Epic Security Report</b>
            </div>
            <div class="card-body">
                <a href="/epicsecurity/download/" class="btn btn-primary">Download</a>
            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
@endif