@extends('layouts.app')

@section('content')

@if(count($medicalAlerts) > 0)
    <div class="alert bg-danger text-white alert-dismissible  fade show"><h4>You have a medical alert! <a href="/employee_health/{{Auth::user()->employee_number}}" style="color: black;">Click here</a> to view your alerts.</h4>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif


<div class="card bg-dark text-white mb-3">
    <div class="card-header">
        <nav>
            <div class="nav nav-tabs btn-sm" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Dashboard</a>
                @if ( Auth::user()->hasRole('History - Widget'))
                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Duplicate Users</a>
                <a class="nav-item nav-link" id="nav-demo-tab" href="/stats" role="tab" >SGMC Stats</a>
                @endif
                @if (Auth::user()->hasRole('ExtraMile'))
                    <a class="nav-item nav-link" id="nav-em-tab" data-toggle="tab" href="#nav-em" role="tab" aria-controls="nav-em" aria-selected="false">Extra Mile</a>
                @endif
                    <a class="nav-item nav-link" id="nav-em-tab" href="/census" role="tab" aria-controls="nav-em" aria-selected="false">Census</a>
                @if (Auth::user()->hasRole('Daily Scorecard'))
                    <a class="nav-item nav-link" id="nav-demo-tab" href="/daily" role="tab" >Daily Scorecard</a>
                @endif
            </div>
        </nav>
    </div>
</div>

<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
        <div class="card-deck">

            @if ( Auth::user()->hasRole('History - Widget'))
                <div class="card text-white bg-dark mb-3">
                    <div class="card-body">
                        <div class="card mb-4 border-info text-white bg-dark">
                            <div class="card-body">
                                <div class="" id="LoadHistory"></div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            <div class="card text-white bg-dark mb-3">
                <div class="card-body">
                    <div class="card mb-4 border-info text-white bg-dark">
                        <div class="card-body ">
                            <div class="row">
                                <div class="col">
                                    @if(Auth::user()->photo != '')
                                        <img src="data:image/jpeg;base64,{{str_replace('</photo>','',str_replace('<photo>','',Auth::user()->photo))}}" height="215" />
                                    @else
                                        <img src="img/nopic.png" width="60" height="60">
                                    @endif
                                </div>
                                <div class="col">
                                    <B>ID #:</B> {{$empstats->employee_number}}<br>
                                    <B>Title:</B> {{$empstats->title}}<br>
                                    <b>Cost Center:</b> {{$empstats->unit_code}} / {{$empstats->unit_code_description}}<br>
                                    <B>Hire Date:</B>@if(empty($empstats->hire_date)) No hire Date @else {{$empstats->hire_date->format('m-d-Y')}} / {{\Carbon\Carbon::createFromTimeStamp(strtotime($empstats->hire_date))->diffinMonths()}} Months @endif<br>
                                    <B>PTO:</B> {{$empstats->accrued - $empstats->taken}} Hours <i>* Pre API balance not included.</i><br>
                                    <hr>
                                    <b>Nurse:</b>@if($empstats->RN == 'Yes')<i class="fa fa-user-md text-success" aria-hidden="true"></i>@else <i class="fa fa-user-md text-danger" aria-hidden="true"></i> @endif <br>
                                    <b>Direct Patient Contact:</b>@if($empstats->DC == 'Yes')<i class="fa fa-check-square-o text-success" aria-hidden="true"></i>@else <i class="fas fa-ban" style="color:red;"></i> @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    @if ( Auth::user()->hasRole('Hand washing Admin'))
                        <div class="card mb-4 border-info text-white bg-dark">
                            <div class="card-body" style="height:200px; overflow-y: auto;">
                                <div class="" id="LoadHandwashing"></div>
                            </div>
                        </div>
                    @endif
                    @if ( Auth::user()->hasRole('Widget - Extra Mile'))
                    <div class="card mb-4 border-info text-white bg-dark">
                        <div class="card-body" style="height:200px; overflow-y: auto;">
                            <div class="" id="LoadExtraMile"></div>
                        </div>
                    </div>
                    @endif
                </div>

            </div>
        </div>
    </div>
    @if ( Auth::user()->hasRole('History - Widget'))
    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
        <div class="card mb-3 border-info text-white bg-dark">
            <div class="card-body" style="height:600px; overflow-y: auto;">
                <div class="" id="LoadDupes"></div>
            </div>
        </div>
    </div>
    @endif
    @if ( Auth::user()->hasRole('ExtraMile'))
        <div class="tab-pane fade" id="nav-em" role="tabpanel" aria-labelledby="nav-em-tab">
            <div class="card mb-3 border-info text-white bg-dark">
                <div class="card-body" style="height:90%; overflow-y: auto;">
                    Awards in the last 6 Months: {{$extra_mile_6->count()}}
                    <div class="text-right">
                        <a class="btn btn-primary mb-3" data-toggle="modal" data-target="#emReport" aria-selected="false">Generate Report</a>
                        <a href="/extramile/all" class="btn btn-secondary mb-3">ALL Extra Mile Awards</a>
                    </div>
                    
                    <table class="table" id="em" width="100%">
                        <thead>
                            <tr>
                                <td><b>Presenter</b></td>
                                <td><b>Receiver</b></td>
                                <td align="center"><b>Meal Ticket #</b></td>
                                <td align="center"><b>Customer</b></td>
                                <td align="center"><b>Handling</b></td>
                                <td align="center"><b>Demonstrating</b></td>
                                <td align="center"><b>Performance</b></td>
                                <td align="center"><b>Teamwork</b></td>
                                <td><b>Issued</b></td>
                                <td align="right"><b>Reprint</b></td>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($extra_mile as $em)
                            <tr>
                                <td>@if(empty($em->presenterInfo->name)) 'N/A' @else {{$em->presenterInfo->name}} @endif</td>
                                <td>@if(empty($em->receiverInfo->name)) 'N/A' @else {{$em->receiverInfo->name}} @endif</td>
                                <td align="center">{{$em->mtn}}</td>
                                <td align="center">@if(!is_null($em->customer)) <i class="far fa-check-circle" style="color: green;"></i> @else - @endif</td>
                                <td align="center">@if(!is_null($em->handling)) <i class="far fa-check-circle" style="color: green;"></i> @else - @endif</td>
                                <td align="center">@if(!is_null($em->demonstrating)) <i class="far fa-check-circle" style="color: green;"></i> @else - @endif</td>
                                <td align="center">@if(!is_null($em->performance)) <i class="far fa-check-circle" style="color: green;"></i> @else - @endif</td>
                                <td align="center">@if(!is_null($em->teamwork)) <i class="far fa-check-circle" style="color: green;"></i> @else - @endif</td>
                                <td>{{$em->created_at->format('m-d-Y')}}</td>
                                <td align="right"><a href="extramile/download/{{$em->id}}"><i class="fas fa-print"></i></a> </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    @endif
</div>


@include('extramile')


@endsection

@section('scripts')

    <script type="application/javascript">

        $(document).ready(function() {
            $('#em').DataTable( {
                "pageLength": 20,
                "order": [],
                "columnDefs": [ {
                    "targets"  : 'no-sort',
                    "orderable": false,
                }]
            } );
        } );

        $(document).ready(function() {
            $('#LoadHistory').load('/history/feed');
            //$("#RequestFeed").load("/reports/request/feed");
            //$('#LoadTermination').load('/widgets/terminations').hide().fadeIn("slow");
            $("#LoadExtraMile").load('/widgets/extramile');
            $("#LoadSGMCbar").load('/widgets/SGMCbar');
            $("#LoadDupes").load('/widgets/dupes');
            $("#LoadHandwashing").load('/handwashing/widget');

            var refreshId = setInterval(function() {
                $("#LoadHandwashing").load('/handwashing/widget');
            }, 180000);

            var refreshId2 = setInterval(function() {
                $("#LoadHistory").load('/history/feed');
            }, 180000);

            //var refreshId3 = setInterval(function() {
            //    $("#LoadTermination").load('/widgets/terminations');
            //}, 10000);

            //var refreshId4 = setInterval(function() {
            //    $("#LoadExtraMile").load('/widgets/extramile');
            //}, 10000);

            //var refreshId4 = setInterval(function() {
            //    $("#LoadSGMCbar").load('/widgets/SGMCbar');
            //}, 10000);


            $.ajaxSetup({ cache: false });
        });


        $(document).ready(function() {
            oTable = $('#users').DataTable(
                    {
                        "info":     false,
                        "pageLength": 10,
                        "lengthChange": false,
                        "order": [],
                        "columnDefs": [
                            { targets: 'no-sort', orderable: false }
                        ],
                        "dom": '<l<t>ip>'

                    }
            );
            $('#myInputTextField').keyup(function(){
                oTable.search($(this).val()).draw() ;
            })

        });

    </script>

    @if(is_null(\Auth::user()->manager_emp_number))


        <script type="text/javascript">
            $('#myInputTextField').focus()
            $(window).load(function(){
            $('.AddManager').modal('show', function() {
                $('#myInputTextField').focus()
            });

            });

        </script>
    @endif

@endsection