{{--New file Template--}}

{{--Add Security for this page below--}}


    @extends('layouts.new_nonav')
    {{--Page Design Goes Below--}}
@section('content')
<form method="post" action="/roster/attendant">
    <input type="hidden" name="roster_id" value="{{$roster->id}}">
    {{ csrf_field() }}
    <div class="card text-white bg-dark">
        <div class="card-header">
            <h4><b>{{$roster->roster_name}}</b></h4>
        </div>
        <div class="card-body ">
            <div class="row justify-content-center align-items-center">
                <div class="col-lg-3">
                    <p align="center" style="font-size: 30px; font-weight: bold">PLEASE SIGN IN</p>
                    <div class="form-group input-group-lg">
                        <input type="number" name="employee_number" id="input" class="form-control input-lg" placeholder="Enter Employee Number" autofocus="autofocus"/>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center align-items-center">
                <div class="col-lg-3">
                    <p align="center" style="font-size: 30px; font-weight: bold">{{$attendants->count()}} participants</p>
                </div>
            </div>

            <p align="center" style="font-size: 20px; font-weight: bold">Scan or Enter you employee number.</p>

            @if($attendants->isEmpty())
                <div class="alert alert-info">There are no attendants.</div>
            @else
                <table class="table stripped">
                    <thead>
                    <tr>
                        <td><b>Picture</b></td>
                        <td><b>Name</b></td>
                        <td class="d-none d-sm-table-cell">Employee #</td>
                        <td class="d-none d-sm-table-cell"><b>Title</b></td>
                        <td class="d-none d-sm-table-cell"><b>Cost Center</b></td>
                        <td class="d-none d-sm-table-cell"><b>Date / Time</b></td>
                        <td class="d-none d-sm-table-cell"><b>Remove</b></td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($attendants as $participant)
                        <tr>
                            <TD style="vertical-align:middle;">@if($participant->attendantInfo != '')<a href="data:image/jpeg;base64,{{str_replace('</photo>','',str_replace('<photo>','',$participant->attendantInfo->photo))}}" target="_blank"><img src="data:image/jpeg;base64,{{str_replace('</photo>','',str_replace('<photo>','',$participant->attendantInfo->photo))}}" height="60" width="60"/></a>@else {{ Html::image('img/nopic.png', 'alt', array( 'width' => 60, 'height' => 60 )) }} @endif</TD>
                            <TD nowrap="" style="vertical-align:middle;">{{$participant->attendantInfo->last_name}}, {{$participant->attendantInfo->first_name}}</TD>
                            <td class="d-none d-sm-table-cell" style="vertical-align:middle;"> {{ $participant->employee_number }} </td>
                            <TD class="d-none d-sm-table-cell" style="vertical-align:middle;">{{$participant->attendantInfo->title}}</TD>
                            <TD class="d-none d-sm-table-cell" style="vertical-align:middle;">{{$participant->attendantInfo->unit_code}} - {{$participant->attendantInfo->unit_code_description}}</TD>
                            <TD class="d-none d-sm-table-cell" style="vertical-align:middle;">{{$participant->created_at}}</TD>
                            <TD class="d-none d-sm-table-cell" style="vertical-align:middle;"> <a href="/roster/attendant/remove/{{$participant->id}}" class="btn btn-danger">Remove</a> </TD>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
</form>


@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        $('#input').focusout(function () {
            console.log('on focusout' + new Date());
            setTimeout(function() {
                $('#input').focus();
            });
        });

        $('#form').submit(function( event ) {
            console.log("submit", $("#input").val());
            event.preventDefault();
        })


    </script>

@endsection
