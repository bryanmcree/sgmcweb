{{--New file Template--}}

{{--Add Security for this page below--}}


    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="card text-white bg-dark mb-3">
            <div class="card-header">
                <span style="font-size:1.5em">Rosters</span>
                <span class="float-right"><a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".CreateRoster">Create New Roster</a></span>
            </div>
            <div class="card-body">
                       
                <ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">My Rosters</a>
                    </li>
                    @if(Auth::user()->employee_number == '21012' || Auth::user()->employee_number == '19745')
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Other Rosters</a>
                        </li>
                    @endif
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="card bg-secondary mb-3" style="border-radius:0px;">
                            <div class="card-header">
                                <h4><b>My Rosters</b></h4>
                            </div>
                            <div class="card-body">
                                @if ($rosters->isEmpty())
                                    <div class="bg-info alert"> <h4><b>You don't have any rosters!</b></h4> </div>
                                @else
                                    <table class="table table-dark table-hover table-striped" id="myRoster">
                                        <thead>
                                            <tr>
                                                <td><b>Roster Name</b></td>
                                                <td class="d-none d-sm-table-cell"><b>Visible</b></td>
                                                <td class="d-none d-sm-table-cell"><b>Active</b></td>
                                                <td class="d-none d-sm-table-cell"><b>Created At</b></td>
                                                <td><b># of Attendees</b></td>
                                                <td>Load</td>
                                                <td class="text-center">Export / Edit / Delete</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($rosters as $roster)
                                            <tr>
                                                <td style="vertical-align:middle;">{{$roster->roster_name}}</td>
                                                <td class="d-none d-sm-table-cell" style="vertical-align:middle;">{{$roster->visible}}</td>
                                                <td class="d-none d-sm-table-cell" style="vertical-align:middle;">{{$roster->active}}</td>
                                                <td class="d-none d-sm-table-cell" style="vertical-align:middle;" class="hidden-xs hidden-sm hidden-md">{{$roster->created_at}}</td>
                                                <td>
                                                    @foreach ($attendants as $attendant)
                                                        @if($attendant->roster_id == $roster->id)
                                                            {{ $attendant->total }}
                                                        @endif
                                                    @endforeach 
                                                </td>
                                                <td>
                                                    <a href="/roster/load/{{$roster->id}}" class="btn btn-sm btn-info btn-block" target="_blank">Load</a>
                                                </td>
                                                <td class="text-center">
                                                    <a href="/roster/download/{{$roster->id}}" title="Download"><i class="fa-2x fas fa-file-export"></i></a>
                                                    <a href="#" class="edit-roster" title="Edit" data-id="{{$roster->id}}" data-name="{{$roster->roster_name}}" data-active="{{$roster->active}}" data-visible="{{$roster->visible}}"> <i class="fa-2x text-warning fas fa-edit"></i></a>
                                                    <a href="#" class="deleteRoster" title="Delete" roster_id="{{$roster->id}}"><i class="fas fa-trash-alt fa-2x text-danger"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                @endif
                            </div>
                        </div>
                    </div>
                    @if(Auth::user()->employee_number == '21012' || Auth::user()->employee_number == '19745')
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="card bg-secondary mb-3" style="border-radius:0px;">
                            <div class="card-header">
                                <h4><b>Other Rosters</b></h4>
                            </div>
                            <div class="card-body">
                                
                                <table class="table table-dark table-hover table-striped" id="otherRoster">
                                    <thead>
                                        <th>Roster Name</th>
                                        <th>Created At</th>
                                        <th># of Attendees</th>
                                        <th>Load</th>
                                    </thead>
                                    <tbody>
                                        @foreach($others as $other)
                                            <tr>
                                                <td> {{ $other->roster_name }} </td>
                                                <td> {{ $other->created_at }} </td>
                                                <td>
                                                    @foreach ($attendants as $attendant)
                                                        @if($attendant->roster_id == $other->id)
                                                            {{ $attendant->total }}
                                                        @endif
                                                    @endforeach 
                                                </td>
                                                <td> <a href="/roster/load/{{$other->id}}" class="btn btn-sm btn-info btn-block" target="_blank">Load</a> </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
            <div class="card-footer">
                <a href="#" class="btn btn-primary btn-block" data-toggle="modal" data-target=".CreateRoster">Create New Roster</a>
            </div>
        </div>
    </div>

@include('roster.modals.AddRoster')
@include('roster.modals.update')

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">

    $(document).on("click", ".deleteRoster", function () {
        var roster_id = $(this).attr("roster_id");
        DeleteRoster(roster_id);
    });

function DeleteRoster(roster_id) {
    swal({
        title: "Delete Roster?",
        text: "Deleting this Roster cannot be undone.",
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "Yes, delete it!",
        confirmButtonColor: "#ec6c62"
    }, function() {
        $.ajax({
            url: "/roster/destroy/" + roster_id,
            type: "GET"
        })
            .done(function(data) {
                swal({
                        title: "Deleted",
                        text: "Roster Deleted",
                        type: "success",
                        timer: 1800,
                        showConfirmButton: false

                    },function() {
                        location.reload();
                    });
            })
            .error(function(data) {
                swal("Oops", "We couldn't connect to the server!", "error");
            });
    });
}

</script>

<script>

    $(document).on("click", ".edit-roster", function () {
        $('.roster-id').val($(this).attr("data-id"));
        $('.roster-name').val($(this).attr("data-name"));
        $('.roster-active').val($(this).attr("data-active"));
        $('.roster-visible').val($(this).attr("data-visible"));
        $('#editRoster').modal('show');
    });

</script>

<script type="application/javascript">
    $(document).ready(function() {
        $('#myRoster').DataTable( {
            "pageLength": 20,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>

<script type="application/javascript">
    $(document).ready(function() {
        $('#otherRoster').DataTable( {
            "pageLength": 20,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>

@endsection
