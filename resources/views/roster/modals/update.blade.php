<!-- Modal -->
<div class="modal fade" id="editRoster" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content text-white bg-dark">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Roster</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="/roster/update">
                {{ csrf_field() }}
                <input type="hidden" name="id" class="roster-id">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="roster_name" class="form-control roster-name" required>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-6">
                            <label>Share roster with other SGMC Users?</label>
                            <select name="visible" class="form-control roster-visible">
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>
                        </div>
                        <div class="form-group col-6">
                            <label>Is roster accepting new attendants?</label>
                            <select name="active" class="form-control roster-active">
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Update Roster">
                </div>
            </form>
        </div>
    </div>
</div>