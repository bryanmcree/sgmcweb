<div class="modal fade CreateRoster" id="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">
                
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-file-o" aria-hidden="true"></i> <b>Create Roster</b></h4>
                <button type="button" class="close btn" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

            </div>
            <div class="modal-body">
                {!! Form::open(array('action' => ['RosterController@createRoster'], 'class' => 'form_control')) !!}
                {!! Form::hidden('created_by', Auth::user()->id,['class'=>'id']) !!}
                <div class="form-group">
                    {!! Form::label('roster_name', 'Roster Name',['class'=>'ldac']) !!}
                    {!! Form::text('roster_name', null, ['class' => 'form-control roster_name', 'id'=>'roster_name', 'Required']) !!}
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            {!! Form::label('visible', 'Share roster with other SGMC Users?',['class'=>'ldac']) !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text bg-danger" id="basic-addon1"><i class="fa fa-exclamation-triangle"></i></span>
                                </div>
                                {!! Form::select('visible', [
                                                    'No' => 'No',
                                                    'Yes' => 'Yes',

                                                     ], null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            {!! Form::label('active', 'Is roster accepting new attendants?',['class'=>'ldac']) !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text bg-danger" id="basic-addon1"><i class="fa fa-exclamation-triangle"></i></span>
                                </div>
                                {!! Form::select('active', [
                                                    'Yes' => 'Yes',
                                                    'No' => 'No',
                                                     ], null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="RightLeft">
                    {!! Form::submit('Create Roster', ['class'=>'btn btn-primary', 'id'=>'AddButton']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>