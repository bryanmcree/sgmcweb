{{--New file Template--}}



    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <h2><b>SGMC Patient Census</b></h2>
            </div>
            <div class="panel-body">
                <div id="dashboard">
                    <div id="chart_div"></div>
                    <br><br>
                    <div align="center" id="control_div"></div>
                    <p><span id='dbgchart'></span></p>
                </div>
            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">

    var jsonData = {!! $results !!};
    //console.log(visitor);

    google.charts.load('44', {
        callback: drawChart,
        packages: ['controls', 'corechart']
    });


    function drawChart() {
        var data = new google.visualization.DataTable(jsonData);

        var control = new google.visualization.ControlWrapper({
            controlType: 'ChartRangeFilter',
            containerId: 'control_div',
            state: {
                range: {
                    start: new Date('{!! \Carbon\Carbon::now()->subDay(1)->format('m-d-Y') !!} '),
                    end:   new Date('{!! \Carbon\Carbon::now()->format('m-d-Y') !!}')
                }
            },
            options: {

                isStacked: true,
                filterColumnLabel: 'Date/Time',
                ui: {
                    chartOptions: {
                        height: 80,
                        chartArea: {
                            width: '60%'
                        }
                    },

                }
            }
        });

        var chart = new google.visualization.ChartWrapper({
            chartType: 'LineChart',
            containerId: 'chart_div',
            options: {
                height:590,
                chartArea: {
                    width:'90%',
                    height:'70%'
                },
                legend:'top',
                title: 'SGMC Census',
                hAxis: {

                    gridlines: {
                        count: -1,
                        units: {
                            days: {format: ['MMM dd']},
                            hours: {format: ['HH:mm', 'ha']},
                        }
                    },
                    minorGridlines: {
                        units: {
                            hours: {format: ['hh:mm:ss a', 'ha']},
                            minutes: {format: ['HH:mm a Z', ':mm']}
                        }
                    }
                },
                vAxis: {minValue: 0},



            }
        });

        var dash = new google.visualization.Dashboard(document.getElementById('dashboard'));
        dash.bind([control], [chart]);
        dash.draw(data);
    }




</script>

@endsection
