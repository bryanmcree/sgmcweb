@if ($extramile->isEmpty())

    <div class="alert alert-success"><b>There are no extra mile awards.</b></div>

@else

    <table class="table table-condensed table-hover text-white bg-dark" style="font-size: 12px ;">
        <thead>
        <tr>
            <td><b>Name</b></td>
            <td><b>Title</b></td>
            <td><b>Issued</b></td>
            <td><b>Presenter</b></td>
        </tr>
        </thead>
        <tbody>
        @foreach($extramile as $awards)
            <tr>
                <td><a href="/search?search={{$awards->receiverInfo->employee_number}}">{{$awards->receiverInfo->last_name}}, {{$awards->receiverInfo->first_name}}</a></td>
                <td>{{$awards->receiverInfo->title}}</td>
                <td>{{$awards->created_at->format('m-d-Y')}}</td>
                <td>{{$awards->presenterInfo->last_name}}, {{$awards->presenterInfo->first_name}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endif