@if ($terms->isEmpty())

    <div class="alert alert-success"><b>There are no pending terminations</b></div>

@else

<table class="table table-condensed table-hover">
    <thead>
    <tr>
        <td><b>Name</b></td>
        <td><b>Emp#</b></td>
        <td><b>Title</b></td>
        <td><b>RN</b></td>
        <td><b>DC</b></td>
        <td><b>Last Day</b></td>
        <td><b>Days</b></td>
        <td><b>Revoke On</b></td>
    </tr>
    </thead>
    <tbody>
    @foreach($terms as $term)
    <tr>
        <td>{{$term->last_name}}, {{$term->first_name}}</td>
        <td>{{$term->emp_number}}</td>
        <td>{{$term->title}}</td>
        <td>{{$term->RN}}</td>
        <td>{{$term->direct_care}}</td>
        <td>{{Carbon\Carbon::parse($term->last_day)->format('m/d/Y') }}</td>
        <td>{{\Carbon\Carbon::createFromTimeStamp(strtotime($term->last_day))->diffInDays()}}</td>
        <td>{{\Carbon\Carbon::createFromTimeStamp(strtotime($term->last_day))->addDays(1)->addHours(8)}}</td>
    </tr>
    @endforeach
    </tbody>
</table>

@endif