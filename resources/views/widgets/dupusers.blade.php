@if($dupes->isEmpty())
    <div class="">No duplicate users.</div>
@ELSE

<table class="table table-condensed table-hover" style="font-size: 12px ;">
    <thead>
    <tr>
        <td><b>Name</b></td>
        <td><b>Employee #</b></td>
        <td><b>ID</b></td>
        <td><b>Created</b></td>
        <td></td>
    </tr>
    </thead>
    <tbody>
    @foreach($dupes as $dupe)
    <tr class="bg-danger">
        <td>{{$dupe->name}}</td>
        <td>{{$dupe->employee_number}}</td>
        <td>{{$dupe->id}}</td>
        <td>{{$dupe->created_at}}</td>
        <td align="right"><a href="widgets/dupes/del/{{$dupe->id}}" class="btn btn-danger btn-xs">Remove</a> </td>
    </tr>
    @endforeach
    </tbody>
</table>
@ENDIF