@if ($census->isEmpty())

    <div class="alert alert-success"><b>Nothing to display.</b></div>

@else

    <table class="table table-condensed table-hover">
        <thead>
        <tr>
            <td><b>Location</b></td>
            <td align="center"><b>BIS</b></td>
            <td align="center"><b>I/P</b></td>
            <td align="center"><b>OBS</b></td>
            <td align="center"><b>NEPD</b></td>
            <td bgcolor="#ffffe0" align="center"><b>Percent</b></td>
            <td bgcolor="#d3d3d3" align="center"><b>ALOS I/P</b></td>
            <td bgcolor="#d3d3d3" align="center"><b>ALOS OBS</b></td>
            <td bgcolor="#d3d3d3" align="center"><b>ALOS TOT</b></td>

        </tr>
        </thead>
        <tbody>
        @foreach($census as $counts)

            <tr>
                <td @if($counts->census_location == 'SGMC Campus              ' OR $counts->census_location == 'SGMC Lanier Campus       ' OR $counts->census_location == 'SGMC Berrien Campus      ') bgcolor="#ffffe0" @endif>{{$counts->census_location}}</td>
                <td @if($counts->census_location == 'SGMC Campus              ' OR $counts->census_location == 'SGMC Lanier Campus       ' OR $counts->census_location == 'SGMC Berrien Campus      ') bgcolor="#ffffe0" @endif align="center">{{$counts->bis}}</td>
                <td @if($counts->census_location == 'SGMC Campus              ' OR $counts->census_location == 'SGMC Lanier Campus       ' OR $counts->census_location == 'SGMC Berrien Campus      ') bgcolor="#ffffe0" @endif align="center" >{{$counts->in_patient}}</div> </td>
                <td @if($counts->census_location == 'SGMC Campus              ' OR $counts->census_location == 'SGMC Lanier Campus       ' OR $counts->census_location == 'SGMC Berrien Campus      ') bgcolor="#ffffe0" @endif align="center">{{$counts->observation}}</td>
                <td @if($counts->census_location == 'SGMC Campus              ' OR $counts->census_location == 'SGMC Lanier Campus       ' OR $counts->census_location == 'SGMC Berrien Campus      ') bgcolor="#ffffe0" @endif align="center">{{$counts->in_patient + $counts->observation}}</td>
                <td bgcolor="#ffffe0" align="center">
                    <div @if($counts->bis != 0 )
                            @if( ROUND((($counts->in_patient + $counts->observation)/$counts->bis)*100) > 89) style="color:red;" @endif
                        @endif
                    >
                    @if($counts->bis == 0) 0%
                        @else {{ROUND((($counts->in_patient + $counts->observation)/$counts->bis)*100)}}% @endif
                    </div>
                </td>
                <td bgcolor="#d3d3d3" align="center">@if($counts->alos_ip < 0)0 @elseif ($counts->alos_ip >= 5) <div style="color:red;">{{$counts->alos_ip}}</div> @else {{$counts->alos_ip}} @endif</td>
                <td bgcolor="#d3d3d3" align="center">@if($counts->alos_obs < 0)0 @elseif ($counts->alos_obs >= 5) <div style="color:red;">{{$counts->alos_obs}}</div> @else {{$counts->alos_obs}} @endif</td>
                <td bgcolor="#d3d3d3" align="center">@if($counts->alos_tot < 0)0 @elseif ($counts->alos_tot >= 5) <div style="color:red;">{{$counts->alos_tot}}</div> @else {{$counts->alos_tot}} @endif</td>
            </tr>
        @endforeach
            <tr>
                <td colspan="9" bgcolor="#d3d3d3"><i>Updated {{\Carbon\Carbon::createFromTimeStamp(strtotime($censusUpdate->created_at))->diffInMinutes()}} Minutes Ago</i></td>
            </tr>
            <tr>
                <td colspan="9">** TOTAL ALOS EXCLUDES ALL NEWBORNS, NURSERY LEVEL I-IV, L&D, 2E AND REHAB FOR SGMC, GERIPSYCH & SWING FOR BERRIEN AND SWING FOR LANIER<br>
                    ** NURSERY PATIENTS ARE NOT INCLUDED IN TOTALS.</td>
            </tr>
        </tbody>
    </table>

@endif