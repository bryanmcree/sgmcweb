<div class="row">
    <div class="col-sm">
        <div class="card border-primary">
            <div style="text-align: center;">Employees</div>
            <div class="text-primary" style="height: 20px; text-align: center;">
                <a href="/employee/chart"> {{$totalemployees}} </a>
            </div>
        </div>
    </div>
    <div class="col-sm">
        <div class="card border-primary">
            <div style="text-align: center;">Full Time</div>
            <div class="text-primary" style="height: 20px; text-align: center;">
                {{$totalFT}}  ({{ROUND(($totalFT/$totalemployees)*100)}}%)
            </div>
        </div>
    </div>
    <div class="col-sm">
        <div class="card border-primary">
            <div style="text-align: center;">Salary</div>
            <div class="text-primary" style="height: 20px; text-align: center;">
                {{$totalSal}} ({{ROUND(($totalSal/$totalemployees)*100)}}%)
            </div>
        </div>
    </div>
    <div class="col-sm">
        <div class="card border-primary">
            <div style="text-align: center;">Hourly</div>
            <div class="text-primary" style="height: 20px; text-align: center;">
                {{$totalHr}} ({{ROUND(($totalHr/$totalemployees)*100)}}%)
            </div>
        </div>
    </div>
    <div class="col-sm">
        <div class="card border-primary">
            <div style="text-align: center;">PRN</div>
            <div class="text-primary" style="height: 20px; text-align: center;">
                {{$totalPRN}} ({{ROUND(($totalPRN/$totalemployees)*100)}}%)
            </div>
        </div>
    </div>
    <div class="col-sm">
        <div class="card border-primary">
            <div style="text-align: center;">Male</div>
            <div class="text-primary" style="height: 20px; text-align: center;">
                {{$totalmale}} ({{ROUND(($totalmale/$totalemployees)*100)}}%)
            </div>
        </div>
    </div>
    <div class="col-sm">
        <div class="card border-primary">
            <div style="text-align: center;">Female</div>
            <div class="text-primary" style="height: 20px; text-align: center;">
                {{$totalfemale}} ({{ROUND(($totalfemale/$totalemployees)*100)}}%)
            </div>
        </div>
    </div>
    <div class="col-sm">
        <div class="card border-primary">
            <div style="text-align: center;">Main</div>
            <div class="text-primary" style="height: 20px; text-align: center;">
                {{$campus_SGMC}}
            </div>
        </div>
    </div>
    <div class="col-sm">
        <div class="card border-primary">
            <div style="text-align: center;">Berrien</div>
            <div class="text-primary" style="height: 20px; text-align: center;">
                {{$campus_BERRIEN}}
            </div>
        </div>
    </div>
    <div class="col-sm">
        <div class="card border-primary">
            <div style="text-align: center;">Lanier</div>
            <div class="text-primary" style="height: 20px; text-align: center;">
                {{$campus_LANIER}}
            </div>
        </div>
    </div>
    <div class="col-sm">
        <div class="card border-primary">
            <div style="text-align: center;">Total RN's</div>
            <div class="text-primary" style="height: 20px; text-align: center;">
                {{$totalRN}}
            </div>
        </div>
    </div>
    <div class="col-sm">
        <div class="card border-primary">
            <div style="text-align: center;">DC</div>
            <div class="text-primary" style="height: 20px; text-align: center;">
                {{$totalDC}}
            </div>
        </div>
    </div>
</div>

