{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Meal Charge') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <form method="post" action="/meal_charge/add">
        {{ csrf_field() }}
        <input type="hidden" value="{{ Auth::user()->employee_number }}" name="created_by">
        <input type="hidden" name="emp_number" id="project_manager" required>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel panel-heading">
                    <h2><b>Meal Charge Exclusions</b></h2>
                </div>
                <div class="panel-body">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="required_id">Employee</label>
                            <small id="emailHelp" class="form-text text-muted">This employee will be excluded from badge swipes in the spice or cafeteria.</small>
                            <input type="text" id="autocomplete" class="form-control" placeholder="Search by employee name" minlength="8" required>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="question_type_id">Classification:</label>
                            <small id="emailHelp" class="form-text text-muted">Select the classification for this employee.</small>
                            <select class="form-control" id="question_type_id" name="emp_class" required>
                                <option value="" selected>[Select Classification]</option>
                                <option value="Administration">Administration</option>
                                <option value="Physician">Physician</option>
                                <option value="Requested Exclusion">Requested Exclusion</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <input type="Submit" class="btn btn-sm btn-sgmc" value="Add Employee">
                    </div>
                </div>
            </div>
        </div>
    </form>

    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <h2><b>Current Exclusions</b></h2>
            </div>
            <div class="panel-body">

            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">
    //employee search script
    $('#autocomplete').autocomplete({
        serviceUrl: '/meal_charge/employeesearch',
        dataType: 'json',
        type:'GET',
        width: 418,
        minChars:2,
        onSelect: function(suggestion) {
            //alert(suggestion.data);
            $("#project_manager").val(suggestion.data);
        }
    });
</script>

@endsection
@endif