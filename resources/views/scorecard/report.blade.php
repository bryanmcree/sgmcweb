{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Scorecard Admin') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <b>KPI Reports</b>
            </div>
            <div class="panel-body">
                <table class="table table-hover">
                    <thead>
                        <td>Report</td>
                        <td>Description</td>
                        <td></td>
                    </thead>
                    <tr>
                        <td>Custom KPI's</td>
                        <td>List of all KPI's by cost center, ordered by cost center in printable format.</td>
                        <td align="right"><a href="/scorecard/report/report1" target="_blank" class="btn btn-info btn-sm" ><i class="fa fa-print" aria-hidden="true"></i> Run Report</a></td>
                    </tr>
                    <tr>
                        <td>KPI Values</td>
                        <td>KPI values for six months for all cost centers, ordered by cost center in printable format.</td>
                        <td align="right"><a href="/scorecard/report/report2" target="_blank" class="btn btn-info btn-sm" ><i class="fa fa-print" aria-hidden="true"></i> Run Report</a></td>
                    </tr>
                    <tr>
                        <td>Cost Centers without KPI's</td>
                        <td>List of cost centers that do not have any KPI's entered.</td>
                        <td align="right"><a href="/scorecard/report/report6" target="_blank" class="btn btn-info btn-sm" ><i class="fa fa-print" aria-hidden="true"></i> Run Report</a></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
@endif