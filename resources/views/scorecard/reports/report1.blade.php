@extends('layouts.sgmc_nonav')

@section('content')
    @foreach($cost_centers as $cost_center)
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <b>{{$cost_center->cost_center}} - {{$cost_center->unit_description}} </b>
            </div>
            <div class="panel-body">
                @if($cost_center->customQuestions->isEmpty())
                    <div class="alert alert-info"><b>There are no custom KPI's for this cost center.</b></div>
                @else
                <table class="table">
                    <thead>
                        <tr>
                            <td></td>
                            <td><div style="font-size: 10px;"><b>Metric</b></div></td>
                            <td><div style="font-size: 10px;"><b>Definition</b></div></td>
                            <td><div style="font-size: 10px;"><b>Unit</b></div></td>
                            <td><div style="font-size: 10px;"><b>Source</b></div></td>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $rowcount = 0 ?>
                        @foreach($cost_center->customQuestions as $question)
                            <?php $rowcount = $rowcount + 1?>
                        <tr>
                            <td width="3%"><div style="font-size: 10px;"><b>{{$rowcount}})</b></div></td>
                            <td><div style="font-size: 10px;">{{$question->metric}}</div></td>
                            <td><div style="font-size: 10px;">{{$question->definition}}</div></td>
                            <td><div style="font-size: 10px;">{{$question->unit}}</div></td>
                            <td><div style="font-size: 10px;">{{$question->source}}</div></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @endif
            </div>
        </div>
        <div class="divFooter">KPI's by Cost Center - Printed on {{\Carbon\Carbon::now()->format('d/m/Y H:i')}}</div>
    </div>
    @endforeach
@endsection

