@extends('layouts.sgmc_nonav')

@section('content')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    <script   src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"   integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="   crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.js"></script>

    <div class="col-md-12">
        <h1><b>{{$cc->cost_center}} - {{$cc->unit_description}} </b></h1>
        <p><b>Type: </b>{{$cc->type}}</p>
        <p><b>Director / Manager: </b>{{$cc->director}}</p>


                <?php $rowcount = 0 ?>
                @foreach($cost_centers as $question)
                <?php $rowcount = $rowcount + 1?>



                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <b>{{$question->metric}}</b>
                            <div class="panel-sub-title">
                                <i>{{$question->definition}}</i>
                            </div>
                        </div>
                        <div class="panel-body">
                            <canvas id="kpichart_{{$rowcount}}" class="chartprint" width="100%" height="20"></canvas>

                            <div id="field_{{$rowcount}}" data-field-id="/scorecard/kpi/chart/json/{{$question->id}}" ></div>

                            <script type="text/javascript">


                                $(function(){
                                    var id = $('#field_{{$rowcount}}').data("field-id");
                                    var new_lable = "{{$question->unit}}";
                                    //alert(id);
                                    $.ajax({
                                        type: 'GET',
                                        url: id,
                                        //data: '/'+id,
                                        dataType: 'json',
                                        success: function (result) {

                                            var labels = [],data=[];
                                            for (var i = 0; i < result.length; i++) {
                                                labels.push(result[i].report_date);
                                                data.push(result[i].report_value);
                                            }

                                            var buyerData = {
                                                labels : labels,
                                                datasets : [
                                                    {
                                                        backgroundColor: ['rgba(8,69,248,0.4)'],
                                                        borderColor: "black",
                                                        borderWidth:1,
                                                        pointBackgroundColor: "black",
                                                        label: new_lable,
                                                        data : data
                                                    }
                                                ],

                                            };
                                            var buyers = document.getElementById('kpichart_{{$rowcount}}').getContext('2d');
                                            var chartInstance_{{$rowcount}} = new Chart(buyers, {
                                                type: 'line',
                                                data: buyerData,
                                                showTooltips: false,
                                                plugins: [{
                                                    afterDatasetsDraw: function(chart) {
                                                        var ctx = chart.ctx;
                                                        chart.data.datasets.forEach(function(dataset, index) {
                                                            var datasetMeta = chart.getDatasetMeta(index);
                                                            if (datasetMeta.hidden) return;
                                                            datasetMeta.data.forEach(function(point, index) {
                                                                var value = dataset.data[index],
                                                                    x = point.getCenterPoint().x,
                                                                    y = point.getCenterPoint().y,
                                                                    radius = point._model.radius,
                                                                    fontSize = 14,
                                                                    fontFamily = 'Verdana',
                                                                    fontColor = 'black',
                                                                    fontStyle = 'normal';
                                                                ctx.save();
                                                                ctx.textBaseline = 'middle';
                                                                ctx.textAlign = 'center';
                                                                ctx.font = fontStyle + ' ' + fontSize + 'px' + ' ' + fontFamily;
                                                                ctx.fillStyle = fontColor;
                                                                ctx.fillText(value, x, y - radius - fontSize);
                                                                ctx.restore();
                                                            });
                                                        });
                                                    }
                                                }]
                                            });
                                        }
                                    });
                                });

                            </script>
                        </div>
                    </div>
                </div>
                @endforeach

        <div class="divFooter">KPI's Cost Center - Printed on {{\Carbon\Carbon::now()->format('d/m/Y H:i')}}</div>
    </div>

@endsection

