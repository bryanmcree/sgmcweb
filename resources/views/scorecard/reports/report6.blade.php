@extends('layouts.sgmc_nonav')

@section('content')

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <b>Cost centers without KPI's </b> - ({{$cost_centers->count()}})
            </div>
            <div class="panel-body">
                @if($cost_centers->isEmpty())
                    <div class="alert alert-info"><b>There are no custom KPI's for this cost center.</b></div>
                @else
                    <table class="table table-bordered table-condensed">

                        <thead>

                        <tr>
                            <td><B><div style="font-size: 10px;">Cost Center</div></B></td>
                            <td><B><div style="font-size: 10px;">Description</div></B></td>
                            <td><b><div style="font-size: 10px;">Director</div></b></td>
                            <td><b><div style="font-size: 10px;">Administrator</div></b></td>
                        </tr>

                        </thead>
                        <tbody>
                        @foreach($cost_centers as $cc)
                            <tr>
                                <td><div style="font-size: 10px;">{{$cc->cost_center}}</div></td>
                                <td><div style="font-size: 10px;">{{$cc->unit_description}}</div></td>
                                <td><div style="font-size: 10px;">{{$cc->director}}</div></td>
                                <td><div style="font-size: 10px;">{{$cc->administrator}}</div></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
        <div class="divFooter">KPI's Cost Center - Printed on {{\Carbon\Carbon::now()->format('d/m/Y H:i')}}</div>
    </div>

@endsection

