@extends('layouts.sgmc_nonav')

@section('content')

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>{{$cc->cost_center}} - {{$cc->unit_code_description}} </b>
                </div>
                <div class="panel-body">
                    @if($cost_centers->isEmpty())
                        <div class="alert alert-info"><b>There are no custom KPI's for this cost center.</b></div>
                    @else
                        <p>Print off this page and fill in the month below month and add values for each month then key in the data at web.sgmc.org</p>
                        <table class="table table-bordered">

                            <thead>

                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td align="center"><b>MONTH</b></td>
                                <td align="center"><b>MONTH</b></td>
                                <td align="center"><b>MONTH</b></td>

                            </tr>
                            <tr>
                                <td></td>
                                <td><div style="font-size: 10px;"><b>Metric</b></div></td>
                                <td><div style="font-size: 10px;"><b>Unit</b></div></td>
                                <td><div style="font-size: 10px;"><b></b></div></td>
                                <td><div style="font-size: 10px;"><b></b></div></td>
                                <td><div style="font-size: 10px;"><b></b></div></td>

                            </tr>
                            </thead>
                            <tbody>
                            <?php $rowcount = 0 ?>
                            @foreach($cost_centers as $question)
                                <?php $rowcount = $rowcount + 1?>
                                <tr>
                                    <td width="3%"><div style="font-size: 10px;"><b>{{$rowcount}})</b></div></td>
                                    <td><div style="font-size: 10px;">{{$question->metric}}</div></td>
                                    <td><div style="font-size: 10px;">{{$question->unit}}</div></td>
                                    <td><div style="font-size: 10px;">Value:</div></td>
                                    <td><div style="font-size: 10px;">Value:</div></td>
                                    <td><div style="font-size: 10px;">Value:</div></td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
            <div class="divFooter">KPI's Cost Center - Printed on {{\Carbon\Carbon::now()->format('d/m/Y H:i')}}</div>
        </div>

@endsection

