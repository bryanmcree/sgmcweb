@extends('layouts.sgmc_nonav')

@section('content')
    @foreach($kpi_cost_center as $cost_center)
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>{{$cost_center->cost_center}} -  {{$cost_center->costcenters->unit_code_description}}</b>
                </div>
                <div class="panel-body">
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <td></td>
                                <td><div style="font-size: 10px;"><b>Metric</b></div></td>
                                @foreach($kpi_values->where('cost_center',$cost_center->cost_center)->groupby('report_month','report_year') as $kpi_value)
                                    <td><div style="font-size: 10px;"><b>{{date("F", mktime(0, 0, 0, $kpi_value->first()->report_month, 1))}}</b></div></td>
                                @endforeach
                            </tr>
                        </thead>
                        <tbody>
                        <?php $rowcount = 0 ?>
                        @foreach($cost_center->customQuestions2 as $question)
                            <?php $rowcount = $rowcount + 1?>
                            <tr>
                                <td width="3%"><div style="font-size: 10px;"><b>{{$rowcount}})</b></div></td>
                                <td width="40%"><div style="font-size: 10px;"> {{$question->metric}}</div></td>

                                @foreach($kpi_values->where('question_id',$question->id) as $kpi_value)
                                    <td>

                                        <div style="font-size: 10px;">
                                            @if ($kpi_value->customQuestions->unit == 'Dollars')
                                                ${{number_format($kpi_value->report_value,2)}}
                                            @elseif($kpi_value->customQuestions->unit == 'Percent')
                                                {{$kpi_value->report_value}}%
                                            @else
                                                {{$kpi_value->report_value}}
                                            @endif
                                        </div>

                                    </td>
                                @endforeach

                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
            <div class="divFooter">KPI Values by Cost Center - Printed on {{\Carbon\Carbon::now()->format('d/m/Y H:i')}}</div>
        </div>
    @endforeach
@endsection

