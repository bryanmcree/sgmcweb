{{--New file Template--}}

{{--Add Security for this page below--}}

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <a href="/scorecard/dashboard" class="btn btn-primary btn-xs" >Return to Dashboard</a>    <b>{{$costcenterQuestions->cost_center}} - {{$costcenterQuestions->unit_description}}</b>
            </div>
            <div class="panel-body">

                <div class="col-lg-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <b>Cost Center Type</b>
                        </div>
                        <div class="panel-body">
                            <i>{{$costcenterQuestions->type}}</i>
                        </div>
                        <div class="panel-footer">
                            @if ( Auth::user()->hasRole('Scorecard Admin')) <a href="/scorecard/dashboard" class="btn btn-primary btn-xs btn-block" >Change </a> @else <a href="/scorecard/dashboard" class="btn btn-primary btn-xs btn-block" disabled="" >Change </a> @endif
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="panel @if(is_null($costcenterQuestions->director)) panel-danger @else panel-default @endif">
                        <div class="panel-heading">
                            <b>Director / Manager</b>
                        </div>
                        <div class="panel-body">
                            @if(is_null($costcenterQuestions->director)) This is mine! Click "Change"  @else {{$costcenterQuestions->director}} @endif
                        </div>
                        <div class="panel-footer">
                            @if (!Auth::user()->hasRole('Scorecard Admin'))
                            <a href="/scorecard/dashboard/manager/{{$costcenterQuestions->cost_center}}" class="btn btn-primary btn-xs " > Change</a>
                            @else
                            <form method="post" action="/scorecard/dashboard/manager_admin">
                                {{ csrf_field() }}
                                <input type="hidden" name="director" id="director">
                                <input type="hidden" name="cost_center" value="{{$costcenterQuestions->cost_center}}">
                                <div class="input-group">
                                    <input type="search" id="autocomplete" class="form-control input-xs" placeholder="Search">
                                    <div class="input-group-btn">
                                        <button class="btn btn-default btn-xs" type="submit">
                                            Assign
                                        </button>
                                    </div>
                                </div>
                            </form>

                            @endif
                        </div>
                    </div>
                </div>
                @if (Auth::user()->hasRole('Scorecard Admin'))
                <div class="col-lg-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <b>Administrator</b>
                        </div>
                        <div class="panel-body">
                            @if(is_null($costcenterQuestions->administrator)) Not Assigned @else{{$costcenterQuestions->administrator}}@endif
                        </div>
                        <div class="panel-footer">
                            <form method="post" action="/scorecard/dashboard/administrator">
                                {{ csrf_field() }}
                                <input type="hidden" name="administrator" id="administrator">
                                <input type="hidden" name="cost_center" value="{{$costcenterQuestions->cost_center}}">
                                <div class="input-group">
                                    <input type="search" id="autocomplete2" class="form-control input-xs" placeholder="Search">
                                    <div class="input-group-btn">
                                        <button class="btn btn-default btn-xs" type="submit">
                                            Assign
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                @endif
                @if(!$myCC->isEmpty())
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <b>Switch between your cost centers</b>
                        </div>
                        <div class="panel-body">
                            @foreach ($myCC as $my)
                            <a href="/scorecard/dashboard/view/{{$my->cost_center}}" class="btn btn-primary btn-xs">{{$my->cost_center}}</a>
                            @endforeach
                        </div>
                        <div class="panel-footer">
                            <a href="#" disabled="" class="btn btn-primary btn-xs btn-block invisible" > Change</a>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>

    @if(!$comments_user->isEmpty())
    <div class="col-md-12">
        @foreach($comments_user as $reminder)
            <div class="panel panel-indo">
                <div class="panel-body bg-info" style="font-size: 20px ">
                    <div class="col-lg-11">
                        {{$reminder->comments}} - <i>{{$reminder->entered_by}}, {{$reminder->created_at->format('m-d-Y')}}</i>
                    </div>
                    <div class="col-lg-1">
                        <a href="/scorecard/dashboard/gotit/{{$reminder->id}}" class="btn btn-info btn-sm"><i class="fa fa-times-circle fa-2x" aria-hidden="true"></i> Got it!</a>
                    </div>
                </div>
            </div>

        @endforeach
    </div>
    @endif

    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-body">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#home">Custom Metrics</a></li>
                    <li><a data-toggle="tab" href="#menu1">Kpi Values</a></li>
                    @if (Auth::user()->hasRole('Scorecard Admin'))
                    <li><a data-toggle="tab" href="#menu2">Kpi Notes</a></li>
                    <li><a data-toggle="tab" href="#menu3">Manage Cost Centers</a></li>
                    <li><a href="/scorecard/admin/view" target="_blank">DGC View</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="/scorecard/report">Reports
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">KPI Notes Report</a></li>
                            <li><a href="/scorecard/report/report1" target="_blank">List of All CC Custom KPI</a></li>
                            <li><a href="/scorecard/report/report2" target="_blank">All CC KPI Values</a></li>
                            <li><a href="/scorecard/report/report6" target="_blank">All CC With NO Custom KPI</a></li>
                        </ul>
                    </li>
                    @endif
                </ul>
                <div class="tab-content">
                    <div id="home" class="tab-pane fade in active">
                        <br>
                        <div class="col-md-12">



                            <div class="panel panel-default">
                                <div class="panel panel-heading">
                                    <b>Custom Metrics</b>
                                </div>
                                <div class="panel-body">
                                    <p><a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".AddMetric">Add Metric</a></p>
                                    <p>Here you can add metrics that you would like to report on.</p>
                                    @if($custommetrics->isEmpty())

                                    @else
                                        <table class="table table-bordered table-condensed table-hover">
                                            <thead>
                                            <tr>
                                                <td><b></b></td>
                                                <td><b>Metric</b></td>
                                                <td><b>Definition</b></td>
                                                <td><b>Unit</b></td>
                                                <td><b>Source</b></td>
                                                <td></td>
                                            </tr>
                                            </thead>

                                            <?php $rowcount = 0 ?>
                                            @foreach($custommetrics as $custom)
                                                <?php $rowcount = $rowcount + 1?>
                                                <tr>
                                                    <td style="vertical-align : middle;"><b>{{$rowcount}})</b></td>
                                                    <td style="vertical-align : middle;">{{$custom->metric}}</td>
                                                    <td style="vertical-align : middle;">{{$custom->definition}}</td>
                                                    <td style="vertical-align : middle;">{{$custom->unit}}</td>
                                                    <td style="vertical-align : middle;" >{{$custom->source}}</td>
                                                    <td style="vertical-align : middle;" nowrap="" align="right">
                                                        @if($custom->auto_generate == 'Yes')
                                                            Default Metric
                                                        @else
                                                            <a class="btn btn-primary btn-xs EditCustomMetric" href="#" data-toggle="modal"
                                                               data-id="{{$custom->id}}"
                                                               data-metric="{{$custom->metric}}"
                                                               data-definition="{{$custom->definition}}"
                                                               data-unit="{{$custom->unit}}"
                                                               data-source="{{$custom->source}}"

                                                            ><i class="fas fa-edit fa-lg"></i> Edit</a>
                                                            @if ( Auth::user()->hasRole('Scorecard Admin'))
                                                                <a class="btn btn-danger btn-xs DeleteCustomMetric" href="#"
                                                                   data-id="{{$custom->id}}"
                                                                   data-cost_center="{{$costcenterQuestions->cost_center}}"
                                                                ><i class="fas fa-trash-alt fa-lg"></i> Delete</a>
                                                            @endif
                                                        @endif

                                                    </td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="menu1" class="tab-pane fade">
                        <br>
                        <div class="col-md-12">
                            <div class="panel panel-warning">
                                <div class="panel panel-heading">
                                    <b>Monthly KPI Values</b>
                                </div>
                                <div class="panel-body">
                                    <p><a href="#" class="btn btn-success btn-sm" data-toggle="modal" data-target=".EnterValues">Enter Values</a>
                                        <a href="/scorecard/report/report3/{{$costcenterQuestions->cost_center}}" target="_blank" class="btn btn-primary btn-sm" ><i class="fa fa-print" aria-hidden="true"></i> Print KPI Worksheet</a>
                                        <a href="/scorecard/report/report5/{{$costcenterQuestions->cost_center}}" target="_blank" class="btn btn-primary btn-sm" ><i class="fa fa-print" aria-hidden="true"></i> Print Summary</a>
                                        <a href="/scorecard/report/report4/{{$costcenterQuestions->cost_center}}" target="_blank" class="btn btn-primary btn-sm" ><i class="fa fa-print" aria-hidden="true"></i> Print Charts</a>
                                    </p>

                                    <p> <i>Have all your values for your KPI's above ready to key in.  It is easier and less time consuming if you key in all your KPI's for the month at one time.  Use the "Print KPI Worksheet" button to collect your data.</i></p>
                                    <p><b>Months of Data (MOD)</b> - How many months of data are available for this KPI.</p>
                                    <p><b>Report Date</b> - The month and year the data represents.</p>
                                    <p><b>Date Entered</b> - The date values were entered into the system.</p>
                                    @if($kpi_values->isEmpty())

                                    @else

                                        @foreach($kpi_dates as $kpi_date)
                                            <div class="panel panel-info">
                                                <div class="panel-heading">
                                                    <b>{{date("F", mktime(0, 0, 0, $kpi_date->report_month, 1))}}, {{$kpi_date->report_year}}</b> <a class="btn btn-danger btn-xs DeleteMonthValues pull-right" href="#"
                                                                                                                                                     data-month="{{$kpi_date->report_month}}"
                                                                                                                                                     data-cost_center="{{$costcenterQuestions->cost_center}}"
                                                                                                                                                     data-year="{{$kpi_date->report_year}}"
                                                    ><i class="fa fa-trash-o fa-lg"></i> Delete all data for this month/year </a>
                                                </div>
                                                <div class="panel-body">
                                                    <table class="table table-bordered table-condensed table-hover">
                                                        <thead>
                                                        <tr>
                                                            <td><b></b></td>
                                                            <td width="40%"><b>Metric</b></td>
                                                            <td><b>Unit</b></td>
                                                            <td><b>Value</b></td>
                                                            <td align="center"><b>MOD</b></td>
                                                            <td><b>Report Date</b></td>
                                                            <td><b>Date Entered</b></td>
                                                            <td nowrap=""></td>
                                                        </tr>
                                                        </thead>

                                                        <?php $rowcount = 0 ?>
                                                        @foreach($kpi_values->where('report_month',$kpi_date->report_month)->where('report_year',$kpi_date->report_year) as $kpi_value)
                                                            <?php $rowcount = $rowcount + 1?>
                                                            <tr>
                                                                <td style="vertical-align : middle;"><b>{{$rowcount}})</b></td>
                                                                <td style="vertical-align : middle;" nowrap="">{{$kpi_value->customQuestions->metric}}</td>
                                                                <td style="vertical-align : middle;">{{$kpi_value->customQuestions->unit}}</td>
                                                                <td style="vertical-align : middle;">
                                                                    @if ($kpi_value->customQuestions->unit == 'Dollars')
                                                                        ${{number_format($kpi_value->report_value,2)}}
                                                                    @elseif($kpi_value->customQuestions->unit == 'Percent')
                                                                        {{$kpi_value->report_value}}%
                                                                    @else
                                                                        {{$kpi_value->report_value}}
                                                                    @endif
                                                                </td>
                                                                <td style="vertical-align : middle;" align="center">{{$kpi_question_total->has($kpi_value->question_id) ? count($kpi_question_total[$kpi_value->question_id]) : 0}}</td>
                                                                <td style="vertical-align : middle;">{{$kpi_value->report_date->format('m/Y')}}</td>
                                                                <td style="vertical-align : middle;">{{$kpi_value->created_at->format('m/d/Y')}}</td>
                                                                <td style="vertical-align : middle;" nowrap="" align="right">
                                                                    <div class="btn-group">
                                                                        <button type="button" class="btn btn-default btn-xs" alt="Comments"><i class="fas fa-comments fa-2x"></i></button>

                                                                        @if(!empty($kpi_value->auto_generate))

                                                                            @if(count($kpi_question_total[$kpi_value->question_id]) > 1)
                                                                                <a href="/scorecard/kpi/chart/{{$kpi_value->question_id}}" target="_blank" type="button" class="btn btn-default btn-xs" alt="Comments"><i class="fas fa-chart-line fa-2x" aria-hidden="true"></i></a>
                                                                            @endif

                                                                        @else
                                                                            @if(count($kpi_question_total[$kpi_value->question_id]) > 1)
                                                                                <a href="/scorecard/kpi/chart/{{$kpi_value->question_id}}" target="_blank" type="button" class="btn btn-default btn-xs"><i class="fas fa-chart-line fa-2x" aria-hidden="true"></i></a>
                                                                            @endif
                                                                            <button type="button" class="btn btn-default btn-xs EditMetricValue" data-id="{{$kpi_value->id}}"
                                                                                    data-report_value="{{$kpi_value->report_value}}"
                                                                                    data-report_date="{{$kpi_value->report_date->format('Y-m-d')}}"><i class="fas fa-edit fa-2x"></i></button>
                                                                            @if ( Auth::user()->hasRole('Scorecard Admin'))
                                                                                <button type="button" class="btn btn-danger btn-xs DeleteMetricValue" data-id="{{$kpi_value->id}}"
                                                                                        data-cost_center="{{$costcenterQuestions->cost_center}}"><i class="fas fa-trash-alt fa-2x "></i></button>
                                                                            @endif



                                                                        @endif





                                                                    </div>


                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </table>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    @if (Auth::user()->hasRole('Scorecard Admin'))
                    <div id="menu2" class="tab-pane fade">
                        <br>
                        <p><a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".AddComment">Add Comment</a></p>
                        @if($comments->isEmpty())
                            <div class="alert alert-info"><h2>No comments for this cost center.</h2></div>
                        @else

                            <table class="table table-bordered table-condensed table-hover">
                                <thead>
                                <tr>
                                    <td><b>Entered By</b></td>
                                    <td><b>Date</b></td>
                                    <td><b>Comment</b></td>
                                    <td><b>Show User?</b></td>
                                    <td><b>Confirmed?</b></td>
                                    <td><b>Reminder?</b></td>
                                    <td></td>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($comments as $comment)
                                <tr>
                                    <td nowrap="">{{$comment->entered_by}}</td>
                                    <td nowrap="">{{$comment->created_at}}</td>
                                    <td>{{$comment->comments}}</td>
                                    <td>{{$comment->show_user}}</td>
                                    <td nowrap="">{{$comment->confirmed_by}}</td>
                                    <td>{{$comment->reminder_set}}</td>
                                    <td>buttons</td>
                                </tr>
                                @endforeach
                                </tbody>

                            </table>
                        @endif
                    </div>
                        <div id="menu3" class="tab-pane fade">
                            <br>
                            <div class="col-md-12">
                                <p><a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".AddComment">Add Comment</a></p>
                                <table class="table" id="example">
                                    <thead>
                                    <tr>
                                        <td><b>Cost Center</b></td>
                                        <td><b>Description</b></td>
                                        <td><b>Director</b></td>
                                        <td><b>Administrator</b></td>
                                        <td></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($cost_centers as $costcenter)
                                        <tr>
                                            <td @if($costcenter->deactivated == 'Yes') bgcolor="#ffb6c1" @endif>{{$costcenter->cost_center}}</td>
                                            <td @if($costcenter->deactivated == 'Yes') bgcolor="#ffb6c1" @endif>{{$costcenter->unit_description}}</td>
                                            <td @if($costcenter->deactivated == 'Yes') bgcolor="#ffb6c1" @endif>{{$costcenter->director}}</td>
                                            <td @if($costcenter->deactivated == 'Yes') bgcolor="#ffb6c1" @endif>{{$costcenter->administrator}}</td>
                                            <td @if($costcenter->deactivated == 'Yes') bgcolor="#ffb6c1" @endif align="right"><a href="#" class="btn btn-primary btn-xs">Manage</a> </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>


                    @endif
                </div>
            </div>
        </div>
    </div>















    @include('scorecard.modals.addmetric')
    @include('scorecard.modals.editmetric')
    @include('scorecard.modals.values')
    @include('scorecard.modals.editvalue')
    @include('scorecard.modals.addcomment')

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">

    //DataTables for Manage Cost Center Tab
    $(document)
        .ready(function () {
            $('#example').dataTable({
                "autoWidth": false,
                "lengthChange": true,
                "pageLength": 10
            });
        });

    //The add comment modal this shows and hides fields

    $('#reminder_set_id').on('change', function(){
        if ( $(this).is(':checked') ) {
            $('#reminder').show();
        } else {
            $('#reminder').hide();
        }
    });



    $('#autocomplete').autocomplete({
        serviceUrl: '/scorecard/employeesearch',
        dataType: 'json',
        type:'GET',
        width: 418,
        minChars:2,
        onSelect: function(suggestion) {
            //alert(suggestion.data);
            $("#director").val(suggestion.data);
        }


    });

    $('#autocomplete2').autocomplete({
        serviceUrl: '/scorecard/employeesearch',
        dataType: 'json',
        type:'GET',
        width: 418,
        minChars:2,
        onSelect: function(suggestion) {
            //alert(suggestion.data);
            $("#administrator").val(suggestion.data);
        }


    });

    $(document).on("click", ".EditCustomMetric", function () {
        //alert($(this).attr("data-id"));

        $('.metric').val($(this).attr("data-metric"));
        $('.id').val($(this).attr("data-id"));
        $('.definition').val($(this).attr("data-definition"));
        $('.unit').val($(this).attr("data-unit"));
        $('.source').val($(this).attr("data-source"));
        $('#EditCustomMetric_modal').modal('show');
        //alert($(this).attr("data-first_name"));
    });

//Edit Metric Value
    $(document).on("click", ".EditMetricValue", function () {
       // alert($(this).attr("data-report_date"));

        $('.report_value').val($(this).attr("data-report_value"));
        $('.id').val($(this).attr("data-id"));
        $('.report_date').val($(this).attr("data-report_date"));
        $('#EditMetricValue_modal').modal('show');
        //alert($(this).attr("data-first_name"));
    });

//Delete Custom Metric
    $(document).on("click", ".DeleteCustomMetric", function () {
        //alert($(this).attr("data-cost_center"));
        var id = $(this).attr("data-id");
        var cost_center = $(this).attr("data-cost_center");
        deleteCustomMetric(id, cost_center);
    });

    function deleteCustomMetric(id, cost_center) {
        swal({
            title: "Delete Metric?",
            text: "Are you sure that you want to delete this metric?",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            confirmButtonText: "Yes, delete it!",
            confirmButtonColor: "#ec6c62"
        }, function() {
            $.ajax({
                url: "/scorecard/dashboard/deletemetric/" + id + "?cost_center=" + cost_center ,
                type: "GET"
            })
                .done(function(data) {
                    swal({
                            title: "Deleted",
                            text: "Metric Deleted",
                            type: "success",
                            timer: 1800,
                            showConfirmButton: false

                        }
                    );
                    setTimeout(function(){window.location.replace('/scorecard/dashboard/view/'+ cost_center)},1900);
                })
                .error(function(data) {
                    swal("Oops", "We couldn't connect to the server!", "error");
                });
        });
    }


    //Delete Metric Value
    $(document).on("click", ".DeleteMetricValue", function () {
        //alert($(this).attr("data-id"));
        var id = $(this).attr("data-id");
        var cost_center = $(this).attr("data-cost_center");
        DeleteMetricValue(id, cost_center);
    });

    function DeleteMetricValue(id, cost_center) {
        swal({
            title: "Delete Value?",
            text: "Are you sure that you want to delete this value?",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            confirmButtonText: "Yes, delete it!",
            confirmButtonColor: "#ec6c62"
        }, function() {
            $.ajax({
                url: "/scorecard/dashboard/deletevalue/" + id + "?cost_center=" + cost_center ,
                type: "GET"
            })
                .done(function(data) {
                    swal({
                            title: "Deleted",
                            text: "Metric Deleted",
                            type: "success",
                            timer: 1800,
                            showConfirmButton: false

                        }
                    );
                    setTimeout(function(){window.location.replace('/scorecard/dashboard/view/'+ cost_center)},1900);
                })
                .error(function(data) {
                    swal("Oops", "We couldn't connect to the server!", "error");
                });
        });
    }


    //Delete Month's values
    $(document).on("click", ".DeleteMonthValues", function () {
        //alert($(this).attr("data-cost_center"));
        var report_month = $(this).attr("data-month");
        var report_year = $(this).attr("data-year");
        var cost_center = $(this).attr("data-cost_center");
        DeleteMonthValues(report_month, report_year, cost_center);
    });

    function DeleteMonthValues(report_month, report_year, cost_center) {
        swal({
            title: "Delete Monthly Values?",
            text: "Are you sure that you want to delete all the values for this month?",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            confirmButtonText: "Yes, delete it!",
            confirmButtonColor: "#ec6c62"
        }, function() {
            $.ajax({
                url: "/scorecard/dashboard/deletemonthvalue/" + report_month + "?cost_center=" + cost_center + "&report_year=" + report_year ,
                type: "GET"
            })
                .done(function(data) {
                    swal({
                            title: "Deleted",
                            text: "Metric Deleted",
                            type: "success",
                            timer: 1800,
                            showConfirmButton: false

                        }
                    );
                    setTimeout(function(){window.location.replace('/scorecard/dashboard/view/'+ cost_center)},1900);
                })
                .error(function(data) {
                    swal("Oops", "We couldn't connect to the server!", "error");
                });
        });
    }
</script>

@endsection
