{{--New file Template--}}



    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-6 col-lg-offset-3">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <b>KPI Balanced Scorecard</b>
            </div>
            <div class="panel-body">
                <div class="col-lg-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <b>Work Life</b>
                        </div>
                        <div class="panel-body">
                            <table class="table">
                                <tr>
                                    <td>Non-RN Staff Retention Rate</td>
                                    <td align="right">@if(empty($monthly_kpi->non_rn_rate))<i class="fa fa-exclamation" aria-hidden="true"></i> @else{{$monthly_kpi->non_rn_rate}}%@endif</td>
                                </tr>
                                <tr>
                                    <td>Overtime</td>
                                    <td align="right">@if(empty($monthly_kpi->overtime))<i class="fa fa-exclamation" aria-hidden="true"></i> @else{{$monthly_kpi->overtime}}@endif</td>
                                </tr>
                                <tr>
                                    <td>Physican Retention Rate</td>
                                    <td align="right">@if(empty($monthly_kpi->doc_rates))<i class="fa fa-exclamation" aria-hidden="true"></i> @else{{$monthly_kpi->doc_rates}}@endif</td>
                                </tr>
                                <tr>
                                    <td>RN Retention Rate</td>
                                    <td align="right">@if(empty($monthly_kpi->rn_rates))<i class="fa fa-exclamation" aria-hidden="true"></i> @else{{$monthly_kpi->rn_rates}}%@endif</td>
                                </tr>
                                <tr>
                                    <td>Staff Satisfaction</td>
                                    <td align="right">@if(empty($monthly_kpi->staff_sat))<i class="fa fa-exclamation" aria-hidden="true"></i> @else{{$monthly_kpi->staff_sat}}@endif</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <b>Quality</b>
                        </div>
                        <div class="panel-body">
                            <table class="table">
                                <tr>
                                    <td>Average Length of Stay</td>
                                    <td align="right">@if(empty($monthly_kpi->avg_stay))<i class="fa fa-exclamation" aria-hidden="true"></i> @else{{$monthly_kpi->avg_stay}}@endif</td>
                                </tr>
                                <tr>
                                    <td>Bed Occupancy Rate</td>
                                    <td align="right">@if(empty($monthly_kpi->bed_rate))<i class="fa fa-exclamation" aria-hidden="true"></i> @else{{$monthly_kpi->bed_rate}}@endif</td>
                                </tr>
                                <tr>
                                    <td>Timely Chart Sign Off</td>
                                    <td align="right">@if(empty($monthly_kpi->charts))<i class="fa fa-exclamation" aria-hidden="true"></i> @else{{$monthly_kpi->charts}}@endif</td>
                                </tr>
                                <tr>
                                    <td>Hospital Incidents</td>
                                    <td align="right">@if(empty($monthly_kpi->incidents))<i class="fa fa-exclamation" aria-hidden="true"></i> @else{{$monthly_kpi->incidents}}@endif</td>
                                </tr>
                                <tr>
                                    <td>Quality of Care</td>
                                    <td align="right">@if(empty($monthly_kpi->quality_care))<i class="fa fa-exclamation" aria-hidden="true"></i> @else{{$monthly_kpi->quality_care}}@endif</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <b>Customer Service</b>
                        </div>
                        <div class="panel-body">
                            <table class="table">
                                <tr>
                                    <td>Time to Service</td>
                                    <td align="right">@if(empty($monthly_kpi->time_service))<i class="fa fa-exclamation" aria-hidden="true"></i> @else{{$monthly_kpi->time_service}}@endif</td>
                                </tr>
                                <tr>
                                    <td>Patient Satisfaction</td>
                                    <td align="right">@if(empty($monthly_kpi->pat_sat))<i class="fa fa-exclamation" aria-hidden="true"></i> @else{{$monthly_kpi->pat_sat}}@endif</td>
                                </tr>
                                <tr>
                                    <td>Physician Satisfaction</td>
                                    <td align="right">@if(empty($monthly_kpi->doc_sat))<i class="fa fa-exclamation" aria-hidden="true"></i> @else{{$monthly_kpi->doc_sat}}@endif</td>
                                </tr>
                                <tr>
                                    <td>Room Turnover Time</td>
                                    <td align="right">@if(empty($monthly_kpi->room_time))<i class="fa fa-exclamation" aria-hidden="true"></i> @else{{$monthly_kpi->room_time}}@endif</td>
                                </tr>
                                <tr>
                                    <td>Staff Per Adjusted Occupied Bed</td>
                                    <td align="right">@if(empty($monthly_kpi->staff_per_bed))<i class="fa fa-exclamation" aria-hidden="true"></i> @else{{$monthly_kpi->staff_per_bed}}@endif</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <b>Finance</b>
                        </div>
                        <div class="panel-body">
                            <table class="table">
                                <tr>
                                    <td>Discharges</td>
                                    <td align="right">@if(empty($monthly_kpi->discharges))<i class="fa fa-exclamation" aria-hidden="true"></i> @else{{$monthly_kpi->discharges}}@endif</td>
                                </tr>
                                <tr>
                                    <td>Labor Productivity</td>
                                    <td align="right">@if(empty($monthly_kpi->productivity))<i class="fa fa-exclamation" aria-hidden="true"></i> @else{{$monthly_kpi->productivity}}@endif</td>
                                </tr>
                                <tr>
                                    <td>Late Charges</td>
                                    <td align="right">@if(empty($monthly_kpi->late_charges))<i class="fa fa-exclamation" aria-hidden="true"></i> @else{{$monthly_kpi->late_charges}}@endif</td>
                                </tr>
                                <tr>
                                    <td>Supply Expense / Utilization Per UOM</td>
                                    <td align="right">@if(empty($monthly_kpi->supply_expense))<i class="fa fa-exclamation" aria-hidden="true"></i> @else{{$monthly_kpi->supply_expense}}@endif</td>
                                </tr>
                                <tr>
                                    <td>Vendor / Contract Spend Per CC</td>
                                    <td align="right">@if(empty($monthly_kpi->vender_spend))<i class="fa fa-exclamation" aria-hidden="true"></i> @else{{$monthly_kpi->vender_spend}}@endif</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection