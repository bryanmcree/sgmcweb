<div class="modal fade AddComment" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><b>Add Comment</b></h4>
            </div>
            <div class="modal-body bg-default">
                <form method="post" action="/scorecard/dashboard/addcomment">
                {{ csrf_field() }}
                    <input type="hidden" name="cost_center" value="{{$costcenterQuestions->cost_center}}">
                    <input type="hidden" name="entered_by" value="{{ Auth::user()->name }}">

                    <div class="form-group">
                        <label for="comments_id">Comments:</label>
                        <textarea class="form-control" id="comments_id" name="comments" rows="5" required></textarea>
                    </div>

                    <div class="form-group">
                        <label for="show_user_id">Display reminder to user?</label>
                        <input type="checkbox" name="show_user" class="form-control" id="show_user_id" value="Yes">
                    </div>

                    <div class="form-group">
                        <label for="reminder_set_id">Send you a reminder for this comment?</label>
                        <input type="checkbox" name="reminder_set" class="form-control" id="reminder_set_id" value="Yes">
                    </div>

                    <div id="reminder" style="display:none">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="reminder_date_id">Select a date?</label>
                                    <input type="date" class="form-control" id="reminder_date_id" name="reminder_date">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="reminder_time_id">Select a time?</label>
                                    <input type="time" class="form-control" id="reminder_date_id" value="13:00" name="reminder_time">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="meeting_time_id">Meeting Time in Minuets:</label>
                                    <input type="number" class="form-control" id="meeting_time_id" name="meeting_time" value="30">
                                </div>
                            </div>
                        </div>




                    </div>




            </div>
            <div class="modal-footer bg-default">
                <div class="RightLeft">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" Value="Add Comment" class="btn btn-sgmc">
                </div>

                </form>
            </div>
        </div>
    </div>
</div>