<div class="modal fade EnterValues" id="" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><b>Enter KPI Values</b></h4>
            </div>
            <div class="modal-body bg-default">
                @if($custommetrics2->isEmpty())
                    <div class="alert alert-info">You do not have any custom metrics entered.</div>
                @else
                    <form method="post" action="/scorecard/kpi">
                        {{ csrf_field() }}
                    <input type="hidden" name="cost_center" value="{{$costcenterQuestions->cost_center}}">

                    <div class="col-md-12">
                        <p><b><I>REMEMBER you are reporting on last months KPI's! </I></b>However
                        if you are providing historical data simply select a date in the month that reflects these values. Example, if you want to report for September, 2017 then select any date in
                        September, 2017.</p>
                    </div>
                    <div class="col-md-3 col-lg-offset-4">
                        <label for="report_date" class="control-label">Report Date:</label>
                        <input type="date" name="report_date" value="" class="form-control" required>
                    </div>
                    <table class="table table-condensed table-striped">
                        <thead>
                        <tr>
                            <td><b></b></td>
                            <td width="60%"><b>Metric</b></td>
                            <td><b>Unit</b></td>
                            <td width="15%"><b>Enter Value</b></td>
                        </tr>
                        </thead>

                        <?php $rowcount = 0 ?>
                        @foreach($custommetrics2 as $custom)
                            <?php $rowcount = $rowcount + 1?>
                        @if($custom->auto_generate == 'Yes')

                            @else
                        <input type="hidden" value="{{$custom->id}}" name="question_id">
                            <tr>
                                <td><b>{{$rowcount}})</b></td>
                                <td >{{$custom->metric}}</td>
                                <td>{{$custom->unit}}</td>
                                <td nowrap="" align="right">
                                    <input type="number"  name="response[{{$custom->id}}][answer]" class="form-control" step="0.01" placeholder="Value" required>
                                </td>
                            </tr>
                        @endif
                        @endforeach
                    </table>
                @endif
            </div>
            <div class="modal-footer bg-default">
                <div class="RightLeft">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    @if($custommetrics2->isEmpty())
                    <button type="submit" class="btn btn-primary" disabled="disabled">Save KPI</button>
                    @else
                    <button type="submit" class="btn btn-primary">Save KPI</button>
                    @endif
                </div>
            </form>
            </div>
        </div>
    </div>
</div>