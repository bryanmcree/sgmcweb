<div class="modal fade" id="EditMetricValue_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><b>Edit Metric Value</b></h4>
            </div>
            <div class="modal-body bg-default">
                <form method="post" action="/scorecard/dashboard/editmetricvalue" >
                    {{ csrf_field() }}
                <input type="hidden" name="id" class="id">

                <div class="form-group">
                    <label for="report_date" class="control-label report_date">Report Date:</label>
                    <input type="date" name="report_date" value="" class="form-control report_date" required>
                </div>

                <div class="form-group">
                    <label for="report_value" class="control-label report_date">Report Value:</label>
                    <input type="number"  name="report_value" class="form-control report_value" step="0.01" placeholder="Value" required>
                </div>


            </div>
            <div class="modal-footer bg-default">
                <div class="RightLeft">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update Value</button>
                </div>

                </form>
            </div>
        </div>
    </div>
</div>