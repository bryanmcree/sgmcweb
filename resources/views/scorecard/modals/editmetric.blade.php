<div class="modal fade" id="EditCustomMetric_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><b>Edit Metric</b></h4>
            </div>
            <div class="modal-body bg-default">
                {!! Form::open(array('action' => ['ScorecardController@editMetric'], 'class' => 'form_control')) !!}
                {!! Form::hidden('id', null,['class' => 'form-control id', 'id'=>'id'])!!}

                <div class="form-group">
                    {!! Form::label('metric', 'Metric:') !!}
                    {!! Form::textarea('metric', null, ['class' => 'form-control metric', 'id'=>'metric', 'rows' => 4]) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('definition', 'Definition of Metric:') !!}
                    {!! Form::textarea('definition', null, ['class' => 'form-control definition', 'id'=>'definition', 'rows' => 4]) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('unit', 'Metric Unit: (NOT VALUES, Units like each, dollars, percent)') !!}
                    <div class="input-group">
                        <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                        {!! Form::text('unit', null, ['class' => 'form-control unit', 'id'=>'unit']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('source', 'Source of Metric:') !!}
                    {!! Form::textarea('source', null, ['class' => 'form-control source', 'id'=>'source', 'rows' => 4]) !!}
                </div>
            </div>
            <div class="modal-footer bg-default">
                <div class="RightLeft">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    {!! Form::submit('Update Metric', ['class'=>'btn btn-sgmc', 'id'=>'updateButton']) !!}
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>