{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Scorecard') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')
    @include('navbars.kpiadmin')
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <b>Custom Metrics</b> <i>({{$metrics->count()}})</i>
            </div>
            <div class="panel-body" style="height:700px; overflow-y: auto;">
                <table class="table table-hover table-condensed">
                    <thead>
                    <tr>
                        <td><b>Cost Center</b></td>
                        <td><b>Metric</b></td>
                        <td></td>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($metrics as $metric)
                    <tr @if($metric->approved =='Yes') class="bg-success" @endif>
                        <td nowrap="">{{$metric->costcenters->style1}}</td>
                        <td>{{$metric->metric}}</td>
                        <td align="right"><a class="btn btn-default btn-sm" href="/scorecard/admin/view/{{$metric->id}}"><i class="fa fa-info-circle"></i> Detail</a></td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
@endif