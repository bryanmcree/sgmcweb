{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Scorecard') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')
    @include('navbars.kpiadmin')
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <b>Custom Metrics</b> <i>({{$metrics->count()}})</i>
            </div>
            <div class="panel-body" style="height:700px; overflow-y: auto;">
                <table class="table table-hover table-condensed">
                    <thead>
                    <tr>
                        <td><b>Cost Center</b></td>
                        <td><b>Metric</b></td>
                        <td></td>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($metrics as $metric)
                        <tr @if($metric->approved =='Yes') class="bg-success" @endif>
                            <td nowrap="">{{$metric->costcenters->style1}}</td>
                            <td>{{$metric->metric}}</td>
                            <td align="right"><a class="btn btn-default btn-sm" href="/scorecard/admin/view/{{$metric->id}}"><i class="fa fa-info-circle"></i> Detail</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <b>{{$metric_detail->costcenters->style1}}</b>
            </div>
            <div class="panel-body">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <b>Metric</b>
                        </div>
                        <div class="panel-body">
                            {{$metric_detail->metric}}
                        </div>
                    </div>
                </div>


                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <b>Metric Definition</b>
                        </div>
                        <div class="panel-body">
                            {{$metric_detail->definition}}
                        </div>
                    </div>
                </div>


                <div class="col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <b>Unit of Service</b>
                        </div>
                        <div class="panel-body">
                            {{$metric_detail->unit}}
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <b>Source</b>
                        </div>
                        <div class="panel-body">
                            {{$metric_detail->source}}
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <b>Goal</b>
                        </div>
                        <div class="panel-body">
                            {{$metric_detail->goal}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <form class="form-horizontal" method="POST" action="/scorecard/admin/approval">
            {{ csrf_field() }}
        <div class="panel panel-default">
            <div class="panel-heading">
                <b>Approval</b>
            </div>
            <div class="panel-body">

                    <input type="hidden" name="id" value="{{$metric_detail->id}}">
                    <div class="row">
                        <label for="approved" class="col-md-2 control-label">Approved?</label>
                        <div class="col-md-4">
                            <input id="approved" type="checkbox" class="form-control" name="approved" checked value="Yes">
                        </div>
                        <label for="division" class="col-md-2 control-label">Division</label>
                        <div class="col-md-4">
                            <input id="division" type="division" placeholder="EPIC, Finance, Manual" class="form-control" name="division">
                        </div>
                    </div>


                    <div class="row">
                        <label for="new_source" class="col-md-2 control-label">New Source</label>
                        <div class="col-md-10">
                            <input id="new_source" type="text" placeholder="New or Other location for data come from" class="form-control" name="new_source">
                        </div>
                    </div>

                    <div class="row">
                        <label for="comments" class="col-md-2 control-label">Comments</label>
                        <div class="col-md-10">
                            <textarea rows="4" cols="50" class="form-control"></textarea>
                        </div>
                    </div>
            </div>
            <div class="panel-footer">
                <input type="submit" value="Submit Approval" class="btn btn-primary">
            </div>
        </div>
        </form>
    </div>
@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
@endif