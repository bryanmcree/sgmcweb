{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Scorecard') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <b>{{$kpi_data->customQuestions->metric}}</b>
            </div>
            <div class="panel-body">
                <canvas id="kpichart" width="100%" height="20"></canvas>
            </div>
        </div>
    </div>
    <div id="field" data-field-id="/scorecard/kpi/chart/json/{{$kpi_data->question_id}}" ></div>
@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">


        $(function(){
            var id = $('#field').data("field-id");
            var new_lable = "{{$kpi_data->customQuestions->unit}}";
            //alert(id);
            $.ajax({
                type: 'GET',
                url: id,
                //data: '/'+id,
                dataType: 'json',
                success: function (result) {

                    var labels = [],data=[];
                    for (var i = 0; i < result.length; i++) {
                        labels.push(result[i].report_date);
                        data.push(result[i].report_value);
                    }

                    var buyerData = {
                        labels : labels,
                        datasets : [
                            {
                                backgroundColor: ['rgba(8,69,248,0.4)'],
                                borderColor: "black",
                                borderWidth:1,
                                pointBackgroundColor: "black",
                                label: new_lable,
                                data : data
                            }
                        ]
                    };
                    var buyers = document.getElementById('kpichart').getContext('2d');
                    var chartInstance = new Chart(buyers, {
                        type: 'line',
                        data: buyerData,
                        plugins: [{
                            afterDatasetsDraw: function(chart) {
                                var ctx = chart.ctx;
                                chart.data.datasets.forEach(function(dataset, index) {
                                    var datasetMeta = chart.getDatasetMeta(index);
                                    if (datasetMeta.hidden) return;
                                    datasetMeta.data.forEach(function(point, index) {
                                        var value = dataset.data[index],
                                            x = point.getCenterPoint().x,
                                            y = point.getCenterPoint().y,
                                            radius = point._model.radius,
                                            fontSize = 14,
                                            fontFamily = 'Verdana',
                                            fontColor = 'black',
                                            fontStyle = 'normal';
                                        ctx.save();
                                        ctx.textBaseline = 'middle';
                                        ctx.textAlign = 'center';
                                        ctx.font = fontStyle + ' ' + fontSize + 'px' + ' ' + fontFamily;
                                        ctx.fillStyle = fontColor;
                                        ctx.fillText(value, x, y - radius - fontSize);
                                        ctx.restore();
                                    });
                                });
                            }
                        }]
                    });
                }




            });
        });

    </script>

@endsection
@endif