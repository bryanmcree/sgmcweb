{{--New file Template--}}

{{--Add Security for this page below--}}

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')
    @if ( Auth::user()->hasRole('Scorecard'))

    @endif
    <style>
        html { margin-bottom: 65px }
    </style>

    @if(!$myCC->isEmpty())
    <div class="col-md-10 col-lg-offset-1">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <b>My Cost Centers</b>
            </div>
            <div class="panel-body">
                <table class="table table-condensed table-hover">
                    <thead>
                        <tr>
                            <td><b>Cost Center</b></td>
                            <td><b>Description</b></td>
                            <td><b>Director</b></td>
                            <td><b>Administrator</b></td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($myCC as $costcenter2)
                        <tr>
                            <td>{{$costcenter2->cost_center}}</td>
                            <td>{{$costcenter2->unit_description}}</td>
                            <td>{{$costcenter2->director}}</td>
                            <td>{{$costcenter2->administrator}}</td>
                            <td align="right"><a href="/scorecard/dashboard/view/{{$costcenter2->cost_center}}" class="btn btn-primary btn-xs">View</a> </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @endif


    <div class="col-md-10 col-lg-offset-1">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <b>Select Cost Center</b>
            </div>
            <div class="panel-body">
                <table class="table table-condensed" id="example">
                    <thead>
                        <tr>
                            <td><b>Cost Center</b></td>
                            <td><b>Description</b></td>
                            <td><b>Director</b></td>
                            <td><b>Administrator</b></td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($cost_centers as $costcenter)
                        <tr>
                            <td @if($costcenter->deactivated == 'Yes') bgcolor="#ffb6c1" @endif>{{$costcenter->cost_center}}</td>
                            <td @if($costcenter->deactivated == 'Yes') bgcolor="#ffb6c1" @endif>{{$costcenter->unit_description}}</td>
                            <td @if($costcenter->deactivated == 'Yes') bgcolor="#ffb6c1" @endif>{{$costcenter->director}}</td>
                            <td @if($costcenter->deactivated == 'Yes') bgcolor="#ffb6c1" @endif>{{$costcenter->administrator}}</td>
                            <td @if($costcenter->deactivated == 'Yes') bgcolor="#ffb6c1" @endif align="right"><a href="/scorecard/dashboard/view/{{$costcenter->cost_center}}" class="btn btn-primary btn-xs">View</a> </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>


@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="application/javascript">
        $(document).ready(function() {
            $('#example').DataTable();
        } );
    </script>


@endsection
