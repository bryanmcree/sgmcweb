<table border="1" width="99%" align="center">
    <tr>
        <td height="99%">
            <table width="99%" align="center">
                <tr>
                    <td colspan="2">
                        <br>
                        <div align="center">{{ Html::image('img/rsz_sgmc_master_logo.png', 'alt', array( 'width' => 400, 'height' => 129 )) }}</div>
                        <div align="center"><h1>Compliance Survey Certificate</h1></div>

                        <div align="center"><h3>Thank you for completing the compliance survey.  SGMC values your input and strives to ensure a safe and healthy workplace.</h3></div>

                        <div align="center"><h3>If you would like to be entered for a chance to win a prize either print this certificate and fill out the blanks below and drop in
                                internal mail to the Compliance Department box in the Main Campus mailroom <b><i>OR</i></b> <a href="mailto:amanda.nijem@sgmc.org?subject=Compliance Survey Certificate&amp;body=I have completed the survey!  Enter me for a chance to win!">CLICK HERE</a> to email your completion
                                certificate to automatically be entered. </h3></div>

                        <div align="center"><h1>REMEMBER! Your results are completely anonymous!</h1></div>

                        <div align="center"><h3>{{\Carbon\Carbon::now()}}</h3></div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><hr><br></td>
                </tr>
                <tr>
                    <td width="50%">Name:___________________________________________<BR><br><br> Email:___________________________________________<BR><br><br>  Phone Ext: _____________ <br><br>

                    <td align="right">{{ Html::image('img/certificateseal.png', 'alt', array( 'width' => 150, 'height' => 111 )) }}</td>
                </tr>
            </table>
        </td>
    </tr>


</table>