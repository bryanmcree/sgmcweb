{{--New file Template--}}



    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')
    <form method="post" action="/compliance/survey/complete">
        {{ csrf_field() }}

    <div class="col-md-12">
        <div class="panel panel-danger">
            <div class="panel panel-heading">
                <h2>Attention:</h2>
            </div>
            <div class="panel-body">
                <h3>At SGMC, we strive to create a culture of compliant and ethical behavior.
                    We encourage open communication of compliance concerns, with the option to remain anonymous. We value your honest feedback on the anonymous survey below.</h3>

                <h3>Please complete your survey and submit completion certificate (optional) with your name and contact information NO LATER THAN Sunday, July 29, 2018 at 11:59PM (23:59) for a chance to win a Corkcicle cup.</h3>

                <h3><b>Drawing for prizes will take place, Monday, July 30, 2018 at 8:00am</b></h3>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="panel panel-success">
            <div class="panel panel-heading">
                <h2>Questions</h2>
            </div>
            <div class="panel-body">

                <h3>Are you comfortable raising and addressing unethical behavior/compliance concerns?</h3>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="addressing" id="addressing1" value="1" required>
                    <label class="form-check-label" for="addressing1">
                        Yes
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="addressing" id="addressing2" value="0" placeholder=".form-control-lg">
                    <label class="form-check-label" for="addressing2">
                        No
                    </label>
                </div>
                <h3>Did you know that SGMC has an anonymous Compliance Hotline to report Compliance concerns?</h3>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="hotline" id="hotline1" value="1" required>
                    <label class="form-check-label" for="hotline1">
                        Yes
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="hotline" id="hotline2" value="0">
                    <label class="form-check-label" for="hotline2">
                        No
                    </label>
                </div>
                <h3>Did you know that SGMC has a Non-Retaliation Policy, which prohibits retaliation or discipline against any employee who reports misconduct?</h3>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="non_retaliation" id="non_retaliation1" value="1" required>
                    <label class="form-check-label" for="non_retaliation1">
                        Yes
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="non_retaliation" id="non_retaliation2" value="0" required>
                    <label class="form-check-label" for="non_retaliation2">
                        No
                    </label>
                </div>
                <h3>Have you felt pressure to compromise your standards in order to meet business objectives?</h3>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="pressure" id="pressure1" value="1" required>
                    <label class="form-check-label" for="pressure1">
                        Yes
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="pressure" id="pressure2" value="0">
                    <label class="form-check-label" for="pressure2">
                        No
                    </label>
                </div>
                <h3>How would you rate SGMC’s culture of compliance and ethical behavior?</h3>
                <select class="form-control" name="rate_culture" id="inlineFormCustomSelectPref" required>
                    <option value ="" selected>Choose...</option>
                    <option value="1">1 - Worst</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5 - Best</option>
                </select>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="panel panel-info">
            <div class="panel panel-heading">
                <h2>Voluntary Information</h2>
            </div>
            <div class="panel-body">
                <h3>If you would like us to contact you regarding your survey please provide us with your name and contact information.  If you would like to remain anonymous leave the field below blank.</h3>
                <div class="form-group">
                    <input type="text" id="" class="form-control mx-sm-3" name="voluntary_info" aria-describedby="">
                </div>
            </div>
            <div class="panel-footer">
                <input type="submit" value="Submit Survey">
            </div>
        </div>
    </div>
    </form>
@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
