{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Compliance Survey Admin') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <b>This months surveys</b>
            </div>
            @if($all_surveys->isEmpty())
                <div class="col-md-12">
                    <div class="alert alert-info">There are no items.</div>
                </div>

            @else

            <div class="panel-body">
                <div class="panel panel-default">
                    <div class="panel panel-heading">
                        <b>Are you comfortable raising and addressing unethical behavior/compliance concerns?</b>
                    </div>
                    <div class="panel-body">
                        <div class="col-lg-10">
                            <canvas id="addressing" height="30"></canvas>
                        </div>
                        <div class="col-lg-2">
                            <h1>{{round(($addressing_yes->yeses/($addressing_yes->yeses + $addressing_no->nos))*100,2)}}% Yes</h1>
                            <h1>{{round(($addressing_no->nos/($addressing_yes->yeses + $addressing_no->nos))*100,2)}}% No</h1>
                        </div>

                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel panel-heading">
                        <b>Did you know that SGMC has an anonymous Compliance Hotline to report Compliance concerns?</b>
                    </div>
                    <div class="panel-body">
                        <div class="col-lg-10">
                            <canvas id="hotline" height="30"></canvas>
                        </div>
                        <div class="col-lg-2">
                            <h1>{{round(($hotline_yes->yeses/($hotline_yes->yeses + $hotline_no->nos))*100,2)}}% Yes</h1>
                            <h1>{{round(($hotline_no->nos/($hotline_yes->yeses + $hotline_no->nos))*100,2)}}% No</h1>
                        </div>

                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel panel-heading">
                        <b>Did you know that SGMC has a Non-Retaliation Policy, which prohibits retaliation or discipline against any employee who reports misconduct?</b>
                    </div>
                    <div class="panel-body">
                        <div class="col-lg-10">
                            <canvas id="non_retaliation" height="30"></canvas>
                        </div>
                        <div class="col-lg-2">
                            <h1>{{round(($non_retaliation_yes->yeses/($non_retaliation_yes->yeses + $non_retaliation_no->nos))*100,2)}}% Yes</h1>
                            <h1>{{round(($non_retaliation_no->nos/($non_retaliation_yes->yeses + $non_retaliation_no->nos))*100,2)}}% No</h1>
                        </div>

                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel panel-heading">
                        <b>Have you felt pressure to compromise your standards in order to meet business objectives?</b>
                    </div>
                    <div class="panel-body">
                        <div class="col-lg-10">
                            <canvas id="pressure" height="30"></canvas>
                        </div>
                        <div class="col-lg-2">
                            <h1>{{round(($pressure_yes->yeses/($pressure_yes->yeses + $pressure_no->nos))*100,2)}}% Yes</h1>
                            <h1>{{round(($pressure_no->nos/($pressure_yes->yeses + $pressure_no->nos))*100,2)}}% No</h1>
                        </div>

                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel panel-heading">
                        <b>How would you rate SGMC’s culture of compliance and ethical behavior?</b>
                    </div>
                    <div class="panel-body">
                        <div class="col-lg-10">
                            <canvas id="rate_culture" height="30"></canvas>
                        </div>
                        <div class="col-lg-2">
                            <h1>{{round(($all_surveys->sum('rate_culture')/($all_surveys->count()*5))*100,2)}}%</h1>
                        </div>

                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel panel-heading">
                        <b>Survey Details</b>
                    </div>
                    <div class="panel-body" style="height:400px; overflow-y: auto;">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <td><b>Submitted At</b></td>
                                <td><b>IP Address</b></td>
                                <td><b>Voluntary Info</b></td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($all_surveys as $surveys)
                                <tr>

                                    <td>{{$surveys->created_at}}</td>
                                    <td>{{$surveys->ip_address}}</td>
                                    <td>{{$surveys->voluntary_info}}</td>
                                    <td align="right">[Details Button]</td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>








                @endif
            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="application/javascript">

        // Bar chart
        new Chart(document.getElementById("addressing"), {
            type: 'horizontalBar',
            data: {
                labels: ["Yes", "No"],
                datasets: [
                    {
                        label: "Responses",
                        backgroundColor: ["#3e95cd", "#8e5ea2"],
                        data: [
                            {{$addressing_yes->yeses}},
                            {{$addressing_no->nos}}
                            ]
                    }
                ]
            },
            options: {
                legend: { display: false },
                title: {
                    display: false,
                    text: 'Predicted world population (millions) in 2050'
                },
                scales: {
                    xAxes: [{
                        ticks: {
                            min: 1 // Edit the value according to what you need
                        }
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                }
            }
        });


        new Chart(document.getElementById("hotline"), {
            type: 'horizontalBar',
            data: {
                labels: ["Yes", "No"],
                datasets: [
                    {
                        label: "Responses",
                        backgroundColor: ["#3e95cd", "#8e5ea2"],
                        data: [
                            {{$hotline_yes->yeses}},
                            {{$hotline_no->nos}}
                        ]
                    }
                ]
            },
            options: {
                legend: { display: false },
                title: {
                    display: false,
                    text: 'Predicted world population (millions) in 2050'
                },
                scales: {
                    xAxes: [{
                        ticks: {
                            min: 1 // Edit the value according to what you need
                        }
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                }
            }
        });

        new Chart(document.getElementById("non_retaliation"), {
            type: 'horizontalBar',
            data: {
                labels: ["Yes", "No"],
                datasets: [
                    {
                        label: "Responses",
                        backgroundColor: ["#3e95cd", "#8e5ea2"],
                        data: [
                            {{$non_retaliation_yes->yeses}},
                            {{$non_retaliation_no->nos}}
                        ]
                    }
                ]
            },
            options: {
                legend: { display: false },
                title: {
                    display: false,
                    text: 'Predicted world population (millions) in 2050'
                },
                scales: {
                    xAxes: [{
                        ticks: {
                            min: 1 // Edit the value according to what you need
                        }
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                }
            }
        });

        new Chart(document.getElementById("pressure"), {
            type: 'horizontalBar',
            data: {
                labels: ["Yes", "No"],
                datasets: [
                    {
                        label: "Responses",
                        backgroundColor: ["#3e95cd", "#8e5ea2"],
                        data: [
                            {{$pressure_yes->yeses}},
                            {{$pressure_no->nos}}
                        ]
                    }
                ]
            },
            options: {
                legend: { display: false },
                title: {
                    display: false,
                    text: 'Predicted world population (millions) in 2050'
                },
                scales: {
                    xAxes: [{
                        ticks: {
                            min: 1 // Edit the value according to what you need
                        }
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                }
            }
        });

        new Chart(document.getElementById("rate_culture"), {
            type: 'horizontalBar',
            data: {
                labels: ["1", "2","3","4","5"],
                datasets: [
                    {
                        label: "Responses",
                        backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                        data: [
                            {{$rate_culture_1->one}},
                            {{$rate_culture_2->two}},
                            {{$rate_culture_3->three}},
                            {{$rate_culture_4->four}},
                            {{$rate_culture_5->five}},
                        ]
                    }
                ]
            },
            options: {
                legend: { display: false },
                title: {
                    display: false,
                    text: 'Predicted world population (millions) in 2050'
                },
                scales: {
                    xAxes: [{
                        ticks: {
                            min: 1 // Edit the value according to what you need
                        }
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                }
            }
        });

    </script>


@endsection
@endif