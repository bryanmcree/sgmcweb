{{--New file Template--}}

{{--Add Security for this page below--}}
    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

<div class="card bg-dark text-white mb-3">
    <div class="card-header">
        <h2><b>{{$eoc->cost_center}} - {{$eoc->eocCostCenter->unit_code_description}}</b></h2>
        <b>Manager: </b> <i>{{$eoc->managers}}</i>
    </div>
    <div class="card-body">

        <div class="card-deck mb-3">

            <div class="card bg-info mt-3 mb-3 text-white">
                <div class="card-header">
                    <b>Score</b>
                </div>
                <div class="card-body">
                    <canvas id="score" ></canvas>
                    <p align="center">@if($survey_answers->sum('yes') == 0) 0 @else {{number_format(($survey_answers->sum('yes')/($survey_answers->count()-$survey_answers->sum('na')))*100,2)}} @endif%</p>
                </div>
            </div>

            <div class="card bg-success mt-3 mb-3 text-white">
                <div class="card-header">
                    <b>YES</b>
                </div>
                <div class="card-body">
                    {{$survey_answers->sum('yes')}}
                </div>
            </div>

            <div class="card bg-danger mt-3 mb-3 text-white">
                <div class="card-header">
                    <b>NO</b>
                </div>
                <div class="card-body">
                    {{$survey_answers->sum('no')}}
                </div>
            </div>

            <div class="card bg-warning mt-3 mb-3">
                <div class="card-header">
                    <b>N/A</b>
                </div>
                <div class="card-body">
                    {{$survey_answers->sum('na')}}
                </div>
            </div>

            <div class="card bg-primary mt-3 mb-3">
                <div class="card-header">
                    <b>Corrected</b>
                </div>
                <div class="card-body">
                    {{$survey_answers->sum('corrected')}}
                </div>
            </div>

            <div class="card bg-primary mt-3 mb-3">
                <div class="card-header">
                    <b>Total Questions</b>
                </div>
                <div class="card-body">
                    {{$survey_answers->count()}}
                </div>
            </div>
        </div>

            <hr>

        <div class="card bg-secondary border-secondary text-white mb-3">
            <div class="card-header">
                Compliance % by Category
            </div>
            <div class="card-body bg-dark">

                <table class="table table-dark table-hover table-bordered">
                    <thead>
                        <th>Category</th>
                        <th>Compliance</th>
                    </thead>
                    <tbody>
                        @foreach($compliance as $comp)
                            <tr>
                                <td>{{ $comp->eocCategory->category_name }}</td>
                                <td>@if($comp->total == 0) No Data Recorded @else {{ number_format(($comp->yes / $comp->total) * 100, 2) }}% @endif</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="card bg-dark text-white border-danger mb-3">
            <div class="card-header bg-danger">
                <b>Negative Responses</b>
            </div>
            <div class="card-body">
                @if($survey_answers->where('no',1)->isEmpty())
                    <div class="alert alert-info">No negative responses.</div>
                @else
                <table class="table table-hover table-condensed">
                    <thead>
                        <tr>
                            <td><b>Question</b></td>
                            <td><b>Category</b></td>
                            <td><b>Notes</b></td>
                            <td class="text-warning"><b>Is Corrected?</b></td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($survey_answers->where('no',1) as $bad)
                        <tr>
                            <td>{{$bad->eocQuestion->question}}</td>
                            <td nowrap="">{{$bad->eocCategory->category_name}}</td>
                            <td>{{$bad->notes}}</td>
                            @if($bad->corrected == 1) 
                                <td class="bg-success">Yes</td> 
                            @else
                                <td class="bg-danger">No</td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @endif
            </div>
        </div>

    </div>
</div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="application/javascript">
        @if($survey_answers->count() >= 1)
        //Score Chart
        new Chart(document.getElementById("score"), {
            type: 'doughnut',

            data: {
//                labels: ["Africa", "Asia", "Europe", "Latin America", "North America"],
                datasets: [
                    {
                        // label: "Population (millions)",
                        backgroundColor: [


                                    @if((number_format(($survey_answers->sum('yes')/($survey_answers->count()-$survey_answers->sum('na')))*100,2)) >= 94)
                                "#29a322"
                            @elseif((number_format(($survey_answers->sum('yes')/($survey_answers->count()-$survey_answers->sum('na')))*100,2)) < 94  && (number_format(($survey_answers->sum('yes')/($survey_answers->count()-$survey_answers->sum('na')))*100,2)) >= 85)
                                "#E4D61A"
                            @elseif((number_format(($survey_answers->sum('yes')/($survey_answers->count()-$survey_answers->sum('na')))*100,2)) < 85  && (number_format(($survey_answers->sum('yes')/($survey_answers->count()-$survey_answers->sum('na')))*100,2)) >= 70)
                                "#E4721A"
                            @elseif((number_format(($survey_answers->sum('yes')/($survey_answers->count()-$survey_answers->sum('na')))*100,2)) < 70  && (number_format(($survey_answers->sum('yes')/($survey_answers->count()-$survey_answers->sum('na')))*100,2)) >= 1)
                                "#E41A28"
                            @endif




                            , "#dee0de"],
                        data: [{{number_format(($survey_answers->sum('yes')/($survey_answers->count()-$survey_answers->sum('na')))*100,2) }},
                            100-{{number_format(($survey_answers->sum('yes')/($survey_answers->count()-$survey_answers->sum('na')))*100,2)}} ],
                        borderWidth: 1
                    }
                ]
            },
            options: {
                rotation: 1 * Math.PI,
                circumference: 1 * Math.PI,
                title: {
                    display: false,
                    text: 'Predicted world population (millions) in 2050'
                },
                cutoutPercentage: 80,

                //responsive: false,
                //maintainAspectRatio: true,
            }

        });
        @endif
    </script>


@endsection