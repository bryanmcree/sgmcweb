@extends('layouts.app')

@section ('content')

<div class="card bg-dark text-white mb-3">
    <div class="card-body">

        <h5 class="text-center"><b>THIS AWARD SEEKS TO RECOGNIZE INDIVIDUALS THAT HAVE EXHIBITED LEADERSHIP AND IMPROVEMENTS IN WORKPLACE SAFETY.</b></h5>
        <br>
        <ul class="list-group mb-3">
            <li class="list-group-item bg-secondary">- PROMOTES A CULTURE OF SAFETY</li>
            <li class="list-group-item bg-secondary">- REPORTS OR STOPS A SAFETY INCIDENT THAT COULD HAVE BEEN CRITICAL TO THE FACILITY</li>
            <li class="list-group-item bg-secondary">- DEMONSTRATES A NEW OR REVISED SAFETY APPROACH THAT HAS NOT BEEN RECOGNIZED PREVIOUSLY</li>
            <li class="list-group-item bg-secondary">- PRESENTS A SAFETY IMPROVEMENT IDEA THAT CAN BE SHARED THROUGHOUT THE FACILITY</li>
        </ul>
                
        <hr>

        <form action="/eoc/safety/store" method="POST" class="mb-5">
            {{ csrf_field() }}
            <input type="hidden" name="submitted_by" value="{{ Auth::user()->employee_number }}">
            <input type="hidden" name="submitter_campus" value="{{ Auth::user()->location }}">
            <input type="hidden" name="submitter_department" value="{{ Auth::user()->unit_code_description }}">
            <input type="hidden" name="submitter_email" value="{{ Auth::user()->mail }}">
            <input type="hidden" name="submitter_phone" value="{{ Auth::user()->phone }}">

            <div class="form-row">
                <div class="form-group col-lg-3">
                    <label>Nominee Name</label>
                    <input type="text" name="nominee" class="form-control" placeholder="Name">
                </div>
                <div class="form-group col-lg-3">
                    <label>Nominee Position</label>
                    <input type="text" name="position" class="form-control" placeholder="Position">
                </div>
                <div class="form-group col-lg-3">
                    <label>Nominee Campus</label>
                    <input type="text" name="campus" class="form-control" placeholder="Campus">
                </div>
                <div class="form-group col-lg-3">
                    <label>Nominee Department</label>
                    <input type="text" name="department" class="form-control" placeholder="Department">
                </div>
            </div>

            <textarea name="reason" rows="5" class="form-control" placeholder="Reason for Nomination... PLEASE DESCRIBE THE INCIDENT OR SITUATION THAT CLEARLY DEMONSTARTES WHY THIS EMPLOYEE SHOULD BE AWARDED THE SAFETY SHINING STAR AWARD.  ONE OR MORE OF THE CRITERIA ABOVE SHOULD BE EXHIBITED BY THE EMPLOYEE."></textarea>

            <hr>

            <input type="submit" class="btn btn-primary btn-block">

        </form>

        @if(Auth::user()->hasRole('EOCsafety_admin') == TRUE)

            <div class="table-responsive">
                <table class="table table-dark table-striped table-bordered table-hover mt-4" id="eocsafety">
                    <thead>
                        <th nowrap>Nominee</th>
                        <th nowrap>Position</th>
                        <th nowrap>Campus</th>
                        <th nowrap>Department</th>
                        <th nowrap>Submitted by</th>
                        <th nowrap>Submitter Email</th>
                        <th nowrap>Created At</th>
                        <th>Reason for Nomination</th>
                    </thead>
                    <tbody>
                        @foreach ($nominations as $nom)
                            <tr>
                                <td nowrap>{{ $nom->nominee }}</td>
                                <td nowrap>{{ $nom->position }}</td>
                                <td nowrap>{{ $nom->campus }}</td>
                                <td nowrap>{{ $nom->department }}</td>
                                <td nowrap>{{ $nom->submittedBy->name }}</td>
                                <td nowrap> <a href="mailto:{{ $nom->submitter_email }}">{{ $nom->submitter_email }}</a> </td>
                                <td nowrap>{{ Carbon::parse($nom->created_at)->format('m-d-Y') }}</td>
                                <td>{{ $nom->reason }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

        @endif

    </div>
</div>


@endsection