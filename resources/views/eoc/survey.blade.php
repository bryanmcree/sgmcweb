{{--New file Template--}}

{{--Add Security for this page below--}}

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-lg-12">
        <div class="card bg-dark text-white mb-3">
            <div class="card-header">
                <b>{{$survey->cost_center}} / {{$survey->managers}} - {{$survey->created_at}}</b>
            </div>
            <div class="card-body">
                <a href="/eoc/" class="btn btn-primary btn-block btn-lg">
                    Back to Surveys
                </a><br>

                @foreach($categories as $category)

                    @if ($complete->contains('category_id', $category->id))
                        <a href="/eoc/survey/category/edit/{{$category->id}}?survey_id={{$survey->id}}" class="btn btn-success btn-block btn-lg">
                            {{$category->category_name}}
                        </a><br>

                        @else
                        <a href="/eoc/survey/category/{{$category->id}}?survey_id={{$survey->id}}" class="btn btn-danger btn-block btn-lg">
                            {{$category->category_name}}
                        </a><br>
                        @endif
                @endforeach
            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection