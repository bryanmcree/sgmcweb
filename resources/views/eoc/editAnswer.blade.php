@extends('layouts.app')

@section('content')
    
    <div class="card bg-dark text-white col-lg-6 mx-auto">
        <div class="card-header">
            Edit EOC Answer
            <span class="float-right"><a href="/eoc" class="btn btn-secondary btn-sm">Return to Dashboard</a></span>
        </div>
        <div class="card-body">

            <form action="/eoc/answer/update/{{ $answer->id }}" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <label>Answer</label>
                    <select name="answer_yna" class="form-control">
                        <option value="@if($answer->yes == 1) 1 @elseif($answer->no == 1) 0 @else 2 @endif" selected>@if($answer->yes == 1) Yes @elseif($answer->no == 1) No @else N/A @endif</option>
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                        <option value="2">N/A</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="corrected">Corrected? <br><small class="text-warning"><i>ignore if above answer is yes</i></small></label>
                    <select name="corrected" class="form-control">
                        <option value="{{ $answer->corrected }}" selected>@if($answer->corrected == 1) Yes @else No @endif</option>
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Notes</label>
                    <textarea name="notes" rows="4" class="form-control">{{ $answer->notes }}</textarea>
                </div>

                <hr>

                <input type="submit" class="btn btn-primary btn-block"> 

            </form>

        </div>
    </div>

@endsection