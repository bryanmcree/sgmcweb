{{--New file Template--}}

{{--Add Security for this page below--}}

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-lg-12">
        <div class="card bg-dark mb-3 text-white">
            <div class="card-header">
                <b>{{$cat->category_name}}</b>
            </div>
            <div class="card-body">

                <ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab">Questions</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="photo-tab" data-toggle="tab" href="#menu1" role="tab">Photos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="return-tab" href="/eoc/survey/{{$survey->id}}" role="tab">Back to Categories</a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div id="home" class="tab-pane fade show active" role="tabpanel">
                        {!! Form::open(array('action' => ['EOCController@submitAnswers'], 'class' => 'form_control')) !!}
                        <input type = "hidden" name="survey_id" value="{{$survey->id}}">
                        <input type = "hidden" name="cost_center" value="{{$survey->cost_center}}">
                        <input type = "hidden" name="completed_by" value="{{Auth::user()->employee_number}}">

                        <table class="table table-condensed">
                            <tbody>
                            <?php $list = -1?>
                            @foreach($questions as $question)
                                <?php $list = $list + 1?>
                                <input type="hidden" value="{{$question->id}}" name="response[{{$list}}][question_id]">
                                <input type="hidden" value="{{$survey->id}}" name="response[{{$list}}][survey_id]">
                                <input type="hidden" value="{{$cat->id}}" name="response[{{$list}}][category_id]">
                                <input type="hidden" value="0" name="response[{{$list}}][corrected]">


                                <tr>
                                    <td colspan="5" bgcolor="#b0c4de"><b><div style="text-align: justify; font-size: 20px;">{{$question->question}}</div> </b></td>
                                </tr>
                                <tr>
                                    <td align="center"><B>YES</B></td>
                                    <td align="center"><b>NO</b></td>
                                    <td align="center"><b>N/A</b></td>
                                    <td align="center"><b>Corrected</b></td>
                                    <td align="center"><b>Notes</b></td>
                                </tr>
                                <tr align="center">
                                    <td align="center"><input type="radio" class="form-control" name="response[{{$list}}][answer]" value="Yes"></td>
                                    <td align="center"><input type="radio" class="form-control" name="response[{{$list}}][answer]" value="No"></td>
                                    <td align="center"><input type="radio" class="form-control" name="response[{{$list}}][answer]" value="NA"></td>
                                    <td align="center"><input type="checkbox" class="form-control" name="response[{{$list}}][corrected]" Value="1"></td>
                                    <td align="center"><input type="text" class="form-control" name="response[{{$list}}][notes]"></td>
                                </tr>
                                <tr>
                                    <td colspan="5"><hr></td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="5"><button type="submit" class="btn btn-sgmc btn-lg btn-block">Save Answers</button></td>
                            </tr>
                            </tbody>
                        </table>
                        {!! Form::close() !!}
                    </div>
                    <div id="menu1" class="tab-pane fade">
                        {!! Form::open(array('url'=>'eoc/survey/documents/add','method'=>'POST', 'files'=>true)) !!}
                        {!! Form::hidden('survey_id', $survey->id,['class'=>'report_id']) !!}
                        {!! Form::hidden('category_id', $cat->id,['class'=>'report_id']) !!}
                        <div class="form-group">
                            {!! Form::label('file_description', 'Description:') !!}
                            <div class="input-group">
                                <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                {!! Form::text('file_description', null, ['class' => 'form-control', 'id'=>'first_name','placeholder'=>'File Description', 'required']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('file', 'File:') !!}
                            <div class="input-group">
                                <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                {!! Form::file('file_name', array('class'=>'form-control')) !!}
                            </div>
                        </div>
                        {!! Form::submit('Upload File', array('class' => 'btn form-control btn-block btn-primary')) !!}
                        {!! Form::close() !!}



                        @if ($docs->isEmpty())
                            <div class="alert bg-info">You do not have any files attached.</div>
                        @else
                            <table class="documents table table-condensed table-hover">
                                <thead>
                                <tr>
                                    <td><B>Thumbnail</B></td>
                                    <td><B>Date Added</B></td>
                                    <td><B>Added By</B></td>
                                    <td><B>File Type</B></td>
                                    <td><B>File Size</B></td>
                                    <td><B>Description</B></td>
                                    <td></td>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach ($docs as $documents)
                                    <tr>
                                        <td>{{asset('storage/'.$documents->new_filename) }}</td>
                                        <td>{{$documents->created_at}}</td>
                                        <td>{{$documents->addedBy->first_name}} {{$documents->addedBy->last_name}}</td>
                                        <td>{{$documents->file_extension}}</td>
                                        <td>{{$documents->file_size}}</td>
                                        <td>{{$documents->file_description}}</td>
                                        <td>
                                            <div class="dropdown" align="right">
                                                <button class="btn btn-primary btn-xs dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">Options<span class="caret"></span></button>
                                                <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="menu1">
                                                    <li role="presentation"><a role="menuitem" href="/eoc/survey/documents/download/{{$documents->id}}"><i class="fa fa-download"></i> Download</a></li>
                                                    <li role="presentation" class="divider"></li>
                                                    <li role="presentation"><a role="menuitem" href="/report/request/documents/delete/{{$documents->id}}?report_id={{$documents->survey_id}}"><i class="fa fa-trash" style="color:red;"></i> Remove Document</a></li>
                                                </ul>
                                            </div>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif



                    </div>
                    <div id="menu2" class="tab-pane fade">
                        <h3>Menu 2</h3>
                        <p>Some content in menu 2.</p>
                    </div>
                </div>



            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection