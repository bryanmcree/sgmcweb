<div class="modal fade AddCategory_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">
                
                <h4 class="modal-title" id="myModalLabel"><b>EOC Category</b></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body bg-default">
                {!! Form::open(array('action' => ['EOCController@newCategory'], 'class' => 'form_control')) !!}
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            {!! Form::label('category_name', 'Category Name:') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text bg-danger" id="basic-addon1"><i class="fa fa-exclamation-triangle"></i></span>
                                </div>
                                {!! Form::text('category_name', null, ['class' => 'form-control', 'id'=>'category_name']) !!}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer bg-default">
                    <div class="RightLeft">
                        {!! Form::submit('Add New Category', ['class'=>'btn btn-sgmc', 'id'=>'updateButton']) !!}
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>