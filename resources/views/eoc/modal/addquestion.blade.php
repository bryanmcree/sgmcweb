<div class="modal fade AddQuestion_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">
                
                <h4 class="modal-title" id="myModalLabel"><b>EOC Question</b></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                
            </div>
            <div class="modal-body bg-default">
                {!! Form::open(array('action' => ['EOCController@newQuestion'], 'class' => 'form_control')) !!}
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            {!! Form::label('question', 'Question:') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text bg-danger" id="basic-addon1"><i class="fa fa-exclamation-triangle"></i></span>
                                </div>
                                {!! Form::text('question', null, ['class' => 'form-control', 'id'=>'question']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::label('category_id', 'Select Category:') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text bg-danger" id="basic-addon1"><i class="fa fa-exclamation-triangle"></i></span>
                                </div>
                                <select name="category_id" class="form-control">
                                    <option value="">[Select Category]</option>
                                    @foreach($categories as $category)

                                        <option value="{{ $category->id }}"> {{$category->category_name}}</option>

                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer bg-default">
                    <div class="RightLeft">
                        {!! Form::submit('Add New Question', ['class'=>'btn btn-sgmc', 'id'=>'updateButton']) !!}
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>