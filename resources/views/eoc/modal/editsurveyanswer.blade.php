<div class="modal fade EditAnswer_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">
                
                <h4 class="modal-title" id="myModalLabel"><b>EOC EDIT Answer</b></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

            </div>
            <div class="modal-body bg-default">
                <form action="/eoc/answer/update" method="post">
                    <input type="hidden" class="id" name="id">

                    <div class="form-group">
                        <label for="">Answer</label><br>
                        <input type="radio" name="answer" class="yes" value="1">Yes
                        <input type="radio" name="answer" class="no" value="0">No
                        <input type="radio" name="answer" class="na" value="2">NA
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>