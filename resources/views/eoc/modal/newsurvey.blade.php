<div class="modal fade NewSurvey_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">

                <h4 class="modal-title" id="myModalLabel"><b>Start New Survey</b></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                
            </div>
            <div class="modal-body bg-default">
                {!! Form::open(array('action' => ['EOCController@newSurvey'], 'class' => 'form_control')) !!}
                {!! Form::hidden('surveyor', Auth::user()->employee_number,['class'=>'surveyor']) !!}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('cost_center', 'Select Cost Center:') !!}
                            <div class="input-group">

                                <div class="input-group-prepend">
                                    <span class="input-group-text bg-danger" id="basic-addon1"><i class="fa fa-exclamation-triangle"></i></span>
                                </div>
                                <select name="cost_center" class="form-control pillar_id">
                                    <option value="">-Select-</option>
                                    @foreach($CostCenters as $costCenter)

                                        <option value="{{ $costCenter->cost_center }}"> {{$costCenter->style1}}</option>

                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('managers', 'Managers Name:') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text bg-danger" id="basic-addon1"><i class="fa fa-exclamation-triangle"></i></span>
                                </div>
                                {!! Form::text('managers', null, ['class' => 'form-control', 'id'=>'managers']) !!}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer bg-default">
                    <div class="RightLeft">
                        {!! Form::submit('Start Survey!', ['class'=>'btn btn-sgmc', 'id'=>'updateButton']) !!}
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>