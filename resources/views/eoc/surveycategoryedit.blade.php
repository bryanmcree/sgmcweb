{{--New file Template--}}

{{--Add Security for this page below--}}
    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-lg-12">
        <div class="card bg-dark mb-3 text-white">
            <div class="card-header">
                <b>{{$cat->category_name}}</b>
            </div>
            <div class="card-body">

                <ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab">Questions</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="photo-tab" data-toggle="tab" href="#menu1" role="tab">Photos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="return-tab" href="/eoc/survey/{{$survey_id}}" role="tab">Back to Categories</a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div id="home" class="tab-pane fade show active">
                        {!! Form::open(array('action' => ['EOCController@submitAnswers'], 'class' => 'form_control')) !!}
                        <input type = "hidden" name="survey_id" value="{{$survey_id}}">

                        <table class="table table-condensed table-dark table-bordered">
                            <tbody>
                            <?php $list = -1?>
                            @foreach($answers as $answer)
                                <?php $list = $list + 1?>

                                <tr>
                                    <td colspan="5" class="bg-primary"><b><div style="text-align: justify;font-size: 20px;">@if(!is_null($answer->eocQuestion)){{$answer->eocQuestion->question}} @else error @endif</div> </b></td>
                                </tr>
                                <tr>
                                    <td colspan="1"><b>Answer</b></td>
                                    <td colspan="1"><b>Corrected</b></td>
                                    <td colspan="2"><b>Notes</b></td>
                                    <td colspan="1"></td>
                                </tr>
                                <tr>
                                    <td colspan="1">@if($answer->yes == 1) Yes @elseif($answer->no == 1) No @else N/A @endif</td>
                                    <td colspan="1">@if($answer->yes == 1) Not Needed @elseif($answer->no == 1) @if($answer->corrected == 1) CORRECTED! @else NOT CORRECTED! @endif @endif</td>
                                    <td colspan="2">{{$answer->notes}}</td>
                                    <td colspan="1">
                                        <a href="/eoc/answer/edit/{{ $answer->id }}" class="btn btn-warning btn-block"
                                           {{-- data-answer_id ="{{$answer->id}}"
                                           data-yes ="{{$answer->yes}}"
                                           data-no ="{{$answer->no}}"
                                           data-na ="{{$answer->na}}"
                                           data-corrected ="{{$answer->corrected}}"
                                           data-notes ="{{$answer->notes}}" --}}
                                        >Edit</a>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        {!! Form::close() !!}
                    </div>
                    <div id="menu1" class="tab-pane fade">
                        {!! Form::open(array('url'=>'eoc/survey/documents/add','method'=>'POST', 'files'=>true)) !!}
                        {!! Form::hidden('survey_id', $survey_id,['class'=>'report_id']) !!}
                        {!! Form::hidden('category_id', $cat->id,['class'=>'report_id']) !!}
                        <div class="form-group">
                            {!! Form::label('file_description', 'Description:') !!}
                            <div class="input-group">
                                <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                {!! Form::text('file_description', null, ['class' => 'form-control', 'id'=>'first_name','placeholder'=>'File Description', 'required']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('file', 'File:') !!}
                            <div class="input-group">
                                <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                {!! Form::file('file_name', array('class'=>'form-control')) !!}
                            </div>
                        </div>
                        {!! Form::submit('Upload File', array('class' => 'btn form-control btn-block btn-primary mb-3')) !!}
                        {!! Form::close() !!}



                        @if ($docs->isEmpty())
                            <div class="alert bg-info">You do not have any files attached.</div>
                        @else
                            <table class="documents table table-condensed table-hover">
                                <thead>
                                <tr>
                                    <td><B>Thumbnail</B></td>
                                    <td><B>Date Added</B></td>
                                    <td><B>Added By</B></td>
                                    <td><B>File Type</B></td>
                                    <td><B>File Size</B></td>
                                    <td><B>Description</B></td>
                                    <td></td>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach ($docs as $documents)
                                    <tr>
                                        <td><img data-src= "{{Storage::url($documents->new_filename) }}" alt="Card image cap"></td>
                                        <td>{{$documents->created_at}}</td>
                                        <td>{{$documents->addedBy->first_name}} {{$documents->addedBy->last_name}}</td>
                                        <td>{{$documents->file_extension}}</td>
                                        <td>{{$documents->file_size}}</td>
                                        <td>{{$documents->file_description}}</td>
                                        <td>
                                            <div class="dropdown" align="right">
                                                <button class="btn btn-primary btn-xs dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">Options<span class="caret"></span></button>
                                                <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="menu1">
                                                    <li role="presentation"><a role="menuitem" href="/eoc/survey/documents/download/{{$documents->id}}"><i class="fa fa-download"></i> Download</a></li>
                                                    <li role="presentation" class="divider"></li>
                                                    <li role="presentation"><a role="menuitem" href="/report/request/documents/delete/{{$documents->id}}?report_id={{$documents->survey_id}}"><i class="fa fa-trash" style="color:red;"></i> Remove Document</a></li>
                                                </ul>
                                            </div>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif



                    </div>
                    <div id="menu2" class="tab-pane fade">
                        <h3>Menu 2</h3>
                        <p>Some content in menu 2.</p>
                    </div>
                </div>



            </div>
        </div>
    </div>
    @include('eoc.modal.editsurveyanswer')
@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">
    $(document).on("click", ".EditAnswer", function () {
        //alert($(this).attr("data-pillar_id"));
        $('.id').val($(this).attr("data-answer_id"));
        $('.yes').val($(this).attr("data-yes"));
        $('.no').val($(this).attr("data-no"));
        $('.na').val($(this).attr("data-na"));
        $('.corrected').val($(this).attr("data-corrected"));
        $('.notes').val($(this).attr("data-notes"));
        $('.EditAnswer_modal').modal('show');
    });
</script>

@endsection