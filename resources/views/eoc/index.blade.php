{{--New file Template--}}

{{--Add Security for this page below--}}

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="card text-white bg-dark mb-3">
            <div class="card-header">
                <h3><b>EOC</b></h3>
            </div>
            <div class="card-body">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a data-toggle="tab" href="#home" class="nav-link active">Home</a>
                    </li>
                    <li class="nav-item">
                        <a data-toggle="tab" href="#menu1" class="nav-link"><b>Questions</b></a>
                    </li>
                    <li class="nav-item">
                        <a href="/eoc/safety/create" class="nav-link"><b>Safety Star Nomination</b></a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div id="home" class="tab-pane active in">
                        <br>
                        <p><a href="#" class="btn btn-primary" data-toggle="modal" data-target=".NewSurvey_modal">
                                Start New Survey
                            </a></p>

                        @if($surveys->isEmpty())

                            <div class="alert alert-info">You don't have any surveys.</div>
                        @else
                        <table class="table">
                            <thead>
                            <tr>
                                <td><b>Cost Center</b></td>
                                <td><b>Manager</b></td>
                                <td><b>Conducted By</b></td>
                                <td><b>Date</b></td>
                                <td><b>Report</b></td>
                                <td></td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($surveys as $survey)
                            <tr>
                                <td>{{$survey->cost_center}} - {{$survey->eocCostCenter->unit_code_description}}</td>
                                <td>{{$survey->managers}}</td>
                                <td>{{$survey->eocSurveyor->last_name}}, {{$survey->eocSurveyor->first_name}}</td>
                                <td>{{$survey->created_at}}</td>
                                <td><a href="/eoc/report/{{ $survey->id }}" class="btn btn-primary">Report</a> </td>
                                <td align="right">

                                    @if ($inprogress->contains('survey_id', $survey->id))
                                        <a href="/eoc/survey/{{$survey->id}}" class="btn btn-success">
                                            Continue Survey
                                        </a>

                                     @else
                                    <a href="/eoc/survey/{{$survey->id}}" class="btn btn-primary">
                                        Start Survey
                                    </a>
                                        @endif


                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @endif
                    </div>

                    <div id="menu1" class="tab-pane fade in pt-2">
                        <div class="card-header mb-3">
                            <h3>Questions</h3>
                        <p><a href="#" class="btn btn-primary" data-toggle="modal" data-target=".AddCategory_modal">
                                Add Category
                            </a>
                            <a href="#" class="btn btn-primary" data-toggle="modal" data-target=".AddQuestion_modal">
                                Add Question
                            </a></p>
                        </div>
                        @foreach($categories as $category)
                        <div class="card bg-secondary mb-3">
                            <div class="card-header">
                                <div class="RightLeft">
                                    <h3><b>{{$category->category_name}}</b></h3>

                                    <ul class="nav nav-pills float-right mb-2">
                                        <li class="nav-item"><a href="#" class="EditCategory btn btn-warning"
                                                data-category_id = "{{$category->id}}"
                                                data-category_name = "{{$category->category_name}}"
                                                >Edit</a></li>
                                                &nbsp;
                                        <li class="nav-item"><a href="#" class="DeleteCategory btn btn-danger"
                                                data-category_id ="{{$category->id}}"
                                            >Delete</a></li>
                                    </ul>
                                </div>

                            </div>
                            <div class="card-body">
                                <table class="table table-bordered">
                                    <?PHP $question_number = 0 ?>
                                    @foreach($category->eocQuestions as $question)
                                    <?PHP $question_number = $question_number +1 ?>
                                        <tr>
                                            <td width="2%"><b>{{$question_number}}.</b></td>
                                            <td>{{$question->question}}</td>
                                            <td width="15%" align="right">
                                                <ul class="nav nav-pills float-right">
                                                    <li class="nav-item"><a href="#" class="EditQuestion btn btn-sm btn-warning"
                                                            data-question_id = "{{$question->id}}"
                                                            data-question = "{{$question->question}}"
                                                            data-category_id = "{{$question->category_id}}">Edit</a></li>
                                                            &nbsp;
                                                    <li class="nav-item"><a href="#" class="DeleteQuestion btn btn-sm btn-danger"
                                                            data-question_id ="{{$question->id}}"
                                                        >Delete</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    
                </div>
            </div>
        </div>
    </div>


    @include('eoc.modal.addcategory')
    @include('eoc.modal.addquestion')
    @include('eoc.modal.editquestion')
    @include('eoc.modal.editcategory')
    @include('eoc.modal.newsurvey')

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">
    $(document).on("click", ".EditQuestion", function () {
        //alert($(this).attr("data-pillar_id"));
        $('.question_id').val($(this).attr("data-question_id"));
        $('.question').val($(this).attr("data-question"));
        $('.category_id').val($(this).attr("data-category_id"));
        $('.EditQuestion_modal').modal('show');
    });

    $(document).on("click", ".EditCategory", function () {
        //alert($(this).attr("data-pillar_id"));
        $('.category_id').val($(this).attr("data-category_id"));
        $('.category_name').val($(this).attr("data-category_name"));
        $('.EditCategory_modal').modal('show');
    });

    

    //Delete Scrip for Questions
    $(document).on("click", ".DeleteQuestion", function () {
        var question_id = $(this).attr("data-question_id");
        //alert($(this).attr("data-question_id"));
        //alert($(this).attr("data-system-id"));
        deleteQuestion(question_id);
    });

    function deleteQuestion(question_id) {
        swal({
            title: "Delete Question?",
            text: "Are you sure that you want to delete this question?",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            confirmButtonText: "Yes, delete it!",
            confirmButtonColor: "#ec6c62"
        }, function() {
            $.ajax({
                url: "/eoc/question/del/" + question_id  ,
                type: "GET"
            })
                .done(function(data) {
                    swal({
                            title: "Deleted",
                            text: "Question was Deleted",
                            type: "success",
                            timer: 1800,
                            showConfirmButton: false

                        }
                    );
                    setTimeout(function(){window.location.replace('/eoc/')},1900);
                })
                .error(function(data, status, error) {
                    alert(status);
                    alert(xhr.responseText);
                    swal("Oops", "We couldn't connect to the server!", "error");
                });
        });



    }
    //END DELETE SCRIPT

    //Delete Scrip for categories
    $(document).on("click", ".DeleteCategory", function () {
        var category_id = $(this).attr("data-category_id");
        //alert($(this).attr("data-category_id"));
        deleteCategory(category_id);
    });

    function deleteCategory(category_id) {
        swal({
            title: "Delete Category?",
            text: "Are you sure that you want to delete this category and ALL the questions assigned to it?  If not " +
            "hit Cancel button and edit the questions by putting them in a different category.",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            confirmButtonText: "Yes, delete it!",
            confirmButtonColor: "#ec6c62"
        }, function() {
            $.ajax({
                url: "/eoc/category/del/" + category_id  ,
                type: "GET"
            })
                .done(function(data) {
                    swal({
                            title: "Deleted",
                            text: "Question was Deleted",
                            type: "success",
                            timer: 1800,
                            showConfirmButton: false

                        }
                    );
                    setTimeout(function(){window.location.replace('/eoc/')},1900);
                })
                .error(function(data, status, error) {
                    alert(status);
                    alert(xhr.responseText);
                    swal("Oops", "We couldn't connect to the server!", "error");
                });
        });



    }
    //END DELETE SCRIPT
</script>

<script>
        $(document).ready(function() {
            $('#eocsafety').DataTable( {
                "pageLength": 15,
                "columnDefs": [ {
                    "targets"  : 'no-sort',
                    "orderable": false,
                }]
            });
        });
    </script>

@endsection