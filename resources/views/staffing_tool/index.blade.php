{{--New file Template--}}

{{--Add Security for this page below--}}


    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="row justify-content-md-center">
        <div class="card col-sm-3 mb-4 border-info text-white bg-dark">
            <div class="card-header">Staffing Matrix Tool</div>
            <div class="card-body">
                <form method="POST" action="/staffing_tool/results">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="question_type_id">Select Unit</label>
                        <select class="form-control" id="question_type_id" name="shift_unit" required>
                            <option value="" selected>[Select Unit]</option>
                            @foreach($units as $unit_shift)
                                <option value="{{$unit_shift->shift_unit}}">{{$unit_shift->shift_unit}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="required_id">Select Shift</label>
                        <select class="form-control" id="required_id" name="shift" required>
                            <option value="" selected>[Select Shift]</option>
                            <option value="Day">Day</option>
                            <option value="Evening">Evening</option>
                            <option value="Night">Night</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="title_id">Enter the number of patients.</label>
                        <input type="number" name="total_patients" class="form-control" id="title_id" required>
                    </div>
                    <input type="Submit" class="btn btn-block btn-primary" value="Calculate Staffing Matrix">
                </form>

                @if($errors->any())
                    <br>
                    <div class="row">
                        <div class="alert alert-danger"><h4>{{$errors->first()}}</h4></div>
                    </div>
                @endif
            </div>
        </div>
    </div>




@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
