{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Page Design Goes Below--}}
@section('content')

    <div class="card-deck">
        <div class="card mb-4 border-info text-white bg-dark">
            <div class="card-header">Staffing Matrix Tool</div>
            <div class="card-body">
                <form method="POST" action="/staffing_tool/results">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="question_type_id">Select Unit</label>
                        <select class="form-control" id="question_type_id" name="shift_unit" required>
                            <option value="" selected>[Select Unit]</option>
                            @foreach($units as $unit_shift)
                                <option value="{{$unit_shift->shift_unit}}">{{$unit_shift->shift_unit}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="required_id">Select Shift</label>
                        <select class="form-control" id="required_id" name="shift" required>
                            <option value="" selected>[Select Shift]</option>
                            <option value="Day">Day</option>
                            <option value="Evening">Evening</option>
                            <option value="Night">Night</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="title_id">Enter the number of patients.</label>
                        <input type="number" name="total_patients" class="form-control" id="title_id" required>
                    </div>
                    <input type="Submit" class="btn btn-block btn-primary" value="Calculate Staffing Matrix">
                </form>

                @if($errors->any())
                    <br>
                    <div class="row">
                        <div class="alert alert-danger"><h4>{{$errors->first()}}</h4></div>
                    </div>
                @endif
            </div>
        </div>
        <div class="card mb-4 pt-4 border-info text-white bg-dark">
            <div class="card-body">
                <table class="table">
                    <thead>
                        <td><b>Total Licensed Staff:</b></td>
                        <td align="right"><b>{{($licensed_staff + $results->min_pcc)}}</b></td>
                    </thead>
                    <tr>
                        <td><b>PCC:</b></td>
                        <td align="right"><b>{{$results->min_pcc}}</b></td>
                    </tr>
                    <tr>
                        <td><b>RN/LPN:</b></td>
                        <td align="right"><b>{{$licensed_staff + $results->min_rn}}</b></td>
                    </tr>
                    <tr>
                        <td><b>PCT III:</b></td>
                        <td align="right"><b>@if($results->min_pct3 == 0){{$nonlicensed_staff}} @else {{$nonlicensed_staff + $results->min_pct3}} @endif</b></td>
                    </tr>
                    <tr>
                        <td><b>HUC:</b></td>
                        <td align="right"><b>{{$results->min_huc}}</b></td>
                    </tr>
                    <tr>
                        <td><b>Monitor Tech:</b></td>
                        <td align="right"><b>{{$results->min_tech}}</b></td>
                    </tr>
                    <tr>
                        <td><b>Total Staff:</b></td>
                        <td align="right"><b>{{(($licensed_staff + $results->min_pcc))+$nonlicensed_staff+$results->min_huc+$results->min_tech}}</b></td>
                    </tr>
                    <tr>
                        <td><b>HPPD:</b></td>
                        <td align="right"><b>{{round((((($licensed_staff + $results->min_pcc))+$nonlicensed_staff+$results->min_huc+$results->min_tech+$results->min_rn) * 24 )/$total_patients,2)}}</b></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="card mb-4 border-info text-white bg-dark">
            <div class="card-body">
                <form method="post" action="/staffing_tool/addnote">
                    {{ csrf_field() }}
                    <input type="hidden" value="{{ Auth::user()->employee_number }}" name="created_by">
                    <input type="hidden" value="{{$unit}}" name="unit">
                    <input type="hidden" value="{{$total_patients}}" name="total_patients">
                    <input type="hidden" value="{{$shift}}" name="shift">
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Notes, Comments, Articulation</label>
                        <textarea class="form-control" name="comments" id="exampleFormControlTextarea1" rows="3" required></textarea>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="csp" value="" id="defaultCheck1">
                        <label class="form-check-label" for="defaultCheck1">
                            <B>CSP</B>
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="" name="call_back" id="defaultCheck2">
                        <label class="form-check-labe2" for="defaultCheck2">
                            <B>Call Back</B>
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="" name="ot" id="defaultCheck3">
                        <label class="form-check-label3" for="defaultCheck3">
                            <B>Over Time</B>
                        </label>
                    </div>
                    <input type="Submit" class="btn btn-block btn-primary" value="Submit Notes">
                </form>
            </div>
        </div>
    </div>



@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
