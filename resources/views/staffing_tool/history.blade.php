{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Staffing Tool Admin') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <b>Staffing Tool Heading</b>
            </div>
            <div class="panel-body">
                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel panel-heading">
                            <b>History</b> ({{$history->count()}})
                        </div>
                        <div class="panel-body">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td>Date</td>
                                        <td>User</td>
                                        <td>String</td>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($history as $entry)
                                    <tr>
                                        <td>{{$entry->created_at}}</td>
                                        <td>{{$entry->userid}}</td>
                                        <td>{{$entry->search_string}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="panel panel-default">
                        <div class="panel panel-heading">
                            <b>Comments</b> ({{$comments->count()}})
                        </div>
                        <div class="panel-body">
                            <table class="table">
                                <thead>
                                <tr>
                                    <td>Date</td>
                                    <td>User</td>
                                    <td>Unit</td>
                                    <td>Shift</td>
                                    <td># Pat</td>
                                    <td>CSP</td>
                                    <td>CB</td>
                                    <td>OT</td>
                                    <td>Comments</td>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($comments as $entry)
                                    <tr>
                                        <td>{{$entry->created_at}}</td>
                                        <td>{{$entry->created_by}}</td>
                                        <td>{{$entry->unit}}</td>
                                        <td>{{$entry->shift}}</td>
                                        <td>{{$entry->total_patients}}</td>
                                        <td>{{$entry->csp}}</td>
                                        <td>{{$entry->call_back}}</td>
                                        <td>{{$entry->ot}}</td>
                                        <td>{{$entry->comments}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
@endif