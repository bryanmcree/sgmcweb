{{--New file Template--}}



    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="card bg-dark text-white mb-3 ">
            <div class="card-header">
                TEST
            </div>
            <div class="card-body">
                <div class="col-lg-6">
                    <table class="table">
                        <thead>
                            <tr>
                                <td><b>CC</b></td>
                                <td align="center"><b>Bedded</b></td>
                                <td align="center"><b>Clocked In</b></td>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($census as $currentCensus)
                            <tr>
                                <td>{{$currentCensus->costCenter->style1}}</td>
                                <td align="center">{{$currentCensus->total_bedded}}</td>
                                <td align="center">
                                    @if(!is_null($clockedin->where('cost_center',$currentCensus->cost_center)->first()['total']))
                                    {{$clockedin->where('cost_center',$currentCensus->cost_center)->first()['total']}}
                                    @else
                                    0
                                    @endif

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
