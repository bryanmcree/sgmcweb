@extends('layouts.app')

@section('content')


    <div class="col-md-12">
    <div class="panel panel-success">
        <div class="panel-heading"><i class="fa fa-question-circle"  data-toggle="popover" data-placement="bottom"
                                      title="Human Resources"
                                      data-content="Searchable list of all current employees and employee information"></i> -><b>Employees ({{$tests->total()}})</b> -> <a href="/employees/print/" ><span class="fa fa-print"></span></a></div>
        <div class="panel-body">
            @if ($tests->isEmpty())
                <div class="container-fluid">
                    <div class="row">
                        {!! Form::open(array('url' => '/employees', 'method' => 'GET', 'files' => true,'class' => 'form_control')) !!}
                        <div class="input-group">
                            {!! Form::text('search', null,array('class'=>'form-control','placeholder'=>'Search for employee.', 'autofocus'=>'autofocus')) !!}
                            <span class="input-group-btn">
                        {!! Form::submit('Search', array('class'=>'btn btn-success')) !!}
                        </span>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            <br>
                <div class="alert alert-danger" role="alert"><b>No ACTIVE employees to display.</b> </div>
            @else
                <div class="container-fluid">
                    <div class="row">
                        {!! Form::open(array('url' => '/hr', 'method' => 'GET', 'files' => true,'class' => 'form_control')) !!}
                        <div class="input-group">
                        {!! Form::text('search', null,array('class'=>'form-control','placeholder'=>'Search for employee.', 'autofocus'=>'autofocus')) !!}
                        <span class="input-group-btn">
                        {!! Form::submit('Search', array('class'=>'btn btn-success')) !!}
                        </span>
                        </div>
                    </div>
                        {!! Form::close() !!}
                </div>
                <table class="table table-condensed table-hover table-responsive table-bordered" id="table">

                    <thead>
                    <tr class="bg-success">
                        <td class=""><b>Name</b></td>
                        <td class="hidden-xs hidden-sm "><b>Title</b></td>
                        @if (Auth::user()->access == 'Admin')
                        <td class="hidden-xs hidden-sm hidden-md"><b>Emp #</b></td>
                        <td class="hidden-xs hidden-sm hidden-md"><b>Hire Date</b></td>
                        <td class="hidden-xs hidden-sm hidden-md"><b>Term. Date</b></td>
                        @endif
                        <td class="hidden-xs hidden-sm "><b>Department</b></td>
                        <td class="hidden-xs"><b>Facility</b></td>
                        @if (Auth::user()->access == 'Admin')
                        <td align="right" colspan="4" style="vertical-align:middle"></td>
                        @endif
                    </tr>
                    </thead>


                    @foreach ($tests as $test)
                        <tbody>
                        <tr>
                            <td class="" @if($test->Status != 'Active' AND $test->termination_date != null)style="color: red;" @endif>{{$test->last_name}}, {{$test->first_name}}</td>
                            <td class="hidden-xs hidden-sm " @if($test->Status != 'Active'  AND $test->termination_date != null)style="color: red;" @endif>{{$test->title}}</td>
                            @if (Auth::user()->access == 'Admin')
                            <td class="hidden-xs hidden-sm hidden-md" @if($test->Status != 'Active' AND $test->termination_date != null)style="color: red;" @endif>{{$test->emp_number}}</td>
                            <td class="hidden-xs hidden-sm hidden-md" @if($test->Status != 'Active' AND $test->termination_date != null)style="color: red;" @endif>{{$test->hire_date}}</td>
                            <td class="hidden-xs hidden-sm hidden-md" @if($test->Status != 'Active' AND $test->termination_date != null)style="color: red;" @endif>{{$test->termination_date}}</td>
                            @endif
                            <td class="hidden-xs hidden-sm " @if($test->Status != 'Active' AND $test->termination_date != null)style="color: red;" @endif>{{$test->unit_code}}: {{$test->unit_description}}</td>
                            <td class="hidden-xs" @if($test->Status != 'Active' AND $test->termination_date != null)style="color: red;" @endif>{{$test->location}}</td>
                            @if (Auth::user()->access == 'Admin')
                                <td align="center" style="vertical-align:middle"><a href="/user/#/edit"><span class="fa fa-desktop" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="System Access"></span></a></td>
                                <td align="center" style="vertical-align:middle"><a href="/hr/par"><span class="fa fa-btn fa-file-text-o" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="PAR Form"></span></a></td>
                                <td align="center" style="vertical-align:middle"><a href="/extramileTemp/create/{{$test->emp_number}}" ><span class="fa fa-trophy" data-toggle="tooltip" data-placement="top" title="ExtraMile Award" aria-hidden="true"></span></a></td>
                            <td align="center" style="vertical-align:middle"><a href="#" data-toggle="modal"  data-target="#View"
                                                                                data-first_name="{{$test->first_name}}"
                                                                                data-last_name="{{$test->last_name}}"
                                                                                data-address="{{$test->address}}"
                                                                                data-city="{{$test->city}}"
                                                                                data-state="{{$test->state}}"
                                                                                data-zip="{{$test->zip}}"
                                                                                data-phone="{{PhoneNumbers::formatNumber($test->phone)}}"
                                                                                data-email="{{$test->email}}"
                                                                                data-hire_date="{{$test->hire_date}}"
                                                                                data-gender="{{$test->gender}}"
                                                                                data-title="{{$test->title}}"
                                                                                data-unit_code="{{$test->unit_code}}"
                                                                                data-unit_description="{{$test->unit_description}}"
                                                                                data-location="{{$test->location}}"
                                                                                data-status="{{$test->Status}}"
                                                                                data-termination_date="{{$test->termination_date}}"
                                                                                ><span class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Employee Details"></span></a></td>
                            @endif
                        </tr>
                        </tbody>
                    @endforeach
                    <tr>
                        <tc colspan="5"><hr></tc>
                    </tr>
                </table>

                <div class="container-fluid">
                    <div class="row">
                        {!! $tests->render() !!}
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>

@endsection



<!-- Modal -->
<div class="modal fade" id="View" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><b>Employee Detail</b></h4>
            </div>
            <div class="modal-body bg-info">
                <div class="row">
                    <div class="col-md-3">
                            <div class="panel panel-default">
                                <div class="panel-heading"><b>Name</b></div>
                                <div class="panel-body">
                                    <span id="last_name"></span>, <span id="first_name"></span>
                                </div>
                            </div>
                    </div>
                    <div class="col-md-4">
                            <div class="panel panel-default">
                                <div class="panel-heading"><b>Title</b></div>
                                <div class="panel-body">
                                    <span id="title"></span>
                                </div>
                            </div>
                    </div>
                    <div class="col-md-5">
                        <div class="panel panel-default">
                            <div class="panel-heading"><b>Department</b></div>
                            <div class="panel-body">
                                <span id="unit_code"></span>: <span id="unit_description"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">

                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading"><b>Address</b></div>
                            <div class="panel-body">
                                <span id="address"></span> <span id="city"></span>, <span id="state"></span> <span id="zip"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-heading"><b>Location</b></div>
                            <div class="panel-body">
                                <span id="location"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-heading"><b>Hire Date</b></div>
                            <div class="panel-body">
                                <span id="hire_date"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading"><b>Phone</b></div>
                            <div class="panel-body">
                                <span id="phone"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading"><b>Email</b></div>
                            <div class="panel-body">
                                <span id="email"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading"><b>Status</b></div>
                            <div class="panel-body">
                                <span id="status"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <div class="panel panel-default">
                            <div class="panel-heading"><b>Gender</b></div>
                            <div class="panel-body">
                                <span id="gender"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading"><b>Termination Date</b></div>
                            <div class="panel-body">
                                <span id="termination_date"></span>
                            </div>
                        </div>
                    </div>
                </div>
             </div>
            <div class="modal-footer bg-primary">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
