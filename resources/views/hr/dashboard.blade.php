@extends('layouts.app')

@section('content')
    <style>
        progress {
            margin-bottom: 0 !important;
        }
    </style>
<div class="panel panel-success">
    <div class="panel-heading">
        <b>Current Task</b>
    </div>
    <div class="panel-body">

        <div class="pandel panel-success">
            <div class="panel-heading">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne2" aria-expanded="true" aria-controls="collapseOne2">
                    <div class="col-md-6">
                    Bryan McRee <b>(New Hire)</b>
            </div>
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped active" role="progressbar"
                             aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width:25%">
                            25%
                        </div>
                    </div>
                </a>
            </div>
            <div id="collapseOne2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                    <table class="table table-condensed table-hover table-responsive">
                        <thead>
                        <tr class="bg-default">
                            <td class=""><b>Task</b></td>
                            <td class=""><b>Started</b></td>
                            <td class="hidden-xs hidden-sm "><b>Assigned To</b></td>
                            <td class="hidden-xs"><b>Status</b></td>
                            <td><b>Time Laps</b></td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="danger">
                            <td class="">Active Directory</td>
                            <td class="">{{Carbon\Carbon::Now()}}</td>
                            <td class="hidden-xs hidden-sm ">Information Services</td>
                            <td class="hidden-xs">In Progress</td>
                            <td></td>
                        </tr>
                        <tr class="success">
                            <td class="">Enter into HRP</td>
                            <td class="">{{Carbon\Carbon::Now()}}</td>
                            <td class="hidden-xs hidden-sm ">Bryan McRee</td>
                            <td class="hidden-xs">Completed</td>
                            <td>00:12:37</td>
                        </tr>
                        <tr class="danger">
                            <td class="">Access to HPF</td>
                            <td class="">{{Carbon\Carbon::Now()}}</td>
                            <td class="hidden-xs hidden-sm ">Connie May</td>
                            <td class="hidden-xs">In Progress</td>
                            <td></td>
                        </tr>
                        <tr class="danger">
                            <td class="">Door Access / Key Card</td>
                            <td class="">{{Carbon\Carbon::Now()}}</td>
                            <td class="hidden-xs hidden-sm ">Security Services</td>
                            <td class="hidden-xs">In Progress</td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <br>
        <div class="pandel panel-success">
            <div class="panel-heading">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne1" aria-expanded="true" aria-controls="collapseOne1">
                    Walker, Curtis <b>(Transfer)</b>
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped active" role="progressbar"
                             aria-valuenow="66" aria-valuemin="0" aria-valuemax="100" style="width:66%">
                            66%
                        </div>
                    </div>
                </a>
            </div>
            <div id="collapseOne1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                    <table class="table table-condensed table-hover table-responsive">
                        <thead>
                        <tr class="bg-default">
                            <td class=""><b>Task</b></td>
                            <td class=""><b>Started</b></td>
                            <td class="hidden-xs hidden-sm "><b>Assigned To</b></td>
                            <td class="hidden-xs"><b>Status</b></td>
                            <td><b>Time Laps</b></td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="success">
                            <td class="">Update HRP</td>
                            <td class="">{{Carbon\Carbon::Now()}}</td>
                            <td class="hidden-xs hidden-sm ">Bryan McRee</td>
                            <td class="hidden-xs">Completed</td>
                            <td>00:12:37</td>
                        </tr>
                        <tr class="danger">
                            <td class="">Access to HPF</td>
                            <td class="">{{Carbon\Carbon::Now()}}</td>
                            <td class="hidden-xs hidden-sm ">Connie May</td>
                            <td class="hidden-xs">In Progress</td>
                            <td></td>
                        </tr>
                        <tr class="success">
                            <td class="">Door Access / Key Card</td>
                            <td class="">{{Carbon\Carbon::Now()}}</td>
                            <td class="hidden-xs hidden-sm ">Security Services</td>
                            <td class="hidden-xs">Completed</td>
                            <td>01:02:37</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>




@endsection