@extends('layouts.app')

@section('content')
    @include('hr.navbar')
<div class="col-md-12">
    <div class="panel panel-success">
        <div class="panel-body">
            <div class="col-md-6">
                <div class="panel panel-default" style="height:420px; overflow-y: scroll;">
                    <div class="panel-heading panel-heading-custom">
                        PAR Status
                    </div>
                    <div class="panel-body panel-body-custom">
                        <div class="panel-body">
                                <table class="table table-condensed table-hover table-responsive">
                                    <thead>
                                    <tr class="bg-default">
                                        <td><b>Name</b></td>
                                        <td><b>Process</b></td>
                                        <td><b>Date</b></td>
                                        <td><b>% Complete</b></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr class='clickable-row' data-toggle="modal" data-target=".bd-example-modal-lg">
                                        <td>McRee, Bryan</td>
                                        <td>New Hire</td>
                                        <td>{{Carbon\Carbon::Now()->format('m-d-Y')}}</td>
                                        <td><div class="progress"><div class="progress-bar progress-bar-striped active" role="progressbar"
                                                                   aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width:25%">
                                                25%
                                            </div></div></td>
                                    </tr>
                                    <tr class='clickable-row'  data-toggle="modal" data-target=".bd-example-modal-lg">
                                        <td>Walker, Curtis</td>
                                        <td>Transfer</td>
                                        <td>{{Carbon\Carbon::Now()->format('m-d-Y')}}</td>
                                        <td><div class="progress"><div class="progress-bar progress-bar-striped active" role="progressbar"
                                                                       aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width:65%">
                                                    65%
                                                </div></div></td>
                                    </tr>
                                    </tbody>
                                </table>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading panel-heading-custom">
                        Employees Currently Clocked In
                    </div>
                    <div class="panel-body">
                        <div id="linechart"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('scripts')


<script type="application/javascript">
    jQuery(document).ready(function($) {
        $(".clickable-row1").click(function() {
            window.location = $(this).data('url');
        });
    });
</script>

<script type="text/javascript">

    google.charts.load('current', {'packages':['corechart']});

    google.charts.setOnLoadCallback(drawChart);


    function drawChart() {

        var jsonData = $.ajax({
            url: "/productivity/clockedin",
            dataType:"json",
            async: false
        }).responseText;

        //var data = google.visualization.arrayToDataTable(jsonData);
        var data = new google.visualization.DataTable(jsonData);
        var options = {
            title:'Employees Currently Clocked In',

            crosshair: { trigger: 'both' },
            height:350,
            hAxis: {

            },
            vAxis: {
                title: 'Number of Employees'
            },
            explorer: {
                axis: 'horizontal',
                keepInBounds: true,
                maxZoomIn:0.25,
                maxZoomOut:4
            },
            animation:{
                startup: true,
                duration: 1000,
                easing: 'out'
            },
            pointSize: 7,
            series: {
                0: {
                    color: 'blue',
                    pointShape: 'diamond'

                },
                3: {
                    type: "line",
                    pointSize: 0,
                    lineDashStyle: [1, 1],
                    color: '#e2431e'
                },
                2: {
                    type: "line",
                    pointSize: 0,
                    lineDashStyle: [1, 1],
                    color: '#e2431e'
                },
                1: {
                    type: "line",
                    pointSize: 0,
                    lineDashStyle: [14, 2, 2, 7],
                    color: 'green'
                }

            }
        };
        var chart = new google.visualization.AreaChart(document.getElementById('linechart'));
        chart.draw(data, options);

    }

</script>
@endsection

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-default">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h1 class="modal-title" id="drawChart">{{ Html::image('img/sgmc_logo_45.png') }} <b>PAR Status Detail</b></h1>
            </div>
            <div class="modal-body bg-default">
                <table class="table table-condensed table-hover table-responsive">
                    <thead>
                    <tr class="bg-default">
                        <td><b>Name</b></td>
                        <td><b>Process</b></td>
                        <td><b>Date</b></td>
                        <td><b>% Complete</b></td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>McRee, Bryan</td>
                        <td>New Hire</td>
                        <td>{{Carbon\Carbon::Now()->format('m-d-Y')}}</td>
                        <td><div class="progress"><div class="progress-bar progress-bar-striped active" role="progressbar"
                                                       aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width:25%">
                                    25%
                                </div></div></td>
                    </tr>
                    </tbody>
                </table>
                <br>
                <table class="table table-condensed table-hover table-responsive">
                    <thead>
                    <tr class="bg-default">
                        <td class=""><b>Task</b></td>
                        <td class=""><b>Started</b></td>
                        <td class="hidden-xs hidden-sm "><b>Assigned To</b></td>
                        <td class="hidden-xs"><b>Status</b></td>
                        <td><b>Time Laps</b></td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="danger">
                        <td class="">Active Directory</td>
                        <td class="">{{Carbon\Carbon::Now()}}</td>
                        <td class="hidden-xs hidden-sm ">Information Services</td>
                        <td class="hidden-xs">In Progress</td>
                        <td></td>
                    </tr>
                    <tr class="success">
                        <td class="">Enter into HRP</td>
                        <td class="">{{Carbon\Carbon::Now()}}</td>
                        <td class="hidden-xs hidden-sm ">Bryan McRee</td>
                        <td class="hidden-xs">Completed</td>
                        <td>00:12:37</td>
                    </tr>
                    <tr class="danger">
                        <td class="">Access to HPF</td>
                        <td class="">{{Carbon\Carbon::Now()}}</td>
                        <td class="hidden-xs hidden-sm ">Connie May</td>
                        <td class="hidden-xs">In Progress</td>
                        <td></td>
                    </tr>
                    <tr class="danger">
                        <td class="">Door Access / Key Card</td>
                        <td class="">{{Carbon\Carbon::Now()}}</td>
                        <td class="hidden-xs hidden-sm ">Security Services</td>
                        <td class="hidden-xs">In Progress</td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
        </div>
    </div>
</div>

@endsection