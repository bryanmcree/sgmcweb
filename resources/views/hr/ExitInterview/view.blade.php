{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Page Design Goes Below--}}
@section('content')
    {!! Form::model($surveys, ['method'=>'PATCH', 'action'=>['UserController@update', $surveys->id], 'files' => true]) !!}
    {!! Form::hidden('employee_number', Auth::user()->employee_number,['class'=>'srv_id']) !!}
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading panel-heading-custom">
                <h4><b>Employee Exit Interview</b></h4>
            </div>
            <div class="panel-body">

                <div class="panel-body">
                    <a href="/hr/exit/complete" class="btn btn-danger hidden-xs hidden-sm hidden-md">
                        Return to Completed Surveys
                    </a>

                    <h3><b>{{ $surveys-> employee->first_name }} {{  $surveys-> employee->last_name  }}</b></h3>
                    {{  $surveys-> employee->title  }}
                </div>

                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel panel-heading">
                            Please select the best answers for the below questions.
                        </div>
                        <div class="panel-body">

                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('tenure', 'How long have you worked for SGMC?') !!}
                                    <div class="input-group">
                                        <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                        {!! Form::select('tenure', [
                                                            '' => '[Select]',
                                                            'Less than 1 year' => 'Less than 1 year',
                                                            '1 Year to less than 3 Years' => '1 Year to less than 3 Years',
                                                            '3 years less than 5 years' => '3 years less than 5 years',
                                                            '5 years to less than 10 years' => '5 years to less than 10 years',
                                                            '10 years or more' => '10 years or more',
                                                             ], null, ['class' => 'form-control','Required']) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('emp_status', 'What was your most recent employment status?') !!}
                                    <div class="input-group">
                                        <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                        {!! Form::select('emp_status', [
                                                            '' => '[Select]',
                                                            'Full-Time' => 'Full-Time',
                                                            'Part-Time' => 'Part-Time',
                                                            'PRN' => 'PRN',
                                                             ], null, ['class' => 'form-control','Required']) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('reason_for_leaving', 'What is your reason for leaving SGMC?') !!}
                                    <div class="input-group">
                                        <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                        {!! Form::select('reason_for_leaving', [
                                                            '' => '[Select]',
                                                            'Benefits' => 'Benefits',
                                                            'Commute' => 'Commute',
                                                            'PRN' => 'PRN',
                                                            'Department Issues' => 'Department Issues',
                                                            'Facility Issues' => 'Facility Issues',
                                                            'Schedule Flexibility' => 'Schedule Flexibility',
                                                            'Pay' => 'Pay',
                                                            'Lack of Career Opportunities' => 'Lack of Career Opportunities',
                                                            'Poor relations with co-workers' => 'Poor relations with co-workers',
                                                            'Relocation' => 'Relocation',
                                                            'Retirement' => 'Retirement',
                                                            'Supervisor(s)' => 'Supervisor(s)',
                                                            'Workload' => 'Workload',
                                                            'Advanced Career Opportunity' => 'Advanced Career Opportunity',
                                                            'Want a different setting/type of work' => 'Want a different setting/type of work',
                                                             ], null, ['class' => 'form-control','Required']) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('work_related_addressed', 'If work-related, did you try and have your concern addressed?') !!}
                                    <div class="input-group">
                                        <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                        {!! Form::select('work_related_addressed', [
                                                            '' => '[Select]',
                                                            'Yes' => 'Yes',
                                                            'No' => 'No',
                                                             ], null, ['class' => 'form-control','Required']) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('like_least_about_job', 'What did you like least about your job?') !!}
                                    <div class="input-group">
                                        <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                        {!! Form::select('like_least_about_job', [
                                                            '' => '[Select]',
                                                            'Benefits' => 'Benefits',
                                                            'Culture' => 'Culture',
                                                            'Schedule' => 'Schedule',
                                                            'Management' => 'Management',
                                                            'Pay' => 'Pay',
                                                            'Workload' => 'Workload',

                                                             ], null, ['class' => 'form-control','Required']) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('like_most_about_job', 'What did you like most about working for SGMC?') !!}
                                    <div class="input-group">
                                        <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                        {!! Form::select('like_most_about_job', [
                                                            '' => '[Select]',
                                                            'Benefits' => 'Benefits',
                                                            'Facility/Amenities' => 'Facility/Amenities',
                                                            'Flexibility/Hours' => 'Flexibility/Hours',
                                                            'Supervisor' => 'Supervisor',
                                                            'Pay' => 'Pay',
                                                            'Working with patients/Type of Work' => 'Working with patients/Type of Work',

                                                             ], null, ['class' => 'form-control','Required']) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('new_employer_name', 'What is the name of your new employer?') !!}
                                    {!! Form::text('new_employer_name', null, ['class' => 'form-control', 'id'=>'sys_name']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Please rate the statement below from "Strongly Agree" to "Strongly Disagree".  Everything in this section is required.
                        </div>
                        <div class="panel-body">
                            <table class="table table-hover bg-danger">
                                <thead>
                                <tr>
                                    <td align="center"><b>Question</b></td>
                                    <td align="center"><b>Strongly Agree</b></td>
                                    <td align="center"><b>Agree</b></td>
                                    <td align="center"><b>Neither Agree nor Disagree</b></td>
                                    <td align="center"><b>Disagree</b></td>
                                    <td align="center"><b>Strongly Disagree</b></td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><b>I had a say in the work that I did</b></td>
                                    <td align="center">{{ Form::radio('say_in_what_i_did', 'Strongly Agree') }}</td>
                                    <td align="center">{{ Form::radio('say_in_what_i_did', 'Agree') }}</td>
                                    <td align="center">{{ Form::radio('say_in_what_i_did', 'Neither Agree nor Disagree') }}</td>
                                    <td align="center">{{ Form::radio('say_in_what_i_did', 'Disagree') }}</td>
                                    <td align="center">{{ Form::radio('say_in_what_i_did', 'Strongly Disagree') }}</td>
                                </tr>
                                <tr>
                                    <td><b>My workload allowed me to do a quality job</b></td>
                                    <td align="center">{{ Form::radio('workload_quality_job', 'Strongly Agree') }}</td>
                                    <td align="center">{{ Form::radio('workload_quality_job', 'Agree') }}</td>
                                    <td align="center">{{ Form::radio('workload_quality_job', 'Neither Agree nor Disagree') }}</td>
                                    <td align="center">{{ Form::radio('workload_quality_job', 'Disagree') }}</td>
                                    <td align="center">{{ Form::radio('workload_quality_job', 'Strongly Disagree') }}</td>
                                </tr>
                                <tr>
                                    <td><b>I had the proper equipment/resources to do my job</b></td>
                                    <td align="center">{{ Form::radio('proper_resources', 'Strongly Agree') }}</td>
                                    <td align="center">{{ Form::radio('proper_resources', 'Agree') }}</td>
                                    <td align="center">{{ Form::radio('proper_resources', 'Neither Agree nor Disagree') }}</td>
                                    <td align="center">{{ Form::radio('proper_resources', 'Disagree') }}</td>
                                    <td align="center">{{ Form::radio('proper_resources', 'Strongly Disagree') }}</td>
                                </tr>
                                <tr>
                                    <td><b>My supervisor treated me with respect</b></td>
                                    <td align="center">{{ Form::radio('supervisor_respect', 'Strongly Agree') }}</td>
                                    <td align="center">{{ Form::radio('supervisor_respect', 'Agree') }}</td>
                                    <td align="center">{{ Form::radio('supervisor_respect', 'Neither Agree nor Disagree') }}</td>
                                    <td align="center">{{ Form::radio('supervisor_respect', 'Disagree') }}</td>
                                    <td align="center">{{ Form::radio('supervisor_respect', 'Strongly Disagree') }}</td>
                                </tr>
                                <tr>
                                    <td><b>I was recognized when a project was completed</b></td>
                                    <td align="center">{{ Form::radio('project_completed', 'Strongly Agree') }}</td>
                                    <td align="center">{{ Form::radio('project_completed', 'Agree') }}</td>
                                    <td align="center">{{ Form::radio('project_completed', 'Neither Agree nor Disagree') }}</td>
                                    <td align="center">{{ Form::radio('project_completed', 'Disagree') }}</td>
                                    <td align="center">{{ Form::radio('project_completed', 'Strongly Disagree') }}</td>
                                </tr>
                                <tr>
                                    <td><b>My department worked well together</b></td>
                                    <td align="center">{{ Form::radio('department_worked_well', 'Strongly Agree') }}</td>
                                    <td align="center">{{ Form::radio('department_worked_well', 'Agree') }}</td>
                                    <td align="center">{{ Form::radio('department_worked_well', 'Neither Agree nor Disagree') }}</td>
                                    <td align="center">{{ Form::radio('department_worked_well', 'Disagree') }}</td>
                                    <td align="center">{{ Form::radio('department_worked_well', 'Strongly Disagree') }}</td>
                                </tr>
                                <tr>
                                    <td><b>My level of workplace stress was manageable</b></td>
                                    <td align="center">{{ Form::radio('stress_manageable', 'Strongly Agree') }}</td>
                                    <td align="center">{{ Form::radio('stress_manageable', 'Agree') }}</td>
                                    <td align="center">{{ Form::radio('stress_manageable', 'Neither Agree nor Disagree') }}</td>
                                    <td align="center">{{ Form::radio('stress_manageable', 'Disagree') }}</td>
                                    <td align="center">{{ Form::radio('stress_manageable', 'Strongly Disagree') }}</td>
                                </tr>
                                <tr>
                                    <td><b>Other departments pitched in to help each other</b></td>
                                    <td align="center">{{ Form::radio('other_departments_pitch', 'Strongly Agree') }}</td>
                                    <td align="center">{{ Form::radio('other_departments_pitch', 'Agree') }}</td>
                                    <td align="center">{{ Form::radio('other_departments_pitch', 'Neither Agree nor Disagree') }}</td>
                                    <td align="center">{{ Form::radio('other_departments_pitch', 'Disagree') }}</td>
                                    <td align="center">{{ Form::radio('other_departments_pitch', 'Strongly Disagree') }}</td>
                                </tr>
                                <tr>
                                    <td><b>I had opportunities for professional growth and development</b></td>
                                    <td align="center">{{ Form::radio('opportunities_growth', 'Strongly Agree') }}</td>
                                    <td align="center">{{ Form::radio('opportunities_growth', 'Agree') }}</td>
                                    <td align="center">{{ Form::radio('opportunities_growth', 'Neither Agree nor Disagree') }}</td>
                                    <td align="center">{{ Form::radio('opportunities_growth', 'Disagree') }}</td>
                                    <td align="center">{{ Form::radio('opportunities_growth', 'Strongly Disagree') }}</td>
                                </tr>
                                <tr>
                                    <td><b>I knew what I was supposed to be doing in my position</b></td>
                                    <td align="center">{{ Form::radio('knew_supposed_to_do', 'Strongly Agree') }}</td>
                                    <td align="center">{{ Form::radio('knew_supposed_to_do', 'Agree') }}</td>
                                    <td align="center">{{ Form::radio('knew_supposed_to_do', 'Neither Agree nor Disagree') }}</td>
                                    <td align="center">{{ Form::radio('knew_supposed_to_do', 'Disagree') }}</td>
                                    <td align="center">{{ Form::radio('knew_supposed_to_do', 'Strongly Disagree') }}</td>
                                </tr>
                                <tr>
                                    <td><b>The attitude of employees at the facility towards SGMC was positive</b></td>
                                    <td align="center">{{ Form::radio('attitude_employees', 'Strongly Agree') }}</td>
                                    <td align="center">{{ Form::radio('attitude_employees', 'Agree') }}</td>
                                    <td align="center">{{ Form::radio('attitude_employees', 'Neither Agree nor Disagree') }}</td>
                                    <td align="center">{{ Form::radio('attitude_employees', 'Disagree') }}</td>
                                    <td align="center">{{ Form::radio('attitude_employees', 'Strongly Disagree') }}</td>
                                </tr>
                                <tr>
                                    <td><b>I believe SGMC`s mission will lead to its success</b></td>
                                    <td align="center">{{ Form::radio('believe_mission', 'Strongly Agree') }}</td>
                                    <td align="center">{{ Form::radio('believe_mission', 'Agree') }}</td>
                                    <td align="center">{{ Form::radio('believe_mission', 'Neither Agree nor Disagree') }}</td>
                                    <td align="center">{{ Form::radio('believe_mission', 'Disagree') }}</td>
                                    <td align="center">{{ Form::radio('believe_mission', 'Strongly Disagree') }}</td>
                                </tr>
                                <tr>
                                    <td><b>Overall, I enjoyed working at SGMC</b></td>
                                    <td align="center">{{ Form::radio('enjoyed_sgmc', 'Strongly Agree') }}</td>
                                    <td align="center">{{ Form::radio('enjoyed_sgmc', 'Agree') }}</td>
                                    <td align="center">{{ Form::radio('enjoyed_sgmc', 'Neither Agree nor Disagree') }}</td>
                                    <td align="center">{{ Form::radio('enjoyed_sgmc', 'Disagree') }}</td>
                                    <td align="center">{{ Form::radio('enjoyed_sgmc', 'Strongly Disagree') }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>





                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Please rate the below benifit plan from "Excellent" to "Poor".  If you were not enrolled in the plan simply select "N/A"
                        </div>
                        <div class="panel-body">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <td align="center"><b>Question</b></td>
                                    <td align="center"><b>Excellent</b></td>
                                    <td align="center"><b>Good</b></td>
                                    <td align="center"><b>Fair</b></td>
                                    <td align="center"><b>Poor</b></td>
                                    <td align="center"><b>N/A</b></td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><b>Medical Coverage</b></td>
                                    <td align="center">{{ Form::radio('medical', 'Excellent') }}</td>
                                    <td align="center">{{ Form::radio('medical', 'Good') }}</td>
                                    <td align="center">{{ Form::radio('medical', 'Fair') }}</td>
                                    <td align="center">{{ Form::radio('medical', 'Poor') }}</td>
                                    <td align="center">{{ Form::radio('medical', 'N/A') }}</td>
                                </tr>
                                <tr>
                                    <td><b>Dental Coverage</b></td>
                                    <td align="center">{{ Form::radio('dental', 'Excellent') }}</td>
                                    <td align="center">{{ Form::radio('dental', 'Good') }}</td>
                                    <td align="center">{{ Form::radio('dental', 'Fair') }}</td>
                                    <td align="center">{{ Form::radio('dental', 'Poor') }}</td>
                                    <td align="center">{{ Form::radio('dental', 'N/A') }}</td>
                                </tr>
                                <tr>
                                    <td><b>Vision Coverage</b></td>
                                    <td align="center">{{ Form::radio('vision', 'Excellent') }}</td>
                                    <td align="center">{{ Form::radio('vision', 'Good') }}</td>
                                    <td align="center">{{ Form::radio('vision', 'Fair') }}</td>
                                    <td align="center">{{ Form::radio('vision', 'Poor') }}</td>
                                    <td align="center">{{ Form::radio('vision', 'N/A') }}</td>
                                </tr>
                                <tr>
                                    <td><b>Paid Time Off (PTO)</b></td>
                                    <td align="center">{{ Form::radio('pto', 'Excellent') }}</td>
                                    <td align="center">{{ Form::radio('pto', 'Good') }}</td>
                                    <td align="center">{{ Form::radio('pto', 'Fair') }}</td>
                                    <td align="center">{{ Form::radio('pto', 'Poor') }}</td>
                                    <td align="center">{{ Form::radio('pto', 'N/A') }}</td>
                                </tr>
                                <tr>
                                    <td><b>401(k)/Retirement Plans</b></td>
                                    <td align="center">{{ Form::radio('retirement_plans', 'Excellent') }}</td>
                                    <td align="center">{{ Form::radio('retirement_plans', 'Good') }}</td>
                                    <td align="center">{{ Form::radio('retirement_plans', 'Fair') }}</td>
                                    <td align="center">{{ Form::radio('retirement_plans', 'Poor') }}</td>
                                    <td align="center">{{ Form::radio('retirement_plans', 'N/A') }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>




                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Please rate the below from 1 being not at all likely to 10 being extremely likely.  Everything in this section is required.
                        </div>
                        <div class="panel-body">
                            <table class="table table-hover bg-danger">
                                <thead>
                                <tr>
                                    <td align="center"><b>Question</b></td>
                                    <td align="center"><b>1</b></td>
                                    <td align="center"><b>2</b></td>
                                    <td align="center"><b>3</b></td>
                                    <td align="center"><b>4</b></td>
                                    <td align="center"><b>5</b></td>
                                    <td align="center"><b>6</b></td>
                                    <td align="center"><b>7</b></td>
                                    <td align="center"><b>8</b></td>
                                    <td align="center"><b>9</b></td>
                                    <td align="center"><b>10</b></td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><b>How likely are you to recommend SGMC as a place for care, with 1 being not at all likely and 10 being extremely likely?</b></td>
                                    <td align="center">{{ Form::radio('recommend_care', '1') }}</td>
                                    <td align="center">{{ Form::radio('recommend_care', '2') }}</td>
                                    <td align="center">{{ Form::radio('recommend_care', '3') }}</td>
                                    <td align="center">{{ Form::radio('recommend_care', '4') }}</td>
                                    <td align="center">{{ Form::radio('recommend_care', '5') }}</td>
                                    <td align="center">{{ Form::radio('recommend_care', '6') }}</td>
                                    <td align="center">{{ Form::radio('recommend_care', '7') }}</td>
                                    <td align="center">{{ Form::radio('recommend_care', '8') }}</td>
                                    <td align="center">{{ Form::radio('recommend_care', '9') }}</td>
                                    <td align="center">{{ Form::radio('recommend_care', '10') }}</td>
                                </tr>
                                <tr>
                                    <td><b>How likely are you to recommend SGMC as a place to work, with 1 being not at all likely and 10 being extremely likely?</b></td>
                                    <td align="center">{{ Form::radio('recommend_work', '1') }}</td>
                                    <td align="center">{{ Form::radio('recommend_work', '2') }}</td>
                                    <td align="center">{{ Form::radio('recommend_work', '3') }}</td>
                                    <td align="center">{{ Form::radio('recommend_work', '4') }}</td>
                                    <td align="center">{{ Form::radio('recommend_work', '5') }}</td>
                                    <td align="center">{{ Form::radio('recommend_work', '6') }}</td>
                                    <td align="center">{{ Form::radio('recommend_work', '7') }}</td>
                                    <td align="center">{{ Form::radio('recommend_work', '8') }}</td>
                                    <td align="center">{{ Form::radio('recommend_work', '9') }}</td>
                                    <td align="center">{{ Form::radio('recommend_work', '10') }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>






                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Please select the best answer below and provide comments.
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                {!! Form::label('work_sgmc_again', 'Would you work for SGMC again?') !!}
                                {!! Form::select('work_sgmc_again', [
                                                    '' => '[Select]',
                                                    'Yes' => 'Yes',
                                                    'No' => 'No',
                                                     ], null, ['class' => 'form-control']) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('work_sgmc_again_comments', 'If no, please comment.') !!}
                                {!! Form::textarea('work_sgmc_again_comments', null, ['class' => 'form-control', 'id'=>'srv_comments', 'rows' => 4]) !!}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Please select the best answer below and provide comments.
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                {!! Form::label('work_supervisor_again', 'Would you work for your supervisor again?') !!}
                                {!! Form::select('work_supervisor_again', [
                                                    '' => '[Select]',
                                                    'Yes' => 'Yes',
                                                    'No' => 'No',
                                                     ], null, ['class' => 'form-control']) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('work_supervisor_again_comments', 'If no, please comment.') !!}
                                {!! Form::textarea('work_supervisor_again_comments', null, ['class' => 'form-control', 'id'=>'work_supervisor_again_comments', 'rows' => 4]) !!}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Please select the best answer below.
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                {!! Form::label('complianceissues', 'Are you aware of any compliance issues within SGMC?') !!}
                                <div class="input-group">
                                    <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                    {!! Form::select('complianceissues', [
                                                        '' => '[Select]',
                                                        'Yes' => 'Yes',
                                                        'No' => 'No',
                                                         ], null, ['class' => 'form-control','Required']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('Are you aware of any compliance issues within SGMC? Comments', 'If Yes, please comment.') !!}
                                {!! Form::textarea('ComplianceComments', null, ['class' => 'form-control', 'id'=>'srv_comments', 'rows' => 4]) !!}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
    {!! Form::close() !!}
@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
