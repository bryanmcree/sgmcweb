{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('HR') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <a href="#" class="btn btn-danger " onclick="window.history.back()">
                    Return to Completed Surveys
                </a>
            </div>
            <div class="panel-body">
                <div id="piechart_3d" style="width: 1200px; height: 500px;"></div>

                <table class="table">
                    <thead>
                        <tr>
                            <td><b>Employee Name</b></td>
                            <td><b>Title</b></td>
                            <td><b>Response</b></td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($detail as $detailresponse)
                        <tr>
                            <td>{{$detailresponse->employee->last_name}}, {{$detailresponse->employee->first_name}}</td>
                            <td>{{$detailresponse->employee->title}}</td>
                            <td>{{$detailresponse->$fieldname}}</td>
                            <td>[view survey]</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">

    google.charts.load("current", {packages:["corechart"]});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {

        var record={!! json_encode($chartdata) !!};
        console.log(record);
        // Create our data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Source');
        data.addColumn('number', 'Total_Signup');
        for(var k in record){
            var v = record[k];

            data.addRow([k,v]);
            console.log(v);
        }
        var options = {
            //title: 'My Daily Activities',
            is3D: true,
        };
        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        chart.draw(data, options);
    }

</script>

@endsection
@endif