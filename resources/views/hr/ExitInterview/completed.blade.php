{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('HR') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                Completed Exit Surveys
            </div>
            <div class="panel-body">

                <div class="col-md-2">
                    <div class="form-horizontal">
                        {!! Form::open(array('url' => '/hr/exit/report', 'method' => 'POST', 'files' => true,'class' => 'form_control')) !!}
                        {!! Form::label('start_date', 'Start Date:') !!}
                        {!! Form::input('date','start_date', '', ['class' => 'form-control','required']) !!}
                        <BR>
                        {!! Form::label('end_date', 'End Date:') !!}
                        {!! Form::input('date','end_date', '', ['class' => 'form-control','required']) !!}
                        <BR>
                        <div class="RightLeft">
                            {!! Form::reset('Clear Form', array('class' => 'btn btn-default')) !!}
                            {!! Form::submit('Run Report', array('class' => 'btn btn-sgmc')) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <br>
                </div>

                <table class="table table-hover table-responsive table-bordered" id="surveys">
                    <thead>
                    <tr>
                        <td class="hidden-xs"><b>Employee Number</b></td>
                        <td class="hidden-xs"><b>Employee Name</b></td>
                        <td class="hidden-xs"><b>Title</b></td>
                        <td class="hidden-xs"><b>Completed At</b></td>
                        <td class='no-sort' align="center"></td>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($surveys as $survey)

                        <tr>
                            <td class="hidden-xs hidden-sm">{{$survey->employee_number}}</td>
                            <td class="hidden-xs hidden-sm">{{$survey->employee->first_name}} {{$survey->employee->last_name}}</td>
                            <td class="hidden-xs hidden-sm">{{$survey->employee->title}}</td>
                            <td class="hidden-xs hidden-sm">{{$survey->created_at}}</td>
                            <td align="center" style="vertical-align:middle"><a href="/hr/exit/view/{{$survey->id}}/edit" role="button" class="btn btn-sgmc btn-xs SecurityRoles" ><span class="fa fa-btn fa-user" aria-hidden="true"></span> View</a></td>
                        </tr>

                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
@endif