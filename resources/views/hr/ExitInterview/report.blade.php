{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('HR') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <a href="/hr/exit/complete" class="btn btn-danger hidden-xs hidden-sm hidden-md">
                    Return to Completed Surveys
                </a>
            </div>

            <div class="panel-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <b>Total Completed Surveys: {{$TotalCompletedSurveys}}</b>
                            </div>
                            <div class="panel-body">

                                <br> Between {{$start_date}} and {{$end_date}}

                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="/hr/exit/detail/tenure?start_date={{$start_date}}&end_date={{$end_date}}" class="btn btn-primary btn-block">
                                    How long have you worked for SGMC?
                                </a>
                            </div>
                            <div class="panel-body">

                                <table class="table table-bordered">
                                    <tbody>
                                    @foreach($tenure as $tenureresults)
                                        <tr>
                                            <td>{{$tenureresults->tenure}}</td>
                                            <td align="center">{{$tenureresults->tenuretotal}}</td>
                                            <td align="right">{{ROUND(($tenureresults->tenuretotal / $TotalCompletedSurveys)*100,2)}}%</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="/hr/exit/detail/emp_status?start_date={{$start_date}}&end_date={{$end_date}}" class="btn btn-primary btn-block">
                                    What was your most recent employment status?
                                </a>
                            </div>
                            <div class="panel-body">

                                <table class="table table-bordered">
                                    <tbody>
                                    @foreach($emp_status as $empstatusresults)
                                        <tr>
                                            <td>{{$empstatusresults->emp_status}}</td>
                                            <td align="center">{{$empstatusresults->empstatustotal}}</td>
                                            <td align="right">{{ROUND(($empstatusresults->empstatustotal / $TotalCompletedSurveys)*100,2)}}%</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="/hr/exit/detail/reason_for_leaving?start_date={{$start_date}}&end_date={{$end_date}}" class="btn btn-primary btn-block">
                                    What is your reason for leaving SGMC?
                                </a>
                            </div>
                            <div class="panel-body">

                                <table class="table table-bordered">
                                    <tbody>
                                    @foreach($reason_for_leaving as $reasonforleavingresults)
                                        <tr>
                                            <td>{{$reasonforleavingresults->reason_for_leaving}}</td>
                                            <td align="center">{{$reasonforleavingresults->reasonforleavingtotal}}</td>
                                            <td align="right">{{ROUND(($reasonforleavingresults->reasonforleavingtotal / $TotalCompletedSurveys)*100,2)}}%</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="/hr/exit/detail/work_related_addressed?start_date={{$start_date}}&end_date={{$end_date}}" class="btn btn-primary btn-block">
                                    If work-related, did you try and have your concern addressed?
                                </a>
                            </div>
                            <div class="panel-body">

                                <table class="table table-bordered">
                                    <tbody>
                                    @foreach($work_related_addressed as $workrelatedaddressedresults)
                                        <tr>
                                            <td>{{$workrelatedaddressedresults->work_related_addressed}}</td>
                                            <td align="center">{{$workrelatedaddressedresults->workrelatedaddressedtotal}}</td>
                                            <td align="right">{{ROUND(($workrelatedaddressedresults->workrelatedaddressedtotal / $TotalCompletedSurveys)*100,2)}}%</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="/hr/exit/detail/like_least_about_job?start_date={{$start_date}}&end_date={{$end_date}}" class="btn btn-primary btn-block">
                                    What did you like least about your job?
                                </a>
                            </div>
                            <div class="panel-body">

                                <table class="table table-bordered">
                                    <tbody>
                                    @foreach($like_least_about_job as $like_least_about_job_results)
                                        <tr>
                                            <td>{{$like_least_about_job_results->like_least_about_job}}</td>
                                            <td align="center">{{$like_least_about_job_results->like_least_about_job_total}}</td>
                                            <td align="right">{{ROUND(($like_least_about_job_results->like_least_about_job_total / $TotalCompletedSurveys)*100,2)}}%</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="/hr/exit/detail/like_most_about_job?start_date={{$start_date}}&end_date={{$end_date}}" class="btn btn-primary btn-block">
                                    What did you like most about working for SGMC?
                                </a>
                            </div>
                            <div class="panel-body">

                                <table class="table table-bordered">
                                    <tbody>
                                    @foreach($like_most_about_job as $like_most_about_job_results)
                                        <tr>
                                            <td>{{$like_most_about_job_results->like_most_about_job}}</td>
                                            <td align="center">{{$like_most_about_job_results->like_most_about_job_total}}</td>
                                            <td align="right">{{ROUND(($like_most_about_job_results->like_most_about_job_total / $TotalCompletedSurveys)*100,2)}}%</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="/hr/exit/detail/say_in_what_i_did?start_date={{$start_date}}&end_date={{$end_date}}" class="btn btn-primary btn-block">
                                    I had a say in the work that I did
                                </a>
                            </div>
                            <div class="panel-body">

                                <table class="table table-bordered">
                                    <tbody>
                                    @foreach($say_in_what_i_did as $say_in_what_i_did_results)
                                        <tr>
                                            <td>{{$say_in_what_i_did_results->say_in_what_i_did}}</td>
                                            <td align="center">{{$say_in_what_i_did_results->say_in_what_i_did_total}}</td>
                                            <td align="right">{{ROUND(($say_in_what_i_did_results->say_in_what_i_did_total / $TotalCompletedSurveys)*100,2)}}%</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="/hr/exit/detail/workload_quality_job?start_date={{$start_date}}&end_date={{$end_date}}" class="btn btn-primary btn-block">
                                    My workload allowed me to do a quality job
                                </a>
                            </div>
                            <div class="panel-body">

                                <table class="table table-bordered">
                                    <tbody>
                                    @foreach($workload_quality_job as $workload_quality_job_results)
                                        <tr>
                                            <td>{{$workload_quality_job_results->workload_quality_job}}</td>
                                            <td align="center">{{$workload_quality_job_results->workload_quality_job_total}}</td>
                                            <td align="right">{{ROUND(($workload_quality_job_results->workload_quality_job_total / $TotalCompletedSurveys)*100,2)}}%</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="/hr/exit/detail/proper_resources?start_date={{$start_date}}&end_date={{$end_date}}" class="btn btn-primary btn-block">
                                    I had the proper equipment/resources to do my job
                                </a>
                            </div>
                            <div class="panel-body">

                                <table class="table table-bordered">
                                    <tbody>
                                    @foreach($proper_resources as $proper_resources_results)
                                        <tr>
                                            <td>{{$proper_resources_results->proper_resources}}</td>
                                            <td align="center">{{$proper_resources_results->proper_resources_total}}</td>
                                            <td align="right">{{ROUND(($proper_resources_results->proper_resources_total / $TotalCompletedSurveys)*100,2)}}%</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="/hr/exit/detail/supervisor_respect?start_date={{$start_date}}&end_date={{$end_date}}" class="btn btn-primary btn-block">
                                    My supervisor treated me with respect
                                </a>
                            </div>
                            <div class="panel-body">

                                <table class="table table-bordered">
                                    <tbody>
                                    @foreach($supervisor_respect as $supervisor_respect_results)
                                        <tr>
                                            <td>{{$supervisor_respect_results->supervisor_respect}}</td>
                                            <td align="center">{{$supervisor_respect_results->supervisor_respect_total}}</td>
                                            <td align="right">{{ROUND(($supervisor_respect_results->supervisor_respect_total / $TotalCompletedSurveys)*100,2)}}%</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="/hr/exit/detail/project_completed?start_date={{$start_date}}&end_date={{$end_date}}" class="btn btn-primary btn-block">
                                    I was recognized when a project was completed
                                </a>
                            </div>
                            <div class="panel-body">

                                <table class="table table-bordered">
                                    <tbody>
                                    @foreach($project_completed as $project_completed_results)
                                        <tr>
                                            <td>{{$project_completed_results->project_completed}}</td>
                                            <td align="center">{{$project_completed_results->project_completed_total}}</td>
                                            <td align="right">{{ROUND(($project_completed_results->project_completed_total / $TotalCompletedSurveys)*100,2)}}%</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="/hr/exit/detail/department_worked_well?start_date={{$start_date}}&end_date={{$end_date}}" class="btn btn-primary btn-block">
                                    My department worked well together
                                </a>
                            </div>
                            <div class="panel-body">

                                <table class="table table-bordered">
                                    <tbody>
                                    @foreach($department_worked_well as $department_worked_well_results)
                                        <tr>
                                            <td>{{$department_worked_well_results->department_worked_well}}</td>
                                            <td align="center">{{$department_worked_well_results->department_worked_well_total}}</td>
                                            <td align="right">{{ROUND(($department_worked_well_results->department_worked_well_total / $TotalCompletedSurveys)*100,2)}}%</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="/hr/exit/detail/stress_manageable?start_date={{$start_date}}&end_date={{$end_date}}" class="btn btn-primary btn-block">
                                    My level of workplace stress was manageable
                                </a>
                            </div>
                            <div class="panel-body">

                                <table class="table table-bordered">
                                    <tbody>
                                    @foreach($stress_manageable as $stress_manageable_results)
                                        <tr>
                                            <td>{{$stress_manageable_results->stress_manageable}}</td>
                                            <td align="center">{{$stress_manageable_results->stress_manageable_total}}</td>
                                            <td align="right">{{ROUND(($stress_manageable_results->stress_manageable_total / $TotalCompletedSurveys)*100,2)}}%</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="/hr/exit/detail/other_departments_pitch?start_date={{$start_date}}&end_date={{$end_date}}" class="btn btn-primary btn-block">
                                    Other departments pitched in to help each other
                                </a>
                            </div>
                            <div class="panel-body">

                                <table class="table table-bordered">
                                    <tbody>
                                    @foreach($other_departments_pitch as $other_departments_pitch_results)
                                        <tr>
                                            <td>{{$other_departments_pitch_results->other_departments_pitch}}</td>
                                            <td align="center">{{$other_departments_pitch_results->other_departments_pitch_total}}</td>
                                            <td align="right">{{ROUND(($other_departments_pitch_results->other_departments_pitch_total / $TotalCompletedSurveys)*100,2)}}%</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="/hr/exit/detail/opportunities_growth?start_date={{$start_date}}&end_date={{$end_date}}" class="btn btn-primary btn-block">
                                    I had opportunities for professional growth and development
                                </a>
                            </div>
                            <div class="panel-body">

                                <table class="table table-bordered">
                                    <tbody>
                                    @foreach($opportunities_growth as $opportunities_growth_results)
                                        <tr>
                                            <td>{{$opportunities_growth_results->opportunities_growth}}</td>
                                            <td align="center">{{$opportunities_growth_results->opportunities_growth_total}}</td>
                                            <td align="right">{{ROUND(($opportunities_growth_results->opportunities_growth_total / $TotalCompletedSurveys)*100,2)}}%</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="/hr/exit/detail/knew_supposed_to_do?start_date={{$start_date}}&end_date={{$end_date}}" class="btn btn-primary btn-block">
                                    I knew what I was supposed to be doing in my position
                                </a>
                            </div>
                            <div class="panel-body">

                                <table class="table table-bordered">
                                    <tbody>
                                    @foreach($knew_supposed_to_do as $knew_supposed_to_do_results)
                                        <tr>
                                            <td>{{$knew_supposed_to_do_results->knew_supposed_to_do}}</td>
                                            <td align="center">{{$knew_supposed_to_do_results->knew_supposed_to_do_total}}</td>
                                            <td align="right">{{ROUND(($knew_supposed_to_do_results->knew_supposed_to_do_total / $TotalCompletedSurveys)*100,2)}}%</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="/hr/exit/detail/attitude_employees?start_date={{$start_date}}&end_date={{$end_date}}" class="btn btn-primary btn-block">
                                    The attitude of employees at the facility towards SGMC was positive
                                </a>
                            </div>
                            <div class="panel-body">

                                <table class="table table-bordered">
                                    <tbody>
                                    @foreach($attitude_employees as $attitude_employees_results)
                                        <tr>
                                            <td>{{$attitude_employees_results->attitude_employees}}</td>
                                            <td align="center">{{$attitude_employees_results->attitude_employees_total}}</td>
                                            <td align="right">{{ROUND(($attitude_employees_results->attitude_employees_total / $TotalCompletedSurveys)*100,2)}}%</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="/hr/exit/detail/believe_mission?start_date={{$start_date}}&end_date={{$end_date}}" class="btn btn-primary btn-block">
                                    I believe SGMC`s mission will lead to its success
                                </a>
                            </div>
                            <div class="panel-body">

                                <table class="table table-bordered">
                                    <tbody>
                                    @foreach($believe_mission as $believe_mission_results)
                                        <tr>
                                            <td>{{$believe_mission_results->believe_mission}}</td>
                                            <td align="center">{{$believe_mission_results->believe_mission_total}}</td>
                                            <td align="right">{{ROUND(($believe_mission_results->believe_mission_total / $TotalCompletedSurveys)*100,2)}}%</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="/hr/exit/detail/enjoyed_sgmc?start_date={{$start_date}}&end_date={{$end_date}}" class="btn btn-primary btn-block">
                                    Overall, I enjoyed working at SGMC
                                </a>
                            </div>
                            <div class="panel-body">

                                <table class="table table-bordered">
                                    <tbody>
                                    @foreach($enjoyed_sgmc as $enjoyed_sgmc_results)
                                        <tr>
                                            <td>{{$enjoyed_sgmc_results->enjoyed_sgmc}}</td>
                                            <td align="center">{{$enjoyed_sgmc_results->enjoyed_sgmc_total}}</td>
                                            <td align="right">{{ROUND(($enjoyed_sgmc_results->enjoyed_sgmc_total / $TotalCompletedSurveys)*100,2)}}%</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>




            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
@endif