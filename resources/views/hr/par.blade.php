@extends('layouts.app')

@section('content')
    @include('hr.navbar')

    <div class="col-md-5">
        <div class="panel panel-default">
            <div class="panel-heading">
                <b>Employee Name (Last, First, M)</b>
            </div>
            <div class="panel-body">
                McRee, Bryan, E
            </div>
        </div>
    </div>

    <div class="col-md-2">
        <div class="panel panel-default">
            <div class="panel-heading" style="text-align: center">
                <b>Employee Number</b>
            </div>
            <div class="panel-body" style="text-align: center">
                19745
            </div>
        </div>
    </div>

    <div class="col-md-5">
        <div class="panel panel-default">
            <div class="panel-heading" style="text-align: right">
                <b>Effective Date of Transaction</b>
            </div>
            <div class="panel-body" style="text-align: right">
                {{Carbon\Carbon::now()->format('l, F j Y')}}
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class = "panel-heading">
                <b>EMPLOYMENT ACTIVITY SECTION</b>
            </div>
            <div class="panel-body">

                <div class="col-md-1">
                    <div class="form-group">
                        {!! Form::label('transition', 'Transition Type:') !!}
                        {!! Form::select('transition', [
                                            '' => 'Select',
                                            'Termination' => 'Termination',
                                            'Resignation' => 'Resignation',
                                            'Salary Change' => 'Salary Change',
                                            'Leave of Absence' => 'Leave of Absence',
                                             ], null, ['class' => 'form-control','id'=>'transition', 'onchange'=>'disable("transition")']) !!}
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                        {!! Form::label('rehire', 'Eligible for Rehire?') !!}
                        {!! Form::select('rehire', [
                                            '' => 'Select',
                                            'Yes' => 'Yes',
                                            'No' => 'No',
                                             ], null, ['class' => 'form-control','id'=>'rehire','disabled'=>'']) !!}
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                        {!! Form::label('pay_benifits', 'Pay Out Benefits?') !!}
                        {!! Form::select('pay_benifits', [
                                            '' => 'Select',
                                            'Yes' => 'Yes',
                                            'No' => 'No',
                                             ], null, ['class' => 'form-control','id'=>'pay_benifits','disabled'=>'']) !!}
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                        {!! Form::label('adequate_notice', 'Adequate Notice') !!}
                        {!! Form::select('adequate_notice', [
                                            '' => 'Select',
                                            'Yes' => 'Yes',
                                            'No' => 'No',
                                             ], null, ['class' => 'form-control','id'=>'adequate_notice','disabled'=>'']) !!}
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                        <div class="form-group">
                            {!! Form::label('last_day_worked', 'Last Day Worked:') !!}
                            {{ Form::input('date', 'last_day_worked', null, ['class' => 'form-control', 'id'=>'last_day_worked','disabled'=>'']) }}
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <div class="form-group">
                            {!! Form::label('notice_date', 'Employee Notice Date:') !!}
                            {{ Form::input('date', 'notice_date', null, ['class' => 'form-control', 'id'=>'notice_date','disabled'=>'']) }}
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('reason_resignation', 'Reason for Resignation:') !!}
                        {!! Form::select('reason_resignation', [
                                            '' => 'Select',
                                            'Advanced Opportunity - R002' => 'Advanced Opportunity - R002',
                                            'Disliked Benefits - R004' => 'Disliked Benefits - R004',
                                            'Disliked Compensation - R006' => 'Disliked Compensation - R006',
                                            'Job Security - R008' => 'Job Security - R008',
                                            'Disliked Commute - R010' => 'Disliked Commute - R010',
                                            'Lack of Training - R012' => 'Lack of Training - R012',
                                            'Stay at Home - R014' => 'Stay at Home - R014',
                                            'Retirement - R018' => 'Retirement - R018',
                                            'Continue Education - R022' => 'Continue Education - R022',
                                            'Health Concerns - R024' => 'Health Concerns - R024',
                                            'Join Military - R026' => 'Join Military - R026',
                                            'Military Transfer - R028' => 'Military Transfer - R028',
                                            'Family Obligations - R030' => 'Family Obligations - R030',
                                            'Dislike Schedule - R038' => 'Dislike Schedule - R038',
                                            'other' => 'Other - Please Specify',
                                             ], null, ['class' => 'form-control','id'=>'reason_resignation','disabled'=>'']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('other_reason_resignation', 'Other:') !!}
                        {!! Form::text('other_reason_resignation', null, ['class' => 'form-control','id'=>'other_reason_resignation', 'disabled'=>'']) !!}
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('reason_salary_change', 'Reason for Salary Change:') !!}
                        {!! Form::select('reason_salary_change', [
                                            '' => 'Select',
                                            'Merit Increase' => 'Merit Increase',
                                            'Adjustment' => 'Adjustment',
                                            'Transfer' => 'Transfer',
                                            'General Increase/Decrease' => 'General Increase/Decrease',
                                            'Pass Cert/Boards' => 'Pass Cert/Boards',
                                            'Certification' => 'Certification',
                                            'other' => 'Other',
                                             ], null, ['class' => 'form-control','id'=>'reason_salary_change', 'disabled'=>'']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('pay_type', 'Pay Type:') !!}
                        {!! Form::text('pay_type', null, ['class' => 'form-control','id'=>'pay_type', 'disabled'=>'']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('other_reason_salary_change', 'Other:') !!}
                        {!! Form::text('other_reason_salary_change', null, ['class' => 'form-control','id'=>'other_reason_salary_change', 'disabled'=>'']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <div class="form-group">
                            {!! Form::label('expiration_date', 'Expiration Date:') !!}
                            {{ Form::input('date', 'expiration_date', null, ['class' => 'form-control', 'id'=>'expiration_date','disabled'=>'']) }}
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('reason_leave_absence', 'Reason Leave of Absence:') !!}
                        {!! Form::select('reason_leave_absence', [
                                            '' => 'Select',
                                            'Family (FMLA)' => 'Family (FMLA)',
                                            'Medical' => 'Medical',
                                            'Military' => 'Military',
                                            'Educational' => 'Educational',
                                            'Personal' => 'Personal',
                                            'Return from Leave' => 'Return from Leave',
                                            'Other' => 'Other',
                                             ], null, ['class' => 'form-control','id'=>'reason_leave_absence', 'disabled'=>'']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        {!! Form::label('other_reason_leave_absence', 'Other:') !!}
                        {!! Form::text('other_reason_leave_absence', null, ['class' => 'form-control','id'=>'other_reason_leave_absence', 'disabled'=>'']) !!}
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <B>CHANGE OF STATUS SECTION</B>
            </div>
                <div class="panel-body">

                        <div class="col-md-12">
                            <div class="form-group">
                                {!! Form::label('change_status', 'Change of Status:') !!}
                                {!! Form::select('reason_leave_absence', [
                                                    '' => 'Select',
                                                    'New Hire' => 'New Hire',
                                                    'Re-Hire' => 'Re-Hire',
                                                    'Demotion' => 'Demotion',
                                                    'Promotion' => 'Promotion',
                                                    'Transfer' => 'Transfer',
                                                    'Status Change' => 'Status Change',
                                                     ], null, ['class' => 'form-control','id'=>'reason_leave_absence']) !!}
                            </div>
                        </div>
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">Current Information</div>
                                <div class="panel-body">

                                </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">NEW Information</div>
                            <div class="panel-body">

                            </div>
                        </div>
                    </div>

                </div>
        </div>

    </div>








    <script type="text/javascript">
        function disable(select_val) {
            var e = document.getElementById(select_val);
            var strUser = e.options[e.selectedIndex].value;

            if(strUser === "Salary Change") {
                document.getElementById("reason_salary_change").disabled = false;
                document.getElementById("pay_type").disabled = false;
                document.getElementById("other_reason_salary_change").disabled = false;
                document.getElementById("expiration_date").disabled = false;
                document.getElementById("rehire").disabled = true;
                document.getElementById("pay_benifits").disabled = true;
                document.getElementById("adequate_notice").disabled = true;
                document.getElementById("last_day_worked").disabled = true;
                document.getElementById("notice_date").disabled = true;
                document.getElementById("reason_resignation").disabled = true;
                document.getElementById("other_reason_resignation").disabled = true;
                document.getElementById("reason_leave_absence").disabled = true;
                document.getElementById("other_reason_leave_absence").disabled = true;
            } else if(strUser === "Termination") {
                document.getElementById("rehire").disabled = false;
                document.getElementById("pay_benifits").disabled = false;
                document.getElementById("adequate_notice").disabled = false;
                document.getElementById("last_day_worked").disabled = false;
                document.getElementById("notice_date").disabled = false;
                document.getElementById("reason_resignation").disabled = false;
                document.getElementById("other_reason_resignation").disabled = false;
                document.getElementById("reason_salary_change").disabled = true;
                document.getElementById("pay_type").disabled = true;
                document.getElementById("other_reason_salary_change").disabled = true;
                document.getElementById("expiration_date").disabled = true;
                document.getElementById("reason_leave_absence").disabled = true;
                document.getElementById("other_reason_leave_absence").disabled = true;
            } else if(strUser === "Resignation") {
                document.getElementById("rehire").disabled = false;
                document.getElementById("pay_benifits").disabled = false;
                document.getElementById("adequate_notice").disabled = false;
                document.getElementById("last_day_worked").disabled = false;
                document.getElementById("notice_date").disabled = false;
                document.getElementById("reason_resignation").disabled = false;
                document.getElementById("other_reason_resignation").disabled = false;
                document.getElementById("reason_salary_change").disabled = true;
                document.getElementById("pay_type").disabled = true;
                document.getElementById("other_reason_salary_change").disabled = true;
                document.getElementById("expiration_date").disabled = true;
                document.getElementById("reason_leave_absence").disabled = true;
                document.getElementById("other_reason_leave_absence").disabled = true;
            } else if(strUser === "Leave of Absence") {
                document.getElementById("reason_salary_change").disabled = true;
                document.getElementById("rehire").disabled = true;
                document.getElementById("pay_benifits").disabled = true;
                document.getElementById("adequate_notice").disabled = true;
                document.getElementById("last_day_worked").disabled = true;
                document.getElementById("notice_date").disabled = true;
                document.getElementById("reason_resignation").disabled = true;
                document.getElementById("other_reason_resignation").disabled = true;
                document.getElementById("pay_type").disabled = true;
                document.getElementById("other_reason_salary_change").disabled = true;
                document.getElementById("expiration_date").disabled = true;
                document.getElementById("reason_leave_absence").disabled = false;
                document.getElementById("other_reason_leave_absence").disabled = false;
            } else{
                document.getElementById("reason_salary_change").disabled = true;
                document.getElementById("rehire").disabled = true;
                document.getElementById("pay_benifits").disabled = true;
                document.getElementById("adequate_notice").disabled = true;
                document.getElementById("last_day_worked").disabled = true;
                document.getElementById("notice_date").disabled = true;
                document.getElementById("reason_resignation").disabled = true;
                document.getElementById("other_reason_resignation").disabled = true;
                document.getElementById("pay_type").disabled = true;
                document.getElementById("other_reason_salary_change").disabled = true;
                document.getElementById("expiration_date").disabled = true;
                document.getElementById("reason_leave_absence").disabled = true;
                document.getElementById("other_reason_leave_absence").disabled = true;
            }

        }
    </script>
@endsection