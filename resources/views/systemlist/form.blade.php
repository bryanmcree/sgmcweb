            <div class="form-group">

                {!! Form::label('system_name', 'System Name:') !!}
                <div class="input-group">
                    <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                {!! Form::text('system_name', null, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('server_name', 'Server Name:') !!}
                {!! Form::text('server_name', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('server_ip_address', 'Server IP Address:') !!}
                {!! Form::text('server_ip_address', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('server_admin_username', 'Server Admin Username:') !!}
                {!! Form::text('server_admin_username', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('server_admin_password', 'Server Admin Password:') !!}
                {!! Form::text('server_admin_password', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('system_description', 'System Description:') !!}
                <div class="input-group">
                    <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                {!! Form::textarea('system_description',null , ['class' => 'form-control', 'size'=>'30x3']) !!}
                    </div>
            </div>
            <div class="form-group">
                {!! Form::label('dept_admin1', 'Department Admin:') !!}
                {!! Form::text('dept_admin1', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('dept_admin1_email', 'Department Admin Email:') !!}
                {!! Form::text('dept_admin1_email', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('system_admin1', 'IS System Admin:') !!}
                <div class="input-group">
                    <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                {!! Form::text('system_admin1', null, ['class' => 'form-control']) !!}
                    </div>
            </div>
            <div class="form-group">
                {!! Form::label('system_admin1_email', 'IS System Admin Email:') !!}
                <div class="input-group">
                    <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                {!! Form::text('system_admin1_email', null, ['class' => 'form-control']) !!}
                    </div>
            </div>
            <div class="form-group">
                {!! Form::label('system_admin2', 'System Admin Secondary:') !!}
                {!! Form::text('system_admin2', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('system_admin2_email', 'System Admin Secondary Email:') !!}
                {!! Form::text('system_admin2_email', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('system_priority', 'System Priority:') !!}
                <div class="input-group">
                    <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                {!! Form::select('system_priority', [
                                    '' => '[Select Priority]',

                                    '1' => 'CRITICAL - The system is critical for the day to day operation of the hospital.
                                    The system contains patient POI and medical data and or employee POI or payroll data.
                                    Access to these systems are strictly limited and monitored.',

                                    '2' => 'Very High - The system is required for the day to day operation of the hospital.
                                    This system relies on other systems to operate and contains patient POI and medical data.
                                    Access to these systems is limited and monitored.',

                                    '3' => 'High - The system is necessary for the day to day operations of the hospital
                                    but not required.  This system works independently from other applications and contains
                                    limited patient POI and medical data.  Access to this system is limited.',

                                    '4' => 'Normal - This system is optional for the day to day operations of the hospital
                                    but is not necessary.  These systems include internal databases and applications that
                                    do not contain patient POI or medical data.  Access to this system is managed through
                                    AD (Active Directory) or other basic password systems.',

                                    '5' => 'Low - This system is not required for the day to day operations of the hospital.
                                    The data or function of this system is internal and does not contain any patient POI
                                    data, medical data, passwords or employee information.',

                                     ], null, ['class' => 'form-control']) !!}
                    </div>
            </div>

