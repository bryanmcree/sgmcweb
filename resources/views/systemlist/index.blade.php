@extends('layouts.app')
@if ( Auth::user()->hasRole('IS') == FALSE)
    @include('layouts.unauthorized')
    @Else

@section('content')
    <div class="card mb-4 border-info text-white bg-dark">
        <div class="card-body">

            <div class="row">
                <a href="#" data-toggle="modal" data-target=".AddSystem"  role="button" class="btn btn-sgmc btn-xs">Add System</a>
            </div>
            <table class="table table-hover table-condensed mb-4 border-info text-white bg-dark" width="100%" id="systems">
                <thead>
                <tr >
                    <td><b>System Name</b></td>
                    <td class="hidden-xs hidden-sm hidden-md"><b>Pri</b></td>
                    <td class="hidden-xs hidden-sm hidden-md"><b>Contact</b></td>
                    <td class="hidden-xs hidden-sm hidden-md"><b>U/A</b></td>
                    <td align="center" class="hidden-xs hidden-sm hidden-md"><b>Contacts</b></td>
                    <td align="center" class="hidden-xs hidden-sm hidden-md"><b>Users</b></td>
                    <td align="center" class="hidden-xs hidden-sm hidden-md"><b>Serv./Dev.</b></td>
                    <td align="center" class="hidden-xs hidden-sm hidden-md"><b>Docs</b></td>
                    <td class="hidden-xs hidden-sm hidden-md hidden-lg"><b>Last Verified</b></td>
                    <td class='no-sort' align="center" ></td>
                </tr>
                </thead>
                <tbody>
                @foreach ($SystemList as $SystemLists)
                    <tr>
                        <td>{{$SystemLists->sys_name}} <div style="display: none"> {{$SystemLists->sys_alias}} {{$SystemLists->serverList()}}</div></td>
                        <td class="hidden-xs hidden-sm hidden-md">
                            @if($SystemLists->system_priority == 1)<i class="fas fa-exclamation-circle" style="color:red"></i>
                            @endif
                            @if($SystemLists->system_priority == 2)<i class="fas fa-exclamation-circle" style="color:orange"></i>
                            @endif
                            @if($SystemLists->system_priority == 3)<i class="fas fa-exclamation-circle" style="color:yellow"></i>
                            @endif
                            @if($SystemLists->system_priority == 4)<i class="fas fa-exclamation-circle" style="color:white"></i>
                            @endif
                            @if($SystemLists->system_priority == 5)<i class="fas fa-exclamation-circle" style="color:green"></i>
                            @endif
                        </td>
                        <td class="hidden-xs hidden-sm hidden-md">@if (!$SystemLists->systemContacts->isEmpty()){{$SystemLists->systemContacts()->first()->last_name}}, {{$SystemLists->systemContacts()->first()->first_name}} @endif</td>
                        <td align="center" class="hidden-xs hidden-sm hidden-md">{{$SystemLists->inv_visiable}}</td>
                        <td align="center" class="hidden-xs hidden-sm hidden-md">{{$SystemLists->systemContacts2()->count()}}</td>
                        <td align="center" class="hidden-xs hidden-sm hidden-md">0</td>
                        <td align="center" class="hidden-xs hidden-sm hidden-md">{{$SystemLists->systemServers()->count()}}</td>
                        <td align="center" class="hidden-xs hidden-sm hidden-md">0</td>
                        <td class="hidden-xs hidden-sm hidden-md hidden-lg">@if (!$SystemLists->systemVerification->isEmpty()){{$SystemLists->systemVerification()->first()->created_at->format('m-d-Y')}} @endif</td>
                        <td align="right" style="vertical-align:middle">

                            <a href="mailto:{{str_replace(',',';',str_replace('"','',$SystemLists->mailContacts()))}}?subject={{$SystemLists->sys_name}} / System List" role="button" data-toggle="tooltip" data-placement="top" title="Email all Contacts" class="btn btn-primary btn-xs hidden-xs hidden-sm hidden-md">
                                <i class="fas fa-user"></i></a>
                            <a href="/systemlist/{{$SystemLists->id}}" role="button" class="btn btn-sgmc btn-xs" data-toggle="tooltip" data-placement="top" title="System Detail"><span class="fa fa-btn fa-info-circle" aria-hidden="true"></span></a>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@include('systemlist.modals.addsystem')



@endsection
@section('scripts')

    <script type="text/javascript">

        $(document).ready(function() {
            oTable = $('#systems').DataTable(
                    {
                        "info":     false,
                        "pageLength": 20,
                        "lengthChange": true,
                        "order": [],
                        "columnDefs": [
                            { targets: 'no-sort', orderable: false }
                        ]
                    }
            );

        });
    </script>
@endif
@endsection