@extends('layouts.app')

@section('content')

    <div class="card mb-3 border-info text-white bg-dark">
        @include('systemlist.navbar')

        <div class="card-header">
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Home</a>
                    <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Servers</a>
                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">SGMC Contacts</a>
                    <a class="nav-item nav-link" id="nav-vendor-tab" data-toggle="tab" href="#nav-vendor" role="tab" aria-controls="nav-vendor" aria-selected="false">Vendor Contacts</a>
                    <a class="nav-item nav-link" id="nav-docs-tab" data-toggle="tab" href="#nav-docs" role="tab" aria-controls="nav-docs" aria-selected="false">Documents</a>
                    <a class="nav-item nav-link" id="nav-history-tab" data-toggle="tab" href="#nav-history" role="tab" aria-controls="nav-history" aria-selected="false">History</a>
                    <a class="nav-item nav-link" id="nav-comments-tab" data-toggle="tab" href="#nav-comments" role="tab" aria-controls="nav-comments" aria-selected="false">Comments</a>
                </div>
            </nav>
        </div>
        <div class="tab-content" id="nav-tabContent">

            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                <div class="card-body">
                    {{$SystemList->sys_description}}
                </div>
            </div> <!-- HOME -->

            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                <div class="card-body">
                    @if ($SystemList->systemContacts->isEmpty())
                        <div class="alert alert-info">You have no contacts.</div> <a href="#AddContact" data-toggle="modal" data-target=".AddContact" data-System_id = "{{$SystemList->id}}" role="button" class="btn btn-sgmc btn-xs">
                            <span class="fa fa-btn fa-plus-circle" aria-hidden="true"></span> Add Contact</a>
                    @else
                        <table class="table table-condensed table-hover table-responsive">
                            <thead class="bg-default header">
                            <tr class="bg-default">
                                <td class=""><b>Name</b></td>
                                <td class="hidden-xs hidden-sm hidden-md"><b>Title</b></td>
                                <td class="hidden-xs hidden-sm hidden-md"><b>Department</b></td>
                                <td class="hidden-xs hidden-sm hidden-md"><b>Type</b></td>
                                <td class="hidden-xs hidden-sm hidden-md"><b>Email</b></td>
                                <td class="hidden-xs hidden-sm hidden-md"><b>Mobile</b></td>
                                <td class="hidden-xs hidden-sm hidden-md"><b>Office</b></td>
                                <td class="" align="right" style="vertical-align:middle"><a href="#AddContact" data-toggle="modal" data-target=".AddContact" data-System_id = "{{$SystemList->id}}" role="button" class="btn btn-sgmc btn-xs">
                                        <span class="fa fa-btn fa-plus-circle" aria-hidden="true"></span> Add Contact</a>
                                </td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($SystemList->systemContacts as $contacts)
                                <tr>
                                    <td class="">{{$contacts->last_name}}, {{$contacts->first_name}}</td>
                                    <td class="hidden-xs hidden-sm hidden-md">{{$contacts->title}}</td>
                                    <td class="hidden-xs hidden-sm hidden-md">{{$contacts->department}}</td>
                                    <td class="hidden-xs hidden-sm hidden-md">{{$contacts->contact_type}}</td>
                                    <td class="hidden-xs hidden-sm hidden-md">{{Html::mailto($contacts->email_address)}}</td>
                                    <td class="hidden-xs hidden-sm hidden-md">{{PhoneNumbers::formatNumber($contacts->mobile_phone)}}</td>
                                    <td class="hidden-xs hidden-sm hidden-md">{{PhoneNumbers::formatNumber($contacts->office_phone)}}</td>
                                    <td class="" align="right" style="vertical-align:middle">
                                        <div class="btn-group" role="group" aria-label="..." >
                                            {!! Form::button('<i class="fas fa-edit"></i>', array('type' => 'submit', 'class'=> 'EditContact btn btn-primary btn-xs' ,
                                            'data-contacts_id'=>$contacts->id,
                                            'data-systems_id'=>$SystemList->id,
                                            'data-first_name'=>$contacts->first_name,
                                            'data-last_name'=>$contacts->last_name,
                                            'data-title'=>$contacts->title,
                                            'data-department'=>$contacts->department,
                                            'data-contact_type'=>$contacts->contact_type,
                                            'data-office_phone'=>$contacts->office_phone,
                                            'data-mobile_phone'=>$contacts->mobile_phone,
                                            'data-email_address'=>$contacts->email_address,
                                            'data-Pri'=>$contacts->Pri,
                                            )) !!}
                                            <a href="#DeleteContact" data-contact-id ="{{$contacts->id}}" data-system-id ="{{$contacts->systems_id}}" role="button" class="DeleteContact btn btn-danger btn-xs hidden-xs hidden-sm hidden-md">
                                                <i class="fas fa-trash-alt"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>
            </div> <!-- CONTACT -->

            <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                <div class="card-body">
                    <table class="table table-condensed table-hover table-responsive">
                        <thead class="bg-success header">
                        <tr class="bg-default">
                            <td class=""><b>Server Name</b></td>
                            <td class="hidden-xs hidden-sm hidden-md"><b>IP (1)</b></td>
                            <td class="hidden-xs hidden-sm hidden-md"><b>Type</b></td>
                            <td class="hidden-xs hidden-sm hidden-md"><b>User/PW (1)</b></td>
                            <td class="hidden-xs hidden-sm hidden-md"><b>User/PW (2)</b></td>
                            <td class="hidden-xs hidden-sm hidden-md"><b>User/PW (3)</b></td>
                            <td class="hidden-xs hidden-sm hidden-md"><b>Updated</b></td>
                            <td colspan="2" align="right" style="vertical-align:middle"><a href="#AddServer" data-toggle="modal" data-target=".AddServer" role="button" class="btn btn-sgmc btn-xs"><span class="fa fa-btn fa-plus-circle" aria-hidden="true"></span> Add Server</a></td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($SystemList->systemServers as $servers)
                            <tr>
                                <td class="">{{$servers->srv_name}}</td>
                                <td class="hidden-xs hidden-sm hidden-md">{{$servers->srv_ip}}</td>
                                <td class="hidden-xs hidden-sm hidden-md">{{$servers->srv_type}}</td>
                                <td class="hidden-xs hidden-sm hidden-md">{{$servers->srv_adminName1}} / {{$servers->srv_adminPassword1}}</td>
                                <td class="hidden-xs hidden-sm hidden-md">{{$servers->srv_adminName2}} / {{$servers->srv_adminPassword2}}</td>
                                <td class="hidden-xs hidden-sm hidden-md">{{$servers->srv_adminName}} / {{$servers->srv_adminPassword}}</td>
                                <td class="hidden-xs hidden-sm hidden-md">{{$servers->updated_at}}</td>
                                <td align="right" style="vertical-align:middle">

                                    <div class="btn-group" role="group" aria-label="..." >
                                        {!! Form::button('<i class="fas fa-edit"></i> Edit ', array('type' => 'submit', 'class'=> 'EditServer btn btn-primary btn-xs' ,
                                        'data-srv_name'=>$servers->srv_name,
                                        'data-srv_os'=>$servers->srv_os,
                                        'data-srv_type'=>$servers->srv_type,
                                        'data-srv_ip'=>$servers->srv_ip,
                                        'data-srv_ip2'=>$servers->srv_ip2,
                                        'data-srv_adminName1'=>$servers->srv_adminName1,
                                        'data-srv_adminName2'=>$servers->srv_adminName2,
                                        'data-srv_adminPassword1'=>$servers->srv_adminPassword1,
                                        'data-srv_adminPassword2'=>$servers->srv_adminPassword2,
                                        'data-srv_admintype1'=>$servers->srv_admintype1,
                                        'data-srv_admintype2'=>$servers->srv_admintype2,
                                        'data-srv_admintype3'=>$servers->srv_admintype3,
                                        'data-srv_adminName'=>$servers->srv_adminName,
                                        'data-srv_adminPassword'=>$servers->srv_adminPassword,
                                        'data-srv_serviceTag'=>$servers->srv_serviceTag,
                                        'data-srv_modelNum'=>$servers->srv_modelNum,
                                        'data-srv_sgmcNum'=>$servers->srv_sgmcNum,
                                        'data-srv_serial'=>$servers->srv_serial,
                                        'data-srv_equType'=>$servers->srv_equType,
                                        'data-srv_BackupRequired'=>$servers->srv_BackupRequired,
                                        'data-srv_software'=>$servers->srv_software,
                                        'data-srv_comments'=>$servers->srv_comments,
                                        'data-srv_id'=>$servers->srv_id,
                                        'data-system_id'=>$servers->srv_id,
                                        'data-srv_vmEnv'=>$servers->srv_vmEnv,


                                        'data-inf_user1'=>$servers->inf_user1,
                                        'data-inf_user2'=>$servers->inf_user2,
                                        'data-inf_user3'=>$servers->inf_user3,
                                        'data-inf_pass1'=>$servers->inf_pass1,
                                        'data-inf_pass2'=>$servers->inf_pass2,
                                        'data-inf_pass3'=>$servers->inf_pass3,
                                        'data-inf_type1'=>$servers->inf_type1,
                                        'data-inf_type2'=>$servers->inf_type2,
                                        'data-inf_type3'=>$servers->inf_type3,





                                        )) !!}
                                        <a href="#" data-server-id ="{{$servers->srv_id}}" data-system-id ="{{$servers->sys_id}}" role="button" class="DeleteServer btn btn-danger btn-xs hidden-xs hidden-sm hidden-md">
                                            <i class="fas fa-trash-alt"></i> Delete</a>
                                    </div>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div> <!-- Server -->

            <div class="tab-pane fade" id="nav-vendor" role="tabpanel" aria-labelledby="nav-vendor-tab">
                <div class="card-body">
                    {{-- Below will be a list of vendors that supplied the equipment, software and any other hardware needed to support this system. --}}





                    <table class="table table-condensed table-hover table-responsive">
                        <thead class="bg-success header">
                        <tr class="bg-default">
                            <td class=""><b>Contact Name</b></td>
                            <td class="hidden-xs hidden-sm hidden-md"><b>Contact Description</b></td>
                            <td class="hidden-xs hidden-sm hidden-md"><b>Company</b></td>
                            <td class="hidden-xs hidden-sm hidden-md"><b>Phone</b></td>
                            <td class="hidden-xs hidden-sm hidden-md"><b>Email</b></td>
                            <td colspan="2" align="right" style="vertical-align:middle"><a href="#AddVendor" data-toggle="modal" data-target=".AddVendor" role="button" class="btn btn-sgmc btn-xs"><span class="fa fa-btn fa-plus-circle" aria-hidden="true"></span> Add Vendor</a></td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($SystemList->systemVendors as $vendors)
                            <tr>
                                <td class="">{{ $vendors->contact_name }}</td>
                                <td class="hidden-xs hidden-sm hidden-md">{{ $vendors->contact_description }}</td>
                                <td class="hidden-xs hidden-sm hidden-md">{{ $vendors->company }}</td>
                                <td class="hidden-xs hidden-sm hidden-md">{{ $vendors->phone1 }}</td>
                                <td class="hidden-xs hidden-sm hidden-md">{{ $vendors->email1 }}</td>
                                {{-- <td align="right" style="vertical-align:middle"> --}}
                                    <td class="" align="right" style="vertical-align:middle">
                                        <div class="btn-group" role="group" aria-label="..." >
                                            {!! Form::button('<i class="fas fa-edit"></i>', array('type' => 'submit', 'class'=> 'EditVendor btn btn-primary btn-xs' ,
                                            'data-vendor_id'=>$vendors->id,
                                            'data-systems_id'=>$SystemList->id,
                                            'data-contact_name'=>$vendors->contact_name,
                                            'data-contact_description'=>$vendors->contact_description,
                                            'data-company'=>$vendors->company,
                                            'data-phone1'=>$vendors->phone1,
                                            'data-email1'=>$vendors->email1,
                                            )) !!}
                                            <a href="#DeleteVendor" data-vendor-id ="{{$vendors->id}}" data-system-id ="{{$vendors->systems_id}}" role="button" class="DeleteVendor btn btn-danger btn-xs hidden-xs hidden-sm hidden-md">
                                                <i class="fas fa-trash-alt"></i></a>
                                        </div>
                                    </td>


                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>









                </div>
            </div> <!-- VENDOR -->

            <div class="tab-pane fade" id="nav-docs" role="tabpanel" aria-labelledby="nav-docs-tab">
                <div class="card-bodyk">

                </div>
            </div> <!-- DOCS -->

            <div class="tab-pane fade" id="nav-history" role="tabpanel" aria-labelledby="nav-history-tab">
                <div class="card-body">
                    <table class="table table-condensed table-hover table-responsive">
                        <thead class="bg-default header">
                        <tr class="bg-default">
                            <td class=""><b>Verified At</b></td>
                            <td class=""><b>Verified By</b></td>
                            <td class="hidden-xs hidden-sm hidden-md"><b>Backup</b></td>
                            <td class="hidden-xs hidden-sm hidden-md"><b>Details</b></td>
                            <td class="hidden-xs hidden-sm hidden-md"><b>Contacts</b></td>
                            <td class="hidden-xs hidden-sm hidden-md"><b>Users</b></td>
                            <td class="hidden-xs hidden-sm hidden-md"><b>Use</b></td>
                            <td class="hidden-xs hidden-sm hidden-md"><b>Notes</b></td>
                        </tr>
                        </thead>
                        @if (!$SystemList->systemVerification->isEmpty())
                            <tbody>
                            @foreach ($SystemList->systemVerification as $verify)
                                <tr>
                                    <td class="">{{$verify->created_at}}</td>
                                    <td class="">{{$verify->username}}</td>
                                    <td class="hidden-xs hidden-sm hidden-md">{{$verify->verify_backup}}</td>
                                    <td class="hidden-xs hidden-sm hidden-md">{{$verify->verify_details}}</td>
                                    <td class="hidden-xs hidden-sm hidden-md">{{$verify->verify_contacts}}</td>
                                    <td class="hidden-xs hidden-sm hidden-md">{{$verify->verify_users}}</td>
                                    <td class="hidden-xs hidden-sm hidden-md">{{$verify->verify_use}}</td>
                                    <td class="hidden-xs hidden-sm hidden-md">{{$verify->verify_notes}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        @endif
                    </table>
                </div>
            </div> <!-- HISTORY -->

            <div class="tab-pane fade" id="nav-comments" role="tabpanel" aria-labelledby="nav-comments-tab">
                <div class="card-body">
                    A list of files to support the system.  Manuals, backup configurations
                </div>
            </div> <!-- COMMENTS -->

        </div>
    </div>



@include('systemlist.modals.editsystem')
@include('systemlist.modals.addcontact')
@include('systemlist.modals.editcontact')
@include('systemlist.modals.addvendor')
@include('systemlist.modals.editvendor')
@include('systemlist.modals.addserver')
@include('systemlist.modals.editserver')
@include('systemlist.modals.verify')

@endsection

@section('scripts')

                    <script type="text/javascript">

                        $().ready(function(){
                            $('.VerifySystem').popover();
                        });

                        $(document).on("click", ".EditContact", function () {
                            //alert($(this).attr("data-id"));

                            $('.first_name').val($(this).attr("data-first_name"));
                            $('.last_name').val($(this).attr("data-last_name"));
                            $('.contact_type').val($(this).attr("data-contact_type"));
                            $('.office_phone').val($(this).attr("data-office_phone"));
                            $('.mobile_phone').val($(this).attr("data-mobile_phone"));
                            $('.email_address').val($(this).attr("data-email_address"));
                            $('.title').val($(this).attr("data-title"));
                            $('.department').val($(this).attr("data-department"));
                            $('.Pri').val($(this).attr("data-Pri"));
                            $('.systems_id').val($(this).attr("data-systems_id"));
                            $('.contacts_id').val($(this).attr("data-contacts_id"));
                            $('#EditContact_modal').modal('show');
                            //alert($(this).attr("data-first_name"));
                        });

                        $(document).on("click", ".EditVendor", function () {
                            //alert($(this).attr("data-id"));

                            $('.contact_name').val($(this).attr("data-contact_name"));
                            $('.contact_description').val($(this).attr("data-contact_description"));
                            $('.company').val($(this).attr("data-company"));
                            $('.phone1').val($(this).attr("data-phone1"));
                            $('.email1').val($(this).attr("data-email1"));
                            $('.systems_id').val($(this).attr("data-systems_id"));
                            $('.vendor_id').val($(this).attr("data-vendor_id"));
                            $('#EditVendor_modal').modal('show');
                            //alert($(this).attr("data-first_name"));
                        });

                        $(document).on("click", ".EditServer", function () {
                            //alert('Edit Server Button Clicked');
                            $('.srv_name').val($(this).attr("data-srv_name"));
                            $('.srv_os').val($(this).attr("data-srv_os"));
                            $('.srv_type').val($(this).attr("data-srv_type"));
                            $('.srv_ip').val($(this).attr("data-srv_ip"));
                            $('.srv_ip2').val($(this).attr("data-srv_ip2"));
                            $('.srv_adminName1').val($(this).attr("data-srv_adminName1"));
                            $('.srv_adminName2').val($(this).attr("data-srv_adminName2"));
                            $('.srv_adminPassword1').val($(this).attr("data-srv_adminPassword1"));
                            $('.srv_adminPassword2').val($(this).attr("data-srv_adminPassword2"));
                            $('.srv_admintype1').val($(this).attr("data-srv_admintype1"));
                            $('.srv_admintype2').val($(this).attr("data-srv_admintype2"));
                            $('.srv_admintype3').val($(this).attr("data-srv_admintype3"));
                            $('.srv_adminName').val($(this).attr("data-srv_adminName"));
                            $('.srv_adminPassword').val($(this).attr("data-srv_adminPassword"));
                            $('.srv_serviceTag').val($(this).attr("data-srv_serviceTag"));
                            $('.srv_modelNum').val($(this).attr("data-srv_modelNum"));
                            $('.srv_sgmcNum').val($(this).attr("data-srv_sgmcNum"));
                            $('.srv_serial').val($(this).attr("data-srv_serial"));
                            $('.srv_equType').val($(this).attr("data-srv_equType"));
                            $('.srv_BackupRequired').val($(this).attr("data-srv_BackupRequired"));
                            $('.srv_software').val($(this).attr("data-srv_software"));
                            $('.srv_comments').val($(this).attr("data-srv_comments"));
                            $('.srv_id').val($(this).attr("data-srv_id"));
                            $('.system_id').val($(this).attr("data-system_id"));
                            $('.srv_vmEnv').val($(this).attr("data-srv_vmEnv"));

                            $('.inf_user1').val($(this).attr("data-inf_user1"));
                            $('.inf_user2').val($(this).attr("data-inf_user2"));
                            $('.inf_user3').val($(this).attr("data-inf_user3"));
                            $('.inf_pass1').val($(this).attr("data-inf_pass1"));
                            $('.inf_pass2').val($(this).attr("data-inf_pass2"));
                            $('.inf_pass3').val($(this).attr("data-inf_pass3"));
                            $('.inf_type1').val($(this).attr("data-inf_type1"));
                            $('.inf_type2').val($(this).attr("data-inf_type2"));
                            $('.inf_type3').val($(this).attr("data-inf_type3"));

                            $('#EditServer_modal').modal('show');
                            //alert($(this).attr("data-srv_ip"));
                        });

                        $(document).on("click", ".VerifySystem", function () {
                            //alert('Edit Server Button Clicked');
                            $('.systems_id').val($(this).attr("data-id"));
                            //alert($(this).attr("data-id"));
                            $('#VerifySystem_modal').modal('show');

                        });

                        $(document).on("click", ".DeleteContact", function () {
                            var contactId = $(this).attr("data-contact-id");
                            var systemId = $(this).attr("data-system-id");
                            deleteContact(contactId, systemId);
                        });

                        function deleteContact(contactId, systemId) {
                            swal({
                                title: "Delete Contact?",
                                text: "Are you sure that you want to delete this system contact?",
                                type: "warning",
                                showCancelButton: true,
                                closeOnConfirm: false,
                                confirmButtonText: "Yes, delete it!",
                                confirmButtonColor: "#ec6c62"
                            }, function() {
                                $.ajax({
                                    url: "/systemlist/systemcontact/del/" + contactId + "?system_id=" + systemId ,
                                    type: "GET"
                                })
                                        .done(function(data) {
                                            swal({
                                                title: "Deleted",
                                                text: "System Contact was Deleted",
                                                type: "success",
                                                timer: 1800,
                                                showConfirmButton: false

                                            }
                                            );
                                            setTimeout(function(){window.location.replace('/systemlist/'+ systemId)},1900);
                                        })
                                        .error(function(data) {
                                            swal("Oops", "We couldn't connect to the server!", "error");
                                        });
                            });
                        }


$(document).on("click", ".DeleteVendor", function () {
    var vendorId = $(this).attr("data-vendor-id");
    var systemId = $(this).attr("data-system-id");
    // alert(systemId);
    deleteVendor(vendorId, systemId);
});

function deleteVendor(vendorId, systemId) {
    swal({
        title: "Delete Vendor Contact?",
        text: "Are you sure that you want to delete this vendor contact?",
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "Yes, delete it!",
        confirmButtonColor: "#ec6c62"
    }, function() {
        $.ajax({
            url: "/systemlist/systemvendor/del/" + vendorId + "?system_id=" + systemId ,
            type: "GET"
        })
                .done(function(data) {
                    swal({
                        title: "Deleted",
                        text: "Vendor Contact was Deleted",
                        type: "success",
                        timer: 1800,
                        showConfirmButton: false

                    }
                    );
                    setTimeout(function(){window.location.replace('/systemlist/'+ systemId)},1900);
                })
                .error(function(data) {
                    swal("Oops", "We couldn't connect to the server!", "error");
                });
    });
}

                        $(document).on("click", ".DeleteServer", function () {
                            var serverId = $(this).attr("data-server-id");
                            var systemId = $(this).attr("data-system-id");
                            //alert($(this).attr("data-server-id"));
                            //alert($(this).attr("data-system-id"));
                            deleteServer(serverId, systemId);
                        });

                        function deleteServer(serverId, systemId) {
                            swal({
                                title: "Delete Server?",
                                text: "Are you sure that you want to delete this server?",
                                type: "warning",
                                showCancelButton: true,
                                closeOnConfirm: false,
                                confirmButtonText: "Yes, delete it!",
                                confirmButtonColor: "#ec6c62"
                            }, function() {
                                $.ajax({
                                    url: "/systemlist/server/del/" + serverId + "?system_id=" + systemId ,
                                    type: "GET"
                                })
                                        .done(function(data) {
                                            swal({
                                                        title: "Deleted",
                                                        text: "Server was Deleted",
                                                        type: "success",
                                                        timer: 1800,
                                                        showConfirmButton: false

                                                    }
                                            );
                                            setTimeout(function(){window.location.replace('/systemlist/'+ systemId)},1900);
                                        })
                                        .error(function(data, status, error) {
                                            alert(status);
                                            alert(xhr.responseText);
                                            swal("Oops", "We couldn't connect to the server!", "error");
                                        });
                            });



                        }


                        $('#autocomplete').autocomplete({
                            serviceUrl: '/json/searchSystemlistContacts',
                            dataType: 'json',
                            type:'GET',
                            width: 418,
                            minChars:2,
                            onSelect: function(suggestion) {
                                //alert(suggestion.data);
                                $("#first_name").val(suggestion.first_name);
                                $("#last_name").val(suggestion.last_name);
                                // $("#contact_type").val(suggestion.contact_type);
                                // $("#office_phone").val(suggestion.office_phone);
                                // $("#mobile_phone").val(suggestion.mobile_phone);
                                $("#email_address").val(suggestion.mail);
                                $("#title").val(suggestion.title);
                                $("#department").val(suggestion.unit_code_description);

                            }


                        });
                    </script>

@endsection