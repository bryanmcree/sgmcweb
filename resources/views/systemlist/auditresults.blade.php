<table class="table table-bordered">
    <thead>
    <tr>
        <td>Name</td>
        <td>Emp #</td>
        <td>Assigned</td>
        <td>TD/PAr</td>
        <td>TD/Rem</td>
        <td>Response</td>
    </tr>
    </thead>
    <tbody>
    @foreach($data as $terms)
        <tr>
            <td>{{$terms->term_name}}</td>
            <td>{{$terms->term_code}}</td>
            <td>{{$terms->assigned_name}}</td>
            <td>{{\Carbon\Carbon::createFromTimeStamp(strtotime($terms->term_date))->diffInHours(\Carbon\Carbon::createFromTimeStamp(strtotime($terms->created_at)))}}</td>
            <td>@if($terms->request_completed != Null){{\Carbon\Carbon::createFromTimeStamp(strtotime($terms->term_date))->diffInHours(\Carbon\Carbon::createFromTimeStamp(strtotime($terms->request_completed)))}}@endif</td>
            <td>{{$terms->response}}</td>
        </tr>
    @endforeach
    </tbody>

</table>