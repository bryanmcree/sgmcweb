<div class="modal fade" id="AddContact_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" data-backdrop="static" >
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><b>Add Contact</b></h4>
            </div>
            <div class="modal-body bg-default">
                {!! Form::open(array('action' => array('SystemListController@AddContact2', $cost_center->cost_center,), 'class' => 'form_control','id'=>'AddContact')) !!}
                {!! Form::hidden('systems_id', null, ['class' => 'form-control','id'=>'systems_id']) !!}
                {!! Form::hidden('group_id', null, ['class' => 'form-control','id'=>'group_id']) !!}
                {!! Form::hidden('added_by', Auth::user()->username) !!}
                <div class="form-group">
                    {!! Form::label('first_name', 'First Name:') !!}
                    <div class="input-group">
                        <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                        {!! Form::text('first_name', null, ['class' => 'form-control', 'id'=>'first_name', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('last_name', 'Last Name:') !!}
                    <div class="input-group">
                        <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                        {!! Form::text('last_name', null, ['class' => 'form-control', 'id'=>'last_name', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('title', 'Title:') !!}
                    {!! Form::text('title', null, ['class' => 'form-control', 'id'=>'title']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('contact_type', 'Contact Type:') !!}
                    <div class="input-group">
                        <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                        {!! Form::select('contact_type', [
                                            '' => '[Select Contact Type]',

                                            'Department' => 'Department',

                                            'Information Services' => 'Information Services',

                                            'Vendor' => 'Vendor',

                                            'Support' => 'Support',

                                             ], null, ['class' => 'form-control', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('office_phone', 'Office Phone:') !!}
                    <div class="input-group">
                        <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                        {!! Form::text('office_phone', null, ['class' => 'form-control', 'id'=>'office_phone','maxlength' => '10', 'required','Placeholder'=>'NUMBERS ONLY']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('mobile_phone', 'Mobile Phone:') !!}
                    {!! Form::text('mobile_phone', null, ['class' => 'form-control', 'id'=>'mobile_phone','maxlength' => '10','Placeholder'=>'NUMBERS ONLY']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('email_address', 'Email Address:') !!}
                    <div class="input-group">
                        <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                        {!! Form::text('email_address', null, ['class' => 'form-control', 'id'=>'email_address', 'required']) !!}
                    </div>
                </div>

            </div>
            <div class="modal-footer bg-default">
                <div class="RightLeft">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    {!! Form::submit('Add Contact', ['class'=>'btn btn-sgmc', 'id'=>'AddButton']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>