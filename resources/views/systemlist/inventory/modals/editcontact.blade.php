
    <div class="modal fade" tabindex="-1" id="EditContact_modal" role="dialog" aria-labelledby="myLargeModalLabel1" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header modal-header-custom">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id=""><b>Edit Contact</b></h4>
                </div>
                <div class="modal-body bg-default">
                    {!! Form::open(array('action' => ['SystemListController@updateContact2', $cost_center->cost_center], 'class' => 'form_control')) !!}
                    <input name="_method" type="hidden" value="POST">
                    {!! Form::hidden('id', null,['class'=>'contacts_id']) !!}
                    <div class="form-group">
                        {!! Form::label('first_name', 'First Name:') !!}
                        <div class="input-group">
                            <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                            {!! Form::text('first_name', null, ['class' => 'form-control first_name', 'id'=>'first_name', 'required']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('last_name', 'Last Name:') !!}
                        <div class="input-group">
                            <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                            {!! Form::text('last_name', null, ['class' => 'form-control last_name', 'id'=>'last_name', 'required']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('title', 'Title:') !!}
                        <div class="input-group">
                            <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                            {!! Form::text('title', null, ['class' => 'form-control title', 'title', 'id'=>'title', 'required']) !!}
                        </div>
                    </div>


                    <div class="form-group">
                        {!! Form::label('contact_type', 'Contact Type:') !!}
                        <div class="input-group">
                            <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                            {!! Form::select('contact_type', [
                                                '' => '[Select Contact Type]',
                                                'Department' => 'Department',
                                                'Information Services' => 'Information Services',
                                                'Vendor' => 'Vendor',
                                                'Support' => 'Support',
                                                 ], null, ['class' => 'form-control contact_type', 'required']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('office_phone', 'Office Phone:') !!}
                        <div class="input-group">
                            <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                            {!! Form::text('office_phone', null, ['class' => 'form-control office_phone','office_phone', 'id'=>'office_phone','maxlength' => '10', 'required']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('mobile_phone', 'Mobile Phone:') !!}
                        {!! Form::text('mobile_phone', null, ['class' => 'form-control mobile_phone', 'id'=>'mobile_phone','maxlength' => '10']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('email_address', 'Email Address:') !!}
                        <div class="input-group">
                            <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                            {!! Form::text('email_address', null, ['class' => 'form-control email_address','email_address', 'id'=>'email_address', 'required']) !!}
                        </div>
                    </div>


                </div>
                <div class="modal-footer bg-default">
                    <div class="RightLeft">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        {!! Form::submit('Update Contact', ['class'=>'btn btn-sgmc', 'id'=>'UpdateButton']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
   