<div class="modal fade AddSystem" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><b>Add System</b></h4>
            </div>
            <div class="modal-body bg-default">
                {!! Form::open(array('action' => array('SystemListController@storeSystem', $cost_center->cost_center), 'class' => 'form_control')) !!}
                {!! Form::hidden('groupadd', 'Yes') !!}
                {!! Form::hidden('groupname', $cost_center->cost_center_description) !!}
                <div class="form-group">
                    {!! Form::label('sys_name', 'System Name:') !!}
                    <div class="input-group">
                        <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                        {!! Form::text('sys_name', null, ['class' => 'form-control', 'id'=>'sys_name', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('sys_type', 'System Type:') !!}
                    <div class="input-group">
                        <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                        {!! Form::select('sys_type', [
                                            '' => '[Select Type]',

                                            'Installed Software' => 'Installed Software',

                                            'Web-Based' => 'Web-Based',

                                            'Web-Based / 3 Party' => 'Web-Based / 3 Party',

                                            'Remote' => 'Remote',

                                            'Mixed' => 'Mixed',

                                             ], null, ['class' => 'form-control', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('frequency_use', 'How often is this system used?:') !!}
                    <div class="input-group">
                        <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                        {!! Form::select('frequency_use', [
                                            '' => '[Select Frequency]',

                                            'Everyday' => 'Everyday',

                                            'Once a Week' => 'Once a Week',

                                            'Once a Month' => 'Once a Month',

                                            'Once a Year' => 'Once a Year',

                                            'I do not know' => 'I do not know',

                                             ], null, ['class' => 'form-control', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('sys_name', 'Does the system contain the following data?:') !!}
                    <div class="input-group">
                        {!! Form::checkbox('data_patient', 1, null, ['class' => 'field', 'id'=>'data_patient'])  !!}{!! Form::label('data_patient', ' - Patient') !!}<br>
                        {!! Form::checkbox('data_financial', 1, null, ['class' => 'field', 'id'=>'data_financial'])  !!} {!! Form::label('data_financial', ' - Financial') !!}<br>
                        {!! Form::checkbox('data_employee', 1, null, ['class' => 'field', 'id'=>'data_employee'])  !!} {!! Form::label('data_employee', ' - Employee') !!}<br>
                        {!! Form::checkbox('data_org', 1, null, ['class' => 'field', 'id'=>'data_org'])  !!} {!! Form::label('data_org', ' - Organizational Performance') !!}<br>
                        {!! Form::checkbox('data_physician', 1, null, ['class' => 'field', 'id'=>'data_physician'])  !!} {!! Form::label('data_physician', ' - Physician Data') !!}<br>
                    </div>
                </div>



                <div class="form-group">
                    {!! Form::label('sys_description', 'System Description:') !!}
                    {!! Form::textarea('sys_description', null, ['class' => 'form-control', 'id'=>'sys_description', 'size' => '30x3','placeholder'=>'Describe what the system does, who uses it.']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('sys_comments', 'System Comments:') !!}
                    {!! Form::textarea('sys_comments', null, ['class' => 'form-control', 'id'=>'sys_comments','size' => '30x3']) !!}
                </div>
            </div>
            <div class="modal-footer bg-default">
                <div class="RightLeft">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    {!! Form::submit('Add System', ['class'=>'btn btn-sgmc', 'id'=>'updateButton']) !!}
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>