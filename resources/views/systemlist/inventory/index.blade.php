@extends('layouts.app')

@section('content')

<div class="col-md-8 col-md-offset-2">
    <div class="panel panel-default">
        <div class="panel-heading panel-heading-custom">
            <b><h1>System List Inventory</h1></b>
        </div>
        <div class="panel-body">

            <ol class="list-group">
                <li class="list-group-item">
                    <b>PURPOSE:</b><br>
                    In an effort to keep track of access to systems at SGMC it is important that a list of all current systems is created and maintained.  Once this list is complete
                    users will be added to this list allowing administration to track who has access to what system.  As personnel changes access to these systems will change.
                </li>

                <li class="list-group-item">
                    <b>DIRECTIONS:</b><br>
                    Select the cost center from the list below.
                </li>

                <li class="list-group-item">
                    <b>Need help?  Have an idea?  Want to eliminate paper in your department? Think a web application will make your department more efficient?</b><br>
                    Contact Bryan McRee at (229) 333-1145 or at {{ Html::mailto('bryan.mcree@sgmc.org') }}
                </li>
            </ol>

        </div>

        <div class="panel-body alert-default">
            <div class="col-md-8 col-md-offset-2">
                <div class="form-horizontal" style="align-items: center">
                    {!! Form::open(array('url' => '/systemlist/inventory/systems', 'method' => 'GET', 'files' => true,'class' => 'form_control')) !!}
                    {!! Form::label('unit', 'Search for Cost Center:') !!}
                    <div class="input-group">
                    {!! Form::text('search', '', ['class' => 'form-control', 'id'=>'autocomplete','placeholder'=>'Search by number or description.','required']) !!}
                    {!! Form::hidden('unit', '', ['class' => 'form-control','id'=>'unit']) !!}
                        <span class="input-group-btn">
                    {!! Form::submit('Select and Continue', array('class' => 'btn btn-block btn-sgmc')) !!}
                    {!! Form::close() !!}
                        </span>

                    </div>
                </div>
                <br>
                <br>
            </div>
        </div>
    </div>


    @if (Auth::user()->access == 'Admin')
        <div class="panel panel-default">
            <div class="panel-heading panel-heading-custom">
                <b>Completed Cost Centers - ({{$grouphistory->count()}})</b>
            </div>
            <div class="panel-body">
                @if ($grouphistory->isEmpty())
                    No systems for this cost center.  Select some from the list to the left.
                @else
                    <table class="table table-condensed table-hover" id="systems">
                        <thead class="bg-default header">
                        <tr class="bg-default">
                            <td class=""><b>Cost Center</b></td>
                            <td class=""><b>Group Name</b></td>
                            <td align="center" class=""><b># of Systems</b></td>
                            <td class=""><b>Last Updated</b></td>
                            <td class="hidden-xs hidden-sm "><b>By</b></td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($grouphistory as $groups)
                            <tr>
                                <td>{{$groups->cost_center}}</td>
                                <td>{{$groups->groupname}}</td>
                                <td align="center">{{$groups->totalsystems}}</td>
                                <td>{{$groups->created_at}}</td>
                                <td>{{$groups->addedby}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    @endif
    <div class="panel panel-default" style="visibility: hidden">
        <div class="panel-heading panel-heading-custom">
            <a role="button" class="panel" data-toggle="collapse" data-parent="#accordion" href="#collapseOne2" aria-expanded="true" aria-controls="collapseOne2">
                <b><i>OOPS!!!! My call center is not listed? Click ME!</i></b>
            </a>
        </div>
        <div id="collapseOne2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body panel-body-custom">
                <div class="col-md-5 col-md-offset-3">
                    <div class="form-horizontal" style="align-items: center">

                        {!! Form::open(array('url' => '/systemlist/inventory/systems', 'method' => 'GET', 'files' => true,'class' => 'form_control')) !!}
                        {!! Form::label('unit', 'Cost Center Number:') !!}
                        {!! Form::text('unit', null, ['class' => 'form-control', 'placeholder'=>'Cost Center #']) !!}
                        {!! Form::label('unit_description', 'Cost Center Name:') !!}
                        {!! Form::text('unit_description', null, ['class' => 'form-control', 'placeholder'=>'Cost Center Name']) !!}
                        <br>
                        {!! Form::submit('Continue', array('class' => 'btn btn-block btn-sgmc')) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

    <script type="text/javascript">

        $('#autocomplete').autocomplete({
            serviceUrl: '/costcenter',
            dataType: 'json',
            type:'GET',
            width: 418,
            minChars:2,
            onSelect: function(suggestion) {
                //alert(suggestion.data);
                $("#unit").val(suggestion.data);
            }


        });

    </script>

@endsection