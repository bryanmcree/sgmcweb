@extends('layouts.app')

@section('content')
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class=" panel-heading panel-heading-custom">
                <h2 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#instructions" aria-expanded="true" aria-controls="instructions">
                        <b>{{$cost_center->cost_center}} - {{$cost_center->cost_center_description}}</b> <- Click for instructions
                    </a>
                    <div class="RightLeft">
                        <div><a href="/systemlist/inventory" class="btn btn-primary" style="display: inline-block;">Start Over</a></div>
                        <div><a href="/systemlist/inventory/finished" class="btn btn-danger" style="display: inline-block;">Finish</a></div>
                    </div>
                </h2>

            </div>
            <div id="instructions" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
                <br>
                <li class="list-group-item">
                    <b>DIRECTIONS:</b><br>
                    <ul class="custom-bullet">
                        <li>Select systems from the left, click
                            <a href = "#" class = "btn btn-default btn-xs" role = "button">Add to Group <span class="fa fa-btn fa-long-arrow-right" aria-hidden="true"></span></a> if your department uses that system.</li>
                        <li>If not listed, click
                            <a href = "#" class = "btn btn-sgmc btn-xs" role = "button">Add System</a>
                            and it will be available for all USERS to choose from.</li>
                        <li>Each system will <u><b>require</b></u> at least ONE point of contact.</li>
                        <li class="padding">If your group system has
                            <a href="#" role="button" class=" btn btn-xs btn-danger"><span class="fa fa-btn fa-user" aria-hidden="true"></span> Add Contact</a>
                            , click and complete the required information.
                        </li>
                        <li class="padding">If your group system has
                            <a href="#" role="button" class=" btn btn-xs btn-default"><span class="fa fa-btn fa-user" aria-hidden="true"></span> Add Contact</a>
                            , it indicates that there is a point of contact.
                        </li>
                        <li class="padding">Click on
                            <a href="#" role="button" class=" btn btn-xs btn-default"><span class="fa fa-btn fa-user" aria-hidden="true"></span> Add Contact</a>
                            to include additional contact.
                        </li>
                        <li>To remove a system, click
                            <a href="#"  role="button" class=" btn btn-xs btn-default"><span class="fa fa-btn fa-trash-o" aria-hidden="true"></span> Remove</a>
                        </li>
                        <li>For any <b>CHANGES</b>, log in and update as needed.</li>
                        <li>Once complete, click the
                            <a href="#" role="button" class=" btn btn-xs btn-danger"> Finish</a>
                            button to the far right.

                        </li>
                    </ul>
                </li>
            </div>
                </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading panel-heading-custom">
                <b>Current Systems</b>
            </div>
            <div class="panel-body" style="height: calc(100vh - 250px); overflow-y: auto;">
                <div class="container-fluid">
                    <div class="row">
                        <div class="input-group">
                            {!! Form::text('search', null,array('class'=>'form-control','id'=>'search','placeholder'=>'Search for system...', 'autofocus'=>'autofocus')) !!}
                            <span class="input-group-btn">
                <a href = "#AddSystem" onclick="sweetAlert('Remember!', 'Before adding a system, use the Search for Systems on the left to make sure the system you want is not already present.');"  data-toggle="modal" data-target=".AddSystem" data-page="Inventory" class = "btn btn-sgmc" role = "button">Add System</a>
                </span>
                        </div>
                    </div>
                </div>
                <br>

                <table class="table table-hover" id="systems">

                    <tbody>
                    @foreach ($SystemList as $SystemLists)
                        {!! Form::open(array('url' => '/systemlist/inventory/add2Group', 'method' => 'POST', 'files' => true,'class' => 'form_control')) !!}
                        {!! Form::hidden('systems_id', $SystemLists->id, ['class' => 'form-control']) !!}
                        {!! Form::hidden('cost_center', $cost_center->cost_center, ['class' => 'form-control']) !!}
                        {!! Form::hidden('groupname', $cost_center->cost_center_description, ['class' => 'form-control']) !!}
                        {!! Form::hidden('addedby', Auth::user()->name, ['class' => 'form-control']) !!}
                        {!! Form::hidden('contact', 0, ['class' => 'form-control']) !!}
                        <tr>
                            <td>
                                <B>{{$SystemLists->sys_name}}</B><br>
                                <i>{{$SystemLists->sys_alias}}</i>

                            </td>
                            <td align="right">
                                {!! Form::button('Add to Group <i class="fa fa-long-arrow-right"></i>', array('type' => 'submit', 'class'=> 'actions-line icon-trash btn btn-default btn-xs')) !!}
                            </td>
                        </tr>
                        {!! Form::close() !!}
                    @endforeach

                    </tbody>
                </table>

            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="panel panel-default">
            <div class="panel-heading panel-heading-custom" >
                Systems assigned to <i>{{$cost_center->cost_center}} - {{$cost_center->cost_center_description}}</i> Group.
            </div>
            <div class="panel-body " style="height: calc(100vh - 250px); overflow-y: auto;">

                @if ($CostCenterSystems->isEmpty())
                    No systems for this cost center.  Select some from the list to the left.
                @else
                    <table class="table table-hover" id="systems">
                        <thead class="bg-default header">
                        <tr class="bg-default">
                            <td class=""><b>System Name</b></td>
                            <td class=""><b>Added By</b></td>
                            <td class=""><b>Added</b></td>
                            <td class="hidden-xs hidden-sm "></td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($CostCenterSystems as $groupSystem)
                            <tr>
                                <td>{{$groupSystem->groupSystems->sys_name}}</td>
                                <td>{{$groupSystem->addedby}}</td>
                                <td>{{$groupSystem->created_at}}</td>
                                <td align="right"><button data-toggle="collapse" data-target=".collapseme{{$groupSystem->id}}" role="button" class=" btn btn-xs @if($groupSystem->contact == 0)btn-danger @else btn-default @endif ViewContact2"><span class="fa fa-btn fa-user" aria-hidden="true"></span> Contact</button></td>
                                <td align="right"><a href="#DeleteSystem" data-system-id ="{{$groupSystem->id}}" data-cost-center ="{{$groupSystem->cost_center}}" role="button" class="DeleteSystem btn btn-xs btn-default"><span class="fa fa-btn fa-trash-o" aria-hidden="true"></span> Remove</a></td>
                            </tr>
                            <tr id="collapseme{{$groupSystem->id}}" class="collapse out collapseme{{$groupSystem->id}}">
                                <td bgcolor="gray" colspan="5">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <table class="table table-responsive">
                                                <thead class="bg-default header">
                                                <tr class="bg-default">
                                                    <td class=""><b>Name</b></td>
                                                    <td class=""><b>Title</b></td>
                                                    <td class="hidden-xs hidden-sm "><b>Department</b></td>
                                                    <td class="hidden-xs"><b>Email</b></td>
                                                    <td class="hidden-xs"><b>Office</b></td>
                                                    <td class="" align="right" style="vertical-align:middle"><a href="#" data-system-id = "{{$groupSystem->systems_id}}" data-group-id = "{{$groupSystem->id}}" role="button" class="btn btn-sgmc btn-xs AddContact2"><span class="fa fa-btn fa-plus-circle" aria-hidden="true"></span> Add Contact</a></td>
                                                </tr>
                                                </thead>
                                                @if (!$groupSystem->systemContacts->isEmpty())
                                                    <tbody>
                                                    @foreach ($groupSystem->systemContacts as $contacts)
                                                        <tr>
                                                            <td class="">{{$contacts->last_name}}, {{$contacts->first_name}}</td>
                                                            <td class="">{{$contacts->title}}</td>
                                                            <td class="hidden-xs hidden-sm ">{{$contacts->department}}</td>
                                                            <td class="hidden-xs">{{Html::mailto($contacts->email_address)}}</td>
                                                            <td class="hidden-xs">{{PhoneNumbers::formatNumber($contacts->office_phone)}}</td>
                                                            <td class="" align="right" style="vertical-align:middle">
                                                                @if($contacts->added_by == Auth::user()->username)
                                                                    {!! Form::button('<i class="fa fa-pencil-square"></i> Edit ', array('type' => 'submit', 'class'=> 'EditContact btn btn-primary btn-xs' ,
                                                                                                                        'data-contacts_id'=>$contacts->id,
                                                                                                                        'data-first_name'=>$contacts->first_name,
                                                                                                                        'data-last_name'=>$contacts->last_name,
                                                                                                                        'data-title'=>$contacts->title,
                                                                                                                        'data-department'=>$contacts->department,
                                                                                                                        'data-contact_type'=>$contacts->contact_type,
                                                                                                                        'data-office_phone'=>$contacts->office_phone,
                                                                                                                        'data-mobile_phone'=>$contacts->mobile_phone,
                                                                                                                        'data-email_address'=>$contacts->email_address,
                                                                                                                        'data-Pri'=>$contacts->Pri,
                                                                                                                        )) !!}
                                                                    @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                @endif
                                            </table>
                                        </div>
                                    </div>



                                </td>
                            </tr>

                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>






@endsection

@include("systemlist.inventory.modals.addsystem")
@include('systemlist.inventory.modals.addcontact')
@include('systemlist.inventory.modals.editcontact')

@section('scripts')

    <script type="text/javascript">



        $( '#systems' ).searchable({
            striped: false,
            oddRow: { 'background-color': '#f5f5f5' },
            evenRow: { 'background-color': '#fff' },
            searchType: 'fuzzy',
            onSearchActive : function( elem, term ) {
                elem.show();
            },
            onSearchFocus: function() {
                $( '#feedback' ).show().text( 'Type to search.' );
            },
            onSearchBlur: function() {
                $( '#feedback' ).hide();
            },
            clearOnLoad: true
        });



        $(document).on("click", ".AddContact2", function () {
            $("#systems_id").val($(this).attr("data-system-id"));
            $("#group_id").val($(this).attr("data-group-id"));
            //alert($(this).attr("data-system-id"));
            $('#AddContact_modal').modal('show');
        });

        $(document).on("click", ".EditContact", function () {
            //alert($(this).attr("data-id"));

            $('.first_name').val($(this).attr("data-first_name"));
            $('.last_name').val($(this).attr("data-last_name"));
            $('.contact_type').val($(this).attr("data-contact_type"));
            $('.office_phone').val($(this).attr("data-office_phone"));
            $('.mobile_phone').val($(this).attr("data-mobile_phone"));
            $('.email_address').val($(this).attr("data-email_address"));
            $('.title').val($(this).attr("data-title"));
            $('.department').val($(this).attr("data-department"));
            $('.Pri').val($(this).attr("data-Pri"));
            $('.systems_id').val($(this).attr("data-systems_id"));
            $('.contacts_id').val($(this).attr("data-contacts_id"));
            $('#EditContact_modal').modal('show');
            //alert($(this).attr("data-first_name"));
        });


        $(document).on("click", ".DeleteSystem", function () {
            var system_id = $(this).attr("data-system-id");
            var cost_center = $(this).attr("data-cost-center");
            deleteSystem(system_id, cost_center);
        });

        function deleteSystem(system_id, cost_center) {
            swal({
                title: "Delete System?",
                text: "Are you sure that you want to delete this system from your group?",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                confirmButtonText: "Yes, delete it!",
                confirmButtonColor: "#ec6c62"
            }, function () {
                $.ajax({
                    url: "/systemlist/inventory/del/" + system_id + "?cost_center=" + cost_center,
                    type: "GET"
                })
                        .done(function (data) {
                            swal({
                                        title: "Deleted",
                                        text: "System was Deleted",
                                        type: "success",
                                        timer: 1800,
                                        showConfirmButton: false

                                    }
                            );
                            setTimeout(function () {
                                window.location.replace('/systemlist/inventory/systems?unit=' + cost_center)
                            }, 1900);
                        })
                        .error(function (data) {
                            swal("Oops", "We couldn't connect to the server!", "error");
                        });
            });
        }


    </script>

@endsection