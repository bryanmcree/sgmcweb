<div id="headerbtn" class="span6">
    <div class="btn-toolbar pt-2 pl-2">
        <div class="btn-group">
            <a href="/systemlist" class="btn btn-primary">
                <span class="fa fa-btn fa-backward" aria-hidden="true"></span> Back to System List
            </a>
            &nbsp;
        </div>
        <div class="btn-group">
            <a href="#editSystem" data-toggle="modal" data-target=".editSystem" class="editButton btn btn-warning"
               data-sys_name="{{$SystemList->sys_name}}"
               data-sys_description="{{$SystemList->sys_description}}"
               data-sys_comments="{{$SystemList->sys_comments}}"
               data-id="{{$SystemList->id}}"
            >
                <i class="fas fa-edit"></i> Edit System
            </a>
            &nbsp;
        </div>

        <div class="btn-group ">
            <a href="mailto:{{str_replace(',',';',str_replace('"','',$SystemList->mailContacts()))}}?subject={{$SystemList->sys_name}} / System List" role="button" class="btn btn-info">
                <i class="fas fa-envelope"></i> Email Contacts</a>
            &nbsp;
        </div>

        <div class="btn-group ">
            <a href="#" class="btn btn-success VerifySystem" data-toggle="modal"
               @if ($SystemList->systemVerification->isEmpty()) data-toggle="popover" data-container="body" data-trigger="focus"
               data-placement="top" data-content="This System has NOT been verified now is a good time don't ya think?"
                @endif
               data-id="{{$SystemList->id}}">
                <i class="fas fa-check"></i> Verify System
            </a>
            &nbsp;
        </div>
        @if ( Auth::user()->hasRole('Systems List - Delete System'))
        <div class="btn-group">
            <a href="del/{{$SystemList->id}}" class="btn btn-danger hidden-xs hidden-sm hidden-md">
                <i class="fas fa-trash-alt"></i> Delete System
            </a>
            &nbsp;
        </div>
        @endif

    </div>
</div>
<hr>