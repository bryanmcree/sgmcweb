<div class="modal fade " tabindex="-1" id="EditServer_modal" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">
                
                <h4 class="modal-title" id="myModalLabel"><b>Edit Server</b></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                
            </div>
            <div class="modal-body bg-default">
                {!! Form::open(array('action' => ['SystemListController@UpdateServer'], 'class' => 'form_control')) !!}
                {!! Form::hidden('srv_id', null,['class'=>'srv_id']) !!}
                {!! Form::hidden('systems_id', $SystemList->id,['class'=>'systems_id']) !!}

                <h5><i>*Please provide as much information as possible.</i></h5>

                <div class="row">
                    <div class="col-md-4">
                        {!! Form::label('srv_name', 'Server Name:') !!}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text bg-danger" id="basic-addon1"><i class="fa fa-exclamation-triangle"></i></span>
                            </div>
                            {!! Form::text('srv_name', null, ['class' => 'form-control srv_name', 'id'=>'srv_name', 'required']) !!}
                        </div>
                    </div>
                    <div class="col-md-4">
                        {!! Form::label('srv_os', 'Server Operating System:') !!}
                        {!! Form::text('srv_os', null, ['class' => 'form-control srv_os', 'id'=>'srv_os', 'placeholder'=>'If Applicable']) !!}
                        {{-- {!! Form::select('srv_os', [
                                            '' => '[Select Server OS]',
                                            'Redhat Linux' => 'Redhat Linux',
                                            'Windows 7' => 'Windows 7',
                                            'Server 2008 or older' => 'Server 2008 or older',
                                             ], null, ['class' => 'form-control srv_os', 'required']) !!} --}}
                    </div>
                    <div class="col-md-4">
                        {!! Form::label('srv_type', 'Server Type:') !!}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text bg-danger" id="basic-addon1"><i class="fa fa-exclamation-triangle"></i></span>
                            </div>
                            {!! Form::select('srv_type', [
                                                '' => '[Select Account Type]',
                                                'Physical' => 'Physical',
                                                'Virtual' => 'Virtual',
                                                 ], null, ['class' => 'form-control srv_type', 'required']) !!}
                        </div>
                    </div>
                </div>
<br>
                <div class="row">
                    <hr>
                    <div class="col-md-4">
                        {!! Form::label('srv_ip', 'Server IP Address:') !!}
                        {!! Form::text('srv_ip', null, ['class' => 'form-control srv_ip', 'id'=>'srv_ip', 'placeholder'=>'If Applicable']) !!}
                    </div>
                    <div class="col-md-4">
                        {!! Form::label('srv_ip2', 'Server IP Address(2):') !!}
                        {!! Form::text('srv_ip2', null, ['class' => 'form-control srv_ip2', 'id'=>'srv_ip2', 'placeholder'=>'If Applicable']) !!}
                    </div>
                    <div class="col-md-4">
                        {!! Form::label('srv_vmEnv', 'VM Enviroment:') !!}

                        {!! Form::select('srv_vmEnv', [
                                            '' => '[Select Enviroment]',

                                            'VADE-VC-01' => 'VADE-VC-01',

                                            'VM-VC-01' => 'VM-VC-01',

                                            'SG-VC-01' => 'SG-VC-01',

                                             ], null, ['class' => 'form-control srv_vmEnv']) !!}
                    </div>
                </div>
<br>
                <div class="row">
                    <hr>
                    <div class="col-md-4">
                        {!! Form::label('srv_adminName', 'User Name(s):') !!}
                        {!! Form::text('srv_adminName1', null, ['class' => 'form-control srv_adminName1', 'id'=>'srv_adminName1', 'placeholder'=>'If Applicable']) !!}
                        {!! Form::text('srv_adminName2', null, ['class' => 'form-control srv_adminName2', 'id'=>'srv_adminName2', 'placeholder'=>'If Applicable']) !!}
                        {!! Form::text('srv_adminName', null, ['class' => 'form-control srv_adminName', 'id'=>'srv_adminName', 'placeholder'=>'If Applicable']) !!}
                    </div>
                    <div class="col-md-4">
                        {!! Form::label('srv_adminpassword', 'Password(s):') !!}
                        {!! Form::text('srv_adminpassword1', null, ['class' => 'form-control srv_adminPassword1', 'id'=>'srv_adminPassword1', 'placeholder'=>'If Applicable']) !!}
                        {!! Form::text('srv_adminpassword2', null, ['class' => 'form-control srv_adminPassword2', 'id'=>'srv_adminPassword2', 'placeholder'=>'If Applicable']) !!}
                        {!! Form::text('srv_adminPassword', null, ['class' => 'form-control srv_adminPassword', 'id'=>'srv_adminPassword', 'placeholder'=>'If Applicable']) !!}
                    </div>
                    <div class="col-md-4">
                        {!! Form::label('srv_type', 'Account Type(s):') !!}

                        {!! Form::select('srv_admintype1', [
                                            '' => '[Select Account Type]',
                                            'Remote Client Password' => 'Remote Client Password',
                                            'SSH' => 'SSH',
                                            'SQL Database' => 'SQL Database',
                                            'Web Access/Interface' => 'Web Access/Interface',
                                            'Active Directory' => 'Active Directory',
                                            'Service Account Password' => 'Service Account Password',
                                            'Application Password' => 'Application Password',
                                            'Other'=>'Other'
                                             ], null, ['class' => 'form-control srv_admintype1']) !!}
                        {!! Form::select('srv_admintype2', [
                                                '' => '[Select Account Type]',
                                                'Remote Client Password' => 'Remote Client Password',
                                                'SSH' => 'SSH',
                                                'SQL Database' => 'SQL Database',
                                                'Web Access/Interface' => 'Web Access/Interface',
                                                'Active Directory' => 'Active Directory',
                                                'Service Account Password' => 'Service Account Password',
                                                'Application Password' => 'Application Password',
                                                'Other'=>'Other'
                                                 ], null, ['class' => 'form-control srv_admintype2']) !!}
                        {!! Form::select('srv_admintype3', [
                                                '' => '[Select Account Type]',
                                                'Remote Client Password' => 'Remote Client Password',
                                                'SSH' => 'SSH',
                                                'SQL Database' => 'SQL Database',
                                                'Web Access/Interface' => 'Web Access/Interface',
                                                'Active Directory' => 'Active Directory',
                                                'Service Account Password' => 'Service Account Password',
                                                'Application Password' => 'Application Password',
                                                'Other'=>'Other'
                                                 ], null, ['class' => 'form-control srv_admintype3']) !!}
                    </div>
                </div>

                @if ( Auth::user()->hasRole('Infrastructure') == TRUE)
                    <hr>

                    <div class="row bg-warning p-2">

                        <div class="col-md-4">
                            {!! Form::label('inf_user', 'User Name(s):') !!}
                            {!! Form::text('inf_user1', null, ['class' => 'form-control inf_user1', 'id'=>'inf_user1', 'placeholder'=>'If Applicable']) !!}
                            {!! Form::text('inf_user2', null, ['class' => 'form-control inf_user2', 'id'=>'inf_user2', 'placeholder'=>'If Applicable']) !!}
                            {!! Form::text('inf_user3', null, ['class' => 'form-control inf_user3', 'id'=>'inf_user3', 'placeholder'=>'If Applicable']) !!}
                        </div>
                        <div class="col-md-4">
                            {!! Form::label('inf_pass', 'Password(s):') !!}
                            {!! Form::text('inf_pass1', null, ['class' => 'form-control inf_pass1', 'id'=>'inf_pass1', 'placeholder'=>'If Applicable']) !!}
                            {!! Form::text('inf_pass2', null, ['class' => 'form-control inf_pass2', 'id'=>'inf_pass2', 'placeholder'=>'If Applicable']) !!}
                            {!! Form::text('inf_pass3', null, ['class' => 'form-control inf_pass3', 'id'=>'inf_pass3', 'placeholder'=>'If Applicable']) !!}
                        </div>
                        <div class="col-md-4">
                            {!! Form::label('srv_type', 'Account Type(s):') !!}

                            {!! Form::select('inf_type1', [
                                                '' => '[Select Account Type]',
                                                'Remote Client Password' => 'Remote Client Password',
                                                'SSH' => 'SSH',
                                                'SQL Database' => 'SQL Database',
                                                'Web Access/Interface' => 'Web Access/Interface',
                                                'Active Directory' => 'Active Directory',
                                                'Service Account Password' => 'Service Account Password',
                                                'Application Password' => 'Application Password',
                                                'Other'=>'Other'
                                                 ], null, ['class' => 'form-control inf_type1', 'id'=>'inf_type1']) !!}
                            {!! Form::select('inf_type2', [
                                                    '' => '[Select Account Type]',
                                                    'Remote Client Password' => 'Remote Client Password',
                                                    'SSH' => 'SSH',
                                                    'SQL Database' => 'SQL Database',
                                                    'Web Access/Interface' => 'Web Access/Interface',
                                                    'Active Directory' => 'Active Directory',
                                                    'Service Account Password' => 'Service Account Password',
                                                    'Application Password' => 'Application Password',
                                                    'Other'=>'Other'
                                                     ], null, ['class' => 'form-control inf_type2', 'id'=>'inf_type2']) !!}
                            {!! Form::select('inf_type3', [
                                                    '' => '[Select Account Type]',
                                                    'Remote Client Password' => 'Remote Client Password',
                                                    'SSH' => 'SSH',
                                                    'SQL Database' => 'SQL Database',
                                                    'Web Access/Interface' => 'Web Access/Interface',
                                                    'Active Directory' => 'Active Directory',
                                                    'Service Account Password' => 'Service Account Password',
                                                    'Application Password' => 'Application Password',
                                                    'Other'=>'Other'
                                                     ], null, ['class' => 'form-control inf_type3', 'id'=>'inf_type3']) !!}
                        </div>
                    </div>
                @endif
<hr>
                <div class="row">
                    <hr>
                    <div class="col-md-3">
                        {!! Form::label('srv_serviceTag', 'Manufacture Service Tag:') !!}
                        {!! Form::text('srv_serviceTag', null, ['class' => 'form-control srv_serviceTag', 'id'=>'srv_serviceTag', 'placeholder'=>'If Applicable']) !!}
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('srv_modelNum', 'Manufacture Model Number:') !!}
                        {!! Form::text('srv_modelNum', null, ['class' => 'form-control srv_modelNum', 'id'=>'srv_modelNum', 'placeholder'=>'If Applicable']) !!}
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('srv_serial', 'Serial Number:') !!}
                        {!! Form::text('srv_serial', null, ['class' => 'form-control srv_serial', 'id'=>'srv_serial', 'placeholder'=>'If Applicable']) !!}
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('srv_sgmcNum', 'SGMC Asset Number:') !!}
                        {!! Form::text('srv_sgmcNum', null, ['class' => 'form-control srv_sgmcNum', 'id'=>'srv_sgmcNum', 'placeholder'=>'If Applicable']) !!}
                    </div>
                </div>
<br>
                <div class="row">
                    <hr>
                    <div class="col-md-6">
                        {!! Form::label('srv_equType', 'Equipment Type:') !!}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text bg-danger" id="basic-addon1"><i class="fa fa-exclamation-triangle"></i></span>
                            </div>
                            {!! Form::select('srv_equType', [
                                                '' => '[Select Equipment Type]',

                                                'Physical Server' => 'Physical Server',

                                                'VM Server' => 'VM Server',

                                                'Switch' => 'Switch',
                                                'Access Point' => 'Access Point',
                                                'Battery Backup' => 'Battery Backup',
                                                'Cooling Unit' => 'Cooling Unit',

                                                 ], null, ['class' => 'form-control srv_equType', 'required']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        {!! Form::label('srv_BackupRequired', 'Requires Backup?:') !!}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text bg-danger" id="basic-addon1"><i class="fa fa-exclamation-triangle"></i></span>
                            </div>
                            {!! Form::select('srv_BackupRequired', [
                                                '' => '[Select Option]',
                                                'Yes' => 'Yes',
                                                'No / Not Applicable' => 'No / Not Applicable',
                                                 ], null, ['class' => 'form-control srv_BackupRequired', 'required']) !!}
                        </div>
                    </div>
                </div>
<br>
                <div class="row">
                    <hr>
                    <div class="col-md-6">
                        {!! Form::label('srv_software', 'Server Software:') !!}
                        {!! Form::textarea('srv_software', null, ['class' => 'form-control srv_software', 'id'=>'srv_software', 'rows' => 4]) !!}
                    </div>
                    <div class="col-md-6">
                        {!! Form::label('srv_comments', 'Server Notes:') !!}
                        {!! Form::textarea('srv_comments', null, ['class' => 'form-control srv_comments', 'id'=>'srv_comments', 'rows' => 4]) !!}
                    </div>
                </div>

                <div class="modal-footer bg-default">
                    <div class="RightLeft">
                        {!! Form::submit('Update Server', ['class'=>'btn btn-sgmc', 'id'=>'EditServer']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>