<div class="modal fade AddSystem" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><b>Add System</b></h4>
            </div>
            <div class="modal-body bg-default">
                {!! Form::open(array('action' => ['SystemListController@store'], 'class' => 'form_control')) !!}
                <div class="form-group">
                    {!! Form::label('sys_name', 'System Name:') !!}
                    <div class="input-group">
                        <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                        {!! Form::text('sys_name', null, ['class' => 'form-control', 'id'=>'sys_name']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('sys_alias', 'System Alias:') !!}
                    <div class="input-group">
                        <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                        {!! Form::textarea('sys_alias', null, ['class' => 'form-control', 'id'=>'sys_alias', 'rows' => 2,'required','placeholder'=>'Other names this system maybe know as.  Also good to list terms associated with the purpose of the system and what the system does.']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('system_priority', 'System Priority:') !!}
                    <div class="input-group">
                        <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                        {!! Form::select('system_priority', [
                                            '' => '[Select Priority]',
                                            '1' => 'CRITICAL',
                                            '2' => 'Very High',
                                            '3' => 'High',
                                            '4' => 'Normal',
                                            '5' => 'Low',
                                             ], null, ['class' => 'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('sys_type', 'System Type:') !!}
                    <div class="input-group">
                        <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                        {!! Form::select('sys_type', [
                                            '' => '[Select Type]',
                                            'Physical' => 'Physical',
                                            'Web-Based' => 'Web-Based',
                                            'Web-Based / 3 Party' => 'Web-Based / 3 Party',
                                            'Virtual' => 'Virtual',
                                            'Remote' => 'Remote',
                                            'Mixed' => 'Mixed',
                                            'Unknown' => 'Unknown',
                                             ], null, ['class' => 'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('sys_description', 'System Description:') !!}
                    {!! Form::textarea('sys_description', null, ['class' => 'form-control', 'id'=>'sys_description', 'rows' => 4]) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('sys_comments', 'System Comments:') !!}
                    {!! Form::textarea('sys_comments', null, ['class' => 'form-control', 'id'=>'sys_comments', 'rows' => 4]) !!}
                </div>
            </div>
            <div class="modal-footer bg-default">
                <div class="RightLeft">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    {!! Form::submit('Add System', ['class'=>'btn btn-sgmc', 'id'=>'updateButton']) !!}
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>