<div class="modal fade AddVendor" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">
                
                <h4 class="modal-title" id="myModalLabel"><b>Add a Vendor Contact</b></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                
            </div>
            <div class="modal-body bg-default">
                <div class="form-group">
                    {{-- <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text bg-info" id="basic-addon1"><i class="fa fa-search"></i></span>
                        </div>
                {!! Form::text('search2', '', ['class' => 'form-control', 'id'=>'autocomplete','placeholder'=>'Search existing vendor contact.','autofocus'=>'autofocus']) !!}
                    </div> --}}
                    </div>
                {!! Form::open(array('action' => ['SystemListController@AddVendor'], 'class' => 'form_control')) !!}
                
                {!! Form::hidden('systems_id', $SystemList->id) !!}
                {!! Form::hidden('added_by', Auth::user()->username) !!}
                


                <div class="form-group">
                    {!! Form::label('conctact_name', 'Contact Name:') !!}
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text bg-danger" id="basic-addon1"><i class="fa fa-exclamation-triangle"></i></span>
                        </div>
                        {!! Form::text('contact_name', null, ['class' => 'form-control', 'id'=>'contact_name', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('contact_description', 'Contact Description:') !!}
                    {!! Form::text('contact_description', null, ['class' => 'form-control', 'id'=>'contact_description','maxlength' => '150']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('company', 'Company:') !!}
                    {!! Form::text('company', null, ['class' => 'form-control', 'id'=>'company','maxlength' => '150']) !!}
                </div>

                {{-- <div class="form-group">
                    {!! Form::label('contact_type', 'Contact Type:') !!}
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text bg-danger" id="basic-addon1"><i class="fa fa-exclamation-triangle"></i></span>
                        </div>
                        {!! Form::select('contact_type', [
                                            '' => '[Select Contact Type]',

                                            'Department' => 'Department',

                                            'Information Services' => 'Information Services',

                                            'Vendor' => 'Vendor',

                                            'Support' => 'Support',

                                             ], null, ['class' => 'form-control', 'required']) !!}
                    </div>
                </div> --}}

                <div class="form-group">
                    {!! Form::label('phone1', 'Phone 1:') !!}
                    <div class="input-group">
                        {!! Form::text('phone1', null, ['class' => 'form-control', 'id'=>'phone1','maxlength' => '10','Placeholder'=>'NUMBERS ONLY']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('phone2', 'Phone 2:') !!}
                    <div class="input-group">
                        {!! Form::text('phone2', null, ['class' => 'form-control', 'id'=>'phone2','maxlength' => '10','Placeholder'=>'NUMBERS ONLY']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('email1', 'Email Address:') !!}
                    <div class="input-group">
                        {!! Form::text('email1', null, ['class' => 'form-control', 'id'=>'email1']) !!}
                    </div>
                </div>

                <div class="col-md-12">
                    {!! Form::label('notes', 'Notes:') !!}
                    {!! Form::textarea('notes', null, ['class' => 'form-control', 'id'=>'notes', 'rows' => 4]) !!}
                </div>

            </div>
            <div class="modal-footer bg-default">
                <div class="RightLeft">
                    {!! Form::submit('Add Vendor Contact', ['class'=>'btn btn-sgmc', 'id'=>'AddButton']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>