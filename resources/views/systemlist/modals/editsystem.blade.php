<div class="modal fade editSystem" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">
                
                <h4 class="modal-title" id="myModalLabel"><b>Edit System</b></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

            </div>
            <div class="modal-body bg-default">
                {!! Form::model($SystemList, ['method'=>'PATCH', 'action'=>['SystemListController@update', $SystemList->id], 'files' => true]) !!}
                <div class="form-group">
                    {!! Form::label('sys_name', 'System Name:') !!}
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text bg-danger" id="basic-addon1"><i class="fa fa-exclamation-triangle"></i></span>
                        </div>
                        {!! Form::text('sys_name', null, ['class' => 'form-control', 'id'=>'sys_name']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('sys_alias', 'System Alias:') !!}
                    {!! Form::textarea('sys_alias', null, ['class' => 'form-control', 'id'=>'sys_alias', 'rows' => 2,'placeholder'=>'Other names this system maybe know as.  Also good to list terms associated with the purpose of the system and what the system does.']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('system_priority', 'System Priority:') !!}
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text bg-danger" id="basic-addon1"><i class="fa fa-exclamation-triangle"></i></span>
                        </div>
                        {!! Form::select('system_priority', [
                                            '' => '[Select Priority]',
                                            '1' => 'CRITICAL',
                                            '2' => 'Very High',
                                            '3' => 'High',
                                            '4' => 'Normal',
                                            '5' => 'Low',
                                             ], null, ['class' => 'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('sys_type', 'System Type:') !!}
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text bg-danger" id="basic-addon1"><i class="fa fa-exclamation-triangle"></i></span>
                        </div>
                        {!! Form::select('sys_type', [
                                            '' => '[Select Type]',
                                            'Physical' => 'Physical',
                                            'Web-Based' => 'Web-Based',
                                            'Web-Based / 3 Party' => 'Web-Based / 3 Party',
                                            'Remote' => 'Remote',
                                            'Virtual' => 'Virtual',
                                            'Mixed' => 'Mixed',
                                            'Unknown' => 'Unknown',
                                             ], null, ['class' => 'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('sys_description', 'System Description:') !!}
                    {!! Form::textarea('sys_description', null, ['class' => 'form-control', 'id'=>'sys_description', 'rows' => 4]) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('sys_comments', 'System Comments:') !!}
                    {!! Form::textarea('sys_comments', null, ['class' => 'form-control', 'id'=>'sys_comments', 'rows' => 4]) !!}
                </div>
                <div class="form-group">
                    {!! Form::checkbox('inv_visiable', 'No', null, ['class' => 'field', 'id'=>'inv_visiable'])  !!}{!! Form::label('inv_visiable', ' - System Not User Assignable') !!}
                </div>
                <div class="form-group">
                    {!! Form::checkbox('disable_sys', 'YES', null, ['class' => 'field', 'id'=>'disable_sys'])  !!}{!! Form::label('disable_sys', ' - Mark System as Disabled / Replaced or No longer in use.') !!}
                </div>
                <div class="modal-footer bg-default">
                    <div class="RightLeft">
                        
                        {!! Form::submit('Update System', ['class'=>'btn btn-sgmc', 'id'=>'updateButton']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>