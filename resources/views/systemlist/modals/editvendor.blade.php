@if (!$SystemList->systemVendors->isEmpty())
    <div class="modal fade" tabindex="-1" id="EditVendor_modal" role="dialog" aria-labelledby="myLargeModalLabel1" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header modal-header-custom">
                    
                    <h4 class="modal-title" id=""><b>Edit Vendor Contact</b></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                </div>
                <div class="modal-body bg-default">
                    {!! Form::open(array('action' => ['SystemListController@updateVendor'], 'class' => 'form_control')) !!}
                    <input name="_method" type="hidden" value="POST">
                    {!! Form::hidden('id', null,['class'=>'vendor_id']) !!}
                    {!! Form::hidden('systems_id', null,['class'=>'systems_id']) !!}

                    <div class="form-group">
                        {!! Form::label('contact_name', 'Contact Name:') !!}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text bg-danger" id="basic-addon1"><i class="fa fa-exclamation-triangle"></i></span>
                            </div>
                            {!! Form::text('contact_name', null, ['class' => 'form-control contact_name', 'contact_name', 'id'=>'contact_name', 'required']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('contact_description', 'Contact Description:') !!}
                        {!! Form::text('contact_description', null, ['class' => 'form-control contact_description']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('company', 'Company:') !!}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text bg-danger" id="basic-addon1"><i class="fa fa-exclamation-triangle"></i></span>
                            </div>
                            {!! Form::text('company', null, ['class' => 'form-control company','company', 'id'=>'company','maxlength' => '10', 'required']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('phone1', 'Phone 1:') !!}
                        {!! Form::text('phone1', null, ['class' => 'form-control phone1', 'id'=>'phone1','maxlength' => '10']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('phone2', 'Phone 2:') !!}
                        {!! Form::text('phone2', null, ['class' => 'form-control phone2', 'id'=>'phone2','maxlength' => '10']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('email1', 'Email Address:') !!}
                        {!! Form::text('email1', null, ['class' => 'form-control email1','email1', 'id'=>'email1', 'required']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('notes', 'Notes:') !!}
                        {!! Form::textarea('notes', null, ['class' => 'form-control notes', 'id'=>'notes', 'rows' => 4]) !!}
                    </div>
    

                </div>
                <div class="modal-footer bg-default">
                    <div class="RightLeft">
                        
                        {!! Form::submit('Update Vendor Contact', ['class'=>'btn btn-sgmc', 'id'=>'UpdateButton']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endif