<div class="modal fade" id="VerifySystem_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">
                
                <h4 class="modal-title" id="myModalLabel"><b>Verify System</b></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

            </div>
            <div class="modal-body bg-default">

                {!! Form::open(array('action' => ['SystemListController@systemsVerify'], 'class' => 'form_control')) !!}
                {!! Form::hidden('systems_id', '', ['class' => 'form-control systems_id']) !!}
                {!! Form::hidden('username', Auth::user()->username, ['class' => 'form-control']) !!}


                <div class="form-group">
                    {!! Form::label('verify_backup', 'Is this system currently backed up?:') !!}
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text bg-danger" id="basic-addon1"><i class="fa fa-exclamation-triangle"></i></span>
                        </div>
                        {!! Form::select('verify_backup', [
                                            '' => '[Select]',
                                            'Yes' => 'System is Currently Backed Up',
                                            'No' => 'System is NOT in a Backup Routine',
                                            'N/A' => 'Not Applicable',
                                            'Unknown' => 'I do not know',
                                             ], null, ['class' => 'form-control', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('verify_details', 'Is the information about the system current?:') !!}
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text bg-danger" id="basic-addon1"><i class="fa fa-exclamation-triangle"></i></span>
                        </div>
                        {!! Form::select('verify_details', [
                                            '' => '[Select]',
                                            'Yes' => 'All details about the system are current',
                                            'No' => 'No details of the system need to be updated',
                                            'N/A' => 'Not Applicable',
                                            'Unknown' => 'I do not know',
                                             ], null, ['class' => 'form-control', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('verify_contacts', 'Are all the system contacts current and up to date?:') !!}
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text bg-danger" id="basic-addon1"><i class="fa fa-exclamation-triangle"></i></span>
                        </div>
                        {!! Form::select('verify_contacts', [
                                            '' => '[Select]',
                                            'Yes' => 'All contacts are current and in correct order of priority',
                                            'No' => 'No contacts are out of order, not current or no longer assigned to system',
                                            'N/A' => 'Not Applicable',
                                            'Unknown' => 'I do not know',
                                             ], null, ['class' => 'form-control', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('verify_users', 'All users who are not part of SGMC Domain have been verified:') !!}
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text bg-danger" id="basic-addon1"><i class="fa fa-exclamation-triangle"></i></span>
                        </div>
                        {!! Form::select('verify_users', [
                                            '' => '[Select]',
                                            'Yes' => 'Yes all outside users have been verified and others been removed from the system AND web.sgmc.org',
                                            'No' => 'No outside contacts could not be verified and are still active in the system',
                                            'N/A' => 'Not Applicable',
                                            'Unknown' => 'I do not know',
                                             ], null, ['class' => 'form-control', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('verify_use', 'System is still being used and is in good operating order:') !!}
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text bg-danger" id="basic-addon1"><i class="fa fa-exclamation-triangle"></i></span>
                        </div>
                        {!! Form::select('verify_use', [
                                            '' => '[Select]',
                                            'Yes' => 'Yes system is still being used and in good order',
                                            'No' => 'No system has been replaced or is no longer being used',
                                            'N/A' => 'Not Applicable',
                                            'Unknown' => 'I do not know',
                                             ], null, ['class' => 'form-control', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('verify_notes', 'Additional Comments:') !!}

                        {!! Form::textarea('verify_notes', null, ['class' => 'form-control', 'id'=>'verify_notes', 'rows' => 3]) !!}
                </div>


                By clicking Verify you affirm that all aspects of this system are up and current.
            </div>
            <div class="modal-footer bg-default">

                <div class="RightLeft">
                    
                    {!! Form::submit('Verify', ['class'=>'btn btn-sgmc', 'id'=>'AddButton']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>