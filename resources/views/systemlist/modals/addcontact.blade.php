<div class="modal fade AddContact" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">
                
                <h4 class="modal-title" id="myModalLabel"><b>Add Contact</b></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                
            </div>
            <div class="modal-body bg-default">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text bg-info" id="basic-addon1"><i class="fa fa-search"></i></span>
                        </div>
                {!! Form::text('search2', '', ['class' => 'form-control', 'id'=>'autocomplete','placeholder'=>'Search existing contact.','autofocus'=>'autofocus']) !!}
                    </div>
                    </div>
                {!! Form::open(array('action' => ['SystemListController@AddContact'], 'class' => 'form_control')) !!}
                {!! Form::hidden('systems_id', $SystemList->id) !!}
                {!! Form::hidden('added_by', Auth::user()->username) !!}
                <div class="form-group">
                    {!! Form::label('first_name', 'First Name:') !!}
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text bg-danger" id="basic-addon1"><i class="fa fa-exclamation-triangle"></i></span>
                        </div>
                        {!! Form::text('first_name', null, ['class' => 'form-control', 'id'=>'first_name', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('last_name', 'Last Name:') !!}
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text bg-danger" id="basic-addon1"><i class="fa fa-exclamation-triangle"></i></span>
                        </div>
                        {!! Form::text('last_name', null, ['class' => 'form-control', 'id'=>'last_name', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('title', 'Title:') !!}
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text bg-danger" id="basic-addon1"><i class="fa fa-exclamation-triangle"></i></span>
                        </div>
                        {!! Form::text('title', null, ['class' => 'form-control', 'id'=>'title', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('department', 'Department:') !!}
                    {!! Form::text('department', null, ['class' => 'form-control', 'id'=>'department']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('contact_type', 'Contact Type:') !!}
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text bg-danger" id="basic-addon1"><i class="fa fa-exclamation-triangle"></i></span>
                        </div>
                        {!! Form::select('contact_type', [
                                            '' => '[Select Contact Type]',

                                            'Operational' => 'Operational',

                                            'Information Services' => 'Information Services',

                                            // 'Vendor' => 'Vendor',

                                            // 'Support' => 'Support',

                                             ], null, ['class' => 'form-control', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('office_phone', 'Office Phone:') !!}
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text bg-danger" id="basic-addon1"><i class="fa fa-exclamation-triangle"></i></span>
                        </div>
                        {!! Form::text('office_phone', null, ['class' => 'form-control', 'id'=>'office_phone','maxlength' => '10', 'required','Placeholder'=>'NUMBERS ONLY']) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label for="mobile_phone">Mobile Phone</label>
                    <input type="text" class="form-control" id="mobile_phone" placeholder="Mobile Phone" maxlength="10" minlength="10">
                </div>

                <div class="form-group">
                    {!! Form::label('email_address', 'Email Address:') !!}
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text bg-danger" id="basic-addon1"><i class="fa fa-exclamation-triangle"></i></span>
                        </div>
                        {!! Form::text('email_address', null, ['class' => 'form-control', 'id'=>'email_address', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('Pri', 'Contact Priority:') !!}
                    {!! Form::input('number', 'Pri', null, ['class' => 'form-control', 'id'=>'Pri', 'min'=>1]) !!}
                </div>

            </div>
            <div class="modal-footer bg-default">
                <div class="RightLeft">
                    {!! Form::submit('Add Contact', ['class'=>'btn btn-sgmc', 'id'=>'AddButton']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>