@extends('layouts.app')

@section('content')

    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading panel-heading-custom">
                <div class="panel-title">System Audit</div>
            </div>
            <div class="panel-body" style="height: calc(100vh - 140px); overflow-y: auto;">
                <table class="table table-responsive">
                    <thead>
                    <tr>
                        <td><b>System Name</b></td>
                        <td><b>Percent</b></td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($request as $requests)
                    <tr>
                        <td>{{$requests->system_name}}</td>
                        <td>
                            <div class="progress2">
                                <div class="progress-bar" role="progressbar" aria-valuenow="{{Round(($requests->Complete/($requests->Incomplete + $requests->Complete))*100,2)}}" aria-valuemin="0" aria-valuemax="100" style="width: {{Round(($requests->Complete/($requests->Incomplete + $requests->Complete))*100,2)}}%;">
                                    <span class="sr-only">{{Round(($requests->Complete/($requests->Incomplete + $requests->Complete))*100,2)}}%</span>
                                </div>
                            </div>
                        </td>
                        <td align="right">
                            <button id="{{$requests->system_name}}" onclick="chk(this)"  role="button"class="btn btn-primary btn-xs sysbutton"><span class="fa fa-cog " aria-hidden="true"></span> Detail</button>

                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="col-md-8">
        <div class="panel panel-default">
            <div class="panel-heading panel-heading-custom">
                <div class="panel-title">Details</div>
            </div>
            <div class="panel-body" style="height: calc(100vh - 140px); overflow-y: auto; overflow-x: auto;">
                <div class="panel-body" id="results"></div>
                <p id="demo"></p>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
<script type="application/javascript">



    function chk(elem) {
        var apt_id=elem.id;
        if (apt_id == "") {
            $('#results').fadeOut().html('<h2><div class="container-fluid alert-danger">You must enter an appointment ID</div></h2>').fadeIn();
            return false;
        }
        var dataString='system_name='+ apt_id;
        $.ajax({
            type:"GET",
            url: "/json/systemaudit",
            data:dataString,
            cashe:false,
            success: function(html){
                $('#results').fadeOut().html(html).fadeIn();
            },
            error : function() {
                $('#results').fadeOut().html('<h2><div class="container-fluid alert-danger">No Record Found</div></h2>').fadeIn();
            }
        })
    }







</script>
@endsection

