<div class="modal fade AddPillar_modal" id="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">
                <button type="button" class="close btn-sgmc" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus-square-o" aria-hidden="true"></i> <b>Add Pillar</b></h4>
            </div>
            <div class="modal-body">
                {!! Form::open(array('action' => ['LemController@addPillar'], 'class' => 'form_control')) !!}
                {!! Form::hidden('created_by', Auth::user()->id,[]) !!}
                {!! Form::hidden('lem_id', null,['class'=>'lem_id']) !!}
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="form-group">
                                    {!! Form::label('pillar_name', 'Pillar Name',['class'=>'pillar_name']) !!}
                                    {!! Form::text('pillar_name', null, ['class' => 'form-control pillar_name', 'id'=>'pillar_name', 'Required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('pillar_percent', 'Pillar Percent',['class'=>'pillar_name']) !!}
                                    {!! Form::text('pillar_percent', null, ['class' => 'form-control pillar_percent', 'id'=>'pillar_percent', 'Required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="RightLeft">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    {!! Form::submit('Add Pillar', ['class'=>'btn btn-sgmc', 'id'=>'AddButton']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>