<div class="modal fade AddEval" id="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">
                <button type="button" class="close btn-sgmc" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-file-o" aria-hidden="true"></i> <b>Add Evaluation</b></h4>
            </div>
            <div class="modal-body">
                {!! Form::open(array('action' => ['LemController@addEval'], 'class' => 'form_control')) !!}
                {!! Form::hidden('created_by', Auth::user()->id,['class'=>'id']) !!}
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="form-group">
                                    {!! Form::label('evaluation_name', 'Evaluation Name',['class'=>'ldac']) !!}
                                    {!! Form::text('evaluation_name', null, ['class' => 'form-control evaluation_name', 'id'=>'evaluation_name', 'Required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="RightLeft">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    {!! Form::submit('Add Eval', ['class'=>'btn btn-sgmc', 'id'=>'AddButton']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>