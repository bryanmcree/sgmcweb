<div class="modal fade AddGoal_modal" id="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">
                <button type="button" class="close btn-sgmc" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus-square-o" aria-hidden="true"></i> <b>Add Goal</b></h4>
            </div>
            <div class="modal-body">
                {!! Form::open(array('action' => ['LemController@addGoal'], 'class' => 'form_control')) !!}
                {!! Form::hidden('created_by', Auth::user()->id,[]) !!}
                {!! Form::hidden('lem_id', null,['class'=>'lem_id']) !!}
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="form-group">
                                    {!! Form::label('pillar_id', 'Select Pillar:') !!}
                                    <div class="input-group">
                                        <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                        <select name="pillar_id" class="form-control">
                                            <option value="">[Select Pillar]</option>
                                            @foreach($eval->lemPillars as $pillar)

                                                <option value="{{ $pillar->id }}"> {{$pillar->pillar_name}}</option>

                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('pillar_goal', 'Goal Description:') !!}
                                    {!! Form::textarea('pillar_goal', null, ['class' => 'form-control pillar_goal', 'id'=>'pillar_goal', 'rows' => 4]) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="RightLeft">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    {!! Form::submit('Add Goal', ['class'=>'btn btn-sgmc', 'id'=>'AddButton']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>