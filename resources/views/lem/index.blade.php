@if ( Auth::user()->hasRole('LEM - Development') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.app')
@section('content')

<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel panel-heading-custom">
            <div class="panel-title"><h2>Leadership Evaluating Management</h2></div>
        </div>
        <div class="panel-body">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#home"><b>Evaluation Templates</b></a></li>
                <li><a data-toggle="tab" href="#menu1"><b>Completed Evaluations</b></a></li>
            </ul>

            <div class="tab-content">
                <div id="home" class="tab-pane fade in active">
                    <br>
                    @if ($names->isEmpty())
                        <div class="bg-success alert"><h4><b>There are no LEM Evaluations.  Would you like to add one?</b></h4> <a href="#" class="btn btn-sgmc btn-xs EditTerm" style="display: inline-block;" data-toggle="modal" data-target=".AddEval"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Add Evaluation</a></div>
                    @else
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <td><b>Evlauation Name</b></td>
                            <td><b>Created By</b></td>
                            <td><b>Created At</b></td>
                            <td><b>Last Update</b></td>
                            <td align="right"><a href="#" class="btn btn-sgmc btn-xs" style="display: inline-block;" data-toggle="modal" data-target=".AddEval"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Add Evaluation</a></td>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($names as $name)
                        <tr>
                            <td>{{$name->evaluation_name}}</td>
                            <td>{{$name->createdBy->last_name}}, {{$name->createdBy->first_name}}</td>
                            <td>{{$name->created_at}}</td>
                            <td>{{$name->updated_at}}</td>
                            <td align="right">
                                <a href="lem/eval/{{$name->id}}" class="btn btn-primary btn-xs" style="display: inline-block;"><i class="fa fa-folder-open-o" aria-hidden="true"></i> Select Evaluation</a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @endif
                </div>
                <div id="menu1" class="tab-pane fade">
                    <table class="table">
                        <thead>
                        <tr>
                            <td><b>Evaluated</b></td>
                            <td><b>Template</b></td>
                            <td><b>Evaluator</b></td>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($evals as $eval)
                        <tr>
                            <td>{{$eval->evaluated->last_name}}, {{$eval->evaluated->first_name}}</td>
                            <td>{{$eval->lemName->evaluation_name}}</td>
                            <td>{{$eval->evalBy->last_name}}, {{$eval->evalBy->first_name}}</td>
                            <td>[PRINT] [EDIT]</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


    </div>
</div>


@include("lem.modals.AddEval")
@endsection


@section('scripts')


@endsection
@endif