{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('LEM - Development') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')
    {!! Form::open(array('action' => ['LemController@storeEval'], 'class' => 'form_control')) !!}
    {!! Form::hidden('lem_id', $eval->id,['class'=>'contacts_id']) !!}
    {!! Form::hidden('effective_date', $effective_date,['class'=>'contacts_id']) !!}
    {!! Form::hidden('emp_id', $emp->id,['class'=>'contacts_id']) !!}
<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2>{{$eval->evaluation_name}}</h2>
            <div class="panel-sub-title"><h4><b>{{$emp->first_name}} {{$emp->last_name}} - {{$emp->title}} - {{$effective_date}}</b></h4></div>
        </div>

        <div class="panel-body">
            <table class="table ">
                @foreach($eval->lemPillars as $pillar)
                    {!! Form::hidden('pillar_id[]', $pillar->id,['class'=>'contacts_id']) !!}
                <tr >
                    <th class="bg-warning" style="vertical-align:middle;" ><h4><b>{{strtoupper($pillar->pillar_name)}}</b></h4></th>
                    <th style="vertical-align:middle;" >{{$pillar->pillar_percent}}%</th>
                    <th style="vertical-align:middle;">


                        <table class="table table-bordered">
                            @foreach($pillar->lemGoals as $goals)
                                {!! Form::hidden('goal_id[]', $goals->id,['class'=>'contacts_id']) !!}
                            <tr >
                                <td style="vertical-align:middle;" width="5%" rowspan="1">{{Round($pillar->pillar_percent/$pillar->lemGoals->count(),3)}}%</td>
                                <td style="vertical-align:middle;" width="" rowspan="1" >{{$goals->pillar_goal}}</td>
                                <td style="vertical-align:middle;" rowspan="1"  width="6%">Previous</td>
                                <td style="vertical-align:middle;" rowspan="1"  width="12%">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                            <select name="actual_value[]" class="form-control pillar_id">
                                                <option value="">[Score]</option>

                                                <option value="1"> 1</option>
                                                <option value="2"> 2</option>
                                                <option value="3"> 3</option>
                                                <option value="4"> 4</option>
                                                <option value="5"> 5</option>

                                            </select>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            @endforeach
                        </table>




                    </th>
                </tr>
                @endforeach
                <tr>
                    <td>TEST</td>
                    <td>Lem Notes</td>
                </tr>
            </table>
        </div>
        <div class="panel-footer">
            <div class="RightLeft">
                <button type="button" class="btn btn-default" data-dismiss="modal">Clear</button>
                {!! Form::submit('Save Evaluation', ['class'=>'btn btn-sgmc', 'id'=>'updateButton']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
@endif