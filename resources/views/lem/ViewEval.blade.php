{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('LEM - Development') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.app')
{{--Page Design Goes Below--}}
@section('content')


    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"><h3><b>{{$eval->evaluation_name}}</b></h3></div>
            <div class="panel-body">
                <ul class="nav nav-pills">
                    <li class="active"><a href="/lem">Home</a></li>
                    <li><a href="#">Edit Evaluation</a></li>
                    <li><a href="#" class="AddPillar" data-toggle="modal" data-lem_id = "{{$eval->id}}" >Add Pillar</a></li>
                    <li><a href="#" class="AddGoal" data-toggle="modal" data-lem_id = "{{$eval->id}}" >Add Goal</a></li>
                </ul>
                <br>
                @foreach($eval->lemPillars as $pillar)
                <div class="col-md-12">
                    <div class="panel @if($pillar->lemGoals->isEmpty()) panel-danger @else panel-success @endif">
                        <div class="panel-heading ">
                            <h4><a href="#" class="EditPillar"
                                   data-pillar_id = "{{$pillar->id}}"
                                   data-pillar_name = "{{$pillar->pillar_name}}"
                                   data-pillar_percent = "{{$pillar->pillar_percent}}"
                                   data-lem_id = "{{$pillar->lem_id}}"
                                   style="display: inline-block;" data-toggle="modal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                <a href="#" class="DeletePillar"
                                   data-pillar_id = "{{$pillar->id}}"
                                   data-id = "{{$eval->id}}"
                                   style="display: inline-block;" data-toggle="modal">
                                    <i class="fa fa-trash" style="color:red" aria-hidden="true"></i></a>
                                <b> - {{strtoupper($pillar->pillar_name)}} - {{$pillar->pillar_percent}}%</b></h4>


                            <div class="panel-sub-title">
                                <i>Updated on {{$pillar->updated_at}} by {{$pillar->createdBy->first_name}} {{$pillar->createdBy->last_name}}</i>
                            </div>
                        </div>
                        <div class="panel-body">
                            @if(!$pillar->lemGoals->isEmpty())

                               <table class="table table-hover table-condensed">
                                   <thead>
                                   <tr>
                                       <td><B>Percent</B></td>
                                       <td><B>Group</B></td>
                                       <td><B>Goal</B></td>
                                       <td><B>Updated By</B></td>
                                       <td><B>Updated</B></td>
                                       <td></td>
                                   </tr>
                                   </thead>

                                   <tbody>
                                   @foreach($pillar->lemGoals as $goals)
                                   <tr>
                                       <td>{{Round($pillar->pillar_percent/$pillar->lemGoals->count(),3)}}%</td>
                                       <td>{{$pillar->pillar_name}}</td>
                                       <td width="60%">{{$goals->pillar_goal}}</td>
                                       <td nowrap="">{{$goals->createdBy->last_name}}, {{$goals->createdBy->first_name}}</td>
                                       <td nowrap="">{{$goals->updated_at}}</td>
                                       <td align="right"><h4><a href="#" class="EditGoal"
                                                  data-pillar_id = "{{$pillar->id}}"
                                                  data-pillar_goal = "{{$goals->pillar_goal}}"
                                                  data-goals_percent = "{{$goals->pillar_percent}}"
                                                  data-lem_id = "{{$pillar->lem_id}}"
                                                  style="display: inline-block;" data-toggle="modal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                               <a href="#" class="DeletePillar"
                                                  data-pillar_id = "{{$pillar->id}}"
                                                  style="display: inline-block;" data-toggle="modal">
                                                   <i class="fa fa-trash" style="color:red" aria-hidden="true"></i></a>
                                               </h4></td>
                                   </tr>
                                   @endforeach
                                   </tbody>
                               </table>
                            @endif
                        </div>
                    </div>
                </div>
                @endforeach
                <div class="col-md-12">
                    <div class="panel @if($eval->lemPillars->sum('pillar_percent') != 100) panel-danger @else panel-success @endif">
                        <div class="panel-heading">
                            <h4><b>Totals</b> - {{$eval->lemPillars->sum('pillar_percent')}}%</h4>
                        </div>
                        <div class="panel-body">
                            {{$eval->lemPillars->sum('pillar_percent')}}%
                            @if($eval->lemPillars->sum('pillar_percent') != 100)
                                <i class="fa fa-exclamation-triangle" style="color: red" aria-hidden="true"></i> <b><i>Value has to equal 100%.  Check your percents above.</i></b>
                            @else
                                <i class="fa fa-thumbs-up" style="color:green" aria-hidden="true"></i>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@include("lem.modals.EditPillar")
@include("lem.modals.EditGoal")
@include("lem.modals.AddPillar")
@include("lem.modals.AddGoal")

{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">
    $(document).on("click", ".EditPillar", function () {
        //alert($(this).attr("data-pillar_id"));
        $('.lem_id').val($(this).attr("data-lem_id"));
        $('.pillar_id').val($(this).attr("data-pillar_id"));
        $('.pillar_name').val($(this).attr("data-pillar_name"));
        $('.pillar_percent').val($(this).attr("data-pillar_percent"));
        $('.EditPillar_modal').modal('show');
    });

    $(document).on("click", ".EditGoal", function () {
        //alert($(this).attr("data-pillar_id"));
        $('.lem_id').val($(this).attr("data-lem_id"));
        $('.pillar_id').val($(this).attr("data-pillar_id"));
        $('.pillar_goal').val($(this).attr("data-pillar_goal"));
        $('.pillar_percent').val($(this).attr("data-pillar_percent"));
        $('.EditGoal_modal').modal('show');
    });

    $(document).on("click", ".AddPillar", function () {
        //alert($(this).attr("data-lem_id"));
        $('.lem_id').val($(this).attr("data-lem_id"));
        $('.AddPillar_modal').modal('show');
    });

    $(document).on("click", ".AddGoal", function () {
        //alert($(this).attr("data-lem_id"));
        $('.lem_id').val($(this).attr("data-lem_id"));
        $('.AddGoal_modal').modal('show');
    });







    $(document).on("click", ".DeletePillar", function () {
        //alert($(this).attr("data-lem_id"));
        var pillarId = $(this).attr("data-pillar_id");
        var Id = $(this).attr("data-id");
        deletePillar(pillarId, Id);
    });

    function deletePillar(pillarId, Id) {
        swal({
            title: "Delete Pillar?",
            text: "Are you sure you want to delete this pillar and ALL goals??",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            confirmButtonText: "Yes, delete it!",
            confirmButtonColor: "#ec6c62"
        }, function() {
            $.ajax({
                url: "/lem/pillar/del/" + pillarId + "?eval=" + Id ,
                type: "GET"
            })
                    .done(function(data) {
                        swal({
                                    title: "Deleted",
                                    text: "System Contact was Deleted",
                                    type: "success",
                                    timer: 1800,
                                    showConfirmButton: false

                                }
                        );
                        setTimeout(function(){window.location.replace('/systemlist/'+ systemId)},1900);
                    })
                    .error(function(data) {
                        swal("Oops", "We couldn't connect to the server!", "error");
                    });
        });
    }
</script>

@endsection
@endif