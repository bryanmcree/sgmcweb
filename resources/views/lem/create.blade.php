{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('LEM - Development') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')
<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel panel-heading"><h2><b>LEM</b></h2></div>
        <div class="panel-body">
            {!! Form::open(array('action' => ['LemController@newEval'], 'class' => 'form_control')) !!}
            {!! Form::hidden('emp_id', $emp->id,['class'=>'emp_id']) !!}
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading panel-heading-custom">
                        <div class="panel-title"><h4><b>Employee Selection</b></h4></div>
                    </div>
                    <div class="panel-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <td><b>Pic</b></td>
                                <td><b>Name</b></td>
                                <td><b>Select Evaluation</b></td>
                                <td><B>Effective Date</b></td>
                                <td><B>History</b></td>
                            </tr>
                            </thead>

                            <tbody>
                            <tr>
                                <td style="vertical-align:middle; text-align:center;">@if($emp->photo != '')<img src="data:image/jpeg;base64,{{str_replace('</photo>','',str_replace('<photo>','',$emp->photo))}}" height="60" width="60"/>@else {{ Html::image('img/nopic.png', 'alt', array( 'width' => 60, 'height' => 60 )) }} @endif</td>
                                <td>{{$emp->last_name}}, {{$emp->first_name}}<br>{{$emp->title}}</td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                            <select name="eval_id" class="form-control">
                                                <option value=""></option>
                                                @foreach($eval as $pillar)
                                                    <option value="{{ $pillar->id }}"> {{$pillar->evaluation_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            {!! Form::input('date','effective_date', '', ['class' => 'form-control','required']) !!}
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                            <select name="last" class="form-control">
                                                <option value="1">Last Evaluation</option>
                                                <option value="3"> Last 3 Evaluations</option>
                                                <option value="5"> Last 5 Evaluations</option>
                                            </select>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>


                    </div>
                    <div class="panel-footer">
                        <div class="RightLeft">
                            {!! Form::reset('Clear', ['class'=>'btn btn-default']) !!}
                            {!! Form::submit('Start Evaluation', ['class'=>'btn btn-default']) !!}
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
@endif