{{--New file Template--}}

{{--Add Security for this page below--}}
@if( Auth::user()->hasRole('Stats Admin') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    <div class="card bg-dark text-white mb-3">
        <div class="card-header">
            Edit Stat {{ $stat->campus }} {{ $stat->stat_description }}
            <span class="float-right"><a href="/stats/detail/{{$stat->id}}" class="btn btn-secondary btn-sm">Return To Detail</a></span>
        </div>

        <form action="/stats/update/{{$stat->id}}" method="POST">
            {{ csrf_field() }}

            <div class="card-body">

                <div class="card-deck">

                    <div class="card bg-dark text-white mb-2" style="border-radius:0px;">
                        <div class="card-body">
                            <table class="table table-hover mt-3">
                                <tbody>
                                    <tr>
                                        <td>Description:</td>
                                        <td><input type="text" class="form-control" name="stat_description" value="{{$stat->stat_description}}"></td>
                                    </tr>
                                    <tr>
                                        <td>Stat Type:</td>
                                        <td><input type="text" class="form-control" name="stat_type" value="{{$stat->stat_type}}"></td>
                                    </tr>
                                    <tr>
                                        <td>Stat Location:</td>
                                        <td><input type="text" class="form-control" name="stat_location" value="{{$stat->stat_location}}"></td>
                                    </tr>
                                    <tr>
                                        <td>Campus:</td>
                                        <td>
                                            <select name="campus" class="form-control">
                                                <option value="{{$stat->campus}}" selected>{{$stat->campus}}</option>
                                                <option value="SGMC">SGMC</option>
                                                <option value="LSH">LSH</option>
                                                <option value="BCH">BCH</option>
                                                <option value="SNH">SNH</option>
                                                <option value="Villa">Villa</option>
                                                <option value="SPN">SPN</option>
                                                <option value="Reimbursement">Reimbursement</option>
                                                <option value="Multiple">Multiple/Combined</option>
                                                <option value="Clinic">Clinic</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Campus ID:</td>
                                        <td>
                                            <select name="campus_number" class="form-control">
                                                <option value="{{$stat->campus_number}}" selected>{{$stat->campus_number}}</option>
                                                <option value="0">0</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Dept2:</td>
                                        <td><input type="text" class="form-control" name="dept2" value="{{$stat->dept2}}"></td>
                                    </tr>

                                    <tr>
                                        <td>Validated:</td>
                                        <td>
                                            @if(is_null($stat->validated_date))
                                                <select name="stat_validated" class="form-control">
                                                    <option value="0" selected>No</option>
                                                    <option value="1">Yes</option>
                                                </select>
                                            @else
                                            Yes, on {{$stat->validated_date->format('m-d-Y')}} by {{$stat->validatedBy->name}} <input class="form-control form-check-input float-right" type="checkbox" value="0" name="stat_validated">
                                            @endif
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="card bg-dark text-white mb-2" style="border-radius:0px;">
                        <div class="card-body">
                            <table class="table table-hover mt-3">
                                <tbody>
                                    <tr>
                                        <td>GL Code:</td>
                                        <td><input type="text" class="form-control" name="concatenate" value="{{$stat->concatenate}}"></td>
                                    </tr>
                                    <tr>
                                        <td>Organization:</td>
                                        <td><input type="text" class="form-control" name="org" value="{{$stat->org}}"></td>
                                    </tr>
                                    <tr>
                                        <td>Dept1:</td>
                                        <td><input type="text" class="form-control" name="dept1" value="{{$stat->dept1}}"></td>
                                    </tr>
                                    <tr>
                                        <td>Prime:</td>
                                        <td><input type="text" class="form-control" name="prime" value="{{$stat->prime}}"></td>
                                    </tr>
                                    <tr>
                                        <td>Sub:</td>
                                        <td><input type="text" class="form-control" name="sub" value="{{$stat->sub}}"></td>
                                    </tr>
                                    <tr>
                                        <td>Stat Link:</td>
                                        <td><input type="text" class="form-control" name="stat_link" value="{{$stat->stat_link}}"></td>
                                    </tr>
                                    <tr>
                                        <td>Flex Stat:</td>
                                        <td>
                                            <select name="flex_stat" class="form-control">
                                                <option value="{{$stat->flex_stat}}" selected> @if($stat->flex_stat == 0) No @else Yes @endif </option>
                                                <option value="0">No</option>
                                                <option value="1">Yes</option>
                                            </select>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="card bg-dark text-white mb-2" style="border-radius:0px;">
                        <div class="card-body">
                            <table class="table table-hover mt-3">
                                <tbody>
                                    <tr>
                                        <td>Calculated Field:</td>
                                        <td>
                                            <select name="calc_field" class="form-control">
                                                <option value="{{$stat->calc_field}}" selected> @if($stat->calc_field == 0) No @else Yes @endif </option>
                                                <option value="0">No</option>
                                                <option value="1">Yes</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Number Format:</td>
                                        <td>
                                            <select name="num_format" class="form-control">
                                                <option value="{{$stat->num_format}}" selected>{{$stat->num_format}}</option>
                                                <option value="Number">Number</option>
                                                <option value="Percent">Percent</option>
                                                <option value="Currency">Currency</option>
                                                <option value="Decimal">Decimal</option>
                                                <option value="Decimal4">Decimal4</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Zero Level:</td>
                                        <td>
                                            <select name="zero_stat" class="form-control">
                                                <option value="{{$stat->zero_stat}}" selected> @if($stat->zero_stat == 0) No @else Yes @endif </option>
                                                <option value="0">No</option>
                                                <option value="1">Yes</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Auto Fill:</td>
                                        <td>
                                            <select name="auto_fill" class="form-control">
                                                <option value="{{$stat->auto_fill}}" selected> @if($stat->auto_fill == 0) No @else Yes @endif </option>
                                                <option value="0">No</option>
                                                <option value="1">Yes</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Flag for Review:</td>
                                        <td>
                                            <select name="flag_review" class="form-control">
                                                <option value="{{$stat->flag_review}}" selected> @if($stat->flag_review == 0) No @else Yes @endif </option>
                                                <option value="0">No</option>
                                                <option value="1">Yes</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Stat Total:</td>
                                        <td>
                                            <select name="stat_total" class="form-control">
                                                <option value="{{$stat->stat_total}}" selected> {{$stat->stat_total}} </option>
                                                <option value="SUM">SUM</option>
                                                <option value="AVG">AVG</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Finance Stat:</td>
                                        <td>
                                            <select name="finance_stat" class="form-control">
                                                <option value="{{$stat->finance_stat}}" selected> @if($stat->finance_stat == 1) Yes @else No @endif </option>
                                                <option value="1">Yes</option>
                                                <option value="0">No</option>
                                            </select>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div> <!-- Card Deck -->
                <div class="card-deck">
                    <div class="card bg-dark text-white mb-2">
                        <div class="card-header">
                            <b>Notes</b>
                        </div>
                        <div class="card-body">
                            <textarea id="example" name="stat_note" style="height:300px;width:100%;">{{$stat->stat_note}}</textarea>
                        </div>
                    </div>
                    <div class="card bg-dark text-white mb-2">
                        <div class="card-header">
                            <b>Clarity SQL Script</b>
                        </div>
                        <div class="card-body">
                            <textarea id="stat_script" name="stat_script" style="height:300px;width:100%;">{{$stat->stat_script}}</textarea>
                        </div>
                    </div>
                    <div class="card bg-dark text-white mb-2">
                        <div class="card-header">
                            <b>Formula</b>
                        </div>
                        <div class="card-body">
                            <textarea id="stat_formula" name="stat_forumula" style="height:300px;width:100%;">{{$stat->stat_forumula}}</textarea>
                        </div>
                    </div>
                </div>

            </div> <!-- Parent Card Body -->

            <div class="card-footer">
                <input type="submit" class="btn btn-block btn-success" value="Update">
            </div>

        </form>

    </div>


@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script>
        var textarea = document.getElementById('example');
        sceditor.create(textarea, {
            format: 'bbcode'
        });

        var textarea = document.getElementById('stat_script');
        sceditor.create(textarea, {
            format: 'bbcode'
        });

        var textarea = document.getElementById('stat_formula');
        sceditor.create(textarea, {
            format: 'bbcode'
        });

        var themeInput = document.getElementById('theme');
        themeInput.onchange = function() {
            var theme = '../minified/themes/' + themeInput.value + '.min.css';

            document.getElementById('theme-style').href = theme;
        };
    </script>

@endsection

@endif