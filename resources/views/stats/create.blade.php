{{--New file Template--}}

{{--Add Security for this page below--}}
@if( Auth::user()->hasRole('Stats Admin') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    <div class="card bg-dark text-white mb-3">
        <div class="card-header">
            Create New Stat
            <span class="float-right"><a href="/stats" class="btn btn-secondary btn-sm">Return To Dashboard</a></span>
        </div>

        <form action="/stats/store" method="POST">
            {{ csrf_field() }}

            <div class="card-body">

                <div class="card-deck">

                    <div class="card bg-dark text-white mb-2" style="border-radius:0px;">
                        <div class="card-body">
                            <table class="table table-hover mt-3">
                                <tbody>
                                    <tr>
                                        <td>Description:</td>
                                        <td><input type="text" class="form-control" name="stat_description"></td>
                                    </tr>
                                    <tr>
                                        <td>Stat Type:</td>
                                        <td><input type="text" class="form-control" name="stat_type"></td>
                                    </tr>
                                    <tr>
                                        <td>Stat Location:</td>
                                        <td><input type="text" class="form-control" name="stat_location"></td>
                                    </tr>
                                    <tr>
                                        <td>Campus:</td>
                                        <td>
                                            <select name="campus" class="form-control">
                                                <option value="">Choose Campus</option>
                                                <option value="SGMC">SGMC</option>
                                                <option value="LSH">LSH</option>
                                                <option value="BCH">BCH</option>
                                                <option value="SNH">SNH</option>
                                                <option value="Villa">Villa</option>
                                                <option value="SPN">SPN</option>
                                                <option value="Reimbursement">Reimbursement</option>
                                                <option value="Multiple">Multiple/Combined</option>
                                                <option value="Clinic">Clinic</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Campus ID:</td>
                                        <td>
                                            <select name="campus_number" class="form-control">
                                                <option value="" selected>Choose Campus #</option>
                                                <option value="0">0</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Dept2:</td>
                                        <td><input type="text" class="form-control" name="dept2"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="card bg-dark text-white mb-2" style="border-radius:0px;">
                        <div class="card-body">
                            <table class="table table-hover mt-3">
                                <tbody>
                                    <tr>
                                        <td>GL Code:</td>
                                        <td><input type="text" class="form-control" name="concatenate"></td>
                                    </tr>
                                    <tr>
                                        <td>Organization:</td>
                                        <td><input type="text" class="form-control" name="org"></td>
                                    </tr>
                                    <tr>
                                        <td>Dept1:</td>
                                        <td><input type="text" class="form-control" name="dept1"></td>
                                    </tr>
                                    <tr>
                                        <td>Prime:</td>
                                        <td><input type="text" class="form-control" name="prime"></td>
                                    </tr>
                                    <tr>
                                        <td>Sub:</td>
                                        <td><input type="text" class="form-control" name="sub"></td>
                                    </tr>
                                    <tr>
                                        <td>Stat Link:</td>
                                        <td><input type="text" class="form-control" name="stat_link"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="card bg-dark text-white mb-2" style="border-radius:0px;">
                        <div class="card-body">
                            <table class="table table-hover mt-3">
                                <tbody>
                                    <tr>
                                        <td>Calculated Field:</td>
                                        <td>
                                            <select name="calc_field" class="form-control">
                                                <option value="" selected>Yes or No</option>
                                                <option value="0">No</option>
                                                <option value="1">Yes</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Number Format:</td>
                                        <td>
                                            <select name="num_format" class="form-control">
                                                <option value="" selected>Choose Format</option>
                                                <option value="Number">Number</option>
                                                <option value="Percent">Percent</option>
                                                <option value="Currency">Currency</option>
                                                <option value="Decimal">Decimal</option>
                                                <option value="Decimal4">Decimal4</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Stat Formula:</td>
                                        <td><input type="text" class="form-control" name="stat_forumula"></td>
                                    </tr>
                                    <tr>
                                        <td>Auto Fill:</td>
                                        <td>
                                            <select name="auto_fill" class="form-control">
                                                <option value="" selected>Yes or No</option>
                                                <option value="0">No</option>
                                                <option value="1">Yes</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Flag for Review:</td>
                                        <td>
                                            <select name="flag_review" class="form-control">
                                                <option value="" selected>Yes or No</option>
                                                <option value="0">No</option>
                                                <option value="1">Yes</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Notes:</td>
                                        <td><input type="text" class="form-control" name="stat_note"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div> <!-- Card Deck -->

            </div> <!-- Parent Card Body -->

            <div class="card-footer">
                <input type="submit" class="btn btn-block btn-success" value="Submit">
            </div>

        </form>

    </div>

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection

@endif