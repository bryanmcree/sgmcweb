{{--New file Template--}}

{{--Add Security for this page below--}}
@if( Auth::user()->hasRole('Stats Admin') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    <div class="card bg-dark text-white mb-3 mx-auto" style="width:50%;">
        <div class="card-header">
            New Stat Value
            <span class="float-right"><a href="/stats" class="btn btn-secondary btn-sm">Return To Dashboard</a></span>
        </div>

        <form action="/stats/endUser/store" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="created_by" value="{{ Auth::user()->employee_number}}">

            <div class="card-body">

                <div class="row">
                    <div class="form-group col-lg-6">
                        <label>Start Date</label>
                        <input type="date" class="form-control" name="start_date" required>
                    </div>
                    <div class="form-group col-lg-6">
                        <label>End Date</label>
                        <input type="date" class="form-control" name="end_date" required>
                    </div>
                </div>

                <div class="form-group">
                    <label>Stat Description</label>
                    <select name="stat_link" class="form-control" id="select1" required> <!-- onchange="populate(this.id, 'select2')" -->
                        <option value="" selected>[Select Stat]</option>
                        <option value="SGMC_Food Services_All Spice_new">SGMC Food Services All Spice Meals</option>
                        <option value="SGMC_Food Services_SGMC Cafeteria_new">SGMC Food Services Cafeteria Meals</option>
                        <option value="SNH_Food Services_SGMC Outpatient Plaza_new">SNH Food Services Outpatient Plaza Meals</option>
                        <option value="SGMC_Security_Assistance Calls Completed_new">SGMC Security Assistance Calls Completed</option>
                        <option value="SGMC_Security_Auto Related_new">SGMC Security Auto Related</option>
                        <option value="SGMC_Security_Facility Access_new">SGMC Security Facility Access</option>
                        <option value="SGMC_Security_Police Intervention_new">SGMC Security Police Intervention</option>
                        <option value="SGMC_Security_Visitor Incidents/Accidents_new">SGMC Security Visitor Incidents</option>
                        <option value="Doses Dispensed_flex_stat">SGMC Pharmacy Doses Dispensed</option>
                        <option value="MCP/Retail Pharmacy Prescription Filled_flex_stat">SGMC MCP/Retail Pharmacy Prescription Filled</option>
                        <option value="LSH Pharmacy Doses">LSH Pharmacy Unit Doses</option>
                        <option value="education_hours">SGMC Education Hours</option>
                        <option value="Items Received_flex_stat">SGMC Materials Management Items Recieved</option>
                        <option value="sterile_processing_stat">SGMC Sterile Processing Cases</option>
                        <option value="BCH_Engineering_Work Orders Completed_new">BCH Engineering Work Orders Completed</option>
                        <option value="BCH_Engineering_Work Orders Created_new">BCH Engineering Work Orders Created</option>
                        <option value="LSH_Engineering_Work Orders Completed_new">LSH Engineering Work Orders Completed</option>
                        <option value="LSH_Engineering_Work Orders Created_new">LSH Engineering Work Orders Created</option>
                        <option value="SGMC_Engineering_Work Orders Completed_new">SGMC Engineering Work Orders Completed</option>
                        <option value="SGMC_Engineering_Work Orders Created_new">SGMC Engineering Work Orders Created</option>
                        <option value="SNH_Engineering_Work Orders Completed_new">SNH Engineering Work Orders Completed</option>
                        <option value="SNH_Engineering_Work Orders Created_new">SNH Engineering Work Orders Created</option>
                        <option value="Villa_Engineering_Work Orders Completed_new">Villa Engineering Work Orders Completed</option>
                        <option value="Villa_Engineering_Work Orders Created_new">Villa Engineering Work Orders Created</option>
                    </select>
                </div>

                {{-- <div class="form-group">
                    <label>Stat Description</label>
                    <select name="stat_description" class="form-control" id="select2" required>
                        
                    </select>
                </div> --}}

                <div class="form-group">
                    <label>Value</label>
                    <input type="number" step="any" class="form-control" name="value" required>
                </div>

            </div> <!-- Card Body -->

            <div class="card-footer">
                <input type="submit" class="btn btn-block btn-primary">
            </div>

        </form>

    </div>

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

<script>

   function populate(s1,s2) 
   {
        var s1 = document.getElementById(s1);
        var s2 = document.getElementById(s2);

        s2.innerHTML = "";

        if(s1.value == "7070-PHARMACY")
        {
            var optionArray = ["Doses Dispensed|Doses Dispensed"];
        }
        else if(s1.value == "7545-SNH - CENTRAL STERILE")
        {
            var optionArray = ["Items Processed|Items Processed"];
        }
        else if(s1.value == "8331-MATERIALS MANAGEMENT")
        {
            var optionArray = ["Items Received|Items Received"];
        }
        else if(s1.value == "7069-O/P PHARMACY")
        {
            var optionArray = ["Rx Processed|Rx Processed"];
        }
        else if(s1.value == "7610-SNH - FOOD & NUTR" || s1.value == "8050-FOOD SERVICES")
        {
            var optionArray = ["Total Meals|Total Meals"];
        }
        else if(s1.value == "6020-CLINICAL INFORMATICS" || s1.value == "8231-INFORMATION SERVICES")
        {
            var optionArray = ["Total Users|Total Users"];
        }
        else if(s1.value == "8060-ENGINEERING" || s1.value == "7378-ENGINEERING" || s1.value == "7665-ENGINEERING" || s1.value == "6584-ENGINEERING" || s1.value == "6710-ENGINEERING")
        {
            var optionArray = ["Work Orders Created|Work Orders Created", "Work Orders Completed|Work Orders Completed"];
        }
    

        for(var option in optionArray)
        {
            var pair = optionArray[option].split("|");
            var newOption = document.createElement("option");
            newOption.value = pair[0];
            newOption.innerHTML = pair[1];
            s2.options.add(newOption);
        }
   }

</script>

@endsection

@endif