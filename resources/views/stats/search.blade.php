{{--New file Template--}}

{{--Add Security for this page below--}}

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')


    <div class="card mb-4 text-white bg-dark" style="border-radius:0px;">
        <div class="card-body">

            <div class="row mb-3">

                <div class="col-lg-4">
                    <form class="form-inline pl-1" action="/stats/search" method="get">
                        <input class="form-control mr-sm-2 custom-select-sm col-lg-8" type="text" name="search" placeholder="Stat Search...">
                        <button class="btn btn-sm text-white col-lg-3" style="background-color:#a228ff;" type="submit">Search</button>
                    </form>
                </div>

                <div class="col-lg-4">

                </div>

                <div class="col-lg-4">
                    <span class="float-right"><a href="/stats" class="btn btn-secondary btn-sm">Return To Dashboard</a></span>
                </div>

            </div>

            <hr>

            @if($results->isEmpty())
                <div class="alert bg-info">No results found</div>
            @else

                <div class="table-responsive">
                    <table class="nowrap table table-dark table-striped table-hover table-bordered" width="100%" id="statSearch">
                        <thead>
                            <tr>
                                @if( Auth::user()->hasRole('Stats Admin') == True)
                                    <th class="no-sort text-center"><a href="/stats/create"><i class="fas fa-plus-square" title="Add New Stat" style="color:#00a560"></i></a></th>
                                @else
                                    <th> </th>
                                @endif
                                <th><B>Campus</B></th>
                                <th><B>Type</B></th>
                                <th><B>Description</B></th>
                                <th>Stat Link</th>
                                <?PHP $datacount = -1?>
                                @foreach($report_dates as $report_date)

                                    <th><B>{{$report_date->start_date->format('m - Y')}}</B></th>

                                @endforeach
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($results as $result)
                                <tr>
                                    <td class="text-center"><a href="/stats/detail/{{$result->id}}" title="View Details" target="_blank"><i class="fas fa-info-circle @if(!is_null($result->validated_date)) text-success @endif"></i></a></td>
                                    <td>{{$result->campus}}</td>
                                    <td>{{$result->stat_type}}</td>
                                    <td>{{$result->stat_description}}
                                        @if($result->flag_review ==1) <i class="fas fa-flag" style="color:gold;"></i>  @endif
                                        @if($result->calc_field ==1) <i class="fas fa-calculator" style="color:#ff916d;"></i> @endif
                                        @if($result->auto_fill ==1) <i class="fas fa-magic" style="color:#14dae5;"></i> @endif
                                    </td>
                                    <td>{{ $result->stat_link }}</td>

                                    @foreach($report_dates as $report_date_test) 
                                        <td>
                                            @foreach($result->statsValues as $values)
                                                @if($report_date_test->start_date == $values->start_date) {{-- Alakazam --}}

                                                    @if($result->num_format == 'Number')
                                                        {{number_format($values->value)}}
                                                    @endif
                                                    @if($result->num_format == 'Currency')
                                                        ${{number_format($values->value)}}
                                                    @endif

                                                    @if($result->num_format == 'Percent')
                                                        {{ number_format(($values->value * 100), 2) }}%
                                                    @endif

                                                    @if($result->num_format == 'Decimal')
                                                        {{number_format($values->value,2)}}
                                                    @endif

                                                    @if($result->num_format == 'Decimal4')
                                                        {{number_format($values->value,4)}}
                                                    @endif

                                                @endif
                                            @endforeach 
                                                
                                        </td> 

                                    @endforeach 
                                </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            @endif
        </div>
    </div>


@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">
    $(document).ready(function() {
        $('#statSearch').DataTable( {
            "pageLength": 25,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>    

@endsection
