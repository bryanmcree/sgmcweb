{{--New file Template--}}

{{--Add Security for this page below--}}

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('css')
    
    <style>
        
        @keyframes down
        {
            0% {transform: translateY(-3%); opacity: 1;}
            100% { transform: translateY(3%); opacity: 0.2;}
        }

        #move-down{
            -webkit-animation:linear infinite alternate;
            -webkit-animation-name: down;
            -webkit-animation-duration: 1.5s;
        }

        @keyframes up
        {
            from {transform: translateY(3%); opacity: 1;}
            to { transform: translateY(-3%); opacity: 0.2;}
        }

        #move-up{
            -webkit-animation:linear infinite alternate;
            -webkit-animation-name: up;
            -webkit-animation-duration: 1.5s;
        }

        @keyframes left
        {
            from {transform: translateX(3%); opacity: 1;}
            to { transform: translateX(-3%); opacity: 0.2;}
        }

        #move-left{
            -webkit-animation:linear infinite alternate;
            -webkit-animation-name: left;
            -webkit-animation-duration: 1.5s;
        }

        @keyframes right
        {
            from {transform: translateX(-3%); opacity: 1;}
            to { transform: translateX(3%); opacity: 0.2;}
        }

        #move-right{
            -webkit-animation:linear infinite alternate;
            -webkit-animation-name: right;
            -webkit-animation-duration: 1.5s;
        }

        #border-bottom{
            border-bottom:solid 1px rgba(74, 74, 74, .3);
        }

    </style>

@endsection

@section('content')


    <div class="card mb-2 text-white bg-secondary" style="border-radius:0px;">

        <div class="card-header">

            <a href="/stats" class="btn btn-dark btn-sm"><i class="far fa-arrow-alt-circle-left"></i></a> &nbsp; {{$stat_detail->stat_type}} - {{$stat_detail->campus}} - {{$stat_detail->stat_description}} - <b>{{$stat_detail->concatenate}}</b>
            <span class="float-right">



                <ul  class="nav nav-pills red">
                    <li class="nav-item">
                        <a class="dropdown-item active text-white" id="details-tab" data-toggle="tab" href="#details" role="tab" aria-controls="details" aria-selected="true">Stat Details</a>
                    </li>
                    <li class="nav-item">
                        <a class="dropdown-item text-white" id="table-tab" data-toggle="tab" href="#table" role="tab" title="View stat in table view next to budget and by period." aria-controls="table" aria-selected="false">Table View</a>
                    </li>
                    <li class="nav-item">
                        <a class="dropdown-item text-white" id="notes-tab" data-toggle="tab" href="#notes" role="tab" title="Description of the stat." aria-controls="notes" aria-selected="false">Notes</a>
                    </li>
                    <li class="nav-item">
                        <a class="dropdown-item text-white" id="clarity-tab" data-toggle="tab" href="#clarity" title="This script comes from Epic" role="tab" aria-controls="clarity" aria-selected="false">Clarity Script</a>
                    </li>
                    @if($stat_detail->calc_field == 1)
                    <li class="nav-item">
                        <a class="dropdown-item text-white" id="calc-tab" data-toggle="tab" href="#calc" role="tab" aria-controls="calc" aria-selected="false">Calculated Details</a>
                    </li>
                    @endif
                    <li class="nav-item">
                        <a class="dropdown-item text-white" id="download-tab" data-toggle="tab" href="#" title="Download stat into Excel format." role="tab" aria-controls="clarity" aria-selected="false">Download Stat</a>
                    </li>
                    @if( Auth::user()->hasRole('Stats Admin') == True)
                    <li class="nav-item">
                        @if(!empty($stat_detail->stat_link))
                            <a href="#" class="statValues btn btn-success" title="Add Stat Value"><i class="fas fa-plus"></i></a> &nbsp;
                        @else
                            <small class="text-danger"><i>You must create a stat link before adding values to stat.</i></small> &nbsp;
                        @endif
                    </li>
                    <li class="nav-item">
                        <a href="/stats/edit/{{$stat_detail->id}}" class="btn btn-warning" title="Edit Stat"><i class="fas fa-edit"></i></a> &nbsp;
                    </li>
                    <li class="nav-item">
                        <a href="#" stat_id="{{$stat_detail->id}}" class="deletebtn btn btn-danger" title="Deactivate Stat"><i class="fas fa-minus-circle"></i></a>
                    </li>
                    @endif
                </ul>
            </span>

        </div>

        <!-- Stats Detail BODY -->
        <div class="card-body">

            <div class="tab-content" id="myTabContent">

                <div class="tab-pane fade show active" id="details" role="tabpanel" aria-labelledby="details-tab">

                    <div class="card-group mb-3">

                        <div class="card" style="box-shadow: none !important; background-color:#343a40; border-radius:0px;"> 
                            <div class="card-body">

                                <h2 class="card-title text-center">
                                    @if($stat_var == 0)
                                        --
                                    @elseif($stat_var > 0)
                                        <div style="color:green"><i class="far fa-arrow-up"></i> {{round($stat_var,2)}}%</div>
                                    @else
                                        <div style="color:red"><i class="far fa-arrow-down"></i> {{round($stat_var,2)}}%</div>
                                    @endif
                                </h2>

                                <p class="text-center text-muted">Month Variance</p>

                                @if($stat_detail_values->isEmpty())
                                    <div class="alert alert-info">No data for chart</div>
                                @else
                                    <canvas id="bar-chart-monthly" height="105"></canvas>
                                @endif

                                <div class="row m-3">
                                    <div class="col-6 text-center m-0" style="border-right: 1px solid white;">
                                        <h3>
                                            @if(!is_null($stat_previous))
                                                @if($stat_detail->num_format == 'Number')
                                                    {{number_format($stat_previous->value)}}
                                                @endif
                                                @if($stat_detail->num_format == 'Currency')
                                                    ${{number_format($stat_previous->value,2)}}
                                                @endif
                                                @if($stat_detail->num_format == 'Percent')
                                                    {{ number_format(($stat_previous->value * 100), 2) }}%
                                                @endif
                                                @if($stat_detail->num_format == 'Decimal')
                                                    {{number_format($stat_previous->value,2)}}
                                                @endif
                                                @if($stat_detail->num_format == 'Decimal4')
                                                    {{number_format($stat_previous->value,4)}}
                                                @endif
                                            @endif
                                        </h3>
                                        @if(!is_null($stat_previous))
                                            {{$stat_previous->start_date->format('M-y')}}
                                        @endif
                                    </div>
                                    <div class="col-6 text-center m-0">
                                        <h3>
                                            @if(!is_null($stat_current))
                                                @if($stat_detail->num_format == 'Number')
                                                    {{number_format($stat_current->value)}}
                                                @endif
                                                @if($stat_detail->num_format == 'Currency')
                                                    ${{number_format($stat_current->value,2)}}
                                                @endif
                                                @if($stat_detail->num_format == 'Percent')
                                                    {{ number_format(($stat_current->value * 100), 2) }}%
                                                @endif
                                                @if($stat_detail->num_format == 'Decimal')
                                                    {{number_format($stat_current->value,2)}}
                                                @endif
                                                @if($stat_detail->num_format == 'Decimal4')
                                                    {{number_format($stat_current->value,4)}}
                                                @endif
                                            @endif
                                        </h3>
                                        @if(!is_null($stat_current))
                                            {{$stat_current->start_date->format('M-y')}}
                                        @endif
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="card" style="box-shadow: none !important; background-color:#343a40; border-radius:0px;">
                            <div class="card-body">

                                <h2 class="card-title text-center">
                                    12 Month Trend Line
                                </h2>

                                <hr>

                                @if($stat_detail_values->isEmpty())
                                    <div class="alert alert-info">No data for chart</div>
                                @else
                                    <canvas id="bar-chart"></canvas>
                                @endif

                            </div>
                        </div>

                        <div class="card" style="box-shadow: none !important; background-color:#343a40; border-radius:0px;">
                            
                            <div class="card-body">

                                <h2 class="card-title text-center">
                                    @if($stat_var_year == 0)
                                        --
                                    @elseif($stat_var_year > 0)
                                        <div style="color:green"><i class="far fa-arrow-up"></i> {{round($stat_var_year,2)}}%</div>
                                    @else
                                        <div style="color:red"><i class="far fa-arrow-down"></i> {{round($stat_var_year,2)}}%</div>
                                    @endif
                                </h2>

                                <p class="text-center text-muted">Year Variance</p>

                                @if($stat_detail_values->isEmpty())
                                    <div class="alert alert-info">No data for chart</div>
                                @else
                                    <canvas id="bar-chart-yearly" height="105"></canvas>
                                @endif

                                <div class="row m-3">
                                    <div class="col-6 text-center m-0" style="border-right: 1px solid white;">
                                        <h3>
                                            @if(!is_null($stat_prior_year))
                                                @if($stat_detail->num_format == 'Number')
                                                    {{number_format($stat_prior_year->value)}}
                                                @endif
                                                @if($stat_detail->num_format == 'Currency')
                                                    ${{number_format($stat_prior_year->value,2)}}
                                                @endif
                                                @if($stat_detail->num_format == 'Percent')
                                                    {{ number_format(($stat_prior_year->value * 100), 2) }}%
                                                @endif
                                                @if($stat_detail->num_format == 'Decimal')
                                                    {{number_format($stat_prior_year->value,2)}}
                                                @endif
                                                @if($stat_detail->num_format == 'Decimal4')
                                                    {{number_format($stat_prior_year->value,4)}}
                                                @endif
                                            @endif
                                        </h3>
                                        @if(!is_null($stat_prior_year))
                                            {{$stat_prior_year->start_date->format('M-y')}}
                                        @endif
                                    </div>
                                    <div class="col-6 text-center m-0">
                                        <h3>
                                            @if(!is_null($stat_current))
                                                @if($stat_detail->num_format == 'Number')
                                                    {{number_format($stat_current->value)}}
                                                @endif
                                                @if($stat_detail->num_format == 'Currency')
                                                    ${{number_format($stat_current->value,2)}}
                                                @endif
                                                @if($stat_detail->num_format == 'Percent')
                                                    {{ number_format(($stat_current->value * 100), 2) }}%
                                                @endif
                                                @if($stat_detail->num_format == 'Decimal')
                                                    {{number_format($stat_current->value,2)}}
                                                @endif
                                                @if($stat_detail->num_format == 'Decimal4')
                                                    {{number_format($stat_current->value,4)}}
                                                @endif
                                            @endif
                                        </h3>
                                        @if(!is_null($stat_current))
                                            {{$stat_current->start_date->format('M-y')}}
                                        @endif
                                    </div>
                                </div>

                            </div>
                                
                        </div>

                    </div>

                    <div class="card text-white bg-dark mb-3">
                        <div class="card-body">

                            <canvas id="bar-chart-variance" height="60"></canvas>
                            
                        </div>
                    </div>

                    <div class="card-deck text-white mb-3">
                        <div class="card bg-dark" style="border-color:#39cccc">

                            <div class="card-header" style="background-color:#39cccc">
                                Last 12 Months
                            </div>

                            <div class="card-body">
                                
                                <div class="row text-center py-3" id="border-bottom">
                                    <div class="col-4">
                                        <h2>Min</h2>
                                    </div>
                                    <div class="col-4">
                                        <i id="move-down" class="fad fa-chevron-double-down fa-3x text-warning"></i>
                                    </div>
                                    <div class="col-4">
                                        <h2>
                                            @if(!is_null($last12))
                                                @if($stat_detail->num_format == 'Number')
                                                    {{number_format($last12->min)}}
                                                @endif
                                                @if($stat_detail->num_format == 'Currency')
                                                    ${{number_format($last12->min,2)}}
                                                @endif
                                                @if($stat_detail->num_format == 'Percent')
                                                    {{ number_format(($last12->min * 100), 2) }}%
                                                @endif
                                                @if($stat_detail->num_format == 'Decimal')
                                                    {{number_format($last12->min,2)}}
                                                @endif
                                                @if($stat_detail->num_format == 'Decimal4')
                                                    {{number_format($last12->min,4)}}
                                                @endif
                                            @endif    
                                        </h2>
                                    </div>
                                </div>
                                <div class="row text-center py-3" id="border-bottom">
                                    <div class="col-4">
                                        <h2>Avg</h2>
                                    </div>
                                    <div class="col-4">
                                        <i id="move-left" class="fad fa-chevron-double-left fa-3x text-success"></i>
                                        <i id="move-right" class="fad fa-chevron-double-right fa-3x text-success"></i>
                                    </div>
                                    <div class="col-4">
                                        <h2>
                                            @if(!is_null($last12))
                                                @if($stat_detail->num_format == 'Number')
                                                    {{number_format($last12->avg)}}
                                                @endif
                                                @if($stat_detail->num_format == 'Currency')
                                                    ${{number_format($last12->avg,2)}}
                                                @endif
                                                @if($stat_detail->num_format == 'Percent')
                                                    {{ number_format(($last12->avg * 100), 2) }}%
                                                @endif
                                                @if($stat_detail->num_format == 'Decimal')
                                                    {{number_format($last12->avg,2)}}
                                                @endif
                                                @if($stat_detail->num_format == 'Decimal4')
                                                    {{number_format($last12->avg,4)}}
                                                @endif
                                            @endif  
                                        </h2>
                                    </div>
                                </div>
                                <div class="row text-center py-3">
                                    <div class="col-4">
                                        <h2>Max</h2>
                                    </div>
                                    <div class="col-4">
                                        <i id="move-up" class="fad fa-chevron-double-up fa-3x text-primary"></i>
                                    </div>
                                    <div class="col-4">
                                        <h2>
                                            @if(!is_null($last12))
                                                @if($stat_detail->num_format == 'Number')
                                                    {{number_format($last12->max)}}
                                                @endif
                                                @if($stat_detail->num_format == 'Currency')
                                                    ${{number_format($last12->max,2)}}
                                                @endif
                                                @if($stat_detail->num_format == 'Percent')
                                                    {{ number_format(($last12->max * 100), 2) }}%
                                                @endif
                                                @if($stat_detail->num_format == 'Decimal')
                                                    {{number_format($last12->max,2)}}
                                                @endif
                                                @if($stat_detail->num_format == 'Decimal4')
                                                    {{number_format($last12->max,4)}}
                                                @endif
                                            @endif  
                                        </h2>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <div class="card bg-dark" style="border-color:#39cccc">

                            <div class="card-header" style="background-color:#39cccc">
                                Fiscal Year
                            </div>

                            <div class="card-body">

                                <div class="row text-center py-3" id="border-bottom">
                                    <div class="col-4">
                                        <h2>Min</h2>
                                    </div>
                                    <div class="col-4">
                                        <i id="move-down" class="fad fa-chevron-double-down fa-3x text-warning"></i>
                                    </div>
                                    <div class="col-4">
                                        <h2>
                                            @if(!is_null($lastFY))
                                                @if($stat_detail->num_format == 'Number')
                                                    {{number_format($lastFY->min)}}
                                                @endif
                                                @if($stat_detail->num_format == 'Currency')
                                                    ${{number_format($lastFY->min,2)}}
                                                @endif
                                                @if($stat_detail->num_format == 'Percent')
                                                    {{ number_format(($lastFY->min * 100), 2) }}%
                                                @endif
                                                @if($stat_detail->num_format == 'Decimal')
                                                    {{number_format($lastFY->min,2)}}
                                                @endif
                                                @if($stat_detail->num_format == 'Decimal4')
                                                    {{number_format($lastFY->min,4)}}
                                                @endif
                                            @endif
                                        </h2>
                                    </div>
                                </div>
                                <div class="row text-center py-3" id="border-bottom">
                                    <div class="col-4">
                                        <h2>Avg</h2>
                                    </div>
                                    <div class="col-4">
                                        <i id="move-left" class="fad fa-chevron-double-left fa-3x text-success"></i>
                                        <i id="move-right" class="fad fa-chevron-double-right fa-3x text-success"></i>
                                    </div>
                                    <div class="col-4">
                                        <h2>
                                            @if(!is_null($lastFY))
                                                @if($stat_detail->num_format == 'Number')
                                                    {{number_format($lastFY->avg)}}
                                                @endif
                                                @if($stat_detail->num_format == 'Currency')
                                                    ${{number_format($lastFY->avg,2)}}
                                                @endif
                                                @if($stat_detail->num_format == 'Percent')
                                                    {{ number_format(($lastFY->avg * 100), 2) }}%
                                                @endif
                                                @if($stat_detail->num_format == 'Decimal')
                                                    {{number_format($lastFY->avg,2)}}
                                                @endif
                                                @if($stat_detail->num_format == 'Decimal4')
                                                    {{number_format($lastFY->avg,4)}}
                                                @endif
                                            @endif
                                        </h2>
                                    </div>
                                </div>
                                <div class="row text-center py-3">
                                    <div class="col-4">
                                        <h2>Max</h2>
                                    </div>
                                    <div class="col-4">
                                        <i id="move-up" class="fad fa-chevron-double-up fa-3x text-primary"></i>
                                    </div>
                                    <div class="col-4">
                                        <h2>
                                            @if(!is_null($lastFY))
                                                @if($stat_detail->num_format == 'Number')
                                                    {{number_format($lastFY->max)}}
                                                @endif
                                                @if($stat_detail->num_format == 'Currency')
                                                    ${{number_format($lastFY->max,2)}}
                                                @endif
                                                @if($stat_detail->num_format == 'Percent')
                                                    {{ number_format(($lastFY->max * 100), 2) }}%
                                                @endif
                                                @if($stat_detail->num_format == 'Decimal')
                                                    {{number_format($lastFY->max,2)}}
                                                @endif
                                                @if($stat_detail->num_format == 'Decimal4')
                                                    {{number_format($lastFY->max,4)}}
                                                @endif
                                            @endif
                                        </h2>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div> <!-- Card-Deck -->

                </div> <!-- Home-nav-tab -->

                <div class="tab-pane fade" id="table" role="tabpanel" aria-labelledby="table-tab" onload="myFunction()">

                    <div class="card-body" id="foo" style="width: 100%; overflow-y: auto;">
    
                        @if($stat_detail_values->isEmpty())
                            <div class="alert alert-info">No data</div>
                        @else
                            <div class="table-responsive">
                                <table class="table table-bordered table-dark nowrap" style="margin-bottom:0;">
                                    <thead>
                                        <tr>
                                            @foreach($stat_detail_values as $values)
                                                @if(!is_null($values->start_date))
                                                    <th nowrap><b>{{$values->start_date->format('m - Y')}}</b></th>
                                                @endif
                                            @endforeach
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            @foreach($stat_detail_values as $values)
                                                <td nowrap class="text-center">
    
                                                    @if(!is_null($values->value))
                                                        @if($stat_detail->num_format == 'Number')
                                                            {{number_format($values->value)}} <br><hr style="background-color:#a228ff;">
                                                            
                                                            @if( Auth::user()->hasRole('Stats Admin') == True)
                                                                <a href="#" class="btn btn-block mb-1 btn-warning editValue" value_id="{{$values->id}}" stat_value="{{$values->value}}"><i class="fas fa-edit"></i></a>
                                                                <a href="#" class="btn btn-block btn-sm btn-danger deleteValue" value_id="{{$values->id}}" title="Delete Stat Value"><i class="fas fa-trash-alt"></i></a>
                                                            @endif
    
                                                        @endif
    
                                                        @if($stat_detail->num_format == 'Currency')
                                                            ${{number_format($values->value,2)}} <br><hr style="background-color:#a228ff;">
    
                                                            @if( Auth::user()->hasRole('Stats Admin') == True)
                                                                <a href="#" class="btn btn-block mb-1 btn-warning editValue" value_id="{{$values->id}}" stat_value="{{$values->value}}"><i class="fas fa-edit"></i></a>
                                                                <a href="#" class="btn btn-block btn-sm btn-danger deleteValue" value_id="{{$values->id}}" title="Delete Stat Value"><i class="fas fa-trash-alt"></i></a>
                                                            @endif
    
                                                        @endif
    
                                                        @if($stat_detail->num_format == 'Percent')
                                                            {{ number_format(($values->value * 100), 2) }}%<br><hr style="background-color:#a228ff;">
    
                                                            @if( Auth::user()->hasRole('Stats Admin') == True)
                                                                <a href="#" class="btn btn-block mb-1 btn-warning editValue" value_id="{{$values->id}}" stat_value="{{$values->value}}"><i class="fas fa-edit"></i></a>
                                                                <a href="#" class="btn btn-block btn-sm btn-danger deleteValue" value_id="{{$values->id}}" title="Delete Stat Value"><i class="fas fa-trash-alt"></i></a>
                                                            @endif
    
                                                        @endif
    
                                                        @if($stat_detail->num_format == 'Decimal')
                                                            {{number_format($values->value,2)}} <br><hr style="background-color:#a228ff;">
    
                                                            @if( Auth::user()->hasRole('Stats Admin') == True)
                                                                <a href="#" class="btn btn-block mb-1 btn-warning editValue" value_id="{{$values->id}}" stat_value="{{$values->value}}"><i class="fas fa-edit"></i></a>
                                                                <a href="#" class="btn btn-block btn-sm btn-danger deleteValue" value_id="{{$values->id}}" title="Delete Stat Value"><i class="fas fa-trash-alt"></i></a>
                                                            @endif
    
                                                        @endif
    
                                                        @if($stat_detail->num_format == 'Decimal4')
                                                            {{number_format($values->value,4)}} <br><hr style="background-color:#a228ff;">
                                                            
                                                            @if( Auth::user()->hasRole('Stats Admin') == True)
                                                                <a href="#" class="btn btn-block mb-1 btn-warning editValue" value_id="{{$values->id}}" stat_value="{{$values->value}}"><i class="fas fa-edit"></i></a>
                                                                <a href="#" class="btn btn-block btn-sm btn-danger deleteValue" value_id="{{$values->id}}" title="Delete Stat Value"><i class="fas fa-trash-alt"></i></a>
                                                            @endif    
                                                            
                                                        @endif
    
                                                    @endif
    
                                                </td>
                                            @endforeach
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        @endif
    
                    </div>

                </div>

                <div class="tab-pane fade" id="notes" role="tabpanel" aria-labelledby="notes-tab">

                    <div class="card bg-dark text-white mt-2">
                        <div class="card-body">
                            <div class="card-title">
                                <h5><b>Notes</b></h5>
                            </div>
                            @if(!empty($stat_detail->stat_note))
                                <textarea id="example" name="stat_note">{{$stat_detail->stat_note}}</textarea>
                            @else There are currently no notes available for this stat. @endif
                        </div>
                    </div>

                </div>

                <div class="tab-pane fade" id="clarity" role="tabpanel" aria-labelledby="clarity-tab">

                    <div class="card bg-dark text-white mt-2" >
                        <div class="card-body">
                            <div class="card-title">
                                <h5><b>Clarity SQL Script</b></h5>
                            </div>
                            @if(!empty($stat_detail->stat_script))
                                <textarea id="stat_script" name="stat_script" style="height:300px;width:100%;">{{$stat_detail->stat_script}}</textarea>
                            @else There are currently no script available for this stat. @endif
                        </div>
                    </div>

                </div>

                @if($stat_detail->calc_field == 1)

                    <div class="tab-pane fade" id="calc" role="tabpanel" aria-labelledby="calc-tab">
                        <div class="card bg-dark text-white mt-2">
                            <div class="card-body">
                                <div class="card-title">
                                    <h5><b>Stat Formula</b></h5>
                                </div>
                                @if(!empty($stat_detail->stat_forumula))
                                    <textarea id="example" name="stat_formula" style="height:300px;width:100%;">{{$stat_detail->stat_forumula}}</textarea>
                                @else There is no formula for this script @endif
                            </div>
                        </div>
                    </div>

                @endif

            </div> <!-- tabs -->

        
        </div> <!-- card-body -->
    </div> <!-- card -->

    @include('stats.modals.values')
    @include('stats.modals.edit_values')


@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">

    @include('stats.modals.javascript')


    //Make table view sroll to the far right
    //document.getElementById('foo').scrollLeft = 99999999;
    $("#foo").animate({scrollLeft:500})

    //Change Color
    Chart.defaults.global.defaultFontColor = '#fff';
    // Bar chart
    new Chart(document.getElementById("bar-chart"), {
        type: 'line',
        data: {
            labels: [
                @foreach($stat_detail_values_chart as $values)
                    @if(!is_null($values))
                        '{{$values->start_date->format('m - Y')}}',
                    @else
                        '',
                    @endif
                @endforeach
            ],
            datasets: [
                {
                    label: "{{$stat_detail->stat_type}} - {{$stat_detail->campus}}",
                    backgroundColor: "rgba(29, 149, 247, .6)",
                    pointBackgroundColor: "rgba(29, 149, 247, .9)",
                    borderWidth: 1,
                    data: [
                        @foreach($stat_detail_values_chart as $values)
                            @if($values->num_format == 'Percent')
                                {{ $values->value * 100}},
                            @else
                                {{ $values->value}},
                            @endif
                        @endforeach
                    ],
                    trendlineLinear: {
                        style: "rgba(255 ,255 ,255, 1)",
                        lineStyle: "solid",
                        width: 1
                    },
                    borderColor: "rgba(0, 0, 0, .8)",
                }
            ]
        },
        options: {
            legend: { display: false },
            title: {
                display: true,
                text: '{{$stat_detail->stat_type}} - {{$stat_detail->campus}} / 12-Month'
            },
            scales: {
                yAxes: [{
                    stacked:false,
                ticks: {
                    beginAtZero: false
                    }
                }],
                xAxes: [{
                    stacked:false,
                    display: false,
                    ticks: {
                        beginAtZero: true
                    }
                }]

            }
        }
    });

/******* Monthly Chart **********/

    new Chart(document.getElementById("bar-chart-monthly"), {
        type: 'bar',
        data: {
            labels: [
                @if(!is_null($stat_values_chart_monthly))
                    @foreach($stat_values_chart_monthly as $monthlyValueC)
                        '{{ $monthlyValueC->start_date->format('m - Y') }}',
                    @endforeach
                @else
                    '',
                @endif

            ],
            datasets: [
                {
                    backgroundColor: @if($stat_var > 0) "rgba(72, 232, 163, 0.5)" @else "rgba(184, 31, 39, 0.5)" @endif,
                    data: [
                        @foreach($stat_values_chart_monthly as $monthlyValueC)
                            @if($monthlyValueC->num_format == 'Percent')
                                {{ $monthlyValueC->value * 100}},
                            @else
                                {{ $monthlyValueC->value}},
                            @endif
                        @endforeach
                    ]
                }
            ]
        },
        options: {
            legend: { display: false },
            title: {
                display: true,
                text: '{{$stat_detail->stat_type}} - {{$stat_detail->campus}}'
            },
            scales: {
                yAxes: [{
                    stacked:false,
                ticks: {
                    beginAtZero: true
                    }
                }]
            }
        }
    });


/******* Yearly Chart **********/

    new Chart(document.getElementById("bar-chart-yearly"), {
        type: 'bar',
        data: {
            labels: [
                @if(!is_null($stat_values_chart_yearly) && !is_null($stat_current))
                    '{{ $stat_values_chart_yearly->start_date->format('m - Y') }}',
                    '{{ $stat_current->start_date->format('m - Y') }}'
                @else
                    'N/A'
                @endif
            ],
            datasets: [
                {
                    backgroundColor: @if($stat_var_year > 0) "rgba(72, 232, 163, 0.5)" @else "rgba(184, 31, 39, 0.5)" @endif,
                    data: [
                        @if(!is_null($stat_values_chart_yearly) && !is_null($stat_current))
                            @if($stat_values_chart_yearly->num_format == 'Percent')
                                {{ $stat_values_chart_yearly->value * 100}},
                                {{ $stat_current->value * 100}},
                            @else
                                {{ $stat_values_chart_yearly->value}},
                                {{ $stat_current->value}},
                            @endif
                        @endif
                    ]
                }
            ]
        },
        options: {
            legend: { display: false },
            title: {
                display: true,
                text: '{{$stat_detail->stat_type}} - {{$stat_detail->campus}}'
            },
            scales: {
                yAxes: [{
                    stacked:false,
                ticks: {
                    beginAtZero: true
                    }
                }]
            }
        }
    });

/**************** Variance Chart ****************/

    var pointBackgroundColors = [];
    @if(!is_null($stat_detail->var_max))
        var max = {{ $stat_detail->var_max }};
        var min = {{ $stat_detail->var_min }};
    @else
        var max = 0;
        var min = 0;
    @endif

    var myChart = new Chart(document.getElementById("bar-chart-variance"), {
        type: 'line',
        data: {
            labels: [
                @if(!is_null($stat_detail_values_chart))
                    @foreach($stat_detail_values_chart as $values)
                        '{{ $values->start_date->format('m - Y')}}',
                    @endforeach
                @else
                    ''
                @endif
            ],
            datasets: [
                {
                    label: "{{$stat_detail->stat_type}} - {{$stat_detail->campus}}",
                    // backgroundColor: "rgba(29, 149, 247, .6)",
                    pointBackgroundColor: pointBackgroundColors,
                    borderWidth: 1,
                    fill: false,
                    data: [
                        @foreach($stat_detail_values_chart as $values)
                            {{ $values->value}},
                        @endforeach
                    ],
                    borderColor: "rgba(0, 0, 0, .8)",
                }
            ]
        },
        options: {
            elements: {
                line: {
                    tension: 0
                }
            },
            legend: { display: false },
            title: {
                display: true,
                text: 'Variance Chart with Min / Max Thresholds'
            },
            scales: {
                yAxes: [{
                    stacked:false,
                ticks: {
                    beginAtZero: false
                    }
                }],
                xAxes: [{
                    stacked:false,
                    display: false,
                    ticks: {
                        beginAtZero: true
                    }
                }]

            },
            annotation: {
                annotations: [
                    {
                        type: 'line',
                        mode: 'horizontal',
                        scaleID: 'y-axis-0',
                        value: min,
                        borderColor: 'rgba(255, 196, 0, 0.6)',
                        borderWidth: 0,
                        label: {
                            backgroundColor: 'transparent',
                            enabled: true,
                            yAdjust: 12,
                            content: '(' + @if($stat_detail->num_format == 'Percent') min.toFixed(2) * 100 +'%' @else min.toFixed(0) @endif + ')'
                        }
                    },
                    {
                        type: 'line',
                        mode: 'horizontal',
                        scaleID: 'y-axis-0',
                        value: max,
                        borderColor: 'rgba(255, 196, 0, 0.6)',
                        borderWidth: 0,
                        label: {
                            backgroundColor: 'transparent',
                            enabled: true,
                            yAdjust: -12,
                            content: '(' + @if($stat_detail->num_format == 'Percent') max.toFixed(2) * 100 +'%' @else max.toFixed(0) @endif + ')'
                        }
                    }
                ]
            }
        }
    });

    for (i = 0; i < myChart.data.datasets[0].data.length; i++) {
        if (myChart.data.datasets[0].data[i] > max || myChart.data.datasets[0].data[i] < min) {
            pointBackgroundColors.push("#f50707");
        } else {
            pointBackgroundColors.push("#00f2b6");
        }
    }

    myChart.update();

</script>

<script type="application/javascript">

$(document).on("click", ".deletebtn", function () {
            //alert($(this).attr("data-cost_center"));
            var stat_id = $(this).attr("stat_id");
            DeleteStat(stat_id);
        });

        function DeleteStat(stat_id) {
            swal({
                title: "Deactivate Stat?",
                text: "This can be restored later.",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                confirmButtonText: "Yes, deactivate it!",
                confirmButtonColor: "#ec6c62"
            }, function() {
                $.ajax({
                    url: "/stats/destroy/" + stat_id,
                    type: "GET"
                })
                    .done(function(data) {
                        swal({
                                title: "Deactivated",
                                text: "Stat Inactive",
                                type: "success",
                                timer: 1800,
                                showConfirmButton: false

                            }
                        );
                        setTimeout(function(){window.location.replace('/stats/trashed')},1900);
                    })
                    .error(function(data) {
                        swal("Oops", "We couldn't connect to the server!", "error");
                    });
            });
        }

</script>

<script type="application/javascript">

$(document).on("click", ".deleteValue", function () {
    //alert($(this).attr("data-cost_center"));
    var value_id = $(this).attr("value_id");
    DeleteValue(value_id);
});

$(document).on("click", ".editValue", function () {
    $('.value-id').val($(this).attr("value_id"));
    $('.stat-value').val($(this).attr("stat_value"));
    $('#editValues').modal('show');
});

function DeleteValue(value_id) {
    swal({
        title: "Delete Stat Value?",
        text: "This cannot be undone.",
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "Yes, delete it!",
        confirmButtonColor: "#ec6c62"
    }, function() {
        $.ajax({
            url: "/stats/values/destroy/" + value_id,
            type: "GET"
        })
            .done(function(data) {
                swal({
                        title: "Deleted",
                        text: "Value Deleted",
                        type: "success",
                        timer: 1800,
                        showConfirmButton: false

                    },function() {
                        location.reload();
                    });
            })
            .error(function(data) {
                swal("Oops", "We couldn't connect to the server!", "error");
            });
    });
}


//Place at bottom messes up chart.js

var textarea = document.getElementById('example');
sceditor.create(textarea, {
    format: 'bbcode',
    readOnly: true

});

var textarea = document.getElementById('stat_script');
sceditor.create(textarea, {
    format: 'bbcode',
    readOnly: true

});

var textarea = document.getElementById('stat_formula');
sceditor.create(textarea, {
    format: 'bbcode',
    readOnly: true

});



</script>

@endsection
