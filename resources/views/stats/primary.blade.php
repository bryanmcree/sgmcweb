{{--New file Template--}}

{{--Add Security for this page below--}}
@if( Auth::user()->hasRole('Stats') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')


    <div class="card mb-4 text-white bg-dark" style="border-radius:0px;">
        <div class="card-header">
            {{$primary->count()}} Primary Stats
            <span class="float-right">
                <a href="/stats" class="btn btn-sm btn-primary">Return to Dashboard</a>
            </span>
        </div>
        <div class="card-body">

            <div class="table-responsive">
            <table class="table table-dark table-striped table-hover table-bordered" width="100%" id="autoTable">
                <thead>
                <tr>
                    @if( Auth::user()->hasRole('Stats Admin') == True)
                        <th class="no-sort text-center"><a href="/stats/create"><i class="fas fa-plus-square" title="Add New Stat" style="color:#00a560"></i></a></th>
                    @else
                        <th> </th>
                    @endif
                    <th nowrap><B>Campus</B></th>
                    {{-- <th><B>GL</B></th> --}}
                    <th nowrap><B>Type</B></th>
                    <th nowrap><B>Description</B></th>
                    
                    <?PHP $datacount = -1?>
                    @foreach($stats_report_dates as $report_date)

                        <th nowrap><B>{{$report_date->start_date->format('m - Y')}}</B></th>

                    @endforeach
                </tr>
                </thead>
                <tbody>
                @foreach($primary as $stat)
                    <tr>
                        <td class="text-center"><a href="/stats/detail/{{$stat->id}}" target="_blank" title="Stat Detail"><i class="fas fa-info-circle"></i></a></td>
                        <td>{{$stat->campus}}</td>
                        {{-- <td>{{$stat->concatenate}}</td> --}}
                        <td>{{$stat->stat_type}}</td>
                        <td nowrap>
                            {{$stat->stat_description}}
                            @if($stat->flag_review ==1) <i class="fas fa-flag" style="color:gold;"></i>  @endif
                            @if($stat->calc_field ==1) <i class="fas fa-calculator" style="color:#ff916d;"></i> @endif
                            @if($stat->auto_fill ==1) <i class="fas fa-magic" style="color:#14dae5;"></i> @endif
                        </td>
                        

                        @foreach($stats_report_dates as $report_date_test)
                            <td nowrap>
                                @foreach($stat->statsValues as $value)

                                    @if($report_date_test->start_date == $value->start_date)

                                        @if($stat->num_format == 'Number')
                                            {{number_format($value->value)}}
                                        @endif
                                        @if($stat->num_format == 'Currency')
                                            ${{number_format($value->value)}}
                                        @endif

                                        @if($stat->num_format == 'Percent')
                                            {{number_format($value->value)}}%
                                        @endif

                                        @if($stat->num_format == 'Decimal')
                                            {{number_format($value->value,2)}}
                                        @endif

                                        @if($stat->num_format == 'Decimal4')
                                            {{number_format($value->value,4)}}
                                        @endif

                                    @endif
                                @endforeach

                            </td>

                        @endforeach
                    </tr>
                @endforeach

                </tbody>
            </table>
            </div>


        </div>
    </div>



@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">
    $(document).ready(function() {
        $('#autoTable').DataTable( {
            "pageLength": 50,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>  

 

@endsection

@endif