{{--New file Template--}}

{{--Add Security for this page below--}}

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('css')

    <style>

        .card-deck>.card>.card-body>.card-deck>.card:hover{
            /* background-color: #495057 !important; */
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 8px 20px 0 rgba(255, 255, 255, 0.40); 
            text-decoration: none;
            cursor: pointer;
        }

        .popover{
            background-color:#34eb7d;
            max-width: 50%;
        }

        /* Info box New */
        .info-box{box-shadow:0 0 1px rgba(0,0,0,.125),0 1px 3px rgba(0,0,0,.2);border-radius:.25rem;background:#495057;color:white;display:-ms-flexbox;display:flex;margin-bottom:1rem;min-height:80px;padding:.5rem;position:relative}
        .info-box .progress{background-color:rgba(0,0,0,.125);height:2px;margin:5px 0}
        .info-box .progress .progress-bar{background-color:#fff}
        .info-box .info-box-icon{color:#fff;border-radius:.25rem;-ms-flex-align:center;align-items:center;display:-ms-flexbox;display:flex;font-size:1.875rem;-ms-flex-pack:center;justify-content:center;text-align:center;width:70px}
        .info-box .info-box-icon>img{max-width:100%}
        .info-box .info-box-content{-ms-flex:1;flex:1;padding:5px 10px;white-space:nowrap;overflow:hidden;text-overflow: ellipsis;}
        .info-box .info-box-number{display:block;font-weight:700;white-space:nowrap;overflow:hidden;text-overflow: ellipsis;}
        .info-box .info-box-text,.info-box .progress-description{display:block;overflow:hidden;text-overflow:ellipsis;white-space:nowrap}
        .info-box .info-box-more{display:block}.info-box .progress-description{margin:0}
        @media (min-width:768px){.col-lg-2 .info-box .progress-description,.col-md-2 .info-box .progress-description,.col-xl-2 .info-box .progress-description{display:none}.col-lg-3 .info-box .progress-description,.col-md-3 .info-box .progress-description,.col-xl-3 .info-box .progress-description{display:none}}
        @media (min-width:992px){.col-lg-2 .info-box .progress-description,.col-md-2 .info-box .progress-description,.col-xl-2 .info-box .progress-description{font-size:.75rem;display:block}.col-lg-3 .info-box .progress-description,.col-md-3 .info-box .progress-description,.col-xl-3 .info-box .progress-description{font-size:.75rem;display:block}}
        @media (min-width:1200px){.col-lg-2 .info-box .progress-description,.col-md-2 .info-box .progress-description,.col-xl-2 .info-box .progress-description{font-size:1rem;display:block}.col-lg-3 .info-box .progress-description,.col-md-3 .info-box .progress-description,.col-xl-3 .info-box .progress-description{font-size:1rem;display:block}}

        .bg-aqua{
            background-color:#00c0ef;
        }

        .bg-green{
            background:#00a65a;
        }

        .bg-red{
            background:#dd4b39;
        }

        .bg-yellow{
            background:#f39c12;
        }

        .bg-purple{
            background:#7e3de0;
        }

        .bg-light-orange{
            background:#e0913d;
        }

        #info-auto:hover{
            transition: .3s ease-in-out;
            background-color: #00c0ef;
            text-decoration: none;
            color: white;
        }

        #info-calc:hover{
            transition: .3s ease-in-out;
            background-color: #7e3de0;
            text-decoration: none;
            color: white;
        }

        #info-flex:hover{
            transition: .3s ease-in-out;
            background-color: #e0913d;
            text-decoration: none;
            color: white;
        }

        #info-finance:hover{
            transition: .3s ease-in-out;
            background-color: #00a65a;
            text-decoration: none;
            color: white;
        }

    </style>

@endsection

@section('content')

@include('stats.modals.validated')
@include('stats.modals.invalid')
@include('stats.modals.flagged')
@include('stats.modals.unlinked')
@include('stats.modals.deactivated')
@include('stats.modals.explanation')
@include('stats.modals.recalculate')

    <div class="card mb-3 text-white bg-custom" style="border-radius:0px;">

        <div class="card-body">

            <div class="row">

                <div class="col-lg-4">
                    <form class="form-inline pl-1" action="/stats/search" method="get">
                        <input class="form-control mr-sm-2 custom-select-sm col-lg-8" type="text" name="search" placeholder="Stat Search...">
                        <button class="btn btn-sm bg-dark text-white col-lg-3" type="submit">Search</button>
                    </form>
                </div>

                <div class="col-lg-8 text-right">
                    <a href="/stats/download" class="btn btn-primary btn-sm mr-2">Download Stats</a>
                    @if( Auth::user()->hasRole('Stats Admin') == True)
                        <a href="/stats/download/axiom" class="btn btn-primary btn-sm mr-2">Download Axiom Stats</a>
                        <a href="#recalculateModal" data-toggle="modal" class="btn btn-warning btn-sm mr-2">Recalculate Stats</a>
                        <a href="/stats/manual" class="btn btn-success btn-sm mr-2">Enter Manual Stats</a>
                        <a href="/stats/create" class="btn btn-info btn-sm mr-2">Create New Stat</a>
                        <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Filter Views
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a href="/stats/empty" class="dropdown-item">Missing Value this Month</a>
                            <a href="/stats/flex" class="dropdown-item">Flex Stats</a>
                            <a href="/stats/finance" class="dropdown-item">Finance Stats</a>
                            <a href="/stats/auto" class="dropdown-item">Auto-Fill Stats</a>
                            <a href="/stats/calculated" class="dropdown-item">Calculated Stats</a>
                            <a href="/stats/reports/unlinked_auto" class="dropdown-item">Unlinked Auto Stats</a>
                            <a href="/stats/flagged" class="dropdown-item">Flagged Stats</a>
                            <a href="/stats/trashed" class="dropdown-item">Deactivated Stats</a> 
                        </div>
                    @endif
                </div>

            </div>
            
        </div>
    </div>

@if( Auth::user()->hasRole('Stats Admin') == True)
    <!-- Info boxes -->
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">

            <a class="info-box" id="info-finance" href="/stats">
                <span class="info-box-icon bg-green">
                    <i class="fas fa-analytics"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Total Stats</span>
                    <span class="info-box-number">{{ $numOfStats }}</span>
                </div>
            </a>

        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">

            <a class="info-box" id="info-auto" href="/stats/auto">
                <span class="info-box-icon bg-aqua">
                    <i class="fas fa-bolt"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Auto-Fill Stats</span>
                    <span class="info-box-number">{{ $numOfAuto }}</span>
                </div>
            </a>

        </div>

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>
    
        <div class="col-md-3 col-sm-6 col-xs-12">

            <a class="info-box" id="info-calc" href="/stats/calculated">
                <span class="info-box-icon bg-purple">
                    <i class="fad fa-calculator"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Calculated Stats</span>
                    <span class="info-box-number">{{ $numOfCalc }}</span>
                </div>
            </a>

        </div>
        
        <div class="col-md-3 col-sm-6 col-xs-12">

            <a class="info-box" id="info-flex" href="/stats/primary">
                <span class="info-box-icon bg-light-orange">
                    <i class="fas fa-balance-scale-left"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Primary Stats</span>
                    <span class="info-box-number">{{ $numOfPrimary }}</span>
                </div>
            </a>

        </div>

    </div>
    <!-- /.row -->

    <div class="card-deck mb-3 text-white">

        <div class="card bg-custom col-lg-4 p-0">
            <div class="card-body">

                <canvas id="validatedStats" height="100"></canvas>

                <div class="card-deck mt-3">

                    <a class="card text-white" style="border-radius:0px; background-color:rgba(52, 235, 125, .6)" data-toggle="modal" href="#validatedModal">
                        <div class="card-header">
                            # of validated stats
                        </div>
                        <div class="card-body text-center">
                            <h3>{{ count($validatedStats) }}</h3>
                        </div>
                    </a>
                    <a class="card text-white" style="border-radius:0px; background-color:rgba(221, 75, 57, .6)" data-toggle="modal" href="#invalidatedModal">
                        <div class="card-header">
                            # of non-validated stats
                        </div>
                        <div class="card-body text-center">
                            <h3>{{ count($invalidStats) }}</h3>
                        </div>
                    </a>

                </div>

            </div>
        </div>

        <div class="card bg-custom col-lg-8 p-0">
            <div class="card-body">

                <canvas id="dangerStats" height="50"></canvas>

                <div class="card-deck mt-3">

                    <a class="card text-white" style="border-radius:0px; background-color:rgba(255, 193, 7, .6)" data-toggle="modal" href="#flaggedModal">
                        <div class="card-header">
                            # of flagged stats
                        </div>
                        <div class="card-body text-center">
                            <h3>{{ count($flaggedStats) }}</h3>
                        </div>
                    </a>
                    <a class="card text-white" style="border-radius:0px; background-color:rgba(253, 126, 20, .6)" data-toggle="modal" href="#unlinkedModal">
                        <div class="card-header">
                            # of unlinked stats
                        </div>
                        <div class="card-body text-center">
                            <h3>{{ count($unlinkedStats) }}</h3>
                        </div>
                    </a>
                    <a class="card text-white" style="border-radius:0px; background-color:rgba(220, 53, 70, .6)" data-toggle="modal" href="#deactivatedModal">
                        <div class="card-header">
                            # of deactivated stats
                        </div>
                        <div class="card-body text-center">
                            <h3>{{ count($deactivatedStats) }}</h3>
                        </div>
                    </a>

                </div>

            </div>
        </div>

    </div>

    <div class="card-deck mb-3 text-white">

        <div class="card bg-custom col-lg-9 p-0">
            <div class="card-header">
                Out Of Variance This Month <span class="text-warning"> ({{ count($outOfVar) }}) </span>
            </div>
            <div class="card-body">

                <div class="table-responsive">
                    <table class="nowrap table table-dark table-striped table-hover" style="border-radius:10px;" id="varianceTable">
                        <thead>
                            <tr>
                                <th style="border:none; width:3%;"></th>
                                <th style="border:none;">Campus</th>
                                <th style="border:none;">Type</th>
                                <th style="border:none;">Description</th>
                                <th style="border:none;" class="text-center">Avg</th>
                                <th style="border:none;" class="text-center">Value</th>
                                <th style="border:none;">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $counter = 0 ?> <!-- Assign Stats to BI Team -->
                        @foreach($outOfVar as $variance)
                            <tr>
                                <td class="text-center"><a href="/stats/detail/{{$variance->stat_id}}" title="View Details" target="_blank"><i class="fas fa-info-circle"></i></a></td>
                                <td>{{$variance->campus}}</td>
                                <td>{{$variance->stat_type}}</td>
                                <td>{{$variance->stat_description}}</td>
                                <td class="text-center text-success">{{ number_format($variance->var_avg, 2) }}</td>
                                <td class="text-center text-danger">{{ number_format($variance->value, 2) }}</td>
                                <td class="text-center">
                                    <a href="#" class="btn btn-sm btn-secondary explanation-btn" data-stat-value-id="{{ $variance->value_id }}" data-explanation="{{ $variance->variance_explanation }}">
                                        @if(is_null($variance->variance_explanation) OR empty($variance->variance_explanation))
                                            <i class="fad fa-edit fa-lg text-warning" title="Provide Variance Explanation"></i>
                                        @else
                                            <i class="fad fa-check fa-lg text-success" title="Complete!"></i>
                                        @endif
                                    </a>
                                </td>
                            </tr>
                            @if($counter == 3)
                                <?php $counter = 0 ?>
                            @else
                                <?php $counter++ ?>
                            @endif
                        @endforeach
        
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

        <div class="card bg-custom col-lg-3 p-0">
            <div class="card-header">
                Missing Value this Month <span class="text-warning">({{ count($missingStats) }})</span>
            </div>
            <div class="card-body">

                <div class="table-responsive" style="max-height:600px; overflow-y:auto">
                    <table class="nowrap table table-dark table-striped table-hover table-sm" style="border-radius:10px;">
                        <thead>
                            <tr>
                                <th style="border:none;"></th>
                                <th style="border:none;">Campus</th>
                                <th style="border:none;">Type</th>
                                <th style="border:none;"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($missingStats as $stat)
                                <tr @if($stat->stat_link == '') class="bg-danger" @endif>
                                    <td class="text-center"><a href="/stats/detail/{{$stat->id}}" title="View Details" target="_blank"><i class="fas fa-info-circle"></i></a></td>
                                    <td>{{$stat->campus}}</td>
                                    <td>{{$stat->stat_type}}</td>
                                    <td title="Board Packet">
                                        @if(preg_match( '/.*Board Packet/', $stat->stat_description))
                                            <i class="far fa-file-archive text-success"></i>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

    </div> <!-- Bottom Card Deck -->

@else <!-- NON Admin View -->

    <table class="nowrap table table-dark table-striped table-hover table-bordered" width="100%" id="statSearch">
        <thead>
            <tr>
                <th></th>
                <th><B>Campus</B></th>
                <th><B>Type</B></th>
                <th><B>Description</B></th>
                <?PHP $datacount = -1?>
                @foreach($stats_report_dates as $report_date)
                    <th><B>{{$report_date->start_date->format('m - Y')}}</B></th>
                @endforeach
            </tr>
        </thead>
        <tbody>
            @foreach($stats as $stat)
                <tr>
                    <td class="text-center"><a href="/stats/detail/{{$stat->id}}" title="View Details" target="_blank"><i class="fas fa-info-circle"></i></a></td>
                    <td>{{$stat->campus}}</td>
                    <td>{{$stat->stat_type}}</td>
                    <td>{{$stat->stat_description}}
                        @if($stat->flag_review ==1) <i class="fas fa-flag" style="color:gold;"></i>  @endif
                        @if($stat->calc_field ==1) <i class="fas fa-calculator" style="color:#ff916d;"></i> @endif
                        @if($stat->auto_fill ==1) <i class="fas fa-magic" style="color:#14dae5;"></i> @endif
                    </td>

                    @foreach($stats_report_dates as $report_date) 
                        <td>
                            @foreach($stat->statsValues as $values)
                                @if($report_date->start_date == $values->start_date) {{-- Alakazam --}}

                                    @if($stat->num_format == 'Number')
                                        {{number_format($values->value)}}
                                    @endif
                                    @if($stat->num_format == 'Currency')
                                        ${{number_format($values->value)}}
                                    @endif

                                    @if($stat->num_format == 'Percent')
                                        {{ number_format(($values->value * 100), 2) }}%
                                    @endif

                                    @if($stat->num_format == 'Decimal')
                                        {{number_format($values->value,2)}}
                                    @endif

                                    @if($stat->num_format == 'Decimal4')
                                        {{number_format($values->value,4)}}
                                    @endif

                                @endif
                            @endforeach 
                                
                        </td> 

                    @endforeach 
                </tr>
            @endforeach

        </tbody>
    </table>

    <span class="float-right"> @include('pagination', ['paginator' => $stats]) </span>

@endif

@endsection


{{--END of Content and START of Scripts--}}
@section('scripts')

    <script>

        //Pass to modal
        $(document).on("click", ".explanation-btn", function () {
            $('.value-id').val($(this).attr("data-stat-value-id"));
            $('.explanation').val($(this).attr("data-explanation"));
            $('#explanation-modal').modal('show');
        });

        /******* DataTables *******/

        $(document).ready(function() {
            $('#varianceTable').DataTable( {
                "pageLength": 10,
                "order": [],
                "columnDefs": [ {
                    "targets"  : 'no-sort',
                    "orderable": false,
                }]
            } );
        });

        $(document).ready(function() {
            $('#validTable').DataTable( {
                "pageLength": 15,
                "order": [],
                "columnDefs": [ {
                    "targets"  : 'no-sort',
                    "orderable": false,
                }]
            } );
        });

        $(document).ready(function() {
            $('#invalidTable').DataTable( {
                "pageLength": 15,
                "order": [],
                "columnDefs": [ {
                    "targets"  : 'no-sort',
                    "orderable": false,
                }]
            } );
        });

        $(document).ready(function() {
            $('#flaggedTable').DataTable( {
                "pageLength": 15,
                "order": [],
                "columnDefs": [ {
                    "targets"  : 'no-sort',
                    "orderable": false,
                }]
            } );
        });

        $(document).ready(function() {
            $('#unlinkedTable').DataTable( {
                "pageLength": 15,
                "order": [],
                "columnDefs": [ {
                    "targets"  : 'no-sort',
                    "orderable": false,
                }]
            } );
        });

        $(document).ready(function() {
            $('#deactivatedTable').DataTable( {
                "pageLength": 15,
                "order": [],
                "columnDefs": [ {
                    "targets"  : 'no-sort',
                    "orderable": false,
                }]
            } );
        });

        $(document).ready(function() {
            $('#missingTable').DataTable( {
                "pageLength": 5,
                "order": [],
                "columnDefs": [ {
                    "targets"  : 'no-sort',
                    "orderable": false,
                }]
            } );
        });

    </script>

    <script type="application/javascript">

        Chart.defaults.global.defaultFontColor = '#fff';
        
        new Chart(document.getElementById('validatedStats'), {
            type: 'doughnut',

            data: {
                labels: [
                    'Validated',
                    'Not Validated',
                ],
                datasets: [{
                    label: "",
                    backgroundColor: [
                        'rgba(52, 235, 125, .6)',
                        'rgba(221, 75, 57, .6)',
                    ],
                    data: [
                        {{ count($validatedStats) }},
                        {{ count($invalidStats) }},
                    ]
                }]
            },
            options: {
                rotation: 1 * Math.PI,
                circumference: 1 * Math.PI,
                responsive: true,
                legend: { display: true },
                title: {
                    display: false,
                    text: 'Predicted world population (millions) in 2050'
                },
                
            }
        });

    /************************************************/

        new Chart(document.getElementById('dangerStats'), {
            type: 'bar',

            data: {
                labels: [
                    'Flagged',
                    'Unlinked',
                    'Deactivated'
                ],
                datasets: [{
                    label: "",
                    backgroundColor: [
                        'rgba(255, 193, 7, .6)',
                        'rgba(253, 126, 20, .6)',
                        'rgba(220, 53, 70, .6)',
                    ],
                    data: [
                        {{ count($flaggedStats) }},
                        {{ count($unlinkedStats) }},
                        {{ count($deactivatedStats) }},
                    ]
                }]
            },
            options: {
                responsive: true,
                legend: { display: false },
                title: {
                    display: false,
                    text: 'Predicted world population (millions) in 2050'
                },
                
            }
        });

    

    </script>

@endsection
