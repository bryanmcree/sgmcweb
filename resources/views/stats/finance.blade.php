{{--New file Template--}}

{{--Add Security for this page below--}}
@if( Auth::user()->hasRole('Stats') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')


    <div class="card mb-4 text-white bg-dark" style="border-radius:0px;">
        <div class="card-header">
            {{$finance->count()}} Finance Stats
            <span class="float-right">
                <a href="/stats" class="btn btn-sm btn-primary">Return to Dashboard</a>
            </span>
        </div>
        <div class="card-body">

            <div class="table-responsive mb-3">
                <table class="nowrap table table-dark table-striped table-hover" style="border-radius:10px;" id="varianceTable">
                    <thead>
                        <tr>
                            <th style="border:none; width:3%;"></th>
                            <th style="border:none;">Campus</th>
                            <th style="border:none;">Type</th>
                            <th style="border:none;">Description</th>
                            <th style="border:none;" class="text-center">Avg</th>
                            <th style="border:none;" class="text-center">Value</th>
                            <th style="border:none;">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $counter = 0 ?> <!-- Assign Stats to BI Team -->
                    @foreach($outOfVar as $variance)
                        <tr>
                            <td class="text-center"><a href="/stats/detail/{{$variance->stat_id}}" title="View Details" target="_blank"><i class="fas fa-info-circle" @if($counter == 0) style="color: rgba(57, 200, 219);" @elseif($counter == 1) style="color: rgba(140, 36, 28);" @elseif($counter == 2) style="color: rgba(227, 230, 50);" @elseif($counter == 3) style="color: rgba(187, 30, 214);" @endif></i></a></td>
                            <td>{{$variance->campus}}</td>
                            <td>{{$variance->stat_type}}</td>
                            <td>{{$variance->stat_description}}</td>
                            <td class="text-center text-success">{{ number_format($variance->var_avg, 2) }}</td>
                            <td class="text-center text-danger">{{ number_format($variance->value, 2) }}</td>
                            <td class="text-center">
                                <a href="#" class="btn btn-sm btn-secondary explanation-btn" data-stat-value-id="{{ $variance->value_id }}" data-explanation="{{ $variance->variance_explanation }}">
                                    @if(is_null($variance->variance_explanation) OR empty($variance->variance_explanation))
                                        <i class="fad fa-edit fa-lg text-warning" title="Provide Variance Explanation"></i>
                                    @else
                                        <i class="fad fa-check fa-lg text-success" title="Complete!"></i>
                                    @endif
                                </a>
                            </td>
                        </tr>
                        @if($counter == 3)
                            <?php $counter = 0 ?>
                        @else
                            <?php $counter++ ?>
                        @endif
                    @endforeach
    
                    </tbody>
                </table>
            </div>

            <div class="table-responsive">
                <table class="table table-dark table-striped table-hover table-bordered" width="100%" id="autoTable">
                    <thead>
                    <tr>
                        @if( Auth::user()->hasRole('Stats Admin') == True)
                            <th class="no-sort text-center"><a href="/stats/create"><i class="fas fa-plus-square" title="Add New Stat" style="color:#00a560"></i></a></th>
                        @else
                            <th> </th>
                        @endif
                        <th nowrap><B>Campus</B></th>
                        <th><B>GL</B></th>
                        <th nowrap><B>Type</B></th>
                        <th nowrap><B>Description</B></th>
                        
                        <?PHP $datacount = -1?>
                        @foreach($stats_report_dates as $report_date)

                            <th nowrap><B>{{$report_date->start_date->format('m - Y')}}</B></th>

                        @endforeach
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($finance as $stat)
                        <tr>
                            <td class="text-center"><a href="/stats/detail/{{$stat->id}}" target="_blank" title="Stat Detail"><i class="fas fa-info-circle"></i></a></td>
                            <td>{{$stat->campus}}</td>
                            <td>{{$stat->concatenate}}</td>
                            <td>{{$stat->stat_type}}</td>
                            <td nowrap>
                                {{$stat->stat_description}}
                                @if($stat->flag_review ==1) <i class="fas fa-flag" style="color:gold;"></i>  @endif
                                @if($stat->calc_field ==1) <i class="fas fa-calculator" style="color:#ff916d;"></i> @endif
                                @if($stat->auto_fill ==1) <i class="fas fa-magic" style="color:#14dae5;"></i> @endif
                            </td>
                            

                            @foreach($stats_report_dates as $report_date_test)
                                <td nowrap>
                                    @foreach($stat->statsValues as $value)

                                        @if($report_date_test->start_date == $value->start_date)

                                            @if($stat->num_format == 'Number')
                                                {{number_format($value->value)}}
                                            @endif
                                            @if($stat->num_format == 'Currency')
                                                ${{number_format($value->value)}}
                                            @endif

                                            @if($stat->num_format == 'Percent')
                                                {{number_format($value->value)}}%
                                            @endif

                                            @if($stat->num_format == 'Decimal')
                                                {{number_format($value->value,2)}}
                                            @endif

                                            @if($stat->num_format == 'Decimal4')
                                                {{number_format($value->value,4)}}
                                            @endif

                                        @endif

                                    @endforeach

                                </td>

                            @endforeach
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>


        </div>
    </div>

    @include('stats.modals.explanation')

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">

    //Pass to modal
    $(document).on("click", ".explanation-btn", function () {
        $('.value-id').val($(this).attr("data-stat-value-id"));
        $('.explanation').val($(this).attr("data-explanation"));
        $('#explanation-modal').modal('show');
    });

    $(document).ready(function() {
        $('#varianceTable').DataTable( {
            "pageLength": 10,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    });

    $(document).ready(function() {
        $('#autoTable').DataTable( {
            "pageLength": 50,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    });

</script>  

    {{-- <script type="application/javascript">
        oTable = $('#stats').DataTable(

            {
                dom: 't',         // This shows just the table
                scrollY:        "700px",
                scrollX:        true,
                scrollCollapse: true,
                paging:         false,
                fixedColumns:   {
                    leftColumns: 4,
                    //rightColumns: 1
                },

                //"pageLength": 20,
                "order": [],
                "columnDefs": [
                    {
                        "targets": [ 2 ],
                        "visible": false,
                        "searchable": true
                    },{
                        "targets"  : 'no-sort',
                        "orderable": false,
                    }]

            }

        );   //pay attention to capital D, which is mandatory to retrieve "api" datatables' object, as @Lionel said
        $('#myInputTextField').keyup(function(){
            oTable.search($(this).val()).draw() ;
        })
    </script> --}}

@endsection

@endif