<!-- Modal -->
<div class="modal fade" id="deactivatedModal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content text-white bg-dark">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Deactivated Stats</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">

                <table class="table table-dark table-hover table-striped table-bordered table-sm" id="deactivatedTable">
                    <thead>
                        <th></th>
                        <th>Campus</th>
                        <th>Stat Type</th>
                        <th>Stat Description</th>
                    </thead>
                    <tbody>
                        @foreach($deactivatedStats as $dStat)
                            <tr>
                                <td class="text-center"><a href="/stats/detail/{{$dStat->id}}" title="View Details" target="_blank"><i class="fas fa-info-circle"></i></a></td>
                                <td>{{ $dStat->campus }}</td>
                                <td>{{ $dStat->stat_type }}</td>
                                <td>{{ $dStat->stat_description }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>