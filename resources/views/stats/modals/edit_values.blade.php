<!-- Modal -->
<div class="modal fade" id="editValues" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content text-white bg-dark">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Stat Value</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form method="POST" action="/stats/values/edit">
                {{ csrf_field() }}
                <input type="hidden" class="value-id" name="value_id">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="stat_value">Stat Value</label>
                        <input type="decimal" name="stat_value" class="form-control stat-value" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Update Stat Value">
                </div>
            </form>

        </div>
    </div>
</div>