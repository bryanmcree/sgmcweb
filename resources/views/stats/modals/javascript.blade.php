//Pass values to modal
$(document).on("click", ".statValues", function () {
$('#statValues').modal('show');
});


//Pass values to modal for editing
$(document).on("click", ".editValues", function () {
$('.value-id').val($(this).attr("data-value-id"));
$('.stat-id').val($(this).attr("data-stat-id"));
$('.stat-value').val($(this).attr("data-stat-value"));
$('.report-date').val($(this).attr("data-report-date"));
$('#editValues').modal('show');
});

