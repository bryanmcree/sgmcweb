<!-- Modal -->
<div class="modal fade" id="explanation-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content text-white bg-dark">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Variance Explanation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form method="POST" action="/stats/variance-explanation">
                {{ csrf_field() }}
                <input type="hidden" name="value_id" class="value-id">

                <div class="modal-body">
                    <div class="form-group">
                        <label>Explanation</label>
                        <textarea name="variance_explanation" rows="5" class="form-control explanation"></textarea>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Record Variance Explanation">
                </div>
            </form>

        </div>
    </div>
</div>