<!-- Modal -->
<div class="modal fade" id="statValues" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content text-white bg-dark">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Add Stat Values</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form method="POST" action="/stats/values">
                {{ csrf_field() }}
                <input type="hidden" name="stat_name" value="{{ $stat_detail->campus }}_{{ $stat_detail->stat_description }}">
                <input type="hidden" name="location_code" value="0">
                <input type="hidden" name="stat_category" value="{{ $stat_detail->stat_type }}">
                <input type="hidden" name="stat_link" value="{{ $stat_detail->stat_link }}">

                <div class="modal-body">
                    <div class="form-group">
                        <label for="start_date">Start Date</label>
                        <input type="date" name="start_date" value="{{\Carbon\Carbon::now()->subMonth(1)->firstOfMonth()->format('Y-m-d')}}" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="end_date">End Date</label>
                        <input type="date" name="end_date" value="{{\Carbon\Carbon::now()->firstOfMonth()->format('Y-m-d')}}" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="value">Stat Value</label>
                        <input type="decimal" name="value" class="form-control" required>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Add Stat Value">
                </div>
            </form>

        </div>
    </div>
</div>