{{--New file Template--}}

{{--Add Security for this page below--}}
@if( Auth::user()->hasRole('Stats Admin') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    <div class="card bg-dark text-white mb-3">
        <div class="card-header">
            All Flagged Stats
            <span class="float-right"><a href="/stats" class="btn btn-secondary btn-sm">Return To Dashboard</a></span>
        </div>
        <div class="card-body">

            <table class="table table-dark table-bordered table-hover" id="flagSearch">
                <thead>
                    <th>Axiom Acct</th>
                    <th>Campus</th>
                    <th>Stat Type</th>
                    <th>Stat Description</th>
                    <th>Details</th>
                </thead>
                <tbody>
                    @foreach($flagged as $flag)
                        <tr>
                            <td> {{ $flag->axiom_act }} </td>
                            <td> {{ $flag->campus }} </td>
                            <td> {{ $flag->stat_type }} </td>
                            <td> {{ $flag->stat_description }} </td>
                            <td> <a href="/stats/detail/{{$flag->id}}" class="btn btn-primary btn-sm btn-block">View Details</a> </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">
    $(document).ready(function() {
        $('#flagSearch').DataTable( {
            "pageLength": 25,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>    

@endsection

@endif