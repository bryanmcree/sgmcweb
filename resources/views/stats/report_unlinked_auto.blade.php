{{--New file Template--}}

{{--Add Security for this page below--}}
@if( Auth::user()->hasRole('Stats') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')


    <div class="card mb-4 text-white bg-dark" style="border-radius:0px;">
        <div class="card-header">
            {{$stats->count()}} Unlinked Auto Fill Stats
            <span class="float-right">
                <a href="/stats" class="btn btn-sm btn-primary">Return to Dashboard</a>
            </span>
        </div>
        <div class="card-body">
            {{-- <input type="text" id="myInputTextField" placeholder="Search...">  --}}
            {{-- <a href="/stats/trashed" class="btn btn-warning">View Inactive Stats</a> <!-- Style me --> --}}

            <table class="nowrap table table-dark table-striped table-hover table-bordered" width="100%" id="unlinkedTable">
                <thead>
                <tr>
                    @if( Auth::user()->hasRole('Stats Admin') == True)
                        <th class="no-sort text-center"><a href="/stats/create"><i class="fas fa-plus-square" title="Add New Stat" style="color:#00a560"></i></a></th>
                    @else
                        <th> </th>
                    @endif
                    <th><B>Campus</B></th>
                    <th><B>GL</B></th>
                    <th><B>Type</B></th>
                    <th><B>Description</B></th>
                    <?PHP $datacount = -1?>
                    @foreach($stats_report_dates as $report_date)

                        <th><B>{{$report_date->start_date->format('m - Y')}}</B></th>

                    @endforeach
                </tr>
                </thead>
                <tbody>
                @foreach($stats as $stat)
                    <tr>
                        <td class="text-center"><a href="/stats/edit/{{$stat->id}}" target="_blank" title="Link Stat"><i class="fas fa-info-circle"></i></a></td>
                        <td>{{$stat->campus}}</td>
                        <td>{{$stat->concatenate}}</td>
                        <td>{{$stat->stat_type}}</td>
                        <td>{{$stat->stat_description}} &nbsp;
                            @if($stat->flag_review ==1) <i class="fas fa-flag" style="color:gold;"></i>  @endif
                            @if($stat->calc_field ==1) <i class="fas fa-calculator" style="color:#ff916d;"></i> @endif
                            @if($stat->auto_fill ==1) <i class="fas fa-magic" style="color:#14dae5;"></i> @endif
                        </td>

                        @foreach($stats_report_dates as $report_date_test)
                            <td>
                                @foreach($stat->statsValues as $value)

                                    @if($report_date_test->start_date == $value->start_date)

                                        @if($stat->num_format == 'Number')
                                            {{number_format($value->value)}}
                                        @endif
                                        @if($stat->num_format == 'Currency')
                                            ${{number_format($value->value)}}
                                        @endif

                                        @if($stat->num_format == 'Percent')
                                            {{number_format($value->value)}}%
                                        @endif

                                        @if($stat->num_format == 'Decimal')
                                            {{number_format($value->value,2)}}
                                        @endif

                                        @if($stat->num_format == 'Decimal4')
                                            {{number_format($value->value,4)}}
                                        @endif

                                    @endif
                                @endforeach

                            </td>

                        @endforeach
                    </tr>
                @endforeach

                </tbody>
            </table>


        </div>
    </div>



@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">
    $(document).ready(function() {
        $('#unlinkedTable').DataTable( {
            "pageLength": 50,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>  

    {{-- <script type="application/javascript">
        oTable = $('#stats').DataTable(

            {
                dom: 't',         // This shows just the table
                scrollY:        "700px",
                scrollX:        true,
                scrollCollapse: true,
                paging:         false,
                fixedColumns:   {
                    leftColumns: 4,
                    //rightColumns: 1
                },

                //"pageLength": 20,
                "order": [],
                "columnDefs": [
                    {
                        "targets": [ 2 ],
                        "visible": false,
                        "searchable": true
                    },{
                        "targets"  : 'no-sort',
                        "orderable": false,
                    }]

            }

        );   //pay attention to capital D, which is mandatory to retrieve "api" datatables' object, as @Lionel said
        $('#myInputTextField').keyup(function(){
            oTable.search($(this).val()).draw() ;
        })
    </script> --}}

@endsection

@endif