{{--New file Template--}}

{{--Add Security for this page below--}}
@if( Auth::user()->hasRole('Stats') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')


    <div class="card mb-4 text-white bg-dark" style="border-radius:0px;">
        <div class="card-header">
           {{$manuals->count()}} Total Stats
           <span class="float-right">
               <a href="/stats" class="btn btn-sm btn-primary">Return to Dashboard</a>
           </span>
        </div>
        <div class="card-body">

            <table class="nowrap table table-dark table-striped table-hover table-bordered" width="100%" id="manualTable">
                <thead>
                    <tr>
                        @if( Auth::user()->hasRole('Stats Admin') == True)
                            <th class="no-sort text-center"><a href="/stats/create"><i class="fas fa-plus-square" title="Add New Stat" style="color:#00a560"></i></a></th>
                        @else
                            <th> </th>
                        @endif
                        <th><B>Campus</B></th>
                        <th><B>GL</B></th>
                        <th><B>Type</B></th>
                        <th><B>Description</B></th>
                        <th>{{ Carbon::now()->subMonth()->format('m-Y') }}</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($manuals as $stat)
                    <tr @if($stat->stat_link == '') class="bg-danger" @endif>
                        <td class="text-center"><a href="/stats/detail/{{$stat->id}}" title="View Details"><i class="fas fa-info-circle"></i></a></td>
                        <td>{{$stat->campus}}</td>
                        <td>{{$stat->concatenate}}</td>
                        <td>{{$stat->stat_type}}</td>
                        <td>{{$stat->stat_description}}</td>
                        <td>
                            @if($stat->stat_link == '')
                                STAT DOES NOT HAVE A STAT_LINK
                            @else
                                <form action="/stats/manual/update" method="POST">
                                    <input type="hidden" name="start_date" value="{{ Carbon::now()->startOfMonth()->subMonth()->toDateString() }}">
                                    <input type="hidden" name="end_date" value="{{ Carbon::now()->startOfMonth()->toDateString() }}">
                                    <input type="hidden" name="stat_name" value="{{ $stat->campus }}_{{ $stat->stat_description }}">
                                    <input type="hidden" name="location_code" value="0">
                                    <input type="hidden" name="stat_category" value="{{ $stat->stat_type }}">
                                    <input type="hidden" name="stat_link" value="{{ $stat->stat_link }}">
                                    {{ csrf_field() }}

                                    <input type="number" class="form-control bg-secondary text-white" step="0.0001" onfocusout="this.form.submit()" name="value" 
                                        @foreach($stat->statsValues as $value)
                                            @if(Carbon::parse($value->start_date)->format('m-Y') == Carbon::now()->subMonth()->format('m-Y')) 
                                                value="{{ $value->value }}"                   
                                            @endif
                                        @endforeach>
                                </form>
                            @endif
                        </td>
                        
                    </tr>
                @endforeach

                </tbody>
            </table>

        </div>
    </div>



@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">
    $(document).ready(function() {
        $('#manualTable').DataTable( {
            "pageLength": 25,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>  

@endsection

@endif