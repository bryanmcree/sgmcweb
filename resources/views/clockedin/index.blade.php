@extends('layouts.app')

@section('content')
    <div class="col-md-2">
    <div class="panel panel-success">
        <div class="panel-heading">Clocked In</div>
        <div class="panel-body">
            {!! Form::open(array('url' => '/clockedin', 'method' => 'GET', 'files' => true,'class' => 'form_control')) !!}
            <div class="form-group">
                {!! Form::label('unit', 'Unit:') !!}
                {!!  Form::select('unit', $OrgUnit, null, ['class' => 'form-control']) !!}
            </div>
            <input type="submit" name="submit_data" class="btn btn-success btn-block" value="Get Report">
            {!! Form::close() !!}
            <br>
            Report will display employees who are clocked in for the above unit.  This report does not display employees who do not clock in.
        </div>
    </div>
    </div>

    <div class="col-md-5">
        <div class="panel panel-success">
            <div class="panel-heading"><b>Results</b></div>
            <div class="panel-body">
                @if ($prod == 'nothing')
                    <div class="alert alert-info" role="alert">No data has been selected.</div>
                @else
                    <table class="table table-condensed table-hover table-responsive table-bordered table-striped" id="table">
                        <thead>
                        <tr class="success">
                            <td colspan="9"><b>Unit Code -></b> {{$unit}}<br>  <b># Employees Clocked In-></b> {{$prod->count()}} </td>
                        </tr>
                        </thead>
                        <thead>
                        <tr class="success">
                            <td><b>Name</b></td>
                            <td class="hidden-xs"><b>Employee #</b></td>
                            <td class="hidden-xs hidden-sm hidden-md"><b>Title</b></td>
                            <td class="hidden-xs hidden-sm hidden-md"><b>Cost Center</b></td>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($prod as $prods)

                            <tr>
                                <td>{{$prods->LastName}}, {{$prods->FirstName}}</td>
                                <td class="hidden-xs hidden-sm hidden-md">{{$prods->Code}}</td>
                                <td class="hidden-sm hidden-md hidden-xs">{{$prods->Expr2}}</td>
                                <td class="hidden-xs hidden-sm">{{$prods->Description}}</td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
@endsection