<div class="modal fade AddNewSurvey" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><b>Add New Survey</b></h4>
            </div>
            <form method="post" action="/scorecard/dashboard/addcomment">
                {{ csrf_field() }}
                <input type="hidden" name="entered_by" value="{{ Auth::user()->name }}">
                <div class="modal-body bg-default">
                        <div class="form-group">
                            <label for="survey_name_id">Title of new Survey:</label>
                            <input type="text" name="survey_name" id="survey_name_id" class="form-control">
                        </div>
                </div>
            <div class="modal-footer bg-default">
                <div class="RightLeft">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" Value="Add New Survey" class="btn btn-sgmc">
                </div>
            </div>
        </form>
        </div>
    </div>
</div>