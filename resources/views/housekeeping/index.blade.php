{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Housekeeping') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <p><a href="#" data-target=".AddNewSurvey" data-toggle="modal" class="btn btn-sm btn-primary">Create New Survey</a> </p>
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <b>Select Survey</b>
            </div>
            <div class="panel-body">

                TEST
            </div>
        </div>
    </div>


@include("housekeeping.modals.add_new_survey")

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
@endif