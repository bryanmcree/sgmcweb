{{--New file Template--}}

{{--Add Security for this page below--}}

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')


@if( Auth::user()->hasRole('Policies Admin') == TRUE)

    <div class="card mb-4 text-white bg-dark"  style="display:flex;">
        <div class="card-header">
            Emergency Management Forms
            <span class="float-right">
                <a href="/policies/create" class="btn btn-primary btn-sm">Upload New</a>
            </span>
        </div>
        <div class="card-body">

            <div class="table-responsive">
                <table class="table table-dark table-bordered" id="admins">
                    <thead>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Function</th>
                        <th>Download</th>
                        <th>Edit</th>
                        <th class="text-center">Active</th>
                    </thead>
                    <tbody>
                        @forelse($all as $policy)
                            <tr>
                                <td> {{ $policy->title }} <div style="display:none;"> {{$policy->keywords}} {{ $policy->policy_num }} </div> </td>
                                <td> {{ $policy->description }} </td>
                                <td> {{ $policy->responsibility }} </td>
                                <td> <a href="/policies/download/{{$policy->id}}" class="btn btn-info btn-sm btn-block">Download</a> </td>
                                <td> <a href="/policies/edit/{{$policy->id}}" class="btn btn-warning btn-sm btn-block">Edit</a> </td>
                                @if($policy->active == 'True')
                                <td class="text-center">
                                    <form action="/policies/active/{{$policy->id}}" method="POST">
                                        {{ csrf_field() }}
                                        <input type="checkbox" name="active" class="mt-1" style="width: 30px; height: 30px;" onChange="this.form.submit()" @if ($policy->active == 'True') checked @endif>
                                    </form>
                                </td>
                                @else
                                <td class="text-center bg-danger">
                                    <form action="/policies/active/{{$policy->id}}" method="POST">
                                        {{ csrf_field() }}
                                        <input type="checkbox" name="active" class="mt-1" style="width: 30px; height: 30px;" onChange="this.form.submit()" @if ($policy->active == 'True') checked @endif>
                                    </form>
                                </td>
                                @endif
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4" class="text-center">No Data Available</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>

            
            <a href="/policies/create" class="btn btn-primary">Upload New</a>
            
        </div>
    </div>

    @else

    <div class="card mb-4 text-white bg-dark"  style="display:flex;">
        <div class="card-header">
            Policies and Procedures
        </div>
        <div class="card-body">

            <div class="table-responsive">
                <table class="table table-dark table-bordered" id="users">
                    <thead>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Download</th>
                    </thead>
                    <tbody>
                        @forelse($policies as $policy)
                            <tr>
                                <td> {{ $policy->file_name }}  <div style="display:none;"> {{$policy->keywords}} {{ $policy->policy_num }} </div> </td>
                                <td> {{ $policy->description }} </td>
                                <td> <a href="/policies/download/{{$policy->id}}" class="btn btn-info btn-block">Download</a> </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3" class="text-center">No Data Available</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            
        </div>
    </div>

@endif

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

 <script type="application/javascript">

    $(document).ready(function() {
        $('#admins').DataTable( {
            "pageLength": 20,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );

    $(document).ready(function() {
        $('#users').DataTable( {
            "pageLength": 15,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );

</script>

@endsection
