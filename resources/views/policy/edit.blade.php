{{--New file Template--}}

{{--Add Security for this page below--}}
@if( Auth::user()->hasRole('Policies Admin') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')


    <div class="card mb-4 text-white bg-dark col-lg-8 m-auto">
        <div class="card-header">
            New Policy or Procedure
            <span class="float-right"><a href="/policies" class="btn btn-primary btn-sm">Return to Policies</a></span>
        </div>
        <div class="card-body">
            
            <form action="/policies/update/{{$policy->id}}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="form-row">
                    <div class="form-group col-lg-6">
                        <label for="">Title</label>
                        <input type="text" name="title" class="form-control" value="{{$policy->title}}" required>
                    </div>
                    <div class="form-group col-lg-6">
                        <label for="">Policy #</label>
                        <input type="text" name="policy_num" class="form-control" value="{{$policy->policy_num}}">
                    </div>
                </div>

                <h6>Facility</h6>
                <div class="form-check form-check-inline ml-5 mb-2">
                    <input class="form-check-input" type="checkbox" name="facilities[]" value="SGMC" @foreach($chosen as $f) @if($f->facility == 'SGMC') checked @endif  @endforeach>
                    <label class="form-check-label mr-3">SGMC</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" name="facilities[]" value="SGMC Berrien Campus" @foreach($chosen as $f) @if($f->facility == 'SGMC Berrien Campus') checked @endif  @endforeach>
                    <label class="form-check-label mr-3">SGMC Berrien Campus</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" name="facilities[]" value="SGMC Lanier Campus" @foreach($chosen as $f) @if($f->facility == 'SGMC Lanier Campus') checked @endif  @endforeach>
                    <label class="form-check-label mr-3">SGMC Lanier Campus</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" name="facilities[]" value="SGMC Lakeland Villa" @foreach($chosen as $f) @if($f->facility == 'SGMC Lakeland Villa') checked @endif  @endforeach>
                    <label class="form-check-label mr-3">SGMC Lakeland Villa</label>
                </div>

                <div class="form-group">
                    <label for="">Function</label>
                    <input type="text" name="responsibility" class="form-control" value="{{ $policy->responsibility }}">
                </div>

                <div class="form-group">
                    <label for="">Department</label>
                    <select name="department" class="form-control">
                        <option value="{{ $policy->department }}" selected>{{ $policy->department }}</option>
                        @foreach($departments as $department)
                            <option value="{{ $department->unit_code_description }}">{{ $department->unit_code_description }}</option>
                        @endforeach
                    </select>
                </div>

                {{-- <div class="form-row">
                    <div class="form-group col-lg-6">
                        <label for="">Last Review/Revision Date</label>
                        <input type="date" name="last_rev_date" class="form-control" value="{{ Carbon::parse($policy->last_rev_date)->format('Y-m-d') }}" required>
                    </div>
                    <div class="form-group col-lg-6">
                        <label for="">Approval Date</label>
                        <input type="date" name="approval_date" class="form-control" value="{{ Carbon::parse($policy->approval_date)->format('Y-m-d') }}" required>
                    </div>
                </div> --}}

                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea name="description" rows="3" class="form-control"> {{$policy->description}}</textarea>
                </div>
                <div class="form-group">
                    <label for="keywords">Key Words (Enter key words that can be used to find this policy in a search. Separate each word with a comma. Ex: Health, Finance, Budget)</label>
                    <input type="text" name="keywords" class="form-control" value="{{$policy->keywords}}">
                </div>
                <div class="form-group">
                    <label for="file">Upload File <span class="text-warning"> (Leave blank to keep current file - {{$policy->file_name}}) </span> </label>
                    <input type="file" name="file_name" class="form-control" value="{{$policy->file_name}}">
                </div>

                <hr>
                <input type="submit" class="btn btn-primary" value="Submit">

            </form>

        </div>
    </div>


@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection

@endif