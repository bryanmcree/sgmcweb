{{--New file Template--}}

{{--Add Security for this page below--}}
@if( Auth::user()->hasRole('Policies Admin') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.new_nonav')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

    <style>
        
        body{
            background-image: url('/img/policy_2.jpg');
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-position: center;
        }

    </style>

@section('content')


    <div class="col-lg-6 m-auto">
        <div class="card mb-4 text-white bg-dark">
            <div class="card-header">
                New Policy or Procedure
                <span class="float-right"><a href="/policies" class="btn btn-secondary btn-sm">Return to Policies</a></span>
            </div>

            <form action="/policies/store" method="POST" enctype="multipart/form-data" class="mb-0">
                {{ csrf_field() }}
                
                <div class="card-body pb-2">
                
                    <div class="form-row">
                        <div class="form-group col-lg-6">
                            <label for="">Title</label>
                            <input type="text" name="title" class="form-control" required>
                        </div>
                        <div class="form-group col-lg-6">
                            <label for="">Policy #</label>
                            <input type="text" name="policy_num" class="form-control">
                        </div>
                    </div>

                    <h6>Facility</h6>
                    <div class="form-check form-check-inline ml-5 mb-2">
                        <input class="form-check-input" type="checkbox" name="facilities[]" value="SGMC">
                        <label class="form-check-label mr-3">SGMC</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="facilities[]" value="SGMC Berrien Campus">
                        <label class="form-check-label mr-3">SGMC Berrien Campus</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="facilities[]" value="SGMC Lanier Campus">
                        <label class="form-check-label mr-3">SGMC Lanier Campus</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="facilities[]" value="SGMC Lakeland Villa">
                        <label class="form-check-label mr-3">SGMC Lakeland Villa</label>
                    </div>

                    <div class="form-group">
                        <label for="">Function</label>
                        <input type="text" name="responsibility" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="">Department</label>
                        <select name="department" class="form-control">
                            <option value="">[Choose Department]</option>
                            @foreach($departments as $department)
                                <option value="{{ $department->unit_code_description }}">{{ $department->unit_code_description }}</option>
                            @endforeach
                        </select>
                    </div>

                    {{-- <div class="form-row">
                        <div class="form-group col-lg-6">
                            <label for="">Last Review/Revision Date</label>
                            <input type="date" name="last_rev_date" class="form-control" required>
                        </div>
                        <div class="form-group col-lg-6">
                            <label for="">Approval Date</label>
                            <input type="date" name="approval_date" class="form-control" required>
                        </div>
                    </div> --}}

                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea name="description" rows="3" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="keywords">Key Words (Enter key words that can be used to find this policy in a search. Separate each word with a comma. Ex: Health, Finance, Budget)</label>
                        <input type="text" name="keywords" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="file">Upload File</label>
                        <input type="file" name="file_name" class="form-control">
                    </div>

                </div>
                <div class="card-footer">
                    <input type="submit" class="btn btn-primary btn-block" value="Submit">
                </div>

            </form>
        </div>
    </div>


@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection

@endif