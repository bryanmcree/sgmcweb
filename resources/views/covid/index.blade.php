@extends('layouts.app')

@if( Auth::user()->hasRole('CovidAdmin') == FALSE)
    @include('layouts.unauthorized')
@Else

@section('content')

<div class="card bg-secondary text-white mb-4">
    <div class="card-header">
        Covid 19 Screening Statistics
        <span class="float-right">
            <a href="/covid/create" class="btn btn-primary btn-sm">New Screening</a>
        </span>
    </div>
    <div class="card-body">

        <div class="card-deck">

            <div class="card bg-dark text-white mb-3">
                <div class="card-header">
                    # of Screenings Completed
                </div>
                <div class="card-body">
                    <h4> {{ $screenings }} </h4>
                </div>
            </div>

            <div class="card bg-dark text-white mb-3">
                <div class="card-header">
                    # of Screenings FAILED
                </div>
                <div class="card-body">
                    <h4> {{ $failed }} </h4>
                </div>
            </div>

            {{-- <div class="card bg-dark text-white mb-3">
                <div class="card-header">
                    Avg Temperature
                </div>
                <div class="card-body">
                    <h4> {{ $avgTemp }} </h4>
                </div>
            </div> --}}

        </div>

        <hr>

        <table class="table table-dark table-striped table-hover table-bordered" id="screenTable">
            <thead>
                <th>Employee Number</th>
                <th>Employee Name</th>
                {{-- <th>Temperature</th> --}}
                <th>Pass or Fail</th>
                <th>Delete</th>
            </thead>
            <tbody>
                @foreach ($all as $screen)
                    <tr>
                        <td>{{ $screen->emp_number }}</td>
                        <td> {{ $screen->employeeName->name }} </td>
                        {{-- <td> {{ $screen->temp }} </td> --}}
                        <td> 
                            @if($screen->is_passing == 1)
                                <span class="text-success">PASSED</span>
                            @else
                                <span class="text-danger">FAILED</span>
                            @endif
                        </td>
                        <td>
                            <a href="#" class="btn btn-danger btn-sm btn-block deleteBtn" screenID="{{ $screen->id }}">Delete</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    </div>
</div>


@endsection


@section('scripts')

    <script>
        $(document).ready(function() {
            $('#screenTable').DataTable( {
                "pageLength": 25,
                "ordering": false,
                "columnDefs": [ {
                    "targets"  : 'no-sort',
                    "orderable": false,
                }]
            });
        });
    </script>
    
    <script>
        $(document).on("click", ".deleteBtn", function () {
            var screenID = $(this).attr("screenID");
            DeleteScreen(screenID);
        });
    
        function DeleteScreen(screenID) {
            swal({
                title: "Delete Screening?",
                text: "This cannot be undone.",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                confirmButtonText: "Yes, delete it!",
                confirmButtonColor: "#ec6c62"
            }, function() {
                $.ajax({
                    url: "/covid/destroy/" + screenID,
                    type: "GET"
                })
                    .done(function(data) {
                        swal({
                                title: "Deleted",
                                text: "Screening Deleted",
                                type: "success",
                                timer: 1800,
                                showConfirmButton: false
        
                            },function() {
                                location.reload();
                            });
                    })
                    .error(function(data) {
                        swal("Oops", "We couldn't connect to the server!", "error");
                    });
            });
        }
    </script>

@endsection

@endif