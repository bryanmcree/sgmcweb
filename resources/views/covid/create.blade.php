@extends('layouts.app')

@section('content')
    
    <div class="container">
        <div class="card bg-secondary text-white mb-3">
            <div class="card-header">
                New Screening Form
                @if( Auth::user()->hasRole('CovidAdmin') == TRUE)
                    <span class="float-right"><a href="/covid/dashboard" class="btn btn-sm btn-dark">Return to Dashboard</a></span>
                @endif
            </div>
            <div class="card-body">

                <form action="/covid/store" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" value="1" name="is_passing">
                    {{-- <div class="form-row"> --}}
                        <div class="form-group">
                            <label>
                                By entering my employee number and clicking submit, 
                                I attest that I have no fever greater than 100 and no shortness of breath or cough.
                            </label>
                            <input type="text" name="emp_number" class="form-control" required>
                        </div>

                        {{-- <div class="form-group col-lg-4">
                            <label>Temperature</label>
                            <input type="number" step="0.1" name="temp" class="form-control" required>
                        </div> --}}

                        {{-- <div class="form-group col-lg-6">
                            <label>Are you able to pass a Covid 19 test?</label>
                            <select name="is_passing" class="form-control" required>
                                <option value="" selected>Choose Yes/No</option>
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div> --}}
                    {{-- </div> --}}

                    <hr>

                    <input type="submit" class="btn btn-primary btn-block" value="Submit Form">

                </form>

            </div>
        </div>

    @endsection

        {{-- <hr>

        <div class="card bg-dark text-white mb-4">
            <div class="card-header">
                Last 10 Screenings
            </div>
            <div class="card-body">
                <table class="table table-dark table-striped table-hover table-bordered">
                    <thead>
                        <th>Employee Number</th>
                        <th>Employee Name</th>
                        <th>Temperature</th>
                        <th>Pass or Fail</th>
                        <th>Delete</th>
                    </thead>
                    <tbody>
                        @foreach ($lastTen as $ten)
                            <tr>
                                <td>{{ $ten->emp_number }}</td>
                                <td> {{ $ten->employeeName->name }} </td>
                                <td> {{ $ten->temp }} </td>
                                <td> 
                                    @if($ten->is_passing == 1)
                                        <span class="text-success">PASSED</span>
                                    @else
                                        <span class="text-danger">FAILED</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="#" class="btn btn-danger btn-sm btn-block deleteBtn" screenID="{{ $ten->id }}">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>

@endsection

@section('scripts')
    
    <script>
        $(document).on("click", ".deleteBtn", function () {
            var screenID = $(this).attr("screenID");
            DeleteScreen(screenID);
        });
    
        function DeleteScreen(screenID) {
            swal({
                title: "Delete Screening?",
                text: "This cannot be undone.",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                confirmButtonText: "Yes, delete it!",
                confirmButtonColor: "#ec6c62"
            }, function() {
                $.ajax({
                    url: "/covid/destroy/" + screenID,
                    type: "GET"
                })
                    .done(function(data) {
                        swal({
                                title: "Deleted",
                                text: "Screening Deleted",
                                type: "success",
                                timer: 1800,
                                showConfirmButton: false
        
                            },function() {
                                location.reload();
                            });
                    })
                    .error(function(data) {
                        swal("Oops", "We couldn't connect to the server!", "error");
                    });
            });
        }
    </script>

@endsection --}}