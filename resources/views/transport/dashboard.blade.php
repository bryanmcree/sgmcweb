<div class="col-md-12">
    <div class="panel panel-default ">
        <div class="panel-body">
            <div class="col-md-2">
                <div style="text-align: center"><h5><b>Total Request</b></h5>{{$TotalRequest}}</div>
            </div>

            <div class="col-md-2 @if($TotalUnassigned > 0) bg-danger @endif" >
                <div style="text-align: center"><h5><b>Unassigned</b></h5>{{$TotalUnassigned}}</div>
            </div>

            <div class="col-md-1">
                <div style="text-align: center"><h5><b>Assigned</b></h5>{{$TotalAssigned}}</div>
            </div>

            <div class="col-md-2">
                <div style="text-align: center"><h5><b>Complete</b></h5>{{$TotalComplete}}</div>
            </div>

            <div class="col-md-1">
                <div style="text-align: center"><h5><b>Delay</b></h5>{{$TotalDelay}}</div>
            </div>

            <div class="col-md-2">
                <div style="text-align: center"><h5><b>Canceled</b></h5>{{$TotalCanceled}}</div>
            </div>

            <div class="col-md-2">
                <div style="text-align: center"><h5><b>Rescheduled</b></h5>{{$TotalRescheduled}}</div>
            </div>

            *<i>Information for today.</i>
        </div>
    </div>
</div>

<div class="col-md-12">

            @if ($ActiveRequest->isEmpty())
                <div class="container-fluid">

                    <div class="alert alert-info" role="alert"><b>No transports scheduled today.</b> </div>
                    @else

            <table class="table">
                <thead>
                <tr>
                    <td class="hidden-xs hidden-sm hidden-md"><b>Line</b></td>
                    <td><b>Patient Name</b></td>
                    <td class='hidden-xs hidden-sm hidden-md' align="center"><b>Isolation</b></td>
                    <td class="hidden-xs hidden-sm hidden-md"><b>Account</b></td>
                    <td class="hidden-xs hidden-sm hidden-md"><b>Coming From</b></td>
                    <td class="hidden-xs hidden-sm hidden-md"><b>Going To</b></td>
                    <td class="hidden-xs hidden-sm hidden-md"><b>Request Time</b></td>
                    <td class="hidden-xs hidden-sm hidden-md"><b>Assigned Time</b></td>
                    <td class="hidden-xs hidden-sm hidden-md"><b>Min</b></td>
                    <td class="hidden-xs hidden-sm hidden-md"><b>Min</b></td>
                    <td class="hidden-xs hidden-sm hidden-md"><b>Requested By</b></td>
                    <td class="hidden-xs hidden-sm hidden-md"><b>Status</b></td>
                    <td class="hidden-xs hidden-sm hidden-md"><b>Min</b></td>
                    <td class="hidden-xs hidden-sm hidden-md"><b>Assigned To</b></td>
                    @if ( Auth::user()->hasRole('Transport'))
                    <td></td>
                    @endif
                </tr>
                </thead>
                <tbody>
                <?php $rowCount = 0?>
                @foreach ($ActiveRequest as $active)
                <?php $rowCount = $rowCount + 1 ?>
                    <tr


                            @if($active->completed_at == Null AND $active->assigned_at == Null AND $active->status =='Unassigned' AND \Carbon\Carbon::createFromTimeStamp(strtotime($active->created_at))->diffInMinutes()> 20 ) class="danger" @endif

                            @if($active->status =='Unassigned')
                            class="bg-warning"
                            @elseif($active->status=='Assigned')
                            class="bg-success"
                            @elseif($active->status=='Completed')
                            class="active"
                            @endif



                    >
                        <td class="hidden-xs hidden-sm hidden-md"><b>{{$rowCount}})</b></td>
                        <td> @if ( Auth::user()->hasRole('Transport')) {{$active->patient_last_name}}, {{$active->patient_first_name}} @else @if($active->patient_first_name =='' OR $active->patient_last_name =='') LNU, FNU @Else {{PatientData::scrambledata($active->patient_last_name)}}@endif @endif  @if($active->er_patient =='Yes')<i class="fa fa-exclamation-circle" aria-hidden="true"></i> @endif</td>
                        <td align="center hidden-xs hidden-sm hidden-md">@if($active->mask !='No')<i class="alert-danger fa fa-exclamation-circle" aria-hidden="true"></i> {{$active->mask}} @endif</td>
                        <td class="hidden-xs hidden-sm hidden-md">@if ( Auth::user()->hasRole('Transport')){{$active->mrn_number}} @else @if($active->mrn_number =='') 00000 @else {{PatientData::scrambledata($active->mrn_number)}} @endif @endif </td>
                        <td class="hidden-xs hidden-sm hidden-md">{{$active->from_building}}</td>
                        <td class="hidden-xs hidden-sm hidden-md">{{$active->to_building}}</td>
                        <td class="hidden-xs hidden-sm hidden-md">{{$active->created_at}}</td>
                        <td class="hidden-xs hidden-sm hidden-md">{{$active->assigned_at}}</td>
                        <td class="hidden-xs hidden-sm hidden-md">@if($active->assigned_at == Null){{\Carbon\Carbon::createFromTimeStamp(strtotime($active->created_at))->diffInMinutes()}} @else {{\Carbon\Carbon::createFromTimeStamp(strtotime($active->created_at))->diffInMinutes(\Carbon\Carbon::createFromTimeStamp(strtotime($active->assigned_at)))}}@endif</td>
                        <td class="hidden-xs hidden-sm hidden-md">@if($active->completed_at == Null)0 @else{{\Carbon\Carbon::createFromTimeStamp(strtotime($active->assigned_at))->diffInMinutes(\Carbon\Carbon::createFromTimeStamp(strtotime($active->completed_at)))}}@endif</td>
                        <td class="hidden-xs hidden-sm hidden-md">{{$active->requesterInfo->last_name}}, {{$active->requesterInfo->first_name}}</td>
                        <td class="hidden-xs hidden-sm hidden-md">{{$active->status}}</td>
                        <td class="hidden-xs hidden-sm hidden-md">@if($active->completed_at != Null){{\Carbon\Carbon::createFromTimeStamp(strtotime($active->created_at))->diffInMinutes(\Carbon\Carbon::createFromTimeStamp(strtotime($active->completed_at)))}}@endif</td>
                        <td class="hidden-xs hidden-sm hidden-md">@if($active->tech1 != Null){{$active->transporterInfo->last_name}}, {{$active->transporterInfo->first_name}} @endif</td>
                        @if ( Auth::user()->hasRole('Transport'))
                        <td align="right">
                            <a href="#" class="Assign btn btn-sgmc btn-sm"
                               data-id = "{{$active->id}}"
                               data-patient_name = "{{$active->patient_last_name}}, {{$active->patient_first_name}} - {{$active->mrn_number}}"
                               data-patient_last_name = "{{$active->patient_last_name}}"
                               data-patient_first_name = "{{$active->patient_first_name}}"
                               data-requester = "{{$active->requesterInfo->last_name}}, {{$active->requesterInfo->first_name}}"
                               data-type = "{{$active->type}}"
                               data-mrn_number = "{{$active->mrn_number}}"
                               data-status = "{{$active->status}}"
                               data-service = "{{$active->service}}"
                               data-from_building = "{{$active->from_building}}"
                               data-from_room = "{{$active->from_room}}"
                               data-to_room = "{{$active->to_room}}"
                               data-to_building = "{{$active->to_building}}"
                               data-tech1 = "{{$active->tech1}}"
                               data-tech2 = "{{$active->tech2}}"
                               data-notes = "{{$active->notes}}"
                               style="display: inline-block;" data-toggle="modal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Assign</a>
                        </td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
                        @endif
        </div>
</div>