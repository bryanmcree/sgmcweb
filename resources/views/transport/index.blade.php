
@extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')


        <div class="panel panel-default">
            <div class="panel-heading">
                <h5><b>Transport Control Panel</b></h5>
            </div>
            <div class="panel-body">

                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#home">Dashboard</a></li>
                    <li><a data-toggle="tab" href="#menu1">Completed</a></li>
                    @if ( Auth::user()->hasRole('Transport'))
                    <li class="hidden-xs hidden-sm hidden-md"><a data-toggle="tab" href="#menu2">Reports</a></li>
                    @endif
                    <li><a data-toggle="tab" href="#menu3" class="bg-primary"><b>New Request</b></a></li>
                </ul>

                <div class="tab-content">
                    <div id="home" class="tab-pane fade in active">
                        <div class="" id="LoadDashboard"></div>
                    </div>
                    <div id="menu1" class="tab-pane fade">
                        <div class="" id="LoadCompleted"></div>
                    </div>
                    @if ( Auth::user()->hasRole('Transport'))
                    <div id="menu2" class="tab-pane fade hidden-xs hidden-sm hidden-md">
                        <h3>Reports - Coming Soon</h3>
                    </div>
                    @endif
                    <div id="menu3" class="tab-pane fade">



                        {!! Form::open(array('action' => ['TransportController@newRequest'], 'class' => 'form_control')) !!}
                        {!! Form::hidden('requester', Auth::user()->employee_number,['class'=>'contacts_id']) !!}
                        {!! Form::hidden('status', 'Unassigned',['class'=>'contacts_id']) !!}
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel panel-heading">
                                    <h2>Patient Transport Request</h2> Open between 7AM and 11:30 PM
                                </div>
                                <div class="panel-body">


                                    @if ( Auth::user()->hasRole('Transport'))
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            {!! Form::label('lunch_break', 'Transporter Lunch Break:') !!}

                                            {!! Form::select('lunch_break', [
                                                                'No' => 'No',
                                                                'Yes' => 'Yes',
                                                                 ], null, ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                    @endif
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            {!! Form::label('patient_last_name', 'Patient Last Name:') !!}
                                            <div class="input-group">
                                                <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                                {!! Form::text('patient_last_name', null, ['class' => 'form-control', 'id'=>'patient_last_name']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            {!! Form::label('patient_first_name', 'Patient First Name:') !!}
                                            <div class="input-group">
                                                <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                                {!! Form::text('patient_first_name', null, ['class' => 'form-control', 'id'=>'patient_first_name']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            {!! Form::label('mrn_number', 'Patient Account Number:') !!}
                                            <div class="input-group">
                                                <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                                {!! Form::text('mrn_number', '00000', ['class' => 'form-control', 'id'=>'patient_last_name', 'required', 'minlength' => '5' ]) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            {!! Form::label('mask', 'Isolation:') !!}
                                            <div class="input-group">
                                                <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                                {!! Form::select('mask', [
                                                                    'No' => 'No',
                                                                    'Yes - Contact' => 'Yes - Contact',
                                                                    'Yes - Airborne' => 'Yes - Airborne',
                                                                    'Yes - Contact / Airborne' => 'Yes - Contact / Airborne',
                                                                     ], null, ['class' => 'form-control']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">

                                        @if ( Auth::user()->hasRole('Transport'))
                                            <div class="form-group">
                                                {!! Form::label('type', 'Transport Type:') !!}
                                                <div class="input-group">
                                                    <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                                    {!! Form::select('type', [
                                                                        '' => '[Select Type]',
                                                                        'Patient Move' => 'Patient Move',
                                                                        '15 Minute Break' => '15 Minute Break',
                                                                        'Admission' => 'Admission',
                                                                        'Admitting' => 'Admitting',
                                                                        'AV Equipment' => 'AV Equipment',
                                                                        'Courtsey' => 'Courtsey',
                                                                        'Discharge' => 'Discharge',
                                                                        'ER Move' => 'ER Move',
                                                                        'General Equipment' => 'General Equipment',
                                                                        'Lunch Break' => 'Lunch Break',
                                                                        'Outpatient Move' => 'Outpatient Move',
                                                                        'Staff Request' => 'Staff Request',
                                                                        'TGO' => 'TGO',
                                                                        'To Be Admitted' => 'To Be Admitted',
                                                                        'Transfer' => 'Transfer',

                                                                         ], null, ['class' => 'form-control']) !!}
                                                </div>
                                            </div>
                                        @else

                                        <div class="form-group">
                                            {!! Form::label('type', 'Transport Type:') !!}
                                            <div class="input-group">
                                                <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                                {!! Form::select('type', [
                                                                    '' => '[Select Type]',
                                                                    'Patient Move' => 'Patient Move',
                                                                    'Admitting' => 'Admitting',
                                                                    'Courtsey' => 'Courtsey',
                                                                    'Discharge' => 'Discharge',
                                                                    'Transfer' => 'Transfer',
                                                                     ], null, ['class' => 'form-control']) !!}
                                            </div>
                                        </div>
                                            @endif
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            {!! Form::label('service', 'Service Type:') !!}
                                            <div class="input-group">
                                                <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                                {!! Form::select('service', [
                                                                    '' => '[Select Service]',
                                                                    'STRETCHER/IV' => 'STRETCHER/IV',
                                                                    'WHEELCHAIR/IV' => 'WHEELCHAIR/IV',
                                                                    'AMBULATORY/IV' => 'AMBULATORY/IV',
                                                                    'BED/O2' => 'BED/O2',
                                                                    'STRETCHER/O2' => 'STRETCHER/O2',
                                                                    'WHEELCHAIR/O2' => 'WHEELCHAIR/O2',
                                                                    'AMBULATORY/O2' => 'AMBULATORY/O2',
                                                                    'BED/IV/O2' => 'BED/IV/O2',
                                                                    'STRETCHER/IV/O2' => 'STRETCHER/IV/O2',
                                                                    'WHEELCHAIR/IV/O2' => 'WHEELCHAIR/IV/O2',
                                                                    'AMBULATORY/IV/O2' => 'AMBULATORY/IV/O2',
                                                                    'STRETCHER' => 'STRETCHER',
                                                                    'WHEELCHAIR' => 'WHEELCHAIR',
                                                                    'BED' => 'BED',
                                                                    'AMBULATORY' => 'AMBULATORY',

                                                                     ], null, ['class' => 'form-control']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            {!! Form::label('er_patient', 'ER / Priority Patient:') !!}

                                                {!! Form::select('er_patient', [
                                                                    'No' => 'No',
                                                                    'Yes' => 'Yes',
                                                                     ], null, ['class' => 'form-control']) !!}
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="panel panel-default">
                                            <div class="panel panel-heading-custom">
                                                <h3>From:</h3>
                                            </div>
                                            <div class="panel-body">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        {!! Form::label('from_building', 'From Building:') !!}
                                                        <div class="input-group">
                                                            <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                                            {!! Form::select('from_building', [
                                                                                '' => '[Select Type]',
                                                                                'Tower' => 'Tower',
                                                                                'Main Building' => 'Main Building',
                                                                                'Cancer Center' => 'Cancer Center',
                                                                                'Youth Care' => 'Youth Care',
                                                                                'Emergency Department' => 'Emergency Department',
                                                                                'Rehab' => 'Rehab',
                                                                                'Out Patient Center' => 'Out Patient Center',
                                                                                'Out Patient Surgery' => 'Out Patient Surgery',
                                                                                'Wound Care' => 'Wound Care',
                                                                                'Radiology' => 'Radiology',
                                                                                'Dialysis' => 'Dialysis',
                                                                                'Respiratory' => 'Respiratory',
                                                                                'Echo' => 'Echo',
                                                                                'Nuc-Med' => 'Nuc-Med',
                                                                                'CT' => 'CT',
                                                                                'NM' => 'NM',
                                                                                'US' => 'US',
                                                                                'EKG' => 'EKG',
                                                                                'MRI' => 'MRI',

                                                                                 ], null, ['class' => 'form-control']) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        {!! Form::label('from_room', 'From Room:') !!}
                                                        {!! Form::text('from_room', null, ['class' => 'form-control', 'id'=>'from_room']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="panel panel-default">
                                            <div class="panel panel-heading-custom">
                                                <h3>To:</h3>
                                            </div>
                                            <div class="panel-body">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        {!! Form::label('to_building', 'To Building:') !!}
                                                        <div class="input-group">
                                                            <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                                            {!! Form::select('to_building', [
                                                                                '' => '[Select Type]',
                                                                                'Tower' => 'Tower',
                                                                                'Main Building' => 'Main Building',
                                                                                'Cancer Center' => 'Cancer Center',
                                                                                'Youth Care' => 'Youth Care',
                                                                                'Emergency Department' => 'Emergency Department',
                                                                                'Rehab' => 'Rehab',
                                                                                'Out Patient Center' => 'Out Patient Center',
                                                                                'Out Patient Surgery' => 'Out Patient Surgery',
                                                                                'Wound Care' => 'Wound Care',
                                                                                'Radiology' => 'Radiology',
                                                                                'Dialysis' => 'Dialysis',
                                                                                'Respiratory' => 'Respiratory',
                                                                                'Echo' => 'Echo',
                                                                                'Nuc-Med' => 'Nuc-Med',
                                                                                'CT' => 'CT',
                                                                                'NM' => 'NM',
                                                                                'US' => 'US',
                                                                                'EKG' => 'EKG',
                                                                                'MRI' => 'MRI',

                                                                                 ], null, ['class' => 'form-control']) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        {!! Form::label('to_room', 'To Room:') !!}
                                                        {!! Form::text('to_room', null, ['class' => 'form-control', 'id'=>'from_room']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            {!! Form::label('notes', 'Patient Notes:') !!}
                                            {!! Form::textarea('notes', null, ['class' => 'form-control', 'id'=>'sys_alias', 'rows' => 2]) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <div class="RightLeft">
                                        {!! Form::reset('Clear', ['class'=>'btn btn-default', 'id'=>'updateButton']) !!}
                                        {!! Form::submit('Request Transport', ['class'=>'btn btn-sgmc', 'id'=>'updateButton']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>

            </div>
        </div>






        <div class="" id="LoadDashboard"></div>

@include('transport.modals.assign')

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">
    $("#LoadDashboard").load('/transport/dashboard').hide().fadeIn();
    $("#LoadCompleted").load('/transport/completed').hide().fadeIn();

    var refreshId4 = setInterval(function() {
        $("#LoadDashboard").load('/transport/dashboard');
    }, 2000);

    var refreshId5 = setInterval(function() {
        $("#LoadCompleted").load('/transport/completed');
    }, 2000);



    $(document).on("click", ".Assign", function () {
        //alert($(this).attr("data-requester"));
        $('.id').val($(this).attr("data-id"));
        $('.patient_first_name').val($(this).attr("data-patient_first_name"));
        $('.patient_name').val($(this).attr("data-patient_name"));
        $('.patient_last_name').val($(this).attr("data-patient_last_name"));
        $('.type').val($(this).attr("data-type"));
        $('.mrn_number').val($(this).attr("data-mrn_number"));
        $('.from_building').val($(this).attr("data-from_building"));
        $('.from_room').val($(this).attr("data-from_room"));
        $('.to_building').val($(this).attr("data-to_building"));
        $('.to_room').val($(this).attr("data-to_room"));
        $('.tech1').val($(this).attr("data-tech1"));
        $('.tech2').val($(this).attr("data-tech2"));
        $('.status').val($(this).attr("data-status"));
        $('.requester').html($(this).attr("data-requester"));
        $('.service').val($(this).attr("data-service"));
        $('.notes').val($(this).attr("data-notes"));
        $('.AssignModal').modal('show');
        //modal.find('.patient_last_name').text($(this).attr("data-patient_last_name"));
    });
</script>

@endsection

