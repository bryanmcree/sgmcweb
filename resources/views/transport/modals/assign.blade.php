<div class="modal fade AssignModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><b>Assign Transport</b></h4>
            </div>
            <div class="modal-body bg-default">
                {!! Form::open(array('action' => ['TransportController@assign'], 'class' => 'form_control')) !!}
                {!! Form::hidden('id', null,['class'=>'id']) !!}
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <b>Patient Information</b>
                            </div>
                            <div class="panel-body">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('patient_last_name', 'Patient Last Name:') !!}
                                        <div class="input-group">
                                            <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                            {!! Form::text('patient_last_name', null, ['class' => 'form-control patient_last_name', 'id'=>'patient_last_name']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('patient_first_name', 'Patient First Name:') !!}
                                        <div class="input-group">
                                            <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                            {!! Form::text('patient_first_name', null, ['class' => 'form-control patient_first_name', 'id'=>'patient_first_name']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('mrn_number', 'Patient Account Number:') !!}
                                        <div class="input-group">
                                            <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                            {!! Form::text('mrn_number', null, ['class' => 'form-control mrn_number', 'id'=>'mrn_number']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <b>Service Type</b>
                            </div>
                            <div class="panel-body">
                                    <div class="input-group">
                                        <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                        {!! Form::select('service', [
                                                            '' => '[Select Service]',
                                                            'STRETCHER/IV' => 'STRETCHER/IV',
                                                            'WHEELCHAIR/IV' => 'WHEELCHAIR/IV',
                                                            'AMBULATORY/IV' => 'AMBULATORY/IV',
                                                            'BED/O2' => 'BED/O2',
                                                            'STRETCHER/O2' => 'STRETCHER/O2',
                                                            'WHEELCHAIR/O2' => 'WHEELCHAIR/O2',
                                                            'AMBULATORY/O2' => 'AMBULATORY/O2',
                                                            'BED/IV/O2' => 'BED/IV/O2',
                                                            'STRETCHER/IV/O2' => 'STRETCHER/IV/O2',
                                                            'WHEELCHAIR/IV/O2' => 'WHEELCHAIR/IV/O2',
                                                            'AMBULATORY/IV/O2' => 'AMBULATORY/IV/O2',
                                                            'STRETCHER' => 'STRETCHER',
                                                            'WHEELCHAIR' => 'WHEELCHAIR',
                                                            'BED' => 'BED',
                                                            'AMBULATORY' => 'AMBULATORY',

                                                             ], null, ['class' => 'form-control service']) !!}
                                    </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <b>Transport Type</b>
                            </div>
                            <div class="panel-body">
                                <div class="input-group">
                                    <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                    {!! Form::select('type', [
                                                        '' => '[Select Type]',
                                                        'Patient Move' => 'Patient Move',
                                                        '15 Minute Break' => '15 Minute Break',
                                                        'Admission' => 'Admission',
                                                        'Admitting' => 'Admitting',
                                                        'AV Equipment' => 'AV Equipment',
                                                        'Courtsey' => 'Courtsey',
                                                        'Discharge' => 'Discharge',
                                                        'ER Move' => 'ER Move',
                                                        'General Equipment' => 'General Equipment',
                                                        'Lunch Break' => 'Lunch Break',
                                                        'Outpatient Move' => 'Outpatient Move',
                                                        'Staff Request' => 'Staff Request',
                                                        'TGO' => 'TGO',
                                                        'To Be Admitted' => 'To Be Admitted',
                                                        'Transfer' => 'Transfer',

                                                         ], null, ['class' => 'form-control type']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <b>From Building / Room</b>
                            </div>
                            <div class="panel-body">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                        {!! Form::select('from_building', [
                                                            '' => '[Select Type]',
                                                            'Tower' => 'Tower',
                                                            'Main Building' => 'Main Building',
                                                            'Cancer Center' => 'Cancer Center',
                                                            'Youth Care' => 'Youth Care',
                                                            'Emergency Department' => 'Emergency Department',
                                                            'Rehab' => 'Rehab',
                                                            'Out Patient Center' => 'Out Patient Center',
                                                            'Out Patient Surgery' => 'Out Patient Surgery',
                                                            'Wound Care' => 'Wound Care',
                                                            'Radiology' => 'Radiology',
                                                            'Dialysis' => 'Dialysis',
                                                            'Respiratory' => 'Respiratory',
                                                            'Echo' => 'Echo',
                                                            'Nuc-Med' => 'Nuc-Med',
                                                            'CT' => 'CT',
                                                            'NM' => 'NM',
                                                            'US' => 'US',
                                                            'EKG' => 'EKG',

                                                             ], null, ['class' => 'form-control from_building']) !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    {!! Form::text('from_room', null, ['class' => 'form-control from_room', 'id'=>'from_room']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <b>To Building / Room</b>
                            </div>
                            <div class="panel-body">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                        {!! Form::select('to_building', [
                                                            '' => '[Select Type]',
                                                            'Tower' => 'Tower',
                                                            'Main Building' => 'Main Building',
                                                            'Cancer Center' => 'Cancer Center',
                                                            'Youth Care' => 'Youth Care',
                                                            'Emergency Department' => 'Emergency Department',
                                                            'Rehab' => 'Rehab',
                                                            'Out Patient Center' => 'Out Patient Center',
                                                            'Out Patient Surgery' => 'Out Patient Surgery',
                                                            'Wound Care' => 'Wound Care',
                                                            'Radiology' => 'Radiology',
                                                            'Dialysis' => 'Dialysis',
                                                            'Respiratory' => 'Respiratory',
                                                            'Echo' => 'Echo',
                                                            'Nuc-Med' => 'Nuc-Med',
                                                            'CT' => 'CT',
                                                            'NM' => 'NM',
                                                            'US' => 'US',
                                                            'EKG' => 'EKG',

                                                             ], null, ['class' => 'form-control to_building']) !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    {!! Form::text('to_room', null, ['class' => 'form-control to_room', 'id'=>'from_room']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <b>Notes</b>
                            </div>
                            <div class="panel-body">
                                {!! Form::textarea('notes', null, ['class' => 'form-control notes', 'id'=>'sys_alias', 'rows' => 2]) !!}
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::label('tech1', 'Assigned Tech:') !!}
                            <div class="input-group">
                                <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>

                                <select name="tech1" class="form-control tech1">
                                    <option value=""></option>
                                    @foreach($transporters as $transport)
                                        <option value="{{ $transport->employee_number }}"> {{$transport->last_name}}, {{$transport->first_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::label('tech2', 'Assigned Additional Tech:') !!}

                                <select name="tech2" class="form-control tech2">
                                    <option value=""></option>
                                    @foreach($transporters as $transport)
                                        <option value="{{ $transport->employee_number }}"> {{$transport->last_name}}, {{$transport->first_name}}</option>
                                    @endforeach
                                </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::label('status', 'Status:') !!}
                            <div class="input-group">
                                <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                {!! Form::select('status', [
                                                    '' => '[Select Status]',
                                                    'Complete' => 'Complete',
                                                    'Delay' => 'Delay',
                                                    'Canceled' => 'Canceled',
                                                    'Rescheduled' => 'Rescheduled',
                                                    'Unassigned' => 'Unassigned',
                                                    'Assigned' => 'Assigned',

                                                     ], null, ['class' => 'form-control status']) !!}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer bg-default">
                <div class="RightLeft">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    {!! Form::submit('Assign Transport', ['class'=>'btn btn-sgmc', 'id'=>'AddButton']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>