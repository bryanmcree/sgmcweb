<div class="col-md-12">

    @if ($ActiveRequest->isEmpty())
        <div class="container-fluid">

            <div class="alert alert-info" role="alert"><b>No transports scheduled today.</b> </div>
            @else
                <table class="table">
                    <thead>
                    <tr>
                        <td class="hidden-xs hidden-sm hidden-md"><b>Line</b></td>
                        <td><b>Patient Name</b></td>
                        <td class="hidden-xs hidden-sm hidden-md" align="center"><b>Isolation</b></td>
                        <td class="hidden-xs hidden-sm hidden-md"><b>Account</b></td>
                        <td class="hidden-xs hidden-sm hidden-md"><b>Request Time</b></td>
                        <td class="hidden-xs hidden-sm hidden-md"><b>Assigned Time</b></td>
                        <td class="hidden-xs hidden-sm hidden-md"><b>Min</b></td>
                        <td class="hidden-xs hidden-sm hidden-md"><b>Completed Time</b></td>
                        <td class="hidden-xs hidden-sm hidden-md"><b>Min</b></td>
                        <td class="hidden-xs hidden-sm hidden-md"><b>Requested By</b></td>
                        <td class="hidden-xs hidden-sm hidden-md"><b>Status</b></td>
                        <td class="hidden-xs hidden-sm hidden-md"><b>Min</b></td>
                        <td class="hidden-xs hidden-sm hidden-md"><b>Assigned To</b></td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $rowCount = 0?>
                    @foreach ($ActiveRequest as $active)
                        <?php $rowCount = $rowCount + 1 ?>
                        <tr


                                @if($active->completed_at == Null AND $active->assigned_at == Null AND $active->status =='Unassigned' AND \Carbon\Carbon::createFromTimeStamp(strtotime($active->created_at))->diffInMinutes()> 20 ) class="danger" @endif

                        @if($active->status =='Unassigned')
                        class="bg-warning"
                                @elseif($active->status=='Assigned')
                                class="bg-success"
                                @elseif($active->status=='Completed')
                                class="active"
                                @endif



                        >
                            <td class="hidden-xs hidden-sm hidden-md"><b>{{$rowCount}})</b></td>
                            <td>{{$active->patient_last_name}}, {{$active->patient_first_name}}</td>
                            <td class="hidden-xs hidden-sm hidden-md" align="center">@if($active->mask !='No')<i class="alert-danger fa fa-exclamation-circle" aria-hidden="true"></i> {{$active->mask}} @endif</td>
                            <td class="hidden-xs hidden-sm hidden-md">{{$active->mrn_number}}</td>
                            <td class="hidden-xs hidden-sm hidden-md">{{$active->created_at}}</td>
                            <td class="hidden-xs hidden-sm hidden-md">{{$active->assigned_at}}</td>
                            <td class="hidden-xs hidden-sm hidden-md">@if($active->assigned_at == Null){{\Carbon\Carbon::createFromTimeStamp(strtotime($active->created_at))->diffInMinutes()}} @else {{\Carbon\Carbon::createFromTimeStamp(strtotime($active->created_at))->diffInMinutes(\Carbon\Carbon::createFromTimeStamp(strtotime($active->assigned_at)))}}@endif</td>
                            <td class="hidden-xs hidden-sm hidden-md">{{$active->completed_at}}</td>
                            <td class="hidden-xs hidden-sm hidden-md">@if($active->completed_at == Null)0 @else{{\Carbon\Carbon::createFromTimeStamp(strtotime($active->assigned_at))->diffInMinutes(\Carbon\Carbon::createFromTimeStamp(strtotime($active->completed_at)))}}@endif</td>
                            <td class="hidden-xs hidden-sm hidden-md">{{$active->requesterInfo->last_name}}, {{$active->requesterInfo->first_name}}</td>
                            <td class="hidden-xs hidden-sm hidden-md">{{$active->status}}</td>
                            <td class="hidden-xs hidden-sm hidden-md">@if($active->completed_at != Null){{\Carbon\Carbon::createFromTimeStamp(strtotime($active->created_at))->diffInMinutes(\Carbon\Carbon::createFromTimeStamp(strtotime($active->completed_at)))}}@endif</td>
                            <td class="hidden-xs hidden-sm hidden-md">@if($active->tech1 != Null){{$active->transporterInfo->last_name}}, {{$active->transporterInfo->first_name}} @endif</td>
                            <td align="right">
                                <a href="#" class="Assign btn btn-sgmc btn-sm"
                                   data-id = "{{$active->id}}"
                                   data-patient_name = "{{$active->patient_last_name}}, {{$active->patient_first_name}} - {{$active->mrn_number}}"
                                   data-patient_last_name = "{{$active->patient_last_name}}"
                                   data-patient_first_name = "{{$active->patient_first_name}}"
                                   data-requester = "{{$active->requesterInfo->last_name}}, {{$active->requesterInfo->first_name}}"
                                   data-type = "{{$active->type}}"
                                   data-mrn_number = "{{$active->mrn_number}}"
                                   data-status = "{{$active->status}}"
                                   data-service = "{{$active->service}}"
                                   data-from_building = "{{$active->from_building}}"
                                   data-from_room = "{{$active->from_room}}"
                                   data-to_room = "{{$active->to_room}}"
                                   data-to_building = "{{$active->to_building}}"
                                   data-tech1 = "{{$active->tech1}}"
                                   data-tech2 = "{{$active->tech2}}"
                                   data-notes = "{{$active->notes}}"
                                   style="display: inline-block;" data-toggle="modal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Assign</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
</div>