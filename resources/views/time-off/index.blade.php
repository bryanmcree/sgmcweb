{{--New file Template--}}

{{--Add Security for this page below--}}


    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <b>Time Off Request</b>
            </div>
            <div class="panel-body">


                    <div class="col-lg-6">

                        <form method="post" action="/time-off/add">
                            {{ csrf_field() }}
                            <input type="hidden" value="{{ Auth::user()->employee_number }}" name="employee_number">
                            <input type="hidden" value="{{ Auth::user()->unit_code }}" name="cost_center">
                            <input type="hidden" value="Pending" name="request_status">

                        <div class="form-group">
                            <label for="required_id">Type of Request?</label>
                            <select class="form-control" id="required_id" name="request_type" required>
                                <option value="">[Select One]</option>
                                <option value="PTO">PTO</option>
                                <option value="Schedule Change">Schedule Change</option>
                                <option value="HPTO">HPTO</option>
                            </select>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="e5_ava_id">Start Date:</label>
                                    <input type="date" class="form-control" id="e5_ava_id" name="start_date">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="e5_ava_id">Start Time:</label>
                                    <input type="time" class="form-control" id="e5_ava_id" name="start_time">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="e5_ava_id">Return Date:</label>
                                    <input type="date" class="form-control" id="e5_ava_id" name="return_date">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="e5_ava_id">Return Time:</label>
                                    <input type="time" class="form-control" id="e5_ava_id" name="return_time">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="title_id">Notes or Comments:</label>
                            <textarea class="form-control" name="notes" id="consumables_id" rows="3"></textarea>
                        </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                    </div>


            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
