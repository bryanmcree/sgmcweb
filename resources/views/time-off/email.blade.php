<table cellpadding="4" cellspacing="0" border="1">
    <tbody>
        <tr>
            <td><b>Employee Name:</b></td>
            <td>{{$requested_by['name']}}</td>
        </tr>
        <tr>
            <td><b>Employee Title:</b></td>
            <td>{{$requested_by['title']}}</td>
        </tr>
        <tr>
            <td><b>Employee Number:</b></td>
            <td>{{$employee_number}}</td>
        </tr>
        <tr>
            <td><b>Employee Email:</b></td>
            <td>{{$requested_by['mail']}}</td>
        </tr>
        <tr>
            <td><b>Request Type:</b></td>
            <td>{{$request_type}}</td>
        </tr>
        <tr>
            <td><b>Start Date / Time:</b></td>
            <td>{{$start_date}} / {{$start_time}}</td>
        </tr>
        <tr>
            <td><b>Return Date / Time:</b></td>
            <td>{{$return_date}} / {{$return_time}}</td>
        </tr>
        <tr>
            <td><b>Notes:</b></td>
            <td>{{$notes}}</td>
        </tr>
    </tbody>
</table>