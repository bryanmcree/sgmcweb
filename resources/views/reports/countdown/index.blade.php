@extends('layouts.nonav')

@section('content')
    <meta http-equiv="refresh" content="300">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading panel-heading-custom" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <i class="fa fa-question-circle"  data-toggle="popover" data-placement="top"
                       title="EPIC Go Live"
                       data-content="Countdown until EPIC go live."></i> ->
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne6" aria-expanded="true" aria-controls="collapseOne4">
                        <b> NEDOCS</b>
                    </a>
                </h4><i>{{Carbon\Carbon::now()->toDateTimeString()}}</i>
            </div>
            <div id="collapseOne6" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">

                <div class="panel-body" >
                    <div class="col-md-12">
                        <div class="panel panel-default">

                            <div class="panel-body">
                                <div class="progress">
                                    @if($nedocs->NEDOCS_SCORE <= 50)
                                    <div class="progress-bar progress-bar-success" style="width: {{($nedocs->NEDOCS_SCORE)/2}}%">
                                        <span>Not Crowded ({{$nedocs->NEDOCS_SCORE}})</span>
                                    </div>
                                    @elseif($nedocs->NEDOCS_SCORE >= 51 && $nedocs->NEDOCS_SCORE <= 100)
                                    <div class="progress-bar progress-bar-info" style="width: {{($nedocs->NEDOCS_SCORE)/2}}%">
                                        <span>Busy ({{$nedocs->NEDOCS_SCORE}})</span>
                                    </div>
                                    @elseif($nedocs->NEDOCS_SCORE >= 101 && $nedocs->NEDOCS_SCORE <= 140)
                                    <div class="progress-bar progress-bar-warning" style="width: {{($nedocs->NEDOCS_SCORE)/2}}%">
                                        <span>Overcrowded ({{$nedocs->NEDOCS_SCORE}})</span>
                                    </div>
                                    @elseif($nedocs->NEDOCS_SCORE >= 141 && $nedocs->NEDOCS_SCORE <= 180)
                                    <div class="progress-bar progress-bar-warning progress-bar-striped active" style="width: {{($nedocs->NEDOCS_SCORE)/2}}%">
                                        <span>Severly Overcrowded ({{$nedocs->NEDOCS_SCORE}})</span>
                                    </div>
                                    @elseif($nedocs->NEDOCS_SCORE >= 181)
                                    <div class="progress-bar progress-bar-danger progress-bar-striped active" style="width: {{($nedocs->NEDOCS_SCORE)/2}}%">
                                        <span>Dangersouly Overcrowded ({{$nedocs->NEDOCS_SCORE}})</span>
                                    </div>
                                    @endif
                                </div>

                            </div>
                        </div>
                    </div>


                    <div class="col-md-2">
                        <div class="panel panel-default">
                            <div class="panel-heading panel-heading-custom">
                                <div class="panel-title"><b>NEDOCS Updated</b></div>
                            </div>
                            <div class="panel-body" align="center">
                                <div style="font-size: 19px; font-weight: bold;" align="center">{{$nedocs->SCORE_DT}}</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="panel panel-default">
                            <div class="panel-heading panel-heading-custom">
                                <div class="panel-title"><b>Longest Lobby Wait For Bed</b></div>
                            </div>
                            <div class="panel-body">
                                <div style="font-size: 19px; font-weight: bold;" align="center">{{date('H:i:s', mktime(0,$nedocs->LONGEST_LOBBY_WAIT_FOR_BED))}}</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="panel panel-default">
                            <div class="panel-heading panel-heading-custom">
                                <div class="panel-title"><b>Longest Admit Time</b></div>
                            </div>
                            <div class="panel-body">
                                <div style="font-size: 19px; font-weight: bold;" align="center">{{date('H:i:s', mktime(0,$nedocs->LONGEST_ADMIT_TIME))}}</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="panel panel-default">
                            <div class="panel-heading panel-heading-custom">
                                <div class="panel-title"><b>Longest Wait For Transfer</b></div>
                            </div>
                            <div class="panel-body">
                                <div style="font-size: 19px; font-weight: bold;" align="center">{{date('H:i:s', mktime(0,$nedocs->LONGEST_WAIT_FOR_TRANSFER))}}</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="panel panel-default">
                            <div class="panel-heading panel-heading-custom">
                                <div class="panel-title"><b>Avg Decision To Disposition</b></div>
                            </div>
                            <div class="panel-body">
                                <div style="font-size: 19px; font-weight: bold;" align="center">{{date('H:i:s', mktime(0,$nedocs->AVG_DECISION_TO_DISP_TO_NOW))}}</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="panel panel-default">
                            <div class="panel-heading panel-heading-custom">
                                <div class="panel-title"><b>Longest Discharge Time</b></div>
                            </div>
                            <div class="panel-body">
                                <div style="font-size: 19px; font-weight: bold;" align="center">{{date('H:i:s', mktime(0,$nedocs->LONGEST_DISCHARGE_TIME))}}</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading panel-heading-custom">
                                <div class="panel-title"><b>Total Patients</b></div>
                            </div>
                            <div class="panel-body" align="center">
                                <div id="chart_div" ></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading panel-heading-custom">
                                <div class="panel-title"><b>Epic Go Live</b></div>
                            </div>
                            <div class="panel-body" align="center">
                                <div id="DateCountdown" data-date="2017-11-01 00:00:00" style="width: 60%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    </div>
</div>

@endsection

@section('scripts')

    <script type="application/javascript">

        google.charts.load('current', {'packages':['gauge']});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {

            var data = google.visualization.arrayToDataTable([
                ['Label', 'Value'],
                ['ED', 0],
                ['ED BED', 0],
                ['Lobby', 0],
                ['Admits', 0],
                ['Acuity "1"', 0],
                ['Transfers', 0],
                ['Discharges', 0]
            ]);

            var options = {
                 height: 250,
                redFrom: 90, redTo: 100,
                yellowFrom:75, yellowTo: 90,
                minorTicks: 5,
                animation:{
                    duration: 1000,
                    easing: 'inAndOut',
                }
            };

            var chart = new google.visualization.Gauge(document.getElementById('chart_div'));
            chart.draw(data, options);
            //clearChart();


            // This is new.
            setTimeout(function(){
                var data = google.visualization.arrayToDataTable([
                    ['Label', 'Value'],
                    ['ED', {{$nedocs->TOTAL_PATIENTS_IN_ED}}],
                    ['ED BED', {{$nedocs->TOTAL_PATIENTS_IN_ER_BED}}],
                    ['Lobby', {{$nedocs->TOTAL_PATIENTS_IN_LOBBY}}],
                    ['Admits', {{$nedocs->NUMBER_OF_ADMITS}}],
                    ['Acuity "1"', {{$nedocs->ACUITY_1_PATIENTS}}],
                    ['Transfers', {{$nedocs->NUMBER_OF_TRANSFERS}}],
                    ['Discharges', {{$nedocs->NUMBER_OF_DISCHARGES}}]
                ]);
                chart.draw(data, options);
            }, 200);


        }


        $("#DateCountdown").TimeCircles({
            "animation": "smooth",
            "bg_width": 1.2,
            "fg_width": 0.1,
            "circle_bg_color": "#60686F",
            "time": {
                "Days": {
                    "text": "Days",
                    "color": "#FFCC66",
                    "show": true
                },
                "Hours": {
                    "text": "Hours",
                    "color": "#99CCFF",
                    "show": true
                },
                "Minutes": {
                    "text": "Minutes",
                    "color": "#BBFFBB",
                    "show": true
                },
                "Seconds": {
                    "text": "Seconds",
                    "color": "#FF9999",
                    "show": true
                }
            }
        });

    </script>

@endsection