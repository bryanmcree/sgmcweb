@extends('layouts.nonav')

@section('content')
<meta http-equiv="refresh" content="300">

<div class="col-md-8">
    <div class="panel panel-default">
        <div class="panel-body">

            <div class="col-md-12">
                <div class="mini-charts-item">
                    <small>SCORE: ({{$nedocs->NEDOCS_SCORE}})</small>
                    <div class="chartWithOverlay">
                        <div id="nedocsnow"></div>
                        <div class="overlay">
                            <div style="font-family:'Arial Black'; font-size: 128px; font-size: 4vw;">{{$nedocs->NEDOCS_SCORE}} <i class="fa fa-arrow-circle-up" aria-hidden="true"></i>{{$nedocsNowHi}} <span style="text-decoration: overline;">{{$nedocsNowavg}}</span></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="mini-charts-item">
                    <div id="gauges"></div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="mini-charts-item">
                    <div id="gauges2"></div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="mini-charts-item">
                    <div class="clearfix">
                        <div class="chart" id="longestwaitfortransfers"></div>
                        <div class="count">
                            <small>LONGEST WAIT FOR TRANSFERS</small>
                            <h2>{{date('H:i:s', mktime(0,$nedocs->LONGEST_WAIT_FOR_TRANSFER))}}</h2>
                            <h2><div style="text-decoration: overline;">{{date('H:i:s', mktime(0,$longestwaitfortransfers))}}</div></h2>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="mini-charts-item">
                    <div class="clearfix">
                        <div class="chart" id="avgdecisiontodisposition"></div>
                        <div class="count">
                            <small>AVG DECISION TO DISPOSITION</small>
                            <h2>{{date('H:i:s', mktime(0,$nedocs->AVG_DECISION_TO_DISP_TO_NOW))}}</h2>
                            <h2><div style="text-decoration: overline;">{{date('H:i:s', mktime(0,$avgdecisiontodisposition))}}</div></h2>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="mini-charts-item">
                    <div class="clearfix">
                        <div class="chart" id="longestlobbywaitforbed"></div>
                        <div class="count">
                            <small>LONGEST LOBBY WAIT FOR BED</small>
                            <h2>{{date('H:i:s', mktime(0,$nedocs->LONGEST_LOBBY_WAIT_FOR_BED))}}</h2>
                            <h2><div style="text-decoration: overline;">{{date('H:i:s', mktime(0,$longestlobbywaitforbed))}}</div></h2>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-6">
                <div class="mini-charts-item">
                    <div class="clearfix">
                        <div class="chart" id="longetsdischargetime"></div>
                        <div class="count">
                            <small>LONGEST DISCHARGE TIME</small>
                            <h2>{{date('H:i:s', mktime(0,$nedocs->LONGEST_DISCHARGE_TIME))}}</h2>
                            <h2><div style="text-decoration: overline;">{{date('H:i:s', mktime(0,$longetsdischargetime))}}</div></h2>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="mini-charts-item">
                    <div class="clearfix">
                        <div class="chart" id="longestadmittime"></div>
                        <div class="count">
                            <small>LONGEST ADMIT TIME</small>
                            <h2>{{date('H:i:s', mktime(0,$nedocs->LONGEST_ADMIT_TIME))}}</h2>
                            <h2><div style="text-decoration: overline;">{{date('H:i:s', mktime(0,$longestadmittime))}}</div></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-4">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="col-md-12">
                <div class="mini-charts-item">
                    <small>NEDOCS WEEKLY STATS</small>
                    <div class="chartWithOverlay">
                        <div id="nedocstrend"></div>
                        <div class="nedocstrend">
                            <div style="font-family:'Arial Black'; font-size: 20px;"><i class="fa fa-arrow-circle-up" aria-hidden="true"></i>{{$nedocsHi}}  <span style="text-decoration: overline;">{{$nedocsavg}}</span></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="mini-charts-item">
                    <small>Authority Commitments</small>
                    <table class="table table-condensed">
                        <thead>
                        <tr>
                            <td></td>
                            <td><b>Target</b></td>
                            <td><b>Baseline</b></td>
                            <td><b>May-17</b></td>
                            <td><b>June-17</b></td>
                        </tr>
                        </thead>

                        <tbody>
                        <tr>
                            <td><b>Admission orders to leave ED</b></td>
                            <td>45</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><b>Arrival to Triage</b></td>
                            <td>10</td>
                            <td>15</td>
                            <td>13</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><b>X-ray T/A Order to Prelim Report</b></td>
                            <td>60</td>
                            <td>65</td>
                            <td>64.7</td>
                            <td>63.4</td>
                        </tr>
                        <tr>
                            <td><b>CT w/o Oral Contrast   T/A Order to Prelim</b></td>
                            <td>90</td>
                            <td>96</td>
                            <td>58.6</td>
                            <td>59.3</td>
                        </tr>
                        <tr>
                            <td><b>CT w/ Oral Contrast   T/A Order to Prelim</b></td>
                            <td>120</td>
                            <td>147</td>
                            <td>119.5</td>
                            <td>118.7</td>
                        </tr>
                        <tr>
                            <td><b>Laboratory T/A Order to Complete</b></td>
                            <td>60</td>
                            <td>64</td>
                            <td>64</td>
                            <td>61</td>
                        </tr>
                        <tr>
                            <td><b>Hospitalist Admis Orders Paged to Entered</b></td>
                            <td>45</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>

            <div class="col-md-12">
                <div class="mini-charts-item">
                    <small>SGEP Commitments</small>
                    <table class="table table-condensed">
                        <thead>
                        <tr>
                            <td></td>
                            <td><b>Target</b></td>
                            <td><b>Baseline</b></td>
                            <td><b>May-17</b></td>
                            <td><b>June-17</b></td>
                        </tr>
                        </thead>

                        <tbody>
                        <tr>
                            <td><b>Bed to Physician orders</b></td>
                            <td>20 Min</td>
                            <td>25% +</td>
                            <td>21</td>
                            <td>21</td>
                        </tr>
                        <tr>
                            <td><b>Doctor Rating</b></td>
                            <td>75%</td>
                            <td>25% +</td>
                            <td>17</td>
                            <td>10</td>
                        </tr>
                        <tr>
                            <td><b>LOS Discharged patients (Median)</b></td>
                            <td>180 Min</td>
                            <td>25% +</td>
                            <td>256</td>
                            <td>236</td>
                        </tr>
                        <tr>
                            <td><b>Core Measures</b></td>
                            <td>99.90%</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><b>LBBS%</b></td>
                            <td>2%</td>
                            <td>25% -</td>
                            <td>8.30%</td>
                            <td>5.30%</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>



<div id="score" data-field-id="{{$nedocs->NEDOCS_SCORE}}" ></div>
<div id="longestwaitfortransfersPer" data-field-id="{{Round(($nedocs->LONGEST_WAIT_FOR_TRANSFER/$longestwaitfortransfers)*100,0)}}" ></div>
<div id="longestlobbywaitforbedPer" data-field-id="{{ROUND($nedocs->LONGEST_LOBBY_WAIT_FOR_BED/$longestlobbywaitforbed*100,0)}}" ></div>
<div id="avgdecisiontodispositionPer" data-field-id="{{ROUND($nedocs->AVG_DECISION_TO_DISP_TO_NOW/$avgdecisiontodisposition*100,0)}}" ></div>
<div id="longetsdischargetimePer" data-field-id="{{ROUND($nedocs->LONGEST_DISCHARGE_TIME/$longetsdischargetime*100,0)}}" ></div>
<div id="longestadmittimePer" data-field-id="{{ROUND($nedocs->LONGEST_ADMIT_TIME/$longestadmittime*100,0)}}" ></div>

@endsection

@section('scripts')

    <script type="application/javascript">
        var NedocsScore = $('#score').data("field-id");

        if(NedocsScore <= 50){
            //alert("Not Crowded");
            var NedocColor = '#00FF00';
            var NedocTitle = 'Not Crowded';
        }
        else if (NedocsScore >= 51 && NedocsScore <= 100){
            //alert("Busy");
            var NedocColor = '#7FFF00';
            var NedocTitle = 'Busy';
        }
        else if (NedocsScore >= 101 && NedocsScore <= 140){
            //alert("Overcrowded");
            var NedocColor = '#FFFF00';
            var NedocTitle = 'Overcrowded';
        }
        else if (NedocsScore >= 141 && NedocsScore <= 180){
            //alert("Severly Overcrowded");
            var NedocColor = '#FFFF00';
            var NedocTitle = 'Severly Overcrowded';
        }
        else if (NedocsScore >= 181){
            //alert("Dangersouly Overcrowded");
            var NedocColor = '#FF0000';
            var NedocTitle = 'Dangersouly Overcrowded';
        }
        //LINE CHart
        google.charts.load('current', {'packages':['corechart','gauge']});

        google.charts.setOnLoadCallback(drawemployeesclockedin);

        google.charts.setOnLoadCallback(drawnedocstrend);

        google.charts.setOnLoadCallback(drawnedocsnow);

        google.charts.setOnLoadCallback(drawemployeestatus);

        google.charts.setOnLoadCallback(drawnedocstrendBIG);

        google.charts.setOnLoadCallback(drawgauges);

        google.charts.setOnLoadCallback(drawgauges2);



        //SGMC Census
        var jsonData = {!! $results !!};
        function maincampus() {

            //var data = google.visualization.arrayToDataTable(jsonData);
            var data = new google.visualization.DataTable(jsonData);
            var options = {
                height:650,
                chartArea: {
                    width:'90%',
                    height:'80%'
                },
                legend:'top',
                title: 'SGMC Census',
                hAxis: {

                    gridlines: {
                        count: -1,
                        units: {
                            days: {format: ['MMM dd']},
                            hours: {format: ['HH:mm', 'ha']},
                        }
                    },
                    minorGridlines: {
                        units: {
                            hours: {format: ['hh:mm:ss a', 'ha']},
                            minutes: {format: ['HH:mm a Z', ':mm']}
                        }
                    }
                },
                vAxis: {minValue: 0},
            };

            var chart = new google.visualization.AreaChart(document.getElementById('MainCampus'));
            chart.draw(data, options);
        }

        function drawemployeesclockedin() {
            var jsonData = $.ajax({
                url: "/productivity/clockedin",
                dataType:"json",
                async: false
            }).responseText;
            //var data = google.visualization.arrayToDataTable(jsonData);
            var data = new google.visualization.DataTable(jsonData);
            var options = {
                curveType: 'function',
                animation:{
                    startup: true,
                    duration: 1000,
                    easing: 'out'
                },
                chartArea: {
                    width:'100%',
                    height:'100%'
                },
                backgroundColor: {
                    fill:'transparent'
                },
                'is3D':true,
                //title:'Employees Currently Clocked In',
                crosshair: { trigger: 'both', color: 'WHITE' },
                //height:650,
                legend: {
                    position:'none'
                },
                vAxis: {
                    //title: 'Number of Employees',
                    scaleType: 'mirrorLog',
                    gridlines: { count: 0 }
                },
                hAxis: { textPosition: 'none' },
                pointSize: 3,
                series: {
                    0: {
                        color: '#3193fc',
                        pointShape: 'diamond'
                    }
                }
            };
            var chart = new google.visualization.AreaChart(document.getElementById('employeesclockedin'));
            chart.draw(data, options);
        }

        function drawnedocstrendBIG() {
            var jsonData = $.ajax({
                url: "/json/nedocs",
                dataType:"json",
                async: false
            }).responseText;
            //var data = google.visualization.arrayToDataTable(jsonData);
            var data = new google.visualization.DataTable(jsonData);
            var options = {
                curveType: 'function',
                animation:{
                    startup: true,
                    duration: 1000,
                    easing: 'out'
                },
                chartArea: {
                    width:'100%',
                    height:'100%'
                },
                backgroundColor: {
                    fill:'transparent'
                },
                'is3D':true,
                //title:'Employees Currently Clocked In',
                //crosshair: { trigger: 'both', color: 'WHITE' },
                //height:650,
                legend: {
                    position:'none'
                },
                vAxis: {
                    //title: 'Number of Employees',
                    scaleType: 'mirrorLog',
                    gridlines: { count: 0 },
                    viewWindow:{
                        //max:100,
                        //min:0
                    }
                },
                hAxis: { textPosition: 'none',
                    gridlines: { count: 0 }},


                pointSize: 3,
                series: {
                    0: {
                        color: NedocColor,
                        //color: "white",
                        pointShape: 'diamond',
                        pointsVisible: true
                    }
                }
            };

            var chart = new google.visualization.AreaChart(document.getElementById('nedocstrendBIG'));
            chart.draw(data, options);
        }


        function drawnedocstrend() {
            var jsonData = $.ajax({
                url: "/json/nedocs",
                dataType:"json",
                async: false
            }).responseText;
            //var data = google.visualization.arrayToDataTable(jsonData);
            var data = new google.visualization.DataTable(jsonData);
            var options = {
                trendlines: {
                    0: {
                        visibleInLegend: false,
                        type: 'linear',
                        color: 'white',
                        lineWidth: 1,
                        //opacity: 0.1,
                        pointsVisible: false
                    }
                },
                curveType: 'function',
                animation:{
                    startup: true,
                    duration: 1000,
                    easing: 'out'
                },
                chartArea: {
                    width:'100%',
                    height:'100%'
                },
                backgroundColor: {
                    fill:'transparent'
                },
                'is3D':true,
                //title:'Employees Currently Clocked In',
                crosshair: { trigger: 'both', color: 'WHITE' },
                //height:650,
                legend: {
                    position:'none'
                },
                vAxis: {
                    //title: 'Number of Employees',
                    scaleType: 'mirrorLog',
                    gridlines: { count: 0 }
                },
                hAxis: { textPosition: 'none',
                    gridlines: { count: 0 }},


                pointSize: 3,
                series: {
                    0: {
                        color: 'purple',
                        pointShape: 'diamond',
                        pointsVisible: false
                    }
                }
            };

            var chart = new google.visualization.AreaChart(document.getElementById('nedocstrend'));
            chart.draw(data, options);
        }

        function drawnedocsnow() {
            var jsonData = $.ajax({
                url: "/json/nedocsnow",
                dataType:"json",
                async: false
            }).responseText;
            //var data = google.visualization.arrayToDataTable(jsonData);
            var data = new google.visualization.DataTable(jsonData);
            var options = {


                curveType: 'function',
                animation:{
                    startup: true,
                    duration: 1000,
                    easing: 'out'
                },
                chartArea: {
                    width:'100%',
                    height:'100%'
                },
                backgroundColor: {
                    fill:'transparent'
                },
                'is3D':true,
                //title:'Employees Currently Clocked In',
                //crosshair: { trigger: 'both', color: 'WHITE' },
                //height:650,
                legend: {
                    position:'none'
                },
                vAxis: {
                    //title: 'Number of Employees',
                    scaleType: 'mirrorLog',
                    gridlines: { count: 0 },
                    viewWindow:{
                        //max:100,
                        //min:0
                    }
                },
                hAxis: { textPosition: 'none',
                    gridlines: { count: 0 }},


                pointSize: 3,
                series: {
                    0: {
                        color: NedocColor,
                        //color: "white",
                        pointShape: 'diamond',
                        pointsVisible: true
                    }
                }
            };
            var chart = new google.visualization.AreaChart(document.getElementById('nedocsnow'));
            chart.draw(data, options);
        }

        //BAR CHART
        google.charts.load('current', {packages: ['corechart', 'bar']});
        google.charts.setOnLoadCallback(drawemployeestatus);

        function drawemployeestatus() {

            var jsonData = $.ajax({
                url: "/json/currentemployees",
                dataType:"json",
                async: false
            }).responseText;
            //var data = google.visualization.arrayToDataTable(jsonData);
            var data = new google.visualization.DataTable(jsonData);

            var options = {
                animation:{
                    startup: true,
                    duration: 1000,
                    easing: 'out'
                },
                //title: 'Population of Largest U.S. Cities',
                'width': '100%',
                //'height': 400,
                chartArea: {
                    //width:'100%',
                    height:'100%',
                    left:150
                },
                backgroundColor: {
                    fill:'transparent'
                },
                colors: ['#61c1d6'],
                is3D:true,
                hAxis: {
                    //title: 'Total Population',
                    minValue: 0,
                    gridlines: { count: 0 }
                },
                vAxis: {
                    //title: 'City'
                    textStyle:{color: '#cccccc'}
                },
                legend: {
                    position:'none'
                }
            };

            var chart = new google.visualization.BarChart(document.getElementById('employeestatus'));

            chart.draw(data, options);
        }


        google.charts.load('current', {'packages':['gauge']});
        function drawgauges() {

            var data = google.visualization.arrayToDataTable([
                ['Label', 'Value'],
                ['ED', 0],
                ['ED BED', 0],
                ['Lobby', 0]
            ]);

            var options = {
                textcolor:"white",
                height: 175,
                redFrom: 80, redTo: 100,
                yellowFrom:55, yellowTo: 80,
                minorTicks: 5,
                animation:{
                    duration: 1000,
                    easing: 'inAndOut',
                }

            };

            var chart = new google.visualization.Gauge(document.getElementById('gauges'));

            google.visualization.events.addListener(chart, 'ready', function () {
                $('#gauges circle:nth-child(2)').attr('fill', '#ccc');
            });

            chart.draw(data, options);
            //clearChart();


            // This is new.
            setTimeout(function(){
                var data = google.visualization.arrayToDataTable([
                    ['Label', 'Value'],
                    ['ED', {{$nedocs->TOTAL_PATIENTS_IN_ED}}],
                    ['ED BED', {{$nedocs->TOTAL_PATIENTS_IN_ER_BED}}],
                    ['Lobby', {{$nedocs->TOTAL_PATIENTS_IN_LOBBY}}]

                ]);
                chart.draw(data, options);
            }, 200);


        }


        function drawgauges2() {

            var data2 = google.visualization.arrayToDataTable([
                ['Label', 'Value'],
                ['Admits', 0],
                ['Acuity "1"', 0],
                ['Transfers', 0],
                ['Discharges', 0]
            ]);

            var options = {
                textcolor:"white",
                height: 175,
                redFrom: 80, redTo: 1000,
                yellowFrom:55, yellowTo: 80,
                minorTicks: 5,
                animation:{
                    duration: 1000,
                    easing: 'inAndOut',
                }

            };

            var chart = new google.visualization.Gauge(document.getElementById('gauges2'));

            google.visualization.events.addListener(chart, 'ready', function () {
                $('#gauges2 circle:nth-child(2)').attr('fill', '#ccc');
            });

            chart.draw(data2, options);
            //clearChart();


            // This is new.
            setTimeout(function(){
                var data2 = google.visualization.arrayToDataTable([
                    ['Label', 'Value'],
                    ['Admits', {{$nedocs24->NUMBER_OF_ADMITS}}],
                    ['Acuity "1"', {{$nedocs24->ACUITY_1_PATIENTS}}],
                    ['Transfers', {{$nedocs24->NUMBER_OF_TRANSFERS}}],
                    ['Discharges', {{$nedocs24->NUMBER_OF_DISCHARGES}}]
                ]);
                chart.draw(data2, options);
            }, 200);


        }

        var longestwaitfortransfersPer = $('#longestwaitfortransfersPer').data("field-id");

        var longestwaitfortransfersData = [
            {value:longestwaitfortransfersPer,color:"#288a6c"},
            {value:100-longestwaitfortransfersPer,color:"#ccc"}
        ];

        $( "#longestwaitfortransfers" ).doughnutit({
            dnData: longestwaitfortransfersData,
            dnSize: 130,
            dnInnerCutout: 70,
            dnAnimation: true,
            dnAnimationSteps: 60,
            dnAnimationEasing: 'linear',
            dnStroke: false,
            dnShowText: true,
            dnFontSize: '25px',
            dnFontOffset: 30,
            dnFontColor: "#288a6c",
            dnText: longestwaitfortransfersPer +'%',
            dnStartAngle: 90,
            dnCounterClockwise: false,

        });// longestwaitfortransfers


        var avgdecisiontodispositionPer = $('#avgdecisiontodispositionPer').data("field-id");

        var avgdecisiontodispositionData = [
            {value:avgdecisiontodispositionPer,color:"#FFCC66"},
            {value:100-avgdecisiontodispositionPer,color:"#ccc"}
        ];

        $( "#avgdecisiontodisposition" ).doughnutit({
            dnData: avgdecisiontodispositionData,
            dnSize: 130,
            dnInnerCutout: 70,
            dnAnimation: true,
            dnAnimationSteps: 60,
            dnAnimationEasing: 'linear',
            dnStroke: false,
            dnShowText: true,
            dnFontSize: '25px',
            dnFontOffset: 30,
            dnFontColor: "#FFCC66",
            dnText: avgdecisiontodispositionPer + '%',
            dnStartAngle: 90,
            dnCounterClockwise: false,

        });// avgdecisiontodisposition


        var longestlobbywaitforbedPer = $('#longestlobbywaitforbedPer').data("field-id");
        //alert($('#longestlobbywaitforbedPer').data("field-id"));
        var longestlobbywaitforbedData = [
            {value:longestlobbywaitforbedPer,color:"#BBFFBB"},
            {value:100-longestlobbywaitforbedPer,color:"#ccc"}
        ];

        $( "#longestlobbywaitforbed" ).doughnutit({
            dnData: longestlobbywaitforbedData,
            dnSize: 130,
            dnInnerCutout: 70,
            dnAnimation: true,
            dnAnimationSteps: 60,
            dnAnimationEasing: 'linear',
            dnStroke: false,
            dnShowText: true,
            dnFontSize: '25px',
            dnFontOffset: 30,
            dnFontColor: "#BBFFBB",
            dnText: longestlobbywaitforbedPer +'%',
            dnStartAngle: 90,
            dnCounterClockwise: false,

        });// longestlobbywaitforbed


        var longetsdischargetimePer = $('#longetsdischargetimePer').data("field-id");
        var longetsdischargetimeData = [
            {value:longetsdischargetimePer,color:"#99CCFF"},
            {value:100-longetsdischargetimePer,color:"#ccc"}
        ];

        $( "#longetsdischargetime" ).doughnutit({
            dnData: longetsdischargetimeData,
            dnSize: 130,
            dnInnerCutout: 70,
            dnAnimation: true,
            dnAnimationSteps: 60,
            dnAnimationEasing: 'linear',
            dnStroke: false,
            dnShowText: true,
            dnFontSize: '25px',
            dnFontOffset: 30,
            dnFontColor: "#99CCFF",
            dnText: longetsdischargetimePer +'%',
            dnStartAngle: 90,
            dnCounterClockwise: false,

        });// longetsdischargetime


        var longestadmittimeData = [
            {value:65,color:"#FF9999"},
            {value:100-65,color:"#ccc"}
        ];

        $( "#longestadmittime" ).doughnutit({
            dnData: longestadmittimeData,
            dnSize: 130,
            dnInnerCutout: 70,
            dnAnimation: true,
            dnAnimationSteps: 60,
            dnAnimationEasing: 'linear',
            dnStroke: false,
            dnShowText: true,
            dnFontSize: '25px',
            dnFontOffset: 30,
            dnFontColor: "#FF9999",
            dnText: '65%',
            dnStartAngle: 90,
            dnCounterClockwise: false,

        });// longestadmittime


    </script>

@endsection