<div class="progress skill-bar " id="progressbar">


@if($nedocs2->NEDOCS_SCORE <= 50)
<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{$nedocs2->NEDOCS_SCORE}}" aria-valuemin="0" aria-valuemax="300">
<span class="skill">Not Crowded  <b class="val">{{$nedocs2->NEDOCS_SCORE}}</b></span>
@elseif($nedocs2->NEDOCS_SCORE >= 51 || $nedocs2->NEDOCS_SCORE <= 100)
<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="{{$nedocs2->NEDOCS_SCORE}}" aria-valuemin="0" aria-valuemax="300">
<span class="skill">Busy  <b class="val">{{$nedocs2->NEDOCS_SCORE}}</b></span>
@elseif($nedocs2->NEDOCS_SCORE >= 101 || $nedocs2->NEDOCS_SCORE <= 140)
<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="{{$nedocs2->NEDOCS_SCORE}}" aria-valuemin="0" aria-valuemax="300">
<span class="skill">Busy  <b class="val">{{$nedocs2->NEDOCS_SCORE}}</b></span>
@elseif($nedocs2->NEDOCS_SCORE >= 141 || $nedocs2->NEDOCS_SCORE <= 180)
<div class="progress-bar progress-bar-warning progress-bar-striped active" role="progressbar" aria-valuenow="{{$nedocs2->NEDOCS_SCORE}}" aria-valuemin="0" aria-valuemax="300">
<span class="skill">Busy  <b class="val">{{$nedocs2->NEDOCS_SCORE}}</b></span>
@elseif($nedocs2->NEDOCS_SCORE >= 181)
<div class="progress-bar progress-bar-danger progress-bar-striped active" role="progressbar" aria-valuenow="{{$nedocs2->NEDOCS_SCORE}}" aria-valuemin="0" aria-valuemax="300">
<span class="skill">Busy  <b class="val" id="progress"></b></span>
@endif


</div>
</div>








{{$nedocs2->NEDOCS_SCORE}}