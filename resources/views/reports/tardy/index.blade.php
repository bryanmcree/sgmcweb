@extends('layouts.app')

@section('content')
    @if ( Auth::user()->hasRole('Reports - Tardy') == FALSE)
        @include('layouts.unauthorized')
        @Else
<div class="card-deck">
    <div class="card border-info col-6 mb-4">
        <div class="card-body">
            <div class="form-horizontal">
                {!! Form::open(array('url' => '/reports/tardy', 'method' => 'POST', 'files' => true,'class' => 'form_control')) !!}
                {!! Form::label('unit', 'Search for Cost Center:') !!}
                {!! Form::text('search', '', ['class' => 'form-control', 'id'=>'autocomplete','placeholder'=>'Search by number or description.','required']) !!}
                {!! Form::hidden('unit', '', ['class' => 'form-control','id'=>'unit','required']) !!}
                <BR>
                {!! Form::label('start_date', 'Start Date:') !!}
                {!! Form::input('date','start_date', '', ['class' => 'form-control','required']) !!}
                <BR>
                {!! Form::label('end_date', 'End Date:') !!}
                {!! Form::input('date','end_date', '', ['class' => 'form-control','required']) !!}
                <BR>
                <div class="RightLeft">
                    {!! Form::reset('Clear Form', array('class' => 'btn btn-default')) !!}
                    {!! Form::submit('Run Report', array('class' => 'btn btn-sgmc')) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <div class="card border-info mb-4 col-6">
        <div class="card-body">
            <div class="card-title">
                <b>Be sure to "click" the cost center you want from the drop down.  See video below.</b>
            </div>
            <video width="380" height="290" controls autoplay loop>
                <source src="{{URL::asset('video/tardy.mp4')}}" type="video/mp4">
                Your browser does not support the video tag.
            </video>
        </div>
    </div>
</div>

        @if($report == 'Nothing')
            <div class="col-md-12">
                <div class="alert alert-info" role="alert">No results.  Please search again.</div>
            </div>

        @else
            <div class="card border-info mb-3">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-condensed table-hover table-bordered table-striped" id="table">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th class="hidden-xs hidden-sm hidden-md">Employee #</th>
                                <th class="hidden-xs hidden-sm hidden-md">Title</th>
                                <th class="hidden-xs hidden-sm hidden-md">Schedule Date/Time</th>
                                <th>Clocked In Date/Time</th>
                                <th>*Difference</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($tardy as $tardies)
                                <tr>
                                    <td>{{$tardies->last_name}}, {{$tardies->first_name}}</td>
                                    <td class="hidden-xs hidden-sm hidden-md">{{$tardies->employeenumber}}</td>
                                    <td class="hidden-xs hidden-sm hidden-md">{{$tardies->title}}</td>
                                    <td class="hidden-xs hidden-sm hidden-md">{{$tardies->schedule}}</td>
                                    <td>{{$tardies->clock}}</td>
                                    <td>{{\Carbon\Carbon::createFromTimeStamp(strtotime($tardies->clock))->diffForHumans(\Carbon\Carbon::createFromTimeStamp(strtotime($tardies->schedule)))}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <i>* Large differences could be the results of missed punches.</i>
                </div>
                
            </div>
        @endif
    </div>

@endsection

@section('scripts')

    <script type="application/javascript">
        $('#autocomplete').autocomplete({
            serviceUrl: '/costcenter',
            dataType: 'json',
            type:'GET',
            width: 418,
            minChars:2,
            onSelect: function(suggestion) {
                //alert(suggestion.data);
                $("#unit").val(suggestion.data);
            }


        });
    </script>
@endif
@endsection