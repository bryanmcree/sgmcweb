@if ($newrequest->isEmpty())

    <div class="alert alert-success"><b>There are no pending request.</b></div>

@else
<div class="col-md-12">
    <table class="table table-condensed table-hover table-responsive">
        <thead>

        </thead>
        <tbody>
        <tr>
            <td colspan="5"><b>Total Number of Pending Request:</b> {{$newrequest->count()}}</td>
        </tr>
        @foreach ($newrequest as $newrequests)
            <tr @if($newrequests->request_priority =='Critical') class="bg-danger" @endif>
                <td><a href="/reports/request/termination/{{$newrequests->system_name}}" role="button" @if($newrequests->request_priority =='Critical') class="btn btn-danger btn-xs" @else class="btn btn-sgmc btn-xs" @endif ><span class="fa fa-spinner fa-pulse fa-fw" aria-hidden="true"></span> Respond</a></td>
                <td nowrap=""><b>{{$newrequests->created_at}}</b></td>
                <td>{{$newrequests->system_name}}</td>
                <td class="hidden-xs hidden-sm hidden-md">{{$newrequests->request_type}}</td>
                <TD align="right">{{$newrequests->request_priority}}</TD>
            </tr>
        @endforeach
        </tbody>

    </table>
</div>
@endif