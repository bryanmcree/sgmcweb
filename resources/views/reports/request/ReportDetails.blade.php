{{--New file Template--}}

{{--Add Security for this page below--}}


    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')
    {!! Form::open(array('action' => ['ReportNameController@detailSubmit'], 'class' => 'form_control')) !!}
    {!! Form::hidden('requestor_sup', $ManagerInfo->employee_number,['class'=>'id']) !!}
    {!! Form::hidden('managerMail', $ManagerInfo->mail,['class'=>'id']) !!}
    {!! Form::hidden('report_id', $ReportName->id,['class'=>'id']) !!}
    {!! Form::hidden('report_priority', '0',['class'=>'id']) !!}
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel panel-heading">
               <h2><b>{{$ReportName->report_name}}</b></h2>
            </div>
            <div class="panel-body">

                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <b>Report Requester</b>
                        </div>
                        <div class="panel-body">
                            {{$ReportName->requestedBy->first_name}} {{$ReportName->requestedBy->last_name}}<br>
                            {{$ReportName->requestedBy->title}}<br>
                            {{$ReportName->requestedBy->unit_code}} / {{$ReportName->requestedBy->unit_code_description}}
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <b>Requester Supervisor</b> <i><a href="#" data-toggle="modal" data-target=".AddManager">(Change)</a></i>
                        </div>
                        <div class="panel-body">
                            {{$ManagerInfo->first_name}} {{$ManagerInfo->last_name}}<br>
                            {{$ManagerInfo->title}}<br>
                            {{$ManagerInfo->unit_code}} / {{$ManagerInfo->unit_code_description}}
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <b>Requester Phone Number</b>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                {!! Form::label('requestor_phone', 'Area Code and Number') !!}
                                <div class="input-group">
                                    <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                    {!! Form::text('requestor_phone', null, ['class' => 'form-control', 'id'=>'requestor_phone','maxlength' => '10', 'required','Placeholder'=>'2293331145']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <b>Report Description</b>
                            <div class="panel-sub-title">
                                Describe the report, include where the data will come from and the format you want it in.
                                Also include the time frame you are looking to have this report completed and anyone who
                                in the past has helped you pull this report.
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="form-group">
                                {!! Form::textarea('report_desc', null, ['class' => 'form-control report_desc', 'id'=>'report_desc', 'rows' => 6]) !!}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <b>Supporting Documents</b> <span  class="btn-group fileupload pull-right" data-report_id={{$ReportName->id}}><a class="btn btn-primary pull-right" href="#" role="button"  >Add Documents</a></span>
                            <div class="panel-sub-title">
                                Attach examples of the report, or old reports you have.  You can also attach other documentation if necessary.
                            </div>
                        </div>

                        <div class="panel-body">
                            @if ($ReportName->reportDocs->isEmpty())
                            <div class="alert bg-info">You do not have any files attached.</div>
                            @else
                                <table class="documents table table-condensed table-hover">
                                    <thead>
                                    <tr>
                                        <td><B>Date Added</B></td>
                                        <td><B>Added By</B></td>
                                        <td><B>File Type</B></td>
                                        <td><B>File Size</B></td>
                                        <td><B>Description</B></td>
                                        <td></td>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach ($ReportName->reportDocs as $documents)
                                        <tr>
                                            <td>{{$documents->created_at}}</td>
                                            <td>{{$documents->addedBy->first_name}} {{$documents->addedBy->last_name}}</td>
                                            <td>{{$documents->file_extension}}</td>
                                            <td>{{$documents->file_size}}</td>
                                            <td>{{$documents->file_description}}</td>
                                            <td>
                                                <div class="dropdown" align="right">
                                                    <button class="btn btn-primary btn-xs dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">Options<span class="caret"></span></button>
                                                    <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="menu1">
                                                        <li role="presentation"><a role="menuitem" href="/report/request/documents/download/{{$documents->id}}"><i class="fa fa-download"></i> Download</a></li>
                                                        <li role="presentation" class="divider"></li>
                                                        <li role="presentation"><a role="menuitem" href="/report/request/documents/delete/{{$documents->id}}?report_id={{$documents->report_id}}"><i class="fa fa-trash" style="color:red;"></i> Remove Document</a></li>
                                                    </ul>
                                                </div>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <b>Submit Request for Approval</b>
                            <div class="panel-sub-title">
                                This request will be sent to your supervisor for approval then go to the Data Governance Committee (DGC)
                                for final approval.  As this process is new, it is not know how long it will take for the DGC to approve
                                an assign a report writer to complete this request.  All request will be reviewed in the order in which they
                                were received.  No request for reports will be accepted verbally or by email.  The status of this request
                                can be viewed in web.sgmc.org using the Reports Tab and clicking “Report Request.”  If this request is critical
                                and has to do with the safety of the patients or staff, please let your supervisor know as they will have the
                                ability to request the report be expedited by the Committee when they review this request for approval.
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary btn-block">Submit Request for Approval</button>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
    {!! Form::close() !!}
    @include('reports.request.modals.updateManager')
    @include('reports.request.modals.fileupload')
@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')
<script type="application/javascript">



    $(document).on("click", ".fileupload", function () {
        $('.report_id').val($(this).attr("data-report_id"));
        //alert($(this).attr("data-report_id"));
        $('#fileupload').modal('show');
    });

    //For modal to list all employees so user can select manager
    $(document).ready(function() {
        oTable = $('#users').DataTable(
            {
                "info":     false,
                "pageLength": 10,
                "lengthChange": false,
                "order": [],
                "columnDefs": [
                    { targets: 'no-sort', orderable: false }
                ],
                "dom": '<l<t>ip>'

            }
        );
        $('#myInputTextField').keyup(function(){
            oTable.search($(this).val()).draw() ;
        })

    });
</script>


@endsection
