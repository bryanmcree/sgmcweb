@extends('layouts.app')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel panel-heading-custom">
                <div class="panel-title">
                    <H2><B>Respond to Request Form</B></H2>
                </div>
                <div class="panel-sub-title">Complete to respond to a request.</div>
            </div>
            <div class="panel-body">
                {!! Form::open(array('action' => ['ReportRequestController@submitRequest'], 'class' => 'form_control')) !!}
                {!! Form::hidden('managername', Auth::user()->managername) !!}
                {!! Form::hidden('manageremail', Auth::user()->manageremail) !!}
                {!! Form::hidden('current_status', 'Pending Manager Approval') !!}


                <div><h3>A request has been made by <b>{{$reviewrequest->requester_name}}</b> for the following information.  They can be reached at {{$reviewrequest->requester_phone}}.</h3></div>


                <div class="well well-lg"><div><h4>{{$reviewrequest->request_requirements}}</h4></div></div>

                <div @if($reviewrequest->request_priority == 'Critical') class="alert bg-danger" @endif><h3>The priority of this request is: <B>{{$reviewrequest->request_priority}}</B></h3>
                    @if($reviewrequest->request_priority == 'Critical')
                        <div>{{$reviewrequest->critical_justification}}</div>
                    @endif
                </div>



                <div><h2>Response</h2></div>

                @include('reports.request.formmgr')
                <div class="RightLeft">
                    {{ Form::reset('Clear Form', ['class' => 'btn btn-default']) }}
                    {!! Form::submit('Submit Request', ['class'=>'btn btn-sgmc', 'id'=>'UpdateButton']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>



@endsection


@section('scripts')



@endsection