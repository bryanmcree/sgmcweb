<table align="center" border="1" cellpadding="0" cellspacing="0" width="800">
    <thead>
    <tr>
        <td bgcolor="#288a6c" align="center"><h2><div style="font-family: Arial; color: white;"><br>Report Request Approval</div></h2><br></td>
    </tr>
    </thead>

    <tbody>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td><div style="font-family: Arial">{{$report_name}}</div></td>
    </tr>
    <tr>
        <td><div style="font-family: Arial">The above report has been approved by your manager.</div></td>
    </tr>
    <tr>
        <td align="center"><br> [<a href="http://web.sgmc.org/report/request/detail/view/{{$id}}">Proceed to Web.SGMC.Org</a>]</td>
    </tr>
    </tbody>
</table>
