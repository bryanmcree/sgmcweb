<table align="center" border="1" cellpadding="0" cellspacing="0" width="800">
    <thead>
    <tr>
        <td bgcolor="#288a6c" align="center"><h2><div style="font-family: Arial; color: white;"><br>Report Request - Approval</div></h2><br></td>
    </tr>
    </thead>

    <tbody>
    <tr>
        <td><div style="font-family: Arial"><b>Priority:</b>{{$report_priority}}</div><br></td>
    </tr>
    @if($report_priority == 'Critical')
        <tr>
            <td bgcolor="yellow"><div style="font-family: Arial"><b>Critical Request:</b>{{$critical_justification}}</div><br></td>
        </tr>
    @endif
    <tr>
        <td><div style="font-family: Arial"><b>Report Requirements:</b><br>{{$report_requirements}}</div><br></td>
    </tr>
    <tr>
        <td><div style="font-family: Arial">Your report request has been approved by your manager.  If the request was Critical it now needs to be approved by 2 committee members
            before it will be routed to the correct division for creation.  If not critical it will be placed in the committee queue for approval.</div></td>
    </tr>
    <tr>
        <td align="center"><br> [<a href="http://web.sgmc.org/reports/request">Proceed to Web.SGMC.Org</a>]</td>
    </tr>
    </tbody>
</table>