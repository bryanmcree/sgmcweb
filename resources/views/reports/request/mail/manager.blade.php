<table align="center" border="1" cellpadding="0" cellspacing="0" width="800">
    <thead>
        <tr>
            <td bgcolor="#288a6c" align="center"><h2><div style="font-family: Arial; color: white;"><br>Report Request</div></h2><br></td>
        </tr>
    </thead>

    <tbody>
    <tr>
        <td><div style="font-family: Arial">A report request has been sent to you requiring your approval.  Please use the link below to review the request.</div></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td><div style="font-family: Arial">{{$report_desc}}</div></td>
    </tr>
    <tr>
        <td align="center"><br> [<a href="http://web.sgmc.org/report/request/detail/view/{{$report_id}}">Proceed to Web.SGMC.Org</a>]</td>
    </tr>
    </tbody>
</table>
