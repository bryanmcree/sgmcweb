<table align="center" border="1" cellpadding="0" cellspacing="0" width="800">
    <thead>
    <tr>
        <td bgcolor="#288a6c" align="center"><h2><div style="font-family: Arial; color: white;"><br>Report Request - DENIED</div></h2><br></td>
    </tr>
    </thead>

    <tbody>
    <tr>
        <td><div style="font-family: Arial"><b>Priority:</b>{{$report_priority}}</div><br></td>
    </tr>
    @if($report_priority == 'Critical')
        <tr>
            <td bgcolor="yellow"><div style="font-family: Arial"><b>Critical Request:</b>{{$critical_justification}}</div><br></td>
        </tr>
    @endif
    <tr>
        <td><div style="font-family: Arial"><b>Report Requirements:</b><br>{{$report_requirements}}</div><br></td>
    </tr>
    <tr>
        <td><div style="font-family: Arial">Your report request has been denied by your manager.  Please make modifications and resubmit.</div></td>
    </tr>
    <tr>
        <td align="center"><br> [<a href="http://web.sgmc.org/reports/request">Proceed to Web.SGMC.Org</a>]</td>
    </tr>
    </tbody>
</table>