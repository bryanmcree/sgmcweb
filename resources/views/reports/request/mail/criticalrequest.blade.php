<table align="center" border="1" cellpadding="6" cellspacing="0" width="800">
    <thead>
    <tr>
        <td bgcolor="#e00f0f" align="center"><h2><div style="font-family: Arial; color: white;"><br>Critical Report Request</div></h2><br></td>
    </tr>
    <tr>
        <td align="center"><div style="font-family: Arial">This request has been approved and designated <b>CRITICAL</b> by {{$approved_by}} on {{$approved_on}}</div></td>
    </tr>
    </thead>

    <tbody>
    <tr>
        <td bgcolor="#e00f0f"></td>
    </tr>
    <tr>
        <td><div style="font-family: Arial"><b>Report Name: </b><br>{{$report_name}}</div></td>
    </tr>
    <tr>
        <td><div style="font-family: Arial"><b>Origional Requestor: </b>{{$requested_by['name']}}<br> <b>Phone: </b>{{$report_detail['requestor_phone']}}<br> <b>Approved by: </b>{{$approved_by}}</div></td>
    </tr>
    <tr>
        <td><div style="font-family: Arial"><b>Report Details: </b><br>{{$report_detail['report_desc']}}</div></td>
    </tr>
    <tr>
        <td align="center"><br> [<a href="http://web.sgmc.org/report/request/detail/view/{{$id}}">Proceed to Web.SGMC.Org</a>]</td>
    </tr>
    </tbody>
</table>
