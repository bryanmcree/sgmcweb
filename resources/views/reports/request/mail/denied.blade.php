<table align="center" border="1" cellpadding="6" cellspacing="0" width="800">
    <thead>
    <tr>
        <td bgcolor="#E0BC0F" align="center"><h2><div style="font-family: Arial; color: white;"><br>Denied Report Request</div></h2><br></td>
    </tr>
    </thead>

    <tbody>
    <tr>
        <td bgcolor="#E0BC0F"></td>
    </tr>
    <tr>
        <td><div style="font-family: Arial"><b>Report Name: </b><br>{{$report_name}}</div></td>
    </tr>
    <tr>
        <td><div style="font-family: Arial">Login in to view more details about this request.</div></td>
    </tr>
    <tr>
        <td align="center"><br> [<a href="http://web.sgmc.org/report/request/detail/view/{{$id}}">Proceed to Web.SGMC.Org</a>]</td>
    </tr>
    </tbody>
</table>
