@extends('layouts.app')

@section('content')
    <div class="col-md-12">
        <div class="alert-danger alert"><b>NOTICE:</b>  This tool is for report / dashboard request only, NOT computer / application support.
            If you need a printer installed, access to Epic, help with Epic application (ASAP, Willow, Beaker, etc.) or other SGMC system please create a ticket in Sysaid.
            <a href="https://helpdesk.sgmc.org/Login.jsp" target="_blank" class="btn btn-danger btn-xs pull-right">Go to SysAid</a> </div>
    </div>

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#home">Dashboard</a></li>
                    <li><a data-toggle="tab" href="#menu1">My Request</a></li>
                    <li><a data-toggle="tab" href="#menu2">Request to be Approved</a></li>
                    <li><a href="#" @if(empty($ManagerInfo)) data-target=".NeedManager" @else data-target=".NewReport" @endif data-toggle="modal" >Begin New Request</a></li>
                    @if ( Auth::user()->hasRole('Report Request Admin'))
                    <li><a href="/reports/request/dg" target="_blank">Data Governance View</a></li>
                    <li><a data-toggle="tab" href="#menu3">All Report Request</a></li>
                    @endif
                </ul>

                <div class="tab-content">
                    <div id="home" class="tab-pane fade in active">
                        <h3>Dashboard</h3>
                        <p>Users will see widgets with the status of request as well as alerts.</p>

                        @if(is_null(\Auth::user()->manager_emp_number))
                            @include("updatemanager")
                        @endif

                        <div class="row">
                            <div class="col-md-2">
                                <div class="panel panel-default">
                                    <div class="panel-heading panel-heading-custom">
                                        <b>Total Request</b>
                                    </div>
                                    <div class="panel-body">
                                        <div style="font-size: 40px; font-weight: bold; text-align: center;">{{$reports->count()}}</div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="panel @if($managerview->count() == 0) panel-default @else panel-danger @endif">
                                    <div class="panel-heading @if($managerview->count() == 0) panel-heading-custom @endif">
                                        <b>Request to be Approved</b>
                                    </div>
                                    <div class="panel-body">
                                        <div style="font-size: 40px; font-weight: bold; text-align: center;">{{$managerview->count()}}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="panel panel-default">
                                    <div class="panel-heading panel-heading-custom">
                                        <b>Current Manager</b>
                                    </div>
                                    <div class="panel-body">
                                        <p style="text-align: center;">Manager that will approve all request.</p>
                                        <b><i>{{$ManagerInfo->name}}</i></b> <a href="/manager?redirect=ReportRequest" class="btn btn-primary btn-sm pull-right">Change</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="menu1" class="tab-pane fade">
                        <h3>My Request</h3>
                        <p>User will see their request, their status as well as make changes or provide additional info.</p>
                        <div class="col-md-10 col-lg-offset-1">
                            @if ($reports->isEmpty())
                                <div class="alert bg-info">You do not have any form request.</div>
                            @else
                                <table class="table table-hover table-condensed" id="myRequest">
                                    <thead>
                                    <tr>
                                        <td><b>Report Name</b></td>
                                        <td><b>Created Date</b></td>
                                        <td><b>Duration</b></td>
                                        <td><b>Submitted By</b></td>
                                        <td><b>Status</b></td>
                                        <td class="no-sort"></td>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach($reports as $report)
                                        <tr>
                                            <td>{{$report->report_name}}</td>
                                            <td>{{$report->created_at}}</td>
                                            <td>{{\Carbon\Carbon::createFromTimeStamp(strtotime($report->created_at))->diffForHumans()}}</td>
                                            <td>{{$report->requestedBy->first_name}} {{$report->requestedBy->last_name}}</td>
                                            <td>{{$report->report_status}}</td>
                                            <td><a href="/report/request/detail/view/{{$report->id}}" type="button" class="btn btn-xs btn-primary pull-right">View</a> </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @endif
                        </div>

                    </div>
                    <div id="menu2" class="tab-pane fade">
                        <h3>Approve Request</h3>
                        <p>Here manager users will see request provided by their employees.  They can view them, add to them and will assign a priority to them and either approve them or deny them.</p>
                        <div class="col-md-10 col-lg-offset-1">
                            @if ($managerview->isEmpty())
                                <div class="alert bg-info">You do not have request to approve.</div>
                            @else
                                <table class="table table-hover table-condensed" id="ApproveRequest">
                                    <thead>
                                    <tr>
                                        <td><b>Report Name</b></td>
                                        <td><b>Created Date</b></td>
                                        <td><b>Duration</b></td>
                                        <td><b>Submitted By</b></td>
                                        <td><b>Status</b></td>
                                        <td class="no-sort"></td>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach($managerview as $report)
                                        <tr>
                                            <td>{{$report->report_name}}</td>
                                            <td>{{$report->created_at}}</td>
                                            <td>{{\Carbon\Carbon::createFromTimeStamp(strtotime($report->created_at))->diffForHumans()}}</td>
                                            <td>{{$report->requestedBy->first_name}} {{$report->requestedBy->last_name}}</td>
                                            <td>{{$report->report_status}}</td>
                                            <td><a href="/report/request/detail/view/{{$report->id}}" type="button" class="btn btn-xs btn-primary pull-right">View</a> </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </div>

                    <div id="menu3" class="tab-pane fade">
                        <h3>All Report Request</h3>
                        <p>Admin can view all report request that have been submitted and their status.</p>
                        <div class="col-md-10 col-lg-offset-1">
                            @if ($reports->isEmpty())
                                <div class="alert bg-info">You do not have any form request.</div>
                            @else
                                <table class="table table-hover table-condensed" id="AllRequest">
                                    <thead>
                                    <tr>
                                        <td><b>Report Name</b></td>
                                        <td><b>Created Date</b></td>
                                        <td><b>Duration</b></td>
                                        <td><b>Submitted By</b></td>
                                        <td><b>Status</b></td>
                                        <td class="no-sort"></td>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach($Allreports as $report)
                                        <tr>
                                            <td>{{$report->report_name}}</td>
                                            <td>{{$report->created_at}}</td>
                                            <td>{{\Carbon\Carbon::createFromTimeStamp(strtotime($report->created_at))->diffForHumans()}}</td>
                                            <td>{{$report->requestedBy->first_name}} {{$report->requestedBy->last_name}}</td>
                                            <td>{{$report->report_status}}</td>
                                            <td><a href="/report/request/detail/view/{{$report->id}}" type="button" class="btn btn-xs btn-primary pull-right">View</a> </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @endif
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>




@include('reports.request.modals.NewReport')


@endsection


@section('scripts')

<script type="application/javascript">



    $(document).ready(function() {
        $('#myRequest').DataTable({
            "autoWidth": false,
            "columnDefs": [
                { targets: 'no-sort', orderable: false }
            ],
        });
    } );

    $(document).ready(function() {
        $('#ApproveRequest').DataTable({
            "autoWidth": false,
            "columnDefs": [
                { targets: 'no-sort', orderable: false }
            ],
        });
    } );

    $(document).ready(function() {
        $('#AllRequest').DataTable({
            "autoWidth": false,
            "columnDefs": [
                { targets: 'no-sort', orderable: false }
            ],
        });
    } );



</script>

@endsection