<div class="modal fade" id="ApproveRequest_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">
                <button type="button" class="close btn-sgmc" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-file-o" aria-hidden="true"></i> <b>Approve Report Request</b></h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/reports/request/approval/{{$ReportName->id}}">
                    {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <p>The importance will determine the processing speed of the request.  <b>CRITICAL</b> request will be expedited and should only be used
                                for reports <b><i>"immediately affecting patient safety, compliance, or financials greater than 100K".</i></b>  All other request are normal request.</p>
                                <div class="form-group">
                                    <label for="cost_center_id">Importance:</label>
                                    <select class="form-control" name="importance">
                                        <option value="Normal" selected>Normal</option>
                                        <option value="Critical">Critical</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="RightLeft">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-sgmc" value="Approve Request">
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>