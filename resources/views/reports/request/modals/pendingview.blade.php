<div class="modal fade PendingViewRequest" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><b>View Report Request</b></h4>
            </div>
            <div class="modal-body bg-default">
                <div class="row">
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading panel-heading-custom">
                                <b>Requested By</b>
                            </div>
                            <div class="panel-body">
                                <div class="requested_by"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading panel-heading-custom">
                                <b>Report Priority</b>
                            </div>
                            <div class="panel-body">
                                <div class="report_priority"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading panel-heading-custom">
                                <b>Requester Phone</b>
                            </div>
                            <div class="panel-body">
                                <div class="requester_phone_number"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="panel panel-default">
                            <div class="panel-heading panel-heading-custom">
                                <b>Current Status</b>
                            </div>
                            <div class="panel-body">
                                <div class="current_status"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading panel-heading-custom">
                                <b>Report Requirements</b>
                            </div>
                            <div class="panel-body">
                                <div class="report_requirements"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading panel-heading-custom">
                                <b>Critical Justification if Applicable</b>
                            </div>
                            <div class="panel-body">
                                <div class="critical_justification"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        This report is pending your review and approval.  Pay special attention to critical reports as they must be justified
                        and directly relate to patient or staff safety.
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="RightLeft">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                </div>
            </div>
        </div>
    </div>
</div>