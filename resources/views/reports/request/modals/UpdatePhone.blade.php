<div class="modal fade " id="UpdatePhone" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><b>Phone Number</b></h4>
            </div>
            <div class="modal-body">
                {!! Form::open(array('url'=>'/report/request/detail/update','method'=>'POST', 'files'=>true)) !!}
                {!! Form::hidden('id', '',['class'=>'id']) !!}
                {!! Form::hidden('report_id', '',['class'=>'report_id']) !!}
                <div class="form-group">
                    {!! Form::label('requestor_phone', 'Number including area code') !!}
                    <div class="input-group">
                        <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                        {!! Form::text('requestor_phone', null, ['class' => 'form-control requestor_phone', 'id'=>'requestor_phone','maxlength' => '10', 'required','Placeholder'=>'2293331145']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="RightLeft">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>