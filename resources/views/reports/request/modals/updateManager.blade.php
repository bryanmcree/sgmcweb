<div class="modal fade AddManager" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><b>Select your supervisor</b></h4>
            </div>
            <div class="modal-body bg-default">

                <div class="panel panel-default">
                    <div class="panel-body">
                        Please take a moment and select your supervisor by typing their name in the field below.  This person is where all request will go for approval.
                        You can change your supervisor by editing your account.  <BR>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="input-group">
                                    {!! Form::text('search', null,array('class'=>'form-control','id'=>'myInputTextField','placeholder'=>'Search for supervisor...', 'autofocus'=>'autofocus')) !!}
                                    <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search" aria-hidden="true"></i></span>
                                </div>
                            </div>
                        </div>
                        <BR>
                        <table class="table table-hover table-responsive table-bordered" id="users">
                            <thead>
                            <tr>
                                <td class="hidden-xs"><b>Name</b></td>
                                <td class="hidden-xs"><b>Title</b></td>
                                <td class='no-sort' align="center"></td>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach ($adusers as $user)

                                <tr>
                                    <td class="hidden-xs hidden-sm">{{$user->name}}</td>
                                    <td class="hidden-xs hidden-sm">{{$user->title}}</td>
                                    <td align="center" style="vertical-align:middle"><a href="/user/updatemanager/{{$user->employee_number}}?returnTo=1" role="button" class="btn btn-sgmc btn-xs SecurityRoles" data-user_id = {{$user->id}}><span class="fa fa-btn fa-user" aria-hidden="true"></span> Select</a></td>
                                </tr>

                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>