<div class="modal fade NewReport" id="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">
                <button type="button" class="close btn-sgmc" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-file-o" aria-hidden="true"></i> <b>New Report Request</b></h4>
            </div>
            <div class="modal-body">
                <p>What would you like to name the report.  Remember as reports added a unique name will make finding your report easier.</p>
                {!! Form::open(array('action' => ['ReportNameController@newReport'], 'class' => 'form_control')) !!}
                {!! Form::hidden('requested_by', Auth::user()->employee_number,['class'=>'id']) !!}
                {!! Form::hidden('report_status', 'Report Details Needed',['class'=>'id']) !!}
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="form-group">
                                    {!! Form::label('report_name', 'Report Name',['class'=>'ldac']) !!}
                                    {!! Form::text('report_name', null, ['class' => 'form-control report_name', 'id'=>'report_name', 'Required']) !!}
                                </div>
                                <div class="form-group">
                                    <select name="category" name="" class="form-control" required>
                                        <option selected value="">[Select Category]</option>
                                        <option value="">ASAP, Beaker, Inpatient, Stork</option>
                                        <option value="">OpTime, Anesthesia, Ambulatory</option>
                                        <option value="">Radiant, Cupid, HIM, Willow</option>
                                        <option value="">Cadence, PB</option>
                                        <option value="">Grand Central, Prelude, HB</option>
                                        <option value="">Non-Epic Report Request</option>
                                        <option value="">Other Epic Request (Quality, MU, etc.)</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="RightLeft">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    {!! Form::submit('Next', ['class'=>'btn btn-sgmc', 'id'=>'AddButton']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>