<div class="modal fade" id="DenyRequest_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">
                <button type="button" class="close btn-sgmc" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-file-o" aria-hidden="true"></i> <b>Deny Report Request</b></h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/reports/request/denial/{{$ReportName->id}}">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <p>Describe the reason for denying this report request.  The request will be returned to the requester to be updated.</p>
                                    <div class="form-group">
                                        <label for="notes_id">Reason:</label>
                                        <textarea class="form-control" name="notes" required></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <div class="RightLeft">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-sgmc" value="Deny Request">
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>