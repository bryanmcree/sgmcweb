<div class="modal fade " id="fileupload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Upload Documents</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(array('url'=>'report/request/documents/add','method'=>'POST', 'files'=>true)) !!}
                {!! Form::hidden('report_id', '',['class'=>'report_id']) !!}
                <div class="form-group">
                    {!! Form::label('file_description', 'Description:') !!}
                    <div class="input-group">
                        <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                        {!! Form::text('file_description', null, ['class' => 'form-control', 'id'=>'first_name','placeholder'=>'File Description', 'required']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('file', 'File:') !!}
                    <div class="input-group">
                        <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                        {!! Form::file('file_name', array('class'=>'form-control')) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="RightLeft">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Upload</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>