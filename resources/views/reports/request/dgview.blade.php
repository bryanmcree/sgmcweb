@extends('layouts.sgmc_nonav')

@section('content')

<br>

        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading panel-heading-custom">
                    <div class="panel-title">
                        <H3><B>Report Request Form</B></H3>
                    </div>
                    <div class="panel-sub-title">Complete to request custom report.</div>
                </div>
                <div class="panel-body">

                    @if ($reports->isEmpty())
                        <a href="#" class="btn btn-xs btn-default">DG View</a> <div class="alert bg-info">You do not have any form request. <a href="#" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target=".NewReport">Start New Request</a></div>
                    @else
                        <a href="#" class="btn btn-xs btn-default">DG View</a> <a href="#" type="button" class="btn btn-primary btn-xs pull-right "  data-toggle="modal" data-target=".NewReport">Start New Request</a>
                        <table class="table table-hover table-condensed">
                            <thead>
                            <tr>
                                <td><b>Report Name</b></td>
                                <td><b>Created Date</b></td>
                                <td></td>
                                <td><b>Submitted By</b></td>
                                <td><b>Status</b></td>
                                <td></td>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($reports as $report)
                                <tr>
                                    <td>{{$report->report_name}}</td>
                                    <td>{{$report->created_at}}</td>
                                    <td>{{\Carbon\Carbon::createFromTimeStamp(strtotime($report->created_at))->diffForHumans()}}</td>
                                    <td>{{$report->requestedBy->first_name}} {{$report->requestedBy->last_name}}</td>
                                    <td>{{$report->report_status}}</td>
                                    <td><a href="/report/request/detail/view/{{$report->id}}" type="button" class="btn btn-xs btn-primary pull-right">View</a> </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>



@endsection


@section('scripts')

    <script type="application/javascript">

        $(document).ready(function() {
            oTable = $('#users').DataTable(
                {
                    "info":     false,
                    "pageLength": 15,
                    "lengthChange": false,
                    "order": [],
                    "columnDefs": [
                        { targets: 'no-sort', orderable: false }
                    ],
                    "dom": '<l<t>ip>'

                }
            );
            $('#myInputTextField').keyup(function(){
                oTable.search($(this).val()).draw() ;
            })

        });


        $(document).on("click", ".ViewReportRequest", function () {
            //alert($(this).attr("data-requester_email"));

            $('.id').val($(this).attr("data-id"));
            $('.report_requirements').html($(this).attr("data-report_requirements"));
            $('.report_priority').html($(this).attr("data-report_priority"));
            $('.requester_phone_number').html($(this).attr("data-requester_phone_number"));
            $('.critical_justification').html($(this).attr("data-critical_justification"));
            $('.requested_by').html($(this).attr("data-requested_by"));
            $('.requester_email').val($(this).attr("data-requester_email"));

            $('.ViewRequest').modal('show');
            //alert($(this).attr("data-first_name"));
        });

        $(document).on("click", ".ViewPendingReportRequest", function () {
            //alert($(this).attr("data-requester_email"));

            $('.id').val($(this).attr("data-id"));
            $('.report_requirements').html($(this).attr("data-report_requirements"));
            $('.report_priority').html($(this).attr("data-report_priority"));
            $('.requester_phone_number').html($(this).attr("data-requester_phone_number"));
            $('.critical_justification').html($(this).attr("data-critical_justification"));
            $('.requested_by').html($(this).attr("data-requested_by"));
            $('.requester_email').val($(this).attr("data-requester_email"));
            $('.current_status').html($(this).attr("data-current_status"));

            $('.PendingViewRequest').modal('show');
            //alert($(this).attr("data-first_name"));
        });


    </script>

@endsection