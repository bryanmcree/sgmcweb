@extends('layouts.app')

@section('content')

    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading-custom panel-heading">
                <div class="panel-title">Request</div>
            </div>
            <div class="panel-body" style="height: calc(100vh - 140px); overflow-y: auto;">
                    <div class="" id="RequestFeed"></div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading-custom panel-heading">
                <div class="panel-title">Employees to be removed ({{$terminations->count()}}) - {{$id}}</div>
            </div>
            <div class="panel-body" style="height: calc(100vh - 195px); overflow-y: auto;">
                {!! Form::open(array('action' => ['ReportRequestController@clearTerminated'], 'class' => 'form_control')) !!}
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <td></td>
                            <td></td>
                        </tr>
                    </thead>

                    @foreach ($terminations as $termination)
                    <tr>
                        <td style="vertical-align:middle"><h4><b>{{$termination->term_name}}</b></h4>
                            Emp#: {{$termination->term_code}}<br>
                            {{$termination->term_title}}<br>
                            {{$termination->term_costcenter}} / {{$termination->term_costcenter_desc}}<br>
                            Reason: {{$termination->request_type}}<br>
                        </td>
                        <td style="vertical-align:middle;">
                            <div class="funkyradio btn-group">
                                <div class="funkyradio-primary">
                                    <input type="radio" name="{{ $termination->id }}" id="first_{{ $termination->id }}" value="System Checked, Employee NOT in system." class="required" />
                                    <label for="first_{{ $termination->id }}">System Checked, Employee NOT in {{$termination->system_name}}.&nbsp;&nbsp;</label>
                                </div>
                                <div class="funkyradio-success">
                                    <input type="radio" name="{{ $termination->id }}" id="second_{{ $termination->id }}" value="Employee Found and REMOVED from system." class="required"/>
                                    <label for="second_{{ $termination->id }}">Employee Found and REMOVED from {{$termination->system_name}}.&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                </div>
                            </div>
                        </td>
                    </tr>
                        <script>
                            var allRadios = document.getElementsByName('{{ $termination->id }}');
                            var booRadio;
                            var x = 0;
                            for(x = 0; x < allRadios.length; x++){

                                allRadios[x].onclick = function(){

                                    if(booRadio == this){
                                        this.checked = false;
                                        booRadio = null;
                                    }else{
                                        booRadio = this;
                                    }
                                };

                            }
                        </script>
                    @endforeach

                </table>

            </div>
            <div class="panel-footer">
                <div class="RightLeft">
                    {!! Form::submit('Submit Conformation', ['class'=>'btn btn-sgmc', 'id'=>'UpdateButton']) !!}
                    {{ Form::reset('Clear form', ['class' => 'form-button']) }}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection

@section('scripts')
<script type="application/javascript">

    $(document).ready(function() {
        $("#RequestFeed").load("/reports/request/feed");
        var refreshId = setInterval(function() {
            $("#RequestFeed").load('/reports/request/feed');
        }, 9000);
        $.ajaxSetup({ cache: false });
    });

</script>



@endsection