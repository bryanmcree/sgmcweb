
@if ( Auth::user()->hasRole('Terminations') == FALSE)
    @include('layouts.unauthorized')
    @Else

@extends('layouts.app')
@section('content')
<div class="col-md-3">
    <div class="panel panel-default">
        <div class="panel-heading-custom">
            <div class="panel-title">Date Range</div>
            <div class="panel-sub-title"><i>Select date range for charts</i></div>
        </div>
        <div class="panel-body">
            <div class="form-horizontal">
                {!! Form::open(array('url' => '/phs/export', 'method' => 'POST', 'files' => true,'class' => 'form_control')) !!}
                {!! Form::label('start_date', 'Start Date:') !!}
                {!! Form::input('date','start_date', '', ['class' => 'form-control','required']) !!}
                <BR>
                {!! Form::label('end_date', 'End Date:') !!}
                {!! Form::input('date','end_date', '', ['class' => 'form-control','required']) !!}
                <BR>
                <div class="RightLeft">
                    {!! Form::reset('Clear Form', array('class' => 'btn btn-default')) !!}
                    {!! Form::submit('Download File', array('class' => 'btn btn-sgmc')) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        <div class="panel-footer">Footer</div>
    </div>
</div>

<div class="col-md-3">
    <div class="panel panel-default">
        <div class="panel-heading-custom">
            <div class="panel-title">Total Terminations</div>
            <div class="panel-sub-title">Total number of records in termination tracker</div>
        </div>
        <div class="panel-body">
            {{$terms->count()}}
        </div>
        <div class="panel-footer"><i></i></div>
    </div>
</div>

<div class="col-md-3">
    <div class="panel panel-default">
        <div class="panel-heading-custom">
            <div class="panel-title">Date Range</div>
            <div class="panel-sub-title">Test Sub Title</div>
        </div>
        <div class="panel-body">
            BODY
        </div>
        <div class="panel-footer">Footer</div>
    </div>
</div>

<div class="col-md-3">
    <div class="panel panel-default">
        <div class="panel-heading-custom">
            <div class="panel-title">Date Range</div>
            <div class="panel-sub-title">Test Sub Title</div>
        </div>
        <div class="panel-body">
            BODY
        </div>
        <div class="panel-footer">Footer</div>
    </div>
</div>


<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading-custom">
            <div class="panel-title">Raw Data ({{$terms->count()}})</div>
            <div class="panel-sub-title">Data details used to create charts.</div>
        </div>
        <div class="panel-body">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <td><B>Emp#</b></td>
                    <td><B>Name</b></td>
                    <td><B>DOB</b></td>
                    <td><B>Hire Date</b></td>
                    <td><B>Termination Date</b></td>
                    <td><B>Cost Center</b></td>
                    <td><B>Title</b></td>
                    <td><B>Reason</b></td>
                    <td><B>Race</b></td>
                    <td><B>Gender</b></td>
                    <td><B>Shift</b></td>
                    <td><B>RN</b></td>
                    <td><B>Direct Care</b></td>
                </tr>
                </thead>

                <tbody>
                @foreach ($terms as $term)
                <tr>
                    <td>{{$term->emp_number}}</td>
                    <td>{{$term->last_name}}, {{$term->first_name}}</td>
                    <td>{{ date('m-d-Y', strtotime($term->birthdate)) }}</td>
                    <td>{{ date('m-d-Y', strtotime($term->hire_date)) }}</td>
                    <td>{{ date('m-d-Y', strtotime($term->termination_date)) }}</td>
                    <td>{{$term->cost_center}} / {{$term->cost_center_description}}</td>
                    <td>{{$term->title}}</td>
                    <td>{{$term->termination_reason_description}}</td>
                    <td>{{$term->race}}</td>
                    <td>{{$term->gender}}</td>
                    <td>{{$term->shift}}</td>
                    <td>{{$term->RN}}</td>
                    <td>{{$term->direct_care}}</td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="panel-footer"></div>
    </div>
</div>



@endsection
@section('scripts')



@endsection
@endif