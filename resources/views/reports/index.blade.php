@extends('layouts.app')

@section('content')
    @if ( Auth::user()->hasRole('Reports') == FALSE)
        @include('layouts.unauthorized')
        @Else
        <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel panel-heading-custom">
                <div class="panel-title">
                    <h2><b>Reports</b></h2>
                </div>
            </div>
            <div class="panel-body">
                <div class="col-md-8 col-md-offset-2">

                    <table class="reports table table-condensed table-hover" id="reports">
                        <thead>
                        <tr>
                            <th class="">Report</th>
                            <th class="hidden-xs hidden-sm hidden-md no-sort">Description</th>
                            <th class="hidden-xs hidden-sm hidden-md no-sort" align="right"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="hidden-xs hidden-sm hidden-md"><b>Productivity</b></td>
                            <td class="hidden-xs hidden-sm hidden-md">Productivity charts and reports</td>
                            <td class="" align="right"><a href="{{ URL::to('/productivity') }}" class="btn btn-sgmc btn-sm btn-block"><i class="fa fa-play"></i> Run Productivity Report</a></td></td>
                        </tr>
                            <tr>
                                <td class="hidden-xs hidden-sm hidden-md"><b>Tardy</b></td>
                                <td class="hidden-xs hidden-sm hidden-md">Pulls all tardy time clock transactions by cost center and date range</td>
                                <td class="" align="right"><a href="{{ URL::to('reports/tardy') }}" class="btn btn-sgmc btn-sm btn-block"><i class="fa fa-play"></i> Run Tardy Report</a></td>
                            </tr>
                        <tr>
                            <td class="hidden-xs hidden-sm hidden-md"><b>Dashboard</b></td>
                            <td class="hidden-xs hidden-sm hidden-md">Hospital dashboard</td>
                            <td class="" align="right"><a href="{{ URL::to('reports/countdown') }}" target="_blank" class="btn btn-sgmc btn-sm btn-block"><i class="fa fa-play"></i> Run Hospital Dashboard</a></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endif
@endsection

@section('scripts')

    <script type="text/javascript">

        $(document).ready(function() {
            oTable = $('#reports').DataTable(
                    {
                        "info":     true,
                        "pageLength": 20,
                        "lengthChange": false,
                        "order": [],
                        "columnDefs": [
                            { targets: 'no-sort', orderable: false }
                        ],


                    }
            );
            $('#search').keyup(function(){
                oTable.search($(this).val()).draw() ;
            })


        });
    </script>

@endsection