@extends('layouts.app')

<!-- Main Content -->
@section('content')
    <!-- Page Security -->
    @if ( Auth::user()->hasRole('Reports') == FALSE)
        @include('layouts.unauthorized')
        @Else
        <!-- Page Content -->

        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel panel-heading-custom">
                    <div class="panel-title">
                        <b>Select Report</b>
                    </div>
                </div>
                <div class="panel-body">
                    {!! Form::open(array('url' => '/reports/history', 'method' => 'POST', 'files' => true,'class' => 'form_control')) !!}
                    {!! Form::hidden('RunReport', 'RunReport', ['class' => 'form-control']) !!}
                    <div class="form-group">
                        {!! Form::label('action', 'Select Action:') !!}
                        <div class="input-group">
                            <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                            <select name="action" class="form-control">
                            <option value="">All Actions</option>
                            @foreach($dropdown_action as $action)

                                <option value="{{ $action->action }}"> {{$action->action}}</option>

                            @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('user', 'Select User:') !!}
                        <div class="input-group">
                            <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                            <select name="users" class="form-control">
                                <option value="">All Users</option>
                                @foreach($dropdown_user as $users)

                                    <option value="{{ $users->userid }}"> {{$users->userid}}</option>

                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="RightLeft">
                        {!! Form::reset('Clear Form', array('class' => 'btn btn-default')) !!}
                        {!! Form::submit('Run Report', array('class' => 'btn btn-sgmc')) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        @if($report == 'Nothing')
            <div class="col-md-9">
                <div class="alert alert-info" role="alert">No results.  Please search again.</div>
            </div>

        @else
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table class="table table-condensed table-hover table-responsive table-bordered table-striped" id="table">
                            <thead>
                            <tr>
                                <th>Action</th>
                                <th class="hidden-xs hidden-sm hidden-md">User</th>
                                <th class="hidden-xs hidden-sm hidden-md">Search String</th>
                                <th class="hidden-xs hidden-sm hidden-md">Date / Time</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($history as $logs)
                                <tr>
                                    <td>{{$logs->action}}</td>
                                    <td class="hidden-xs hidden-sm hidden-md">{{$logs->userid}}</td>
                                    <td class="hidden-xs hidden-sm hidden-md">{{$logs->search_string}}</td>
                                    <td class="hidden-xs hidden-sm hidden-md">{{$logs->created_at}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endif


    @endif
@endsection


<!-- Script Content -->
@section('scripts')

    <script type="text/javascript">


    </script>
@endsection