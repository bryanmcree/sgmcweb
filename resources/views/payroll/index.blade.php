@extends('layouts.app')

@section('content')
    @if ( Auth::user()->hasRole('Reports - Payroll') == FALSE)
        @include('layouts.unauthorized')
        @Else
    <div class="col-md-2">
    <div class="panel panel-success">
        <div class="panel-heading">Payroll Report</div>
        <div class="panel-body">
            <p>Pay period End Date NOT check date</p>
            {!! Form::open(array('action' => ['PayrollController@payrollReport'], 'class' => 'form_control')) !!}

                <div class="form-group">
            {!!Form::label('start', 'Enter Pay Date:') !!}
            {!!Form::input('date', 'start_date', null, ['class' => 'form-control', 'required']) !!}
                </div>
            {!! Form::submit('Generate Report', ['class'=>'btn btn-success btn-block']) !!}
            {!! Form::close() !!}

        </div>
    </div>
    </div>
    @endif
@endsection