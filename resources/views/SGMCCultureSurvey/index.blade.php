
@extends('layouts.no_login')

@section('content')
<div class="card mb-4 text-white bg-dark col-6">
    <div class="card-header">
        <h2><b>SGMC Culture of Safety - Followup Survey</b></h2>
    </div>
    <div class="card-body">
        <form method="post" action="/SGMCCultureSurvey/submit">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="exampleFormControlSelect1">Cost Center (Optional)</label>
                <select class="form-control" name="cost_center" id="exampleFormControlSelect1">
                    <option>[Optional]</option>
                    @foreach($costcenters as $costcenter)
                        <option value="{{$costcenter->style1}}">{{$costcenter->style1}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Name (Optional)</label>
                <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Optional" name="submitter_name">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Handoffs and Transitions (REQUIRED)</label>
                <textarea class="form-control" id="exampleFormControlTextarea1" name="handoffs" rows="3" required></textarea>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Non-punitive Response to Error (REQUIRED)</label>
                <textarea class="form-control" id="exampleFormControlTextarea1" name="response_error" rows="3" required></textarea>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Shift Changes (REQUIRED)</label>
                <textarea class="form-control" id="exampleFormControlTextarea1" name="shift_changes" rows="3" required></textarea>
            </div>
            <input type="submit" value="Submit Feedback" class="btn btn-success btn-block">
        </form>
    </div>
</div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
