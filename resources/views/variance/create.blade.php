{{--New file Template--}}

{{--Add Security for this page below--}}
{{-- @if( Auth::user()->hasRole('') == FALSE)
    @include('layouts.unauthorized')
@Else --}}

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

{{-- 2 --}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

@section('content')


    <div class="card mb-4 text-white bg-dark">
        <div class="card-header">
            Productivity Index Variance Explanations
            @if( Auth::user()->hasRole('PI_Variance_Admin') == TRUE) <span class="float-right"><a href="/variance" class="btn btn-primary btn-sm">Return To Dashboard</a></span> @endif
        </div>

        <form action="/variance/store" method="POST">

            <div class="card-body">
            
                {{ csrf_field() }}

                <table class="table table-dark table-hover table-bordered">
                    <thead>
                        <th style="width:40%;">Description</th>
                        <th>Data</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Cost Center # and Name:</td>
                            <td> 
                                <select name="cost_center" class="form-control basic" id="cc_dropdown" required>
                                    <option value="">[Choose Your Cost Center]</option>
                                    @foreach($costcenters as $cc)
                                        <option value="{{$cc->cost_center}}">{{$cc->cost_center}}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Pay Period End Date</td>
                            <td> 
                                <input type="date" class="form-control" name="pay_end" id="pp_dropdown" required> 
                            </td>
                        </tr>
                        <tr>
                            <td>Was this Variance Approved?</td>
                            <td> 
                                <select name="approved" class="form-control" required>
                                    <option value="">[Choose Answer]</option>
                                    <option value="0">No</option>
                                    <option value="1">Yes</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Date Range of Approval [Start]</td>
                            <td> 
                                <input type="date" name="start_approval" class="form-control">
                                <small class="form-text text-muted">Start Date</small>
                            </td>
                        </tr>
                        <tr>
                            <td>Date Range of Approval [End]</td>
                            <td> 
                                <input type="date" name="end_approval" class="form-control">
                                <small class="form-text text-muted">End Date</small>    
                            </td>
                        </tr>
                    </tbody>
                </table>

                <br>
                <h4>PI Variance</h4>
                <hr>

                <table class="table table-dark table-bordered table-hover">
                    <thead>
                        <th class="text-center" style="width:40%;">Category</th>
                        <th>Current Pay Period</th>
                        <th>Previous Pay Period</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text-center">Worked Hours Per UOS</td>
                            <td><input type="number" step=any class="form-control" name="curr_paid_UOS" id="curr_uos" required></td>
                            <td><input type="number" step=any class="form-control" name="prev_paid_UOS" id="prev_uos" required></td>
                        </tr>
                        <tr>
                            <td class="text-center">Worked Target</td>
                            <td><input type="number" step=any class="form-control" name="curr_paid_target" id="curr_target" required></td>
                            <td><input type="number" step=any class="form-control" name="prev_paid_target" id="prev_target" required></td>
                        </tr>
                        <tr>
                            <td class="text-center">Worked Productivity Index</td>
                            <td><input type="number" step=any class="form-control" name="curr_paid_index" id="curr_index" required></td>
                            <td><input type="number" step=any class="form-control" name="prev_paid_index" id="prev_index" required></td>
                        </tr>
                        <tr>
                            <td class="text-center">Worked FTE Variance</td>
                            <td><input type="number" step=any class="form-control" name="curr_paid_FTE_var" id="curr_fte_var" required></td>
                            <td><input type="number" step=any class="form-control" name="prev_paid_FTE_var" id="prev_fte_var" required></td>
                        </tr>
                        <tr>
                            <td class="text-center">Rolling 6 Pay Period FTE Variance</td>
                            <td><input type="number" step=any class="form-control" name="six_period_FTE_var" id="rolling6" required></td>
                            <td class="text-center text-muted">Not Applicable</td>
                        </tr>
                        <tr>
                            <td class="text-center">Rolling 12 Month FTE Variance</td>
                            <td><input type="number" step=any class="form-control" name="twelve_month_FTE_var" id="rolling12" required></td>
                            <td class="text-center text-muted">Not Applicable</td>
                        </tr>
                    </tbody>
                </table>

                <br>
                <h4>Overtime Variance</h4>
                <hr>

                <table class="table table-dark table-bordered table-hover">
                    <thead>
                        <th class="text-center" style="width:40%;">Category</th>
                        <th>Current Pay Period</th>
                        <th>Previous Pay Period</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text-center">Current Pay Period Overtime %</td>
                            <td><input type="number" step=any class="form-control" name="current_period_OT_percent" required></td>
                            <td><input type="number" step=any class="form-control" name="previous_period_OT_percent" required></td>
                        </tr>
                        <tr>
                            <td class="text-center">Rolling 6 Pay Period Overtime %</td>
                            <td><input type="number" step=any class="form-control" name="six_period_OT_percent" required></td>
                            <td class="text-center text-muted">Not Applicable</td>
                        </tr>
                        <tr>
                            <td class="text-center">Rolling 12 Month Overtime %</td>
                            <td><input type="number" step=any class="form-control" name="twelve_month_OT_percent" required></td>
                            <td class="text-center text-muted">Not Applicable</td>
                        </tr>
                    </tbody>
                </table>

                <br>
                <h4>Reasons for Variance from Budget</h4>
                <hr>

                <textarea name="reasons_for_variance" rows="5" class="form-control" placeholder="Ex: High Acuity Patients, Education/Training, Staff/Department Restructuring, Call off, etc."></textarea>

                <br>
                <h4 class="mt-3">Action Plan</h4>
                <hr>

                <table class="table table-dark table-bordered table-hover">
                    <thead>
                        <th style="width:40%;">Dates of Variance Starting with Most Recent</th>
                        <th>Variance Correction Plan</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td><input type="date" class="form-control" name="recent1_var_date"></td>
                            <td><input type="text" class="form-control" name="recent1_correction"></td>
                        </tr>
                        <tr>
                            <td><input type="date" class="form-control" name="recent2_var_date"></td>
                            <td><input type="text" class="form-control" name="recent2_correction"></td>
                        </tr>
                        <tr>
                            <td><input type="date" class="form-control" name="recent3_var_date"></td>
                            <td><input type="text" class="form-control" name="recent3_correction"></td>
                        </tr>
                    </tbody>
                </table>

                <br>

                <table class="table table-dark table-hover table-bordered">
                    <thead>
                        <th>Aniticipated End Of Variance Date</th>
                        <th>Signature</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td> <input type="date" name="anticipated_end_var" class="form-control"> </td>
                            <td> <input type="text" class="form-control" name="auth_user" value="{{Auth::user()->name}}" readonly> </td>
                        </tr>
                    </tbody>
                </table>
                

            </div>

            <div class="card-footer">
                <input type="submit" class="btn btn-primary btn-block">
            </div>

        </form>

    </div>


@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script type="text/javascript">

    $(".basic").select2({
        allowClear: false
    });

            
    // Grabs value of cc_dropdown and sets other fields based on dropdown choice
    $(function() {

        var auto = <?= json_encode($auto); ?>;
        
        $('#cc_dropdown').on('change', function(){

            var cc = document.getElementById("cc_dropdown").value;

            for(var key in auto)
            {
                // console.log(auto[key].pay_period); console.log(cc); console.log(pp);
                // if(auto[key].pay_period >= pp)
                // {
                //     console.log('working');
                // }

                if(auto[key].cost_center == cc)
                {
                    // $('#curr_uos').val(auto[key].worked_hours_uos)
                    // $('#prev_uos').val(auto[key].pp_worked_hours_uos)
                    // $('#curr_target').val(auto[key].worked_target)
                    // $('#prev_target').val(auto[key].pp_worked_target)
                    // $('#curr_index').val(auto[key].worked_pi)
                    // $('#prev_index').val(auto[key].pp_worked_pi)
                    // $('#curr_fte_var').val(auto[key].worked_fte_var)
                    // $('#prev_fte_var').val(auto[key].pp_worked_fte_var)
                    // $('#rolling6').val(auto[key].rolling6)
                    // $('#rolling12').val(auto[key].rolling12)
                }
            }

            // New Function
            // $('#pp_dropdown').on('change', function(){

            //     var pp = document.getElementById("pp_dropdown").value;
                
                    
            // });
            
        });
    });



</script>

@endsection

{{-- @endif --}}