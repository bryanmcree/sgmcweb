{{--New file Template--}}

{{--Add Security for this page below--}}
@if( Auth::user()->hasRole('PI_Variance_Admin') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')


    <div class="card mb-4 text-white bg-dark">
        <div class="card-header">
            PI Variance Explanation Dashboard
        </div>
        <div class="card-body">
            
            <table class="table table-dark table-bordered table-hover" id="variance">
                <thead>
                    <th>Cost Center</th>
                    <th>Submitted By</th>
                    <th>Submit Date</th>
                    <th>Details</th>
                    <th class="text-center">Actions</th>
                </thead>
                <tbody>
                    @foreach($variances as $var)
                        <tr>
                            <td> {{ $var->cost_center }} </td>
                            <td> {{ $var->auth_user }} </td>
                            <td> {{ Carbon::parse($var->created_at)->format('m-d-Y') }} </td>
                            <td> <a href="/variance/show/{{$var->id}}" class="btn btn-info btn-sm btn-block">View</a> </td>
                            <td class="text-center"> 
                                <a href="/variance/print/{{$var->id}}" target="blank" title="Print"><i class="fas fa-print fa-2x"></i></a>
                                <a href="#" class="text-danger" id="deleteVariance" variance_id={{ $var->id }} title="Delete"><i class="fas fa-trash-alt fa-2x"></i></a>
                             </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div>

        <div class="card-footer">
            <a href="/variance/create" class="btn btn-primary">Start New PI Variance Form</a>
        </div>

    </div>


@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">
    $(document).ready(function() {
        $('#variance').DataTable( {
            "pageLength": 10,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>

<script type="application/javascript">

$(document).on("click", "#deleteVariance", function () {
            //alert($(this).attr("data-cost_center"));
            var var_id = $(this).attr("variance_id");
            DeleteVar(var_id);
        });

        function DeleteVar(var_id) {
            swal({
                title: "Delete PI Variance Explanation?",
                text: "Deleting this PI Variance cannot be undone.",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                confirmButtonText: "Yes, delete it!",
                confirmButtonColor: "#ec6c62"
            }, function() {
                $.ajax({
                    url: "/variance/destroy/" + var_id,
                    type: "GET"
                })
                    .done(function(data) {
                        swal({
                                title: "Deleted",
                                text: "PI Variance Explanation Deleted",
                                type: "success",
                                timer: 2200,
                                showConfirmButton: false

                            }
                        );
                        setTimeout(function(){window.location.replace('/variance')},1900);
                    })
                    .error(function(data) {
                        swal("Oops", "We couldn't connect to the server!", "error");
                    });
            });
        }

</script>

@endsection

@endif