    {{--New file Template--}}

{{--Add Security for this page below--}}
@if( Auth::user()->hasRole('PI_Variance_Admin') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')


    <div class="card mb-4 bg-dark text-white">

        <div class="card-header">
            Details for: {{$var->cost_center}}
            <span class="float-right"><a href="/variance" class="btn btn-primary btn-sm">Return To Dashboard</a></span>
        </div>

        <div class="card-body">

            <table class="table table-dark table-hover table-bordered">
                <thead>
                    <th style="width:40%;">Description</th>
                    <th>Data</th>
                </thead>
                <tbody>
                    <tr>
                        <td>Cost Center # and Name:</td>
                        <td> {{ $var->cost_center }} </td>
                    </tr>
                    <tr>
                        <td>Pay Period End Date</td>
                        <td> {{ Carbon::parse($var->pay_end)->format('m/d/Y') }} </td>
                    </tr>
                    <tr>
                        <td>Was this Variance Approved?</td>
                        <td> @if($var->approved == 1) Yes @else No @endif </td>
                    </tr>
                    @if($var->approved == 1)
                        <tr>
                            <td>Date Range of Approval [Start]</td>
                            <td> {{ Carbon::parse($var->start_approval)->format('m/d/Y') }} </td>
                        </tr>
                        <tr>
                            <td>Date Range of Approval [End]</td>
                            <td> {{ Carbon::parse($var->end_approval)->format('m/d/Y') }} </td>
                        </tr>
                    @endif
                </tbody>
            </table>

            <br>
            <h4>PI Variance</h4>
            <hr>

            <table class="table table-dark table-bordered table-hover">
                <thead class="text-center">
                    <th style="width:40%;">Category</th>
                    <th>Current Pay Period</th>
                    <th>Previous Pay Period</th>
                </thead>
                <tbody class="text-center">
                    <tr>
                        <td>Worked Hours Per UOS</td>
                        <td> {{ number_format($var->curr_paid_UOS, 2) }} </td>
                        <td> {{ number_format($var->prev_paid_UOS, 2) }} </td>
                    </tr>
                    <tr>
                        <td>Worked Target</td>
                        <td> {{ number_format($var->curr_paid_target, 2) }} </td>
                        <td> {{ number_format($var->prev_paid_target, 2) }} </td>
                    </tr>
                    <tr>
                        <td>Worked Productivity Index</td>
                        <td> {{ number_format($var->curr_paid_index, 2) }} </td>
                        <td> {{ number_format($var->prev_paid_index, 2) }} </td>
                    </tr>
                    <tr>
                        <td>Worked FTE Variance</td>
                        <td> {{ number_format($var->curr_paid_FTE_var, 2) }} </td>
                        <td> {{ number_format($var->prev_paid_FTE_var, 2) }} </td>
                    </tr>
                    <tr>
                        <td>Rolling 6 Pay Period FTE Variance</td>
                        <td> {{ number_format($var->six_period_FTE_var, 2) }} </td>
                        <td class="text-center text-muted">Not Applicable</td>
                    </tr>
                    <tr>
                        <td>Rolling 12 Month FTE Variance</td>
                        <td> {{ number_format($var->twelve_month_FTE_var, 2) }} </td>
                        <td class="text-center text-muted">Not Applicable</td>
                    </tr>
                </tbody>
            </table>

            <br>
            <h4>Overtime Variance</h4>
            <hr>

            <table class="table table-dark table-hover table-bordered">
                <thead class="text-center">
                    <th style="width:40%;">Category</th>
                    <th>Current Pay Period</th>
                    <th>Previous Pay Period</th>
                </thead>
                <tbody class="text-center">
                    <tr>
                        <td>Current Pay Period Overtime %</td>
                        <td> {{ $var->current_period_OT_percent }} </td>
                        <td> {{ $var->previous_period_OT_percent }} </td>
                    </tr>
                    <tr>
                        <td class="text-center">Rolling 6 Pay Period Overtime %</td>
                        <td> {{ $var->six_period_OT_percent }} </td>
                        <td class="text-center text-muted">Not Applicable</td>
                    </tr>
                    <tr>
                        <td class="text-center">Rolling 12 Month Overtime %</td>
                        <td> {{ $var->twelve_month_OT_percent }} </td>
                        <td class="text-center text-muted">Not Applicable</td>
                    </tr>
                </tbody>
            </table>

            <br>
            <h4>Reasons for Variance from Budget</h4>
            <hr>

            <table class="table table-dark table-hover table-bordered">
                <tbody>
                    @if(!empty($var->reasons_for_variance))
                        <td> {{ $var->reasons_for_variance }} </td>
                    @else
                        <td class="text-danger"> No Reason Given </td>
                    @endif
                </tbody>
            </table>

            <br>
            <h4 class="mt-3">Action Plan</h4>
            <hr>

            <table class="table table-dark table-hover table-bordered">
                <thead>
                    <th style="width:40%;">Dates of Variance Starting with Most Recent</th>
                    <th>Variance Correction Plan</th>
                </thead>
                <tbody>
                    @if(Carbon::parse($var->recent1_var_date)->format('m/d/Y') > '01/01/1901')
                        <tr>
                            <td> {{ Carbon::parse($var->recent1_var_date)->format('m/d/Y') }} </td>
                            <td> {{ $var->recent1_correction }} </td>
                        </tr>
                        <tr>
                            <td> {{ Carbon::parse($var->recent2_var_date)->format('m/d/Y') }} </td>
                            <td> {{ $var->recent2_correction }} </td>
                        </tr>
                        <tr>
                            <td> {{ Carbon::parse($var->recent3_var_date)->format('m/d/Y') }} </td>
                            <td> {{ $var->recent3_correction }} </td>
                        </tr>
                    @else
                        <tr>
                            <td colspan="2" class="text-danger">No Data Entered</td>
                        </tr>
                    @endif
                </tbody>
            </table>

            <br>

            <table class="table table-dark table-hover table-bordered">
                <thead>
                    <th>Aniticipated End Of Variance Date</th>
                    <th>Signature</th>
                </thead>
                <tbody>
                    <tr>
                        <td> {{ Carbon::parse($var->pay_end)->format('m/d/Y') }} </td>
                        <td> {{ $var->auth_user }} </td>
                    </tr>
                </tbody>
            </table>

        </div>

        <div class="card-footer">
            <a href="/variance" class="btn btn-primary">Return To Dashboard</a>
        </div>

    </div>


@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection

@endif