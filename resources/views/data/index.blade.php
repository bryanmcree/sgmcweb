@extends('layouts.app')

@section('content')

<div class="col-md-4 col-lg-offset-4">
    <div class="panel panel-default">
        <div class="panel-heading panel-heading-custom">
            <div class="panel-title">
                Report Request
            </div>
        </div>
            <div class="panel-body">

                {!! Form::open(array('action' => array('DataSubmitController@store'), 'class' => 'form_control', 'method' => 'POST')) !!}
                <div class="form-group">
                    {!! Form::label('first_name', 'First Name:') !!}
                    <div class="input-group">
                        <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                        {!! Form::text('first_name', null, ['class' => 'form-control', 'id'=>'first_name', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('last_name', 'Last Name:') !!}
                    <div class="input-group">
                        <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                        {!! Form::text('last_name', null, ['class' => 'form-control', 'id'=>'last_name', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('office_phone', 'Office Phone:') !!}
                    <div class="input-group">
                        <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                        {!! Form::text('office_phone', null, ['class' => 'form-control', 'id'=>'office_phone', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('email', 'Email:') !!}
                    <div class="input-group">
                        <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                        {!! Form::text('email', null, ['class' => 'form-control', 'id'=>'email', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('cost_center', 'Department:') !!}
                    <div class="input-group">
                        <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                        {!! Form::text('cost_center', null, ['class' => 'form-control', 'id'=>'cost_center', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('description', 'Reason:') !!}
                    {!! Form::textarea('description', null, ['class' => 'form-control', 'id'=>'description','size' => '30x3']) !!}
                </div>

            </div>
        <div class="modal-footer bg-default">

            {!! Form::submit('Submit Form', ['class'=>'btn btn-sgmc', 'id'=>'updateButton']) !!}
            {!! Form::close() !!}
        </div>
            </div>
        </div>
    </div>
</div>

@endsection