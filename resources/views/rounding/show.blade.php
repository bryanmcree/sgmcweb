@extends('layouts.rounding')

@section('content')
    @if ( Auth::user()->hasRole('Reports') == FALSE)

        @include('layouts.unauthorized')
        @Else

        <div class="hidden-sm hidden-xs hidden-md">
            @include("navbars.rounding")
        </div>
        <div class="panel panel-default">
            <div class="panel-heading panel-heading-custom">
                <div class="panel-title"><h4>{{$survey->survey_name}}</h4></div>
            </div>
            <div class="panel-body">
                <a href="/rounding/survey/{{$survey->id}}" class="btn btn-primary">Start Survey</a>
                <a href="#" class="btn btn-warning hidden-xs hidden-sm" data-toggle="modal" data-target=".AddQuestion">Add Question</a>

                <div class="panel panel-default hidden-xs hidden-sm">
                    <div class="panel-heading-custom panel-heading">
                        <div class="panel-title"><h4>Questions</h4></div>
                    </div>
                        <div class="panel-body">
                            <table class="table table-striped table-responsive">
                                <thead>
                                <tr>
                                    <td></td>
                                    <td><b>Question</b></td>
                                    <td class="hidden-sm hidden-xs"><b>Type</b></td>
                                    <td class="hidden-sm hidden-xs"><b>Entered</b></td>
                                    <td></td>
                                </tr>
                                </thead>
                                <tbody>
                                <?PHP $number = 0 ?>
                                @foreach ($survey->surveyQuestion as $questions)
                                    <?PHP $number = $number + 1 ?>
                                <tr>
                                    <td><b>{{$number}}.</b></td>
                                    <td>{{$questions->question}}</td>
                                    <td class="hidden-sm hidden-xs">{{$questions->question_type}}</td>
                                    <td class="hidden-sm hidden-xs">{{$questions->created_at}}</td>
                                    <td>

                                        <ul class="nav nav-pills" style="float: right">
                                            <li><a href="#" class="EditQuestion"
                                                   data-question_id = "{{$questions->id}}"
                                                   data-question_type = "{{$questions->question_type}}"
                                                   data-survey_id = "{{$questions->survey_id}}"
                                                   data-question = "{{$questions->question}}"
                                                   data-category_id = "">Edit</a></li>
                                            <li><a href="#" class="DeleteQuestion"
                                                   data-question_id ="{{$questions->id}}"
                                                   data-survey_id = "{{$questions->survey_id}}"
                                                >Delete</a></li>
                                        </ul>

                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    @endif

    @include("rounding.modals.addquestion")
    @include("rounding.modals.editquestion")

@section('scripts')

    <script type="text/javascript">

        $(document).on("click", ".EditQuestion", function () {
            //alert($(this).attr("data-question_id"));
            $('.question_id').val($(this).attr("data-question_id"));
            $('.question').val($(this).attr("data-question"));
            $('.question_type').val($(this).attr("data-question_type"));
            $('.survey_id').val($(this).attr("data-survey_id"));
            $('.EditQuestion_modal').modal('show');
        });

        //Delete Scrip for Questions
        $(document).on("click", ".DeleteQuestion", function () {
            var question_id = $(this).attr("data-question_id");
            var survey_id = $(this).attr("data-survey_id");
            //alert($(this).attr("data-question_id"));
            //alert($(this).attr("data-system-id"));
            deleteQuestion(question_id, survey_id);
        });

        function deleteQuestion(question_id, survey_id) {
            swal({
                title: "Delete Question?",
                text: "Are you sure that you want to delete this question?",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                confirmButtonText: "Yes, delete it!",
                confirmButtonColor: "#ec6c62"
            }, function() {
                $.ajax({
                    url: "/rounding/question/del/" + question_id + "?survey_id=" + survey_id ,
                    type: "GET"
                })
                    .done(function(data) {
                        swal({
                                title: "Deleted",
                                text: "Question was Deleted",
                                type: "success",
                                timer: 1800,
                                showConfirmButton: false

                            }
                        );
                        setTimeout(function(){window.location.replace('/rounding/show/'+ survey_id)},1900);
                    })
                    .error(function(data, status, error) {
                        alert(status);
                        alert(xhr.responseText);
                        swal("Oops", "We couldn't connect to the server!", "error");
                    });
            });



        }
        //END DELETE SCRIPT

        $(document).ready(function() {
            oTable = $('#reports').DataTable(
                    {
                        "info":     true,
                        "pageLength": 20,
                        "lengthChange": false,
                        "order": [],
                        "columnDefs": [
                            { targets: 'no-sort', orderable: false }
                        ],


                    }
            );
            $('#search').keyup(function(){
                oTable.search($(this).val()).draw() ;
            })


        });
    </script>

@endsection
@endsection