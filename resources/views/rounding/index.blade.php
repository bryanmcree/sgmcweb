@extends('layouts.rounding')

@section('content')
    @if ( Auth::user()->hasRole('Reports') == FALSE)

        @include('layouts.unauthorized')
        @Else


        <div class="hidden-sm hidden-xs hidden-md">
            @include("navbars.rounding")
        </div>


        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#home">Surveys</a></li>
            <li><a data-toggle="tab" href="#menu1" class="hidden-sm hidden-xs hidden-md">Completed Surveys</a></li>
        </ul>

        <div class="tab-content">
            <div id="home" class="tab-pane fade in active">
                <h3>Surveys</h3>

                <div class="panel panel-default">
                    <div class="panel-body">
                        @foreach ($survey as $surveys)
                            <div class="col-sm-12" style="text-align: center">
                                <a href="/rounding/show/{{$surveys->id}}" class="btn-sgmc btn-lg btn-block">{{$surveys->survey_name}}</a>
                                <br>
                            </div>
                        @endforeach
                    </div>
                </div>

            </div>
            <div id="menu1" class="tab-pane fade">
                <h3>Completed Surveys</h3>

                <div class="panel panel-default hidden-sm hidden-xs hidden-md">
                    <div class="panel-heading panel-heading-custom">
                        <div class="panel-title">Completed Surveys</div>
                    </div>
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <td><b>Survey Name</b></td>
                            <td class="hidden-sm hidden-xs"><b>Completed</b></td>
                            <td></td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($completedSurveys as $survey)
                            <tr>
                                <td>{{$survey->surveys->survey_name}}</td>
                                <td>{{$survey->surveys->created_at}}</td>
                                <td >{{\Carbon\Carbon::createFromTimeStamp(strtotime($survey->surveys->created_at))->diffForHumans()}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>












    @endif

@include("rounding.modals.addsurvey")

@section('scripts')

    <script type="text/javascript">

        $(document).ready(function() {
            oTable = $('#reports').DataTable(
                    {
                        "info":     true,
                        "pageLength": 20,
                        "lengthChange": false,
                        "order": [],
                        "columnDefs": [
                            { targets: 'no-sort', orderable: false }
                        ],


                    }
            );
            $('#search').keyup(function(){
                oTable.search($(this).val()).draw() ;
            })


        });
    </script>

@endsection
@endsection