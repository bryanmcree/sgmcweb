<div class="modal fade EditQuestion_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><b>Edit Question</b></h4>
            </div>
            <div class="modal-body">
                {!! Form::open(array('action' => ['RoundingController@editQuestion'], 'class' => 'form_control')) !!}
                {!! Form::hidden('id',null,['class'=>'question_id']) !!}
                {!! Form::hidden('survey_id',null,['class'=>'survey_id']) !!}

                <div class="form-group">
                    {!! Form::label('question', 'Question:') !!}
                    {!! Form::text('question', null, ['class' => 'form-control question', 'id'=>'question', 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('question_type', 'Question Type:') !!}

                    {!! Form::select('question_type', [
                                        '' => '[Select Type]',

                                        'Yes/No' => 'Yes/No',

                                        'Mutiple Choice' => 'Mutiple Choice',

                                        'Checkbox(s)' => 'Checkbox',
                                        'Short Answer' => 'Short Answer',

                                         ], null, ['class' => 'form-control question_type', 'required']) !!}
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Update Question</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
