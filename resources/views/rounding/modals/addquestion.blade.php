<div class="modal fade AddQuestion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><b>Add Question</b></h4>
            </div>
            <div class="modal-body">
                {!! Form::open(array('action' => ['RoundingController@storeQuestion'], 'class' => 'form_control')) !!}
                {!! Form::hidden('survey_id', $survey->id,['class'=>'survey_id']) !!}

                @include("rounding.modals.formAddQuestion")

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save Question</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
