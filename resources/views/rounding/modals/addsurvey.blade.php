<div class="modal fade AddSurvey" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><b>Add Survey</b></h4>
            </div>
            <div class="modal-body">
                {!! Form::open(array('action' => ['RoundingController@store'], 'class' => 'form_control')) !!}

                @include("rounding.modals.formAddSurvey")

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save Survey</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
