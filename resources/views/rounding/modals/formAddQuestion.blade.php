<div class="form-group">
    {!! Form::label('question', 'Question:') !!}
    {!! Form::text('question', null, ['class' => 'form-control', 'id'=>'question', 'required']) !!}
</div>

<div class="form-group">
    {!! Form::label('question_type', 'Question Type:') !!}

    {!! Form::select('question_type', [
                        '' => '[Select Type]',

                        'Yes/No' => 'Yes/No',

                        'Mutiple Choice' => 'Mutiple Choice',

                        'Checkbox(s)' => 'Checkbox',
                        'Short Answer' => 'Short Answer',

                         ], null, ['class' => 'form-control ', 'required']) !!}
</div>