@extends('layouts.rounding')

@section('content')
    @if ( Auth::user()->hasRole('Reports') == FALSE)

        @include('layouts.unauthorized')
        @Else

        <div class="hidden-sm hidden-xs hidden-md">
            @include("navbars.rounding")
        </div>
        <div class="panel panel-default">
            <div class="panel-heading panel-heading-custom">
                <div class="panel-title"><h4>{{$survey->survey_name}}</h4></div>
            </div>
            <div class="panel-body">
                {!! Form::open(array('action' => ['RoundingController@storingSurvey'], 'class' => 'form_control')) !!}
                {!! Form::hidden('survey_id', $survey->id,['class'=>'survey_id']) !!}
                <table class="table-bordered table-responsive table-striped">
                    <tbody>
                    @foreach ($survey->surveyQuestion as $questions)
                        <tr>
                            <td>
                                @if($questions->question_type == 'Yes/No')

                                <b>{{$questions->question}}</b>
                                <br>
                                <br>
                                    <div class="input-group">
                                        <div id="radioBtn" class="btn-group">
                                            <a class="btn btn-primary btn-sm notActive" data-toggle="{{$questions->id}}" data-title="Y">YES</a>
                                            <a class="btn btn-primary btn-sm notActive" data-toggle="{{$questions->id}}" data-title="X">IDK/NA</a>
                                            <a class="btn btn-primary btn-sm active" data-toggle="{{$questions->id}}" data-title="N">NO</a>
                                        </div>
                                        <input type="hidden" name="{{ $questions->id }}[answer]" id="{{$questions->id}}">
                                    </div>
                                @elseif ($questions->question_type == 'Short Answer')
                                    <input type="hidden" value="No" name="{{ $questions->id }}[answer]">
                                    <b>{{$questions->question}}</b>
                                    <br>
                                    <br>
                                    <textarea name="{{ $questions->id }}[answer]" class="form-control" cols="50" rows="4"></textarea>
                                @endif

                            </td>
                        </tr>
                    @endforeach
                    <tr>
                        <td>
                            <br>
                            <br>
                            <button type="submit" class="btn btn-info btn-block">Complete Survey</button>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br>
                            <br>
                            <a href="/rounding" class="btn btn-danger btn-block">Cancle Survey</a>
                        </td>
                    </tr>
                    </tbody>
                </table>
                {!! Form::close() !!}
            </div>
        </div>
        </div>
    @endif

    @include("rounding.modals.addquestion")

@section('scripts')

    <script type="text/javascript">

        $('#radioBtn a').on('click', function(){
            var sel = $(this).data('title');
            var tog = $(this).data('toggle');
            $('#'+tog).prop('value', sel);

            $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
            $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
        })

    </script>

@endsection
@endsection