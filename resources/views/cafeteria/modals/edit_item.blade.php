<div class="modal fade" id="editItem" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document" style="width:25%">
            <div class="modal-content text-white bg-dark">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Edit Menu Item</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form method="POST" action="/cafeteria/item/update">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" class="item-id">

                    <div class="modal-body">

                        <div class="form-group">
                            <label>Item Name</label>
                            <input type="text" class="form-control item-title" name="title">
                        </div>
                        <div class="form-group">
                            <label>Item Price</label>
                            <input type="number" step="0.01" class="form-control item-price" name="price">
                        </div>
                        <div class="form-group">
                            <label>Item Calories</label>
                            <input type="number" class="form-control item-calories" name="calories">
                        </div>
                        <div class="form-group">
                            <label>Item Category</label>
                            <select name="category" class="form-control item-category">
                                <option value="">[Choose Category]</option>
                                <option value="Main">Main Dish</option>
                                <option value="Side">Side Item</option>
                                <option value="Extra">Extra</option>
                            </select>
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" value="Update Item">
                    </div>

                </form>

            </div>

        </div>
    </div>