//Pass values to modal for editing
$(document).on("click", ".editItem", function () {
$('.item-id').val($(this).attr("var_id"));
$('.item-title').val($(this).attr("var_title"));
$('.item-price').val($(this).attr("var_price"));
$('.item-calories').val($(this).attr("var_calories"));
$('.item-category').val($(this).attr("var_category"));
$('#editItem').modal('show');
});