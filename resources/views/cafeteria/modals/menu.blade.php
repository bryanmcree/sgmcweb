<div class="modal fade" id="cafeteriaMenu" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document" style="width:25%">
            <div class="modal-content text-white bg-dark">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Create New Menu</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form method="POST" action="/cafeteria/menu/store">
                    {{ csrf_field() }}
                    <input type="hidden" name="created_by" value="{{ Auth::user()->employee_number }}">

                    <div class="modal-body">

                        <div class="form-group">
                            <label>Menu Title</label>
                            <input type="text" class="form-control" name="title">
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" value="Create Menu">
                    </div>

                </form>

            </div>

        </div>
    </div>