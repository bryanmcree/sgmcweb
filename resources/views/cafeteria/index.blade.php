{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Cafeteria Menu') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    <div class="card bg-dark text-white mb-3">
        <div class="card-header">
            Cafeteria Menu
        </div>
        <div class="card-body">

            <table class="table table-dark table-striped table-hover table-bordered">
                <thead>
                    <th>Menu</th>
                    <th>Created by</th>
                    <th>Created at</th>
                    <th>Add/Edit Items</th>
                    <th>View</th>
                </thead>
                <tbody>
                    @foreach($menus as $menu)

                        <tr>
                            <td>{{ $menu->title }}</td>
                            <td>{{ $menu->createdBy->name }}</td>
                            <td>{{ Carbon::parse($menu->created_at)->format('m-d-Y') }}</td>
                            <td> <a href="/cafeteria/menu/items/{{ $menu->id }}" class="btn btn-sm btn-primary btn-block">Items</a> </td>
                            <td> <a href="/cafeteria/show/{{ $menu->id }}"class="btn btn-sm btn-info btn-block">View</a> </td>
                        </tr>

                    @endforeach
                </tbody>
            </table>

        </div>
        <div class="card-footer">
            <a href="#cafeteriaMenu" data-toggle="modal" class="btn btn-sm btn-primary">Create New Menu</a>
            <a href="#cafeteriaItem" data-toggle="modal" class="btn btn-sm btn-secondary">Create New Food Item</a>
            <a href="/cafeteria/items" class="btn btn-sm btn-warning">Manage Food Items</a>
        </div>
    </div>

@include('cafeteria.modals.menu')
@include('cafeteria.modals.item')

@endsection

@endif