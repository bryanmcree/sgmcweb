{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Cafeteria Menu') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    <div class="card bg-dark text-white mb-3">
        <div class="card-header">
            Choose Menu Items
            <span class="float-right"> <a href="/cafeteria" class="btn btn-sm btn-secondary">Return Home</a> </span>
        </div>
        <div class="card-body">

            <h5>Choose items for {{ $menu->title }}</h5>
            <hr>

            <form method="POST" action="/cafeteria/menu/items/store/{{ $menu->id }}">
                {{ csrf_field() }}

                <div class="form-group text-dark">
                    <label class="text-white" for="items">Items:</label>
                    <select name="items[]" class="form-control" multiple="multiple" id="item_select">

                        <optgroup label = "Main Dishes">
                        @foreach($items as $item)

                            <option value="{{$item->id}}" @foreach($chosen as $chose) @if($item->id == $chose->item_id) selected @endif @endforeach>{{$item->title}}</option>

                        @endforeach

                    </select>
                </div>

                <hr>

                <input type="submit" class="btn btn-primary btn-block">

            </form>

        </div>
    </div>

@include('cafeteria.modals.menu')
@include('cafeteria.modals.item')

@endsection

@section('scripts')

    
<script>
    var select_element = document.getElementById('item_select');
    multi(select_element), {
        'enable_search': true,
        'search_placeholder': 'Search...',
        'non_selected_header': 'All options',
        'selected_header': 'Selected options'
    };

</script>

@endsection 

@endif