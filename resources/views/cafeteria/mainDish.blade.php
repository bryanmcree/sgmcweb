<meta http-equiv="refresh" content="90">

@extends('layouts.new_nonav')
    {{--Page Design Goes Below--}}
@section('content')

@if($menu->title == 'The Main Dish')
    <style>
        body{
            background-image: url('/img/mainDish.png');
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-position: center;
            font-family: Century Gothic,CenturyGothic,AppleGothic,sans-serif; 
        }
    </style>
@elseif($menu->title == 'Salad Bar')
    <style>
        body{
            background-image: url('/img/Salad_Header.png');
            background-repeat: no-repeat;
            background-attachment: fixed;
            font-family: Century Gothic,CenturyGothic,AppleGothic,sans-serif; 
            background-color: darkslategray;
        }
    </style>
@endif

    <body>

        <div class="row text-white" style="margin-top: 20%;">
            <div class="col-lg-6" style="border-right:1px solid white">
                @foreach($menu->menuItems as $item)
                    @if($item->category == 'Main')
                        <div class="row">
                            <div class="col-lg-1">
                            
                            </div>
                            <div class="col-lg-5 text-left">
                                <h3>{{strtolower($item->title)}}</h3>
                            </div>
                            <div class="col-lg-3 text-center">
                                <h3>${{ number_format($item->price, 2) }}</h3>
                            </div>
                            <div class="col-lg-3 text-right">
                                <h3>cal. {{$item->calories}}</h3>
                            </div>
                        </div>
                        <br>
                    @endif
                @endforeach
            </div>

            <div class="col-lg-1 mr-4 ml-2">
                <button class="btn btn-circle-grill" style="background-color:#75cdd6">Sides</button>
                
                <button class="btn btn-circle-grill" style="margin-top:175%; background-color:#a9d84b">Xtras</button>
            </div>

            <div class="col-lg-4">
                @foreach($menu->menuItems as $item)
                    @if($item->category == 'Side')
                        <div class="row">
                            <div class="col-lg-6 text-left">
                                <h4>{{strtolower($item->title)}}</h4>
                            </div>
                            <div class="col-lg-3 text-left">
                                <h4>${{ number_format($item->price, 2) }}</h4>
                            </div>
                            <div class="col-lg-3 text-left">
                                <h4>cal. {{$item->calories}}</h4>
                            </div>
                        </div>
                        <br>
                    @endif
                @endforeach
                <hr class="bg-white">
                @foreach($menu->menuItems as $item)
                    @if($item->category == 'Extra')
                        <div class="row">
                            <div class="col-lg-6 text-left">
                                <h4>{{strtolower($item->title)}}</h4>
                            </div>
                            <div class="col-lg-3 text-left">
                                <h4>${{ number_format($item->price, 2) }}</h4>
                            </div>
                            <div class="col-lg-3 text-left">
                                <h4>cal. {{$item->calories}}</h4>
                            </div>
                        </div>
                        <br>
                    @endif
                @endforeach
            </div>

        </div> <!-- ROW -->

    </body>

    <footer style="background-color: transparent;" class="mt-4 text-white">
        <h3 class="text-center mb-0">2000 calories a day is used for general nutrition advice, but calorie needs vary.
            <br>Additional nutrition information is avaliable upon request.
        </h3>
    </footer>

    

@endsection