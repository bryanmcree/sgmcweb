{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Cafeteria Menu') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    <div class="card bg-dark text-white mb-3">
        <div class="card-header">
            Cafeteria Menu
            <span class="float-right"> <a href="/cafeteria" class="btn btn-sm btn-primary">Return to Dashboard</a> </span>
        </div>
        <div class="card-body">

            <table class="table table-dark table-striped table-hover table-bordered" id="itemT">
                <thead>
                    <th>Item</th>
                    <th>Price</th>
                    <th>Calories</th>
                    <th>Category</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </thead>
                <tbody>
                    @foreach($items as $item)

                        <tr>
                            <td>{{ $item->title }}</td>
                            <td>{{ $item->price }}</td>
                            <td>{{ $item->calories }}</td>
                            <td>{{ $item->category }}</td>
                            <td> <a href="#" class="btn btn-sm btn-warning editItem btn-block" var_id="{{ $item->id }}" var_title="{{ $item->title }}" var_price="{{ $item->price }}"
                                    var_calories="{{ $item->calories }}" var_category="{{ $item->category }}">Edit</a> 
                            </td>
                            <td> <a href="#" class="btn btn-sm btn-danger deleteItem btn-block" item_id="{{ $item->id }}">Delete</a> </td>
                        </tr>

                    @endforeach
                </tbody>
            </table>

        </div>
        <div class="card-footer">
            <a href="#cafeteriaItem" data-toggle="modal" class="btn btn-sm btn-secondary">Create New Food Item</a>
        </div>
    </div>

@include('cafeteria.modals.item')
@include('cafeteria.modals.edit_item')


@endsection

@section('scripts')
    
    <script>
        @include('cafeteria.modals.javascript')
    </script>

    <script type="application/javascript">
        $(document).ready(function() {
            $('#itemT').DataTable( {
                "pageLength": 10,
                "order": [],
                "columnDefs": [ {
                    "targets"  : 'no-sort',
                    "orderable": false,
                }]
            } );
        } );
    </script>

    <script type="application/javascript">

        $(document).on("click", ".deleteItem", function () {
                var item_id = $(this).attr("item_id");
                DeleteItem(item_id);
            });
        
        function DeleteItem(item_id) {
            swal({
                title: "Delete Item?",
                text: "Deleting this Item cannot be undone.",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                confirmButtonText: "Yes, delete it!",
                confirmButtonColor: "#ec6c62"
            }, function() {
                $.ajax({
                    url: "/cafeteria/item/destroy/" + item_id,
                    type: "GET"
                })
                    .done(function(data) {
                        swal({
                                title: "Deleted",
                                text: "Item Deleted",
                                type: "success",
                                timer: 1800,
                                showConfirmButton: false
        
                            },function() {
                                location.reload();
                            });
                    })
                    .error(function(data) {
                        swal("Oops", "We couldn't connect to the server!", "error");
                    });
            });
        }
        
    </script>

@endsection

@endif