{{--New file Template--}}

{{--Add Security for this page below--}}

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}


@section('content')

    <div class="card bg-dark">
        <div class="card-header text-white">
            <h2>Welcome to the SGMC Event Dashboard!</h2>
        </div>
        <div class="card-body">
            <div class="card-deck">

                <div class="card col-lg-8 mb-4 text-white bg-secondary" style="border-radius:0px;">
                    <div class="card-header">
                        Upcoming Events!
                    </div>
                    <div class="card-body">

                        <div class="table-responsive">
                            <table class="table table-dark table-bordered table-striped table-hover">
                                <thead>
                                    <th>Name</th>
                                    <th>Event Date</th>
                                    <th>Location</th>
                                    <th>Schedule</th>
                                </thead>
                                <tbody>
                                    @forelse($upEvents as $event)
                                        <tr>
                                            <td> {{ $event->name }} </td>
                                            <td> {{ Carbon::parse($event->start_date)->format('m-d-Y') }} </td>
                                            <td> {{ $event->location }} </td>
                                            <td class="text-center"> <a href="/events/detail/{{$event->id}}"><i class="fas text-info fa-lg fa-info-circle"></i></a> </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="4" class="text-center text-danger">No Upcoming Events</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                        
                    </div>
                    <div class="card-footer">
                        <i>Choose an event from the list above and click the <i class="fas text-info fa-sm fa-info-circle"></i> icon to register for it.</i>
                    </div>
                </div>

                <div class="card col-lg-4 mb-4 text-white bg-secondary" style="border-radius:0px;">
                    <div class="card-header">
                        Your Events
                    </div>
                    <div class="card-body">

                        <div class="table-responsive">
                            <table class="table table-dark table-bordered table-striped table-hover">
                                <thead>
                                    <th>Name</th>
                                    <th>Event Date</th>
                                    <th>Details</th>
                                </thead>
                                <tbody>
                                    @forelse($myEvents as $event)
                                        <tr>
                                            <td nowrap> {{ $event->name }} </td>
                                            <td> {{ Carbon::parse($event->start_date)->format('m-d-Y') }} </td>
                                            <td class="text-center"> <a href="/events/detail/{{$event->id}}"><i class="fas text-info fa-lg fa-info-circle"></i></a> </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="4" class="text-center text-danger">You Haven't Created Any Events</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>

                    </div>
                    <div class="card-footer">
                        <a href="#createEvent" data-toggle="modal" class="btn btn-primary btn-block">Create New Event</a>
                    </div>
                </div>

            </div>
        </div>
    </div>

    
@include('events.modals.create')

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">



</script>

@endsection
