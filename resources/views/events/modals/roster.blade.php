<div class="modal fade" id="roster" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document" style="max-width: 35%">
        <div class="modal-content text-white bg-dark">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Link Roster To Event</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form method="POST" class="mb-0" action="/events/roster/{{$event->id}}">
                {{ csrf_field() }}

                <div class="modal-body">

                    <div class="form-group">
                        <label>Roster</label>
                        <select name="roster" class="form-control">
                            @if(empty($event->roster_id))
                                <option value="" selected>[Choose Roster]</option>
                            @else 
                                <option value="{{$event->roster_id}}" selected>{{$event->eventRoster->roster_name}}</option>
                            @endif
                            @foreach($rosters as $roster)
                                <option value="{{$roster->id}}">{{$roster->roster_name}}</option>
                            @endforeach
                        </select>
                    </div>
                   
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Submit">
                </div>

            </form>
        </div>
    </div>
</div>