<div class="modal fade" id="createSchedule" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document" style="max-width: 35%">
        <div class="modal-content text-white bg-dark">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">New Schedule</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form method="POST" action="/events/schedule/store">
                <input type="hidden" name="event_id" value="{{$event->id}}">
                {{ csrf_field() }}

                <div class="modal-body">

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Start Time</label>
                            <input type="time" class="form-control" name="start_time" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>End Time</label>
                            <input type="time" class="form-control" name="end_time" required>
                        </div>
                    </div>

                    @if($event->multiple == 0)
                        <div class="form-group">
                            <label>Intervals</label>
                            <div class="input-group">
                                <input type="number" class="form-control" name="interval" placeholder="Time length of each meeting in minutes. ex: 30, 60, 90 etc." required>
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">minutes</span>
                                </div>
                            </div>
                        </div>
                    @endif
                   
                    <i>Note: Please enter times on a 24 hour clock</i>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Submit">
                </div>

            </form>
        </div>
    </div>
</div>