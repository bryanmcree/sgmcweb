<div class="modal fade" id="eventNotes" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document" style="max-width: 35%">
        <div class="modal-content text-white bg-dark">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Meeting Notes</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form method="POST" action="/events/notes">
                {{ csrf_field() }}
                <input type="hidden" class="time-id" name="id">

                <div class="modal-body">

                    <div class="form-group">
                        <label>Notes For Meeting (Optional)</label>
                        <textarea name="notes" rows="5" class="form-control time-notes" placeholder="Here is where you can put notes for the upcoming meeting. This allows the host to be better prepared and improve the quality of the meeting." required></textarea>
                    </div>
                   
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Submit">
                </div>

            </form>
        </div>
    </div>
</div>