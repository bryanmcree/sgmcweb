//Pass Event Data to Event Edit modal
$(document).on("click", ".edit-event", function () {
$('.event-id').val($(this).attr("data-id"));
$('.event-name').val($(this).attr("data-name"));
$('.event-start_date').val($(this).attr("data-start_date"));
$('.event-end_date').val($(this).attr("data-end_date"));
$('.event-location').val($(this).attr("data-location"));
$('.event-active').val($(this).attr("data-active"));
$('.event-schedule').val($(this).attr("data-schedule"));
$('.event-roster').val($(this).attr("data-roster"));
$('.event-multiple').val($(this).attr("data-multiple"));
$('.event-limit').val($(this).attr("data-limit"));
$('#editEvent').modal('show');
});

//Pass Time Data to Notes modal
$(document).on("click", ".event-notes", function () {
$('.time-id').val($(this).attr("data-time-id"));
$('.time-notes').val($(this).attr("data-notes"));
$('#eventNotes').modal('show');
});

//View Notes Modal
$(document).on("click", ".event-notes-view", function () {
$('.time-notes-view').val($(this).attr("data-notes-view"));
$('#eventNotesView').modal('show');
});