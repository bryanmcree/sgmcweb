<div class="modal fade" id="editEvent" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document" style="max-width: 45%">
        <div class="modal-content text-white bg-dark">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Event</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form method="POST" action="/events/update">
                <input type="hidden" name="id" class="event-id">
                {{ csrf_field() }}

                <div class="modal-body">

                    <div class="form-row">
                        <div class="form-group col-6">
                            <label>Name</label>
                            <input type="text" class="form-control event-name" name="name" placeholder="Name Of Event" required>
                        </div>
                        <div class="form-group col-6">
                            <label>Location</label>
                            <input type="text" class="form-control event-location" name="location" placeholder="Location The Event Will Take Place" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Event Date</label>
                        <input type="date" class="form-control event-start_date" name="start_date" required>
                    </div>

                    <div class="form-group">
                        <label for="">Do you wish to allow multiple users to schedule in the same time slot?</label>
                        <select name="multiple" class="form-control event-multiple" required>
                            <option value="0">No</option>
                            <option value="1">Yes</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>If so, how many users would you like to limit this event to? </label>
                        <input type="number" name="user_limit" class="form-control event-limit">
                    </div>

                    {{--<hr>

                    <div class="form-group">
                        <label>Do you want your event to appear on the SGMC Dashboard?</label>
                        <select name="active" class="form-control event-active" required>
                            <option value="" selected>[Choose Result]</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Will attendees need to schedule a time to be at this event?</label>
                        <select name="schedule" class="form-control event-schedule" required>
                            <option value="" selected>[Choose Result]</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Will attendees need to sign in when they arrive at the event?</label>
                        <select name="roster" class="form-control event-roster" required>
                            <option value="" selected>[Choose Result]</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </div> --}}

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Update">
                </div>

            </form>
        </div>
    </div>
</div>