{{--New file Template--}}

{{--Add Security for this page below--}}

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

@section('content')

    @if($event->active == 0)
        <div class="alert alert-danger">
        <h4>This event is not active.</h4> Therefore it will not appear on the SGMC Dashboard for others to see. Once you are ready to make this event open to the public simply click the 'Activate' button. To make it unavailable to the public click the same button which will now say 'Deactivate'. 
        </div>
    @endif

    <div class="card bg-dark mb-4">
        <div class="card-header text-white">
            <h2> 
                {{ $event->name }} 
                <span class="float-right">
                    @if($event->created_by == Auth::user()->employee_number || Auth::user()->employee_number == '21012' || Auth::user()->employee_number == '19745')
                        @if(empty($event->roster_id))
                            <a href="#roster" data-toggle="modal" class="btn btn-primary" title="Link Roster"><i class="fas fa-link"></i></a> 
                        @else
                            <a href="#roster" data-toggle="modal" class="btn btn-warning" title="Edit Roster"><i class="fas fa-link"></i></a>
                            <a href="/roster/load/{{$event->roster_id}}" class="btn btn-primary btn-sm" target="_blank">Load Roster</a>
                        @endif
                        <a href="mailto:@if(!empty($reserved_names)) @foreach($reserved_names as $reserve) {{ $reserve->reservedBy->mail }}; @endforeach @endif" class="btn btn-info" title="Email all students"><i class="fas fa-envelope"></i></a>
                    @endif
                    <a href="/events" class="btn btn-sm btn-secondary">Return to Dashboard</a>
                </span> 
            </h2>
        </div>
        <div class="card-body">
            <div class="card-deck">

                <div class="card col-6 mb-2 text-white bg-secondary" style="border-radius:0px;">
                    <div class="card-header">
                        Schedule Details
                    </div>
                    <div class="card-body">

                        <div class="table-responsive">
                            <table class="table table-dark table-bordered table-striped table-hover">
                                <thead>
                                    <th>Name</th>
                                    <th>Event Date</th>
                                    <th>Location</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td> {{ $event->name }} </td>
                                        <td> {{ Carbon::parse($event->start_date)->format('m-d-Y') }} </td>
                                        <td> {{ $event->location }} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <hr>

                        @if(empty($schedule))
                            @if($event->created_by == Auth::user()->employee_number)
                                <div class="alert alert-info">You have no schedule created, please create one by clicking the button below.</div>
                                <a href="#createSchedule" data-toggle="modal" class="btn btn-primary btn-block">Create Schedule</a>
                            @else
                                <div class="alert alert-warning">A schedule has not yet been set up. Please check back later for scheduling times.</div>
                            @endif
                        @else

                            <div class="table-responsive">
                                <table class="table table-dark table-hover table-striped table-bordered">
                                    <thead>
                                        <th class="text-center">Time Slot</th>
                                        <th>Cost Center(s)</th>
                                        <th>Reserved By</th>
                                    </thead>
                                    <tbody>
                                        @foreach ($scheduleData as $data)

                                            <tr>
                                                <td nowrap class="text-center"> {{ $data->time_slot }} </td>
                                                <td>
                                                    @foreach($reserved as $reserve)
                                                        @if($reserve->time_id == $data->id)
                                                            {{ $reserve->cost_center }} @if($event->multiple == 1) <span class="float-right"> -> {{ $reserve->reservedBy->name }} </span> <hr> @else <br> @endif
                                                        @endif
                                                    @endforeach
                                                </td>
                                                <td>
                                                    @if($event->multiple == 0)
                                                        @if(!empty($data->reservedBy)) {{ $data->reservedBy->name }} @endif
                                                    @else
                                                        @foreach($reserved_names as $name)
                                                            @if(!empty($name->reservedBy)) {{ $name->reservedBy->name }} @endif <br>
                                                        @endforeach
                                                    @endif

                                                    @if($event->created_by == Auth::user()->employee_number && $data->reserved_by == Auth::user()->employee_number)
                                                        <a href="/events/complete/{{$data->id}}" class="float-right ml-2" title="Mark Meeting as Complete"><i class="fas fa-check @if($data->isComplete == 0) text-secondary @else text-success @endif"></i></a>
                                                        <a href="#" class="float-right event-notes" title="notes" data-time-id="{{$data->id}}" data-notes="{{$data->notes}}"><i class="fas fa-comment @if(empty($data->notes)) text-secondary @endif"></i></a>
                                                    @elseif($event->created_by == Auth::user()->employee_number)
                                                        <a href="/events/complete/{{$data->id}}" class="float-right ml-2" title="Mark Meeting as Complete"><i class="fas fa-check @if($data->isComplete == 0) text-secondary @else text-success @endif"></i></a>
                                                        <a href="#" class="float-right event-notes-view" title="notes" data-notes-view="{{$data->notes}}"><i class="fas fa-comment @if(empty($data->notes)) text-secondary @endif"></i></a>
                                                    @elseif($data->reserved_by == Auth::user()->employee_number)
                                                        <a href="#" class="float-right event-notes" title="notes" data-time-id="{{$data->id}}" data-notes="{{$data->notes}}"><i class="fas fa-comment @if(empty($data->notes)) text-secondary @endif"></i></a>
                                                    @endif
                                                </td>
                                            </tr>

                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @endif

                        {{-- Roster here maybe --}}

                    </div>
                    <div class="card-footer">
                        <div class="alert alert-info mb-0">
                            <i>
                                Use the <i class="fas fa-comment text-white"></i> icon to leave notes for your meeting. 
                                <i class="fas fa-comment text-secondary"></i> = no notes have been added. 
                                <i class="fas fa-comment text-primary"></i> = notes have been added.
                            </i>
                        </div>
                    </div>
                </div>

                <div class="card col-6 mb-2 text-white bg-secondary" style="border-radius:0px;">
                    <div class="card-header">
                        Reserve
                        <span class="float-right text-warning">
                            @if($event->multiple == 1) 
                                {{ count($reserved_names) }} of {{ $event->user_limit }} slots reserved
                            @endif
                        </span>
                    </div>
                    <div class="card-body">

                        @if(empty($schedule))
                            @if($event->created_by == Auth::user()->employee_number)
                                <div class="alert alert-info">You have no schedule created, please create one by clicking the button below.</div>
                                <a href="#createSchedule" data-toggle="modal" class="btn btn-primary btn-block">Create Schedule</a>
                            @else
                                <div class="alert alert-warning">A schedule has not yet been set up. Please check back later for scheduling times.</div>
                            @endif
                        @else

                            <div class="table-responsive">
                                <table class="table table-dark table-hover table-striped table-bordered">
                                    <thead>
                                        <th class="text-center">Time Slot</th>
                                        <th>Cost Center</th>
                                        <th>Reserve</th>
                                    </thead>
                                    <tbody>
                                        @foreach ($scheduleData as $data)

                                            <form action="/events/schedule/{{$data->id}}" method="POST">
                                                <input type="hidden" name="reserved_by" value="{{Auth::user()->employee_number}}">
                                                <input type="hidden" name="event_id" value="{{$event->id}}">
                                                <input type="hidden" name="schedule_id" value="{{$schedule->id}}">
                                                {{ csrf_field() }}
                                            @if($event->multiple == 0)
                                                <tr @if($data->reserved_by == Auth::user()->employee_number) class="bg-info" @elseif($data->reserved == 1) class="bg-danger" @endif>
                                                    <td nowrap class="text-center"> {{ $data->time_slot }} </td>
                                                    <td>
                                                        <select name="cost_center[]" class="multi-2" style="width:100%;" multiple="multiple" @if($data->reserved == 1) disabled @endif required>
                                                            <option value="">[Choose Your Cost Center(s)]</option>
                                                            @foreach($costcenters as $cc)
                                                                <option value="{{$cc->style1}}">{{$cc->style1}}</option>
                                                            @endforeach
                                                        </select>
                                                        <small class="form-text">Select all cost centers involved in this meeting</small>
                                                    </td>
                                                    <td style="width:10%;" class="text-center"> 
                                                        @if($data->reserved == 0) <input type="submit" class="btn btn-success btn-sm btn-block" value="Reserve"> 
                                                        @elseif($data->reserved_by == Auth::user()->employee_number) <a href="/events/cancel/{{$data->id}}" class="btn btn-sm btn-block btn-danger">Cancel</a> 
                                                        @else N/A 
                                                        @endif 
                                                    </td> 
                                                </tr>
                                            @else 
                                                <tr @if($data->reserved_by == Auth::user()->employee_number) class="bg-info" @endif>
                                                    <td nowrap class="text-center"> {{ $data->time_slot }} </td>
                                                    <td>
                                                        <select name="cost_center[]" class="multi-2" style="width:100%;" multiple="multiple" required>
                                                            <option value="">[Choose Your Cost Center(s)]</option>
                                                            @foreach($costcenters as $cc)
                                                                <option value="{{$cc->style1}}">{{$cc->style1}}</option>
                                                            @endforeach
                                                        </select>
                                                        <small class="form-text ">Select all cost centers involved in this meeting</small>
                                                    </td>
                                                    <td style="width:10%;" class="text-center"> 
                                                        
                                                        @foreach($reserved_names as $res)
                                                            @if($res->reserved_by == Auth::user()->employee_number) 
                                                                <a href="/events/cancel/{{$data->id}}" class="btn btn-sm btn-block btn-danger">Cancel</a> 
                                                                <?php $flag = 1 ?>
                                                            @endif
                                                        @endforeach

                                                        @if($flag == 0)
                                                            @if($event->user_limit <= count($reserved_names))
                                                                <span class="text-danger"><small><i>Event Full</i></small></span>
                                                            @else
                                                                <input type="submit" class="btn btn-success btn-sm btn-block" value="Reserve">  
                                                            @endif                                                             
                                                        @endif
                                                    </td> 
                                                </tr>
                                            @endif

                                            </form>

                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @endif

                    </div>
                    <div class="card-footer">

                        @if($event->created_by == Auth::user()->employee_number)
                            <div class="alert alert-info mb-0">
                                <i>If you wish to reserve a time slot for a break (ex: lunch) choose your cost center in that slot and click reserve.</i>
                            </div>
                        @else 
                            <div class="alert alert-info mb-0">
                                <i>If you wish to cancel a meeting simply click the cancel button next to the timeslot you had reserved.</i>
                            </div>
                        @endif
                    </div>
                </div>

            </div>
        </div>

        @if($event->created_by == Auth::user()->employee_number || Auth::user()->employee_number == '21012' || Auth::user()->employee_number == '19745')
        <div class="card-footer">

            <form action="/events/activate/{{$event->id}}" method="POST" class="mb-0">
                {{ csrf_field() }}
                <input type="hidden" name="active" value="{{$event->active}}">

                <div class="btn-group" style="width:100%;" role="group" aria-label="Basic example">
                    <a href="#" class="btn text-white edit-event m-auto" style="width:33%; background-color:#f4b942;"
                                data-id="{{ $event->id }}"
                                data-name="{{ $event->name }}"
                                data-start_date="{{ Carbon::parse($event->start_date)->format('Y-m-d') }}"
                                data-location="{{ $event->location }}"
                                data-active="{{ $event->active }}"
                                data-schedule="{{ $event->schedule }}"
                                data-roster="{{ $event->roster }}"
                                data-multiple="{{ $event->multiple }}"
                                data-limit="{{ $event->user_limit }}"
                                >Edit
                    </a>
                    <input type="submit" class="btn text-white m-auto btn-secondary" style="width:33%;" value="@if($event->active == 0) Activate @else Deactivate @endif"></input>
                    <a href="#" event_id="{{$event->id}}" class="btn text-white deleteEvent m-auto" style="width:33%; background-color:#f7513b;">Delete</a>
                </div>
            
            </form>

        </div>
        @endif

    </div>

    
@include('events.modals.create')
@include('events.modals.edit')
@include('events.modals.schedule')
@include('events.modals.notes')
@include('events.modals.notes_view')
@include('events.modals.roster')

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')


<script type="application/javascript">

    @include('events.modals.javascript')

    $(document).on("click", ".deleteEvent", function () {
        //alert($(this).attr("data-cost_center"));
        var event_id = $(this).attr("event_id");
        DeleteEvent(event_id);
    });
    
    function DeleteEvent(event_id) {
        swal({
            title: "Delete Event?",
            text: "Deleting this Event cannot be undone.",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            confirmButtonText: "Yes, delete it!",
            confirmButtonColor: "#ec6c62"
        }, function() {
            $.ajax({
                url: "/events/destroy/" + event_id,
                type: "GET"
            })
                .done(function(data) {
                    swal({
                            title: "Deleted",
                            text: "Event Deleted",
                            type: "success",
                            timer: 1800,
                            showConfirmButton: false

                        });
                        setTimeout(function(){window.location.replace('/events')},1900);
                })
                .error(function(data) {
                    swal("Oops", "We couldn't connect to the server!", "error");
                });
        });
    }


</script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script>
    $(document).ready(function() {
        $(".multi-2").select2();
    });
</script>

@endsection
