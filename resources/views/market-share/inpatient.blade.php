@extends('layouts.new_nonav')

<style>
    
</style>

@section('content')

    <div class="card bg-dark text-white mb-3">

        <div class="card-header">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <h4>Inpatient Market Share: <span class="text-warning"> {{ $category }} </span> </h4>
                </div>
                <div class="col-lg-6 col-md-6">
                    <span class="float-right">
                        <div class="btn-group">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="inpatient-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Inpatient
                            </button>
                            <div class="dropdown-menu" aria-labelledby="inpatient-btn">
                                @foreach($ipCategories as $ipcat)
                                    <a href="/market-share/inpatient/{{ $ipcat->category }}" class="dropdown-item py-0">{{ $ipcat->category }}</a>
                                @endforeach
                            </div>
                        </div>
                        
                        <div class="btn-group">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="outpatient-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Outpatient
                            </button>
                            <div class="dropdown-menu" aria-labelledby="outpatient-btn">
                                @foreach($opCategories as $opcat)
                                    <a href="/market-share/outpatient/{{ $opcat->category }}" class="dropdown-item py-0">{{ $opcat->category }}</a>
                                @endforeach
                            </div>
                        </div>
                        <a href="/market-share" class="btn btn-primary">Return to Chart Dashboard</a>
                    </span>
                </div>
            </div>
            
        </div>
        
        <div class="card-body">

            <div id="map" style="height:100%;"></div>
        
            <script>
        
                var data = <?= json_encode($data); ?>;
                var total = <?= json_encode($totalShare); ?>;
        
                function initMap() {
        
                    var map = new google.maps.Map(document.getElementById('map'), {
                        zoom: 8,
                        center: {lat: 30.831048, lng: -83.278934}
                    });

                    var src = 'http://mysgmcbenefits.com/forms/ga_counties_19.kml';

                    // console.log(src);

                    var kmlLayer = new google.maps.KmlLayer(src, {
                        suppressInfoWindows: true,
                        preserveViewport: false,
                        map: map
                    });
        
                    var opacity = .3;

                    for (var i = 0; i < data.length; i++)
                    {
                        var setOpacity = opacity;
                        
                        opacity += .02;

                        var setScale = ((data[i].num_records / total.total).toFixed(4) * 100) * 1.2; 

                        if(setScale < 6)
                        {
                            setScale = 6;
                        }

                        var countyTitle = 'County: ' + data[i].county; //data[i].avg_los.toString(10)
                        var alosTitle = 'ALOS: ' + data[i].avg_los.toFixed(4);
                        var shareFormat = (data[i].num_records / total.total).toFixed(4) * 100; 
                        var shareTitle = 'Share: ' + shareFormat + '%';
                        // console.log(shareFormat);

                        var marker = new google.maps.Marker({
                            position: {lat: data[i].latitude, lng: data[i].longitude},
                            icon: {
                                path: google.maps.SymbolPath.CIRCLE,
                                scale: setScale,
                                fillColor: 'black',
                                fillOpacity: setOpacity,
                                strokeColor: 'white',
                                strokeWeight: .3
                            },
                            map: map,
                            title: countyTitle + '\n' + alosTitle + '\n' + shareTitle
                        });
                        
                    }
        
                }
        
            </script>
            

        </div>

        <div class="card-footer">
            <small><i>*Records based on zip code of patient’s residence.</i></small>    
        </div>

    </div>


    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDDqCLn5-u1VpkILKXPs71_u-jBaLRGJy4&callback=initMap">
    </script>
    

    
@endsection