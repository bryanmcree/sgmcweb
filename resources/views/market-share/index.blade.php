@extends('layouts.app')

<style>

    body {
        margin: 0;
        padding: 0;
        width:100vw;
        height: 100vh;
        background-color: #eee;
    }
    .content {
        display: flex;
        justify-content: center;
        align-items: center;
        width:100%;
        height:100%;
    }
    .loader-wrapper {
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0;
        left: 0;
        background-color: #242f3f;
        display:flex;
        justify-content: center;
        align-items: center;
        z-index: 9;
    }
    .loader {
        display: inline-block;
        width: 30px;
        height: 30px;
        position: relative;
        border: 4px solid #Fff;
        animation: loader 2s infinite ease;
    }
    .loader-inner {
        vertical-align: top;
        display: inline-block;
        width: 100%;
        background-color: #fff;
        animation: loader-inner 2s infinite ease-in;
    }
    @keyframes loader {
        0% { transform: rotate(0deg);}
        25% { transform: rotate(180deg);}
        50% { transform: rotate(180deg);}
        75% { transform: rotate(360deg);}
        100% { transform: rotate(360deg);}
    }
    @keyframes loader-inner {
        0% { height: 0%;}
        25% { height: 0%;}
        50% { height: 100%;}
        75% { height: 100%;}
        100% { height: 0%;}
    }

</style>

@section('content')

    <div class="loader-wrapper">
        <span class="loader"><span class="loader-inner"></span></span>
    </div>

    <div class="card bg-dark text-white mb-3 col-lg-8 mx-auto">
        <div class="card-body">

            <div class="row">
                <div class="btn-group col-lg-3">
                    <button class="btn btn-primary btn-block dropdown-toggle" type="button" id="inpatient-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Inpatient Going To Map
                    </button>
                    <div class="dropdown-menu" aria-labelledby="inpatient-btn">
                        @foreach($ipCategoriesGoing as $ipcat)
                            <a href="/market-share/inpatient-going/{{ $ipcat->DRG }}" class="dropdown-item py-0">{{ $ipcat->DRG }}</a>
                        @endforeach
                    </div>
                </div>

                <div class="btn-group col-lg-3">
                    <button class="btn btn-primary btn-block dropdown-toggle" type="button" id="inpatient-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Inpatient Coming From Map
                    </button>
                    <div class="dropdown-menu" aria-labelledby="inpatient-btn">
                        @foreach($ipCategoriesComing as $ipcat)
                            <a href="/market-share/inpatient-coming/{{ $ipcat->DRG }}" class="dropdown-item py-0">{{ $ipcat->DRG }}</a>
                        @endforeach
                    </div>
                </div>

                <div class="btn-group col-lg-3">
                    <button class="btn btn-primary btn-block dropdown-toggle" type="button" id="inpatient-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Outpatient Going To Map
                    </button>
                    <div class="dropdown-menu" aria-labelledby="inpatient-btn">
                        @foreach($opCategoriesGoing as $opcat)
                            <a href="/market-share/outpatient-going/{{ $opcat->county }}" class="dropdown-item py-0">{{ $opcat->county }}</a>
                        @endforeach
                    </div>
                </div>

                <div class="btn-group col-lg-3">
                    <button class="btn btn-primary btn-block dropdown-toggle" type="button" id="inpatient-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Outpatient Coming From Map
                    </button>
                    <div class="dropdown-menu" aria-labelledby="inpatient-btn">
                        @foreach($opCategoriesComing as $opcat)
                            <a href="/market-share/outpatient-coming/{{ $opcat->facility }}" class="dropdown-item py-0">{{ $opcat->facility }}</a>
                        @endforeach
                    </div>
                </div>

            </div>
            
        </div>
        <div class="card-footer">
            <small>*Records based on zip code of patient’s residence. &nbsp; *Data based on primary and secondary service areas.</small>
            <span class="float-right">
                <a href="/market-share/print-all" class="btn btn-sm btn-info">Print All</a>
            </span>
        </div>
    </div>

    <div class="col-lg-12 col-md-12 mb-4 ">
        <ul class="nav nav-pills nav-justified flex-column flex-sm-row flex-md-row" id="myTab" role="tablist">
            <li class="nav-item mr-2 mt-2">
                <a class="btn btn-block btn-outline-light active" id="Inpatient-going-tab" data-toggle="pill" href="#InpatientGoing" role="tab" aria-controls="Inpatient" aria-selected="true">Inpatient Going To Charts</a>
            </li>
            <li class="nav-item mr-2 mt-2">
                <a class="btn btn-block btn-outline-light" id="Inpatient-coming-tab" data-toggle="pill" href="#InpatientComing" role="tab" aria-controls="Inpatient" aria-selected="true">Inpatient Coming From Charts</a>
            </li>
            <li class="nav-item mr-2 mt-2">
                <a class="btn btn-block btn-outline-light" id="Inpatient-going-tab" data-toggle="pill" href="#OutpatientGoing" role="tab" aria-controls="Inpatient" aria-selected="true">Outpatient Going To Charts</a>
            </li>
            <li class="nav-item mr-2 mt-2">
                <a class="btn btn-block btn-outline-light" id="Inpatient-coming-tab" data-toggle="pill" href="#OutpatientComing" role="tab" aria-controls="Inpatient" aria-selected="true">Outpatient Coming From Charts</a>
            </li>
        </ul>
    </div>

    

    <div class="tab-content mt-5 col-12" id="myTabContent">

        <div class="tab-pane fade show active" id="InpatientGoing" role="tabpanel" aria-labelledby="Inpatient-tab">

            <div class="card-deck">
            
                @foreach($ipCategoriesGoing as $cat)
                
                    <div class="col-lg-6 m-auto p-0">
                        <div class="card bg-dark text-white mb-3">
                            <div class="card-header">
                                {{ $cat->DRG }}
                                <span class="float-right text-warning">
                                    Service Line Margin: ${{ number_format($ipDataGoing->where('DRG', $cat->DRG)->first()->service_line_margin, 2) }}
                                    ||
                                    {{ $ipDataGoing->where('DRG', $cat->DRG)->first()->margin_percent * 100 }}%
                                    &nbsp; <a href="/market-share/ip-going/print/{{ $cat->DRG }}" class="btn btn-sm btn-info" title="print" target="_blank"><i class="fal fa-print"></i></a>
                                </span>
                            </div>
                            
                            <div class="card-body">

                                <canvas id="ip-going-{{ str_replace('&', '', $cat->DRG) }}" ></canvas>
                                <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.js"></script>
                                <script type="application/javascript">
            
                                    Chart.defaults.global.defaultFontColor = 'white';
            
                                    new Chart(document.getElementById('ip-going-{{ str_replace('&', '', $cat->DRG) }}'), {
                                        type: 'horizontalBar',
            
                                        data: {
                                            labels: [
                                                @foreach($ipDataGoing->where('DRG', $cat->DRG) as $label)
                                                    "{{ $label->facility }}",
                                                @endforeach
                                            ],
                                            datasets: [{
                                                label: "",
                                                backgroundColor: "#51e8e8",
                                                data: [
                                                    @foreach($ipDataGoing->where('DRG', $cat->DRG) as $answer)
                                                        @foreach($totalSharesIPGoing->where('DRG', $cat->DRG) as $total)
                                                            @if($total->total == 0)
                                                                0,
                                                            @else
                                                                {{ number_format(($answer->num_records / $total->total) * 100, 2) }},
                                                            @endif
                                                        @endforeach
                                                    @endforeach
                                                ]
                                            }]
                                        },
                                        options: {
                                            legend: { display: false },
                                            title: {
                                                display: true,
                                                text: '% of {{ str_replace('&', 'and', $cat->DRG) }} patients going to the following facilities'
                                            },
                                            scales: {
                                                yAxes: [{
                                                    stacked:false,
                                                ticks: {
                                                    beginAtZero: true
                                                    }
                                                }]
                                            },
                                            tooltips: {
                                                enabled: true,
                                                mode: 'single',
                                                callbacks: {
                                                    label: function (tooltipItems, data) {
                                                        return "Market Share: " + tooltipItems.xLabel + "%";
                                                    }
                                                }
                                            }
                                        }
                                    });
                                </script>

                            </div>
                        </div>
                    </div>

                @endforeach
            
            </div>  

        </div>

        <div class="tab-pane fade show" id="InpatientComing" role="tabpanel" aria-labelledby="Inpatient-tab">

            
            @foreach($ipCategoriesComing as $cat)
                
                <div class="card bg-dark text-white mb-3">
                    <div class="card-header">
                        {{ $cat->DRG }}
                        <span class="float-right text-warning">
                            Service Line Margin: ${{ number_format($ipDataComing->where('DRG', $cat->DRG)->first()->service_line_margin, 2) }}
                            ||
                            {{ $ipDataComing->where('DRG', $cat->DRG)->first()->margin_percent * 100 }}%
                            &nbsp; <a href="/market-share/ip-coming/print/{{ $cat->DRG }}" class="btn btn-sm btn-info" title="print" target="_blank"><i class="fal fa-print"></i></a>
                        </span>
                    </div>
                    
                    <div class="card-body">

                        <canvas id="ip-coming-{{ str_replace('&', '', $cat->DRG) }}" height="150"></canvas>
                        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.js"></script>
                        <script type="application/javascript">
    
                            Chart.defaults.global.defaultFontColor = 'white';
    
                            new Chart(document.getElementById('ip-coming-{{ str_replace('&', '', $cat->DRG) }}'), {
                                type: 'horizontalBar',
    
                                data: {
                                    labels: [
                                        @foreach($ipDataComing->where('DRG', $cat->DRG) as $label)
                                            "{{ $label->zip }} ({{ $label->city }})",
                                        @endforeach
                                    ],
                                    datasets: [{
                                        label: "",
                                        backgroundColor: "#51e8e8",
                                        data: [
                                            @foreach($ipDataComing->where('DRG', $cat->DRG) as $answer)
                                                @foreach($totalSharesIPComing->where('DRG', $cat->DRG) as $total)
                                                    @if($total->total == 0)
                                                        0,
                                                    @else
                                                        {{ number_format(($answer->num_records / $total->total) * 100, 2) }},
                                                    @endif
                                                @endforeach
                                            @endforeach
                                        ]
                                    }]
                                },
                                options: {
                                    legend: { display: false },
                                    title: {
                                        display: true,
                                        text: '% of {{ str_replace('&', 'and', $cat->DRG) }} patients coming from following ZIP codes',
                                    },
                                    scales: {
                                        yAxes: [{
                                            stacked:false,
                                        ticks: {
                                            beginAtZero: true
                                            }
                                        }]
                                    },
                                    tooltips: {
                                        enabled: true,
                                        mode: 'single',
                                        callbacks: {
                                            label: function (tooltipItems, data) {
                                                    return "Market Share: " + tooltipItems.xLabel + "%";
                                            }
                                        }
                                    }
                                }
                            });
                        </script>

                    </div>
                </div>

            @endforeach
            

        </div>

        <div class="tab-pane fade show" id="OutpatientGoing" role="tabpanel">

            <div class="card-deck">
            
                @foreach($opCategoriesGoing as $cat)
                
                    <div class="col-lg-6 m-auto p-0">
                        <div class="card bg-dark text-white mb-3">
                            <div class="card-header">
                                {{ $cat->county }}
                                <span class="float-right">
                                    <a href="/market-share/op-going/print/{{ $cat->county }}" class="btn btn-sm btn-info" title="print" target="_blank"><i class="fal fa-print"></i></a>
                                </span>
                            </div>
                            
                            <div class="card-body">

                                <canvas id="op-going-{{ str_replace('&', '', $cat->county) }}" ></canvas>
                                <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.js"></script>
                                <script type="application/javascript">
            
                                    Chart.defaults.global.defaultFontColor = 'white';
            
                                    new Chart(document.getElementById('op-going-{{ str_replace('&', '', $cat->county) }}'), {
                                        type: 'horizontalBar',
            
                                        data: {
                                            labels: [
                                                @foreach($opDataGoing->where('county', $cat->county) as $label)
                                                    "{{ $label->facility }}",
                                                @endforeach
                                            ],
                                            datasets: [{
                                                label: "",
                                                backgroundColor: "#51e8e8",
                                                data: [
                                                    @foreach($opDataGoing->where('county', $cat->county) as $answer)
                                                        @foreach($totalSharesOPGoing->where('county', $cat->county) as $total)
                                                            @if($total->total == 0)
                                                                0,
                                                            @else
                                                                {{ number_format(($answer->num_records / $total->total) * 100, 2) }},
                                                            @endif
                                                        @endforeach
                                                    @endforeach
                                                ]
                                            }]
                                        },
                                        options: {
                                            legend: { display: false },
                                            title: {
                                                display: true,
                                                text: '% of {{ $cat->county }} county residents go to the following facilities',
                                            },
                                            scales: {
                                                yAxes: [{
                                                    stacked:false,
                                                ticks: {
                                                    beginAtZero: true
                                                    }
                                                }]
                                            },
                                            tooltips: {
                                                enabled: true,
                                                mode: 'single',
                                                callbacks: {
                                                    label: function (tooltipItems, data) {
                                                            return "Market Share: " + tooltipItems.xLabel + "%";
                                                    }
                                                }
                                            }
                                        }
                                    });
                                </script>

                            </div>
                        </div>
                    </div>

                @endforeach
            
            </div>  

        </div>

        <div class="tab-pane fade show" id="OutpatientComing" role="tabpanel">

            <div class="card-deck">
            
                @foreach($opCategoriesComing as $cat)
                
                    <div class="col-lg-6 m-auto p-0">
                        <div class="card bg-dark text-white mb-3">
                            <div class="card-header">
                                {{ $cat->facility }}
                                <span class="float-right">
                                    <a href="/market-share/op-coming/print/{{ $cat->facility }}" class="btn btn-sm btn-info" title="print" target="_blank"><i class="fal fa-print"></i></a>
                                </span>
                            </div>
                            
                            <div class="card-body">

                                <canvas id="op-coming-{{ str_replace('&', '', $cat->facility) }}" ></canvas>
                                <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.js"></script>
                                <script type="application/javascript">
            
                                    Chart.defaults.global.defaultFontColor = 'white';
            
                                    new Chart(document.getElementById('op-coming-{{ str_replace('&', '', $cat->facility) }}'), {
                                        type: 'horizontalBar',
            
                                        data: {
                                            labels: [
                                                @foreach($opDataComing->where('facility', $cat->facility) as $label)
                                                    "{{ $label->county }}",
                                                @endforeach
                                            ],
                                            datasets: [{
                                                label: "",
                                                backgroundColor: "#51e8e8",
                                                data: [
                                                    @foreach($opDataComing->where('facility', $cat->facility) as $answer)
                                                        @foreach($totalSharesOPComing->where('facility', $cat->facility) as $total)
                                                            @if($total->total == 0)
                                                                0,
                                                            @else
                                                                {{ number_format(($answer->num_records / $total->total) * 100, 2) }},
                                                            @endif
                                                        @endforeach
                                                    @endforeach
                                                ]
                                            }]
                                        },
                                        options: {
                                            legend: { display: false },
                                            title: {
                                                display: true,
                                                text: '% of {{ $cat->facility }} patients come from the following counties',
                                            },
                                            scales: {
                                                yAxes: [{
                                                    stacked:false,
                                                ticks: {
                                                    beginAtZero: true
                                                    }
                                                }]
                                            },
                                            tooltips: {
                                                enabled: true,
                                                mode: 'single',
                                                callbacks: {
                                                    label: function (tooltipItems, data) {
                                                            return "Market Share: " + tooltipItems.xLabel + "%";
                                                    }
                                                }
                                            }
                                        }
                                    });
                                </script>

                            </div>
                        </div>
                    </div>

                @endforeach
            
            </div>  

        </div>

    </div>    

    
@endsection

@section('scripts')
    <script>
        $(window).on("load",function(){
            $(".loader-wrapper").fadeOut("slow");
        });
    </script>
@endsection