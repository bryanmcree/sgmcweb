@extends('layouts.new_nonav')

@section('content')

    <div class="col-lg-6 m-auto">

        <div class="card">
            <div class="card-header">
                <span class="text-primary">{{ $drg }}</span> <small>(Inpatient)</small>
            </div>
            <div class="card-body">
                
                <canvas id="ip-coming"></canvas>
                <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.js"></script>
                <script type="application/javascript">

                    new Chart(document.getElementById('ip-coming'), {
                        type: 'horizontalBar',

                        data: {
                            labels: [
                                @foreach($data as $label)
                                    "{{ $label->zip }} ({{ $label->city }})",
                                @endforeach
                            ],
                            datasets: [{
                                label: "",
                                backgroundColor: "#51e8e8",
                                data: [
                                    @foreach($data as $answer)
                                        {{ number_format(($answer->num_records / $totalShare->total) * 100, 2) }},
                                    @endforeach
                                ]
                            }]
                        },
                        options: {
                            legend: { display: false },
                            title: {
                                display: true,
                                text: '% of {{ str_replace('&', 'and', $drg) }} patients coming from following ZIP codes'
                            },
                            scales: {
                                yAxes: [{
                                    stacked:false,
                                ticks: {
                                    beginAtZero: true
                                    }
                                }]
                            },
                            tooltips: {
                                enabled: true,
                                mode: 'single',
                                callbacks: {
                                    label: function (tooltipItems, data) {
                                        return "Market Share: " + tooltipItems.xLabel + "%";
                                    }
                                }
                            }
                        }
                    });
                </script>

            </div>
        </div>

    </div>

@endsection