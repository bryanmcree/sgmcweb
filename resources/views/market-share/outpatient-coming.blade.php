@extends('layouts.new_nonav')

<style>
    
</style>

@section('content')

    <div class="card bg-dark text-white mb-3">

        <div class="card-header">
            <div class="row">
                <div class="col-lg-5 col-md-6">
                    <h5>Patients of <span class="text-warning"> {{ $category }} </span> come from the following counties</h5> 
                </div>
                <div class="col-lg-7 col-md-6">
                    <span class="float-right">
                        <div class="btn-group">
                            <button class="btn btn-secondary dropdown-toggle btn-sm" type="button" id="inpatient-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Inpatient Going To
                            </button>
                            <div class="dropdown-menu" aria-labelledby="inpatient-btn">
                                @foreach($goingCategories as $gcat)
                                    <a href="/market-share/inpatient-going/{{ $gcat->DRG }}" class="dropdown-item py-0">{{ $gcat->DRG }}</a>
                                @endforeach
                            </div>
                        </div>
                        
                        <div class="btn-group">
                            <button class="btn btn-secondary dropdown-toggle btn-sm" type="button" id="outpatient-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Inpatient Coming From
                            </button>
                            <div class="dropdown-menu" aria-labelledby="outpatient-btn">
                                @foreach($comingCategories as $ccat)
                                    <a href="/market-share/inpatient-coming/{{ $ccat->DRG }}" class="dropdown-item py-0">{{ $ccat->DRG }}</a>
                                @endforeach
                            </div>
                        </div>

                        <div class="btn-group">
                            <button class="btn btn-secondary dropdown-toggle btn-sm" type="button" id="inpatient-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Outpatient Going To Map
                            </button>
                            <div class="dropdown-menu" aria-labelledby="inpatient-btn">
                                @foreach($opCategoriesGoing as $opcat)
                                    <a href="/market-share/outpatient-going/{{ $opcat->county }}" class="dropdown-item py-0">{{ $opcat->county }}</a>
                                @endforeach
                            </div>
                        </div>

                        <div class="btn-group">
                            <button class="btn btn-secondary dropdown-toggle btn-sm" type="button" id="inpatient-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Outpatient Coming From Map
                            </button>
                            <div class="dropdown-menu" aria-labelledby="inpatient-btn">
                                @foreach($opCategoriesComing as $opcat)
                                    <a href="/market-share/outpatient-coming/{{ $opcat->facility }}" class="dropdown-item py-0">{{ $opcat->facility }}</a>
                                @endforeach
                            </div>
                        </div>

                        <a href="/market-share" class="btn btn-primary btn-sm">Return to Chart Dashboard</a>
                    </span>
                </div>
            </div>
            
        </div>
        
        <div class="card-body">

            <div id="map" style="height:100%;"></div>
        
            <script>
        
                var data = <?= json_encode($data); ?>;
                var total = <?= json_encode($totalShare); ?>;
        
                function initMap() {
        
                    var map = new google.maps.Map(document.getElementById('map'), {
                        zoom: 10,
                        center: {lat: 30.831048, lng: -83.278934}
                    });

                    var src = 'http://mysgmcbenefits.com/forms/ga_counties_20.kml';

                    // console.log(src);

                    var kmlLayer = new google.maps.KmlLayer(src, {
                        suppressInfoWindows: true,
                        preserveViewport: false,
                        map: map
                    });
        
                    var opacity = .3;

                    for (var i = 0; i < data.length; i++)
                    {
                        var setOpacity = opacity;
                        
                        opacity += .02;

                        var setScale = ((data[i].num_records / total.total).toFixed(4) * 100) * 1.1; 

                        if(setScale < 6)
                        {
                            setScale += 5;
                        }

                        // var serviceLine = 'Service Line Margin: ' + data[i].service_line_margin; //data[i].avg_los.toString(10)
                        // var alosTitle = 'ALOS: ' + data[i].avg_los.toFixed(4);
                        var county = 'County: ' + data[i].county;
                        var shareFormat = (data[i].num_records / total.total).toFixed(4) * 100; 
                        var shareTitle = 'Share: ' + shareFormat + '%';
                        console.log(shareFormat, county);

                        var marker = new google.maps.Marker({
                            position: {lat: data[i].lat, lng: data[i].long},
                            icon: {
                                path: google.maps.SymbolPath.CIRCLE,
                                scale: setScale,
                                fillColor: 'black',
                                fillOpacity: setOpacity,
                                strokeColor: 'white',
                                strokeWeight: .3
                            },
                            map: map,
                            title: county + '\n' + shareTitle
                        });
                        
                    }
        
                }
        
            </script>
            

        </div>

        <div class="card-footer">
            <small><i>*Records based on zip code of patient’s residence.</i></small>    
        </div>

    </div>


    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDDqCLn5-u1VpkILKXPs71_u-jBaLRGJy4&callback=initMap">
    </script>
    

    
@endsection