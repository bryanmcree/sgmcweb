<p>
    You have received a new request for help with web.sgmc.org!
</p>
<p>
    Here are the details:
</p>
<ul>
    <li>Name: <strong>{{ $name }}</strong></li>
    <li>Email: <strong>{{ $email_address }}</strong></li>
    <li>Phone Number: <strong>{{ $phone_number }}</strong></li>
    <li>Device: <strong>{{ $platform }}</strong></li>
    <li>Device Type: <strong>{{ $type_device }}</strong></li>
    <li>Owner: <strong>{{ $owner }}</strong></li>
</ul>
<hr>
<p>
    Use: <strong>{{ $use }}</strong>
</p>
<hr>