<p>
    You have received a new request for help with web.sgmc.org!
</p>
<p>
    Here are the details:
</p>
<ul>
    <li>First Name: <strong>{{ $first_name }}</strong></li>
    <li>Last Name: <strong>{{ $last_name }}</strong></li>
    <li>Username: <strong>{{ $userid }}</strong></li>
    <li>Email: <strong>{{ $mail }}</strong></li>
    <li>Phone Number: <strong>{{ $phone_number }}</strong></li>
</ul>
<hr>
<p>
    Description: <strong>{{ $problem }}</strong>
</p>
<hr>