<p>
    You have received a new request for access to the ExtraMile site.
</p>
<p>
    Here are the details:
</p>
<ul>
    <li>First Name: <strong>{{ $first_name }}</strong></li>
    <li>Last Name: <strong>{{ $last_name }}</strong></li>
    <li>Title: <strong>{{ $title }}</strong></li>
    <li>Username: <strong>{{ $username }}</strong></li>
    <li>Email: <strong>{{ $email }}</strong></li>
</ul>
<hr>
<p>

</p>
<hr>