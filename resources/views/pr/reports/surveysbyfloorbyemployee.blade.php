{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Patient Relations') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="card bg-dark mb-3 text-white">
            <div class="card-header">
                <b>{{$chart->first()->completedBy->name}}</b>
            </div>
            <div class="card-body">
                <canvas id="patients_floor" height="75"></canvas>
                <p><i>Shows the number of total checks (ones and twos) this employee has completed over all time by floors.</i></p>
            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">
    // Total Patients vs Part One and Two
    new Chart(document.getElementById("patients_floor"), {
        type: 'horizontalBar',
        data: {
            labels: [
                @foreach($chart as $data)
                    '{{$data->floor}}',
                @endforeach
            ],
            datasets: [
                {
                    label: "Patients",
                    //backgroundColor: ["#3e95cd"],
                    data: [
                        @foreach($chart as $floor1)
                        "{{$floor1->total}}",
                        @endforeach
                    ],
                    backgroundColor: [
                        'rgba(0, 99, 132, 0.6)',
                        'rgba(10, 99, 132, 0.6)',
                        'rgba(20, 99, 132, 0.6)',
                        'rgba(30, 99, 132, 0.6)',
                        'rgba(40, 99, 132, 0.6)',
                        'rgba(50, 99, 132, 0.6)',
                        'rgba(60, 99, 132, 0.6)',
                        'rgba(70, 99, 132, 0.6)',
                        'rgba(80, 99, 132, 0.6)',
                        'rgba(90, 99, 132, 0.6)',
                        'rgba(100, 99, 132, 0.6)',
                        'rgba(110, 99, 132, 0.6)',
                        'rgba(120, 99, 132, 0.6)',
                        'rgba(130, 99, 132, 0.6)',
                        'rgba(140, 99, 132, 0.6)',
                        'rgba(150, 99, 132, 0.6)',
                    ],
                    borderColor: [
                        'rgba(0, 99, 132, 0.6)',
                        'rgba(10, 99, 132, 0.6)',
                        'rgba(20, 99, 132, 0.6)',
                        'rgba(30, 99, 132, 0.6)',
                        'rgba(40, 99, 132, 0.6)',
                        'rgba(50, 99, 132, 0.6)',
                        'rgba(60, 99, 132, 0.6)',
                        'rgba(70, 99, 132, 0.6)',
                        'rgba(80, 99, 132, 0.6)',
                        'rgba(90, 99, 132, 0.6)',
                        'rgba(100, 99, 132, 0.6)',
                        'rgba(110, 99, 132, 0.6)',
                        'rgba(120, 99, 132, 0.6)',
                        'rgba(130, 99, 132, 0.6)',
                        'rgba(140, 99, 132, 0.6)',
                        'rgba(150, 99, 132, 0.6)',
                    ],
                    borderWidth: 2,
                }
            ]
        },
        options: {
            legend: { display: false },
            title: {
                display: true,
                //text: {{$chart->first()->emp}}
            }
        }
    });

</script>

@endsection
@endif