<!-- The Modal -->
<div class="modal fade" id="myModal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">

            <form method="post" action="/pr/filtered">
            {{ csrf_field() }}
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title"><b>Filter Results</b></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">

                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="example-date-input" class="col-form-label">Start Date</label>
                            <input class="form-control" name="start_date" type="date" value="{{\Carbon\Carbon::now()->format('Y-m-d')}}" id="example-date-input">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="example-date-input" class="col-form-label">End Date</label>
                            <input class="form-control" name="end_date" type="date" value="{{\Carbon\Carbon::now()->format('Y-m-d')}}" id="example-date-input">
                        </div>
                    </div>
<hr>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="exampleSelect1">Select Floor</label>
                                    <select class="form-control" id="exampleSelect1" name="department">
                                        <option value="" selected>[All Floors]</option>
                                        @foreach($all_floors as $floor)
                                            <option value="{{$floor->dept}}">{{$floor->dept}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <input type="submit" value="Filter" class="btn btn-primary">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>


        </div>
    </div>
</div>