{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Patient Relations') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.new_nonav')
    
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="card bg-dark mb-3 text-white">
            <div class="card-header">
                <b>Select Floor</b>
            </div>
            <div class="card-body">
                <div class="col-lg-12">

                    <div class="card-deck">
                        <div class="card bg-dark text-white mb-3 border-info">
                            <div class="card-body" align="center">
                                <p><b>Total Patients</b></p>
                                <p>{{$total_patients->where('discharged_yn',null)->count()}}</p>
                            </div>
                        </div>
                        <div class="card bg-dark text-white mb-3 border-info">
                            <div class="card-body" align="center">
                                <p><b>Total Discharged Patients</b></p>
                                <p>{{$total_patients->where('discharged_yn','Y')->count()}}</p>
                            </div>
                        </div>
                        <div class="card bg-dark text-white mb-3 border-info">
                            <div class="card-body" align="center">
                                <p><b>Total Admitted via ED</b></p>
                                <p>{{$total_patients->where('discharged_yn','Y')->where('emergency_patient_yn','Y')->count()}}</p>
                            </div>
                        </div>
                        <div class="card bg-dark text-white mb-3 border-info">
                            <div class="card-body" align="center">
                                <p><b>Last Updated</b></p>
                                <p>{{$total_patients->first()->created_at}}</p>
                            </div>
                        </div>
                        <div class="card bg-dark text-white mb-3 border-info">
                            <div class="card-body" align="center">
                                <p><b>Total part 1</b></p>
                                <p>{{$total_part_one->count()}}</p>
                            </div>
                        </div>
                        <div class="card bg-dark text-white mb-3 border-info">
                            <div class="card-body" align="center">
                                <p><b>Total part 2</b></p>
                                <p>{{$total_part_two->count()}}</p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="list-group col-lg-12">
                            @foreach($floors as $floor)
                                <a href="pr/floor/{{$floor->dept}}" class="list-group-item list-group-item-info list-group-item-action"><b>{{$floor->dept}}</b> <i>({{$floor->total}})</i></a>
                            @endforeach
                                <a href="#" class="list-group-item list-group-item-warning list-group-item-action">Discharged Patients</a>
                                <a href="/pr/dashboard" class="list-group-item list-group-item-warning list-group-item-action">Dashboard</a>
                        </div>
                    </div>

                </div>
            </div> <!-- BODY -->

        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
@endif