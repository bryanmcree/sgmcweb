<p>
    <b>PR Compliment</b>
</p>
<p>
    Here are the details:
</p>
<ul>
    <li>Patient MRN: <strong>{{ $patient_mrn }}</strong></li>
    <li>Encounter ID: <strong>{{ $encounter_id }}</strong></li>
    <li>Patient Floor: <strong>{{ $patient_floor }}</strong></li>
    <li>Survey By: <strong>{{ Auth::user()->name  }}</strong></li>
</ul>
<hr>
<p>
    <b>Comments:</b>
</p>
<p>{{$complement_text}}</p>
<hr>