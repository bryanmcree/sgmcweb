{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Patient Relations') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.nonav')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <b>Patient Relations Check - {{$patient->pat_name}} - {{$patient->room}}</b>
            </div>
            <div class="panel-body">

                    @if($patient->discharged_yn == 'Y')
                    <div class="alert alert-info"><b>Patient is discharged.</b></div>
                    @endif

                    @if(!is_null($patient->dob))
                    @if($patient->dob->format('m-d') == \Carbon\Carbon::now()->format('m-d'))
                    <div class="row">
                        <div class="col-lg-12">
                            <p><a href="#" class="btn btn-sgmc btn-block" id="bottom-btn">It's their BIRTHDAY!!!</a></p>

                        </div>
                    </div>
                    @endif
                    @endif

                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#home">Dashboard</a></li>
                    <li><a data-toggle="tab" href="#menu1">Part 1</a></li>
                    <li><a data-toggle="tab" href="#menu2">Part 2</a></li>
                    <li><a data-toggle="tab" href="#menu6">Nurse</a></li>
                    <li><a data-toggle="tab" href="#menu4">Photo/Files</a></li>
                    <li><a data-toggle="tab" href="#menu5">Links</a></li>
                    <li><a @if($patient->emergency_patient_yn =='N')style="cursor: not-allowed" @else data-toggle="tab" @endif href="#menu3">ED</a></li>
                    <li><a href="/pr/floor/{{$patient->dept}}">Back to Patients</a></li>
                    <li><a href="/pr">Select New Floor</a></li>
                </ul>

                <div class="tab-content">
                    <div id="home" class="tab-pane fade in active">
                        <br>
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="panel panel-default">
                                    <div class="panel panel-heading">
                                        <b>Patient Info</b>
                                    </div>
                                    <div class="panel panel-body">
                                        <p><b>{{$patient->pat_name}}</b></p>
                                        <p><b>MRN: </b>{{$patient->mrn}}</p>
                                        <p><b>Encounter: </b>{{$patient->pat_enc_csn_id}}</p>
                                        <p><b>Admission Date/Time: </b>{{$patient->admission}}</p>
                                        <p><b>DOB: </b>@if(!is_null($patient->dob)){{$patient->dob->format('Y-m-d')}}@endif</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                @if($patient->emergency_patient_yn == 'Y')
                                <div class="alert alert-warning" role="alert">
                                    <div class="row vertical-align">
                                        <div class="col-xs-2">
                                            <i class="fas fa-info-circle fa-2x"></i>
                                        </div>
                                        <div class="col-xs-10">
                                            Patient was admitted from the ED.
                                        </div>
                                    </div>
                                </div>
                                @endif
                                @if(is_null($part_one))
                                <div class="alert alert-danger" role="alert">
                                    <div class="row vertical-align">
                                        <div class="col-xs-2">
                                            <i class="fa fa-exclamation-triangle fa-2x"></i>
                                        </div>
                                        <div class="col-xs-10">
                                            Part 1 not complete.
                                        </div>
                                    </div>
                                </div>
                                    @Else
                                <div class="alert alert-success" role="alert">
                                    <div class="row vertical-align">
                                        <div class="col-xs-2">
                                            <i class="fas fa-check fa-2x"></i>
                                        </div>
                                        <div class="col-xs-10">
                                            Part 1 completed by {{$part_one->completedBy->first_name}} {{$part_one->completedBy->last_name}}.
                                        </div>
                                    </div>
                                </div>
                                    @endif
                                    @if(is_null($part_two))
                                        <div class="alert alert-danger" role="alert">
                                            <div class="row vertical-align">
                                                <div class="col-xs-2">
                                                    <i class="fa fa-exclamation-triangle fa-2x"></i>
                                                </div>
                                                <div class="col-xs-10">
                                                    Part 2 not complete.
                                                </div>
                                            </div>
                                        </div>
                                    @Else
                                        <div class="alert alert-success" role="alert">
                                            <div class="row vertical-align">
                                                <div class="col-xs-2">
                                                    <i class="fas fa-check fa-2x"></i>
                                                </div>
                                                <div class="col-xs-10">
                                                    Part 2 completed by {{$part_two->completedBy->first_name}} {{$part_two->completedBy->last_name}}.
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <a href="/pr/survey/swing/{{$patient->pat_enc_csn_id}}?floor={{$patient->dept}}" class="btn btn-info btn-block ">Swing Back Later</a>
                            </div>
                        </div>
                    </div>
                    <div id="menu1" class="tab-pane fade">
                        <form method="post" action="/pr/survey/add">

                            {{ csrf_field() }}
                            <input type="hidden" value="{{ Auth::user()->employee_number }}" name="part_one_completed_by">
                            <input type="hidden" value="{{$patient->mrn}}" name="patient_mrn">
                            <input type="hidden" value="{{$patient->pat_enc_csn_id}}" name="encounter_id">
                            <input type="hidden" value="{{$patient->dept}}" name="patient_floor">
                            <input type="hidden" name="part_one_completed_yn" value="Y">

                        <h4>Part 1</h4>
                        <p><b>Part 1 Checklist</b> - First contact with new patients.</p>
                        <div class="row">

                            <div class="well">
                                <ul class="list-group">
                                    <li class="list-group-item list-group-item-success"><b>Observation check list.</b></li>
                                </ul>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" @if(!is_null($part_two))  disabled @if($part_two->information_current==1)checked @endif @endif name="information_current" value="1" id="defaultChecked2" style="height:35px; width:35px; vertical-align: middle;">
                                    <label class="custom-control-label" for="defaultChecked2">Information on white-board was current</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" @if(!is_null($part_two))  disabled @if($part_two->not_in_room==1)checked @endif @endif name="not_in_room" value="1" id="defaultChecked3" style="height:35px; width:35px; vertical-align: middle;">
                                    <label class="custom-control-label" for="defaultChecked3">Patient was not in room or was asleep</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input"  @if(!is_null($part_two))  disabled @if($part_two->declined==1)checked @endif @endif name="declined" value="1" id="defaultChecked4" style="height:35px; width:35px; vertical-align: middle;">
                                    <label class="custom-control-label" for="defaultChecked4">Patient declined check list</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" @if(!is_null($part_two))  disabled @if($part_two->family==1)checked @endif @endif name="family" value="1" id="defaultChecked5" style="height:35px; width:35px; vertical-align: middle;">
                                    <label class="custom-control-label" for="defaultChecked5">Family member participated in check</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" @if(!is_null($part_two))  disabled @if($part_two->grievance==1)checked @endif @endif name="grievance" value="1" id="defaultChecked6" style="height:35px; width:35px; vertical-align: middle;">
                                    <label class="custom-control-label" for="defaultChecked6">Grievance filed</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" @if(!is_null($part_two))  disabled @if($part_two->work_order==1)checked @endif @endif name="work_order" value="1" id="defaultChecked7" style="height:35px; width:35px; vertical-align: middle;">
                                    <label class="custom-control-label" for="defaultChecked7">Work order submitted</label>
                                </div>
                            </div>

                                <div class="well">
                                    <div class="form-check">
                                        <input class="form-check-input" name="entrance_introduction" @if(!is_null($part_one)) disabled @endif type="checkbox" value="1" id="defaultCheck1" style="height:35px; width:35px; vertical-align: middle;">
                                        <label class="form-check-label" for="defaultCheck1" style="vertical-align: middle;">
                                            Entrance / Introduction
                                        </label>
                                        <ul class="list-group">
                                            <li class="list-group-item list-group-item-success">Knock on patient door as entering</li>
                                            <li class="list-group-item list-group-item-success">Address patient by name and introduce yourself by name and department</li>
                                            <li class="list-group-item list-group-item-success">State the purpose of the visit is to check on the patient and to be sure comfortable and properly oriented to the room</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="well">
                                    <div class="form-check">
                                        <input class="form-check-input" name="review_controls" @if(!is_null($part_one)) checked disabled @endif type="checkbox" value="1" id="defaultCheck2" style="height:35px; width:35px; vertical-align: middle;">
                                        <label class="form-check-label" for="defaultCheck2">
                                            Review Controls
                                        </label>
                                        <ul class="list-group">
                                            <li class="list-group-item list-group-item-success">Review controls for tv, bed, nurse call  button, phone, bath request, change of linens, whiteboards, refer to patient handbook as a resource.</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="well">
                                    <div class="form-check">
                                        <input class="form-check-input" name="set_expectations" @if(!is_null($part_one)) checked disabled @endif type="checkbox" value="1" id="defaultCheck3" style="height:35px; width:35px; vertical-align: middle;">
                                        <label class="form-check-label" for="defaultCheck3">
                                            Set Expectations
                                        </label>
                                        <ul class="list-group">
                                            <li class="list-group-item list-group-item-success">Let patient and family know their nurses and  techs should be rounding on them every hour and a nurse manager should rounde every other day to check on
                                            patients service and care.</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="well">
                                    <div class="form-check">
                                        <input class="form-check-input" name="focus_areas" @if(!is_null($part_one)) checked disabled @endif type="checkbox" value="1" id="defaultCheck4" style="height:35px; width:35px; vertical-align: middle;">
                                        <label class="form-check-label" for="defaultCheck4">
                                            Focus Areas
                                        </label>
                                        <ul class="list-group">
                                            <li class="list-group-item list-group-item-success">Prompt response to call button, clean and quiet environment, communication about new meds.</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="well">
                                    <div class="form-check">
                                        <input class="form-check-input" name="left_card" @if(!is_null($part_one)) checked disabled @endif type="checkbox" value="1" id="defaultCheck5" style="height:35px; width:35px; vertical-align: middle;">
                                        <label class="form-check-label" for="defaultCheck5">
                                            Left Card
                                        </label>
                                        <ul class="list-group">
                                            <li class="list-group-item list-group-item-success">I'm going to leave a card for you.  Please contact me if I can be of assistance for you.</li>
                                            <li class="list-group-item list-group-item-success">Is there anything I can do for you before I leave?  Turn lights on/off, shut door.</li>
                                            <li class="list-group-item list-group-item-success">It was nice to meet you and thank you for choosing SGMC.</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="well">
                                    <div class="form-group">
                                        <label for="question_notes_id">Comments / Notes:</label>
                                        <small id="emailHelp" class="form-text text-muted">Additional information or comments.</small>
                                        <textarea class="form-control" name="part_one_notes" id="question_notes_id" rows="3" @if(!is_null($part_one)) checked disabled @endif></textarea>
                                    </div>
                                </div>

                        </div>
                        <input type="Submit" class="btn btn-primary btn-block" @if(!is_null($part_one)) disabled @endif value="Complete Part 1">
                        </form>
                    </div>
                    <div id="menu2" class="tab-pane fade">
                        <h4>Part 2</h4>
                        <p><b>Part 2 Checklist</b> - Second contact with patient.</p>

                        <form method="post" action="/pr/survey/two">
                            {{ csrf_field() }}
                            <input type="hidden" value="{{ Auth::user()->employee_number }}" name="part_two_completed_by">
                            <input type="hidden" value="{{$patient->mrn}}" name="patient_mrn">
                            <input type="hidden" value="{{$patient->pat_enc_csn_id}}" name="encounter_id">
                            <input type="hidden" value="{{$patient->dept}}" name="patient_floor">
                            <input type="hidden" name="part_two_completed_yn" value="Y">

                        <div class="well">
                            <ul class="list-group">
                                <li class="list-group-item list-group-item-success"><b>Part 2 observation check list.</b></li>
                            </ul>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" @if(!is_null($part_two))  disabled @if($part_two->information_current==1)checked @endif @endif name="information_current" value="1" id="defaultChecked2" style="height:35px; width:35px; vertical-align: middle;">
                                <label class="custom-control-label" for="defaultChecked2">Information on white-board was current</label>
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" @if(!is_null($part_two))  disabled @if($part_two->not_in_room==1)checked @endif @endif name="not_in_room" value="1" id="defaultChecked3" style="height:35px; width:35px; vertical-align: middle;">
                                <label class="custom-control-label" for="defaultChecked3">Patient was not in room or was asleep</label>
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input"  @if(!is_null($part_two))  disabled @if($part_two->declined==1)checked @endif @endif name="declined" value="1" id="defaultChecked4" style="height:35px; width:35px; vertical-align: middle;">
                                <label class="custom-control-label" for="defaultChecked4">Patient declined check list</label>
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" @if(!is_null($part_two))  disabled @if($part_two->family==1)checked @endif @endif name="family" value="1" id="defaultChecked5" style="height:35px; width:35px; vertical-align: middle;">
                                <label class="custom-control-label" for="defaultChecked5">Family member participated in check</label>
                            </div>

                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" @if(!is_null($part_two))  disabled @if($part_two->discharge_folders==1)checked @endif @endif name="discharge_folders" value="1" id="defaultChecked5" style="height:35px; width:35px; vertical-align: middle;">
                                <label class="custom-control-label" for="defaultChecked5">Discharge Folders Complete</label>
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" @if(!is_null($part_two))  disabled @if($part_two->personal_belongings==1)checked @endif @endif name="personal_belongings" value="1" id="defaultChecked5" style="height:35px; width:35px; vertical-align: middle;">
                                <label class="custom-control-label" for="defaultChecked5">Personal Belongings</label>
                            </div>

                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" @if(!is_null($part_two))  disabled @if($part_two->grievance==1)checked @endif @endif name="grievance" value="1" id="defaultChecked6" style="height:35px; width:35px; vertical-align: middle;">
                                <label class="custom-control-label" for="defaultChecked6">Grievance filed</label>
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" @if(!is_null($part_two))  disabled @if($part_two->work_order==1)checked @endif @endif name="work_order" value="1" id="defaultChecked7" style="height:35px; width:35px; vertical-align: middle;">
                                <label class="custom-control-label" for="defaultChecked7">Work order submitted</label>
                            </div>
                        </div>




                        <div class="well">
                            <ul class="list-group">
                                <li class="list-group-item list-group-item-success"><b>Our goal is to keep you informed, from 1-5, how well are we doing at explaining what is happening and how long things take?
                                        (Advocate notes the number indicated and any other open comments to be addressed. Observes and notes whiteboard use while patient speaks)</b></li>
                            </ul>
                            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled @if($part_two->informed==0)active @endif @endif ">
                                    <input type="radio" name="informed" value="0" id="option1" autocomplete="off"> N/A
                                </label>
                                <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled @if($part_two->informed==1)active @endif @endif  ">
                                    <input type="radio" name="informed" value="1" id="option2" autocomplete="off"> <b>1</b>
                                </label>
                                <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled @if($part_two->informed==2)active @endif @endif">
                                    <input type="radio" name="informed" value="2" id="option3" autocomplete="off"> <b>2</b>
                                </label>
                                <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled @if($part_two->informed==3)active @endif @endif ">
                                    <input type="radio" name="informed" value="3" id="option3" autocomplete="off"> <b>3</b>
                                </label>
                                <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled  @if($part_two->informed==4)active @endif @endif ">
                                    <input type="radio" name="informed" value="4" id="option3" autocomplete="off"> <b>4</b>
                                </label>
                                <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled  @if($part_two->informed==5)active @endif @endif ">
                                    <input type="radio" name="informed" value="5" id="option3" autocomplete="off"> <b>5</b>
                                </label>
                            </div>
                        </div>

                        <div class="well">
                            <ul class="list-group">
                                <li class="list-group-item list-group-item-success"><b>We want to be responsive 100% of the time.
                                        From 1-5 how well are we meeting this goal? (Advocate notes the number indicated and any other open comments to be addressed)</b></li>
                            </ul>
                            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled @if($part_two->responsive==0)active @endif @endif">
                                    <input type="radio" name="responsive" value="0" id="option1" autocomplete="off"> N/A
                                </label>
                                <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled @if($part_two->responsive==1)active @endif @endif">
                                    <input type="radio" name="responsive" value="1" id="option2" autocomplete="off"> <b>1</b>
                                </label>
                                <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled @if($part_two->responsive==2)active @endif @endif">
                                    <input type="radio" name="responsive" value="2" id="option3" autocomplete="off"> <b>2</b>
                                </label>
                                <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled @if($part_two->responsive==3)active @endif @endif">
                                    <input type="radio" name="responsive" value="3" id="option3" autocomplete="off"> <b>3</b>
                                </label>
                                <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled @if($part_two->responsive==4)active @endif @endif">
                                    <input type="radio" name="responsive" value="4" id="option3" autocomplete="off"> <b>4</b>
                                </label>
                                <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled @if($part_two->responsive==5)active @endif @endif">
                                    <input type="radio" name="responsive" value="5" id="option3" autocomplete="off"> <b>5</b>
                                </label>
                            </div>
                        </div>

                        <div class="well">
                            <ul class="list-group">
                                <li class="list-group-item list-group-item-success"><b>Our goal is to provide you with a clean environment.  On a scale of 1-5 how well are we meeting
                                    that goal.</b></li>
                            </ul>
                            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled @if($part_two->positioning==0)active @endif @endif">
                                    <input type="radio" name="positioning" value="0" id="option1" autocomplete="off"> N/A
                                </label>
                                <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled @if($part_two->positioning==1)active @endif @endif">
                                    <input type="radio" name="positioning" value="1" id="option2" autocomplete="off"> <b>1</b>
                                </label>
                                <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled @if($part_two->positioning==2)active @endif @endif ">
                                    <input type="radio" name="positioning" value="2" id="option3" autocomplete="off"> <b>2</b>
                                </label>
                                <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled @if($part_two->positioning==3)active @endif @endif">
                                    <input type="radio" name="positioning" value="3" id="option3" autocomplete="off"> <b>3</b>
                                </label>
                                <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled @if($part_two->positioning==4)active @endif @endif">
                                    <input type="radio" name="positioning" value="4" id="option3" autocomplete="off"> <b>4</b>
                                </label>
                                <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled @if($part_two->positioning==5)active @endif @endif">
                                    <input type="radio" name="positioning" value="5" id="option3" autocomplete="off"> <b>5</b>
                                </label>
                            </div>
                        </div>

                        <div class="well">
                            <ul class="list-group">
                                <li class="list-group-item list-group-item-success"><b>Is there a staff member that I can personally thank or recognize for you?
                                        Can you share with me what they did exceptionally well that you liked? (Advocate notes Y/N and if Y the open comments for needed recognition)</b></li>
                            </ul>
                            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled @if($part_two->recgonize_staff==0)active @endif @endif">
                                    <input type="radio" name="recgonize_staff" id="option2" value="0" autocomplete="off"> <b>No</b>
                                </label>
                                <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled @if($part_two->recgonize_staff==1)active @endif @endif">
                                    <input type="radio" name="recgonize_staff" id="option3" value="1" autocomplete="off"> <b>Yes - Comment Below</b>
                                </label>
                            </div>
                            <div class="form-group">
                                <label for="question_notes_id">Comments / Notes:</label>
                                <small id="emailHelp" class="form-text text-muted">Information about employee recognition.</small>
                                <textarea class="form-control" name="complement_text" id="question_notes_id" rows="3"  @if(!is_null($part_two))disabled @endif> @if(!is_null($part_two)) {{$part_two->complement_text}} @endif</textarea>
                            </div>
                        </div>

                        <div class="well">
                            <ul class="list-group">
                                <li class="list-group-item list-group-item-success"><b>Remind patient that they will also receive a survey in the mail.  (HCAHPS)</b></li>
                            </ul>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" @if(!is_null($part_two))  disabled @if($part_two->mail_survey==1)checked @endif @endif name="mail_survey" value="1" id="mail_survey2" style="height:35px; width:35px; vertical-align: middle;">
                                <label class="custom-control-label" for="mail_survey2">Patient was notified of survey in the mail.</label>
                            </div>
                        </div>

                        <div class="well">
                            <ul class="list-group">
                                <li class="list-group-item list-group-item-success"><b>Is there anything else I can do for you before I leave?</b></li>
                            </ul>
                            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled @if($part_two->anything_else==0)active @endif @endif">
                                    <input type="radio" name="anything_else" value="0" id="option2" autocomplete="off"> <b>No</b>
                                </label>
                                <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled @if($part_two->anything_else==1)active @endif @endif">
                                    <input type="radio" name="anything_else" value="1" id="option3" autocomplete="off"> <b>Yes</b>
                                </label>
                            </div>
                        </div>

                        <div class="well">
                            <div class="form-group">
                                <label for="question_notes_id">Comments / Notes:</label>
                                <small id="emailHelp" class="form-text text-muted">Additional information or comments.</small>
                                <textarea class="form-control" name="notes" id="question_notes_id" rows="3"  @if(!is_null($part_two))disabled @endif> @if(!is_null($part_two)) {{$part_two->notes}} @endif</textarea>
                            </div>
                        </div>
                            <input type="Submit" class="btn btn-primary btn-block"  @if(!is_null($part_two))disabled @endif value="Complete Part 2">
                        </form>
                    </div>
                    <div id="menu3" class="tab-pane fade">
                        <h3>ED</h3>
                        <p>If patient was admitted via the ED.  ED check.</p>
                    </div>
                    <div id="menu4" class="tab-pane fade">
                        <h3>Photo / Files</h3>
                        <p>Take a picture or upload a file.</p>
                        <form action="/pr/survey/file" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" value="{{$patient->pat_enc_csn_id}}" name="encounter_id">
                            <input type="hidden" value="{{ Auth::user()->employee_number }}" name="created_by">

                            <div class="form-group">
                                <label for="title_id">Select File:</label>
                                <small id="emailHelp" class="form-text text-muted">Attach files such as quotes.</small>
                                <input type="file" name="file_name" class="form-control" id="title_id">
                            </div>
                            <div class="form-group">
                                <label for="question_notes_id">File Description:</label>
                                <small id="emailHelp" class="form-text text-muted">(Describe what this file contains.)</small>
                                <textarea class="form-control" name="file_description" id="question_notes_id" rows="3" required></textarea>
                            </div>
                            <input type="Submit" class="btn btn-sm btn-sgmc" value="Upload File">
                        </form>
                        <br>
                        @if($files->isEmpty())
                            <div class="alert alert-info">No Files Attached</div>
                        @else
                            <table class="table">
                                <thead>
                                <tr>
                                    <td><b>File Name</b></td>
                                    <td><b>Description</b></td>
                                    <td><b>Type</b></td>
                                    <td><b>Size</b></td>
                                    <td><b>Added By</b></td>
                                    <td><b>Date</b></td>
                                    <td></td>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($files as $file)
                                    <tr>
                                        <td>{{$file->old_filename}}</td>
                                        <td>{{$file->file_description}}</td>
                                        <td>{{$file->file_mime}}</td>
                                        <td>{{$file->file_size}}</td>
                                        <td>{{$file->added_by->first_name}} {{$file->added_by->last_name}}</td>
                                        <td>{{$file->created_at}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        @endif
                    </div>
                    <div id="menu5" class="tab-pane fade">
                        <h3>Links</h3>
                        <div class="col-lg-3">

                            <div class="btn-group-vertical btn-block" role="group" aria-label="Vertical button group">
                                <a href="https://sgmc.tmsonline.com/webrequest/wrMaster.aspx?&p=1&d=lnIXKZnKCheKXuqzl2JN4Q==&s=iFeSFnR5TUlFQjOKd7ErAw==&logout=" target="_blank" class="btn btn-primary ">Engineering</a>
                                <a href="/pr/service_recovery" target="_blank" class="btn btn-primary ">Service Recovery</a>
                                <a href="http://vm-rl6-app/RL6_Prod/Homecenter/Client/Login.aspx?ReturnUrl=%2fRL6_Prod" target="_blank" class="btn btn-primary ">RL Solutions</a>
                                <a href="/search?search=" target="_blank" class="btn btn-primary ">Extra Mile</a>
                                <a href="https://helpdesk.sgmc.org/Login.jsp?navLanguage=en-US" target="_blank" class="btn btn-primary ">IS Help Ticket</a>
                            </div>
                        </div>

                    </div>

                    <div id="menu6" class="tab-pane fade">
                        <h3>Nurse</h3>
                        <div class="well">
                            <div class="form-group">
                                <label for="question_notes_id">We want to make sure you are truly prepared for discharge or the next stage of care when that time comes. What questions/concerns do you have about managing your health once you've left our unit? What questions/concerns do you have about your medications?</label>
                                <textarea class="form-control" name="part_one_notes" id="question_notes_id" rows="3" @if(!is_null($part_one)) checked disabled @endif></textarea>
                            </div>
                        </div>
                        <div class="well">
                            <div class="form-group">
                                <label for="question_notes_id">Tell me how our staff is doing in responding to your calls and addressing your needs?</label>
                                <textarea class="form-control" name="part_one_notes" id="question_notes_id" rows="3" @if(!is_null($part_one)) checked disabled @endif></textarea>
                            </div>
                        </div>
                        <div class="well">
                            <div class="form-group">
                                <label for="question_notes_id">Is there a staff member you would like for me to recognize for going above and beyond?</label>
                                <textarea class="form-control" name="part_one_notes" id="question_notes_id" rows="3" @if(!is_null($part_one)) checked disabled @endif></textarea>
                            </div>
                        </div>
                        <div class="well">
                            <div class="form-group">
                                <label for="question_notes_id">What questions or concerns do you have for me? What can I do for you before I leave?</label>
                                <textarea class="form-control" name="part_one_notes" id="question_notes_id" rows="3" @if(!is_null($part_one)) checked disabled @endif></textarea>
                            </div>
                        </div>
                        <div class="well">
                            <ul class="list-group">
                                <li class="list-group-item list-group-item-success"><b>Is the room clean?</b></li>
                            </ul>
                            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled @if($part_two->recgonize_staff==0)active @endif @endif">
                                    <input type="radio" name="recgonize_staff" id="option2" value="0" autocomplete="off"> <b>No</b>
                                </label>
                                <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled @if($part_two->recgonize_staff==1)active @endif @endif">
                                    <input type="radio" name="recgonize_staff" id="option3" value="1" autocomplete="off"> <b>Yes</b>
                                </label>
                            </div>
                        </div>
                        <div class="well">
                            <ul class="list-group">
                                <li class="list-group-item list-group-item-success"><b>Discharge Folder?</b></li>
                            </ul>
                            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled @if($part_two->recgonize_staff==0)active @endif @endif">
                                    <input type="radio" name="recgonize_staff" id="option2" value="0" autocomplete="off"> <b>No</b>
                                </label>
                                <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled @if($part_two->recgonize_staff==1)active @endif @endif">
                                    <input type="radio" name="recgonize_staff" id="option3" value="1" autocomplete="off"> <b>Yes</b>
                                </label>
                            </div>
                        </div>
                        <div class="well">
                            <ul class="list-group">
                                <li class="list-group-item list-group-item-success"><b>POC Board?</b></li>
                            </ul>
                            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled @if($part_two->recgonize_staff==0)active @endif @endif">
                                    <input type="radio" name="recgonize_staff" id="option2" value="0" autocomplete="off"> <b>No</b>
                                </label>
                                <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled @if($part_two->recgonize_staff==1)active @endif @endif">
                                    <input type="radio" name="recgonize_staff" id="option3" value="1" autocomplete="off"> <b>Yes</b>
                                </label>
                            </div>
                        </div>
                        <div class="well">
                            <ul class="list-group">
                                <li class="list-group-item list-group-item-success"><b>Med Side Effects?</b></li>
                            </ul>
                            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled @if($part_two->recgonize_staff==0)active @endif @endif">
                                    <input type="radio" name="recgonize_staff" id="option2" value="0" autocomplete="off"> <b>No</b>
                                </label>
                                <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled @if($part_two->recgonize_staff==1)active @endif @endif">
                                    <input type="radio" name="recgonize_staff" id="option3" value="1" autocomplete="off"> <b>Yes</b>
                                </label>
                            </div>
                        </div>
                        <div class="well">
                            <ul class="list-group">
                                <li class="list-group-item list-group-item-success"><b>Fall Bundle?</b></li>
                            </ul>
                            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled @if($part_two->recgonize_staff==0)active @endif @endif">
                                    <input type="radio" name="recgonize_staff" id="option2" value="0" autocomplete="off"> <b>No</b>
                                </label>
                                <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled @if($part_two->recgonize_staff==1)active @endif @endif">
                                    <input type="radio" name="recgonize_staff" id="option3" value="1" autocomplete="off"> <b>Yes</b>
                                </label>
                            </div>
                        </div>
                        <div class="well">
                            <ul class="list-group">
                                <li class="list-group-item list-group-item-success"><b>Foley?</b></li>
                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                    <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled @if($part_two->recgonize_staff==0)active @endif @endif">
                                        <input type="radio" name="recgonize_staff" id="option2" value="0" autocomplete="off"> <b>No</b>
                                    </label>
                                    <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled @if($part_two->recgonize_staff==1)active @endif @endif">
                                        <input type="radio" name="recgonize_staff" id="option3" value="1" autocomplete="off"> <b>Yes</b>
                                    </label>
                                </div>
                            </ul>

                            <ul class="list-group">
                                <li class="list-group-item list-group-item-success"><b>Foley meets Criteria for Removal?</b></li>
                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                    <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled @if($part_two->recgonize_staff==0)active @endif @endif">
                                        <input type="radio" name="recgonize_staff" id="option2" value="0" autocomplete="off"> <b>No</b>
                                    </label>
                                    <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled @if($part_two->recgonize_staff==1)active @endif @endif">
                                        <input type="radio" name="recgonize_staff" id="option3" value="1" autocomplete="off"> <b>Yes</b>
                                    </label>
                                </div>
                            </ul>

                            <ul class="list-group">
                                <li class="list-group-item list-group-item-success"><b>Foley Bundle Criteria:</b></li>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" @if(!is_null($part_two))  disabled @if($part_two->information_current==1)checked @endif @endif name="information_current" value="1" id="defaultChecked2" style="height:35px; width:35px; vertical-align: middle;">
                                    <label class="custom-control-label" for="defaultChecked2">Secure w/Statlock</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" @if(!is_null($part_two))  disabled @if($part_two->not_in_room==1)checked @endif @endif name="not_in_room" value="1" id="defaultChecked3" style="height:35px; width:35px; vertical-align: middle;">
                                    <label class="custom-control-label" for="defaultChecked3">No Kinks</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input"  @if(!is_null($part_two))  disabled @if($part_two->declined==1)checked @endif @endif name="declined" value="1" id="defaultChecked4" style="height:35px; width:35px; vertical-align: middle;">
                                    <label class="custom-control-label" for="defaultChecked4">Not Touching Floor/Below Hips</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" @if(!is_null($part_two))  disabled @if($part_two->family==1)checked @endif @endif name="family" value="1" id="defaultChecked5" style="height:35px; width:35px; vertical-align: middle;">
                                    <label class="custom-control-label" for="defaultChecked5"><3/4 Full</label>
                                </div>
                            </ul>
                        </div>

                        <div class="well">
                            <ul class="list-group">
                                <li class="list-group-item list-group-item-success"><b>Central Line</b></li>
                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                    <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled @if($part_two->recgonize_staff==0)active @endif @endif">
                                        <input type="radio" name="recgonize_staff" id="option2" value="0" autocomplete="off"> <b>No</b>
                                    </label>
                                    <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled @if($part_two->recgonize_staff==1)active @endif @endif">
                                        <input type="radio" name="recgonize_staff" id="option3" value="1" autocomplete="off"> <b>Yes</b>
                                    </label>
                                </div>
                            </ul>

                            <ul class="list-group">
                                <li class="list-group-item list-group-item-success"><b>Central Line meets Criteria for Removal?</b></li>
                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                    <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled @if($part_two->recgonize_staff==0)active @endif @endif">
                                        <input type="radio" name="recgonize_staff" id="option2" value="0" autocomplete="off"> <b>No</b>
                                    </label>
                                    <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled @if($part_two->recgonize_staff==1)active @endif @endif">
                                        <input type="radio" name="recgonize_staff" id="option3" value="1" autocomplete="off"> <b>Yes</b>
                                    </label>
                                </div>
                            </ul>

                            <ul class="list-group">
                                <li class="list-group-item list-group-item-success"><b>Location / Type?</b></li>
                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                    <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled @if($part_two->recgonize_staff==0)active @endif @endif">
                                        <input type="radio" name="recgonize_staff" id="option2" value="0" autocomplete="off"> <b>No</b>
                                    </label>
                                    <label class="btn btn-lg btn-default @if(!is_null($part_two))disabled @if($part_two->recgonize_staff==1)active @endif @endif">
                                        <input type="radio" name="recgonize_staff" id="option3" value="1" autocomplete="off"> <b>Yes</b>
                                    </label>
                                </div>
                            </ul>

                            <ul class="list-group">
                                <li class="list-group-item list-group-item-success"><b>CL Bundle Criteria:</b></li>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" @if(!is_null($part_two))  disabled @if($part_two->information_current==1)checked @endif @endif name="information_current" value="1" id="defaultChecked2" style="height:35px; width:35px; vertical-align: middle;">
                                    <label class="custom-control-label" for="defaultChecked2">Site:  No S/S of infection</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" @if(!is_null($part_two))  disabled @if($part_two->not_in_room==1)checked @endif @endif name="not_in_room" value="1" id="defaultChecked3" style="height:35px; width:35px; vertical-align: middle;">
                                    <label class="custom-control-label" for="defaultChecked3">Dressing:   Dry, Intact, No Gauze & Dated</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input"  @if(!is_null($part_two))  disabled @if($part_two->declined==1)checked @endif @endif name="declined" value="1" id="defaultChecked4" style="height:35px; width:35px; vertical-align: middle;">
                                    <label class="custom-control-label" for="defaultChecked4">Biopatch:  Correct</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" @if(!is_null($part_two))  disabled @if($part_two->family==1)checked @endif @endif name="family" value="1" id="defaultChecked5" style="height:35px; width:35px; vertical-align: middle;">
                                    <label class="custom-control-label" for="defaultChecked5">Tubing:   Date/Time</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" @if(!is_null($part_two))  disabled @if($part_two->family==1)checked @endif @endif name="family" value="1" id="defaultChecked5" style="height:35px; width:35px; vertical-align: middle;">
                                    <label class="custom-control-label" for="defaultChecked5">Tubing:   Capped Correctly</label>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" @if(!is_null($part_two))  disabled @if($part_two->family==1)checked @endif @endif name="family" value="1" id="defaultChecked5" style="height:35px; width:35px; vertical-align: middle;">
                                    <label class="custom-control-label" for="defaultChecked5">Hub: Alcohol Cap</label>
                                </div>
                            </ul>
                        </div>
                        <div class="well">
                            <div class="form-group">
                                <label for="question_notes_id">Daily Bath:</label>
                                <textarea class="form-control" name="part_one_notes" id="question_notes_id" rows="3" @if(!is_null($part_one)) checked disabled @endif></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {!! Html::script('js/randomColor.js') !!}
    {!! Html::script('js/confettiKit.js') !!}
@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">
    //Happy Birthday

    @if(!is_null($patient->dob))
    @if($patient->dob->format('m-d') == \Carbon\Carbon::now()->format('m-d'))
        $(window).load(function(e){

            new confettiKit({
                confettiCount: 40,
                angle: 90,
                startVelocity: 50,
                colors: randomColor({hue: 'blue',count: 18}),
                elements: {
                    'confetti': {
                        direction: 'down',
                        rotation: true,
                    },
                    'star': {
                        count: 10,
                        direction: 'down',
                        rotation: true,
                    },
                    'ribbon': {
                        count: 5,
                        direction: 'down',
                        rotation: true,
                    },
                    'custom': [{
                        count: 1,
                        width: 50,
                        textSize: 15,
                        content: '/img/ballon.png',
                        contentType: 'image',
                        direction: 'up',
                        rotation: false,
                    }]
                },
                position: 'bottomLeftRight',
            });
        });
    @endif
    @endif





    //Happy Birthday button
    document.querySelector('#bottom-btn').addEventListener('click', function (e) {

        new confettiKit({
            confettiCount: 40,
            angle: 90,
            startVelocity: 50,
            colors: randomColor({hue: 'blue',count: 18}),
            elements: {
                'confetti': {
                    direction: 'down',
                    rotation: true,
                },
                'star': {
                    count: 10,
                    direction: 'down',
                    rotation: true,
                },
                'ribbon': {
                    count: 5,
                    direction: 'down',
                    rotation: true,
                },
                'custom': [{
                    count: 1,
                    width: 50,
                    textSize: 15,
                    content: '/img/ballon.png',
                    contentType: 'image',
                    direction: 'up',
                    rotation: false,
                }]
            },
            position: 'bottomLeftRight',
        });
    });
</script>


@endsection
@endif