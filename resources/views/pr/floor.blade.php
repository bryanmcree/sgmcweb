{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Patient Relations') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.nonav')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <b>{{$patients->first()->dept}} - ({{$patients->count()}})</b>
            </div>
            <div class="panel-body">
                <p>
                    <a href="/pr" class="btn btn-primary btn-lg btn-block">Select New Floor</a>
                </p>

                <div class="col-lg-12">
                    <div class="row">
                        <div class="btn-group-vertical center-block">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td><b>Name</b></td>
                                        <td class="hidden-xs hidden-sm"><b>MRN</b></td>
                                        <td><b>Room</b></td>
                                        <td><b>Gone</b></td>
                                        <td class="hidden-xs"><b>Days</b></td>
                                        <td class="hidden-xs hidden-sm"><b>Floor</b></td>
                                        <td class="hidden-xs"><b>P1</b></td>
                                        <td class="hidden-xs"><b>P2</b></td>
                                        <td><b></b></td>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($patients as $patient)
                                    <tr @if($patient->gone =='Y') bgcolor="gray" @endif @if($patient->check_back == 1) bgcolor="#adff2f" @endif>
                                        <td style="vertical-align : middle;">@if($patient->check_back == 1)<i class="fa fa-spinner fa-pulse fa-fw"></i> @endif {{$patient->pat_name}}</td>
                                        <td class="hidden-xs hidden-sm" style="vertical-align : middle;">{{$patient->mrn}}</td>
                                        <td style="vertical-align : middle;">{{$patient->room}}</td>
                                        <td style="vertical-align : middle;">@if($patient->gone =='Y')<a href="/pr/survey/ir/{{$patient->pat_enc_csn_id}}?floor={{$patient->dept}}" class="btn btn-success  ">Gone</a> @Else <a href="/pr/survey/nir/{{$patient->pat_enc_csn_id}}?floor={{$patient->dept}}" class="btn btn-danger  ">DC/TR</a> @endif</td>
                                        <td class="hidden-xs" style="vertical-align : middle;">{{\Carbon\Carbon::createFromTimeStamp(strtotime($patient->admission))->diffForHumans()}}</td>
                                        <td class="hidden-xs hidden-sm" style="vertical-align : middle;">{{$patient->dept}}</td>
                                        <td class="hidden-xs" style="vertical-align : middle;">@if(!is_null($patient->PartOne)) <i class="fas fa-check"></i>@endif</td>
                                        <td class="hidden-xs" style="vertical-align : middle;">@if(!is_null($patient->PartTwo)) <i class="fas fa-check"></i>@endif</td>
                                        <td style="vertical-align : middle;" align="right"><a href="/pr/survey/{{$patient->pat_enc_csn_id}}" class="btn btn-primary ">Select</a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>


                        </div>

                    </div>

                </div>




            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
@endif