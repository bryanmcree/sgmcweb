{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Patient Relations') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="card bg-dark text-white mb-3">
            <div class="card card-header">
                <b>Current Month Stats</b> <br> <a href="#"  class="btn btn-xs btn-primary pull-right" data-toggle="modal" data-target="#myModal">Filter Results</a>
            </div>
            <div class="card-body">

                <div class="card-deck">
                    <div class="card bg-dark text-white mb-3 border-info col-lg-6">
                        <div class="card-body">
                            <canvas id="bar-chart"></canvas>
                            <p><i>Total number of patients in bed compared to the number and type of check for current month.</i></p>
                        </div>
                    </div>
                    <div class="card bg-dark text-white mb-3 border-info col-lg-6">
                        <div class="card-body">
                            <canvas id="by_employee_two"></canvas>
                            <form method="post" action="/pr/reports/surveysbyemployeebyfloor" target="_blank">
                                {{ csrf_field() }}
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Select Employee:</span>
                                    </div>
                                    <select class="form-control input-sm" name="employee" id="exampleFormControlSelect1">
                                        @foreach($emp_list as $employee)
                                            <option value="{{$employee->emp}}">{{$employee->completedBy->name}}</option>
                                        @endforeach
                                    </select>
                                    <span class="input-group-btn">
                                        <input type="submit" class="btn btn-primary ml-2">
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="card bg-dark text-white mb-3 border-info col-lg-12">
                    <div class="card-body">
                        <canvas id="patients_floor"></canvas>
                        <p><i>Patients by floor for current month by admission date.</i></p>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <div class="col-lg-12">
        <div class="card bg-dark text-white mb-3">
            <div class=" card card-header">
                <b>Part One</b>
            </div>
            <div class="card-body">
                @if($part_one->isEmpty())
                    <div class="alert alert-info">There is no data for this month.</div>
                @else
                <div class="card-deck">
                    <div class="card bg-dark text-white mb-3 border-info">
                        <div class="card-body" style="height:150px;">
                            <div style="text-align: center"><b>Entrance / Introduction</b></div>
                            <div style="text-align: center; font-size: 25px; color: red; font-weight: bold">{{Round(($part_one->sum('entrance_introduction')/ $part_one->count())*100,2)}}%</div>
                            <div>{{$part_one->sum('entrance_introduction')}} patients out of {{$part_one->count()}} recieved introduction.</div>
                        </div>
                    </div>
                    <div class="card bg-dark text-white mb-3 border-info">
                        <div class="card-body" style="height:150px;">
                            <div style="text-align: center"><b>Review Controls</b></div>
                            <div style="text-align: center; font-size: 25px; color: red; font-weight: bold">{{Round(($part_one->sum('review_controls')/ $part_one->count())*100,2)}}%</div>
                            <div>{{$part_one->sum('review_controls')}} patients out of {{$part_one->count()}} reviewed controls with.</div>
                        </div>
                    </div>
                    <div class="card bg-dark text-white mb-3 border-info">
                        <div class="card-body" style="height:150px;">
                            <div style="text-align: center"><b>Set Expectations</b></div>
                            <div style="text-align: center; font-size: 25px; color: red; font-weight: bold">{{Round(($part_one->sum('set_expectations')/ $part_one->count())*100,2)}}%</div>
                            <div>{{$part_one->sum('set_expectations')}} patients out of {{$part_one->count()}} were notified of expectations.</div>
                        </div>
                    </div>
                </div> <!-- CARD DECK END -->
                <div class="card-deck">
                    <div class="card bg-dark text-white mb-3 border-info">
                        <div class="card-body" style="height:150px;">
                            <div style="text-align: center"><b>Focus Areas</b></div>
                            <div style="text-align: center; font-size: 25px; color: red; font-weight: bold">{{Round(($part_one->sum('focus_areas')/ $part_one->count())*100,2)}}%</div>
                            <div>{{$part_one->sum('focus_areas')}} patients out of {{$part_one->count()}} were advised of focus areas.</div>
                        </div>
                    </div>
                    <div class="card bg-dark text-white mb-3 border-info">
                        <div class="card-body" style="height:150px;">
                            <div style="text-align: center"><b>Left Card</b></div>
                            <div style="text-align: center; font-size: 25px; color: red; font-weight: bold">{{Round(($part_one->sum('left_card')/ $part_one->count())*100,2)}}%</div>
                            <div>{{$part_one->sum('left_card')}} patients out of {{$part_one->count()}} received a card.</div>
                        </div>
                    </div>
                </div> <!-- CARD DECK END -->
                    <div class="card bg-dark text-white mb-3 border-info col-lg-10 m-auto">
                        <div class="card-body" style="height:150px; overflow-y: auto;">
                            <table class="table table-condensed">
                                <thead>
                                <tr>
                                    <td><b>Dept</b></td>
                                    <td><b>Employee</b></td>
                                    <td><b>Patient</b></td>
                                    <td><b>Comments</b></td>
                                    <td></td>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($part_one as $comments1)
                                    <tr>
                                        <td nowrap="">{{$comments1->patient->dept}}</td>
                                        <td nowrap="">{{$comments1->completedBy->name}}</td>
                                        <td nowrap="">{{$comments1->patient->pat_name}}</td>
                                        <td>{{$comments1->part_one_notes}}</td>
                                        <td><a href="/pr/survey/{{$comments1->encounter_id}}" target="_blank" class="btn btn-sm btn-primary ">View</a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>


    <div class="col-lg-12">
        <div class="card bg-dark text-white mb-3">
            <div class="card card-header">
                <b>Part Two</b>
            </div>
            <div class="card-body">
                @if($part_two->isEmpty())
                    <div class="alert alert-info">There is no data for this month.</div>
                @else
            <div class="card-deck">
                <div class="card bg-dark text-white mb-3 border-info">
                    <div class="card-body">
                        <div style="text-align: center"><b>Whiteboard Current</b></div>
                        <div style="text-align: center; font-size: 25px; color: red; font-weight: bold">{{Round(($part_two->sum('information_current')/ $part_two->count())*100,2)}}%</div>
                        <div>{{$part_two->sum('information_current')}} out of {{$part_two->count()}} patients had information that was current.</div>
                    </div>
                </div>
                <div class="card bg-dark text-white mb-3 border-info">
                    <div class="card-body">
                        <div style="text-align: center"><b>Patient Not in Room</b></div>
                        <div style="text-align: center; font-size: 25px; color: red; font-weight: bold">{{Round(($part_two->sum('not_in_room')/ $part_two->count())*100,2)}}%</div>
                        <div>{{$part_two->sum('not_in_room')}} out of {{$part_two->count()}} patients were not present in the room.</div>
                    </div>
                </div>
                <div class="card bg-dark text-white mb-3 border-info">
                    <div class="card-body">
                        <div style="text-align: center"><b>Patient Declined Check</b></div>
                        <div style="text-align: center; font-size: 25px; color: red; font-weight: bold">{{Round(($part_two->sum('declined')/ $part_two->count())*100,2)}}%</div>
                        <div>{{$part_two->sum('declined')}} out of {{$part_two->count()}} patients declined to participate.</div>
                    </div>
                </div>
                <div class="card bg-dark text-white mb-3 border-info">
                    <div class="card-body">
                        <div style="text-align: center"><b>Family Completed Check</b></div>
                        <div style="text-align: center; font-size: 25px; color: red; font-weight: bold">{{Round(($part_two->sum('family')/ $part_two->count())*100,2)}}%</div>
                        <div>{{$part_two->count()-$part_two->sum('family')}} out of {{$part_two->count()}} patients had family answer the questions.</div>
                    </div>
                </div>
            </div>
            <div class="card-deck">
                <div class="card bg-dark text-white mb-3 border-info">
                    <div class="card-body">
                        <div style="text-align: center"><b>Grievance Filed</b></div>
                        <div style="text-align: center; font-size: 25px; color: red; font-weight: bold">{{Round(($part_two->sum('grievance')/ $part_two->count())*100,2)}}%</div>
                        <div>{{$part_two->sum('grievance')}} out of {{$part_two->count()}} patients filed a formal grievance.</div>
                    </div>
                </div>
                <div class="card bg-dark text-white mb-3 border-info">
                    <div class="card-body">
                        <div style="text-align: center"><b>Work Order Submitted</b></div>
                        <div style="text-align: center; font-size: 25px; color: red; font-weight: bold">{{Round(($part_two->sum('work_order')/ $part_two->count())*100,2)}}%</div>
                        <div>{{$part_two->sum('work_order')}} out of {{$part_two->count()}} patients submitted a work order.</div>
                    </div>
                </div>
                <div class="card bg-dark text-white mb-3 border-info">
                    <div class="card-body">
                        <div style="text-align: center"><b>Discharge Folders Complete</b></div>
                        <div style="text-align: center; font-size: 25px; color: red; font-weight: bold">{{Round(($part_two->sum('discharge_folders')/ $part_two->count())*100,2)}}%</div>
                        <div>{{$part_two->sum('discharge_folders')}} out of {{$part_two->count()}} patients discharge folders were complete.</div>
                    </div>
                </div>
                <div class="card bg-dark text-white mb-3 border-info">
                    <div class="card-body">
                        <div style="text-align: center"><b>Personal Belongings</b></div>
                        <div style="text-align: center; font-size: 25px; color: red; font-weight: bold">{{Round(($part_two->sum('personal_belongings')/ $part_two->count())*100,2)}}%</div>
                        <div>{{$part_two->sum('personal_belongings')}} out of {{$part_two->count()}} patients.</div>
                    </div>
                </div>
            </div> <!-- CARD DECK END -->


            <div class="card-deck">
                <div class="card bg-dark text-white mb-3 border-info">
                    <div class="card-body">
                        <canvas id="informed"></canvas>
                        <div style="text-align: center; font-size: 20px; color: darkgreen;">{{Round(($part_two->where('informed',5)->count('informed')/ (($part_two->count())))*100,2)}}%</div>
                    </div>
                </div>
                <div class="card bg-dark text-white mb-3 border-info">
                    <div class="card-body">
                        <canvas id="responsive"></canvas>
                        <div style="text-align: center; font-size: 20px; color: darkgreen;">{{Round(($part_two->where('responsive',5)->count('responsive')/ (($part_two->count() - $part_two->where('responsive',Null)->count())))*100,2)}}%</div>
                    </div>
                </div>
            </div> <!-- CARD DECK END -->
            <div class="card-deck">
                <div class="card bg-dark text-white mb-3 border-info">
                    <div class="card-body">
                        <canvas id="positioning"></canvas>
                        <div style="text-align: center; font-size: 20px; color: darkgreen;">{{Round(($part_two->where('positioning',5)->count('positioning')/ (($part_two->count() - $part_two->where('positioning',Null)->count())))*100,2)}}%</div>
                    </div>
                </div>
                <div class="card bg-dark text-white mb-3 border-info">
                    <div class="card-body">
                        <canvas id="recgonize_staff"></canvas>
                        <div style="text-align: center; font-size: 20px; color: darkgreen;">{{Round(($part_two->sum('recgonize_staff')/ (($part_two->count() - $part_two->where('recgonize_staff',Null)->count())*1))*100,2)}}%</div>
                    </div>
                </div>
                <div class="card bg-dark text-white mb-3 border-info">
                    <div class="card-body">
                        <canvas id="anything_else"></canvas>
                        <div style="text-align: center; font-size: 20px; color: darkgreen;">{{Round(($part_two->where('anything_else',0)->count('anything_else')/ (($part_two->count() - $part_two->where('anything_else',Null)->count())*1))*100,2)}}%</div>
                    </div>
                </div>
            </div> <!-- CARD DECK END -->
                <div class="card bg-dark text-white mb-3 border-info col-lg-10 m-auto">
                    <div class="card-body" style="height:500px; overflow-y: auto;">
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <td><b>Dept</b></td>
                                    <td><b>Date</b></td>
                                    <td><b>Employee</b></td>
                                    <td><b>Patient</b></td>
                                    <td><b>Comments</b></td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($part_two as $comments)
                                <tr>
                                    <td nowrap="">{{$comments->patient->dept}}</td>
                                    <td nowrap="">{{$comments->created_at}}</td>
                                    <td nowrap="">{{$comments->completedBy->name}}</td>
                                    <td nowrap="">{{$comments->patient->pat_name}}</td>
                                    <td>{{$comments->notes}}</td>
                                    <td><a href="/pr/survey/{{$comments->encounter_id}}" target="_blank" class="btn btn-sm btn-primary ">View</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
@include("pr.modals.filter")
@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">
    Chart.defaults.global.defaultFontColor = '#fff';

    // Total Patients vs Part One and Two
    new Chart(document.getElementById("bar-chart"), {
        type: 'bar',
        data: {
            labels: ["Total Patients", "Part One", "Part Two"],
            datasets: [
                {
                    label: "Patients / Checks",
                    data: [{{$patients->count()}},{{$part_one->count()}},{{$part_two->count()}}],
                    backgroundColor: [
                        'rgba(0, 99, 132, 0.6)',
                        'rgba(30, 99, 132, 0.6)',
                        'rgba(60, 99, 132, 0.6)',
                        'rgba(90, 99, 132, 0.6)',
                        'rgba(120, 99, 132, 0.6)',
                        'rgba(130, 99, 132, 0.6)',
                        'rgba(140, 99, 132, 0.6)',
                        'rgba(150, 99, 132, 0.6)',
                    ],
                }
            ]
        },
        options: {
            legend: { display: false },
            backgroundColor: "#F5DEB3",
            title: {
                display: true,
                text: 'Patients in bed vs number of checks'
            }
        }
    });


    // Checks by employee
    var URL = 'https://developer.mozilla.org/';
    new Chart(document.getElementById("by_employee_two"), {
        type: 'bar',

        data: {
            labels: [
                @foreach($by_employee_2 as $employee1)
                "{{$employee1->completedBy->name}}",
                @endforeach

            ],
            datasets: [

                {
                    label: "Part Two",
                    data: [

                        @foreach($by_employee_2 as $employee2)
                        {{$employee2->total}},
                        @endforeach

                    ],
                    backgroundColor: [
                        'rgba(0, 99, 132, 0.6)',
                        'rgba(30, 99, 132, 0.6)',
                        'rgba(60, 99, 132, 0.6)',
                        'rgba(90, 99, 132, 0.6)',
                        'rgba(120, 99, 132, 0.6)',
                        'rgba(130, 99, 132, 0.6)',
                        'rgba(140, 99, 132, 0.6)',
                        'rgba(150, 99, 132, 0.6)',
                    ],
                }
            ]
        },
        options: {
            legend: { display: false },
            title: {
                display: true,
                text: 'Number of part two checks by each employee.'
            },
            tooltips: {
                mode: 'index',
                callbacks: {
                    label: function(tooltipItem) {
                        return + Number(tooltipItem.yLabel) + " Surveys";
                    }
                }
            },
            onClick: function(e) {
                //var index = legendItem.datasetIndex;
                var ci = this.chart.lastTooltipActive;;
                //var meta = ci.getDatasetMeta(index);
                console.log(this.chart.item._index);
                //alert("clicked x-axis area: " + ci);
            }
        }
    });


    // Total Patients by Floor
    new Chart(document.getElementById("patients_floor"), {
        type: 'horizontalBar',
        data: {
            labels: [
                @foreach($patients_floor as $floor)
                '{{$floor->dept}}',
                @endforeach
            ],
            datasets: [
                {
                    label: "Patients",
                    //backgroundColor: ["#3e95cd"],
                    data: [
                        @foreach($patients_floor as $floor1)
                        {{$floor1->total}},
                        @endforeach
                        ],
                    backgroundColor: [
                        'rgba(0, 99, 132, 0.6)',
                        'rgba(10, 99, 132, 0.6)',
                        'rgba(20, 99, 132, 0.6)',
                        'rgba(30, 99, 132, 0.6)',
                        'rgba(40, 99, 132, 0.6)',
                        'rgba(50, 99, 132, 0.6)',
                        'rgba(60, 99, 132, 0.6)',
                        'rgba(70, 99, 132, 0.6)',
                        'rgba(80, 99, 132, 0.6)',
                        'rgba(90, 99, 132, 0.6)',
                        'rgba(100, 99, 132, 0.6)',
                        'rgba(110, 99, 132, 0.6)',
                        'rgba(120, 99, 132, 0.6)',
                        'rgba(130, 99, 132, 0.6)',
                        'rgba(140, 99, 132, 0.6)',
                        'rgba(150, 99, 132, 0.6)',
                    ],
                    borderColor: [
                        'rgba(0, 99, 132, 0.6)',
                        'rgba(10, 99, 132, 0.6)',
                        'rgba(20, 99, 132, 0.6)',
                        'rgba(30, 99, 132, 0.6)',
                        'rgba(40, 99, 132, 0.6)',
                        'rgba(50, 99, 132, 0.6)',
                        'rgba(60, 99, 132, 0.6)',
                        'rgba(70, 99, 132, 0.6)',
                        'rgba(80, 99, 132, 0.6)',
                        'rgba(90, 99, 132, 0.6)',
                        'rgba(100, 99, 132, 0.6)',
                        'rgba(110, 99, 132, 0.6)',
                        'rgba(120, 99, 132, 0.6)',
                        'rgba(130, 99, 132, 0.6)',
                        'rgba(140, 99, 132, 0.6)',
                        'rgba(150, 99, 132, 0.6)',
                    ],
                    borderWidth: 2,
                }
            ]
        },
        options: {
            legend: { display: false },
            title: {
                display: true,
                text: 'Patients by Floor'
            }
        }
    });

    // part two informed
    new Chart(document.getElementById("informed"), {
        type: 'pie',
        data: {
            labels: ["1", "2", "3", "4", "5","Not Answered"],
            datasets: [{
                label: "Scale 1-5",
                backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                data: [
                    {{$part_two->where('informed',1)->count()}},
                    {{$part_two->where('informed',2)->count()}},
                    {{$part_two->where('informed',3)->count()}},
                    {{$part_two->where('informed',4)->count()}},
                    {{$part_two->where('informed',5)->count()}},
                    {{$part_two->where('informed',Null)->count()}},
                ]
            }]
        },
        options: {
            title: {
                display: true,
                text: 'Patient Informed'
            }
        }
    });

    // part two responsive
    new Chart(document.getElementById("responsive"), {
        type: 'pie',
        data: {
            labels: ["1", "2", "3", "4", "5","Not Answered"],
            datasets: [{
                label: "Scale 1-5",
                backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                data: [
                    {{$part_two->where('responsive',1)->count()}},
                    {{$part_two->where('responsive',2)->count()}},
                    {{$part_two->where('responsive',3)->count()}},
                    {{$part_two->where('responsive',4)->count()}},
                    {{$part_two->where('responsive',5)->count()}},
                    {{$part_two->where('responsive',Null)->count()}},
                ]
            }]
        },
        options: {
            title: {
                display: true,
                text: 'Employee Responsive'
            }
        }
    });

    // Cleanliness
    new Chart(document.getElementById("positioning"), {
        type: 'pie',
        data: {
            labels: ["1", "2", "3", "4", "5","Not Answered"],
            datasets: [{
                label: "Scale 1-5",
                backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                data: [
                    {{$part_two->where('positioning',1)->count()}},
                    {{$part_two->where('positioning',2)->count()}},
                    {{$part_two->where('positioning',3)->count()}},
                    {{$part_two->where('positioning',4)->count()}},
                    {{$part_two->where('positioning',5)->count()}},
                    {{$part_two->where('positioning',Null)->count()}},
                ]
            }]
        },
        options: {
            title: {
                display: true,
                text: 'Cleanliness'
            }
        }
    });


    // recgonize staff
    new Chart(document.getElementById("recgonize_staff"), {
        type: 'pie',
        data: {
            labels: ["No", "Yes","Not Answered"],
            datasets: [{
                label: "Scale 1-5",
                backgroundColor: ["#3e95cd", "#8e5ea2"],
                data: [
                    {{$part_two->where('recgonize_staff',0)->count()}},
                    {{$part_two->where('recgonize_staff',1)->count()}},

                    {{$part_two->where('recgonize_staff',Null)->count()}},
                ]
            }]
        },
        options: {
            title: {
                display: true,
                text: 'Recognize Staff'
            }
        }
    });

    // recgonize staff
    new Chart(document.getElementById("anything_else"), {
        type: 'pie',
        data: {
            labels: ["No", "Yes","Not Answered"],
            datasets: [{
                label: "Scale 1-5",
                backgroundColor: ["#3e95cd", "#8e5ea2"],
                data: [
                    {{$part_two->where('anything_else',0)->count()}},
                    {{$part_two->where('anything_else',1)->count()}},

                    {{$part_two->where('anything_else',Null)->count()}},
                ]
            }]
        },
        options: {
            title: {
                display: true,
                text: 'Anything Else'
            }
        }
    });

</script>


@endsection
@endif