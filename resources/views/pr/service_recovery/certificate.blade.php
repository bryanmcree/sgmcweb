<table border="1" width="99%" align="center">
    <tr>
        <td height="99%">
            <table width="99%" align="center">
                <tr>
                    <td colspan="2">
                        <br>
                        <div align="center">{{ Html::image('img/rsz_sgmc_master_logo.png', 'alt', array( 'width' => 400, 'height' => 129 )) }}</div>
                        <div align="center"><h1>$10.00 Gift Card</h1></div>


                        <div align="center"><h3>Presented to</h3></div>
                        <div align="center"><h1>{{$presented_to->presented_to}}</h1></div>
                        <div align="center"><b><i>We value</i></b>
                                <br>Excellence in all we do
                                <br>Integrity and professional conduct
                                <br>Team spirit and individual initiative
                                <br>Efficiency and effectiveness
                                <br>Effective communication
                                <br>And accept responsibility to our community.</div>
                        <div align="center"><h3>{{$presented_to->cost_centers->style1}}</h3></div>
                        <div align="center"><h3>{{\Carbon\Carbon::now()}}</h3></div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><hr><br></td>
                </tr>
                <tr>
                    <td width="50%">Manager/Director Signature:____________________________________

                    <td align="right">{{ Html::image('img/certificateseal.png', 'alt', array( 'width' => 150, 'height' => 111 )) }}</td>
                </tr>
            </table>
        </td>
    </tr>


</table>