{{--New file Template--}}



    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <h2><b>Service Recovery</b></h2>
            </div>
            <div class="panel-body">
                <h3>I'm In!</h3>

                <form method="post" action="/pr/service_recovery/add" target="_blank">
                    {{ csrf_field() }}
                    <input type="hidden" value="{{ Auth::user()->employee_number }}" name="created_by">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="title_id">Present to:</label>
                                <small id="emailHelp" class="form-text text-muted">Name of the individual receiving the gift card.</small>
                                <input type="text" name="presented_to" class="form-control" id="title_id" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="question_type_id">Select Issuing Cost Center:</label>
                                <small id="emailHelp" class="form-text text-muted">Select the cost center to be billed for this gift card.</small>
                                <select class="form-control" id="question_type_id" name="cost_center" required>
                                    <option value="" selected>[Select Cost Center]</option>
                                    @foreach($cost_centers as $cc)
                                        <option value="{{$cc->cost_center}}">{{$cc->style1}}</option>
                                    @endforeach

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="question_type_id">Select Service Issue:</label>
                                <small id="emailHelp" class="form-text text-muted">Select the most appropiate service issue.</small>
                                <select class="form-control" id="question_type_id" name="service_issue" required>
                                    <option value="" selected>[Select Service Issue]</option>
                                    <option value="Staff Concern">Staff Concern (i.e. behavior)</option>
                                    <option value="Physician Concern ">Physician Concern  (i.e. behavior)</option>
                                    <option value="Process / Procedure">Process / Procedure</option>
                                    <option value="Environment of Care">Environment of Care</option>
                                    <option value="Food Services">Food Services</option>
                                    <option value="Quality of Care">Quality of Care (i.e. communication, method of care, etc.)</option>
                                    <option value="Other">Other</option>

                                </select>
                            </div>
                        </div>
                    </div>
                    <input type="Submit" class="btn btn-sm btn-sgmc" value="Print Certificate">
                </form>
            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
