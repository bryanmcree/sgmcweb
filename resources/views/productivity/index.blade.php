@extends('layouts.app')


@section('content')
@if ( Auth::user()->hasRole('Reports - Productivity') == FALSE)
@include('layouts.unauthorized')
@Else
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading-custom panel-heading">
                Productivity
            </div>
            <div class="panel-body">
                <div class="form-horizontal" style="align-items: center">
                    {!! Form::open(array('url' => '/productivity/chart', 'method' => 'POST', 'files' => true,'class' => 'form_control')) !!}
                    {!! Form::label('unit', 'Search for Cost Center:') !!}
                    <div class="input-group">
                        {!! Form::text('search', '', ['class' => 'form-control', 'id'=>'autocomplete','placeholder'=>'Search by number or description.','required']) !!}
                        {!! Form::hidden('unit', '', ['class' => 'form-control','id'=>'unit']) !!}
                        <span class="input-group-btn">
                    {!! Form::submit('Select and Continue', array('class' => 'btn btn-block btn-sgmc')) !!}
                            {!! Form::close() !!}
                        </span>

                    </div>
                </div>

                <br>
                <li class="list-group-item">
                    <b>Welcome to the productivity charts.</b><br>
                    <ul class="custom-bullet">
                        <li>
                            We are currently tracking <b>{{$totalcost}}</b> cost centers.
                        </li>
                            <li class="padding">
                                <b>{{$totalfixed}}</b>: Fixed
                            </li>
                            <li class="padding">
                                <b>{{$totaluos}}</b>: Unit of Service
                            </li>
                        <li>
                            There is currently <b>{{\Carbon\Carbon::createFromFormat('Y-m-d',$startdate)->diffInDays(\Carbon\Carbon::createFromFormat('Y-m-d',$enddate))}}</b> days of data to track and
                            <b>{{number_format($totalrec)}}</b> records.
                        </li>
                            <li class="padding">
                                <b>{{$startdate}}</b> through <b>{{$enddate}}</b>
                            </li>
                            <li class="padding">
                                Most charts are delayed by one day.
                            </li>
                        <li>
                            The chart to the right tracks how many employees are currently clocked in.
                            It logs data every minute and has <b>{{number_format($totalclockedin)}}</b> records.
                        </li>

                    </ul>
                </li>


            </div>
        </div>
    </div>

<div class="col-md-8">
    <div class="panel panel-default">
        <div class="panel-heading panel-heading-custom">
            <b>Current Employees Clocked In</b>
        </div>
            <div class="panel-body">

                <div class="col-md-12">
                    <div id="linechart"></div>
                </div>
            </div>
    </div>
</div>




@section('scripts')


    <script type="text/javascript">

        google.charts.load('current', {'packages':['corechart']});

        google.charts.setOnLoadCallback(drawChart);


        function drawChart() {

            var jsonData = $.ajax({
                url: "/productivity/clockedin",
                dataType:"json",
                async: false
            }).responseText;

            //var data = google.visualization.arrayToDataTable(jsonData);
            var data = new google.visualization.DataTable(jsonData);
            var options = {
                animation:{
                    startup: true,
                    duration: 1000,
                    easing: 'out'
                },
                chartArea: {
                    width:'90%',
                    height:'60%'
                },
                title:'Employees Currently Clocked In',
                crosshair: { trigger: 'both', color: 'black' },
                height:650,
                legend: {
                    position:'none'
                },
                vAxis: {
                    title: 'Number of Employees',
                    scaleType: 'mirrorLog'
                },

                pointSize: 7,
                series: {
                    0: {
                        color: 'blue',
                        pointShape: 'diamond'
                    }
                }
            };
            var chart = new google.visualization.AreaChart(document.getElementById('linechart'));

            chart.draw(data, options);

        }

        $('#autocomplete').autocomplete({
            serviceUrl: '/costcenter',
            dataType: 'json',
            type:'GET',
            width: 418,
            minChars:2,
            onSelect: function(suggestion) {
                //alert(suggestion.data);
                $("#unit").val(suggestion.data);
            }


        });

    </script>

@endsection









@endif
@endsection


