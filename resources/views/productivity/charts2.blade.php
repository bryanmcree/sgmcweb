
                        <div id="linechart"></div>



                        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
                        <script   src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"   integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="   crossorigin="anonymous"></script>
                        <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
                        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
                        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>



    <script type="text/javascript">
        var visitor = {!! $results !!};
        var list = {!! $results2 !!};
        console.log(visitor);
        google.charts.load('current', {'packages':['corechart','table']});

        google.charts.setOnLoadCallback(drawChart);

        google.charts.setOnLoadCallback(drawTable);

        function drawChart() {
            var data = google.visualization.arrayToDataTable(visitor);
            var options = {
                title:'{{$data3->cost_center_description}}',
                crosshair: { trigger: 'both', color: 'pink' },
                height:450,
                chartArea: {
                    width:'95%',
                    height:'60%'
                },
                legend:'top',
                hAxis: {


                },
                vAxis: {
                    title: '{{$data3->unit_measure}} / Paid Hours',
                    format: 'decimal',
                    scaleType: 'mirrorLog'
                },
                animation:{
                    startup: true,
                    duration: 1000,
                    easing: 'out'
                },
                pointSize: 7,
                series: {
                    0: {
                        color: 'blue',
                        pointShape: 'diamond'

                    },
                    3: {
                        type: "line",
                        pointsVisible: false,
                        enableInteractivity: false,
                        lineDashStyle: [1, 1],
                        color: '#e2431e'
                    },
                    2: {
                        type: "line",
                        pointsVisible: false,
                        enableInteractivity: false,
                        pointSize: 0,
                        lineDashStyle: [1, 1],
                        color: '#e2431e'
                    },
                    1: {
                        type: "line",
                        pointsVisible: false,
                        enableInteractivity: false,
                        pointSize: 0,
                        lineDashStyle: [14, 2, 2, 7],
                        color: 'green'
                    },
                    4: {
                        targetAxisIndex:4,
                        enableInteractivity: false,
                        type: "bars",
                        color: '#d9f2e6'
                    }

                }
            };
            var chart = new google.visualization.LineChart(document.getElementById('linechart'));

            // Wait for the chart to finish drawing before calling the getImageURI() method.
            google.visualization.events.addListener(chart, 'ready', function () {
                linechart.innerHTML = '<img src="' + chart.getImageURI() + '">';
                console.log(linechart.innerHTML);
            });

            chart.draw(data, options);

            google.visualization.events.addListener(chart, 'select', function() {
                // grab a few details before redirecting
                var selection = chart.getSelection();
                var row = selection[0].row;
                var col = selection[0].column;
                var run_date = data.getValue(row, 0);
                var hrs_uos = data.getValue(row, 1);
                var cost_center = {{$data3->cost_center}};
                var cost_center_description = '{{$data3->cost_center_description}}';
                $(".modal-title", "#formModel").text( cost_center_description +' ('+cost_center+')'  );
                $(".test", "#formModel").text( cost_center_description );
                $(".run_date", "#formModel").text( run_date );
                $(".hrs_uos", "#formModel").text( hrs_uos );
                $('#formModel').modal('show');

                //location.href = 'http://www.google.com?row=' + row + '&col=' + col + '&year=' + run_date + hrs_ous + cost_center;
                //alert("You selected :" + run_date);
                //$('#formModel').modal('show', function(e) {
                //$('#cost_center').val(cost_center);
                //$("#cost_center").html($(e.relatedTarget).val(cost_center));
                // Do some stuff w/ it.
                //});



            });

        }


        function drawTable() {
            var data = new google.visualization.arrayToDataTable(list);


            var table = new google.visualization.Table(document.getElementById('table_div'));



            table.draw(data, {showRowNumber: true, width: '100%', height: '100%'});

        }

    </script>
