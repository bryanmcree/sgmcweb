@extends('layouts.app')


@section('content')
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading panel-heading-custom">
                <b>Productivity</b>
            </div>
            <div class="panel-body">

                {!! Form::open(array('url' => '/productivity/chart', 'method' => 'POST', 'files' => true,'class' => 'form_control')) !!}
                <div class="input-group">
                    {!! Form::text('search', '', ['class' => 'form-control', 'id'=>'autocomplete','placeholder'=>'Search by number or description.','required']) !!}
                    {!! Form::hidden('unit', '', ['class' => 'form-control','id'=>'unit']) !!}
                    <span class="input-group-btn">
                    <input type="submit" name="submit_data" class="btn btn-sgmc" value="View Chart">
                        {!! Form::close() !!}
                        <a href = "#ChartOptions" data-toggle="modal" data-target=".ChartOptions" data-page="Inventory" class = "btn btn-sgmc" role = "button">Options</a>
                </span>
                </div>
                <div id="png"></div>
            </div>

            <div class="panel-body" style="height: calc(100vh - 225px); overflow-y: auto;">
                    <div class="col-md-12">
                        <div id="dashboard">
                            <div id="chart_div"></div>
                            <br><br>
                            <div align="center" id="control_div"></div>
                            <p><span id='dbgchart'></span></p>
                        </div>
                        <div id="png"></div>
                    </div>
            </div>
        </div>
    </div>
    <div id="png"></div>

@section('scripts')

    <script type="text/javascript">



        var jsonData = {!! $results !!};
        //console.log(visitor);

        google.charts.load('44', {
            callback: drawChart,
            packages: ['controls', 'corechart']
        });


        function drawChart() {
            var data = new google.visualization.DataTable(jsonData);

            var control = new google.visualization.ControlWrapper({
                controlType: 'ChartRangeFilter',
                containerId: 'control_div',
                state: {
                    range: {
                        start: new Date('{!! \Carbon\Carbon::now()->subDay(30)->format('m-d-Y') !!} '),
                        end:   new Date('{!! \Carbon\Carbon::now()->format('m-d-Y') !!}')
                    }
                },
                options: {
                    filterColumnLabel: 'Date',
                    ui: {
                        chartOptions: {
                            height: 100,
                            chartArea: {
                                width: '70%'
                            }
                        },

                    }
                }
            });

            var chart = new google.visualization.ChartWrapper({
                chartType: 'LineChart',
                containerId: 'chart_div',
                options: {
                    trendlines: { 0: {
                        visibleInLegend: false,
                        type: 'linear',
                        color: 'black',
                        lineWidth: 2,
                        //opacity: 0.1,
                        pointsVisible: false
                    } },
                    title:'{{$data3->cost_center_description}}',
                    crosshair: { trigger: 'both', color: 'black' },
                    height:495,
                    chartArea: {
                        width:'90%',
                        height:'80%'
                    },
                    legend:'top',
                    hAxis: {


                    },

                    vAxes: {0:{
                        title: '{{$data3->unit_measure}} / Hours',
                        format: 'decimal',
                        scaleType: 'mirrorLog',
                        viewWindowMode:'explicit',
                        gridlines:{ color:'transparent' },
                        viewWindow: { }
                    },4:{
                        minValue:0,
                        gridlines:{ color:'transparent' },
                        format:'currency'
                    }

                    },
                    pointSize: 7,

                    series: {
                        0: {
                            color: 'blue',
                            pointShape: 'diamond',
                            interpolateNulls: true

                        },
                        3: {
                            type: "line",
                            pointsVisible: false,
                            enableInteractivity: false,
                            lineDashStyle: [1, 1],
                            color: '#e2431e'
                        },
                        2: {
                            type: "line",
                            pointsVisible: false,
                            enableInteractivity: false,
                            pointSize: 0,
                            lineDashStyle: [1, 1],
                            color: '#e2431e'
                        },
                        1: {
                            type: "line",
                            pointsVisible: false,
                            enableInteractivity: false,
                            pointSize: 0,
                            lineDashStyle: [14, 2, 2, 7],
                            color: 'green'
                        },
                        //Overtime Axis
                        4: {
                            targetAxisIndex:4,
                            enableInteractivity: false,
                            type: "bars",
                            color: '#d9f2e6'
                        }

                    }

                }
            });



            //var chart = new google.visualization.LineChart(document.getElementById('linechart'));
            //chart.draw(data, options);

            var dash = new google.visualization.Dashboard(document.getElementById('dashboard'));
            dash.bind([control], [chart]);

                google.visualization.events.addListener(chart, 'ready', function() {
                    console.log(chart.getChart().getImageURI());
                    document.getElementById('png').innerHTML = '<a href="' + chart.getChart().getImageURI() + '">*Printable Version - Select date ranges then export to PNG file.</a>';
                });



            dash.draw(data);








            google.visualization.events.addListener(chart, 'select', function() {
                // grab a few details before redirecting
                var selection = chart.getSelection();
                var row = selection[0].row;
                var col = selection[0].column;
                var run_date = data.getValue(row, 0);
                var hrs_uos = data.getValue(row, 1);
                var cost_center = {{$data3->cost_center}};
                var cost_center_description = '{{$data3->cost_center_description}}';
                $(".modal-title", "#formModel").text( cost_center_description +' ('+cost_center+')'  );
                $(".test", "#formModel").text( cost_center_description );
                $(".run_date", "#formModel").text( run_date );
                $(".hrs_uos", "#formModel").text( hrs_uos );
                $('#formModel').modal('show');

                //location.href = 'http://www.google.com?row=' + row + '&col=' + col + '&year=' + run_date + hrs_ous + cost_center;
                //alert("You selected :" + run_date);
                //$('#formModel').modal('show', function(e) {
                //$('#cost_center').val(cost_center);
                //$("#cost_center").html($(e.relatedTarget).val(cost_center));
                // Do some stuff w/ it.
                //});



            });

        }




        $('#autocomplete').autocomplete({
            serviceUrl: '/costcenter',
            dataType: 'json',
            type:'GET',
            width: 418,
            minChars:2,
            onSelect: function(suggestion) {
                //alert(suggestion.data);
                $("#unit").val(suggestion.data);
            }


        });

        function ResetMean() {
            document.getElementById("newmean").value = '{{round($oldmean,2)}}';
        }

        function ResetLCL() {
            document.getElementById("resetLCL").value = 4;
        }

        function ResetUCL() {
            document.getElementById("resetUCL").value = 4;
        }






















    </script>


@endsection

@endsection


<!-- Modal -->


<div class="modal fade" id="formModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Run Date
                            </div>
                            <div class="panel-body">
                                <div class="run_date"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Hours/UOS
                            </div>
                            <div class="panel-body">
                                <div class="hrs_uos"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Submitted By
                            </div>
                            <div class="panel-body">
                                {!! Auth::user()->name !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Explanation
                            </div>
                            <div class="panel-body">
                                {!! Form::textarea('sys_description', null, ['class' => 'form-control', 'id'=>'sys_description','size' => '10x3']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>





<div class="modal fade ChartOptions" id="ChartOptions" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    {!! Form::open(array('url' => '/productivity/chart', 'method' => 'POST', 'files' => true,'class' => 'form_control')) !!}
    {!! Form::hidden('unit', $data3->cost_center, ['class' => 'form-control']) !!}
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header panel-heading-custom">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title" id="myModalLabel"><b>Chart Options</b></h2>
            </div>
            <div class="modal-body">
                    Changes here will be saved for the current call center.  Reset buttons will return calculated values to that field.
                <div class="row">
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Mean (CL)
                            </div>
                            <div class="panel-body">

                                <div class="input-group">
                                    {!! Form::text('newmean', round($data2,2), ['class' => 'form-control', 'id'=>'newmean']) !!}
                                    <span class="input-group-btn">
                                <button type="button" class="btn btn-default"  onclick="ResetMean();" >Reset</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Upper Control Limit Offset (UCL)
                            </div>
                            <div class="panel-body">
                                <div class="input-group">
                                    {!! Form::input('number','newucl', $uclValue, ['class' => 'form-control', 'id'=>'resetUCL','min'=>$uclValue-2, 'max'=>$uclValue+2]) !!}

                                    <span class="input-group-btn">
                                <button type="button" class="btn btn-default"  onclick="ResetUCL();">Reset</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Lower Control Limit Offset (LCL)
                            </div>
                            <div class="panel-body">
                                <div class="input-group">
                                    {!! Form::input('number','newlcl', $lclValue, ['class' => 'form-control', 'id'=>'resetLCL','min'=>$lclValue-2, 'max'=>$lclValue+2]) !!}
                                    <span class="input-group-btn">
                                <button type="button" class="btn btn-default"  onclick="ResetLCL();">Reset</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Target
                            </div>
                            <div class="panel-body">
                                    {!! Form::text('target', null, ['class' => 'form-control', 'id'=>'target','disabled', 'placeholder'=>'Still Testing']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Description
                            </div>
                            <div class="panel-body">
                                    {!! Form::text('desc', null, ['class' => 'form-control', 'id'=>'sys_description','disabled', 'placeholder'=>'Still Testing']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Ratio
                            </div>
                            <div class="panel-body">
                                <div class="col-md-6">
                                    {!! Form::text('newlcl', null, ['class' => 'form-control', 'id'=>'sys_description','disabled', 'placeholder'=>'Still Testing']) !!}
                                </div>
                                <div class="col-md-6">
                                    {!! Form::text('newlcl', null, ['class' => 'form-control', 'id'=>'sys_description','disabled', 'placeholder'=>'Still Testing']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-sgmc">Save and Redraw Chart</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>