<div id="png"></div>

    <div id="dashboard" >
        <div id="chart_div" style="visibility: hidden"></div>
        <div align="center" id="control_div" style="display: none"></div>
        <p><span id='dbgchart' style="display: none"></span></p>
    </div>



    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    <script   src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"   integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="   crossorigin="anonymous"></script>
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>


    <script type="text/javascript">



        var jsonData = {!! $results !!};
        //console.log(visitor);

        google.charts.load('44', {
            callback: drawChart,
            packages: ['controls', 'corechart']
        });


        function drawChart() {
            var data = new google.visualization.DataTable(jsonData);

            var control = new google.visualization.ControlWrapper({
                controlType: 'ChartRangeFilter',
                containerId: 'control_div',
                state: {
                    range: {
                        start: new Date('8/01/2016'),
                        end:   new Date('1/9/2017')
                    }
                },
                options: {
                    filterColumnLabel: 'Date',
                    ui: {
                        chartOptions: {
                            height: 100,
                            chartArea: {
                                width: '70%'
                            }
                        },

                    }
                }
            });

            var chart = new google.visualization.ChartWrapper({
                chartType: 'LineChart',
                containerId: 'chart_div',
                options: {
                    title:'{{$data3->cost_center_description}}',
                    crosshair: { trigger: 'both', color: 'black' },
                    height:625,
                    chartArea: {
                        width:'90%',
                        height:'80%'
                    },
                    legend:'top',
                    hAxis: {


                    },
                    vAxes: {0:{
                        title: '{{$data3->unit_measure}} / Hours',
                        format: 'decimal',
                        scaleType: 'mirrorLog',
                        viewWindowMode:'explicit',
                        gridlines:{ color:'transparent' },
                        viewWindow: { }
                    },4:{
                        minValue:0,
                        gridlines:{ color:'transparent' },
                        format:'currency'
                    }

                    },
                    pointSize: 7,

                    series: {
                        0: {
                            color: 'blue',
                            pointShape: 'diamond',
                            interpolateNulls: true

                        },
                        3: {
                            type: "line",
                            pointsVisible: false,
                            enableInteractivity: false,
                            lineDashStyle: [1, 1],
                            color: '#e2431e'
                        },
                        2: {
                            type: "line",
                            pointsVisible: false,
                            enableInteractivity: false,
                            pointSize: 0,
                            lineDashStyle: [1, 1],
                            color: '#e2431e'
                        },
                        1: {
                            type: "line",
                            pointsVisible: false,
                            enableInteractivity: false,
                            pointSize: 0,
                            lineDashStyle: [14, 2, 2, 7],
                            color: 'green'
                        },
                        //Overtime Axis
                        4: {
                            targetAxisIndex:4,
                            enableInteractivity: false,
                            type: "bars",
                            color: '#d9f2e6'
                        }

                    }

                }
            });



            //var dash = new google.visualization.LineChart(document.getElementById('png'));
            //chart.draw(data, options);


            //var chart_div = document.getElementById('png');
            var dash = new google.visualization.Dashboard(document.getElementById('dashboard'));

            dash.bind([control], [chart]);

            //google.visualization.events.addListener(chart, 'ready', function() {
            //    console.log(chart.getChart().getImageURI());
            //    document.getElementById('png').innerHTML = '<a href="' + chart.getChart().getImageURI() + '">Printable Version</a>';
            //});

            google.visualization.events.addListener(chart, 'ready', function () {
                png.innerHTML = '<img src="' + chart.getChart().getImageURI() + '">';
            });
            //chart.draw(data, options);
            dash.draw(data);








            google.visualization.events.addListener(chart, 'select', function() {
                // grab a few details before redirecting
                var selection = chart.getSelection();
                var row = selection[0].row;
                var col = selection[0].column;
                var run_date = data.getValue(row, 0);
                var hrs_uos = data.getValue(row, 1);
                var cost_center = {{$data3->cost_center}};
                var cost_center_description = '{{$data3->cost_center_description}}';
                $(".modal-title", "#formModel").text( cost_center_description +' ('+cost_center+')'  );
                $(".test", "#formModel").text( cost_center_description );
                $(".run_date", "#formModel").text( run_date );
                $(".hrs_uos", "#formModel").text( hrs_uos );
                $('#formModel').modal('show');

                //location.href = 'http://www.google.com?row=' + row + '&col=' + col + '&year=' + run_date + hrs_ous + cost_center;
                //alert("You selected :" + run_date);
                //$('#formModel').modal('show', function(e) {
                //$('#cost_center').val(cost_center);
                //$("#cost_center").html($(e.relatedTarget).val(cost_center));
                // Do some stuff w/ it.
                //});



            });

        }




        $('#autocomplete').autocomplete({
            serviceUrl: '/costcenter',
            dataType: 'json',
            type:'GET',
            width: 418,
            minChars:2,
            onSelect: function(suggestion) {
                //alert(suggestion.data);
                $("#unit").val(suggestion.data);
            }


        });

        function ResetMean() {
            document.getElementById("newmean").value = '{{round($oldmean,2)}}';
        }

        function ResetLCL() {
            document.getElementById("resetLCL").value = 4;
        }

        function ResetUCL() {
            document.getElementById("resetUCL").value = 4;
        }






















    </script>

