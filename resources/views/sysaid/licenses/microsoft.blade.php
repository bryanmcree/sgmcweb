{{--New file Template--}}

    @extends('layouts.dashboard')
    {{--Page Design Goes Below--}}
@section('content')
    <div class="col-sm-12">
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel panel-heading">
                        <b>Update Microsoft Licenses</b>
                    </div>
                    <div class="panel-body" style="height:300px;">
                        <form method="post" action="/sysaid/licenses/microsoft/add">
                            {{ csrf_field() }}
                            <input type="hidden" value="{{ Auth::user()->employee_number }}" name="created_by">
                            <input type="hidden" value="Microsoft" name="license">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="e5_ava_id">E5 Licenses Available</label>
                                    <input type="number" class="form-control" id="e5_ava_id" value="{{$microsoft->first()->e5_available}}" name="e5_available" aria-describedby="emailHelp">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="e5_used_id">E5 Licenses Used</label>
                                    <input type="number" class="form-control" id="e5_used_id" value="{{$microsoft->first()->e5_used}}" name="e5_used" aria-describedby="emailHelp">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="e5_ava_id">F1 Licenses Available</label>
                                    <input type="number" class="form-control" id="e5_ava_id" value="{{$microsoft->first()->f1_available}}" name="f1_available" aria-describedby="emailHelp">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="e5_used_id">F1 Licenses Used</label>
                                    <input type="number" class="form-control" id="e5_used_id" value="{{$microsoft->first()->f1_used}}" name="f1_used" aria-describedby="emailHelp">
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel panel-heading">
                        <b>Microsoft Licenses History</b>
                    </div>
                    <div class="panel-body" style="height:300px; overflow-y: auto;">
                        <table class="table table-hover table-condensed">
                            <thead>
                                <tr>
                                    <td><b>E5's Available</b></td>
                                    <td><b>E5's Used</b></td>
                                    <td bgcolor=""><b>E5's Remaining</b></td>
                                    <td><b>F1's Available</b></td>
                                    <td><b>F1's Used</b></td>
                                    <td  bgcolor=""><b>F1's Remaining</b></td>
                                    <td><b>Last Updated</b></td>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($microsoft as $office)
                                <tr>
                                    <td>{{$office->e5_available}}</td>
                                    <td><div style="color: red">{{$office->e5_used}}</div></td>
                                    <td bgcolor=""><div style="color: green">{{$office->e5_available - $office->e5_used}}</div></td>
                                    <td>{{$office->f1_available}}</td>
                                    <td><div style="color: red">{{$office->f1_used}}</div></td>
                                    <td bgcolor=""><div style="color: green">{{$office->f1_available - $office->f1_used}}</div></td>
                                    <td>{{$office->created_at}}</td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-12">
        <div class="row">
            <canvas id="usage" height="70"></canvas>
        </div>
    </div>




@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')
<script type="application/javascript">
    new Chart(document.getElementById("usage"), {
        type: 'line',
        data: {
            labels: [
                @foreach($e5_used as $e5)
                {{$e5->e5_used}},
                @endforeach

            ],
            datasets: [{
                data: [
                    @foreach($e5_used as $e5)
                    {{$e5->e5_used}},
                    @endforeach

                ],
                label: "E5 Licenses",
                borderColor: "#3e95cd",
                fill: false
            }, {
                data: [
                    @foreach($f1_used as $f1)
                    {{$f1->f1_used}},
                    @endforeach
                ],
                label: "F1 Licenses",
                borderColor: "#8e5ea2",
                fill: false
            }
            ]
        },
        options: {
            scales: {xAxes: [{display: false}] },//Removes lables on the bottom of the chart
            elements: { point: { radius: 0 } }, //Removes data Points
            title: {display: true, text: 'Usage' //Shows title at the TOP of the chart
            }
        }
    });
</script>


@endsection
