{{--New file Template--}}

@extends('layouts.dashboard')
{{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <b>Update Airwatch Licenses</b>
            </div>
            <div class="panel-body">
                <form method="post" action="/sysaid/licenses/airwatch/add">
                    {{ csrf_field() }}
                    <input type="hidden" value="{{ Auth::user()->employee_number }}" name="created_by">
                    <input type="hidden" value="Airwatch" name="license">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="e5_ava_id">Airwatch Licenses Available</label>
                            <input type="number" class="form-control" id="e5_ava_id" name="airwatch_available" aria-describedby="emailHelp">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="e5_used_id">Airwatch Licenses Used</label>
                            <input type="number" class="form-control" id="e5_used_id" name="airwatch_used" aria-describedby="emailHelp">
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
