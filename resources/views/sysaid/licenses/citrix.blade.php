{{--New file Template--}}

@extends('layouts.dashboard')
{{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <b>Update Citrix Licenses</b>
            </div>
            <div class="panel-body">
                <form method="post" action="/sysaid/licenses/citrix/add">
                    {{ csrf_field() }}
                    <input type="hidden" value="{{ Auth::user()->employee_number }}" name="created_by">
                    <input type="hidden" value="Citrix" name="license">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="e5_ava_id">Citrix Licenses Available</label>
                            <input type="number" class="form-control" id="e5_ava_id" name="citrix_available" aria-describedby="emailHelp">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="e5_used_id">Citrix Licenses Used</label>
                            <input type="number" class="form-control" id="e5_used_id" name="citrix_used" aria-describedby="emailHelp">
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
