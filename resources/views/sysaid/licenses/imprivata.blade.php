{{--New file Template--}}

@extends('layouts.dashboard')
{{--Page Design Goes Below--}}
@section('content')

    <div class="col-sm-12">
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel panel-heading">
                        <b>Update Imprivata Licenses</b>
                    </div>
                    <div class="panel-body" style="height:300px;">
                        <form method="post" action="/sysaid/licenses/imprivata/add">
                            {{ csrf_field() }}
                            <input type="hidden" value="{{ Auth::user()->employee_number }}" name="created_by">
                            <input type="hidden" value="Imprivata" name="license">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="e5_ava_id">Imprivata Licenses Available</label>
                                    <input type="number" class="form-control" value="{{$imprivata->first()->imprivata_available}}" id="e5_ava_id" name="imprivata_available" aria-describedby="emailHelp">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="e5_used_id">Imprivata Licenses Used</label>
                                    <input type="number" class="form-control" value="{{$imprivata->first()->imprivata_used}}" id="e5_used_id" name="imprivata_used" aria-describedby="emailHelp">
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel panel-heading">
                        <b>Imprivata Licenses History</b>
                    </div>
                    <div class="panel-body" style="height:300px; overflow-y: auto;">
                        <table class="table table-hover table-condensed">
                            <thead>
                            <tr>
                                <td><b>Available</b></td>
                                <td align="center"><b>Used</b></td>
                                <td align="center" bgcolor=""><b>Remaining</b></td>
                                <td align="right"><b>Last Updated</b></td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($imprivata as $swiper)
                                <tr>
                                    <td>{{$swiper->imprivata_available}}</td>
                                    <td align="center"><div style="color: red">{{$swiper->imprivata_used}}</div></td>
                                    <td align="center" bgcolor=""><div style="color: green">{{$swiper->imprivata_available - $swiper->imprivata_used}}</div></td>
                                    <td align="right">{{$swiper->created_at}}</td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="row">
            <canvas id="usage" height="70"></canvas>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="application/javascript">
        new Chart(document.getElementById("usage"), {
            type: 'line',
            data: {
                labels: [@foreach($imprivata_used as $e5){{$e5->imprivata_used}},@endforeach

                ],
                datasets: [{
                    data: [
                        @foreach($imprivata_used as $e5){{$e5->imprivata_used}},@endforeach

                    ],
                    label: "Used Licenses",
                    borderColor: "#3e95cd",
                    fill: false
                }
                ]
            },
            options: {
                scales: {xAxes: [{display: false}] },//Removes lables on the bottom of the chart
               // elements: { point: { radius: 0 } }, //Removes data Points
                title: {display: true, text: 'Usage', //Shows title at the TOP of the chart
                    trendlineLinear: {
                        style: "rgba(255,105,180, .8)",
                        width: 2
                    }
                }

            }
        });
    </script>

@endsection
