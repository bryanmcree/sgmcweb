{{--New file Template--}}

{{--Add Security for this page below--}}

    @extends('layouts.dashboard')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">

            <div class="col-lg-2 col-md-3">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="height:180px;">
                        <div class="row">
                            <div class="col-xs-3">
                                <a href="/sysaid/licenses/microsoft" style="color: inherit;"> <i class="fab fa-microsoft fa-5x"></i></a>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="h5">E5 Total: {{$microsoft->e5_available}}</div>
                                <div class="h5">E5 Used: {{$microsoft->e5_used}}</div>
                                <div class="h5"><b><i>E5 Remaining: {{$microsoft->e5_available - $microsoft->e5_used}}</i></b></div>
                                <div class="h5">F1 Total: {{$microsoft->f1_available}}</div>
                                <div class="h5">F1 Used: {{$microsoft->f1_used}}</div>
                                <div class="h5"><b><i>F1 Remaining: {{$microsoft->f1_available - $microsoft->f1_used}}</i></b></div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="RightLeft">
                            <b><i>Microsoft</i></b><div class="pull-right"><i>{{$microsoft->created_at}}</i></div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-3">
                <div class="panel panel-green">
                    <div class="panel-heading" style="height:180px;">
                        <div class="row">
                            <div class="col-xs-3">
                                <a href="/sysaid/licenses/imprivata" style="color: inherit;"><i class="fas fa-id-badge fa-5x"></i></a>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="h5">License Total: {{$imprivata->imprivata_available}}</div>
                                <div class="h5">License Used: {{$imprivata->imprivata_used}}</div>
                                <div class="h5"><b><i>License Remaining: {{$imprivata->imprivata_available - $imprivata->imprivata_used}}</i></b></div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <b><i>Imprivata</i></b>
                        <div class="pull-right"><i>{{$imprivata->created_at}}</i></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-3">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="height:180px;">
                        <div class="row">
                            <div class="col-xs-3">
                                <a href="/sysaid/licenses/airwatch" style="color: inherit;"><i class="fas fa-mobile-alt fa-5x"></i></a>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="h5">License Total: {{$airwatch->airwatch_available}}</div>
                                <div class="h5">License Used: {{$airwatch->airwatch_used}}</div>
                                <div class="h5"><b><i>License Remaining: {{$airwatch->airwatch_available - $airwatch->airwatch_used}}</i></b></div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <b><i>Airwatch</i></b>
                        <div class="pull-right"><i>{{$airwatch->created_at}}</i></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-3">
                <div class="panel panel-green">
                    <div class="panel-heading" style="height:180px;">
                        <div class="row">
                            <div class="col-xs-3">
                                <a href="/sysaid/licenses/adobe" style="color: inherit;"><i class="fas fa-file-pdf fa-5x"></i></a>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="h5">Regular Total: {{$adobe->adobe_lic}}</div>
                                <div class="h5">Regular Used:{{$adobe->adobe_lic}}</div>
                                <div class="h5"><b><i>Regular Remaining: 0</i></b></div>
                                <div class="h5">Pro Total: {{$adobe->adobe_pro}}</div>
                                <div class="h5">Pro Used: {{$adobe->adobe_pro}}</div>
                                <div class="h5"><b><i>Pro Remaining: 0</i></b></div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <b><i>Adobe</i></b>
                        <div class="pull-right"><i>{{$adobe->created_at}}</i></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-3">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="height:180px;">
                        <div class="row">
                            <div class="col-xs-3">
                                <a href="/sysaid/licenses/citrix" style="color: inherit;"><i class="fas fa-desktop fa-5x"></i></a>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="h5">License Total: {{$citrix->citrix_available}}</div>
                                <div class="h5">License Used: {{$citrix->citrix_used}}</div>
                                <div class="h5"><b><i>License Remaining: {{$citrix->citrix_available - $citrix->citrix_used}}</i></b></div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <b><i>Citrix</i></b>
                        <div class="pull-right"><i>{{$citrix->created_at}}</i></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-6">
                <div class="panel panel-red">
                    <div class="panel-heading" style="height:180px;">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fas fa-life-ring fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{$total_open->count()}}</div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div><b><i>Open Tickets</i></b></div>
                    </div>
                </div>
            </div>

    </div>


<div class="col-md-12">

        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <canvas id="textline" width="100%" height="38"></canvas>
                </div>
            </div>
        </div>
    <div class="page-break"></div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <canvas id="epic_breakdown" width="100%" height="38"></canvas>
                </div>
            </div>
        </div>
    <div class="page-break"></div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <canvas id="bar-chart" width="100%" height="38"></canvas>
                </div>
            </div>
        </div>

</div>
    <div class="page-break"></div>

    <div class="col-md-12">
        <br>

            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <canvas id="ticket_dow" width="100%" height="38"></canvas>
                    </div>
                </div>
            </div>
        <div class="page-break"></div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <canvas id="by_shift" width="100%" height="38"></canvas>
                    </div>
                </div>
            </div>
        <div class="page-break"></div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <canvas id="by_hour" width="100%" height="38"></canvas>
                    </div>
                </div>
            </div>
    </div>
    <div class="page-break"></div>

    <div class="col-md-12">
        <br>

            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <canvas id="by_user" width="100%" height="38"></canvas>
                    </div>
                </div>
            </div>
        <div class="page-break"></div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <canvas id="closed_30" width="100%" height="38"></canvas>
                    </div>
                </div>
            </div>
        <div class="page-break"></div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <canvas id="by_esclation" width="100%" height="38"></canvas>
                    </div>
                </div>
            </div>

    </div>

    <div class="col-lg-12">
        <br>

            <nav class="navbar navbar-toggleable-md" style="background-color: #fdc2c2;">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <!-- Branding Image -->
                <a class="navbar-brand margin-top: auto" href="{{ url('/home') }}">
                    <img src="{{asset('img/sgmc_logo_navbar.png')}}">
                </a>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link active" href="/sysaid">IS Dashboard</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link disabled" href="#">Epic Dashboard</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link disabled" href="#">Print Epic Dashboard</a>
                        </li>
                    </ul>
                </div>
                <div class="margin-right: auto text-nowrap">
                    <p class="navbar-text pull-right"><i>Updated:{{$last_updated->created_at->format('m-d-Y')}}</i></p>
                </div>
                <div class="margin-right: auto text-nowrap">
                    <a class="nav-link" href="#" onclick="myFunction()"><i class="fas fa-print fa-1x"></i></a>
                </div>
            </nav>

    </div>





@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">

    function myFunction() {
        window.print();
    }


    $(function(){
        $.getJSON("sysaid/is_breakdown", function (result) {
            var labels = [],data=[];
            for (var i = 0; i < result.length; i++) {
                labels.push(result[i].sgmc_category);
                data.push(result[i].total);
            }
            var buyerData = {
                labels : labels,
                datasets: [
                    {
                        label: 'Open Tickets in the last 30 days',
                        data: data,
                        backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850",'#c4c44f','#c44fc4'],
                        borderColor: "black",
                        borderWidth:1,
                    }
                ]
            };
            var buyers = document.getElementById('textline').getContext('2d');
            var chartInstance = new Chart(buyers, {
                type: 'horizontalBar',
                options: {
                    legend: { display: false },
                    title: {
                        display: true,
                        text: 'IS Open Tickets for last 30 days ({{$last_updated->created_at->subDays(30)->format('m-d-Y')}} - {{$last_updated->created_at->format('m-d-Y')}})'
                    },
                    responsiveAnimationDuration: 6000,
                    animation: {
                        duration: 800,
                        onComplete: function () {
                            // render the value of the chart above the bar
                            var ctx = this.chart.ctx;
                            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
                            ctx.fillStyle = "White";
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'bottom';
                            this.data.datasets.forEach(function (dataset) {
                                for (var i = 0; i < dataset.data.length; i++) {
                                    var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                                    ctx.fillText(dataset.data[i], model.x -10, model.y + 7);
                                }
                            });
                        }}
                },
                data: buyerData,
            });
        });

    });


    $(function(){
        $.getJSON("sysaid/epic_breakdown", function (result) {
            var labels = [],data=[];
            for (var i = 0; i < result.length; i++) {
                labels.push(result[i].sgmc_sub_category);
                data.push(result[i].total);
            }
            var buyerData = {
                labels : labels,
                datasets: [
                    {
                        label: 'Open Tickets in the last 30 days',
                        data: data,
                        backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850",'#c4c44f','#c44fc4'],
                        borderColor: "black",
                        borderWidth:1,
                    }
                ]
            };
            var buyers = document.getElementById('epic_breakdown').getContext('2d');
            var chartInstance = new Chart(buyers, {
                type: 'horizontalBar',
                options: {
                    legend: { display: false },
                    title: {
                        display: true,
                        text: 'Epic Open Tickets for last 30 days ({{$last_updated->created_at->subDays(30)->format('m-d-Y')}} - {{$last_updated->created_at->format('m-d-Y')}})'
                    },
                    responsiveAnimationDuration: 6000,
                    animation: {
                        duration: 800,
                        onComplete: function () {
                            // render the value of the chart above the bar
                            var ctx = this.chart.ctx;
                            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
                            ctx.fillStyle = "White";
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'bottom';
                            this.data.datasets.forEach(function (dataset) {
                                for (var i = 0; i < dataset.data.length; i++) {
                                    var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                                    ctx.fillText(dataset.data[i], model.x -10, model.y + 7);
                                }
                            });
                        }}
                },
                data: buyerData,
            });
        });

    });



    new Chart(document.getElementById("bar-chart"), {
        type: 'bar',
        options: {
            legend: { display: true },
            title: {
                display: true,
                text: 'Epic / IS Tickets current open tickets.'
            },
            responsiveAnimationDuration: 6000,
            animation: {
                duration: 900,
                onComplete: function () {
                    // render the value of the chart above the bar
                    var ctx = this.chart.ctx;
                    ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
                    ctx.fillStyle = this.chart.config.options.defaultFontColor;
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'bottom';
                    this.data.datasets.forEach(function (dataset) {
                        for (var i = 0; i < dataset.data.length; i++) {
                            var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                            ctx.fillText(dataset.data[i], model.x + 5, model.y + 1);
                        }
                    });
                }}
        },
        data: {
            //labels: ["Open Tickets"],
            datasets: [
                {
                    label: "Epic",
                    backgroundColor: "#3e95cd",
                    data: [{{$epic_open->total}}]
                }, {
                    label: "IS",
                    backgroundColor: "#8e5ea2",
                    data: [{{$is_open->total}}]
                }
            ]
        }
    });



    new Chart(document.getElementById("closed_30"), {
        type: 'bar',
        options: {
            legend: { display: true },
            title: {
                display: true,
                text: 'Epic / IS Tickets closed in the last 30 days ({{$last_updated->created_at->subDays(30)->format('m-d-Y')}} - {{$last_updated->created_at->format('m-d-Y')}})'
            },
            responsiveAnimationDuration: 6000,
            animation: {
                duration: 900,
                onComplete: function () {
                    // render the value of the chart above the bar
                    var ctx = this.chart.ctx;
                    ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
                    ctx.fillStyle = this.chart.config.options.defaultFontColor;
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'bottom';
                    this.data.datasets.forEach(function (dataset) {
                        for (var i = 0; i < dataset.data.length; i++) {
                            var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                            ctx.fillText(dataset.data[i], model.x + 5, model.y + 1);
                        }
                    });
                }}
        },
        data: {
            //labels: ["Open Tickets"],
            datasets: [
                {
                    label: "Epic",
                    backgroundColor: "#3e95cd",
                    data: [{{$epic_closed->total}}]
                }, {
                    label: "IS",
                    backgroundColor: "#8e5ea2",
                    data: [{{$is_closed->total}}]
                }
            ]
        }
    });



    $(function(){
        $.getJSON("sysaid/byday", function (result) {
            var labels = [],data=[];
            for (var i = 0; i < result.length; i++) {
                labels.push(result[i].ticket_dow);
                data.push(result[i].total);
            }
            var buyerData = {
                labels : ['Saturday','Sunday','Monday','Tuesday','Wednesday','Thursday','Friday'],
                datasets: [
                    {
                        label: 'New ticket volumes by day of week for the last 30 days',
                        data: data,
                        backgroundColor: ['#f2dad9'],
                        borderColor: "black",
                        borderWidth:1,
                    }
                ],


            };
            var buyers = document.getElementById('ticket_dow').getContext('2d');
            var chartInstance = new Chart(buyers, {
                type: 'line',

                options: {
                    legend: { display: false },
                    title: {
                        display: true,
                        text: 'New ticket volumes by day of week for the last 30 days ({{$last_updated->created_at->subDays(30)->format('m-d-Y')}} - {{$last_updated->created_at->format('m-d-Y')}})'
                    },
                    responsiveAnimationDuration: 6000,
                    animation: {
                        duration: 200,
                        onComplete: function () {
                            // render the value of the chart above the bar
                            var ctx = this.chart.ctx;
                            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
                            ctx.fillStyle = this.chart.config.options.defaultFontColor;
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'bottom';
                            this.data.datasets.forEach(function (dataset) {
                                for (var i = 0; i < dataset.data.length; i++) {
                                    var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                                    ctx.fillText(dataset.data[i], model.x, model.y - 5);
                                }
                            });
                        }}
                },
                data: buyerData,


            });
        });

    });


    $(function(){
        $.getJSON("sysaid/byshift", function (result) {
            var labels = [],data=[];
            for (var i = 0; i < result.length; i++) {
                labels.push(result[i].ticket_shift);
                data.push(result[i].total);
            }
            var buyerData = {
                labels : labels,
                datasets: [
                    {
                        label: 'New tickets by shift in the last 30 days',
                        data: data,
                        backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                        borderColor: "black",
                        borderWidth:1,
                    }
                ]
            };
            var buyers = document.getElementById('by_shift').getContext('2d');
            var chartInstance = new Chart(buyers, {
                type: 'horizontalBar',
                options: {
                    legend: { display: false },
                    title: {
                        display: true,
                        text: 'New ticket by shift for the last 30 days ({{$last_updated->created_at->subDays(30)->format('m-d-Y')}} - {{$last_updated->created_at->format('m-d-Y')}})'
                    },
                    responsiveAnimationDuration: 6000,
                    animation: {
                        duration: 300,
                        onComplete: function () {
                            // render the value of the chart above the bar
                            var ctx = this.chart.ctx;
                            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
                            ctx.fillStyle =  'white'//this.chart.config.options.defaultFontColor;
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'bottom';
                            this.data.datasets.forEach(function (dataset) {
                                for (var i = 0; i < dataset.data.length; i++) {
                                    var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                                    ctx.fillText(dataset.data[i], model.x - 20, model.y + 8);
                                }
                            });
                        }}
                },
                data: buyerData,
            });
        });

    });


    $(function(){
        $.getJSON("sysaid/byhour", function (result) {
            var labels = [],data=[];
            for (var i = 0; i < result.length; i++) {
                labels.push(result[i].ticket_hour);
                data.push(result[i].total);
            }
            var buyerData = {
                labels : ['12AM','1AM','2AM','3AM','4AM','5AM','6AM','7AM','8AM','9AM','10AM','11AM','12PM','1PM','2PM','3PM','4PM','5PM','6PM','7PM','8PM','9PM','10PM','11PM'],
                datasets: [
                    {
                        label: 'New ticket volumes by hour for the last 30 days',
                        data: data,
                        backgroundColor: ['#3cba9f'],
                        borderColor: "black",
                        borderWidth:1,
                    }
                ]
            };
            var buyers = document.getElementById('by_hour').getContext('2d');
            var chartInstance = new Chart(buyers, {
                type: 'line',
                options: {
                    legend: { display: false },
                    title: {
                        display: true,
                        text: 'New ticket volumes by hour for the last 30 days ({{$last_updated->created_at->subDays(30)->format('m-d-Y')}} - {{$last_updated->created_at->format('m-d-Y')}})'
                    },
                    responsiveAnimationDuration: 6000,
                    animation: {
                        duration: 400,
                        onComplete: function () {
                            // render the value of the chart above the bar
                            var ctx = this.chart.ctx;
                            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
                            ctx.fillStyle = this.chart.config.options.defaultFontColor;
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'bottom';
                            this.data.datasets.forEach(function (dataset) {
                                for (var i = 0; i < dataset.data.length; i++) {
                                    var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                                    ctx.fillText(dataset.data[i], model.x, model.y - 5);
                                }
                            });
                        }}
                },
                data: buyerData,
            });
        });

    });



    $(function(){
        $.getJSON("sysaid/byuser", function (result) {
            var labels = [],data=[];
            for (var i = 0; i < result.length; i++) {
                labels.push(result[i].requested_user);
                data.push(result[i].total);
            }
            var buyerData = {
                labels : labels,
                datasets: [
                    {
                        label: 'Top 5 End Users in the last 30 days',
                        data: data,
                        backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850",'#c4c44f','#c44fc4'],
                        borderColor: "black",
                        borderWidth:1,
                    }
                ]
            };
            var buyers = document.getElementById('by_user').getContext('2d');
            var chartInstance = new Chart(buyers, {
                type: 'horizontalBar',
                options: {
                    responsiveAnimationDuration: 6000,
                    legend: { display: false },
                    title: {
                        display: true,
                        text: 'Top 5 End Users in the last 30 days ({{$last_updated->created_at->subDays(30)->format('m-d-Y')}} - {{$last_updated->created_at->format('m-d-Y')}})'
                    },
                    animation: {
                        duration: 500,
                        onComplete: function () {
                            // render the value of the chart above the bar
                            var ctx = this.chart.ctx;
                            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
                            ctx.fillStyle = this.chart.config.options.defaultFontColor;
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'bottom';
                            this.data.datasets.forEach(function (dataset) {
                                for (var i = 0; i < dataset.data.length; i++) {
                                    var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                                    ctx.fillText(dataset.data[i], model.x + 12, model.y + 7);
                                }
                            });
                        }}
                },
                data: buyerData,
            });
        });

    });


    $(function(){
        $.getJSON("sysaid/bymanager", function (result) {
            var labels = [],data=[];
            for (var i = 0; i < result.length; i++) {
                labels.push(result[i].process_manager);
                data.push(result[i].total);
            }
            var buyerData = {
                labels : labels,
                datasets: [
                    {
                        label: 'Admin/Analyst',
                        data: data,
                        backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850",'#c4c44f','#c44fc4'],
                        borderColor: "black",
                        borderWidth:1,
                    }
                ]
            };
            var buyers = document.getElementById('by_manager').getContext('2d');
            var chartInstance = new Chart(buyers, {
                type: 'horizontalBar',
                options: {
                    responsiveAnimationDuration: 6000,
                    legend: { display: false },
                    title: {
                        display: true,
                        text: 'Admin/Analyst'
                    },
                    animation: {
                        duration: 600,
                        onComplete: function () {
                            // render the value of the chart above the bar
                            var ctx = this.chart.ctx;
                            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
                            ctx.fillStyle = this.chart.config.options.defaultFontColor;
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'bottom';
                            this.data.datasets.forEach(function (dataset) {
                                for (var i = 0; i < dataset.data.length; i++) {
                                    var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                                    ctx.fillText(dataset.data[i], model.x + 12, model.y + 7);
                                }
                            });
                        }}
                },
                data: buyerData,

            });
        });

    });

    $(function(){
        $.getJSON("sysaid/byyear", function (result) {
            var labels = [],data=[];
            for (var i = 0; i < result.length; i++) {
                labels.push(result[i].ticket_year);
                data.push(result[i].total);
            }
            var buyerData = {
                labels : labels,
                datasets: [
                    {
                        label: 'Ticket ',
                        data: data,
                        backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850",'#c4c44f','#c44fc4'],
                        borderColor: "black",
                        borderWidth:1,
                    },
                ]
            };
            var buyers = document.getElementById('by_year').getContext('2d');
            var chartInstance = new Chart(buyers, {
                type: 'bar',
                options: {

                    responsiveAnimationDuration: 6000,
                    animation: {
                        duration: 800,
                        onComplete: function () {
                            // render the value of the chart above the bar
                            var ctx = this.chart.ctx;
                            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
                            ctx.fillStyle = this.chart.config.options.defaultFontColor;
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'bottom';
                            this.data.datasets.forEach(function (dataset) {
                                for (var i = 0; i < dataset.data.length; i++) {
                                    var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                                    ctx.fillText(dataset.data[i], model.x + 5, model.y + 1);
                                }
                            });
                        }},
                    legend: { display: false },
                    title: {
                        display: true,
                        text: 'Ticket Volumes by Year'
                    }
                },
                data: buyerData,
            });
        });

    });

    $(function(){
        $.getJSON("sysaid/byesclation", function (result) {
            var labels = [],data=[];
            for (var i = 0; i < result.length; i++) {
                labels.push(result[i].esclation);
                data.push(result[i].total);
            }
            var buyerData = {
                labels : ['30 Days +','High SLA','Med SLA','Low SLA','1 HR','30 M','No Violation'],
                //labels : labels,
                datasets: [
                    {
                        label: 'SLA Violations',
                        data: data,
                        backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850",'#c4c44f','#c44fc4'],
                        borderColor: "black",
                        borderWidth:1,
                    }
                ]
            };
            var buyers = document.getElementById('by_esclation').getContext('2d');
            var chartInstance = new Chart(buyers, {
                type: 'horizontalBar',
                options: {
                    responsiveAnimationDuration: 6000,

                    legend: { display: false },
                    title: {
                        display: true,
                        text: 'SLA Violations for All Curent Open Tickets'
                    },
                    animation: {
                        duration: 700,
                        onComplete: function () {
                            // render the value of the chart above the bar
                            var ctx = this.chart.ctx;
                            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
                            ctx.fillStyle = this.chart.config.options.defaultFontColor;
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'bottom';
                            this.data.datasets.forEach(function (dataset) {
                                for (var i = 0; i < dataset.data.length; i++) {
                                    var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                                    ctx.fillText(dataset.data[i], model.x + 12, model.y + 7);
                                }
                            });
                        }}
                },
                data: buyerData,
            });
        });

    });


    new Chart(document.getElementById("admin_cat"), {
        type: 'bar',
        options: {
            legend: { display: false },
            title: {
                display: true,
                text: 'System Administration Related Request Breakdown for last 30 days ({{$last_updated->created_at->subDays(30)->format('m-d-Y')}} - {{$last_updated->created_at->format('m-d-Y')}})'
            },
            responsiveAnimationDuration: 6000,
            animation: {
                duration: 900,
                onComplete: function () {
                    // render the value of the chart above the bar
                    var ctx = this.chart.ctx;
                    ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
                    ctx.fillStyle = this.chart.config.options.defaultFontColor;
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'bottom';
                    this.data.datasets.forEach(function (dataset) {
                        for (var i = 0; i < dataset.data.length; i++) {
                            var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                            ctx.fillText(dataset.data[i], model.x + 5, model.y + 1);
                        }
                    });
                }}
        },
        data: {
            labels: ["Epic Tickets", "All Other"],
            datasets: [
                {
                    label: ["Tickets"],
                    backgroundColor: ["#3e95cd","#8e5ea2"],
                    data: [{{$category_epic->total}},{{$category_is->total}}]
                }
            ]
        }
    });


</script>

@endsection
