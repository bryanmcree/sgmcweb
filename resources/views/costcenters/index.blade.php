{{--New file Template--}}


    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="card mb-4 text-white bg-dark">
        <div class="card-body">
            <a class="btn btn-sm btn-default" href="/costcenters/report"><i class="fas fa-chart-line"></i> PI Report</a>
            <a class="btn btn-sm btn-default" href="/costcenters/download"><i class="fas fa-download"></i> Cost Center Download</a>
            <a class="btn btn-sm btn-default" href="/costcenters/analyst"><i class="fas fa-download"></i> Analyst Audit</a>
            <a class="btn btn-sm btn-info" href="/costcenters/edit/LE">Update Labor Efficiency</a>

            <span class="float-right">
                <a href="/costcenter/create" class="btn btn-info btn-sm">Add New Cost Center</a>
            </span>

            <p></p>
            <table class="table table-condensed table-striped table-hover" id="costcenters" style="font-size: 13px;">
                <thead>
                <tr>
                    <td><b>Cost Center</b></td>
                    <td align="center"><b>Emp C/I</b></td>
                    <td><b>C-Suite</b></td>
                    <td><b>Admin</b></td>
                    <td><b>Director</b></td>
                    <td><b>Manager</b></td>
                    <td><b>Clinical</b></td>
                    <td class="no-sort"></td>
                </tr>
                </thead>
                <tbody>
                @foreach($costcenters as $costcenter)
                    <tr>
                        <td style="vertical-align: middle;" @if($costcenter->active_status == 0) class="bg-danger" @endif>{{$costcenter->style1}}</td>
                        <td style="vertical-align: middle;"  @if($costcenter->active_status == 0) class="bg-danger" @endif align="center">{{$clockedin->where('cost_center',$costcenter->cost_center)->first()['total']}}</td>
                        <td style="vertical-align: middle;" @if($costcenter->active_status == 0) class="bg-danger" @endif>{{$costcenter->csuite}}</td>
                        <td style="vertical-align: middle;" @if($costcenter->active_status == 0) class="bg-danger" @endif>@if(!is_null($costcenter->administrator)) {{$costcenter->adminEmployee->name}} @else  @endif</td>
                        <td style="vertical-align: middle;" @if($costcenter->active_status == 0) class="bg-danger" @endif>@if(!is_null($costcenter->directorEmployee)) {{$costcenter->directorEmployee->name}} @else  @endif</td>
                        <td style="vertical-align: middle;" @if($costcenter->active_status == 0) class="bg-danger" @endif>@if(!is_null($costcenter->managerEmployee)) {{$costcenter->managerEmployee->name}} @else  @endif</td>
                        <td style="vertical-align: middle;" @if($costcenter->active_status == 0) class="bg-danger" @endif align="center">@if($costcenter->clinical == 1) <i class="fas fa-user-md" style="color:deeppink;"></i> @else - @endif</td>
                        <td style="vertical-align: middle;" @if($costcenter->active_status == 0) class="bg-danger" @endif align="right">
                            <a href="costcenters/{{$costcenter->cost_center}}" class=""><i class="fas fa-info-circle"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')
<script type="application/javascript">
    $(document).ready(function() {
        $('#costcenters').DataTable( {
            "pageLength": 20,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>


@endsection
