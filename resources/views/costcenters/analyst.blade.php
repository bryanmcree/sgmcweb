{{--New file Template--}}



    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="card bg-dark text-white mb-3">
            <div class="card-header">
                <B>Business Intelligence - Analyst Audit</B>
            </div>
            <div class="card-body">
                <div class="col-lg-4">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <td><b>Analyst</b></td>
                            <td align="center"><b>Total CC</b></td>
                            <td align="center"><b>Clinical CC</b></td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($analysts as $analyst)
                            <tr>
                                <td>@if(is_null($analyst->analyst)) Not Assigned @else {{$analyst->analyst}} @endif</td>
                                <td align="center">{{$analyst->TotalCC}}</td>
                                <td align="center">{{$analyst->totalClinical}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                @foreach($analysts as $analyst)
                <div class="col-lg-12">
                    <div class="card bg-dark text-white mb-3">
                        <div class="card-header">
                            <b>@if(is_null($analyst->analyst)) Not Assigned @else {{$analyst->analyst}} @endif</b>
                        </div>
                        <div class="card-body">
                            <table class="table table-condensed table-striped table-hover">
                                <thead>
                                <tr>
                                    <td><b>Cost Center</b></td>
                                    <td><b>C-Suite</b></td>
                                    <td><b>Director</b></td>
                                    <td><b>Manager</b></td>
                                    <td><b>BI Analyst</b></td>
                                    <td><b>Fixed/Flexed</b></td>
                                    <td><b>Clinical</b></td>
                                    <td class="no-sort"></td>
                                </tr>
                                </thead>
                                <tbody>
                                {{$analyst->analyst}}
                                @foreach($costcentersUnassigned->where('analyst',$analyst->analyst_number) as $costcenter)
                                    <tr>
                                        <td @if($costcenter->active_status == 0) class="bg-danger" @endif>{{$costcenter->style1}}</td>
                                        <td @if($costcenter->active_status == 0) class="bg-danger" @endif>@if(!is_null($costcenter->csuite)) {{$costcenter->csuiteEmployee->name}} @else  @endif</td>
                                        <td @if($costcenter->active_status == 0) class="bg-danger" @endif>@if(!is_null($costcenter->directorEmployee)) {{$costcenter->directorEmployee->name}} @else  @endif</td>
                                        <td @if($costcenter->active_status == 0) class="bg-danger" @endif>@if(!is_null($costcenter->managerEmployee)) {{$costcenter->managerEmployee->name}} @else  @endif</td>
                                        <td @if($costcenter->active_status == 0) class="bg-danger" @endif>@if(!is_null($costcenter->analystEmployee)) {{$costcenter->analystEmployee->name}} @else  @endif</td>
                                        <td @if($costcenter->active_status == 0) class="bg-danger" @endif>@if(!is_null($costcenter->uosName)) {{$costcenter->uosName->unit_of_service}} @else @endif</td>
                                        <td @if($costcenter->active_status == 0) class="bg-danger" @endif align="center">@if($costcenter->clinical == 1) <i class="fas fa-user-md" style="color:deeppink;"></i> @else - @endif</td>
                                        <td @if($costcenter->active_status == 0) class="bg-danger" @endif align="right">
                                            <a href="/costcenters/{{$costcenter->cost_center}}" class="btn btn-default btn-xs" target="_blank">Detail</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="application/javascript">
        $(document).ready(function() {
            $('#costcenters').DataTable( {
                "pageLength": 20,
                "order": [],
                "columnDefs": [ {
                    "targets"  : 'no-sort',
                    "orderable": false,
                }]
            } );
        } );
    </script>

@endsection
