{{--New file Template--}}



@extends('layouts.app')
{{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="card bg-dark text-white mb-3">
            <div class="card-header">
                <b>Director Score Card</b>
            </div>
            <div class="card-body">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/costcenters/report">C-Suite</a></li>
                        <li class="breadcrumb-item"><a href="/costcenters/report_dir/{{$csuite->csuite}}">{{$csuite_name->name}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$csuite->directorEmployee->name}}</li>
                    </ol>
                </nav>
                <table class="table table-striped table-bordered table-hover ">
                    <thead>
                    <tr>
                        <td></td>
                        <td><b>Cost Center</b></td>
                        <td align="center"><b>Rolled Up</b></td>
                        <td align="center"><b>$$</b></td>
                        <td align="center"><b>UOS</b></td>

                        <td align="center"><b>Contractors</b></td>
                        <td align="center"><b>Contract $</b></td>
                        <td align="center"><b>WT</b></td>
                        <td align="center"><b>$ FTE's</b></td>
                        <td align="center"><b>W FTE's</b></td>
                        <td align="center"><b>E FTE's</b></td>
                        <td align="center"><b>FTE's Var</b></td>
                        <td align="center"><b>PI %</b></td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $rowCount = 0 ?>
                    @foreach($costcenters as $cc)
                        <?php $rowCount = $rowCount +1 ?>
                        <tr>
                            <td>{{$rowCount}}.</td>
                            <td>{{$cc->style1}}</td>
                            <td align="center">@if(is_null($cc->cost_center_roll_up) OR $cc->cost_center_roll_up == $cc->cost_center) None @else {{$cc->cost_center_roll_up}} @endif</td>
                            <td align="center">@if($cc->rev_gen == 1) <i class="fas fa-money-bill-alt" style="color:green;"></i> @else <i class="far fa-times-circle"  style="color:red;"></i> @endif</td>
                            <td align="center">
                                @if(is_null($cc->cost_center_roll_up) OR $cc->cost_center_roll_up == $cc->cost_center)
                                @if(is_null($cc->uos_amount)) 0 @else {{$cc->uos_amount}} @endif
                                @else<i class="far fa-circle" style="color:red;"></i>
                                @endif
                            </td>

                            <td align="center">@if(is_null($cc->contract_fte))0 @else {{$cc->contract_fte}} @endif</td>
                            <TD align="center">@if(!is_null($cc->Activity))${{number_format($cc->Activity,2)}}@endif</TD>
                            <td align="center">
                                @if(is_null($cc->cost_center_roll_up) OR $cc->cost_center_roll_up == $cc->cost_center)
                                @if(is_null($cc->worked_target)) N/A @else{{$cc->worked_target}} @endif</td>
                            @else<i class="far fa-circle" style="color:red;"></i>
                                @endif
                            <td align="center">
                                @if(is_null($cc->cost_center_roll_up) OR $cc->cost_center_roll_up == $cc->cost_center)
                                {{number_format($cc->paid_fte,2)}}
                                @else<i class="far fa-circle" style="color:red;"></i>
                                @endif
                            </td>
                            <td align="center">
                                @if(is_null($cc->cost_center_roll_up) OR $cc->cost_center_roll_up == $cc->cost_center)
                                {{number_format($cc->worked_fte,2)}}
                                @else<i class="far fa-circle" style="color:red;"></i>
                                @endif
                            </td>
                            <td align="center">
                                @if(is_null($cc->cost_center_roll_up) OR $cc->cost_center_roll_up == $cc->cost_center)
                                {{number_format($cc->expected_fte,2)}}
                                @else<i class="far fa-circle" style="color:red;"></i>
                                @endif
                            </td>
                            <td align="center">{{number_format($cc->fte_variance,2)}}</td>
                            <td align="center">
                                @if(is_null($cc->cost_center_roll_up) OR $cc->cost_center_roll_up == $cc->cost_center)
                                {{number_format($cc->pi,2)}}%
                                @else<i class="far fa-circle" style="color:red;"></i>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                        <tr>
                            <td colspan="6"></td>

                            <td align="center"></td>

                            <td align="center">{{number_format($costcenters->sum('paid_fte'),2)}}</td>
                            <td align="center">{{number_format($costcenters->sum('worked_fte'),2)}}</td>
                            <td align="center">{{number_format($costcenters->sum('expected_fte'),2)}}</td>
                            <td></td>
                            <td align="center">{{number_format($costcenters->sum('fte_variance'),2)}}</td>
                            <td align="center" colspan="1"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="panel-footer">
                @include('costcenters.footer_report')
            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
