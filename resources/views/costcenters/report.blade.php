{{--New file Template--}}



    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="card bg-dark text-white mb-3">
            <div class="card-header">
                <b>C-Suite Score Card</b>
            </div>
            <div class="card-body">
                <table class="table table-striped table-bordered table-hover">

                    <thead>
                        <tr>
                            <td><b>C-Suite</b></td>
                            <td align="center"><b># CC</b></td>
                            <td align="center"><b>PI Percent</b></td>
                            <td align="center"><b>FTE Variance</b></td>
                            <td align="center"><b>Contract Emp</b></td>
                            <td align="center"><b>Contract Exp</b></td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($csuite as $oteam)
                        <tr>
                            <td>{{$oteam->name}}</td>
                            <td align="center">{{$oteam->totalcc}}</td>

                            @if($oteam->PI == 0.00)
                                <td align="center" style="color:black; font-weight: bold">{{number_format($oteam->PI,2)}}%</td>
                            @elseif($oteam->PI < 90)
                                <td align="center" style="color:red;font-weight: bold">{{number_format($oteam->PI,2)}}%</td>
                            @elseif($oteam->PI > 91 AND $oteam->PI <= 95)
                                <td align="center" style="color:black; font-weight: bold">{{number_format($oteam->PI,2)}}%</td>
                            @else
                                <td align="center" style="color:green;font-weight: bold">{{number_format($oteam->PI,2)}}%</td>
                            @endif

                            @if($oteam->fte_variance > 0)
                            <td align="center" style="font-weight: bold;color:red;">{{number_format($oteam->fte_variance,2)}}</td>
                            @else
                                <td align="center" style="font-weight: bold;color:green;">{{number_format($oteam->fte_variance,2)}}</td>
                            @endif

                            @if($oteam->contract_fte > 0)
                                <td align="center" style="font-weight: bold;color:red;">{{number_format($oteam->contract_fte,2)}}</td>
                            @else
                                <td align="center" style="font-weight: bold;color:green;">{{number_format($oteam->contract_fte,2)}}</td>
                            @endif
                            <td align="center">${{number_format($oteam->contract_expense,2)}}</td>
                            <td align="right"><a href="/costcenters/report_dir/{{$oteam->csuite}}" class="btn btn-default btn-xs"><i class="fas fa-plus-square"></i> Directors</a></td>
                        </tr>
                        @endforeach
                        <tr>
                            <td colspan="3"></td>
                            @if($csuite->sum('fte_variance') > 0)
                            <td align="center" style="font-weight: bold;color:red;">{{number_format($csuite->sum('fte_variance'),2)}}</td>
                            @else
                                <td align="center" style="font-weight: bold;color:green;">{{number_format($csuite->sum('fte_variance'),2)}}</td>
                            @endif
                            @if($csuite->sum('contract_fte') > 0)
                                <td align="center" style="font-weight: bold;color:red;">{{number_format($csuite->sum('contract_fte'),2)}}</td>
                            @else
                                <td align="center" style="font-weight: bold;color:green;">{{number_format($csuite->sum('contract_fte'),2)}}</td>
                            @endif
                            <td align="center">{{number_format($csuite->sum('contract_expense'),2)}}</td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="panel-footer">
                @include('costcenters.footer_report')
            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
