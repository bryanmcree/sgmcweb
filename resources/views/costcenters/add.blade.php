{{--New file Template--}}

{{--Add Security for this page below--}}


    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="card bg-dark text-white mb-3">
            <div class="card-header">
                <h3><b>Cost Centers</b></h3>
            </div>
            <div class="card-body">
                <div class="col-lg-12">
                    <div class="card bg-dark text-white mb-3">
                        <div class="card-header">
                            <B>All Cost Centers</B>
                        </div>
                        <div class="card-body">
                            <form method="post" action="/costcenters/update">
                                {{ csrf_field() }}
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <td><b>Number</b></td>
                                        <td><b>Description</b></td>
                                        <td><b>Status</b></td>
                                        <td><b>C-Suite</b></td>
                                        <td><b>Admin</b></td>
                                        <td><b>Director</b></td>
                                        <td><b>Manager</b></td>
                                        <td><b>Analyst</b></td>
                                    </tr>
                                </thead>
                                <tbody>
                                <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
                                {!! Html::script('js/jquery.autocomplete.js') !!}
                                @foreach($costcenters as $costcenter)
                                    <tr>
                                        <td><label for="{{$costcenter->cost_center}}">{{$costcenter->cost_center}}</label></td>
                                        <td><label for="{{$costcenter->cost_center}}">{{$costcenter->unit_code_description}}</label></td>
                                        <td><label for="{{$costcenter->cost_center}}">
                                            @if($costcenter->active_status == 1)
                                            Active
                                            @else
                                            Inactive
                                            @endif
                                            </label>
                                        </td>
                                        <td><label for="{{$costcenter->cost_center}}">
                                                @if(is_null($costcenter->csuite))
                                                    <div class="form-group">
                                                        <input type="hidden" class="form-control input-sm" name="{{$costcenter->cost_center}}[csuite]" value="{{$costcenter->csuite}}" id="csuite_value_{{$costcenter->cost_center}}">
                                                        <input type="search" id="csuite_{{$costcenter->cost_center}}" class="form-control" placeholder="C-Suite" >
                                                    </div>
                                                @else
                                                    <input type="hidden" class="form-control input-sm" name="{{$costcenter->cost_center}}[csuite]" value="{{$costcenter->csuite}}" id="csuite_value_{{$costcenter->cost_center}}">
                                                    {{$costcenter->csuiteEmployee->name}}
                                                @endif
                                            </label>
                                        </td>
                                        <td><label for="{{$costcenter->cost_center}}">
                                                @if(is_null($costcenter->administrator))
                                                    <div class="form-group">
                                                        <input type="hidden" class="form-control input-sm" name="{{$costcenter->cost_center}}[admin]" value="{{$costcenter->administrator}}" id="admin_value_{{$costcenter->cost_center}}">
                                                        <input type="search" id="admin_{{$costcenter->cost_center}}" class="form-control" placeholder="Admin" >
                                                    </div>
                                                @else
                                                    <input type="hidden" class="form-control input-sm" name="{{$costcenter->cost_center}}[admin]" value="{{$costcenter->administrator}}" id="admin_value_{{$costcenter->cost_center}}">
                                                    {{$costcenter->adminEmployee->name}}
                                                @endif
                                            </label>
                                        </td>
                                        <td><label for="{{$costcenter->cost_center}}">
                                            @if(is_null($costcenter->director))
                                                    <div class="form-group">
                                                        <input type="hidden" class="form-control input-sm" name="{{$costcenter->cost_center}}[director]"  value="{{$costcenter->director}}" id="director_value_{{$costcenter->cost_center}}">
                                                        <input type="search" id="director_{{$costcenter->cost_center}}" class="form-control" placeholder="Director" >
                                                    </div>
                                            @else
                                                    <input type="hidden" class="form-control input-sm" name="{{$costcenter->cost_center}}[director]"  value="{{$costcenter->director}}" id="director_value_{{$costcenter->cost_center}}">
                                                    {{$costcenter->directorEmployee->name}}
                                            @endif
                                            </label>
                                        </td>
                                        <td><label for="{{$costcenter->cost_center}}">
                                            @if(is_null($costcenter->manager))
                                                    <div class="form-group">
                                                        <input type="hidden" class="form-control input-sm" name="{{$costcenter->cost_center}}[manager]"  value="{{$costcenter->manager}}" id="manager_value_{{$costcenter->cost_center}}">
                                                        <input type="search" id="manager_{{$costcenter->cost_center}}" class="form-control" placeholder="Manager" >
                                                    </div>
                                            @else
                                                    <input type="hidden" class="form-control input-sm" name="{{$costcenter->cost_center}}[manager]"  value="{{$costcenter->manager}}" id="manager_value_{{$costcenter->cost_center}}">
                                                    {{$costcenter->managerEmployee->name}}
                                            @endif
                                            </label>
                                        </td>
                                        <td><label for="{{$costcenter->cost_center}}">
                                                @if(is_null($costcenter->analyst))
                                                    <div class="form-group">
                                                        <input type="hidden" class="form-control input-sm" name="{{$costcenter->cost_center}}[analyst]"  value="{{$costcenter->analyst}}" id="analyst_value_{{$costcenter->cost_center}}">
                                                        <input type="search" id="analyst_{{$costcenter->cost_center}}" class="form-control" placeholder="Analyst" >
                                                    </div>
                                                @else
                                                    <input type="hidden" class="form-control input-sm" name="{{$costcenter->cost_center}}[analyst]"  value="{{$costcenter->analyst}}" id="analyst_value_{{$costcenter->cost_center}}">
                                                    {{$costcenter->analystEmployee->name}}
                                                @endif
                                            </label>
                                        </td>
                                    </tr>

                                    <script type="application/javascript">
                                        $('#csuite_{{$costcenter->cost_center}}').autocomplete({
                                            serviceUrl: '/survey/employeesearch',
                                            dataType: 'json',
                                            type:'GET',
                                            width: 418,
                                            minChars:2,
                                            onSelect: function(suggestion) {
                                                //alert(suggestion.data);
                                                $("#csuite_value_{{$costcenter->cost_center}}").val(suggestion.data);
                                            }
                                        });

                                        $('#admin_{{$costcenter->cost_center}}').autocomplete({
                                            serviceUrl: '/survey/employeesearch',
                                            dataType: 'json',
                                            type:'GET',
                                            width: 418,
                                            minChars:2,
                                            onSelect: function(suggestion) {
                                                //alert(suggestion.data);
                                                $("#admin_value_{{$costcenter->cost_center}}").val(suggestion.data);
                                            }
                                        });

                                        $('#director_{{$costcenter->cost_center}}').autocomplete({
                                            serviceUrl: '/survey/employeesearch',
                                            dataType: 'json',
                                            type:'GET',
                                            width: 418,
                                            minChars:2,
                                            onSelect: function(suggestion) {
                                                //alert(suggestion.data);
                                                $("#director_value_{{$costcenter->cost_center}}").val(suggestion.data);
                                            }
                                        });

                                        $('#manager_{{$costcenter->cost_center}}').autocomplete({
                                            serviceUrl: '/survey/employeesearch',
                                            dataType: 'json',
                                            type:'GET',
                                            width: 418,
                                            minChars:2,
                                            onSelect: function(suggestion) {
                                                //alert(suggestion.data);
                                                $("#manager_value_{{$costcenter->cost_center}}").val(suggestion.data);
                                            }
                                        });

                                        $('#analyst_{{$costcenter->cost_center}}').autocomplete({
                                            serviceUrl: '/survey/employeesearch',
                                            dataType: 'json',
                                            type:'GET',
                                            width: 418,
                                            minChars:2,
                                            onSelect: function(suggestion) {
                                                //alert(suggestion.data);
                                                $("#analyst_value_{{$costcenter->cost_center}}").val(suggestion.data);
                                            }
                                        });
                                    </script>
                                @endforeach
                                </tbody>
                            </table>
                                <input type="submit">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
