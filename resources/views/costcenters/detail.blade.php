{{--New file Template--}}

{{--Add Security for this page below--}}


    @extends('layouts.app')
    {{--Page Design Goes Below--}}

     <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

@section('content')

    <div class="card mb-3 border-info text-white bg-dark">
        <div class="card-header">
            <nav>
                <div class="nav nav-tabs mb-4" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Home</a>
                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Clocked in Employees</a>
                    <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">PI</a>
                    <a class="nav-item nav-link" id="nav-notes-tab" data-toggle="tab" href="#nav-notes" role="tab" aria-controls="nav-notes" aria-selected="false">Notes</a>
                    <a class="nav-item nav-link" id="nav-edit-tab" data-toggle="tab" href="#nav-edit" role="tab" aria-controls="nav-edit" aria-selected="false">Edit</a>
                    <a class="nav-item nav-link" href="/costcenters">Cost Centers</a>
                </div>
            </nav>
        </div>

        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                <div class="card mb-3 text-white bg-dark">
                    <div class="card-body">

                        @if(empty($costcenter->csuiteEmployee) or empty($costcenter->directorEmployee))
                            <div class="col-lg-6">
                                <div class="alert alert-danger" role="alert">
                                    <div class="row vertical-align">
                                        <div class="col-xs-2">
                                            <i class="fa fa-exclamation-triangle fa-2x"></i>
                                        </div>
                                        <div class="col-xs-10" style="font-size: 18px;">
                                            Required Cost Center Data is Missing!
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="col-lg-6">
                                <div class="alert alert-success" role="alert">
                                    <div class="row vertical-align">
                                        <div class="col-xs-2">
                                            <i class="fa fa-exclamation-triangle fa-2x"></i>
                                        </div>
                                        <div class="col-xs-10"  style="font-size: 18px;">
                                            All Required Data is Entered
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        
                        <div class="col-lg-6">
                            <div class="alert alert-warning" role="alert">
                                <div class="row vertical-align">
                                    <div class="col-xs-10"  style="font-size: 18px;">
                                        ActionOI ID: {{$costcenter->actionoi}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="alert alert-warning" role="alert">
                                <div class="row vertical-align">
                                    <div class="col-xs-10"  style="font-size: 18px;">
                                        @if(!is_null($costcenter->uosName))
                                            Unit of Service: {{$costcenter->uosName->unit_of_service}}
                                        @else
                                            <b>Not Entered</b>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        @if(empty($rollup))
                            <div class="col-lg-12">
                                <div class="alert alert-danger" role="alert">
                                    <div class="row vertical-align">
                                        <div class="col-xs-1">
                                            <i class="fa fa-exclamation-triangle fa-2x"></i>
                                        </div>
                                        <div class="col-xs-10"  style="font-size: 18px;">
                                            NO DATA AVAILABLE
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @elseif($rolledupto->count() >1)
                            <div class="col-lg-12">
                                <div class="alert alert-info" role="alert">
                                    <div class="row vertical-align">
                                        <div class="col-xs-1">
                                            <i class="fa fa-exclamation-triangle fa-2x"></i>
                                        </div>
                                        <div class="col-xs-10"  style="font-size: 18px;">
                                            This cost center has the following cost centers rolled up to it:
                                            @foreach($rolledupto as $rolledup)
                                                <a href="/costcenters/{{$rolledup->cost_center}}" >{{$rolledup->cost_center}}</a>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @elseif($rolledupto->count() <= 1 AND $rollup->cost_center_roll_up == $rollup->cost_center)
                            <div class="col-lg-12">
                                <div class="alert alert-info" role="alert">
                                    <div class="row vertical-align">
                                        <div class="col-xs-1">
                                            <i class="fa fa-exclamation-triangle fa-2x"></i>
                                        </div>
                                        <div class="col-xs-10"  style="font-size: 18px;">
                                            This cost center has no other cost centers rolled up into it.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="col-lg-12">
                                <div class="alert alert-info" role="alert">
                                    <div class="row vertical-align">
                                        <div class="col-xs-1">
                                            <i class="fa fa-exclamation-triangle fa-2x"></i>
                                        </div>
                                        <div class="col-xs-10"  style="font-size: 18px;">
                                            This cost center is rolled up to: <a href="/costcenters/{{$rollup->cost_center_roll_up}}" >{{$rollup->cost_center_roll_up}}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <table class="table table-condensed table-hover table-bordered table-dark">
                            <thead>
                            <tr>
                                <td><b>Cost Center</b></td>
                                <td><b>Director</b></td>
                                <td><b>Date</b></td>
                                <td><b>Worked PI Var</b></td>
                                <td><b>Activity</b></td>
                                <td><b>Budget</b></td>
                                <td><b>Variance</b></td>
                                <td><b>Percent</b></td>
                                <td><b>Activity</b></td>
                                <td><b>Budget</b></td>
                                <td><b>Variance</b></td>
                                <td><b>Percent</b></td>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td colspan="4"></td>
                                <td colspan="4" align="center" bgcolor="#f08080"><b>Expense</b></td>
                                <td colspan="4" align="center" bgcolor="#adff2f"><b>Revenue</b></td>
                            </tr>
                            <tr>
                                <td>{{$costcenter->style1}}</td>
                                <td>@if(!is_null($costcenter->directorEmployee)){{$costcenter->directorEmployee->name}} @endif</td>
                                <td>{{$costcenter->pro_click['month']}}/{{$costcenter->pro_click['year']}}</td>
                                <td>@if(!is_null($costcenter->sgmc_pi['pi']))
                                        <i class="fas fa-circle"
                                        @if($costcenter->sgmc_pi['pi'] < 85)
                                        style="color:red;"
                                        @elseif($costcenter->sgmc_pi['pi'] >= 85 AND $costcenter->sgmc_pi['pi'] < 95 )
                                        style="color:yellow;"
                                        @elseif($costcenter->sgmc_pi['pi'] > 95 )
                                        style="color:green;"
                                        @endif

                                        ></i>
                                        {{$costcenter->sgmc_pi['pi']}}%
                                    @else
                                        N/A

                                    @endif

                                </td>
                                <td>${{number_format($costcenter->pro_click['activity'],2)}}</td>
                                <td>${{number_format($costcenter->pro_click['budget'],2)}}</td>
                                <td @if($costcenter->pro_click['variance'] < 0) style="color:red;" @endif>${{number_format($costcenter->pro_click['variance'],2)}}</td>
                                <td><i class="fas fa-circle"
                                    @if($costcenter->pro_click['var_percent'] < 80)
                                    style="color:red;"
                                    @elseif($costcenter->pro_click['var_percent'] > 80 AND $costcenter->pro_click['var_percent'] < 90 )
                                    style="color:yellow;"
                                    @elseif($costcenter->pro_click['var_percent'] > 90 )
                                    style="color:green;"
                                            @endif


                                    ></i> {{number_format($costcenter->pro_click['var_percent'],2)}}%</td>
                                <td>${{number_format($costcenter->pro_clickRevenue['activity'],2)}}</td>
                                <td>${{number_format($costcenter->pro_clickRevenue['budget'],2)}}</td>
                                <td @if($costcenter->pro_clickRevenue['variance'] < 0) style="color:red;" @endif>${{number_format($costcenter->pro_clickRevenue['variance'],2)}}</td>
                                <td><i class="fas fa-circle"
                                    @if($costcenter->pro_clickRevenue['var_percent'] < 80)
                                    style="color:red;"
                                    @elseif($costcenter->pro_clickRevenue['var_percent'] > 80 AND $costcenter->pro_click['var_percent'] < 90 )
                                    style="color:yellow;"
                                    @elseif($costcenter->pro_clickRevenue['var_percent'] > 90 )
                                    style="color:green;"
                                            @endif
                                    ></i> {{number_format($costcenter->pro_clickRevenue['var_percent'],2)}}%
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> <!-- HOME -->

            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                <div class="card-body">
                    <div class="card-title">
                        <h3>Clocked In ({{$clockedin->count()}})</h3>
                    </div>
                    <div class="col-lg-8">
                        <table class="table table-striped table-condensed table-dark">
                            <thead>
                            <tr>
                                <td></td>
                                <td><b>Employee</b></td>
                                <td align="center"><b>Job Code</b></td>
                                <td><b>Title</b></td>
                                <td align="right"><b>Employee #</b></td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($clockedin as $clocked)
                                <tr>
                                    <td>@if(is_null($clocked->empoyee) OR $clocked->empoyee == ''){{ Html::image('img/nopic.png', 'alt', array( 'width' => 60, 'height' => 60 )) }} @else <a href="data:image/jpeg;base64,{{str_replace('</photo>','',str_replace('<photo>','',$clocked->empoyee->photo))}}" target="_blank"><img src="data:image/jpeg;base64,{{str_replace('</photo>','',str_replace('<photo>','',$clocked->empoyee->photo))}}" height="60" width="60"/></a>@endif</td>
                                    <td>@if(!is_null($clocked->name)) {{$clocked->name}}@endif</td>
                                    <td align="center">@if(!is_null($clocked->empoyee)){{$clocked->empoyee->job_code}}@endif</td>
                                    <td>@if(!is_null($clocked->empoyee)){{$clocked->empoyee->title}}@endif</td>
                                    <td align="right">{{$clocked->employee_number}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> <!-- CLOCKED IN -->

            <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                <div class="card-body">
                    <form method="post" action="/costcenters/edit/pi/{{ $costcenter->id }}">
                        {{-- <input type="hidden" value="{{$costcenter->id}}" name="id"> --}}
                        {{ csrf_field() }}
                        <div class="card-title">
                            <h3>PI</h3>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="title_id">ActionOI Number</label>
                                <input type="text" name="actionoi" value="{{$costcenter->actionoi}}" class="form-control" id="title_id">
                            </div>
                            <div class="form-group">
                                <label for="title_id">PI Normalized</label>
                                <input type="number" name="pi_norm" value="{{$costcenter->pi_norm}}" step="0.01" class="form-control" id="title_id">
                            </div>
                            <div class="form-group">
                                <label for="title_id">PI Actual</label>
                                <input type="number" name="pi_act" value="{{$costcenter->pi_act}}" step="0.01" class="form-control" id="title_id">
                            </div>
                            <div class="form-group">
                                <label for="uos">UOS</label>
                                <select name="flexed" class="form-control">
                                    @if(!empty($costcenter->uosName))
                                        <option selected value="{{$costcenter->flexed}}">{{$costcenter->uosName->unit_of_service}}</option>
                                    @else
                                        <option selected value="">[Choose UOS]</option>
                                    @endif
                                    @foreach ($uos as $unit)
                                        <option value="{{$unit->id}}">{{$unit->unit_of_service}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="title_id">UOS Amount</label>
                                <input type="number" name="uos_amount" value="{{$costcenter->uos_amount}}" step="0.01" class="form-control" id="title_id">
                            </div>
                            <div class="form-group">
                                <label for="title_id">Paid FTE's</label>
                                <input type="number" name="paid_fte" value="{{$costcenter->paid_fte}}" step="0.01" class="form-control" id="title_id">
                            </div>
                            <div class="form-group">
                                <label for="title_id">Worked Target</label>
                                <input type="number" name="worked_target" value="{{$costcenter->worked_target}}" step="0.01" class="form-control" id="title_id">
                            </div>
                            <input type="Submit" class="btn btn-primary" value="Update PI">
                        </div>
                    </form>
                </div>
            </div> <!-- PI -->

            <div class="tab-pane fade" id="nav-notes" role="tabpanel" aria-labelledby="nav-notes-tab">
                <div class="card-body">
                    <form method="post" action="/costcenters/notes">
                        <input type="hidden" value="{{Auth::user()->employee_number}}" name="created_by">
                        <input type="hidden" value="{{$costcenter->cost_center}}" name="cost_center">
                        {{ csrf_field() }}
                        <div class="card-title">
                            <h3>Notes</h3>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <textarea class="form-control" id="exampleFormControlTextarea1" name="notes" rows="10"></textarea>
                            </div>
                            <input type="Submit" class="btn btn-sm btn btn-default" value="Add Notes">

                        </div>
                    </form>
                    <div class="col-lg-8">

                        @if($costcenter_notes->isEmpty())

                        @else
                            @foreach($costcenter_notes as $notes)
                                <p><b>{{$notes->createdBy->name}}</b> - {{$notes->created_at}}</p>
                                <p>{{$notes->notes}}</p>

                                <hr>
                            @endforeach
                        @endif

                    </div>
                </div>
            </div> <!-- NOTES -->

            <div class="tab-pane fade" id="nav-edit" role="tabpanel" aria-labelledby="nav-edit-tab">
                <div class="card-body">
                    <form method="post" action="/costcenters/edit">
                        {{ csrf_field() }}
                        <input type="hidden" value="{{$costcenter->id}}" name="id">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="title_id">Cost Center Number</label>
                                <input type="text" name="cost_center" value="{{$costcenter->cost_center}}" class="form-control" id="title_id" minlength="4" maxlength="4" required>
                            </div>
                            <div class="form-group">
                                <label for="title_id">Cost Center Name</label>
                                <input type="text" name="unit_code_description" value="{{$costcenter->unit_code_description}}" class="form-control" id="title_id" required>
                            </div>
                            <div class="form-group">
                                <label for="required_id">Unit of Service</label>
                                <select class="form-control" id="required_id" name="flexed">
                                    @if(is_null($costcenter->flexed) OR empty($costcenter->flexed))
                                        <option selected value="{{$costcenter->flexed}}">[Select]</option>
                                    @else
                                        <option selected value="{{$costcenter->flexed}}">{{$costcenter->uosName->unit_of_service}}</option>
                                    @endif
                                    @foreach($uos as $unit)
                                        <option value="{{$unit->id}}">{{$unit->unit_of_service}}</option>
                                    @Endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="required_id">Clinical Department?</label>
                                <select class="form-control" id="required_id" name="clinical">
                                    <option selected  value="{{$costcenter->clinical}}">@if($costcenter->clinical ==1) Yes @else No @endif</option>
                                    <option value="0">No</option>
                                    <option value="1">Yes</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="required_id">Is Cost Center Active?</label>
                                <select class="form-control" id="required_id" name="active_status">
                                    <option selected  value="{{$costcenter->active_status}}">@if($costcenter->active_status ==1) Yes @else No @endif</option>
                                    <option value="0">No</option>
                                    <option value="1">Yes</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Cost Center Rollup</label><br>
                                <select name="cost_center_roll_up" class="basic" style="width:100%;">
                                    @if(!empty($rollup))
                                        <option value="{{$rollup->cost_center_roll_up}}">{{$rollup->cost_center_roll_up}}</option>
                                    @else 
                                        <option value="">[Choose Roll Up]</option>
                                    @endif
                                    @foreach ($costcenters as $roll)
                                        <option value="{{$roll->cost_center}}">{{$roll->cost_center}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="required_id">C-Suite</label>
                                <div class="input-group">
                                    <select name="csuite" class="form-control" style="width:100%;">
                                        <option value="{{ $costcenter->csuite }}" selected>{{ $costcenter->csuite }}</option>
                                        <option value="">[Set To Empty]</option>
                                        @foreach ($csuites as $csuite)
                                            <option value="{{$csuite->csuite}}">{{$csuite->csuite}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="required_id">Administrator</label>
                                <div class="input-group">
                                    <select name="administrator" class="basic" style="width:100%;">
                                        <option value="{{ $costcenter->administrator }}" selected>@if(!empty($costcenter->administrator)){{ $costcenter->adminEmployee->name }} @else @endif</option>
                                        <option value="">[Set To Empty]</option>
                                        @foreach ($emps as $emp)
                                            <option value="{{$emp->employee_number}}">{{$emp->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="required_id">Director</label>
                                <div class="input-group">
                                    <select name="director" class="basic" style="width:100%;">
                                        <option value="{{ $costcenter->director }}" selected>@if(!empty($costcenter->director)){{ $costcenter->directorEmployee->name }} @else @endif</option>
                                        <option value="">[Set To Empty]</option>
                                        @foreach ($emps as $emp)
                                            <option value="{{$emp->employee_number}}">{{$emp->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="required_id">Manager</label>
                                <div class="input-group">
                                    <select name="manager" class="basic" style="width:100%;">
                                        <option value="{{ $costcenter->manager }}" selected>@if(!empty($costcenter->manager)){{ $costcenter->managerEmployee->name }} @else  @endif</option>
                                        <option value="">[Set To Empty]</option>
                                        @foreach ($emps as $emp)
                                            <option value="{{$emp->employee_number}}">{{$emp->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <input type="Submit" class="btn btn-primary" value="Update Cost Center">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


 <!-- ??? -->

 <!--
{{-- 
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <b>Business Intelligence - Cost Center Detail</b>
            </div>
            <div class="panel-body">
                <p style="font-weight: bold; font-size: 20px;">{{$costcenter->style1}}</p>
                <ul class="nav nav-pills">
                    <li class="active"><a data-toggle="pill" href="#home">Home</a></li>
                    <li><a data-toggle="pill" href="#menu2">Clocked in Employees</a></li>
                    <li><a data-toggle="pill" href="#menu4">PI</a></li>
                    <li><a data-toggle="pill" href="#menu3">Notes</a></li>
                    <li><a data-toggle="pill" href="#menu1">Edit</a></li>
                    <li><a href="/costcenters">Search</a></li>
                </ul>

                <div class="tab-content">
                    <div id="home" class="tab-pane fade in active">
                        <hr>

                        @if(is_null($costcenter->csuiteEmployee) or is_null($costcenter->directorEmployee))
                            <div class="col-lg-4">
                                <div class="alert alert-danger" role="alert">
                                    <div class="row vertical-align">
                                        <div class="col-xs-2">
                                            <i class="fa fa-exclamation-triangle fa-2x"></i>
                                        </div>
                                        <div class="col-xs-10" style="font-size: 18px;">
                                            Required Cost Center Data is Missing!
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="col-lg-4">
                                <div class="alert alert-success" role="alert">
                                    <div class="row vertical-align">
                                        <div class="col-xs-2">
                                            <i class="fa fa-exclamation-triangle fa-2x"></i>
                                        </div>
                                        <div class="col-xs-10"  style="font-size: 18px;">
                                            All Required Data is Entered
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div class="col-lg-4">
                            <div class="alert alert-warning" role="alert">
                                <div class="row vertical-align">
                                    <div class="col-xs-10"  style="font-size: 18px;">
                                        ActionOI ID: {{$costcenter->actionoi}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="alert alert-warning" role="alert">
                                <div class="row vertical-align">
                                    <div class="col-xs-10"  style="font-size: 18px;">
                                        @if(!is_null($costcenter->uosName))
                                        Unit of Service: {{$costcenter->uosName->unit_of_service}}
                                        @else
                                        <b>Not Entered</b>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if($rolledupto->count() >1)
                            <div class="col-lg-12">
                                <div class="alert alert-info" role="alert">
                                    <div class="row vertical-align">
                                        <div class="col-xs-1">
                                            <i class="fa fa-exclamation-triangle fa-2x"></i>
                                        </div>
                                        <div class="col-xs-10"  style="font-size: 18px;">
                                            This cost center has the following cost centers rolled up to it:
                                            @foreach($rolledupto as $rolledup)
                                                <a href="/costcenters/{{$rolledup->cost_center}}" >{{$rolledup->cost_center}}</a>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @elseif($rolledupto->count() <= 1 AND $rollup->cost_center_roll_up == $rollup->cost_center)
                            <div class="col-lg-12">
                                <div class="alert alert-info" role="alert">
                                    <div class="row vertical-align">
                                        <div class="col-xs-1">
                                            <i class="fa fa-exclamation-triangle fa-2x"></i>
                                        </div>
                                        <div class="col-xs-10"  style="font-size: 18px;">
                                            This cost center has no other cost centers rolled up into it.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="col-lg-12">
                                <div class="alert alert-info" role="alert">
                                    <div class="row vertical-align">
                                        <div class="col-xs-1">
                                            <i class="fa fa-exclamation-triangle fa-2x"></i>
                                        </div>
                                        <div class="col-xs-10"  style="font-size: 18px;">
                                            This cost center is rolled up to: <a href="/costcenters/{{$rollup->cost_center_roll_up}}" >{{$rollup->cost_center_roll_up}}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif


                        <div class="col-lg-12">
                            <table class="table table-condensed table-hover table-bordered">
                                <thead>
                                <tr>
                                    <td><b>Cost Center</b></td>
                                    <td><b>Director</b></td>
                                    <td><b>Date</b></td>
                                    <td><b>Worked PI Var</b></td>
                                    <td><b>Activity</b></td>
                                    <td><b>Budget</b></td>
                                    <td><b>Variance</b></td>
                                    <td><b>Percent</b></td>
                                    <td><b>Activity</b></td>
                                    <td><b>Budget</b></td>
                                    <td><b>Variance</b></td>
                                    <td><b>Percent</b></td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td colspan="4"></td>
                                    <td colspan="4" align="center" bgcolor="#f08080"><b>Expense</b></td>
                                    <td colspan="4" align="center" bgcolor="#adff2f"><b>Revenue</b></td>
                                </tr>
                                <tr>
                                    <td>{{$costcenter->style1}}</td>
                                    <td>@if(!is_null($costcenter->directorEmployee)){{$costcenter->directorEmployee->name}} @endif</td>
                                    <td>{{$costcenter->pro_click['month']}}/{{$costcenter->pro_click['year']}}</td>
                                    <td>@if(!is_null($costcenter->sgmc_pi['pi']))
                                            <i class="fas fa-circle"
                                               @if($costcenter->sgmc_pi['pi'] < 85)
                                               style="color:red;"
                                               @elseif($costcenter->sgmc_pi['pi'] >= 85 AND $costcenter->sgmc_pi['pi'] < 95 )
                                               style="color:yellow;"
                                               @elseif($costcenter->sgmc_pi['pi'] > 95 )
                                               style="color:green;"
                                                    @endif

                                            ></i>
                                            {{$costcenter->sgmc_pi['pi']}}%
                                        @else
                                            N/A

                                        @endif

                                    </td>
                                    <td>${{number_format($costcenter->pro_click['activity'],2)}}</td>
                                    <td>${{number_format($costcenter->pro_click['budget'],2)}}</td>
                                    <td @if($costcenter->pro_click['variance'] < 0) style="color:red;" @endif>${{number_format($costcenter->pro_click['variance'],2)}}</td>
                                    <td><i class="fas fa-circle"
                                           @if($costcenter->pro_click['var_percent'] < 80)
                                           style="color:red;"
                                           @elseif($costcenter->pro_click['var_percent'] > 80 AND $costcenter->pro_click['var_percent'] < 90 )
                                           style="color:yellow;"
                                           @elseif($costcenter->pro_click['var_percent'] > 90 )
                                           style="color:green;"
                                                @endif


                                        ></i> {{number_format($costcenter->pro_click['var_percent'],2)}}%</td>
                                    <td>${{number_format($costcenter->pro_clickRevenue['activity'],2)}}</td>
                                    <td>${{number_format($costcenter->pro_clickRevenue['budget'],2)}}</td>
                                    <td @if($costcenter->pro_clickRevenue['variance'] < 0) style="color:red;" @endif>${{number_format($costcenter->pro_clickRevenue['variance'],2)}}</td>
                                    <td><i class="fas fa-circle"
                                           @if($costcenter->pro_clickRevenue['var_percent'] < 80)
                                           style="color:red;"
                                           @elseif($costcenter->pro_clickRevenue['var_percent'] > 80 AND $costcenter->pro_click['var_percent'] < 90 )
                                           style="color:yellow;"
                                           @elseif($costcenter->pro_clickRevenue['var_percent'] > 90 )
                                           style="color:green;"
                                                @endif
                                        ></i> {{number_format($costcenter->pro_clickRevenue['var_percent'],2)}}%
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                    <div id="menu1" class="tab-pane fade">
                        <form method="post" action="/costcenters/edit">
                            {{ csrf_field() }}
                            <input type="hidden" value="{{$costcenter->id}}" name="id">
                            <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="title_id">Cost Center Number</label>
                                        <input type="text" name="cost_center" value="{{$costcenter->cost_center}}" class="form-control" id="title_id" minlength="4" maxlength="4" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="title_id">Cost Center Name</label>
                                        <input type="text" name="unit_code_description" value="{{$costcenter->unit_code_description}}" class="form-control" id="title_id" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="required_id">Unit of Service</label>
                                        <select class="form-control" id="required_id" name="flexed">
                                            @if(is_null($costcenter->flexed) OR empty($costcenter->flexed))
                                                <option selected value="{{$costcenter->flexed}}">[Select]</option>
                                            @else
                                                <option selected value="{{$costcenter->flexed}}">{{$costcenter->uosName->unit_of_service}}</option>
                                            @endif
                                            @foreach($uos as $unit)
                                            <option value="{{$unit->id}}">{{$unit->unit_of_service}}</option>
                                            @Endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="required_id">Clinical Department?</label>
                                        <select class="form-control" id="required_id" name="clinical">
                                            <option selected  value="{{$costcenter->clinical}}">@if($costcenter->clinical ==1) Yes @else No @endif</option>
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="required_id">Is Cost Center Active?</label>
                                        <select class="form-control" id="required_id" name="active_status">
                                            <option selected  value="{{$costcenter->active_status}}">@if($costcenter->active_status ==1) Yes @else No @endif</option>
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="required_id">C-Suite</label>
                                    <div class="input-group">
                                        <input type="hidden" class="form-control input-sm" name="csuite" value="{{$costcenter->csuite}}" id="csuite_value_{{$costcenter->cost_center}}">
                                        <input type="search" id="csuite_{{$costcenter->cost_center}}" @if(!is_null($costcenter->csuite)) value="{{$costcenter->csuiteEmployee->name}}" @endif class="form-control" placeholder="C-Suite" >
                                        <span class="input-group-btn">
                                        <input type="button" class="btn btn-default" onclick="document.getElementById('csuite_{{$costcenter->cost_center}}').value = '', document.getElementById('csuite_value_{{$costcenter->cost_center}}').value = ''" value="Clear C-Suite" id="btn" />
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="required_id">Administrator</label>
                                    <div class="input-group">
                                        <input type="hidden" class="form-control input-sm" name="administrator" value="{{$costcenter->administrator}}" id="admin_value_{{$costcenter->cost_center}}">
                                        <input type="search" id="admin_{{$costcenter->cost_center}}" @if(!is_null($costcenter->administrator)) value="{{$costcenter->adminEmployee->name}}" @endif class="form-control" placeholder="Admin" >
                                        <span class="input-group-btn">
                                        <input type="button" class="btn btn-default" onclick="document.getElementById('admin_{{$costcenter->cost_center}}').value = '', document.getElementById('admin_value_{{$costcenter->cost_center}}').value = ''" value="Clear Administrator" id="btn" />
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="required_id">Director</label>
                                    <div class="input-group">
                                        <input type="hidden" class="form-control input-sm" name="director"  value="{{$costcenter->director}}" id="director_value_{{$costcenter->cost_center}}">
                                        <input type="search" id="director_{{$costcenter->cost_center}}" @if(!is_null($costcenter->director)) value="{{$costcenter->directorEmployee->name}}" @endif class="form-control" placeholder="Director" >
                                        <span class="input-group-btn">
                                        <input type="button" class="btn btn-default" onclick="document.getElementById('director_{{$costcenter->cost_center}}').value = '', document.getElementById('director_value_{{$costcenter->cost_center}}').value = ''" value="Clear Director" id="btn" />
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="required_id">Manager</label>
                                    <div class="input-group">
                                    <input type="hidden" class="form-control input-sm" name="manager"  value="{{$costcenter->manager}}" id="manager_value_{{$costcenter->cost_center}}">
                                    <input type="search" id="manager_{{$costcenter->cost_center}}" @if(!is_null($costcenter->manager)) value="{{$costcenter->managerEmployee->name}}" @endif class="form-control" placeholder="Manager" >
                                        <span class="input-group-btn">
                                        <input type="button" class="btn btn-default" onclick="document.getElementById('manager_{{$costcenter->cost_center}}').value = '', document.getElementById('manager_value_{{$costcenter->cost_center}}').value = ''" value="Clear Manager" id="btn" />
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="required_id">Analyst</label>
                                    <div class="input-group">
                                    <input type="hidden" class="form-control input-sm" name="analyst"  value="{{$costcenter->analyst}}" id="analyst_value_{{$costcenter->cost_center}}">
                                    <input type="search" id="analyst_{{$costcenter->cost_center}}" @if(!is_null($costcenter->analyst)) value="{{$costcenter->analystEmployee->name}}" @endif class="form-control" placeholder="Analyst" >
                                        <span class="input-group-btn">
                                        <input type="button" class="btn btn-default" onclick="document.getElementById('analyst_{{$costcenter->cost_center}}').value = '', document.getElementById('analyst_value_{{$costcenter->cost_center}}').value = ''" value="Clear Analyst" id="btn" />
                                        </span>
                                    </div>
                                </div>
                            </div>
                        <input type="Submit" class="btn btn-sm btn btn-default" value="Update Cost Center">
                        </form>
                    </div>
                    <div id="menu2" class="tab-pane fade">
                        <h3>Clocked In ({{$clockedin->count()}})</h3>
                        <div class="col-lg-6">
                            <table class="table table-striped table-condensed">
                                <thead>
                                    <tr>
                                        <td><b>Employee</b></td>
                                        <td align="center"><b>Job Code</b></td>
                                        <td><b>Title</b></td>
                                        <td align="right"><b>Employee #</b></td>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($clockedin as $clocked)
                                    <tr>
                                        <td>{{$clocked->name}}</td>
                                        <td align="center">@if(!is_null($clocked->empoyee)){{$clocked->empoyee->job_code}}@endif</td>
                                        <td>@if(!is_null($clocked->empoyee)){{$clocked->empoyee->title}}@endif</td>
                                        <td align="right">{{$clocked->employee_number}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="menu3" class="tab-pane fade">
                        <form method="post" action="/costcenters/notes">
                            <input type="hidden" value="{{Auth::user()->employee_number}}" name="created_by">
                            <input type="hidden" value="{{$costcenter->cost_center}}" name="cost_center">
                            {{ csrf_field() }}
                            <h3>Notes</h3>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <textarea class="form-control" id="exampleFormControlTextarea1" name="notes" rows="10"></textarea>
                                </div>
                                <input type="Submit" class="btn btn-sm btn btn-default" value="Add Notes">

                            </div>
                        </form>
                            <div class="col-lg-8">

                                @if($costcenter_notes->isEmpty())

                                @else
                                    @foreach($costcenter_notes as $notes)
                                    <p><b>{{$notes->createdBy->name}}</b> - {{$notes->created_at}}</p>
                                    <p>{{$notes->notes}}</p>

                                    <hr>
                                    @endforeach
                                @endif

                            </div>
                    </div>
                    <div id="menu4" class="tab-pane fade">
                        <form method="post" action="/costcenters/edit/pi">
                            <input type="hidden" value="{{$costcenter->id}}" name="id">
                            {{ csrf_field() }}
                            <h3>PI</h3>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label for="title_id">ActionOI Number</label>
                                    <input type="text" name="actionoi" value="{{$costcenter->actionoi}}" class="form-control" id="title_id">
                                </div>
                                <div class="form-group">
                                    <label for="title_id">PI Normalized</label>
                                    <input type="number" name="pi_norm" value="{{$costcenter->pi_norm}}" step="0.01" class="form-control" id="title_id">
                                </div>
                                <div class="form-group">
                                    <label for="title_id">PI Actual</label>
                                    <input type="number" name="pi_act" value="{{$costcenter->pi_act}}" step="0.01" class="form-control" id="title_id">
                                </div>
                                <div class="form-group">
                                    <label for="title_id">UOS Amount</label>
                                    <input type="number" name="uos_amount" value="{{$costcenter->uos_amount}}" step="0.01" class="form-control" id="title_id">
                                </div>
                                <div class="form-group">
                                    <label for="title_id">Paid FET's</label>
                                    <input type="number" name="paid_fte" value="{{$costcenter->paid_fte}}" step="0.01" class="form-control" id="title_id">
                                </div>
                                <div class="form-group">
                                    <label for="title_id">Worked Target</label>
                                    <input type="number" name="worked_target" value="{{$costcenter->worked_target}}" step="0.01" class="form-control" id="title_id">
                                </div>
                                <input type="Submit" class="btn btn-sm btn btn-default" value="Update PI">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}

-->

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')
    <script type="application/javascript">
        $('#btn').click(function(){
            $('#csuite').each(function(){
                $(this).val('');
            });
        });

        $('#csuite_{{$costcenter->cost_center}}').autocomplete({
            serviceUrl: '/survey/employeesearch',
            dataType: 'json',
            type:'GET',
            width: 418,
            minChars:2,
            onSelect: function(suggestion) {
                //alert(suggestion.data);
                $("#csuite_value_{{$costcenter->cost_center}}").val(suggestion.data);
            }
        });

        $('#admin_{{$costcenter->cost_center}}').autocomplete({
            serviceUrl: '/survey/employeesearch',
            dataType: 'json',
            type:'GET',
            width: 418,
            minChars:2,
            onSelect: function(suggestion) {
                //alert(suggestion.data);
                $("#admin_value_{{$costcenter->cost_center}}").val(suggestion.data);
            }
        });

        $('#director_{{$costcenter->cost_center}}').autocomplete({
            serviceUrl: '/survey/employeesearch',
            dataType: 'json',
            type:'GET',
            width: 418,
            minChars:2,
            onSelect: function(suggestion) {
                //alert(suggestion.data);
                $("#director_value_{{$costcenter->cost_center}}").val(suggestion.data);
            }
        });

        $('#manager_{{$costcenter->cost_center}}').autocomplete({
            serviceUrl: '/survey/employeesearch',
            dataType: 'json',
            type:'GET',
            width: 418,
            minChars:2,
            onSelect: function(suggestion) {
                //alert(suggestion.data);
                $("#manager_value_{{$costcenter->cost_center}}").val(suggestion.data);
            }
        });

        $('#analyst_{{$costcenter->cost_center}}').autocomplete({
            serviceUrl: '/survey/employeesearch',
            dataType: 'json',
            type:'GET',
            width: 418,
            minChars:2,
            onSelect: function(suggestion) {
                //alert(suggestion.data);
                $("#analyst_value_{{$costcenter->cost_center}}").val(suggestion.data);
            }
        });
    </script>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script>
    $(".basic").select2({
        allowClear: true
    });
</script>


@endsection
