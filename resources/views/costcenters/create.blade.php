
@extends('layouts.app')
{{--Page Design Goes Below--}}
@section('content')

    <div class="card bg-dark text-white mb-3">
        <div class="card-header">
            Create New Cost Center
        </div>
        <form action="/costcenter/store" method="POST">
            {{ csrf_field() }}

            <div class="card-body">

                <div class="form-group">
                    <label>Cost Center #</label>
                    <input type="text" name="cost_center" class="form-control">
                </div>

                <div class="form-group">
                    <label>Unit Code Description</label>
                    <input type="text" name="unit_code_description" class="form-control">
                </div>

            </div>

            <div class="card-footer">

                <input type="submit" class="btn btn-primary">

            </div>

        </form>
    </div>

@endsection


@section('scripts')


@endsection