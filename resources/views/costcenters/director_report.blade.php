{{--New file Template--}}



@extends('layouts.app')
{{--Page Design Goes Below--}}
@section('content')
    <div class="col-md-12">
        <div class="card bg-dark text-white mb-3">
            <div class="card-header">
                <b>Director Score Card</b>
            </div>
            <div class="card-body">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/costcenters/report">C-Suite</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$csuite_name->name}}</li>
                    </ol>
                </nav>
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <td><b>Director</b></td>
                        <td align="center"><b># CC</b></td>
                        <td align="center"><b>PI Percent</b></td>
                        <td align="center"><b>Paid FTE's</b></td>
                        <td align="center"><b>Worked FTE's</b></td>
                        <td align="center"><b>Expected FTE's</b></td>
                        <td align="center"><b>Variance FTE's</b></td>
                        <td align="center"><b>Contractors</b></td>
                        <td align="center"><b>Contract Exp</b></td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($director as $dir)
                        <tr>
                            <td>{{$dir->name}}</td>
                            <td align="center">{{$dir->totalcc}}</td>

                            @if($dir->PI == 0.00)
                                <td align="center" style="color:black; font-weight: bold">{{number_format($dir->PI,2)}}%</td>
                            @elseif($dir->PI < 90)
                                <td align="center" style="color:red;font-weight: bold">{{number_format($dir->PI,2)}}%</td>
                            @elseif($dir->PI > 91 AND $dir->PI <= 95)
                                <td align="center" style="color:black;">{{number_format($dir->PI,2)}}%</td>
                            @else
                                <td align="center" style="color:green;font-weight: bold">{{number_format($dir->PI,2)}}%</td>
                            @endif
                            <td align="center">{{number_format($dir->paid_fte,2)}}</td>
                            <td align="center">{{number_format($dir->worked_fte,2)}}</td>
                            <td align="center">{{number_format($dir->expected_fte,2)}}</td>
                            @if($dir->fte_variance > 0)
                            <td align="center" style="color:red;font-weight: bold">{{number_format($dir->fte_variance,2)}}</td>
                            @else
                                <td align="center">{{number_format($dir->fte_variance,2)}}</td>
                            @endif

                            @if($dir->contract_fte > 0)
                                <td align="center" style="font-weight: bold;color:red;">{{number_format($dir->contract_fte,2)}}</td>
                            @else
                                <td align="center" style="font-weight: bold;color:green;">{{number_format($dir->contract_fte,2)}}</td>
                            @endif
                            <td align="center">${{number_format($dir->contract_expense,2)}}</td>
                            <td align="right"><a href="/costcenters/report_cc/{{$dir->director}}" class="btn btn-default btn-xs"><i class="fas fa-plus-square"></i> Cost Centers</a></td>
                        </tr>
                    @endforeach
                    <tr>
                        <td ><b>Total</b></td>
                        <td align="center"><b>{{$director->sum('totalcc')}}</b></td>

                        <td align="center"><b>{{number_format($director->avg('PI'),2)}}</b></td>
                        <td align="center">{{number_format($director->sum('paid_fte'),2)}}</td>
                        <td align="center">{{number_format($director->sum('worked_fte'),2)}}</td>
                        <td align="center">{{number_format($director->sum('expected_fte'),2)}}</td>
                        @if($director->sum('fte_variance') > 0)
                            <td align="center" style="font-weight: bold;color:red;">{{number_format($director->sum('fte_variance'),2)}}</td>
                        @else
                            <td align="center" style="font-weight: bold;color:green;">{{number_format($director->sum('fte_variance'),2)}}</td>
                        @endif

                        @if($director->sum('contract_fte') > 0)
                            <td align="center" style="font-weight: bold;color:red;">{{number_format($director->sum('contract_fte'),2)}}</td>
                        @else
                            <td align="center" style="font-weight: bold;color:green;">{{number_format($director->sum('contract_fte'),2)}}</td>
                        @endif
                        <td align="center">{{number_format($director->sum('contract_expense'),2)}}</td>
                        <td colspan="3"></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="panel-footer">
                @include('costcenters.footer_report')
            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
