<table class="table table-condensed">
    <tr>
        <td><i>* Variance dollars are gross dollars.</i></td>
    </tr>
    <tr>
        <td><i>* PI is the last pay period of the month</i></td>
    </tr>
    <tr>
        <td><i>* FTE Variance is a SUM of the FTE Variance in HBI</i></td>
    </tr>
</table>