{{--New file Template--}}


@extends('layouts.app')
{{--Page Design Goes Below--}}
@section('content')

<div class="col-lg-12 m-auto">
    <table class="table table-dark table-bordered table-striped table-hover" id="costcenters">
        <thead>
            <th>Cost Center</th>
            <th>Target MH/Stat</th>
            <th>Stat</th>
            <th>Stat Link</th>
            <th>Administrator</th>
            <th>Division</th>
            <th>LE Denominator</th>
            <th>Update</th>
        </thead>
        <tbody>
            @foreach($ccs as $cc)
                <tr>
                    <form action="/costcenters/update/LE/{{ $cc->id }}" method="POST">
                        {{ csrf_field() }}
                        
                        <td><a href="/stats/search?search={{ $cc->cost_center }}" class="text-white"">{{ $cc->style1 }}</a></td>
                        <td>
                            <input type="number" step="0.0001" class="form-control" name="worked_target" @if(!is_null($cc->worked_target)) value={{ $cc->worked_target }} @else value=0 @endif>
                        </td>
                        <td>
                            <input type="text" class="form-control" name="uos" value="{{ $cc->uos }}">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="stat_link" value="{{ $cc->stat_link }}">
                        </td>
                        <td>
                            <select name="administrator" class="form-control" required>
                                <option value="{{ $cc->administrator }}" selected>@if(!is_null($cc->administrator)) {{$cc->adminEmployee->name}} @else  @endif</option>
                                @foreach($adminselect as $admin)
                                    <option value="{{ $admin->employee_number }}">{{ $admin->name }}</option>
                                @endforeach
                            </select>
                        </td>
                        <td>
                            <select name="csuite" class="form-control" required>
                                <option value="{{ $cc->csuite }}" selected>{{$cc->csuite}}</option>
                                @foreach($divisionSelect as $div)
                                    <option value="{{ $div->csuite }}">{{ $div->csuite }}</option>
                                @endforeach
                            </select>
                        </td>
                        <td>
                            <input type="number" step="0.0001" class="form-control" name="le_denom" @if(!is_null($cc->le_denom)) value={{ $cc->le_denom }} @else value=0 @endif>
                        </td>
                        <td>
                            <input type="submit" class="btn btn-sm btn-primary btn-block" value="Update">
                        </td>
                    </form>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">
$(document).ready(function() {
    $('#costcenters').DataTable( {
        "pageLength": 20,
        "order": [],
        "columnDefs": [ {
            "targets"  : 'no-sort',
            "orderable": false,
        }]
    } );
} );

</script>


@endsection
