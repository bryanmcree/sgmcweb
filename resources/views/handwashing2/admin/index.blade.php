{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Hand washing Admin') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="card bg-dark">
            <div class="card-header">
                <b>Handwashing - ADMIN</b>
            </div>
            <div class="card-body">
                
                <ul class="nav nav-tabs">
                    <li class="nav-link active"><a data-toggle="tab" href="#home">Dashboard</a></li>
                    <li class="nav-link"><a data-toggle="tab" href="#menu1">Cost Centers</a></li>
                    <li class="nav-link"><a data-toggle="tab" href="#menu3">Completed Surveys</a></li>
                    <li class="nav-link"><a data-toggle="tab" href="#menu5">Questions</a></li>
                    <li class="nav-link"><a data-toggle="tab" href="#menu4">Admin Access</a></li>
                    <li class="nav-link"><a data-toggle="tab" href="#menu2">Reporting</a></li>
                </ul>

                <div class="tab-content">
                    <div id="home" class="tab-pane in active">
                        <br>
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="well">
                                        <p style="font-size: 15px; font-weight: bold; text-align: center;">Hospital Score</p>
                                        <div class="progress" style="height: 20px;">
                                            <div class="progress-bar" role="progressbar" style="width: {{$hospital_score}}%; font-size: 15px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">{{$hospital_score}}%</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="well">
                                        <p style="font-size: 15px; font-weight: bold; text-align: center;">Handwashing Score</p>
                                        <div class="progress" style="height: 20px;">
                                            <div class="progress-bar" role="progressbar" style="width: {{$handwashing_score}}%; font-size: 15px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">{{$handwashing_score}}%</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="well">
                                        <p style="font-size: 15px; font-weight: bold; text-align: center;">OSHA/CDC Score</p>
                                        <div class="progress" style="height: 20px;">
                                            <div class="progress-bar" role="progressbar" style="width: {{$osha_score}}%; font-size: 15px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">{{$osha_score}}%</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="well">
                                <p>Incomplete Surveys</p>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <td><b>Cost Center</b></td>
                                        <td><b>Manager</b></td>
                                        <td align="center"><b># To Do</b></td>
                                        <td align="center"><b># Done</b></td>
                                        <td align="center"><b># Left</b></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($incomplete_surveys as $inc)
                                        <tr class="bg-danger">
                                            <td>{{$inc->cost_center}}</td>
                                            <td>{{$inc->manager}}</td>
                                            <td align="center">{{$inc->to_do}}</td>
                                            <td align="center">{{$inc->complete}}</td>
                                            <td align="center">{{$inc->to_do - $inc->complete}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>

                    <div id="menu1" class="tab-pane fade">
                        <br>
                        <div class="col-lg-12">
                            <table class="table table-condensed table-hover" id="managers" width="99%">
                                <thead>
                                <tr>
                                    <td><b>Cost Center</b></td>
                                    <td><b>Manager</b></td>
                                    <td align="center"><b># To Do</b></td>
                                    <td align="center"><b># Done</b></td>
                                    <td align="center"><b># Left</b></td>
                                    <td align="center"><b># Extra</b></td>
                                    <td class="no-sort"></td>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($hs_cost_centers as $cc)
                                    <?php
                                    //Function on model that calculates completed total
                                    if(is_null($cc->surveyCount)){
                                        $recordCount = 0;
                                    }else{
                                        $recordCount = $cc->surveyCount->completedtotal;
                                    }
                                    ?>
                                    <tr>
                                        <td>{{$cc->costCenters->style1}}</td>
                                        <td>{{$cc->manager->name}}</td>
                                        <td align="center">{{$cc->surveys}}</td>
                                        <td align="center">{{$recordCount}}</td>
                                        <td align="center" @if(($cc->surveys - $recordCount) > 0)style="color:red;"@endif>
                                            @if(($cc->surveys - $recordCount) < 0) 0 @else {{$cc->surveys - $recordCount}} @endif
                                        </td>
                                        <td align="center">
                                            @if($recordCount > $cc->surveys) {{$recordCount - $cc->surveys}} @else 0 @endif
                                        </td>
                                        <td align="right">[Button]</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="menu2" class="tab-pane fade">
                        <h3>Menu 2</h3>
                        <p>Some content in menu 2.</p>
                    </div>
                    <div id="menu3" class="tab-pane fade">
                        <h3>Menu 2</h3>
                        <p>Some content in menu 2.</p>
                    </div>
                    <div id="menu4" class="tab-pane fade">
                        <h3>Menu 2</h3>
                        <p>Some content in menu 2.</p>
                    </div>
                    <div id="menu5" class="tab-pane fade">
                        <h3>Menu 2</h3>
                        <p>Some content in menu 2.</p>
                    </div>
                </div>




            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="application/javascript">
        $(document).ready(function() {
            $('#managers').DataTable( {
                "pageLength": 15,
                "order": [],
                "columnDefs": [ {
                    "targets"  : 'no-sort',
                    "orderable": false,
                }]
            } );
        } );
    </script>

@endsection
@endif