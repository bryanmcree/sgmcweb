{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Parking') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <h4><b>Parking Lot Log</b></h4>
            </div>
            <div class="panel-body">
                <form method="post" action="/parking/add">

                    {{ csrf_field() }}
                    <input type="hidden" value="{{ Auth::user()->employee_number }}" name="created_by">

                    <div class="form-group">
                        <label for="required_id">Ticket Number:</label>
                        <small id="emailHelp" class="form-text text-muted">Number off of paper ticket.</small>
                        <input type="text" id="autocomplete" name="parking_number" class="form-control" placeholder="Ticket #" required>
                    </div>

                    <div class="form-group">
                        <label for="required_id">Vehicle Make:</label>
                        <small id="emailHelp" class="form-text text-muted">Make of the vehicle.</small>
                        <input type="text" id="autocomplete" name="make" class="form-control" placeholder="Make" >
                    </div>

                    <div class="form-group">
                        <label for="required_id">Vehicle Color:</label>
                        <small id="emailHelp" class="form-text text-muted">Color of the vehicle.</small>
                        <input type="text" id="autocomplete" name="color" class="form-control" placeholder="Color" >
                    </div>

                    <div class="form-group">
                        <label for="required_id">License Plate:</label>
                        <small id="emailHelp" class="form-text text-muted">License plate info from vehicle.</small>
                        <input type="text" id="autocomplete" name="license" class="form-control" placeholder="Tag Number" >
                    </div>

                    <div class="form-group">
                        <label for="required_id">Date Parked:</label>
                        <small id="emailHelp" class="form-text text-muted">Date vehicle was parked.</small>
                        <input type="date" id="autocomplete" name="parkdate" class="form-control" placeholder="" >
                    </div>

                    <div class="form-group">
                        <label for="question_notes_id">Comments / Notes:</label>
                        <small id="emailHelp" class="form-text text-muted">Additional information or comments.</small>
                        <textarea class="form-control" name="notes" id="question_notes_id" rows="3"></textarea>
                    </div>

                    <input type="Submit" class="btn btn-sm btn-sgmc" value="Submit">

                </form>

            </div>
        </div>
    </div>

    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <h4><b>Tickets</b></h4>
            </div>
            <div class="panel panel-body">
                @if($tickets->isEmpty())
                <div class="alert alert-info"><b>No tickets entered</b></div>
                @else
                <table class="table table-hover table-condensed">
                    <thead>
                        <tr>
                            <td><b>Ticket #</b></td>
                            <td><b>Make</b></td>
                            <td><b>Color</b></td>
                            <td><b>Plate</b></td>
                            <td><b>Date</b></td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($tickets  as $ticket)
                        <tr>
                            <td>{{$ticket->parking_number}}</td>
                            <td>{{$ticket->make}}</td>
                            <td>{{$ticket->color}}</td>
                            <td>{{$ticket->license}}</td>
                            <td>{{$ticket->parkdate}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @endif
            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
@endif