<!-- Modal -->
<div class="modal fade" id="assignComp" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content text-white bg-dark">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Assign Nursing Competencies</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form method="POST" action="/competencies/assign/store">
                {{ csrf_field() }}

                <div class="modal-body">
                    <div class="form-group">
                        <label for="nurse_id">Choose Nurse to assign Competency</label>
                        <select name="nurse_id" class="form-control" required>
                            <option value="">Choose Nurse</option>
                            @foreach ($nurses as $nurse)
                                <option value="{{ $nurse->id }}">{{ $nurse->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="competencies[]">Choose Competencies to Assign to Nurse</label>
                        <select name="competencies[]" class="form-control basic" style="width:100%" multiple="multiple" required>
                            @foreach ($comps as $comp)
                                <option value="{{ $comp->id }}">{{ $comp->title }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Submit">
                </div>

            </form>

        </div>
    </div>
</div>