<!-- Modal -->
<div class="modal fade" id="newCategory" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content text-white bg-dark">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Create New Nursing Competency Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
    
                <form method="POST" action="/competencies/category/store">
                    {{ csrf_field() }}
                    <input type="hidden" name="created_by" value="{{ Auth::user()->employee_number }}">
                    <input type="hidden" name="comp_id" value="{{ $competency->id }}">
    
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="category">Title of Category</label>
                            <input type="text" name="category" class="form-control" required>
                        </div>
                    </div>
    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" value="Submit">
                    </div>
    
                </form>
    
            </div>
        </div>
    </div>