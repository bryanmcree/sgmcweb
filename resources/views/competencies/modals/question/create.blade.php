<!-- Modal -->
<div class="modal fade" id="newQuestion" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content text-white bg-dark">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Create New Nursing Competency Question</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
    
                <form method="POST" action="/competencies/question/store">
                    {{ csrf_field() }}
                    <input type="hidden" name="created_by" value="{{ Auth::user()->employee_number }}">
                    <input type="hidden" name="comp_id" value="{{ $competency->id }}">
    
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="category_id">Choose Which Category this Question belongs to</label>
                            <select name="category_id" class="form-control" required>
                                <option value="">Choose Category</option>
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->category }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="question">Question</label>
                            <input type="text" name="question" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="is_checkoff">Is this a multi-checkoff task? (Addendum)</label>
                            <select name="is_checkoff" class="form-control" required>
                                <option value="0">No</option>
                                <option value="1">Yes</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="is_checkoff">If yes, how many check-offs are required</label>
                            <input type="number" name="checks_required" class="form-control">
                        </div>
                    </div>
    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" value="Submit">
                    </div>
    
                </form>
    
            </div>
        </div>
    </div>