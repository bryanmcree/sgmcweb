<!-- Modal -->
<div class="modal fade" id="nurseNotes" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document" style="min-width: 50%">
        <div class="modal-content text-white bg-dark">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Nurse Notes</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="/competencies/notes/{{ $pivot->id }}">
                {{ csrf_field() }}
                <div class="modal-body">

                    <div class="form-group">
                        <textarea name="notes" rows="20" class="form-control" placeholder="Notes regarding Nurse's performance on this Competency">{{ $pivot->notes }}</textarea>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Update Notes">
                </div>

            </form>
        </div>
    </div>
</div>