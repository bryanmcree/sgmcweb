{{--New file Template--}}

@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}


@section('content')


<div class="card mb-4 text-white bg-secondary" style="border-radius:0px;">
    <div class="card-header">
        <div class="row">
            <div class="col-lg-6">
                <h5>Dashboard for: {{ $comp->title }}</h5>
            </div>
            <div class="col-lg-6 text-right">
                <span><a href="/competencies" class="btn btn-sm btn-primary">Return Home</a></span>
            </div>
        </div>
    </div>
    <div class="card-body">

        <div class="row">

            <div class="col-lg-6">

                <h4>Completed ({{ count($completed) }})</h4>
                <hr>

                <table class="table table-dark table-striped table-hover table-bordered">
                    <thead>
                        <th class="text-center" style="width:3%">Detail</th>
                        <th>Nurse</th>
                        <th style="width:5%">Status</th>
                    </thead>
                    <tbody>
                        @forelse($completed as $complete)
                            <tr>
                                <td class="text-center" style="width:3%"><a href="/competencies/detail/{{ $complete->id }}"><i class="fas fa-info-circle"></i></a></td>
                                <td>{{ $complete->nurseName->name }}</td>
                                <td class="text-center"><i class="fa fa-check text-success" aria-hidden="true"></i></td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3" class="text-center">No Competencies Awaiting</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>

            </div>

            <div class="col-lg-6">

                <h4>Incomplete / Awaiting Approval ({{ count($incomplete) }})</h4>
                <hr>

                <table class="table table-dark table-striped table-hover table-bordered">
                    <thead>
                        <th>Nurse</th>
                        <th style="width:5%"></th>
                    </thead>
                    <tbody>
                        @forelse($incomplete as $incomp)
                            <tr>
                                <td>{{ $incomp->nurseName->name }}</td>
                                <td class="text-center"><a href="/competencies/approve/view/{{ $incomp->nurse_id }}/{{ $incomp->comp_id }}/{{ $incomp->id }}" class="btn btn-sm btn-info">Approve</a></td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3" class="text-center">No Competencies Awaiting</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>

            </div>

        </div>

    </div>
</div>







{{-- <table class="table table-dark table-hover table-striped table-bordered" id="approvalT">
        <thead>
            <th>Question</th>
            <th nowrap>Nurse</th>
            <th nowrap>Method</th>
            <th nowrap>Age Group</th>
        </thead>
        <tbody>
            @foreach ($awaitingApproval as $awaiting)
                <tr>
                    <td>{{ $awaiting->questionTitle->question }}</td>
                    <td nowrap>{{ $awaiting->nurseName->name }}</td>
                    <td nowrap>test 2</td>
                </tr>
            @endforeach
        </tbody>
    </table> --}}


@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">
    $(document).ready(function() {
        $('#approvalT').DataTable( {
            "pageLength": 25,
            "ordering": false,
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>

@endsection
