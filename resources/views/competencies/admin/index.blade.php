{{--New file Template--}}

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

    {{-- 2 --}}
 <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" /> 

@section('content')


    <div class="card mb-4 text-white bg-secondary" style="border-radius:0px;">
        <div class="card-header">
            <div class="row">
                <div class="col-lg-6">
                    <h5>Nursing Competencies Dashboard</h5>
                </div>
                <div class="col-lg-6 text-right">
                    <a href="#newComp" class="btn btn-success btn-sm" data-toggle="modal">New Competency</a>
                    <a href="#assignComp" class="btn btn-primary btn-sm" data-toggle="modal">Assign Competency</a>
                </div>
            </div>
        </div>
        <div class="card-body">

            <ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link text-dark active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Dashboard</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-dark" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Competencies</a>
                </li>
            </ul>

            <div class="tab-content" id="myTabContent">
                
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

                    <div class="card-deck text-white mb-4">

                        <div class="card bg-dark">
                            <div class="card-header">
                                Competencies Assigned in Last 90 days
                            </div>
                            <div class="card-body">
                                <h4 class="text-center">{{ $assigned90 }}</h4>
                            </div>
                        </div>
                        <div class="card bg-dark">
                            <div class="card-header">
                                Competencies Completed in Last 90 days
                            </div>
                            <div class="card-body">
                                <h4 class="text-center">{{ $completed90 }}</h4>
                            </div>
                        </div>

                    </div>

                    <div class="card-deck text-white mb-4">

                        <div class="card bg-dark">
                            <div class="card-header">
                                Total Competencies Completed All Time
                            </div>
                            <div class="card-body">
                                <h4 class="text-center">{{ $completedAll }}</h4>
                            </div>
                        </div>
                        <div class="card bg-dark">
                            <div class="card-header">
                                # of Active Competencies
                            </div>
                            <div class="card-body">
                                <h4 class="text-center">{{ $activeComps }}</h4>
                            </div>
                        </div>
                        <div class="card bg-dark">
                            <div class="card-header">
                                Competencies Not Completed on Time
                                <span class="float-right"><a href="/competencies/not-complete" class="btn btn-sm btn-info">Show</a></span>
                            </div>
                            <div class="card-body">
                                <h4 class="text-center">{{ $expired }}</h4>
                            </div>
                        </div>

                    </div>
                    
                    <hr>

                    <div class="row">

                        <div class="col-lg-6">
                            <table class="table table-dark table-hover table-bordered" id="approvalT">
                                <thead>
                                    <th>Name</th>
                                    <th class="text-center">View Competencies</th>
                                </thead>
                                <tbody>
                                    @foreach ($nurse_detail as $detail)
                                        <tr>
                                            <td style="width:80%">{{ $detail->nurseName->name }}</td>
                                            <td> <a href="/competencies/dashboard/nurse-view/{{ $detail->nurse_id }}" class="btn btn-info btn-sm btn-block">Nurse Dashboard</a> </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="col-lg-6">
                            <table class="table table-dark table-hover table-bordered" id="compApproval">
                                <thead>
                                    <th>Name</th>
                                    <th class="text-center">View Competencies</th>
                                </thead>
                                <tbody>
                                    @foreach ($comp_detail as $cDetail)
                                        <tr>
                                            <td style="width:80%">{{ $cDetail->compTitle->title }}</td>
                                            <td> <a href="/competencies/dashboard/comp-view/{{ $cDetail->comp_id }}" class="btn btn-info btn-sm btn-block">Competency Dashboard</a> </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        
                    </div>

                </div>

                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">

                    <table class="table table-dark table-hover table-bordered" id="competenciesTable">
                        <thead>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Days to Complete</th>
                            <th>Details</th>
                            <th> </th>
                        </thead>
                        <tbody>
                            @foreach ($comps as $comp)
                                <tr>
                                    <td>{{ $comp->title }}</td>
                                    <td>{{ $comp->description }}</td>
                                    <td>{{ $comp->days_to_complete }}</td>
                                    <td> <a href="/competencies/show/{{ $comp->id }}" class="btn btn-info btn-sm btn-block">Details</a> </td>
                                    <td style="width:3%;" class="text-center"> <a href="#" class="btn btn-danger deleteComp" comp_id="{{ $comp->id }}" title="Deactivate Competency"><i class="fas fa-trash-alt"></i></a> </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                
                </div>

            </div>

        </div>
    </div>


@include('competencies.modals.competency.create')
@include('competencies.modals.assign')

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

{{-- 2 --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script type="text/javascript">

    $(document).ready(function() {
        $('#competenciesTable').DataTable( {
            "pageLength": 25,
            "ordering": false,
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );

    $(document).ready(function() {
        $('#approvalT').DataTable( {
            "pageLength": 50,
            "ordering": false,
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );

    $(document).ready(function() {
        $('#compApproval').DataTable( {
            "pageLength": 50,
            "ordering": false,
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );

    $(document).ready(function() {
        $('.basic').select2();
    });

    $(document).on("click", ".deleteComp", function () {
        //alert($(this).attr("data-cost_center"));
        var comp_id = $(this).attr("comp_id");
        DeleteComp(comp_id);
    });

    function DeleteComp(comp_id) {
        swal({
            title: "Deactivate Competency?",
            text: "This can be undone.",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            confirmButtonText: "Yes, deactivate it!",
            confirmButtonColor: "#ec6c62"
        }, function() {
            $.ajax({
                url: "/competencies/destroy/" + comp_id,
                type: "GET"
            })
                .done(function(data) {
                    swal({
                            title: "Deleted",
                            text: "Competency Deactivated",
                            type: "success",
                            timer: 1800,
                            showConfirmButton: false

                        },function() {
                            location.reload();
                        });
                })
                .error(function(data) {
                    swal("Oops", "We couldn't connect to the server!", "error");
                });
        });
    }

</script>



@endsection
