{{--New file Template--}}

@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')


<div class="card mb-4 text-white bg-secondary" style="border-radius:0px;">
    <div class="card-header">
        <div class="row">
            <div class="col-lg-9">
                Competencies Not Completed on Time
            </div>
            <div class="col-lg-3 text-right">
                <a href="/competencies" class="btn btn-primary btn-sm">Return to Dashboard</a>
            </div>
        </div>
    </div>
    <div class="card-body">
    
        <table class="table table-dark table-bordered table-hover" id="qTable">
            <thead>
                <th style="width:3%" class="text-center">Email</th>
                <th>Competency</th>
                <th>Nurse</th>
                <th>Date Completed</th>
                <th>Date Due</th>
                <th>Days Past Due</th>
            </thead>
            <tbody>
                @foreach($expired as $exp)
                    <tr>
                        <td class="text-center"><a href="mailto:{{ $exp->nurseName->mail }}" class="btn btn-sm btn-primary"><i class="fas fa-envelope"></i></a></td>
                        <td>{{ $exp->compTitle->title }}</td>
                        <td>{{ $exp->nurseName->name }}</td>
                        <td>@if(empty($exp->date_completed)) <span class="text-danger">Not Complete</span> @else {{ Carbon::parse($exp->date_completed)->format('m-d-Y') }} @endif</td>
                        <td>{{ Carbon::parse($exp->created_at)->addDays($exp->days_to_complete)->format('m-d-Y') }}</td>
                        <td>{{ Carbon::parse($exp->date_completed)->diffInDays(Carbon::parse($exp->created_at)->addDays($exp->days_to_complete)) }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    </div>
</div>


@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">
    $(document).ready(function() {
        $('#qTable').DataTable( {
            "pageLength": 35,
            "ordering": false,
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>



@endsection
