{{--New file Template--}}

@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')


<div class="card mb-4 text-white bg-secondary" style="border-radius:0px;">
    <div class="card-header">
        <div class="row">
            <div class="col-lg-9">
                <h5>{{ $competency->title }}</h5>
            </div>
            <div class="col-lg-3 text-right">
                <a href="/competencies" class="btn btn-primary btn-sm">Return to Dashboard</a>
            </div>
        </div>
    </div>
    <div class="card-body">
        
        <div class="row">

            <div class="col-lg-6">
                <a href="#newCategory" data-toggle="modal" class="btn btn-dark btn-block">Create New Category</a>
            </div>

            <div class="col-lg-6">
                <a href="#newQuestion" data-toggle="modal" class="btn btn-dark btn-block">Create New Question</a>
            </div>

        </div>

        <hr>

        <table class="table table-dark table-bordered table-hover" id="qTable">
            <thead style="display:none;">
                <th>test</th>
                <th>test2</th>
                <th>test3</th>
                <th>test4</th>
            </thead>
            <tbody>
                @foreach($categories as $cat)
                    <?php $counter = 1; ?>
                    <tr>
                        <td colspan="4" class="bg-primary"><b> {{ $cat->category }} </b></td>
                        <td style="display:none;"></td>
                        <td style="display:none;"></td>
                        <td style="display:none;"></td>
                    </tr>
                    @foreach($questions->where('category_id', $cat->id) as $q)
                        <tr>
                            <td style="width:3%" class="text-center">{{ $counter }}.</td>
                            <td style="width:93%">{{ $q->question }}</td>
                            <td> <a href="#" q_id="{{ $q->id }}" class="btn btn-sm btn-danger btn-block deleteQ" title="Delete Question"><i class="fas fa-trash-alt"></i></a> </td>
                        </tr>
                        <?php $counter++; ?>
                    @endforeach
                @endforeach
            </tbody>
        </table>

    </div>
</div>


@include('competencies.modals.category.create')
@include('competencies.modals.question.create')

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">
    $(document).ready(function() {
        $('#qTable').DataTable( {
            "pageLength": 35,
            "ordering": false,
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>


<script type="application/javascript">

    $(document).on("click", ".deleteQ", function () {
            var q_id = $(this).attr("q_id");
            DeleteQuestion(q_id);
        });

    function DeleteQuestion(q_id) {
        swal({
            title: "Delete Question?",
            text: "Deleting this Question cannot be undone.",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            confirmButtonText: "Yes, delete it!",
            confirmButtonColor: "#ec6c62"
        }, function() {
            $.ajax({
                url: "/competencies/question/destroy/" + q_id,
                type: "GET"
            })
                .done(function(data) {
                    swal({
                            title: "Deleted",
                            text: "Question Deleted",
                            type: "success",
                            timer: 1800,
                            showConfirmButton: false

                        },function() {
                            location.reload();
                        });
                })
                .error(function(data) {
                    swal("Oops", "We couldn't connect to the server!", "error");
                });
        });
    }

</script>

@endsection
