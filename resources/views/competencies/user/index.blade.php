{{--New file Template--}}

@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}


@section('content')


<div class="card mb-4 text-white bg-secondary" style="border-radius:0px;">
    <div class="card-header">
        <h5>Nursing Competencies</h5>
    </div>
    <div class="card-body">

        <ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
            @if(Auth::user()->hasRole('Nursing Competency Manager') == TRUE)
                <li class="nav-item">
                    <a class="nav-link text-dark active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Awaiting Approval</a>
                </li>
            @endif
            <li class="nav-item">
                <a class="nav-link text-dark @if(Auth::user()->hasRole('Nursing Competency Manager') == FALSE) active @endif" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">My Competencies</a>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            @if(Auth::user()->hasRole('Nursing Competency Manager') == TRUE)
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    
                    <table class="table table-dark table-hover table-bordered" id="approvalT">
                        <thead>
                            <th>Name</th>
                            <th class="text-center">View Competencies</th>
                        </thead>
                        <tbody>
                            @foreach ($nurses as $nurse)
                                <tr>
                                    <td style="width:80%">{{ $nurse->nurseName->name }}</td>
                                    <td> <a href="/competencies/dashboard/{{ $nurse->nurse_id }}" class="btn btn-info btn-sm btn-block">Nurse Dashboard</a> </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            @endif
            <div class="tab-pane fade @if(Auth::user()->hasRole('Nursing Competency Manager') == FALSE) show active @endif" id="profile" role="tabpanel" aria-labelledby="profile-tab">
            
                <table class="table table-dark table-hover table-bordered">
                    <thead>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Details</th>
                    </thead>
                    <tbody>
                        @foreach ($user->competencies as $comps)
                            <tr>
                                <td>{{ $comps->title }}</td>
                                <td>{{ $comps->description }}</td>
                                <td> <a href="/competencies/complete/{{ $comps->id }}" class="btn btn-primary btn-sm btn-block">Complete</a> </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>

    </div>
</div>







{{-- <table class="table table-dark table-hover table-striped table-bordered" id="approvalT">
        <thead>
            <th>Question</th>
            <th nowrap>Nurse</th>
            <th nowrap>Method</th>
            <th nowrap>Age Group</th>
        </thead>
        <tbody>
            @foreach ($awaitingApproval as $awaiting)
                <tr>
                    <td>{{ $awaiting->questionTitle->question }}</td>
                    <td nowrap>{{ $awaiting->nurseName->name }}</td>
                    <td nowrap>test 2</td>
                </tr>
            @endforeach
        </tbody>
    </table> --}}


@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">
    $(document).ready(function() {
        $('#approvalT').DataTable( {
            "pageLength": 25,
            "ordering": false,
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>

@endsection
