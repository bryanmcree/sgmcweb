{{--New file Template--}}

@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')


<div class="card mb-4 text-white bg-secondary" style="border-radius:0px;">
    <div class="card-header">
        <div class="row">
            <div class="col-lg-9">
                <h5>{{ $comp->title }}</h5>
            </div>
            <div class="col-lg-3 text-right">
                <a href="/competencies" class="btn btn-primary btn-sm">Return to Dashboard</a>
            </div>
        </div>
    </div>
    <div class="card-body">

        <div class="row">
            <div class="col-lg-6">
                <div class="progress">
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: @if($answersComplete == 0) 0% @else {{ ($answersComplete / $answersPossible) * 100 }}% @endif"> Completion: @if($answersComplete == 0) 0% @else {{ number_format(($answersComplete / $answersPossible) * 100, 2) }}% @endif</div>
                </div>
            </div>
            <div class="col-lg-6 text-right my-auto">
                <span class="text-warning"><i>Check the Box for each Competency you Feel Comfortable Performing.</i></span>
            </div>
        </div>

        <hr>

        <form action="/competencies/answers/update" method="POST">
            <input type="hidden" name="comp_id" value="{{ $comp->id }}">
            <input type="hidden" name="nurse_id" value="{{ Auth::user()->id }}">
            <input type="hidden" name="unit" value="{{ Auth::user()->unit_code_description }}">
            {{ csrf_field() }}

            <table class="table table-dark table-bordered table-hover">
                <tbody>
                    @foreach($categories as $cat)
                        <?php $counter = 1; ?>
                        <tr>
                            <td colspan="3" class="bg-primary"><b> {{ $cat->category }} </b></td>
                            <td style="display:none;"></td>
                            <td style="display:none;"></td>
                        </tr>
                        @foreach($questions->where('category_id', $cat->id) as $q)
                            <tr>
                                <td style="width:3%" class="text-center">{{ $counter }}.</td>
                                <td>
                                    {{ $q->question }}
                                    @if($q->is_checkoff == 1)
                                        @foreach($answersCheckoff->where('question_id', $q->id) as $ans)
                                            @if($ans->total == $q->checks_required)
                                                <span class="float-right text-success">{{ $ans->total }} of {{ $q->checks_required }} instances approved by nursing manager</span>
                                            @else
                                                <span class="float-right text-warning">{{ $ans->total }} of {{ $q->checks_required }} instances approved by nursing manager</span>
                                            @endif
                                        @endforeach
                                    @else
                                        @foreach($answers as $ansBack)
                                            @if($ansBack->question_id == $q->id && $ansBack->is_complete == 1)
                                                <span class="float-right text-success">Approved by nursing manager!</span>  
                                            @elseif($ansBack->question_id == $q->id)
                                                <span class="float-right text-warning">awaiting approval by nursing manager!</span>  
                                            @endif 
                                        @endforeach
                                    @endif
                                </td>
                                <td style="width:5%" class="text-center">
                                    <input type="hidden" name="all_questions[]" value="{{ $q->id }}">
                                    <input type="checkbox" style="width:30px; height:30px;" name="answers[]" value="{{ $q->id }}" @foreach($answers as $answer) @if($answer->question_id == $q->id) @if($answer->is_comfortable == 1) checked @endif @endif @endforeach>
                                </td>
                            </tr>
                            <?php $counter++; ?>
                        @endforeach
                    @endforeach
                </tbody>
            </table>

            <input type="submit" class="btn btn-primary btn-block">

        </form>

    </div>
</div>


@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
