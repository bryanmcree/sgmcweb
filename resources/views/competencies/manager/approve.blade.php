{{--New file Template--}}

@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}


@section('content')


<div class="card mb-4 text-white bg-secondary" style="border-radius:0px;">
    <div class="card-header">
        <div class="row">
            <div class="col-lg-6">
                <h5>Approve Competencies</h5>
            </div>
            <div class="col-lg-6 text-right">
                <span><a href="/competencies/dashboard/nurse-view/{{ $nurse }}" class="btn btn-sm btn-primary">Return to Dashboard</a></span>
            </div>
        </div>
    </div>
    <div class="card-body">

            <div class="card-deck mb-4">

                <div class="card bg-dark col-lg-3">
                    <div class="card-header">
                        Date of Assignment
                    </div>
                    <div class="card-body">
                        <h1 class="text-center pt-4">{{ Carbon::parse($pivot->created_at)->format('m-d-Y') }}</h1>
                    </div>
                </div>
    
                <div class="card bg-dark col-lg-3">
                    <div class="card-header">
                        Days remaining to complete
                    </div>
                    <div class="card-body">
                        <h1 class="text-center pt-4">{{ $remainingDays }}</h1>
                    </div>
                </div>
    
                <div class="card bg-dark col-lg-6">
                    <div class="card-header">
                        Notes
                        <span class="float-right">
                            <a href="#nurseNotes" class="btn btn-sm btn-warning" data-toggle="modal" title="Add/Edit Notes"><i class="fas fa-edit"></i></a>
                        </span>
                    </div>
                    <div class="card-body">
                        <textarea rows="5" class="bg-secondary form-control text-white" placeholder="No Notes" readonly>{{ $pivot->notes }}</textarea>
                    </div>
                </div>
    
            </div>

        <div class="table-responsive">
            
            <table class="table table-dark table-hover table-striped table-bordered" id="approvalT">
                <thead>
                    <th>Question</th>
                    <th nowrap>Nurse</th>
                    <th nowrap>Method</th>
                    <th nowrap>Age Group</th>
                    <th nowrap>Date Completed</th>
                    <th nowrap>Approve</th>
                </thead>
                <tbody>
                    @forelse($awaitingApproval as $awaiting)
                        <tr>
                            <form method="POST" action="/competencies/approve/{{ $awaiting->id }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="pivot_id" value="{{ $pivot->id }}">
                                <td style="width:50%">{{ $awaiting->questionTitle->question }}</td>
                                <td nowrap>{{ $awaiting->nurseName->name }}</td>
                                <td nowrap> 
                                    <select name="validation_method" id="" class="form-control" required>
                                        <option value="">Choose Method</option>
                                        <option value="Written Test">Written Test</option>
                                        <option value="Verbalization of Process">Verbalization of Process</option>
                                        <option value="Return Demonstration">Return Demonstration</option>
                                        <option value="Observation">Observation</option>
                                    </select> 
                                </td>
                                <td nowrap>
                                    <select name="age_group" id="" class="form-control" required>
                                        <option value="">Choose Age Group</option>
                                        <option value="Adolescent 13-17 yrs">Adolescent 13-17 yrs</option>
                                        <option value="Adult 18-65 yrs">Adult 18-65 yrs</option>
                                        <option value="All age groups">All age groups</option>
                                        <option value="All age groups except Neonate">All age groups except Neonate</option>
                                        <option value="Geriatric > 65 yrs">Geriatric > 65 yrs</option>
                                        <option value="Neonate (Birth - Discharge)">Neonate (Birth - Discharge)</option>
                                        <option value="Pediatric">Pediatric</option>
                                    </select> 
                                </td>
                                <td nowrap>
                                    <input type="date" class="form-control" name="date_completed">
                                </td>
                                <td class="text-center">
                                    <input type="submit" class="btn btn-primary" value="Approve">
                                </td>
                            </form>
                        </tr>
                    @empty
                        <td colspan="6" class="text-center text-success">There is no Criteria Currently Awaiting Approval</td>
                    @endforelse
                </tbody>
            </table>
        
        </div>

    </div>
</div>

@include('competencies.modals.notes')

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')


@endsection
