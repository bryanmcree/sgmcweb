{{--New file Template--}}

@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')


<div class="card mb-4 text-white bg-secondary" style="border-radius:0px;">
    <div class="card-header">
        <div class="row">
            <div class="col-lg-8">
                <h5>{{ $pivot->compTitle->title }}</h5>
            </div>
            <div class="col-lg-4 text-right">
                <a href="/competencies/dashboard/nurse-view/{{ $pivot->nurse_id }}" class="btn btn-primary btn-sm">Return to Nurse Dashboard</a>
                <a href="/competencies/dashboard/comp-view/{{ $pivot->comp_id }}" class="btn btn-success btn-sm">Return to Competency Dashboard</a>
            </div>
        </div>
    </div>
    <div class="card-body">

        <div class="card-deck mb-4">

            <div class="card bg-dark col-lg-3">
                <div class="card-header">
                    Date of Assignment
                </div>
                <div class="card-body">
                    <h1 class="text-center pt-4">{{ Carbon::parse($pivot->created_at)->format('m-d-Y') }}</h1>
                </div>
            </div>

            <div class="card bg-dark col-lg-3">
                <div class="card-header">
                    Date of Completion
                </div>
                <div class="card-body">
                    <h1 class="text-center pt-4">{{ Carbon::parse($pivot->date_completed)->format('m-d-Y') }}</h1>
                </div>
            </div>

            <div class="card bg-dark col-lg-6">
                <div class="card-header">
                    Notes
                    <span class="float-right">
                        <a href="#nurseNotes" class="btn btn-sm btn-warning" data-toggle="modal" title="Add/Edit Notes"><i class="fas fa-edit"></i></a>
                    </span>
                </div>
                <div class="card-body">
                    <textarea rows="5" class="bg-secondary form-control text-white" placeholder="No Notes" readonly>{{ $pivot->notes }}</textarea>
                </div>
            </div>

        </div>

        <div class="table-responsive">
            <table class="table table-dark table-bordered table-hover">
                <thead class="bg-info">
                    <th>Question</th>
                    <th nowrap class="text-center">Completed</th>
                    <th nowrap class="text-center">Method</th>
                    <th nowrap class="text-center">Age Group</th>
                    <th nowrap class="text-center">Unit</th>
                </thead>
                <tbody>
                    @foreach($categories as $cat)
                        <tr>
                            <td colspan="5" class="bg-primary"><b> {{ $cat->category }} </b></td>
                        </tr>
                        @foreach($questions->where('category_id', $cat->id) as $q)
                            @if($q->is_checkoff == 0)
                                <tr>
                                    <td>{{ $q->question }}</td>
                                    @foreach($answers->where('question_id', $q->id) as $data)
                                        <td nowrap class="text-center">{{ Carbon::parse($data->date_completed)->format('m-d-Y') }}</td>
                                        <td nowrap class="text-center">{{ $data->validation_method }}</td>
                                        <td nowrap class="text-center">{{ $data->age_group }}</td>
                                        <td nowrap class="text-center">{{ $data->unit }}</td>
                                    @endforeach
                                </tr>
                            @else
                                <?php $counter = 1; ?>
                                @foreach($answers->where('question_id', $q->id) as $data)
                                    <tr>
                                        <td>
                                            {{ $q->question }} 
                                            <br><small class="text-warning"><i>checkoff instance #{{ $counter }}</i></small>
                                        </td>
                                        <td nowrap class="text-center">{{ Carbon::parse($data->date_completed)->format('m-d-Y') }}</td>
                                        <td nowrap class="text-center">{{ $data->validation_method }}</td>
                                        <td nowrap class="text-center">{{ $data->age_group }}</td>
                                        <td nowrap class="text-center">{{ $data->unit }}</td>
                                    </tr>
                                    <?php $counter++; ?>
                                @endforeach
                            @endif
                        @endforeach
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
</div>

@include('competencies.modals.notes')

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
