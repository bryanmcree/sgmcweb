{{--New file Template--}}

{{--Add Security for this page below--}}
@if( Auth::user()->hasRole('dialysis') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')


    <div class="card mb-4 text-white bg-dark">
        <div class="card-header">
            Dialysis surveillance Detailed Page
            <span class="float-right"> <a href="/dialysis" class="btn btn-sm btn-secondary">Return to Dashboard</a> </span>
        </div>
        <div class="card-body">

            <div class="table-responsive">
                <table class="table table-dark table-hover table-striped table-bordered">
                    <thead>
                        <th nowrap class="text-primary">Actions</th>
                        <th nowrap>Date</th>
                        <th nowrap>Patient</th>
                        <th nowrap>Nurse</th>
                        <th nowrap>Hospital MR #</th>
                        <th nowrap>Machine #</th>
                        <th nowrap>Treatmeant Location</th>
                        <th nowrap>Initial Date of Hepatitis Profile</th>
                        <th nowrap>HBsAG Positive/Negative and Date Lab was Drawn</th>
                        <th nowrap>HBsAG >10 Immune < 10 Susceptible and Date Lab was Drawn</th>
                        <th nowrap>Date of Next set of 'Screening' Labs to be Drawn</th>
                        <th nowrap>Patient Identified as 'Susceptible'</th>
                        <th nowrap>VRE</th>
                        <th nowrap>MRSA</th>
                        <th nowrap>C-diff</th>
                        <th nowrap>Other</th>
                        <th nowrap>Consent Verified</th>
                        <th nowrap>Result Verified</th>
                        <th nowrap>Entered By</th>
                    </thead>
                    <tbody>
                        @foreach($surveillances as $surveillance)
                            <tr class="text-center">
                                <td nowrap> 
                                    <a href="/dialysis/surveillance/edit/{{ $surveillance->id }}" class="btn btn-sm btn-warning" title="Edit"><i class="fas fa-edit"></i></a> 
                                    <a href="#" class="btn btn-sm btn-danger deleteSurv" surv_id="{{ $surveillance->id }}" title="Delete"><i class="fas fa-trash-alt"></i></a> 
                                </td>
                                <td nowrap>{{ Carbon::parse($surveillance->date)->format('m-d-Y') }}</td>
                                <td nowrap>{{ $surveillance->patient }}</td>
                                <td nowrap>{{ $surveillance->nurse }}</td>
                                <td nowrap>{{ $surveillance->hospital_MR }}</td>
                                <td nowrap>{{ $surveillance->machine_num }}</td>
                                <td nowrap>{{ $surveillance->treatment_location }}</td>
                                <td nowrap>{{ Carbon::parse($surveillance->initial_hep)->format('m-d-Y') }}</td>
                                <td nowrap>{{ $surveillance->HB_pos_neg }} &nbsp; | &nbsp; {{ Carbon::parse($surveillance->pos_neg_date)->format('m-d-Y') }}</td>
                                <td nowrap>@if(is_null($surveillance->immune_susceptible_string)) {{ $surveillance->immune_susceptable }} @else {{ $surveillance->immune_susceptible_string }} @endif &nbsp; | &nbsp; {{ $surveillance->imm_sus_date }}</td>
                                <td nowrap>@if(Carbon::parse($surveillance->next_date)->format('m-d-Y') == '01-01-1900') TBD @else {{ Carbon::parse($surveillance->next_date)->format('m-d-Y') }} @endif </td>
                                <td nowrap>@if($surveillance->is_susceptible == '1') <i class="fas fa-check text-success"></i> @elseif($surveillance->is_susceptible == '0') <i class="fas fa-ban text-danger"></i> @else TBD @endif </td>
                                <td nowrap>@if($surveillance->VRE == '1') <i class="fas fa-check text-success"></i> @else <i class="fas fa-ban text-danger"></i> @endif </td>
                                <td nowrap>@if($surveillance->MRSA == '1') <i class="fas fa-check text-success"></i> @else <i class="fas fa-ban text-danger"></i> @endif </td>
                                <td nowrap>@if($surveillance->Cdiff == '1') <i class="fas fa-check text-success"></i> @else <i class="fas fa-ban text-danger"></i> @endif </td>
                                <td nowrap>{{ $surveillance->other }} </td>
                                <td nowrap>@if($surveillance->consent == '1') <i class="fas fa-check text-success"></i> | {{ Carbon::parse($surveillance->consent_date)->format('m-d-Y') }}  @else <i class="fas fa-ban text-danger"></i> @endif</td>
                                <td nowrap>@if($surveillance->verified == '1') <i class="fas fa-check text-success"></i> | {{ Carbon::parse($surveillance->verified_date)->format('m-d-Y') }} @else <i class="fas fa-ban text-danger"></i> @endif </td>
                                <td nowrap>@if(is_null($surveillance->createdBy)) ? @else {{ $surveillance->createdBy->name }} @endif </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <span class="float-right"> @include('pagination', ['paginator' => $surveillances]) </span>
            
        </div>
    </div>


@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">

$(document).on("click", ".deleteSurv", function () {
        var surv_id = $(this).attr("surv_id");
        DeleteSurveillance(surv_id);
    });

function DeleteSurveillance(surv_id) {
    swal({
        title: "Delete surveillance entry?",
        text: "Deleting this surveillance entry cannot be undone.",
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "Yes, delete it!",
        confirmButtonColor: "#ec6c62"
    }, function() {
        $.ajax({
            url: "/dialysis/surveillance/destroy/" + surv_id,
            type: "GET"
        })
            .done(function(data) {
                swal({
                        title: "Deleted",
                        text: "Surveillance Entry Deleted",
                        type: "success",
                        timer: 1800,
                        showConfirmButton: false

                    },function() {
                        location.reload();
                    });
            })
            .error(function(data) {
                swal("Oops", "We couldn't connect to the server!", "error");
            });
    });
}

</script>

@endsection

@endif