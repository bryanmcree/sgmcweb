{{--New file Template--}}

{{--Add Security for this page below--}}
@if( Auth::user()->hasRole('dialysis') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    <div class="card mb-4 text-white bg-dark mx-auto" style="width:60%;">
        <div class="card-header">
            Dialysis Surveillance
            <span class="float-right"><a href="/dialysis" class="btn btn-sm btn-secondary">Return to Dashboard</a></span>
        </div>
        <div class="card-body">
            
            <form action="/dialysis/surveillance/store" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="created_by" value="{{ Auth::user()->employee_number }}">

                <div class="form-group">
                    <label for="date">Date</label>
                    <input type="date" name="date" class="form-control">
                </div>
                
                <div class="form-row">

                    <div class="form-group col-6">
                        <label for="patient">Patient</label>
                        <input type="text" name="patient" class="form-control">
                    </div>
                    <div class="form-group col-6">
                        <label for="nurse">Nurse Performing Treatment</label>
                        <select name="nurse" class="form-control">
                            <option value="">[Choose Nurse]</option>
                            @foreach($nurses as $nurse)
                                <option value="{{ $nurse->name }}">{{ $nurse->name }}</option>
                            @endforeach
                        </select>
                    </div>

                </div>

                <div class="form-row">

                    <div class="form-group col-4">
                        <label for="hospital MR">Hospital MR #</label>
                        <input type="text" name="hospital_MR" class="form-control">
                    </div>
                    <div class="form-group col-4">
                        <label for="machine_num">Machine #</label>
                        <input type="text" name="machine_num" class="form-control">
                    </div>
                    <div class="form-group col-4">
                        <label for="treatment_location">Treatment Location</label>
                        <input type="text" name="treatment_location" class="form-control">
                    </div>

                </div>

                <div class="form-row">
                    
                    <div class="form-group col-6">
                        <label for="initial_hep">Initial Date of Hepatitus Profile</label>
                        <input type="date" name="initial_hep" class="form-control">
                    </div>
                    <div class="form-group col-6">
                        <label for="next_date">Date of next set of "screening" labs to be drawn</label>
                        <input type="date" name="next_date" class="form-control">
                        <small><i>Leave Blank if TBD</i></small>
                    </div>

                </div>

                <hr>

                <div class="form-row">

                    <div class="form-group col-6">
                        <label for="HB_pos_neg">HBsAG Positive/Negative & Date Lab was Drawn</label>
                        <select name="HB_pos_neg" class="form-control">
                            <option value="">[Positive/Negative]</option>
                            <option value="Positive">Positive</option>
                            <option value="Negative">Negative</option>
                            <option value="Unknown">Unknown</option>
                        </select>
                        <input type="date" name="pos_neg_date" class="form-control mt-2">
                    </div>
                    <div class="form-group col-6">
                        <label for="immune_susceptable">HBsAG > 10 immune < 10 susceptible and lab date</label>
                        <input type="number" step="0.01" class="form-control" name="immune_susceptable">
                        <select name="immune_susceptible_string" class="form-control mt-2">
                            <option value="">[ > 1000 OR < 5 OR < 10 ]</option>
                            <option value="< 10"> < 10 </option>
                            <option value="> 1000"> > 1000 </option>
                            <option value="< 5"> < 5 </option>
                        </select>
                        <input type="date" name="imm_sus_date" class="form-control mt-2">
                    </div>

                </div>

                <div class="form-row">

                    <div class="form-group col-4">
                        <label for="is_susceptible">Patient identified as susceptible</label>
                        <select name="is_susceptible" class="form-control">
                            <option value="">[Yes/No]</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                            <option value="2">TBD</option>
                        </select>
                    </div>
                    <div class="form-group col-4">
                        <label for="VRE">VRE</label>
                        <select name="VRE" class="form-control">
                            <option value="">[Yes/No]</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </div>
                    <div class="form-group col-4">
                        <label for="MRSA">MRSA</label>
                        <select name="MRSA" class="form-control">
                            <option value="">[Yes/No]</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </div>

                </div>

                <div class="form-row">

                    <div class="form-group col-3">
                        <label for="Cdiff">C-diff</label>
                        <select name="Cdiff" class="form-control">
                            <option value="">[Yes/No]</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </div>
                    <div class="form-group col-3">
                        <label for="other">Other</label>
                        <select name="other" class="form-control">
                            <option value="">[Epic/Media]</option>
                            <option value="Epic">Epic</option>
                            <option value="Media">Media</option>
                            <option value="Epic/Media">Epic/Media</option>
                        </select>
                    </div>
                    <div class="form-group col-3">
                        <label for="consent">Consent Verified</label>
                        <select name="consent" class="form-control">
                            <option value="">[Yes/No]</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </div>
                    <div class="form-group col-3">
                        <label for="verified">Result Verified</label>
                        <select name="verified" class="form-control">
                            <option value="">[Yes/No]</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </div>

                </div>

                <div class="form-row">
                    <div class="col-3"></div>
                    <div class="form-group col-6">
                        <label for="">Consent Verified Date</label>
                        <input type="date" name="consent_date" class="form-control">
                    </div>
                    <div class="col-3"></div>
                    {{-- <div class="form-group col-6">
                        <label for="">Result Verified Date</label>
                        <input type="date" name="verified_date" class="form-control">
                    </div> --}}
                </div>

                <hr>

                <input type="submit" class="btn btn-primary btn-block">

            </form>

        </div>
    </div>


@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection

@endif