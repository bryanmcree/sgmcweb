{{--New file Template--}}

{{--Add Security for this page below--}}
@if( Auth::user()->hasRole('dialysis') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}


@section('content')

<div class="card bg-dark text-white mb-3">
    <div class="card-header">
        Dialysis Report for Dates: <span class="text-primary">{{Carbon::parse($startDate)->format('M jS, Y')}} - {{Carbon::parse($endDate)->format('M jS, Y')}}</span>
        <span class="float-right">
            <a href="/dialysis" class="btn btn-primary btn-sm">Return to Dashboard</a>
        </span>
    </div>
    <div class="card-body">

        <div class="card-deck mb-3">

            <div class="card bg-secondary" style="border-radius:0px;">
                <div class="card-header">
                    Total Treatments
                    <br>
                    <small class="text-warning"><i>{{Carbon::parse($startDate)->format('M jS, Y')}} - {{Carbon::parse($endDate)->format('M jS, Y')}}</i></small>
                </div>
                <div class="card-body text-center">
                    <h2>{{ count($logs) }}</h2>
                </div>
            </div>

            <div class="card bg-secondary" style="border-radius:0px;">
                <div class="card-header">
                    # of First Treatments
                    <br>
                    <small class="text-warning"><i>{{Carbon::parse($startDate)->format('M jS, Y')}} - {{Carbon::parse($endDate)->format('M jS, Y')}}</i></small>
                </div>
                <div class="card-body text-center">
                    <h2>{{ $firstT }}</h2>
                </div>
            </div>

            <div class="card bg-secondary" style="border-radius:0px;">
                <div class="card-header">
                    # of Failed Treatments
                    <br>
                    <small class="text-warning"><i>Last 30 Days</i></small>
                </div>
                <div class="card-body text-center">
                    <h2>@if($failedT == 0) 0 @else {{ $failedT }} @endif</h2>
                </div>
            </div>

            <div class="card bg-secondary" style="border-radius:0px;">
                <div class="card-header">
                    # of Treatments >= 4 hours
                    <br>
                    <small class="text-warning"><i>{{Carbon::parse($startDate)->format('M jS, Y')}} - {{Carbon::parse($endDate)->format('M jS, Y')}}</i></small>
                </div>
                <div class="card-body text-center">
                    <h2>@if($fourHours == 0) 0 @else {{ $fourHours }} @endif</h2>
                </div>
            </div>

            <div class="card bg-secondary" style="border-radius:0px;">
                <div class="card-header">
                    # of TX with Herapin
                    <br>
                    <small class="text-warning"><i>{{Carbon::parse($startDate)->format('M jS, Y')}} - {{Carbon::parse($endDate)->format('M jS, Y')}}</i></small>
                </div>
                <div class="card-body text-center">
                    <h2>{{ $txHerapin }}</h2>
                </div>
            </div>

            <div class="card bg-secondary" style="border-radius:0px;">
                <div class="card-header">
                    # of TX with Naturalyte
                    <br>
                    <small class="text-warning"><i>Last 30 Days</i></small>
                </div>
                <div class="card-body text-center">
                    <h2>{{ $txNaturalyte }}</h2>
                </div>
            </div>

        </div>

        <div class="card-deck mb-3">

            <div class="card bg-secondary" style="border-radius:0px;">
                <div class="card-header">
                    # of AMA
                    <br>
                    <small class="text-warning"><i>Last 30 Days</i></small>
                </div>
                <div class="card-body text-center">
                    <h2>{{ $AMA }}</h2>
                </div>
            </div>

            <div class="card bg-secondary" style="border-radius:0px;">
                <div class="card-header">
                    # of Fistulas
                    <br>
                    <small class="text-warning"><i>{{Carbon::parse($startDate)->format('M jS, Y')}} - {{Carbon::parse($endDate)->format('M jS, Y')}}</i></small>
                </div>
                <div class="card-body text-center">
                    <h2>{{ $fistulas }}</h2>
                </div>
            </div>

            <div class="card bg-secondary" style="border-radius:0px;">
                <div class="card-header">
                    # of Catheters
                    <br>
                    <small class="text-warning"><i>{{Carbon::parse($startDate)->format('M jS, Y')}} - {{Carbon::parse($endDate)->format('M jS, Y')}}</i></small>
                </div>
                <div class="card-body text-center">
                    <h2>{{ $catheters }}</h2>
                </div>
            </div>

            <div class="card bg-secondary" style="border-radius:0px;">
                <div class="card-header">
                    # of Clotted
                    <br>
                    <small class="text-warning"><i>{{Carbon::parse($startDate)->format('M jS, Y')}} - {{Carbon::parse($endDate)->format('M jS, Y')}}</i></small>
                </div>
                <div class="card-body text-center">
                    <h2>{{ $clotted }}</h2>
                </div>
            </div>

        </div>

        <div class="card-deck">

            <div class="card bg-secondary" style="border-radius:0px;">
                <div class="card-header">
                    Immune/Susceptible/Unknown
                    <br>
                    <small class="text-warning"><i>{{Carbon::parse($startDate)->format('M jS, Y')}} - {{Carbon::parse($endDate)->format('M jS, Y')}}</i></small>
                </div>
                <div class="card-body">
                    
                    <canvas id="iusChart" height="75"></canvas>
                    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.js"></script>
                    <script type="application/javascript">

                        Chart.defaults.global.defaultFontColor = 'white';

                        new Chart(document.getElementById('iusChart'), {
                            type: 'bar',

                            data: {
                                labels: [
                                    @foreach($ius as $answer)
                                        @if($answer->citric_IS == 'Immune')
                                            "Immune",
                                        @elseif($answer->citric_IS == 'Susceptible')
                                            "Susceptible",
                                        @else
                                            "Unknown",
                                        @endif
                                    @endforeach
                                ],
                                datasets: [{
                                    label: "",
                                    backgroundColor: [
                                        @foreach($ius as $answer)
                                            @if($answer->citric_IS == 'Immune')
                                                'rgba(22, 145, 6, 0.6)',
                                            @elseif($answer->citric_IS == 'Susceptible')
                                                'rgba(194, 2, 12, 0.6)',
                                            @else
                                                'rgba(255, 255, 255, 0.6)',
                                            @endif
                                        @endforeach
                                    ],
                                    data: [

                                        @foreach($ius as $answer)
                                            {{$answer->total}},
                                        @endforeach
                                    ]
                                }]
                            },
                            options: {
                                legend: { display: false },
                                title: {
                                    //display: true,
                                    //text: 'Predicted world population (millions) in 2050'
                                },
                                scales: {
                                    yAxes: [{
                                        stacked:false,
                                    ticks: {
                                        beginAtZero: true
                                        }
                                    }]
                                }
                            }
                        });
                    </script>

                    <hr>

                    <canvas id="iusPie" height="125"></canvas>
                    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.js"></script>
                    <script type="application/javascript">

                        Chart.defaults.global.defaultFontColor = 'white';

                        new Chart(document.getElementById('iusPie'), {
                            type: 'pie',

                            data: {
                                labels: [
                                    @foreach($ius as $answer)
                                        @if($answer->citric_IS == 'Immune')
                                            "Immune",
                                        @elseif($answer->citric_IS == 'Susceptible')
                                            "Susceptible",
                                        @else
                                            "Unknown",
                                        @endif
                                    @endforeach
                                ],
                                datasets: [{
                                    label: "",
                                    backgroundColor: [
                                        @foreach($ius as $answer)
                                            @if($answer->citric_IS == 'Immune')
                                                'rgba(22, 145, 6, 0.6)',
                                            @elseif($answer->citric_IS == 'Susceptible')
                                                'rgba(194, 2, 12, 0.6)',
                                            @else
                                                'rgba(255, 255, 255, 0.6)',
                                            @endif
                                        @endforeach
                                    ],
                                    data: [

                                        @foreach($ius as $answer)
                                            {{$answer->total}},
                                        @endforeach
                                    ]
                                }]
                            },
                            options: 
                            {
                                // scales: {
                                //     xAxes: [{
                                //         ticks: {
                                //             beginAtZero: true
                                //         }
                                //     }]
                                // }, --for bar charts                         
                                title: 
                                {
                                    display: true,
                                    //text: 'Anything Else'
                                }
                            }
                        });
                    </script>

                    <hr>

                    <div class="card-deck mt-3">

                        <div class="card bg-dark" style="border-radius:0px;">
                            <div class="card-header">
                                Immune
                            </div>
                            <div class="card-body text-center">
                                <h4> {{ $immune }} </h4>
                            </div>
                        </div>
                        <div class="card bg-dark" style="border-radius:0px;">
                            <div class="card-header">
                                Susceptible
                            </div>
                            <div class="card-body text-center">
                                <h4> {{ $susceptible }} </h4>
                            </div>
                        </div>
                        <div class="card bg-dark" style="border-radius:0px;">
                            <div class="card-header">
                                Unknown
                            </div>
                            <div class="card-body text-center">
                                <h4> {{ $unknown }} </h4>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

            <div class="card bg-secondary" style="border-radius:0px;">
                <div class="card-header">
                    MD Treatments
                    <br>
                    <small class="text-warning"><i>{{Carbon::parse($startDate)->format('M jS, Y')}} - {{Carbon::parse($endDate)->format('M jS, Y')}}</i></small>
                </div>
                <div class="card-body">
                    
                    <canvas id="MDbar" height="75"></canvas>
                    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.js"></script>
                    <script type="application/javascript">

                        Chart.defaults.global.defaultFontColor = 'white';

                        new Chart(document.getElementById('MDbar'), {
                            type: 'bar',

                            data: {
                                labels: [
                                    @foreach($MD as $answer)
                                        @if($answer->HD_MD == 'Chiang')
                                            "Chiang",
                                        @elseif($answer->HD_MD == 'McCleish')
                                            "McCleish",
                                        @else
                                            "Urbonas",
                                        @endif
                                    @endforeach
                                ],
                                datasets: [{
                                    label: "",
                                    backgroundColor: [
                                        @foreach($MD as $answer)
                                            @if($answer->HD_MD == 'Chiang')
                                                'rgba(35, 99, 132, 0.6)',
                                            @elseif($answer->HD_MD == 'McCleish')
                                                'rgba(89, 186, 188, 0.6)',
                                            @else
                                                'rgba(168, 70, 200, 0.6)',
                                            @endif
                                        @endforeach
                                    ],
                                    data: [

                                        @foreach($MD as $answer)
                                            {{$answer->total}},
                                        @endforeach
                                    ]
                                }]
                            },
                            options: {
                                legend: { display: false },
                                title: {
                                    //display: true,
                                    //text: 'Predicted world population (millions) in 2050'
                                },
                                scales: {
                                    yAxes: [{
                                        stacked:false,
                                    ticks: {
                                        beginAtZero: true
                                        }
                                    }]
                                }
                            }
                        });
                    </script>

                    <hr>

                    <canvas id="MDPie" height="125"></canvas>
                    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.js"></script>
                    <script type="application/javascript">

                        Chart.defaults.global.defaultFontColor = 'white';

                        new Chart(document.getElementById('MDPie'), {
                            type: 'pie',

                            data: {
                                labels: [
                                    @foreach($MD as $answer)
                                        @if($answer->HD_MD == 'Chiang')
                                            "Chiang",
                                        @elseif($answer->HD_MD == 'McCleish')
                                            "McCleish",
                                        @else
                                            "Urbonas",
                                        @endif
                                    @endforeach
                                ],
                                datasets: [{
                                    label: "",
                                    backgroundColor: [
                                        @foreach($MD as $answer)
                                            @if($answer->HD_MD == 'Chiang')
                                                'rgba(35, 99, 132, 0.6)',
                                            @elseif($answer->HD_MD == 'McCleish')
                                                'rgba(89, 186, 188, 0.6)',
                                            @else
                                                'rgba(168, 70, 200, 0.6)',
                                            @endif
                                        @endforeach
                                    ],
                                    data: [

                                        @foreach($MD as $answer)
                                            {{$answer->total}},
                                        @endforeach
                                    ]
                                }]
                            },
                            options: 
                            {
                                // scales: {
                                //     xAxes: [{
                                //         ticks: {
                                //             beginAtZero: true
                                //         }
                                //     }]
                                // }, --for bar charts                         
                                title: 
                                {
                                    display: true,
                                    //text: 'Anything Else'
                                }
                            }
                        });
                    </script>

                    <hr>

                    <div class="card-deck mt-3">

                        <div class="card bg-dark" style="border-radius:0px;">
                            <div class="card-header">
                                Chiang
                            </div>
                            <div class="card-body text-center">
                                <h4> {{ $Chiang }} </h4>
                            </div>
                        </div>
                        <div class="card bg-dark" style="border-radius:0px;">
                            <div class="card-header">
                                McCleish
                            </div>
                            <div class="card-body text-center">
                                <h4> {{ $McCleish }} </h4>
                            </div>
                        </div>
                        <div class="card bg-dark" style="border-radius:0px;">
                            <div class="card-header">
                                Urbonas
                            </div>
                            <div class="card-body text-center">
                                <h4> {{ $Urbonas }} </h4>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

        </div>

    </div>
</div>

<div class="card-deck">

    <div class="card mb-4 text-white bg-dark">
        <div class="card-header">
            Dialysis Treatment Log Table View
        </div>
        <div class="card-body">

            <div class="table-responsive">
                <table class="table table-dark table-hover table-striped table-bordered" id="log">
                    <thead>
                        <tr>
                            <th colspan="7" class="text-center text-warning bg-secondary">Dialysis Log <br><small><i>This table shows {{Carbon::parse($startDate)->format('M jS, Y')}} - {{Carbon::parse($endDate)->format('M jS, Y')}}</i></small></th>
                        </tr>
                        <tr>
                            <th>Patient</th>
                            <th>Med Rec</th>
                            <th>Date</th>
                            <th>HD MD</th>
                            <th>Machine #</th>
                            <th>Station #</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($logs as $log)
                            <tr>
                                <td>{{ $log->patient }}</td>
                                <td>{{ $log->med_rec }}</td>
                                <td>{{ Carbon::parse($log->date)->format('m-d-Y') }}</td>
                                <td>{{ $log->HD_MD }}</td>
                                <td>{{ $log->machine_num }}</td>
                                <td>{{ $log->station_num }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>

    <div class="card mb-4 text-white bg-dark">
        <div class="card-header">
            Dialysis Surveillance Log Table View
        </div>
        <div class="card-body">

            <table class="table table-dark table-hover table-striped table-bordered" id="surv">
                <thead>
                    <tr>
                        <th colspan="7" class="text-center text-warning bg-secondary">Surveillance Log <br><small><i>This table shows {{Carbon::parse($startDate)->format('M jS, Y')}} - {{Carbon::parse($endDate)->format('M jS, Y')}}</i></small></th>
                    </tr>
                    <tr>
                        <th>Date</th>
                        <th>Patient</th>
                        <th>Nurse</th>
                        <th>Hospital MR #</th>
                        <th>Machine #</th>
                        <th>Treatment Location</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($surveillance as $surv)
                        <tr>
                            <td>{{ Carbon::parse($surv->date)->format('m-d-Y') }}</td>
                            <td>{{ $surv->patient }}</td>
                            <td>{{ $surv->nurse }}</td>
                            <td>{{ $surv->hospital_MR }}</td>
                            <td>{{ $surv->machine_num }}</td>
                            <td>{{ $surv->treatment_location }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>

</div>

@include('dialysis.modals.report')

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">
    $(document).ready(function() {
        $('#log').DataTable( {
            "pageLength": 20,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>

<script type="application/javascript">
    $(document).ready(function() {
        $('#surv').DataTable( {
            "pageLength": 20,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>

@endsection

@endif