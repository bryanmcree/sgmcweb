{{--New file Template--}}

{{--Add Security for this page below--}}
@if( Auth::user()->hasRole('dialysis') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    <div class="card mb-4 text-white bg-dark mx-auto" style="width:60%;">
        <div class="card-header">
            Edit Dialysis Log Entry
            <span class="float-right"><a href="/dialysis" class="btn btn-sm btn-secondary">Return to Dashboard</a></span>
        </div>
        <div class="card-body">
            
            <form action="/dialysis/log/update/{{ $log->id }}" method="POST">
                {{ csrf_field() }}
                
                <div class="form-row">
                    <div class="form-group col-4">
                        <label for="patient">Patient Name (Last, First)</label>
                        <input type="text" class="form-control" name="patient" value="{{ $log->patient }}">
                    </div>
                    <div class="form-group col-4">
                        <label for="med_rec">Med Rec</label>
                        <input type="text" class="form-control" name="med_rec" value="{{ $log->med_rec }}">
                    </div>
                    <div class="form-group col-4">
                        <label for="first_treatment">Is this the Patients First Treatment?</label>
                        <select name="first_treatment" class="form-control">
                            <option value="{{ $log->first_treatment }}"> @if($log->first_treatment == '0') No @else Yes @endif </option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-6">
                        <label for="date">Date</label>
                        <input type="date" class="form-control" name="date" value={{ $log->date }}>
                    </div>
                    <div class="form-group col-6">
                        <label for="hgb">HGB</label>
                        <input type="number" step="0.01" class="form-control" name="hgb" value="{{ $log->hgb }}">
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-2">
                        <label for="tx_herapin">Tx with Herapin</label>
                        <select name="tx_herapin" class="form-control">
                            <option value="{{ $log->tx_herapin }}" selected> @if($log->tx_herapin == '0') No @else Yes @endif </option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </div>
                    <div class="form-group col-3">
                        <label for="tx_cistrate">Tx with Naturalyte/CitraPure</label>
                        <select name="tx_cistrate" class="form-control">
                            <option value="{{ $log->tx_cistrate }}" selected> {{ $log->tx_cistrate }} </option>
                            <option value="CitraPure">CitraPure</option>
                            <option value="Naturalyte">Naturalyte</option>
                        </select>
                    </div>
                    <div class="form-group col-3">
                        <label for="fis_cath">Fistula/Catheter/Graft</label>
                        <select name="fis_cath" class="form-control">
                            <option value="{{ $log->fis_cath }}">{{ $log->fis_cath }}</option>
                            <option value="Fistula">Fistula</option>
                            <option value="Catheter">Catheter</option>
                            <option value="Graft">Graft</option>
                        </select>
                    </div>
                    <div class="form-group col-2">
                        <label for="clotted_tx">Clotted during Tx</label>
                        <select name="clotted_tx" class="form-control">
                            <option value="{{ $log->clotted_tx }}" selected> @if($log->clotted_tx == '0') No @else Yes @endif </option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </div>
                    <div class="form-group col-2">
                        <label for="tx_yesterday">Tx Yesterday</label>
                        <select name="tx_yesterday" class="form-control">
                            <option value="{{ $log->tx_yesterday }}" selected> @if($log->tx_yesterday == '0') No @else Yes @endif </option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-4">
                        <label for="procrit_dose">Procrit Dose</label>
                        <input type="number" step="0.01" class="form-control" name="procrit_dose" value="{{ $log->procrit_dose }}">
                    </div>
                    <div class="form-group col-4">
                        <label for="vials_open">Vials Open</label>
                        <input type="number" step="0.01" class="form-control" name="vials_open" value="{{ $log->vials_open }}">
                    </div>
                    <div class="form-group col-4">
                        <label for="HD_MD">HD MD</label>
                        <select name="HD_MD" class="form-control">
                            <option value="{{ $log->HD_MD }}">{{ $log->HD_MD }}</option>
                            <option value="Chiang">Chiang</option>
                            <option value="McCleish">McCleish</option>
                            <option value="Urbonas">Urbonas</option>
                        </select>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-6">
                        <label for="start">Start</label>
                        <input type="time" class="form-control" name="start" value={{ Carbon::parse($log->start)->format('H:i:s') }}>
                    </div>
                    <div class="form-group col-6">
                        <label for="stop">Stop</label>
                        <input type="time" class="form-control" name="stop" value={{ Carbon::parse($log->stop)->format('H:i:s') }}>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-6">
                        <label for="machine_num">Machine #</label>
                        <input type="text" class="form-control" name="machine_num" value="{{ $log->machine_num }}">
                    </div>
                    <div class="form-group col-6">
                        <label for="station_num">Station #</label>
                        <input type="text" class="form-control" name="station_num" value="{{ $log->station_num }}">
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-6">
                        <label for="citric">Disinfection of Machine</label>
                        <select name="citric" class="form-control">
                            <option value="{{ $log->citric }}" selected>@if($log->citric == '1') Citric @else Descale @endif </option>
                            <option value="1">Citric</option>
                            <option value="0">Descale</option>
                        </select>
                    </div>
                    <div class="form-group col-6">
                        <label for="citric_IS">Hepatitis Status</label>
                        <select name="citric_IS" class="form-control">
                            <option value="{{ $log->citric_IS }}" selected>{{ $log->citric_IS }}</option>
                            <option value="Immune">Immune</option>
                            <option value="Susceptible">Susceptible</option>
                            <option value="Unknown">Unknown</option>
                            <option value="Known (positive Hep B)">Known (positive Hep B)</option>
                        </select>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-6">
                        <label for="failed_treatment">Failed Treatment?</label>
                        <select name="failed_treatment" class="form-control">
                            <option value="{{ $log->failed_treatment }}"> @if($log->failed_treatment == '1') Yes @else No @endif </option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </div>
                    <div class="form-group col-6">
                        <label for="AMA">Sign off AMA?</label>
                        <select name="AMA" class="form-control">
                            <option value="{{ $log->AMA }}"> @if($log->AMA == '1') Yes @else No @endif </option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="comments">Comments</label>
                    <textarea name="comments" rows="5" class="form-control">{{ $log->comments }}</textarea>
                </div>

                <input type="submit" class="btn btn-primary btn-block" value="Update">

            </form>

        </div>
    </div>


@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection

@endif