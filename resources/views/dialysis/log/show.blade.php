{{--New file Template--}}

{{--Add Security for this page below--}}
@if( Auth::user()->hasRole('dialysis') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')


    <div class="card mb-4 text-white bg-dark">
        <div class="card-header">
            Dialysis Log Detailed Page
            <span class="float-right"> <a href="/dialysis" class="btn btn-sm btn-secondary">Return to Dashboard</a> </span>
        </div>
        <div class="card-body">

            <div class="table-responsive">
                <table class="table table-dark table-hover table-striped table-bordered">
                    <thead>
                        <th nowrap class="text-primary">Actions</th>
                        <th nowrap>Patient</th>
                        <th nowrap>Med Rec</th>
                        <th nowrap>First Treatment</th>
                        <th nowrap>Failed Treatment</th>
                        <th nowrap>Sign off AMA</th>
                        <th nowrap>Date</th>
                        <th nowrap>HGB</th>
                        <th nowrap>TX with Heparin</th>
                        <th nowrap>TX with Citrasate</th>
                        <th nowrap>Fistula or Catheter</th>
                        <th nowrap>Clotted During TX</th>
                        <th nowrap>TX Yesterday?</th>
                        <th nowrap>Procrit Dose</th>
                        <th nowrap>Vials Opened</th>
                        <th nowrap>HD MD</th>
                        <th nowrap>Start</th>
                        <th nowrap>Stop</th>
                        <th nowrap>TX Time</th>
                        <th nowrap>Machine #</th>
                        <th nowrap>Station #</th>
                        <th nowrap>Citric</th>
                        <th nowrap>I/S/U</th>
                        <th nowrap>Entered By</th>
                        <th nowrap>Comments</th>
                    </thead>
                    <tbody>
                        @foreach($logs as $log)
                            <tr class="text-center">
                                <td nowrap> 
                                    <a href="/dialysis/log/edit/{{ $log->id }}" class="btn btn-sm btn-warning" title="Edit"><i class="fas fa-edit"></i></a> 
                                    <a href="#" class="btn btn-sm btn-danger deleteLog" log_id="{{ $log->id }}" title="Delete"><i class="fas fa-trash-alt"></i></a> 
                                </td>
                                <td nowrap>{{ $log->patient }}</td>
                                <td nowrap>{{ $log->med_rec }}</td>
                                <td nowrap> @if($log->first_treatment == '1') <i class="fas fa-check text-success"></i> @else <i class="fas fa-ban text-danger"></i> @endif </td>
                                <td nowrap> @if($log->failed_treatment == '1') <i class="fas fa-check text-success"></i> @else <i class="fas fa-ban text-danger"></i> @endif </td>
                                <td nowrap> @if($log->AMA == '1') <i class="fas fa-check text-success"></i> @else <i class="fas fa-ban text-danger"></i> @endif </td>
                                <td nowrap>{{ Carbon::parse($log->date)->format('m-d-Y') }}</td>
                                <td nowrap>{{ $log->hgb }}</td>
                                <td nowrap> @if($log->tx_herapin == '1') <i class="fas fa-check text-success"></i> @else <i class="fas fa-ban text-danger"></i> @endif </td>
                                <td nowrap>{{ $log->tx_cistrate }}</td>
                                <td nowrap>{{ $log->fis_cath }}</td>
                                <td nowrap> @if($log->clotted_tx == '1') <i class="fas fa-check text-success"></i> @else <i class="fas fa-ban text-danger"></i> @endif </td>
                                <td nowrap> @if($log->tx_yesterday == '1') <i class="fas fa-check text-success"></i> @else <i class="fas fa-ban text-danger"></i> @endif </td>
                                <td nowrap>{{ $log->procrit_dose }}</td>
                                <td nowrap>{{ $log->vials_open }}</td>
                                <td nowrap>{{ $log->HD_MD }}</td>
                                <td nowrap>{{ Carbon::parse($log->start)->format('H:i') }}</td>
                                <td nowrap>{{ Carbon::parse($log->stop)->format('H:i') }}</td>
                                <td nowrap>{{ Carbon::parse($log->tx_time)->format('H:i') }}</td>
                                <td nowrap>{{ $log->machine_num }}</td>
                                <td nowrap>{{ $log->station_num }}</td>
                                <td nowrap>@if($log->citric == '1') Citric @else Descale @endif </td>
                                <td nowrap>{{ $log->citric_IS }}</td>
                                <td nowrap>@if(is_null($log->createdBy)) ? @else {{ $log->createdBy->name }} @endif </td>
                                <td>{{ $log->comments }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <span class="float-right mt-3"> @include('pagination', ['paginator' => $logs]) </span>

        </div>
    </div>


@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">

    $(document).on("click", ".deleteLog", function () {
            var log_id = $(this).attr("log_id");
            DeleteLog(log_id);
        });
    
    function DeleteLog(log_id) {
        swal({
            title: "Delete Log?",
            text: "Deleting this Log cannot be undone.",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            confirmButtonText: "Yes, delete it!",
            confirmButtonColor: "#ec6c62"
        }, function() {
            $.ajax({
                url: "/dialysis/log/destroy/" + log_id,
                type: "GET"
            })
                .done(function(data) {
                    swal({
                            title: "Deleted",
                            text: "Log Deleted",
                            type: "success",
                            timer: 1800,
                            showConfirmButton: false
    
                        },function() {
                            location.reload();
                        });
                })
                .error(function(data) {
                    swal("Oops", "We couldn't connect to the server!", "error");
                });
        });
    }
    
    </script>

@endsection

@endif