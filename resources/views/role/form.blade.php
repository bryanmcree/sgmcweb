
            <div class="form-group">
                {!! Form::label('Name', 'Name:') !!}
                {!! Form::text('name', null, ['class' => 'form-control','placeholder'=>'Name of the role to be assigned ie Systems List - Delete']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('label', 'Label:') !!}
                {!! Form::text('label', null, ['class' => 'form-control','placeholder'=>'Description of the role and what it secures.']) !!}
            </div>


