@extends('layouts.app')

@section('content')
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {!! Form::open(array('action' => ['RoleController@store'], 'class' => 'form_control')) !!}
    <div class="card bg-dark text-white">
        <div class="card-header"><b>Create Roles</b></div>
        <div class="card-body">
            @include('role.form')
        </div>
        <div class="card-footer">
            {!! Form::submit('Add Role', ['class'=>'btn btn-primary btn-block']) !!}
        </div>
    </div>
    
    {!! Form::close() !!}

@endsection