@extends('layouts.app')

@section('content')
    <div class="card mb-4 border-info text-white bg-dark">
        <div class="card-header"><b>User Roles ({{$roles->count()}})</b></div>
        <div class="card-body">
            @if ($roles->isEmpty())
                <div class="alert alert-info" role="alert">You do not have any roles entered.  <a href="/role/create" role="button" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> New Role</a></div>
            @else
                <p><a href = "/role/create" class = "btn btn-success" role = "button">Add Role</a></p>
                <table class="table table-hover table-dark mb-4 table-bordered" id="roles">
                    <thead>
                    <tr>
                        <td><b>ID</b></td>
                        <td><b>Name</b></td>
                        <td><b>Label</b></td>
                        <td><b>Delete</b></td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($roles as $role)
                        <tr>
                            <td>{{$role->id}}</td>
                            <td>{{$role->name}}</td>
                            <td nowrap="">{{$role->label}}</td>
                            <td> <a href="#" class="btn btn-danger btn-sm btn-block deleteRole" role_id = {{ $role->id }}>Delete</a> </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>

@endsection

@section('scripts')

    <script type="text/javascript">

        $(document)
            .ready(function () {
                $('#roles').dataTable({
                    "pageLength": 25
                });
            });

    </script>

    <script type="application/javascript">

    $(document).on("click", ".deleteRole", function () {
                //alert($(this).attr("data-cost_center"));
                var role_id = $(this).attr("role_id");
                DeleteRole(role_id);
            });

            function DeleteRole(role_id) {
                swal({
                    title: "Delete Role?",
                    text: "Deleting this Role cannot be undone.",
                    type: "warning",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    confirmButtonText: "Yes, delete it!",
                    confirmButtonColor: "#ec6c62"
                }, function() {
                    $.ajax({
                        url: "/role/destroy/" + role_id,
                        type: "GET"
                    })
                        .done(function(data) {
                            swal({
                                    title: "Deleted",
                                    text: "Role Deleted",
                                    type: "success",
                                    timer: 2200,
                                    showConfirmButton: false

                                }
                            );
                            setTimeout(function(){window.location.replace('/role')},1900);
                        })
                        .error(function(data) {
                            swal("Oops", "We couldn't connect to the server!", "error");
                        });
                });
            }

    </script>

@endsection
