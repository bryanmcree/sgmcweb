{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('DOS') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')
    <style>
        @media (max-width: 767px) {
            .carousel-indicators,
            .carousel-indicators li {
                cursor: default;
                pointer-events: none;
            }
        }
    </style>

    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#"><b>Discharges</b>
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#" onClick="javascript:goToSlide(2);">Acute Discharges</a></li>
                        <li><a href="#" onClick="javascript:goToSlide(3);">ADLT/PED Discharges</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#"><b>Emergency Department</b>
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#" onClick="javascript:goToSlide(5);">ED Admits to Bed</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#"><b>Length of Stay</b>
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#" onClick="javascript:goToSlide(1);">Average Length of Stay</a></li>
                        <li><a href="#" onClick="javascript:goToSlide(0);">Acute Average Length of Stay</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#"><b>Patient Days</b>
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#" onClick="javascript:goToSlide(4);">ADLT/PED Patient Days</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#"><b>Revenue</b>
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#" onClick="javascript:goToSlide(6);">Patient Revenue</a></li>
                        <li><a href="#" onClick="javascript:goToSlide(7);">PB Revenue</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#"><b>Imaging</b>
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#" onClick="javascript:goToSlide(8);">CT Scans</a></li>
                        <li><a href="#" onClick="javascript:goToSlide(9);">Mammography</a></li>
                        <li><a href="#" onClick="javascript:goToSlide(10);">MIR</a></li>
                        <li><a href="#" onClick="javascript:goToSlide(11);">Nuclear Medicine</a></li>
                        <li><a href="#" onClick="javascript:goToSlide(12);">Ultrasounds</a></li>
                        <li><a href="#" onClick="javascript:goToSlide(13);">X-Rays</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#" id="pauseButton"><i class="fa fa-pause" aria-hidden="true"></i> Pause</a></li>
                <li><a href="#" id="playButton"><i class="fa fa-play" aria-hidden="true"></i> Play</a></li>
            </ul>
        </div>
    </nav>



    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->


        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel panel-heading">
                            <B>Acute Average Length of Stay by Location</B>
                        </div>
                        <div class="panel-body">
                            <canvas id="ACUTE_ALOS" width="100%" height="30"></canvas>
                        </div>
                    </div>
                </div>
            </div>

            <div class="item">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel panel-heading">
                            <B>Average Length of Stay by Location</B>
                        </div>
                        <div class="panel-body">
                            <canvas id="ALOS" width="100%" height="30"></canvas>
                        </div>
                    </div>
                </div>
            </div>

            <div class="item">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel panel-heading">
                            <B>Acute Discharges by Location</B>
                        </div>
                        <div class="panel-body">
                            <canvas id="ACUTE_DISCHARGES" width="100%" height="30"></canvas>
                        </div>
                    </div>
                </div>
            </div>

            <div class="item">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel panel-heading">
                            <B>ADLT/PED Discharges by Location</B>
                        </div>
                        <div class="panel-body">
                            <canvas id="ADLT/PED Discharges" width="100%" height="30"></canvas>
                        </div>
                    </div>
                </div>
            </div>

            <div class="item">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel panel-heading">
                            <B>ADLT/PED Patient Days by Location</B>
                        </div>
                        <div class="panel-body">
                            <canvas id="ADULT/PED PATIENT_DAYS" width="100%" height="30"></canvas>
                        </div>
                    </div>
                </div>
            </div>

            <div class="item">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel panel-heading">
                            <B>ED Admits to Bed by Location</B>
                        </div>
                        <div class="panel-body">
                            <canvas id="ED_ADMITS_TO_BED" width="100%" height="30"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel panel-heading">
                            <B>Patient Revenue</B>
                        </div>
                        <div class="panel-body">
                            <canvas id="PATIENT_REV" width="100%" height="30"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel panel-heading">
                            <B>PB Revenue</B>
                        </div>
                        <div class="panel-body">
                            <canvas id="PB_REVENUE" width="100%" height="30"></canvas>
                        </div>
                    </div>
                </div>
            </div>



        </div>

    </div>


@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="application/javascript">

        //Caro
        function goToSlide(number) {
            $("#myCarousel").carousel(number);
        }

        $('#myCarousel').carousel({
            interval: 10000
        });

        $('#playButton').click(function () {
            $('#myCarousel').carousel('cycle');
        });
        $('#pauseButton').click(function () {
            $('#myCarousel').carousel('pause');
        });


        //Acute Adv LOS
        new Chart(document.getElementById("ACUTE_ALOS").getContext('2d'), {
            type: 'line',
            responsive:true,
            data: {
                labels: [

                    @foreach($chart_lables as $lable)
                    '{{$lable->rpt_date}}',
                    @endforeach

                ],
                datasets: [{
                    data: [
                        @foreach($ber as $ber_value)
                            {{$ber_value->value}},
                        @endforeach
                        ],
                    label: "BER Parent Location",
                    borderColor: "#3e95cd",
                    fill: false
                }, {
                    data: [
                        @foreach($lan as $lan_value)
                        {{$lan_value->value}},
                        @endforeach
                    ],
                    label: "LAN Parent Location",
                    borderColor: "#8e5ea2",
                    fill: false
                }, {
                    data: [
                        @foreach($sga as $sga_value)
                        {{$sga_value->value}},
                        @endforeach
                    ],
                    label: "SGA Parent Location",
                    borderColor: "#3cba9f",
                    fill: false
                }
                ]
            },
            options: {
                animation: {
                    duration: 750
                }
            }
        });



        // Adv LOS
        new Chart(document.getElementById("ALOS").getContext('2d'), {
            type: 'line',
            data: {
                labels: [

                    @foreach($chart_lables as $lable)
                        '{{$lable->rpt_date}}',
                    @endforeach

                ],
                datasets: [{
                    data: [
                        @foreach($ALOS_ber as $ber_value)
                        {{$ber_value->value}},
                        @endforeach
                    ],
                    label: "BER Parent Location",
                    borderColor: "#3e95cd",
                    fill: false
                }, {
                    data: [
                        @foreach($ALOS_lan as $lan_value)
                        {{$lan_value->value}},
                        @endforeach
                    ],
                    label: "LAN Parent Location",
                    borderColor: "#8e5ea2",
                    fill: false
                }, {
                    data: [
                        @foreach($ALOS_sga as $sga_value)
                        {{$sga_value->value}},
                        @endforeach
                    ],
                    label: "SGA Parent Location",
                    borderColor: "#3cba9f",
                    fill: false
                }, {
                    data: [
                        @foreach($ALOS_vil as $vil_value)
                        {{$sga_value->value}},
                        @endforeach
                    ],
                    label: "VIL Parent Location",
                    borderColor: "#D42A4F",
                    fill: false
                }
                ]
            },
            options: {
                animation: {
                    duration: 750
                }
            }
        });


        // ACUTE_DISCHARGES
        new Chart(document.getElementById("ACUTE_DISCHARGES").getContext('2d'), {
            type: 'line',
            data: {
                labels: [

                    @foreach($chart_lables as $lable)
                        '{{$lable->rpt_date}}',
                    @endforeach

                ],
                datasets: [{
                    data: [
                        @foreach($AcuteDis_ber as $ber_value)
                        {{$ber_value->value}},
                        @endforeach
                    ],
                    label: "BER Parent Location",
                    borderColor: "#3e95cd",
                    fill: false
                },  {
                    data: [
                        @foreach($AcuteDis_sga as $sga_value)
                        {{$sga_value->value}},
                        @endforeach
                    ],
                    label: "SGA Parent Location",
                    borderColor: "#3cba9f",
                    fill: false
                }
                ]
            },
            options: {
                animation: {
                    duration: 750
                }
            }
        });


        // ADLT/PED Discharges
        new Chart(document.getElementById("ADLT/PED Discharges").getContext('2d'), {
            type: 'line',
            data: {
                labels: [

                    @foreach($chart_lables as $lable)
                        '{{$lable->rpt_date}}',
                    @endforeach

                ],
                datasets: [{
                    data: [
                        @foreach($adultDis_ber as $ber_value)
                        {{$ber_value->value}},
                        @endforeach
                    ],
                    label: "BER Parent Location",
                    borderColor: "#3e95cd",
                    fill: false
                }, {
                    data: [
                        @foreach($adultDis_lan as $lan_value)
                        {{$lan_value->value}},
                        @endforeach
                    ],
                    label: "LAN Parent Location",
                    borderColor: "#8e5ea2",
                    fill: false
                }, {
                    data: [
                        @foreach($adultDis_sga as $sga_value)
                        {{$sga_value->value}},
                        @endforeach
                    ],
                    label: "SGA Parent Location",
                    borderColor: "#3cba9f",
                    fill: false
                }, {
                    data: [
                        @foreach($adultDis_vil as $vil_value)
                        {{$sga_value->value}},
                        @endforeach
                    ],
                    label: "VIL Parent Location",
                    borderColor: "#D42A4F",
                    fill: false
                }
                ]
            },
            options: {
                animation: {
                    duration: 750
                }
            }
        });


        // ADULT/PED PATIENT_DAYS
        new Chart(document.getElementById("ADULT/PED PATIENT_DAYS").getContext('2d'), {
            type: 'line',
            data: {
                labels: [

                    @foreach($chart_lables as $lable)
                        '{{$lable->rpt_date}}',
                    @endforeach

                ],
                datasets: [{
                    data: [
                        @foreach($adult_days_ber as $ber_value)
                        {{$ber_value->value}},
                        @endforeach
                    ],
                    label: "BER Parent Location",
                    borderColor: "#3e95cd",
                    fill: false
                }, {
                    data: [
                        @foreach($adult_days_lan as $lan_value)
                        {{$lan_value->value}},
                        @endforeach
                    ],
                    label: "LAN Parent Location",
                    borderColor: "#8e5ea2",
                    fill: false
                }, {
                    data: [
                        @foreach($adult_days_sga as $sga_value)
                        {{$sga_value->value}},
                        @endforeach
                    ],
                    label: "SGA Parent Location",
                    borderColor: "#3cba9f",
                    fill: false
                }, {
                    data: [
                        @foreach($adult_days_vil as $vil_value)
                        {{$sga_value->value}},
                        @endforeach
                    ],
                    label: "VIL Parent Location",
                    borderColor: "#D42A4F",
                    fill: false
                }
                ]
            },
            options: {
                animation: {
                    duration: 750
                }
            }
        });

        // ED_ADMITS_TO_BED
        new Chart(document.getElementById("ED_ADMITS_TO_BED").getContext('2d'), {
            type: 'line',
            data: {
                labels: [

                    @foreach($chart_lables as $lable)
                        '{{$lable->rpt_date}}',
                    @endforeach

                ],
                datasets: [{
                    data: [
                        @foreach($ed_bed_ber as $ber_value)
                        {{$ber_value->value}},
                        @endforeach
                    ],
                    label: "BER Parent Location",
                    borderColor: "#3e95cd",
                    fill: false
                }, {
                    data: [
                        @foreach($ed_bed_lan as $lan_value)
                        {{$lan_value->value}},
                        @endforeach
                    ],
                    label: "LAN Parent Location",
                    borderColor: "#8e5ea2",
                    fill: false
                }, {
                    data: [
                        @foreach($ed_bed_sga as $sga_value)
                        {{$sga_value->value}},
                        @endforeach
                    ],
                    label: "SGA Parent Location",
                    borderColor: "#3cba9f",
                    fill: false
                }, {
                    data: [
                        @foreach($ed_bed_vil as $vil_value)
                        {{$sga_value->value}},
                        @endforeach
                    ],
                    label: "VIL Parent Location",
                    borderColor: "#D42A4F",
                    fill: false
                }
                ]
            },
            options: {
                animation: {
                    duration: 750
                }
            }
        });


        // PATIENT_REV
        new Chart(document.getElementById("PATIENT_REV").getContext('2d'), {
            type: 'line',
            data: {
                labels: [

                    @foreach($chart_lables as $lable)
                        '{{$lable->rpt_date}}',
                    @endforeach

                ],
                datasets: [{
                    data: [
                        @foreach($patient_rev_ber as $ber_value)
                        {{$ber_value->value}},
                        @endforeach
                    ],
                    label: "BER Parent Location",
                    borderColor: "#3e95cd",
                    fill: false
                }, {
                    data: [
                        @foreach($patient_rev_lan as $lan_value)
                        {{$lan_value->value}},
                        @endforeach
                    ],
                    label: "LAN Parent Location",
                    borderColor: "#8e5ea2",
                    fill: false
                }, {
                    data: [
                        @foreach($patient_rev_sga as $sga_value)
                        {{$sga_value->value}},
                        @endforeach
                    ],
                    label: "SGA Parent Location",
                    borderColor: "#3cba9f",
                    fill: false
                }, {
                    data: [
                        @foreach($patient_rev_vil as $vil_value)
                        {{$sga_value->value}},
                        @endforeach
                    ],
                    label: "VIL Parent Location",
                    borderColor: "#D42A4F",
                    fill: false
                }
                ]
            },
            options: {
                animation: {
                    duration: 750
                }
            }
        });


        // PB_REVENUE
        new Chart(document.getElementById("PB_REVENUE").getContext('2d'), {
            type: 'line',
            data: {
                labels: [

                    @foreach($chart_lables as $lable)
                        '{{$lable->rpt_date}}',
                    @endforeach

                ],
                datasets: [{
                    data: [
                        @foreach($pb_rev_ber as $one)
                        {{$one->value}},
                        @endforeach
                    ],
                    label: "BER Revenue Location",
                    borderColor: "#3e95cd",
                    fill: false
                }, {
                    data: [
                        @foreach($pb_rev_lucas as $two)
                        {{$two->value}},
                        @endforeach
                    ],
                    label: "LAN Dr Lucas",
                    borderColor: "#8e5ea2",
                    fill: false
                }, {
                    data: [
                        @foreach($pb_rev_lan as $three)
                        {{$three->value}},
                        @endforeach
                    ],
                    label: "LAN Revenue Location",
                    borderColor: "#3cba9f",
                    fill: false
                }, {
                    data: [
                        @foreach($pb_rev_asc as $four)
                        {{$four->value}},
                        @endforeach
                    ],
                    label: "SGA ASC",
                    borderColor: "#D42A4F",
                    fill: false
                }, {
                    data: [
                        @foreach($pb_rev_ashley as $five)
                        {{$five->value}},
                        @endforeach
                    ],
                    label: "SGA Ashley Street OPC",
                    borderColor: "#3e95cd",
                    fill: false
                }, {
                    data: [
                        @foreach($pb_rev_cancer as $six)
                        {{$six->value}},
                        @endforeach
                    ],
                    label: "SGA CANCER CENTER",
                    borderColor: "#8e5ea2",
                    fill: false
                }, {
                    data: [
                        @foreach($pb_rev_cardio as $seven)
                        {{$seven->value}},
                        @endforeach
                    ],
                    label: "SGA CARDIOLOGY CLINIC",
                    borderColor: "#3cba9f",
                    fill: false
                }, {
                    data: [
                        @foreach($pb_rev_cvi as $eight)
                        {{$eight->value}},
                        @endforeach
                    ],
                    label: "SGA CVI",
                    borderColor: "#D42A4F",
                    fill: false
                }, {
                    data: [
                        @foreach($pb_rev_diabetes as $nine)
                        {{$nine->value}},
                        @endforeach
                    ],
                    label: "SGA Diabetes Mgmt",
                    borderColor: "#3e95cd",
                    fill: false
                }, {
                    data: [
                        @foreach($pb_rev_family as $ten)
                        {{$lan_value->value}},
                        @endforeach
                    ],
                    label: "SGA FAMILY PRACTICE",
                    borderColor: "#8e5ea2",
                    fill: false
                }, {
                    data: [
                        @foreach($pb_rev_surgery as $eleven)
                        {{$sga_value->value}},
                        @endforeach
                    ],
                    label: "SGA General Surgery",
                    borderColor: "#3cba9f",
                    fill: false
                }, {
                    data: [
                        @foreach($pb_rev_south as $twelve)
                        {{$twelve->value}},
                        @endforeach
                    ],
                    label: "SGA Healthcare South",
                    borderColor: "#D42A4F",
                    fill: false
                }, {
                    data: [
                        @foreach($pb_rev_img as $thirteen)
                        {{$thirteen->value}},
                        @endforeach
                    ],
                    label: "SGA Imaging Center",
                    borderColor: "#3e95cd",
                    fill: false
                }, {
                    data: [
                        @foreach($pb_rev_sga as $fourteen)
                        {{$fourteen->value}},
                        @endforeach
                    ],
                    label: "SGA Revenue Location",
                    borderColor: "#8e5ea2",
                    fill: false
                }, {
                    data: [
                        @foreach($pb_rev_snh1 as $fifteen)
                        {{$fifteen->value}},
                        @endforeach
                    ],
                    label: "SNH Parent Location",
                    borderColor: "#3cba9f",
                    fill: false
                }, {
                    data: [
                        @foreach($pb_rev_snh2 as $sixteen)
                        {{$sixteen->value}},
                        @endforeach
                    ],
                    label: "SNH Revenue Location",
                    borderColor: "#D42A4F",
                    fill: false
                }
                ]
            },
            options: {
                animation: {
                    duration: 750
                }
            }
        });


    </script>


@endsection
@endif