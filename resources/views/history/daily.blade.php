{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('History - Widget') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <b>Today's Activity</b>
            </div>
            <div class="panel-body">
                <table class="table table-condensed table-striped table-bordered" id="example">
                    <thead>
                        <tr>
                            <td><b>Action</b></td>
                            <td><b>UserName</b></td>
                            <td><b>IP</b></td>
                            <td><b>Details</b></td>
                            <td><b>Date/Time</b></td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($entries as $entry)
                        <tr>
                            <td>{{$entry->action}}</td>
                            <td><a href="/search?search={{$entry->userid}}">{{$entry->userid}}</a></td>
                            <td>{{$entry->user_ip}}</td>
                            <td>{{$entry->search_string}}</td>
                            <td>{{$entry->created_at}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="application/javascript">
        $(document)
            .ready(function () {
                $('#example').dataTable({
                    "order": [[ 4, "desc" ]],
                    "pageLength": 100
                });
            });
    </script>

@endsection
@endif