
<canvas id="canvas" height="250"></canvas>


<script type="application/javascript">

    var jsonfile = $.ajax({
        url: "/json/history",
        dataType:"json",
        async: false,
    }).responseJSON;


   var labels = jsonfile.jsonarray.map(function(e) {
        return e.action;
    });
    var data = jsonfile.jsonarray.map(function(e) {
        return e.total;
    });;

    var ctx = canvas.getContext('2d');
    Chart.defaults.global.defaultFontColor = '#fff';
    var config = {
        type: 'horizontalBar',
        options: {
            animation: {
                duration: 0
            },
            legend: { display: false },
            title: {
                display: false,
                text: 'Number of part two checks by each employee.',
                defaultFontColor: '#fff',
            },
            scales:{
                yAxes: [{
                    gridLines: {
                        color: 'rgba(199, 162, 225)',
                        lineWidth: 0.2
                    }
                }],
                xAxes: [{
                    gridLines: {
                        color: 'rgba(199, 162, 225)',
                        lineWidth: 0.2
                    }
                }]
            }
        },
        data: {
            labels: labels,
            datasets: [{
                defaultFontColor: '#fff',
                label: 'History',
                data: data,
                //backgroundColor: 'rgba(0, 119, 204, 0.3)'
                backgroundColor: [
                    'rgba(0, 99, 132, 0.3)',
                    'rgba(5, 99, 132, 0.3)',
                    'rgba(10, 99, 132, 0.3)',
                    'rgba(15, 99, 132, 0.3)',
                    'rgba(20, 99, 132, 0.3)',
                    'rgba(25, 99, 132, 0.3)',
                    'rgba(30, 99, 132, 0.3)',
                    'rgba(35, 99, 132, 0.3)',
                    'rgba(40, 99, 132, 0.3)',
                    'rgba(45, 99, 132, 0.3)',
                    'rgba(50, 99, 132, 0.3)',
                    'rgba(55, 99, 132, 0.3)',
                    'rgba(60, 99, 132, 0.3)',
                    'rgba(65, 99, 132, 0.3)',
                    'rgba(70, 99, 132, 0.3)',
                    'rgba(75, 99, 132, 0.3)',
                    'rgba(80, 99, 132, 0.3)',
                    'rgba(85, 99, 132, 0.3)',
                    'rgba(90, 99, 132, 0.3)',
                    'rgba(95, 99, 132, 0.3)',
                    'rgba(100, 99, 132, 0.3)',
                    'rgba(105, 99, 132, 0.3)',
                    'rgba(110, 99, 132, 0.3)',
                    'rgba(115, 99, 132, 0.3)',
                ],
                borderColor: [
                    'rgba(115, 99, 132, 1)',
                    'rgba(110, 99, 132, 1)',
                    'rgba(105, 99, 132, 1)',
                    'rgba(100, 99, 132, 1)',
                    'rgba(95, 99, 132, 1)',
                    'rgba(90, 99, 132, 1)',
                    'rgba(85, 99, 132, 1)',
                    'rgba(80, 99, 132, 1)',
                    'rgba(75, 99, 132, 1)',
                    'rgba(70, 99, 132, 1)',
                    'rgba(65, 99, 132, 1)',
                    'rgba(60, 99, 132, 1)',
                    'rgba(55, 99, 132, 1)',
                    'rgba(50, 99, 132, 1)',
                    'rgba(45, 99, 132, 1)',
                    'rgba(40, 99, 132, 1)',
                    'rgba(35, 99, 132, 1)',
                    'rgba(30, 99, 132, 1)',
                    'rgba(25, 99, 132, 1)',
                    'rgba(20, 99, 132, 1)',
                    'rgba(15, 99, 132, 1)',
                    'rgba(10, 99, 132, 1)',
                    'rgba(5, 99, 132, 1)',
                    'rgba(0, 99, 132, 1)',
                ],
                borderWidth: 2,
            }]
        }
    };

    var chart = new Chart(ctx, config);

</script>