@extends('layouts.app')

@section('content')
    <div class="col-md-12">
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {!! Form::model($users, ['method'=>'PATCH', 'action'=>['UserController@update', $users->id], 'files' => true]) !!}
    <div class="panel panel-success">
        <div class="panel-heading"><b>Edit User</b></div>
        <div class="panel-body">
            @include('user.form')
        </div>

        <div class="panel-footer">
    {!! Form::submit('Update User', ['class'=>'btn btn-default']) !!}
    {!! Form::reset('Clear', ['class'=>'btn btn-default']) !!}
    {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection