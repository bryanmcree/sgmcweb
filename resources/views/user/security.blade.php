@extends('layouts.app')

@section('content')
    <div class="panel panel-success">
        <div class="panel-heading"><b>Security Setting for {{$userName->first_name}} {{$userName->last_name}}</b></div>
        <div class="panel-body">
            @if ($roleUser === null)
                <div class="container-fluid">

                    <div class="row">
                        {!! Form::open(array('url' => '/security/'. $id, 'method' => 'GET', 'files' => true,'class' => 'form_control')) !!}
                        {!! Form::hidden('user_id', $id) !!}
                        <div class="input-group">
                            {!! Form::select('role_id', $roles, null, ['class' => 'form-control']) !!}
                            <span class="input-group-btn">
                {!! Form::submit('Set Security', ['class'=>'btn btn-success']) !!}
                 </span>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            @endif
            <br>

            <table class="table table-hover table-responsive table-bordered table-striped" id="table">
                <thead>
                <tr class="success">
                    <td colspan="2"><b>Current Role</b></td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    @if ($roleUser === null)
                        <td>No Role Assigned - User has no access.</td>
                    @else
                        <td>{{$roleUser->roles->name}}</td>
                        <td align="center" style="vertical-align:middle"><a href="/user/del/{{$roleUser->role_id}}" role="button" class="btn btn-danger btn-xs" onclick="return ConfirmDelete()"><span class="fa fa-btn fa-trash" aria-hidden="true"></span> Delete</a></td>
                    @endif

                </tr>
                </tbody>
            </table>

        </div>
    </div>

@endsection
