            <div class="form-group">
                {!! Form::label('username', 'SGMC Username:') !!}
                {!! Form::text('username', null, ['class' => 'form-control']) !!}
            </div>

            @if (Auth::user()->access == 'Admin')
                <div class="form-group">
                    {!! Form::label('access', 'Status:') !!}
                    {!! Form::select('access', [
                                        'Active' => 'Active',
                                        'SGMC' => 'SGMC',
                                        'Admin' => 'Admin',
                                        'Medco' => 'Medco',
                                        'Inactive' => 'Inactive',
                                         ], null, ['class' => 'form-control']) !!}
                </div>
            <div class="form-group">
                {!! Form::label('emp_status', 'Employee Status:') !!}
                {!! Form::select('access', [
                                    'Active' => 'Active',
                                    'Terminated' => 'Terminated',
                                     ], null, ['class' => 'form-control']) !!}
            </div>
                @endif

