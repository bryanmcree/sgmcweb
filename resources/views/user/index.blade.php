@extends('layouts.app')

@section('content')


    <div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading panel-heading-custom"><i class="fa fa-question-circle"  data-toggle="popover" data-placement="bottom"
                                      title="Authorized Users"
                                      data-content="Searchable list of all users and security permissions."></i> -><b>Authorized Users ({{$users->count()}})</b></div>
        <div class="panel-body">
    @if ($users->isEmpty())
        <div class="alert alert-info" role="alert">No current employees match your search requirements.</div>
    @else
    <div class="container-fluid">
                    <div class="row">
                        <div class="input-group">
                            {!! Form::text('search', null,array('class'=>'form-control','id'=>'myInputTextField','placeholder'=>'Search for system...', 'autofocus'=>'autofocus')) !!}
                            <span class="input-group-btn">
                <a href = "/user/create" class = "btn btn-success" role = "button">Add User</a>
                </span>
                        </div>
                    </div>
    </div>
            <BR>
        <table class="table table-hover table-responsive table-bordered" id="users">
            <thead>
            <tr>
                <td><b>Photo</b></td>
                <td><b>Name</b></td>
                <td class="hidden-xs hidden-sm hidden-md "><b>Emp#</b></td>
                <td class="hidden-xs hidden-sm hidden-md"><b>SGMC User</b></td>

                <td class="hidden-xs hidden-sm hidden-md "><b>Status</b></td>
                <td class="hidden-xs"><b>Last Login</b></td>
                <td class="hidden-xs"><b>Last Login IP</b></td>
                <td class="hidden-xs hidden-sm"><b>Email</b></td>
                <td class='no-sort' align="center"></td>

            </tr>
            </thead>

            <tbody>
            @foreach ($users as $user)

                <tr>
                    <td style="vertical-align:middle; text-align:center;">@if($user->photo != '')<img src="data:image/jpeg;base64,{{str_replace('</photo>','',str_replace('<photo>','',$user->photo))}}" height="60" width="60"/>@else {{ Html::image('img/nopic.png', 'alt', array( 'width' => 60, 'height' => 60 )) }} @endif</td>
                    <td style="vertical-align:middle"><b>{{$user->last_name}}, {{$user->first_name}}</b><BR><i>{{$user->title}}</i></td>
                    <td style="vertical-align:middle" class="hidden-xs hidden-sm hidden-md ">{{$user->employee_number}}</td>
                    <td style="vertical-align:middle" class="hidden-sm hidden-md hidden-xs">{{$user->username}}</td>

                    <td style="vertical-align:middle" class="hidden-xs hidden-sm hidden-md ">{{$user->emp_status}}</td>
                    <td style="vertical-align:middle" class="hidden-xs hidden-sm">{{$user->last_login}}</td>
                    <td style="vertical-align:middle" class="hidden-xs hidden-sm">{{$user->last_login_ip}}</td>
                    <td style="vertical-align:middle" class="hidden-xs hidden-sm">{{$user->mail}}</td>
                    <td style="vertical-align:middle" align="center" >
                        @if ( Auth::user()->hasRole('Admin'))
                        <a href="#" class="SecurityRoles" data-user_id = {{$user->id}}><span class="fa fa-btn fa-lock" aria-hidden="true"></span></a>
                        <a href="/user/{{$user->id}}/edit" class=""><span class="fa fa-btn fa-pencil" aria-hidden="true"></span></a>
                        @endif
                        <a href="#" class="LongDistance" data-user_id = {{$user->id}} data-ldac = {{$user->ldac}}><span class="fa fa-btn fa-phone" aria-hidden="true"></span></a>
                    </td>
                </tr>

            @endforeach
            </tbody>
        </table>



    @endif

        </div>
    </div>
</div>
@endsection

@include('user.modals.security')
@include('user.modals.longdistance')

@section('scripts')

    <script type="text/javascript">



        $(document).ready(function() {
            oTable = $('#users').DataTable(
                    {
                        "info":     false,
                        "pageLength": 10,
                        "lengthChange": false,
                        "order": [],
                        "columnDefs": [
                            { targets: 'no-sort', orderable: false }
                        ],
                        "dom": '<l<t>ip>'

                    }
            );
            $('#myInputTextField').keyup(function(){
                oTable.search($(this).val()).draw() ;
            })

            $(document).on("click", ".SecurityRoles", function () {
                //alert($(this).attr("data-user_id"));
                $('.user_id').val($(this).attr("data-user_id"));
                $('#Roles').modal('show');
            });


            $(document).on("click", ".LongDistance", function () {
                //alert($(this).attr("data-user_id"));
                $('.id').val($(this).attr("data-user_id"));
                $('.ldac').val($(this).attr("data-ldac"));
                $('#LongDistance').modal('show');
            });

        });
    </script>
@endsection
