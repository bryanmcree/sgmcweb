<div class="modal fade Roles" id="Roles" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">
                <button type="button" class="close btn-sgmc" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-user-secret" aria-hidden="true"></i> <b>Security Roles</b></h4>
            </div>
            <div class="modal-body">
                {!! Form::open(array('action' => ['UserController@newRoles'], 'class' => 'form_control')) !!}
                {!! Form::hidden('user_id', null,['class'=>'user_id']) !!}
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <table class="table table-hover table-responsive table-bordered">
                                    <thead>
                                    <tr>
                                        <td><B>Role Name</B></td>
                                        <td><b>Description</b></td>
                                        <td></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($roles as $role)
                                       <tr>
                                           <td>{{$role->name}}</td>
                                           <td>{{$role->label}}</td>
                                           <td align="center"><input type="checkbox" name="{{ $role->id }}[answer]" value="{{$role->id}}"></td>
                                       </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="RightLeft">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    {!! Form::submit('Add Roles', ['class'=>'btn btn-sgmc', 'id'=>'AddButton']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>