<div class="modal fade " id="LongDistance" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">
                <button type="button" class="close btn-sgmc" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-user-secret" aria-hidden="true"></i> <b>Long Distance Access Code</b></h4>
            </div>
            <div class="modal-body">
                {!! Form::open(array('action' => ['UserController@updateLD'], 'class' => 'form_control')) !!}
                {!! Form::hidden('id', null,['class'=>'id']) !!}
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="form-group">
                                    {!! Form::label('ldac', 'Long Distance Access Code:',['class'=>'ldac']) !!}
                                    {!! Form::text('ldac', null, ['class' => 'form-control ldac', 'id'=>'ldac']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="RightLeft">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    {!! Form::submit('Add LDAC', ['class'=>'btn btn-sgmc', 'id'=>'AddButton']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>