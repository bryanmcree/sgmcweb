
@extends('layouts.app')

@section('content')

<div class="card bg-secondary text-white mb-3" style="border-radius:0px;">
    <div class="card-header">
        Aggregate By Month
        <span class="float-right">
            <a href="/audit/report/print/{{ $audit->id }}" class="btn btn-sm btn-info" title="Print View" target="_blank"><i class="fas fa-print"></i></a>
            <a href="/audit/report/excel/{{ $audit->id }}" class="btn btn-sm btn-success" title="Export to Excel"><i class="far fa-file-excel"></i></a>
            <a href="/audit/details/{{ $audit->id }}" class="btn btn-dark btn-sm" title="Return to Detail"><i class="fas fa-home"></i></a>
        </span>
    </div>
    <div class="card-body">

        <div class="table-responsive">
            <table class="table table-hover table-striped table-dark" id="aggReport">
                <thead>
                    <th>Question</th>
                    <th>Standard</th>
                    <th>EP #</th>
                    <th>Month/Year</th>
                    <th>Compliance</th>
                    <th>%</th>
                </thead>
                <tbody>
                    @foreach($aggregates as $agg)
                        @if(!is_null($agg->question_title))
                            <tr>
                                
                                <td>{{ $agg->question_title->title }}</td>
                                <td>
                                    @foreach ($standards->where('question_id', $agg->question_id) as $standard)
                                    <small class="text-warning"> {{ $standard->standard }} </small>
                                    <br>
                                    @endforeach
                                </td>
                                <td>
                                    @foreach ($eps->where('question_id', $agg->question_id) as $ep)
                                        <small class="text-warning"> ({{ $ep->EP }}) </small>
                                    @endforeach
                                </td>
                                <td>{{ $agg->submitted_month }}/{{ $agg->submitted_year }} </td>
                                <td>{{ $agg->num_of_yes }}/{{ $agg->total }}</td>
                                <td @if(($agg->num_of_yes / $agg->total) * 100 <= 75) class="text-danger" @elseif(($agg->num_of_yes / $agg->total) * 100 <= 90) class="text-warning" @else class="text-success" @endif>{{ number_format(($agg->num_of_yes / $agg->total) * 100, 2) }}%</td>
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
</div>

@if($hasShort > 0)

<div class="card bg-secondary text-white mb-3" style="border-radius:0px;">
    <div class="card-header">
        Short Answer Results
        <span class="float-right">
            <a href="/audit/shortAnswer/excel/{{ $audit->id }}" class="btn btn-sm btn-success" title="Export to Excel"><i class="far fa-file-excel"></i> Export Short Answers</a>
        </span>
    </div>
    <div class="card-body">

        <div class="table-responsive">
            <table class="table table-hover table-striped table-dark" id="shortReport">
                <thead>
                    <th>Question</th>
                    <th>Month/Year</th>
                    <th>Number of Submissions</th>
                    <th>Details</th>
                </thead>
                <tbody>
                    @foreach($shortAnswer as $short)
                        <tr>
                            <td>@if(!empty($short->question_title)) {{ $short->question_title->title }} @endif</td>
                            <td>{{ $short->submitted_month }}/{{ $short->submitted_year }} </td>
                            <td>{{ $short->total }}</td>
                            <td><a href="/audit/short/{{ $audit->id }}/{{ $short->question_id }}" class="btn btn-primary btn-sm" target="_blank">View Results</a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
</div>

@endif

<div class="card bg-secondary text-white mb-3" style="border-radius:0px;">
    <div class="card-header">
        By Month, By Cost Center
    </div>
    <div class="card-body">

        <div class="table-responsive">
            <table class="table table-hover table-striped table-dark" id="auditReport">
                <thead>
                    <th>Question</th>
                    <th>Month/Year</th>
                    <th>Cost Center</th>
                    <th>Compliance</th>
                    <th>%</th>
                </thead>
                <tbody>
                    @foreach($num_denom as $nd)
                        @if(!is_null($nd->question_title))
                            <tr>
                                <td>{{ $nd->question_title->title }}</td>
                                <td>{{ $nd->submitted_month }}/{{ $nd->submitted_year }} </td>
                                <td>{{ $nd->cost_center }}</td>
                                <td>{{ $nd->num_of_yes }}/{{ $nd->total }}</td>
                                <td @if(($nd->num_of_yes / $nd->total) * 100 <= 75) class="text-danger" @elseif(($nd->num_of_yes / $nd->total) * 100 <= 90) class="text-warning" @else class="text-success" @endif>{{ number_format(($nd->num_of_yes / $nd->total) * 100, 2) }}%</td>
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
</div>

<hr class="bg-light">

{{-- <div class="card-deck">

    @foreach($ccs as $cc)

        <div class="col-lg-6 mx-auto p-0">
            <div class="card bg-dark text-white mb-3 border-dark" style="border-radius:0px;">
                <div class="card-header">
                    Cost Center: {{ $cc->cost_center }}
                </div>
                <div class="card-body bg-secondary">

                    @foreach($qs as $q)
                        
                        <div class="card bg-dark text-white mb-3" style="border-radius:0px;">
                            <div class="card-header bg-dark">
                                Question: {{ $q->question_title->title }}
                            </div>
                            <div class="card-body">

                                <canvas id="{{$q->question_id}}_{{ $cc->cost_center }}" height="50"></canvas>
                                <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.js"></script>
                                <script type="application/javascript">

                                    Chart.defaults.global.defaultFontColor = 'white';

                                    new Chart(document.getElementById('{{$q->question_id}}_{{ $cc->cost_center }}'), {
                                        type: 'bar',

                                        data: {
                                            labels: [
                                                @foreach($yesnoNA->where('cost_center', $cc->cost_center)->where('question_id', $q->question_id) as $answer)
                                                    @if($answer->answer == 1)
                                                        "Yes",
                                                    @elseif($answer->answer == 0)
                                                        "No",
                                                    @else
                                                        "N/A",
                                                    @endif
                                                @endforeach
                                            ],
                                            datasets: [{
                                                label: "",
                                                backgroundColor: [
                                                    @foreach($yesnoNA->where('cost_center', $cc->cost_center)->where('question_id', $q->question_id) as $answer)
                                                        @if($answer->answer == 1)
                                                            'rgba(0, 99, 132, 0.6)',
                                                        @elseif($answer->answer == 0)
                                                            'rgba(0, 221, 237, 0.6)',
                                                        @else
                                                            'rgba(0, 235, 109, 0.6)',
                                                        @endif
                                                    @endforeach
                                                ],
                                                data: [

                                                    @foreach($yesnoNA->where('cost_center', $cc->cost_center)->where('question_id', $q->question_id) as $answer)
                                                        {{$answer->total}},
                                                    @endforeach
                                                ]
                                            }]
                                        },
                                        options: {
                                            legend: { display: false },
                                            title: {
                                                //display: true,
                                                //text: 'Predicted world population (millions) in 2050'
                                            },
                                            scales: {
                                                yAxes: [{
                                                    stacked:false,
                                                ticks: {
                                                    beginAtZero: true
                                                    }
                                                }]
                                            }
                                        }
                                    });
                                </script>
                        
                            </div>
                        </div>

                    @endforeach
                    

                </div>
            </div>
        </div>

    @endforeach 

</div> <!-- Card Deck --> --}}


@endsection


@section('scripts')

<script>
    $(document).ready(function() {
        $('#auditReport').DataTable( {
            "pageLength": 15,
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        });
    });

    $(document).ready(function() {
        $('#aggReport').DataTable( {
            "pageLength": 50,
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        });
    });

    $(document).ready(function() {
        $('#shortReport').DataTable( {
            "pageLength": 50,
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        });
    });
</script>
    
@endsection