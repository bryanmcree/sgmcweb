{{--New file Template--}}

{{--Add Security for this page below--}}

@extends('layouts.app')
{{--Page Design Goes Below--}}
@section('content')

<div class="card text-white bg-dark mb-3">
    <div class="card-header">
        <h4>Auditing Tool</h4>
    </div>
    <div class="card-body">

        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab">Dashboard</a>
            </li>
            @if(Auth::user()->hasRole('Survey Admin'))
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#mysurvey" role="tab">My Surveys</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="build-tab" data-toggle="tab" href="#create" role="tab">Create Survey</a>
                </li>
            @endif
        </ul>

        <div class="tab-content" id="myTabContent">

            <div class="tab-pane fade show active" id="home" role="tabpanel">
                <div class="card mb-3 mt-3 text-white bg-secondary">
                    <div class="card-header">
                        <h3>All Surveys</h3>
                        <p>Shared surveys available to take.</p>
                    </div>
                    <div class="card-body">
                        <table class="table table-condensed table-hover table-dark" width="100%" id="otherSurveys">
                            <thead>
                                <th>Survey Name</th>
                                <th class="d-none d-sm-table-cell">Taken</th>
                                <th class="d-none d-sm-table-cell">Created By</th>
                                <th class="d-none d-sm-table-cell">Created At</th>
                                <th class="no-sort text-right">Actions</th>
                            </thead>
                            <tbody>
                            @foreach($surveys as $survey)
                                <tr>
                                    <td>{{$survey->name}}</td>
                                    <td class="d-none d-sm-table-cell">{{ $survey->taken_count }}</td>
                                    <td class="d-none d-sm-table-cell">{{$survey->createdBy->name}}</td>
                                    <td class="d-none d-sm-table-cell">{{$survey->created_at}}</td>
                                    <td align="right">
                                        <div class="btn-group" role="group" aria-label="Basic example">
                                            <a href="/audit/details/{{$survey->id}}" title="Survey Details" class="btn btn-sm btn-info"><i class="fas fa-edit" ></i></a>
                                            <a href="/audit/answers/create/{{$survey->id}}" title="Take Survey" target="_blank" class="btn btn-sm btn-primary @if($survey->active == 0) disabled @endif"><i class="fas fa-clipboard-check"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> <!-- all survey -->


            <div class="tab-pane fade" id="mysurvey" role="tabpanel" aria-labelledby="profile-tab">
                <div class="card mb-3 mt-3 text-white bg-secondary">
                    <div class="card-header"> 
                        <h3>My Surveys ({{ count($mySurveys) }})</h3>
                        <p>All current users surveys</p>
                    </div>
                    <div class="card-body">
                        <table class="table table-condensed table-hover table-dark" width="100%" id="mySurveys">
                            <thead>
                            <tr>
                                <td><b>Survey Name</b></td>
                                <td class="d-none d-sm-table-cell"><b>Visibility</b></td>
                                <td class="d-none d-sm-table-cell"><b>Taken</b></td>
                                <td class="d-none d-sm-table-cell"><b>Active</b></td>
                                <td class="d-none d-sm-table-cell"><b>Created By</b></td>
                                <td class="d-none d-sm-table-cell"><b>Created At</b></td>
                                <td class="no-sort" align="right"><b>Actions</b></td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($mySurveys as $mySurvey)
                                <tr>
                                    <td>{{$mySurvey->name}}</td>
                                    <td class="d-none d-sm-table-cell">@if($mySurvey->visible ==0) <i class="fas fa-user" style="color:red;"></i>  @else <i class="fas fa-users" style="color:green;"></i> @endif</td>
                                    <td class="d-none d-sm-table-cell">{{ $mySurvey->taken_count }}</td>
                                    <td class="d-none d-sm-table-cell">@if($mySurvey->active ==0) <i class="far fa-times-circle" style="color:red;"></i>  @else <i class="far fa-check-circle" style="color:green;"></i> @endif</td>
                                    <td class="d-none d-sm-table-cell">{{$mySurvey->createdBy->name}}</td>
                                    <td class="d-none d-sm-table-cell">{{$mySurvey->created_at}}</td>
                                    <td align="right">
                                        <div class="btn-group" role="group" aria-label="Basic example">
                                            <a href="/audit/details/{{$mySurvey->id}}" class="btn btn-sm btn-info d-none d-sm-block"><i class="fas fa-edit"></i></a>
                                            <a href="/audit/answers/create/{{$mySurvey->id}}" title="Take Survey" target="_blank" class="btn btn-sm btn-primary @if($mySurvey->active == 0) disabled @endif"><i class="fas fa-clipboard-check"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> <!-- my survey -->

            <div class="tab-pane fade" id="create" role="tabpanel" aria-labelledby="build-tab">

                <div class="card mb-3 mt-3 text-white bg-secondary" style="width: 40%">
                    <div class="card-header">
                        <h3>Create Survey</h3>
                    </div>
                    <div class="card-body">
                        <form method="post" action="/audit/store">
                            {{ csrf_field() }}
                            <input type="hidden" name="created_by" value="{{ Auth::user()->employee_number }}">
                            <input type="hidden" name="active" value="0">

                            <div class="form-group">
                                <label for="survey_name_id">Survey Name:</label>
                                <input type="text" class="form-control" name ='name' required>
                            </div>

                            <div class="form-group">
                                <label for="survey_description_id">Survey Description:</label>
                                <textarea class="form-control" name="description"  required></textarea>
                            </div>

                            <div class="form-group">
                                <label for="visibility_id">Who do you want to see this survey?</label>
                                <select name="visible" class="form-control" required>
                                    <option value="" selected>[Select Visibility]</option>
                                    <option value="0">Only You</option>
                                    <option value="1">Everyone in your Organization</option>
                                </select>
                            </div>

                            <input type="submit" class="btn btn-primary" value="Save and Continue">

                        </form>
                    </div>
                </div>

            </div> <!-- create -->

        </div>
    </div>
</div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">
    $(document).ready(function() {
        $('#mySurveys').DataTable( {
            "pageLength": 15,
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );

    $(document).ready(function() {
        $('#otherSurveys').DataTable( {
            "pageLength": 25,
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );


    //Delete Question
    $(document).on("click", ".DeleteSurvey", function () {
        //alert($(this).attr("data-cost_center"));
        var survey_id = $(this).attr("data-survey_id");
        DeleteMonthValues(survey_id);
    });

    function DeleteMonthValues(survey_id) {
        swal({
            title: "Delete Survey?",
            text: "Deleting this survey will delete all the questions and answers.",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            confirmButtonText: "Yes, delete it!",
            confirmButtonColor: "#ec6c62"
        }, function() {
            $.ajax({
                url: "/survey/deletesurvey/" + survey_id ,
                type: "GET"
            })
                .done(function(data) {
                    swal({
                            title: "Deleted",
                            text: "Question Deleted",
                            type: "success",
                            timer: 1800,
                            showConfirmButton: false

                        }
                    );
                    setTimeout(function(){window.location.replace('/survey')},1900);
                })
                .error(function(data) {
                    swal("Oops", "We couldn't connect to the server!", "error");
                });
        });
    }



</script>

@endsection
