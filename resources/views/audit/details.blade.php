{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Page Design Goes Below--}}

{{-- Select 2 --}}
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

@section('content')

<div style="width:80%" class="mx-auto">

<div class="card bg-dark">
    <div class="card-header">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Survey Dashboard</a>
            </li>
            @if( Auth::user()->hasRole('Survey Admin') == TRUE)
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Questions</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Survey Options</a>
                </li>
            @endif
            <li class="nav-item">
                <a class="nav-link" id="take-tab" data-toggle="tab" href="#take" role="tab" aria-controls="take" aria-selected="false">Take this Survey</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/audit/report/{{ $audit->id }}" >Report</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="contact-tab"  href="/audit" role="tab" aria-selected="false">Back to Surveys</a>
            </li>
        </ul>
    </div>
</div>

@if($audit->active ==0)
    <br>
    <h4 class="alert alert-danger">This survey is NOT ACTIVE.  Click survey options to turn on survey.</h4>
@endif

<div class="tab-content" id="myTabContent">

    <div class="tab-pane fade show active " id="home" role="tabpanel" aria-labelledby="home-tab">
        <div class="card-deck mt-4">
            <div class="card mb-3 text-white bg-dark">
                <div class="card-body">
                    <b>Total Questions:</b> {{$questions->count()}}
                </div>
            </div>
            <div class="card mb-3 text-white bg-dark">
                <div class="card-body">
                    <b>Submissions:</b> {{$audit->taken_count}}
                </div>
            </div>
        </div>
        <div class="card mb-3 text-white bg-dark">
            @if($audit->visibility != 1)
                <div class="card-body">
                    <p>This survey is <B>PRIVATE</B>.  Below is a link to share with users so they can take the survey.  Cut and paste this into an email to distribute this survey.</p>
                    <div class="form-group">
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="1">http://web.sgmc.org/audit/answers/create/{{$audit->id}}</textarea>
                    </div>
                    <button class="btn btn-primary" onclick="myFunction()">Copy Link Above to Clipboard</button>
                </div>
            @else
                <div class="card-body">
                    <p>This survey is public.  Everyone will have the ability to participate in this survey.  If you would like to email a link to this survey you will find it below.</p>
                    <div class="form-group">
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="1">http://web.sgmc.org/audit/answers/create/{{$audit->id}}</textarea>
                    </div>
                    <button class="btn btn-primary" onclick="myFunction()">Copy Link Above to Clipboard</button>
                </div>
            @endif
        </div>

        @foreach($questions as $question)
        
            <div class="card bg-secondary text-white mb-3">
                <div class="card-header bg-dark">
                    {{ $question->title }}
                </div>
                <div class="card-body">

                    @if($question->question_type == 'YesNoNA')
                        <canvas id="{{$question->id}}" height="50"></canvas>
                        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.js"></script>
                        <script type="application/javascript">

                            Chart.defaults.global.defaultFontColor = 'white';

                            new Chart(document.getElementById('{{$question->id}}'), {
                                type: 'bar',

                                data: {
                                    labels: [
                                        @foreach($yesnoNA->where('question_id', $question->id) as $answer)
                                            @if($answer->answer == 1)
                                                "Yes",
                                            @elseif($answer->answer == 0)
                                                "No",
                                            @else
                                                "N/A",
                                            @endif
                                        @endforeach
                                    ],
                                    datasets: [{
                                        label: "",
                                        backgroundColor: [
                                            @foreach($yesnoNA->where('question_id', $question->id) as $answer)
                                                @if($answer->answer == 1)
                                                    'rgba(0, 99, 132, 0.6)',
                                                @elseif($answer->answer == 0)
                                                    'rgba(124, 86, 188, 0.6)',
                                                @else
                                                    'rgba(221, 90, 215, 0.6)',
                                                @endif
                                            @endforeach
                                        ],
                                        data: [

                                            @foreach($yesnoNA->where('question_id', $question->id) as $answer)
                                                {{$answer->total}},
                                            @endforeach
                                        ]
                                    }]
                                },
                                options: {
                                    legend: { display: false },
                                    title: {
                                        //display: true,
                                        //text: 'Predicted world population (millions) in 2050'
                                    },
                                    scales: {
                                        yAxes: [{
                                            stacked:false,
                                        ticks: {
                                            beginAtZero: true
                                            }
                                        }]
                                    }
                                }
                            });
                        </script>
                    @else
                        <div class="jumbotron bg-dark mb-2">
                            <h4>Short Answer Results</h4>
                        </div>
                        <a href="/audit/short/{{ $audit->id }}/{{ $question->id }}" class="btn btn-primary float-right">View Results</a>
                    @endif

                </div>
            </div>

        @endforeach

    </div> <!-- HOME -->

    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
        <div class="card mt-4 mb-4 card-block text-white bg-dark">
            <div class="card-header">
                <h3>Questions</h3>
            </div>

            <div class="card-body">
                <form method="post" action="/audit/question/store" style="width:60%">
                    {{ csrf_field() }}
                    <input type="hidden" value="{{ Auth::user()->employee_number }}" name="created_by">
                    <input type="hidden" value="{{$audit->id}}" name="survey_id">

                    <div class="form-group">
                        <label>Question Type</label>
                        <select class="form-control" name="question_type" required>
                            <option value="" selected>[Select Question Type]</option>
                            <option value="YesNoNA">Yes/No/NA</option>
                            <option value="short_answer">Short Answer</option>
                        </select>
                    </div>
                    
                    <div class="form-group">
                        <label>Answer Required</label>
                        <select class="form-control" name="required" required>
                            <option value="1" selected>Yes</option>
                            <option value="0">No</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Question Requester</label>
                        <select class="form-control" name="requester" required>
                            <option value="SGMC" selected>SGMC</option>
                            <option value="Joint Commission">Joint Commission</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Question</label>
                        <input type="text" name="title" class="form-control" required>
                    </div>

                    <div class="form-row">

                        <div class="form-group col-6">
                            <label for="">Select ALL Standards Associated With This Question</label>
                            <select name="Standard[]" class="multi-2" style="width:100%" multiple="multiple">
                                @foreach($standards as $standard)
                                    <option value="{{ $standard->standard }}">{{ $standard->standard }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-6">
                            <label for="">Select ALL EP #'s Associated With This Question</label>
                            <select name="EP[]" class="multi-2" style="width:100%" multiple="multiple">
                                @foreach($eps as $ep)
                                    <option value="{{ $ep->EP }}">{{ $ep->EP }}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>

                    <hr>

                    <input type="Submit" class="btn btn-primary btn-block" value="Submit">

                </form>
                
            </div>

            @if($questions->isEmpty())
                <div class="card-body">
                    <div class="alert alert-info fa-lg mt-3"><i class="far fa-plus-square"></i> You don't have any questions for this survey! Let's fix that.</div>
                </div>
                
            @else
                <div class="card-body">
                    <p>Drag rows to reorder questions.</p>

                    <table class="table table-hover text-white bg-dark">
                        <thead>
                        <tr><td></td>
                            <td><b>Question</b></td>
                            <td align="center"><b>Required</b></td>
                            <td><b>Type</b></td>
                            <td><b>Requester</b></td>
                            <td></td>
                        </tr>
                        </thead>
                        <tbody id="tablecontents">
                        <?php $rowCount = 0 ?>
                        @foreach($questions as $question)
                            <?php $rowCount = $rowCount +1 ?>
                            <tr class="row1" data-id="{{ $question->id }}">
                                <td style="vertical-align : middle;"><b>{{$rowCount}}.</b></td>
                                <td style="vertical-align : middle;">{{$question->title}}</td>
                                <td style="vertical-align : middle;" align="center">@if($question->required == 1)<i class="fas fa-check" style="color:green"></i>@else <i class="fas fa-times" style="color:red"></i> @endif</td>
                                <td nowrap style="vertical-align : middle;">{{$question->question_type}}</td>
                                <td nowrap style="vertical-align : middle;">{{ $question->requester }}</td>
                                <td nowrap style="vertical-align : middle;" align="right">

                                    <a href="/audit/question/edit/{{$audit->id}}/{{$question->id}}" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></a>

                                    <a href="#" class="btn btn-danger btn-sm deleteQuestion" title="Delete Question and All Answers" question_id="{{$question->id}}">
                                        <i class="fas fa-trash-alt"></i>
                                    </a>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @endif
        </div>
    </div> <!-- PROFILE -->
        
        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
            <div class="card bg-dark text-white mt-4 mb-3">
                <div class="card-header">
                    <h3>Survey Options</h3>
                </div>
                <form method="POST" action="/audit/update/{{ $audit->id }}">
                    {{ csrf_field() }}
                    <div class="card-body">

                        <p>Do you want this survey to be on or off? Turning this off will de-activate this survey where others cannot take it.</p>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="active" value="1" @if($audit->active == 1) checked @endif>
                            <label class="form-check-label">
                                On
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="active" value="0" @if($audit->active != 1) checked @endif>
                            <label class="form-check-label">
                                Off
                            </label>
                        </div>

                        <p class="mt-3">Survey visibility.</p>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="visible" value="1" @if($audit->visible == 1) checked @endif>
                            <label class="form-check-label">
                                Public - Everyone will see this survey in their "Other Surveys" tab.
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="visible"" value="0" @if($audit->visible != 1) checked @endif>
                            <label class="form-check-label">
                                Private - Only users with the link can take the survey. (Does not prevent users from sharing the link.)
                            </label>
                        </div>

                        
                        <input type="submit" class="btn btn-primary btn-block mt-3" value="Update Survey">

                    </div>
                </form>
            </div>
        </div> <!-- CONTACT -->

        <div class="tab-pane fade mb-3" id="take" role="tabpanel" aria-labelledby="take-tab">
            @if($audit->active == 1)
                @include('audit.includes.take_survey')
            @else
                <div class="alert alert-danger"><h4>This survey is not active and no longer available for data submission.</h4></div>
            @endif
        </div> <!-- TAKE -->

        
        
    </div>

</div>

@include('audit.includes.report')

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script>
    $(document).ready(function() {
        $(".multi-2").select2();
    });
</script>

<script type="application/javascript">

function myFunction() {
    /* Get the text field */
    var copyText = document.getElementById("exampleFormControlTextarea1");

    /* Select the text field */
    copyText.select();

    /* Copy the text inside the text field */
    document.execCommand("copy");

    /* Alert the copied text */
    //alert("Copied the text: " + copyText.value);
}


$(function () {
    $("#table").DataTable();

    $( "#tablecontents" ).sortable({
        items: "tr",
        cursor: 'move',
        opacity: 0.6,
        update: function() {
            sendOrderToServer();
        }
    });

    function sendOrderToServer() {

        var order = [];
        $('tr.row1').each(function(index,element) {
            order.push({
                id: $(this).attr('data-id'),
                position: index+1
            });
        });
        //alert(order);

        $.ajax({
            type: "POST",
            dataType: "json",
            url: "{{ url('/audit/order') }}",
            data: {
                order:order,
                _token: '{{csrf_token()}}',
                survey_id:'{{$audit->id}}'
            },
            success: function(response) {
                if (response.status == "success") {
                    console.log(response);
                } else {
                    console.log(response);
                }
            }
        });

    }
});


$(document).on("click", ".EditQuestion", function () {
        //alert('Edit Server Button Clicked');
        $('.question_id').val($(this).attr("data-question_id"));
        $('.survey_id').val($(this).attr("data-survey_id"));
        $('.question_type').val($(this).attr("data-question_type"));
        var test = $(this).attr("data-question_type");
        $('.required').val($(this).attr("data-required"));
        $('.title').val($(this).attr("data-title"));
        $('.notes').val($(this).attr("data-question_notes"));
        $('.dropdown').val($(this).attr("data-dropdown_choice[]"));

        $('#edit-question').modal('show');
        //alert($(this).attr("data-id"));
    });


//Delete Question
$(document).on("click", ".deleteQuestion", function () {
        //alert($(this).attr("data-cost_center"));
        var question_id = $(this).attr("question_id");
        DeleteQ(question_id);
    });

    function DeleteQ(question_id) {
        swal({
            title: "Delete Question?",
            text: "This can be undone.",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            confirmButtonText: "Yes, deactivate it!",
            confirmButtonColor: "#ec6c62"
        }, function() {
            $.ajax({
                url: "/audit/question/destroy/" + question_id,
                type: "GET"
            })
                .done(function(data) {
                    swal({
                            title: "Deleted",
                            text: "Question Deactivated",
                            type: "success",
                            timer: 1800,
                            showConfirmButton: false

                        },function() {
                            location.reload();
                        });
                })
                .error(function(data) {
                    swal("Oops", "We couldn't connect to the server!", "error");
                });
        });
    }

</script>


        


@endsection
