
@extends('layouts.app')

@section('content')

<div class="card mb-3" style="border-radius:0px;">
    <div class="card-header">
        Aggregate By Month
    </div>
    <div class="card-body">

        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <th>Question</th>
                    <th>Standard</th>
                    <th>EP #</th>
                    <th>Month/Year</th>
                    <th>Compliance</th>
                    <th>%</th>
                </thead>
                <tbody>
                    @foreach($aggregates as $agg)
                        <tr>
                            <td>{{ $agg->question_title->title }}</td>
                            <td>
                                @foreach ($standards->where('question_id', $agg->question_id) as $standard)
                                   <small> {{ $standard->standard }} </small>
                                   <br>
                                @endforeach
                            </td>
                            <td>
                                @foreach ($eps->where('question_id', $agg->question_id) as $ep)
                                    <small> ({{ $ep->EP }}) </small>
                                @endforeach
                            </td>
                            <td>{{ $agg->submitted_month }}/{{ $agg->submitted_year }} </td>
                            <td>{{ $agg->num_of_yes }}/{{ $agg->total }}</td>
                            <td @if(($agg->num_of_yes / $agg->total) * 100 <= 75) class="text-danger" @elseif(($agg->num_of_yes / $agg->total) * 100 <= 90) class="text-warning" @else class="text-success" @endif>{{ number_format(($agg->num_of_yes / $agg->total) * 100, 2) }}%</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
</div>

<div class="card mb-3" style="border-radius:0px;">
    <div class="card-header">
        By Month, By Cost Center
    </div>
    <div class="card-body">

        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <th>Question</th>
                    <th>Month/Year</th>
                    <th>Cost Center</th>
                    <th>Compliance</th>
                    <th>%</th>
                </thead>
                <tbody>
                    @foreach($num_denom as $nd)
                        <tr>
                            <td>{{ $nd->question_title->title }}</td>
                            <td>{{ $nd->submitted_month }}/{{ $nd->submitted_year }} </td>
                            <td>{{ $nd->cost_center }}</td>
                            <td>{{ $nd->num_of_yes }}/{{ $nd->total }}</td>
                            <td @if(($nd->num_of_yes / $nd->total) * 100 <= 75) class="text-danger" @elseif(($nd->num_of_yes / $nd->total) * 100 <= 90) class="text-warning" @else class="text-success" @endif>{{ number_format(($nd->num_of_yes / $nd->total) * 100, 2) }}%</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
</div>


@endsection


@section('scripts')


    
@endsection