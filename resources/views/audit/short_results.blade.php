{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Page Design Goes Below--}}
@section('content')

<div class="card bg-dark text-white mb-3">
    <div class="card-header bg-secondary">
        {{ $question->title }}
        <span class="float-right">
            <a href="/audit/details/{{ $audit->id }}" class="btn btn-sm btn-primary">Return to Dashboard</a>
        </span>
    </div>
    <div class="card-body">
        <table class="table table-condensed table-striped table-hover table-dark" id="shortAnswer">
            <thead>
                <th nowrap>Submitted By</th>
                <th nowrap>CostCenter</th>
                <th nowrap>Created At</th>
                <th>Answer</th>
            </thead>
            <tbody>
            @foreach($answers as $result)
                <tr>
                    <td nowrap>{{ $result->submittedBy->name or '' }}</td>
                    <td nowrap>{{ $result->cost_center }}</td>
                    <td nowrap>{{ Carbon::parse($result->created_at)->format('M jS, Y') }}</td>
                    <td>{{ $result->answer_string }}</td>
                </tr>
            @endforeach
            </tbody>
        </table> 
    </div>
</div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

<script>
    $(document).ready(function() {
        $('#shortAnswer').DataTable({
            "pageLength": 30,
            "autoWidth": true,
            "ordering": false,
            "columnDefs": [
                { targets: 'no-sort', orderable: false }
            ],
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                if ( aData[3] == "" )
                {
                    $('td', nRow).css('background-color', '#b53838');
                    $('td:eq(3)', nRow).html('<b>NO RESPONSE</b>');
                    $('#printbtn', nRow).css('display', 'none');
                }
            }
        });
    });
</script>

@endsection