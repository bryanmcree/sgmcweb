
@extends('layouts.app')

@section('content')

<div class="card bg-dark text-white mb-3">
    <div class="card-header">
        {{ $audit->name }}
    </div>
    <div class="card-body">

        <form action="/audit/answers/store" method="POST">

            {{ csrf_field() }}
            <input type="hidden" value="{{ Auth::user()->employee_number }}" name="submitted_by">
            <input type="hidden" value="{{$audit->id}}" name="survey_id">

            <table class="table bg-dark text-white table-bordered mb-4 mt-4">
                <thead>
                <tr>
                    <td>#</td>
                    <td><b>Question</b></td>
                </tr>
                <?php $rowCount = 0 ?>
                </thead>
                <tbody>
                    <tr>
                        <td>*</td>
                        <td>
                            Choose Your Cost Center
                            <span class="float-right">
                                <i class="fas fa-exclamation-triangle text-danger"></i> <b>REQUIRED</b>
                            </span>
                            <br>
                            <hr>
                            <div class="col">
                                <div class="form-group">
                                    <select name="cost_center" class="form-control" style="width:30%">
                                        <option value="">Choose A Cost Center</option>
                                        @foreach($cost_centers as $cc)
                                            <option value="{{ $cc->cost_center }}">{{ $cc->cost_center }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </td>
                    </tr>
                @foreach($questions as $question)

                    <input type="hidden" value="{{$question->question_type}}" name="{{$question->id}}[question_type]">

                    <?php $rowCount = $rowCount +1 ?>

                    <tr>
                        <td><b>{{$rowCount}}.</b></td>
                        <td>
                            <!-- Loop through the questions -->
                            {{$question->title}}
                            <span class="float-right">
                                @if($question->required == 1) <i class="fas fa-exclamation-triangle text-danger"></i> <b>REQUIRED</b> @endif
                            </span>
                            <br>
                            <!-- Display input depending on the type of question -->
                            @if($question->question_type == 'short_answer')
                                <p><b><i>Short Answer</i></b></p>
                                <hr>
                                <div class="col">
                                    <div class="form-group">
                                        <textarea class="form-control" id="exampleFormControlTextarea1" name="{{$question->id}}[answer]" rows="3" @if($question->required == 1) required @endif></textarea>
                                    </div>
                                </div>
                            @elseif($question->question_type == 'YesNoNA')
                            <hr>
                                <div class="col">
                                    <div class="form-check form-check-inline">
                                        <input style="width:30px; height:30px;" class="form-check-input" type="radio" name="{{$question->id}}[answer]" value="1" @if($question->required == 1) required @endif>
                                        <label style="font-size: 30px;" class="form-check-label">Yes</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input style="width:30px; height:30px;" class="form-check-input" type="radio" name="{{$question->id}}[answer]" value="0" @if($question->required == 1) required @endif>
                                        <label style="font-size: 30px;" class="form-check-label">No</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input style="width:30px; height:30px;" class="form-check-input" type="radio" name="{{$question->id}}[answer]" value="2" @if($question->required == 1) required @endif>
                                        <label style="font-size: 30px;" class="form-check-label">N/A</label>
                                    </div>
                                </div>
                            @endif
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="3"><input type="submit" class="btn btn-primary btn-block" value="Submit Survey"></td>
                </tr>
                </tbody>
            </table>
        </form>

    </div>
</div>

@endsection