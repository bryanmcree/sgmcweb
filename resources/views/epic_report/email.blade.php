<table>
    <thead>
        <tr>
            <td><b>Requester Name:</b></td>
            <td>{{$requester_name}}</td>
        </tr>
        <tr>
            <td><b>Department Name:</b></td>
            <td>{{$department_name}}</td>
        </tr>
        <tr>
            <td><b>Requester Phone:</b></td>
            <td>{{$requester_phone}}</td>
        </tr>
        <tr>
            <td><b>Requester Email:</b></td>
            <td>{{$requester_email}}</td>
        </tr>


        <tr>
            <td><b>Supervisor name:</b></td>
            <td>{{$supervisor_name}}</td>
        </tr>
        <tr>
            <td><b>Supervisor Email:</b></td>
            <td>{{$supervisor_email}}</td>
        </tr>
        <tr>
            <td><b>Request Approved?</b></td>
            <td>{{$approved}}</td>
        </tr>
        <tr>
            <td><b>Report Name:</b></td>
            <td>{{$report_name}}</td>
        </tr>
        <tr>
            <td><b>Patient Safety:</b></td>
            <td>{{$patient_safety}}</td>
        </tr>
        <tr>
            <td><b>Regulatory Requirement:</b></td>
            <td>{{$regulatory_requirement}}</td>
        </tr>
        <tr>
            <td><b>Revenue:</b></td>
            <td>{{$revenue}}</td>
        </tr>
        <tr>
            <td><b>Productivity:</b></td>
            <td>{{$productivity}}</td>
        </tr>
        <tr>
            <td><b>Patient and Family Experience:</b></td>
            <td>{{$patient_and_family_experience}}</td>
        </tr>
        <tr>
            <td><b>Organizational Impact:</b></td>
            <td>{{$organizational_impact}}</td>
        </tr>
        <tr>
            <td><b>What specifically are you going to do with the data?:</b></td>
            <td>{{$do_with_data}}</td>
        </tr>
        <tr>
            <td><b>What information needs to be included in the report?:</b></td>
            <td>{{$what_information}}</td>
        </tr>
        <tr>
            <td><b>Who needs access to this data/report?:</b></td>
            <td>{{$who_needs_access}}</td>
        </tr>
        <tr>
            <td><b>When do you need this report?</b></td>
            <td>{{$when_need_report}}</td>
        </tr>
        <tr>
            <td><b>Do similar reports already exist?</b></td>
            <td>{{$similar_report}}</td>
        </tr>
        <tr>
            <td><b>Priority:</b></td>
            <td>{{$criteria}}</td>
        </tr>
        <tr>
            <td><b>Criteria:</b></td>
            <td>{{$criteria_two}}</td>
        </tr>
        <tr>
            <td><b>Info to display:</b></td>
            <td>{{$info_to_display}}</td>
        </tr>
        <tr>
            <td><b>Group by:</b></td>
            <td>{{$group_by}}</td>
        </tr>
        <tr>
            <td><b>Criteria Layout:</b></td>
            <td>{{$criteria_text}}</td>
        </tr>
        <tr>
            <td><b>Parameters Layout:</b></td>
            <td>{{$parameters_text}}</td>
        </tr>
        <tr>
            <td><b>Date Range:</b></td>
            <td>{{$date_range}}</td>
        </tr>
    </thead>
</table>