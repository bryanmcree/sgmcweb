{{--New file Template--}}

{{--Add Security for this page below--}}


    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <b>Epic Report Request</b>
            </div>
            <div class="panel-body">
                <form method="post" action="/epic_report/submit">
                    {{ csrf_field() }}


                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="required_id">Requester Name:</label>
                                <small id="emailHelp" class="form-text text-muted">Name of the person making the report request.</small>
                                <input type="text" id="autocomplete" name="requester_name" class="form-control"required>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="required_id">Department Name:</label>
                                <small id="emailHelp" class="form-text text-muted">Name and cost center of the department making the request.</small>
                                <input type="text" id="autocomplete" name="department_name" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="required_id">Requester Phone Number:</label>
                                <small id="emailHelp" class="form-text text-muted">A number where the requester can be contacted.</small>
                                <input type="text" id="autocomplete"  name="requester_phone" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="required_id">Requester Email Address:</label>
                                <small id="emailHelp" class="form-text text-muted">Email of the requester.</small>
                                <input type="email" id="autocomplete" name="requester_email" class="form-control"  required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="required_id">Requester Supervisors Name:</label>
                                <small id="emailHelp" class="form-text text-muted">Name of the requesters supervisor.</small>
                                <input type="text" id="autocomplete" name="supervisor_name" class="form-control"required>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="required_id">Requester Supervisors Email:</label>
                                <small id="emailHelp" class="form-text text-muted">Email of the requesters supervisor.</small>
                                <input type="email" id="autocomplete" name="supervisor_email" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="question_type_id">Approved:</label>
                                <small id="emailHelp" class="form-text text-muted">Was this report discussed with supervisor and approved?</small>
                                <select class="form-control" id="question_type_id" name="approved" required>
                                    <option value="" selected>[Select]</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel panel-heading">
                                    <b>Report Details</b>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="required_id">Select a name for the report:</label>
                                                <small id="emailHelp" class="form-text text-muted">Select a unique name for this report.</small>
                                                <input type="text" id="autocomplete" class="form-control"  name="report_name" required>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            Why do you need this data/report?  What is the business case for this report? What issue do you expect this data to solve? How does this align with our organization's key initiatives?
                                            <div class="form-group">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input"  name="patient_safety" id="defaultUnchecked1">
                                                    <label class="custom-control-label"  for="defaultUnchecked1">Patient Safety</label>
                                                </div>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" name="regulatory_requirement" id="defaultUnchecked2">
                                                    <label class="custom-control-label" for="defaultUnchecked2">Regulatory Requirements</label>
                                                </div>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" name="revenue" id="defaultUnchecked3">
                                                    <label class="custom-control-label" for="defaultUnchecked3">Revenue</label>
                                                </div>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" name="productivity" id="defaultUnchecked4">
                                                    <label class="custom-control-label" for="defaultUnchecked4">Productivity</label>
                                                </div>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" name="patient_and_family_experience" id="defaultUnchecked5">
                                                    <label class="custom-control-label" for="defaultUnchecked5">Patient & Family Experience</label>
                                                </div>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" name="organizational_impact" id="defaultUnchecked6">
                                                    <label class="custom-control-label" for="defaultUnchecked6">Organizational Impact</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="required_id">What specifically are you going to do with the data?</label>
                                                <small id="emailHelp" class="form-text text-muted">What decisions will you make or what actions will you take based on the requested data? What will you do with the data itself? Will this data be reported to physicians?</small>
                                                <textarea class="form-control"  id="consumables_id" rows="5" name="do_with_data"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="required_id">What information needs to be included in the report?</label>
                                                <small id="emailHelp" class="form-text text-muted">Enter the name of the person, group, or role that will use this report. If it is distributed to individual people, include their contact information, such as phone number, email address, and site. Are there any groups of users who should not see this report, such as affiliates or other Community Connect service areas?</small>
                                                <textarea class="form-control"  id="consumables_id" rows="5"  name="what_information"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="required_id">Who needs access to this data/report?</label>
                                                <small id="emailHelp" class="form-text text-muted">Enter the name of the person, group, or role that will use this report. If it is distributed to individual people, include their contact information, such as phone number, email address, and site. Are there any groups of users who should not see this report, such as affiliates or other Community Connect service areas?</small>
                                                <textarea class="form-control"  id="consumables_id" rows="5" name="who_needs_access"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="required_id">When do you need this report?</label>
                                                <small id="emailHelp" class="form-text text-muted">Is there a specific deadline by when you must have this data?
                                                    For example, two weeks – board presentation
                                                </small>
                                                <textarea class="form-control"  id="consumables_id" rows="5" name="when_need_report"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="required_id">Do similar reports already exist?</label>
                                                <small id="emailHelp" class="form-text text-muted">If yes, what is the current report name and what is the current source system of the report? Explain how this request differs from similar reports.</small>
                                                <textarea class="form-control"  id="consumables_id" rows="5" name="similar_report"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="question_type_id">Criteria:</label>
                                                <small id="emailHelp" class="form-text text-muted">Please select one of the following criteria.</small>
                                                <select class="form-control" id="question_type_id" name="criteria" required>
                                                    <option value="" selected>[Select Criteria]</option>
                                                    <option value="Patient Safety">Patient Safety</option>
                                                    <option value="Compliance/Regulatory Need">Compliance/Regulatory Need</option>
                                                    <option value="Revenue Enhancement/Cost Avoidance">Revenue Enhancement/Cost Avoidance</option>
                                                    <option value="Workflow Improvement">Workflow Improvement</option>
                                                    <option value="Information Capture">Information Capture</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="question_type_id">Criteria:</label>
                                                <small id="emailHelp" class="form-text text-muted">Please select one of the following criteria.</small>
                                                <select class="form-control" id="question_type_id" name="criteria_two" required>
                                                    <option value="" selected>[Select Criteria]</option>
                                                    <option value="No risk or impact">No risk or impact</option>
                                                    <option value="Minimal risk or impact">Minimal risk or impact</option>
                                                    <option value="Moderate risk or impact">Moderate risk or impact</option>
                                                    <option value="Critical risk or impact">Critical risk or impact</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel panel-heading">
                                    <b>General Report Layout and Content</b>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="required_id">Info to display</label>
                                                <small id="emailHelp" class="form-text text-muted">Which data elements (fields or columns) should be included in the report?
                                                    For example, patient name, diagnosis, visit date, account balance

                                                    What visualizations, such as graphs or charts, are needed?
                                                </small>
                                                <textarea class="form-control"  id="consumables_id" rows="5" name="info_to_display"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="required_id">Group By</label>
                                                <small id="emailHelp" class="form-text text-muted">How should results be sorted or grouped? If multiple grouping levels, list from less specific to more specific.
                                                    For example, service area, bill area, provider

                                                    Include summaries? If so, which fields should you use? Which type of summary (for example, count/sum/average)?

                                                </small>
                                                <textarea class="form-control" id="consumables_id" rows="5" name="group_by"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="required_id">Criteria</label>
                                                <small id="emailHelp" class="form-text text-muted">Which records qualify for the report?
                                                    For example, all payments posted through electronic remittance by week
                                                    What are the inclusion and exclusion criteria?
                                                    ___ICD codes  ___ Procedures  ___ Meds  ___ DRGs  ___Other
                                                    Notes:


                                                </small>
                                                <textarea class="form-control"  id="consumables_id" rows="5" name="criteria_text"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="required_id">Parameters</label>
                                                <small id="emailHelp" class="form-text text-muted">Which criteria should you be able to select and change?
                                                    For example, departments, date ranges

                                                </small>
                                                <textarea class="form-control" id="consumables_id" rows="5" name="parameters_text"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="required_id">Date Range</label>
                                                <small id="emailHelp" class="form-text text-muted">What is the date range, and where is it derived from?
                                                    For example, encounter date or post date

                                                    How many days will the data be valid for? In other words, how many days should you keep the results around?
                                                </small>
                                                <textarea class="form-control"  id="consumables_id" rows="5" name="date_range"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="submit" class="btn btn-sgmc" value="Submit Epic Report Request">
                </form>
            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
