{{--New file Template--}}

{{--Add Security for this page below--}}


    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <b>Handwashing Survey</b>
            </div>
            <div class="panel-body">
                {!! Form::open(array('action' => ['HandwashingController@submit_survey_Anonymous'], 'class' => 'form_control')) !!}
                {!! Form::hidden('completed_by', 'Anonymous') !!}
                <table class="table table-bordered">

                    <tbody>
                    <tr>
                        <td  colspan="2">Survey completed by</td>
                        <td colspan="3" align="right">Anonymous</td>
                    </tr>
                    <tr>
                        <td colspan="2">Current Date</td>
                        <td colspan="3" align="right">{{\Carbon\Carbon::now()->format('m-d-Y')}}</td>
                    </tr>
                    <tr>
                        <td colspan="5" align="right">
                            <div class="input-group">
                                <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                <select name="cost_center" class="form-control">
                                    <option value="">[Select Cost Center]</option>
                                    @foreach($cost_centers as $costcenter)
                                        <option value="{{ $costcenter->cost_center }}"> {{$costcenter->cost_center_name}} ({{$costcenter->cost_center}})</option>
                                    @endforeach
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" align="right"><div class="input-group">
                                <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                {!! Form::select('shift', [
                                                    '' => '[Select Shift]',
                                                    'Day' => 'Day',
                                                    'Evening' => 'Evening',
                                                    'Night' => 'Night',
                                                     ], null, ['class' => 'form-control', 'required']) !!}
                            </div></td>
                    </tr>
                    <tr>
                        <td colspan="5" align="right">
                            <div class="input-group">
                                <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                {!! Form::select('practitioner', [
                                                    '' => '[Select Practitioner Type]',
                                                    'Licensed' => 'Licensed',
                                                    'Not Licensed' => 'Not Licensed',
                                                    'Physician' => 'Physician',
                                                     ], null, ['class' => 'form-control', 'required']) !!}
                            </div>
                        </td>
                    </tr>

                    <tr bgcolor="#d3d3d3">
                        <td></td>
                        <td><b>Survey Questions</b></td>
                        <td align="center"><b>Yes</b></td>
                        <td align="center"><b>No</b></td>
                        <td align="center"><b>N/A</b></td>
                    </tr>
                    <?php $rowcount = 0 ?>
                    @foreach ($questions as $question)
                        <?php $rowcount = $rowcount + 1?>
                        <tr>
                            <td><b>{{$rowcount}})</b></td>
                            <td>{{$question->question}}</td>
                            <td align="center"><input type="radio" class="form-control" name="response[{{$question->id}}][answer]" value="Yes" required> </td>
                            <td align="center"><input type="radio" class="form-control" name="response[{{$question->id}}][answer]" value="No" required></td>
                            <td align="center">
                                @if($question->category =='Handwashing')
                                    <input type="radio" class="form-control" name="response[{{$question->id}}][answer]" value="NA" disabled>
                                @else
                                    <input type="radio" class="form-control" name="response[{{$question->id}}][answer]" value="NA" required>
                                @endif
                            </td>

                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="5"><b>Comments or other issues.</b></td>
                    </tr>
                    <tr>
                        <td colspan="5"><textarea name="comments" class="form-control" rows="5"></textarea></td>
                    </tr>
                    </tbody>

                </table>
            </div>
            <div class="panel-footer">
                {!! Form::submit('Submit Survey', ['class'=>'btn btn-sgmc', 'id'=>'updateButton']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
