{{--New file Template--}}


@extends('layouts.app')
{{--Page Design Goes Below--}}
@section('content')
    <div class="card bg-dark p-3">
        <h2 class="text-danger">This application has been deactivated. Please use the link below</h2>
        
        <h3><a href="https://forms.office.com/r/RJPvEufJxz" class="text-white">New Handwashing Survey</a></h3>
    </div>
@endsection
    
{{-- @section('content')
    <br>
    <div class="card-deck">
        <div class="col-sm-4">
            <div class="card mb-4 text-white bg-dark">
                <div class="card-header">Hospital </div>
                <div class="card-body">
                    <div class="progress" style="height: 20px;">
                        <div class="progress-bar
                                @if($hospital_score >= 94)
                                bg-success
@elseif($hospital_score < 94 AND $hospital_score >=85)
                                bg-warning
@else
                                bg-danger
@endif

                                " role="progressbar" style="width: {{$hospital_score}}%;" aria-valuenow="{{$hospital_score}}" aria-valuemin="0" aria-valuemax="100">{{$hospital_score}}%</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="card mb-4 text-white bg-dark">
                <div class="card-header">Handwashing </div>
                <div class="card-body">
                    <div class="progress" style="height: 20px;">
                        <div class="progress-bar

                            @if($handwashing_score >= 94)
                                bg-success
@elseif($handwashing_score < 94 AND $handwashing_score >=85)
                                bg-warning
@else
                                bg-danger
@endif

                                " role="progressbar" style="width: {{$handwashing_score}}%;" aria-valuenow="{{$handwashing_score}}" aria-valuemin="0" aria-valuemax="100">{{$handwashing_score}}%</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="card mb-4 text-white bg-dark">
                <div class="card-header">Osha / CDC</div>
                <div class="card-body">
                    <div class="progress" style="height: 20px;">
                        <div class="progress-bar

                            @if($osha_score >= 94)
                                bg-success
@elseif($osha_score < 94 AND $osha_score >=85)
                                bg-warning
@else
                                bg-danger
@endif

                                " role="progressbar" style="width: {{$osha_score}}%;" aria-valuenow="{{$osha_score}}" aria-valuemin="0" aria-valuemax="100">{{$osha_score}}%</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card text-white bg-dark">
        <div class="card-body">
            <form method="post" action="/handwashing/submit">
                {{ csrf_field() }}
                <input type="hidden" name="completed_by" value="{{Auth::user()->employee_number}}">
                <table class="table table-bordered text-white bg-dark">
                    <tbody>
                    <tr>
                        <td colspan="5" align="right">
                            <div class="">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fas fa-exclamation-circle" style="color:red;"></i></div>
                                    </div>
                                    <select name="cost_center" class="form-control">
                                        <option value="">[Select Cost Center]</option>
                                        @foreach($hs_cost_centers as $costcenter)
                                            <option value="{{$costcenter->cost_center}}"> @if(!empty($costcenter->costCenters)) {{$costcenter->costCenters->style1}} @else {{ $costcenter->cost_center }} @endif</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" align="right">
                            <div class="">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fas fa-exclamation-circle" style="color:red;"></i></div>
                                    </div>
                                    <select class="form-control" id="exampleSelect1" name="shift" required>
                                        <option value="" selected>[Select Shift]</option>
                                        <option value="Day">Day</option>
                                        <option value="Evening">Evening</option>
                                        <option value="Night">Night</option>
                                    </select>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" align="right">
                            <div class="">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fas fa-exclamation-circle" style="color:red;"></i></div>
                                    </div>
                                    <select class="form-control" id="exampleSelect1" name="practitioner" required>
                                        <option value="" selected>[Select Practitioner Type]</option>
                                        <option value="Licensed">Licensed</option>
                                        <option value="Not Licensed">Not Licensed</option>
                                        <option value="Physician">Physician</option>
                                    </select>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><b>Survey Questions</b></td>
                        <td align="center"><b>Yes</b></td>
                        <td align="center"><b>No</b></td>
                        <td align="center"><b>N/A</b></td>
                    </tr>
                    @php $rowcount = 0 ?> @endphp
                    @foreach ($questions as $question)
                        @php $rowcount = $rowcount + 1 @endphp
                        <tr>
                            <td ><b>{{$rowcount}})</b></td>
                            <td style="font-weight: bold; font-size: 18px;">{{$question->question}}</td>
                            <td align="center"><input style="width:30px; height:30px;" type="radio" class="form-check-input" name="response[{{$question->id}}][answer]" value="Yes" required> </td>
                            <td align="center"><input style="width:30px; height:30px;" type="radio" class="form-check-input" name="response[{{$question->id}}][answer]" value="No" required></td>
                            <td align="center">
                                @if($question->category =='Handwashing')
                                    <input type="radio" style="width:30px; height:30px;" class="form-check-input" name="response[{{$question->id}}][answer]" value="NA" disabled>
                                @else
                                    <input type="radio" style="width:30px; height:30px;" class="form-check-input" name="response[{{$question->id}}][answer]" value="NA" required>
                                @endif
                            </td>

                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="5"><b>Comments or other issues.</b></td>
                    </tr>
                    <tr>
                        <td colspan="5"><textarea name="comments" class="form-control" rows="5"></textarea></td>
                    </tr>
                    </tbody>
                </table>
                <input type="submit" class="btn btn-sgmc"  value="Submit Handwashing Survey">
                <input type="reset" class="btn btn-default">
            </form>
        </div>
    </div>


@endsection --}}
{{--END of Content and START of Scripts--}}
{{-- @section('scripts')



@endsection --}}
