

<br>

    <div class="card-deck">
        <div class="col-sm-4">
            <div class="card mb-4 text-white bg-dark">
                <div class="card-header">Hospital </div>
                <div class="card-body">
                    <div class="progress" style="height: 20px;">
                        <div class="progress-bar
                                @if($hospital_score >= 94)
                                                                bg-success
                                @elseif($hospital_score < 94 AND $hospital_score >=85)
                                                                bg-warning
                                @else
                                                                bg-danger
                                @endif

                        " role="progressbar" style="width: {{$hospital_score}}%;" aria-valuenow="{{$hospital_score}}" aria-valuemin="0" aria-valuemax="100">{{$hospital_score}}%</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="card mb-4 text-white bg-dark">
                <div class="card-header">Handwashing </div>
                <div class="card-body">
                    <div class="progress" style="height: 20px;">
                        <div class="progress-bar

                            @if($handwashing_score >= 94)
                                                            bg-success
                            @elseif($handwashing_score < 94 AND $handwashing_score >=85)
                                                            bg-warning
                            @else
                                                            bg-danger
                            @endif

                    " role="progressbar" style="width: {{$handwashing_score}}%;" aria-valuenow="{{$handwashing_score}}" aria-valuemin="0" aria-valuemax="100">{{$handwashing_score}}%</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="card mb-4 text-white bg-dark">
                <div class="card-header">Osha / CDC</div>
                <div class="card-body">
                    <div class="progress" style="height: 20px;">
                        <div class="progress-bar

                            @if($osha_score >= 94)
                                                            bg-success
                            @elseif($osha_score < 94 AND $osha_score >=85)
                                                            bg-warning
                            @else
                                                            bg-danger
                            @endif

                    " role="progressbar" style="width: {{$osha_score}}%;" aria-valuenow="{{$osha_score}}" aria-valuemin="0" aria-valuemax="100">{{$osha_score}}%</div>
                    </div>
                </div>
            </div>
        </div>
    </div>





<script type="application/javascript">

</script>