{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Hand washing Admin') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.sgmc_nonav')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="card card-default">
            <div class="card-header">
               <b>Handwashing Survey - View</b>
            </div>
            <div class="card-body">

                <div class="row">
                    <div class="col-xs-6">
                        <div class="card card-default">
                            <div class="card-header">
                                <b>Date Completed</b>
                            </div>
                            <div class="card-body">
                                {{$survey->created_at}}
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="card card-default">
                            <div class="card-header">
                                <b>Cost Center</b>
                            </div>
                            <div class="card-body">
                                {{$survey->cost_center}} - {{$survey->cost_centers->cost_center_name}}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-4">
                        <div class="card card-default">
                            <div class="card-header">
                                <b>Completed By</b>
                            </div>
                            <div class="card-body">
                                {{$survey->completed_by}}
                            </div>
                        </div>
                    </div>


                    <div class="col-xs-4">
                        <div class="card card-default">
                            <div class="card-header">
                                <b>Shift</b>
                            </div>
                            <div class="card-body">
                                {{$survey->shift}}
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-4">
                        <div class="card card-default">
                            <div class="card-header">
                                <b>Practitioner</b>
                            </div>
                            <div class="card-body">
                                {{$survey->practitioner}}
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-xs-12">
                        <div class="card card-default">
                            <div class="card-header">
                                <b>Comments</b>
                            </div>
                            <div class="card-body">
                                {{$survey->comments}}
                            </div>
                        </div>
                    </div>
                </div>



                <div class="col-md-12">
                    <table class="table">
                        <thead>
                            <tr>
                                <td></td>
                                <td><b>Survey Question</b></td>
                                <td align="right"><b>Answer</b></td>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $rowcount = 0 ?>
                        @foreach($questions as $question)
                        <?php $rowcount = $rowcount + 1?>
                            <tr>
                                <td><b>{{$rowcount}})</b></td>
                                <td>{{$question->question}}</td>
                                <td align="right">
                                    @if($question->handwashing_answers->where('main_id', $survey->id)->first()->yes == 1)
                                        <div style="font-weight: bold; color: green;">YES</div>
                                    @endif
                                    @if($question->handwashing_answers->where('main_id', $survey->id)->first()->no == 1)
                                        <div style="font-weight: bold; color: red;">NO</div>
                                    @endif
                                    @if($question->handwashing_answers->where('main_id', $survey->id)->first()->na == 1)
                                        N/A
                                  @endif


                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
@endif