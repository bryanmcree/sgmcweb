{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Hand washing Admin') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-lg-12">
        <div class="card bg-dark mb-3 text-white">
            <div class="card-header">
                <B>Report for: {{$report_month}}/{{$report_year}}</B>
            </div>
            <div class="card-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card bg-dark mb-3 text-white border-info">
                            <div class="card-header">
                                <b>Stats</b>
                            </div>
                            <div class="card-body">
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <td><b>Metric</b></td>
                                            <td align="center"><b>Yes's</b></td>
                                            <td align="center"><b>Yes's %</b></td>
                                            <td align="center"><b>No's</b></td>
                                            <td align="center"><b>No's %</b></td>
                                            <td align="center"><b>N/A's</b></td>
                                            <td align="center"><b>N/A's %</b></td>
                                            <td align="center"><b>Total Responses</b></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="warning">
                                            <td>Handwashing</td>
                                            <td align="center">{{$handwashing_stats->sum('yes')}}</td>
                                            <td align="center">{{number_format(($handwashing_stats->sum('yes')/$handwashing_stats->count()*100),2)}}%</td>
                                            <td align="center">{{$handwashing_stats->sum('no')}}</td>
                                            <td align="center">{{number_format(($handwashing_stats->sum('no')/$handwashing_stats->count())*100,2)}}%</td>
                                            <td align="center">{{$handwashing_stats->sum('na')}}</td>
                                            <td align="center">{{number_format(($handwashing_stats->sum('na')/$handwashing_stats->count())*100,2)}}%</td>
                                            <td align="center">{{$handwashing_stats->count()}}</td>
                                        </tr>
                                        <tr class="success">
                                            <td>CDC/OSHA</td>
                                            <td align="center">{{$handwashing_stats_CDC->sum('yes')}}</td>
                                            <td align="center">{{number_format(($handwashing_stats_CDC->sum('yes')/$handwashing_stats_CDC->count()*100),2)}}%</td>
                                            <td align="center">{{$handwashing_stats_CDC->sum('no')}}</td>
                                            <td align="center">{{number_format(($handwashing_stats_CDC->sum('no')/$handwashing_stats_CDC->count())*100,2)}}%</td>
                                            <td align="center">{{$handwashing_stats_CDC->sum('na')}}</td>
                                            <td align="center">{{number_format(($handwashing_stats_CDC->sum('na')/$handwashing_stats_CDC->count())*100,2)}}%</td>
                                            <td align="center">{{$handwashing_stats_CDC->count()}}</td>
                                        </tr>
                                        <tr class="info">
                                            <td>Hospital</td>
                                            <td align="center">{{$handwashing_stats_hospital->sum('yes')}}</td>
                                            <td align="center">{{number_format(($handwashing_stats_hospital->sum('yes')/$handwashing_stats_hospital->count()*100),2)}}%</td>
                                            <td align="center">{{$handwashing_stats_hospital->sum('no')}}</td>
                                            <td align="center">{{number_format(($handwashing_stats_hospital->sum('no')/$handwashing_stats_hospital->count())*100,2)}}%</td>
                                            <td align="center">{{$handwashing_stats_hospital->sum('na')}}</td>
                                            <td align="center">{{number_format(($handwashing_stats_hospital->sum('na')/$handwashing_stats_hospital->count())*100,2)}}%</td>
                                            <td align="center">{{$handwashing_stats_hospital->count()}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-deck">
                        <div class="card bg-dark col-md-4 mb-3 text-white border-info">
                            <div class="card-header">
                                <b>Audit</b>
                                <br>
                                <i>This report shows who has completed and has not completed the required surveys for the month.</i>
                            </div>
                            <div class="card-body">
                                <table class="table table-condensed table-hover">
                                    <thead>
                                    <tr>
                                        <td><b>Cost Center</b></td>
                                        <td><b>Req</b></td>
                                        <td><b>Com</b></td>
                                        <td><b>Diff</b></td>
                                        <td><b>Manager</b></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($handwashing_audit as $audit)
                                        <tr @if($audit->extra < 0) class="danger" @endif >
                                            <td>{{$audit->cost_center_number}} - {{$audit->cost_center}}</td>
                                            <td>{{$audit->required}}</td>
                                            <td>{{$audit->completed}}</td>
                                            <td>{{$audit->extra}}</td>
                                            <td>{{$audit->manager}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card col-lg-8 bg-dark mb-3 text-white border-info">
                            <div class="card-header">
                                <b>Comments</b>
                                <br>
                                <i>Displays all comments submitted by person conducting the survey.</i>
                            </div>
                            <div class="card-body">
                                @if($handwashing_comments->isEmpty())

                                    <div class="alert alert-info">No comments</div>

                                @else
                                    <table class="table table-condensed table-hover">
                                        <thead>
                                        <tr>
                                            <td><b>Shift</b></td>
                                            <td><b>PR</b></td>
                                            <td><b>CC</b></td>
                                            <td><b>Entered</b></td>
                                            <td><b>Comment</b></td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($handwashing_comments as $comment)
                                            <tr>
                                                <td>{{$comment->shift}}</td>
                                                <td nowrap="">{{$comment->practitioner}}</td>
                                                <td>{{$comment->cost_center}}</td>
                                                <td nowrap="">{{$comment->completed_by}}</td>
                                                <td>{{$comment->comments}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                @endif
                            </div>
                        </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card bg-dark mb-3 text-white border-info">
                            <div class="card-header">
                                <b>Response Audit</b>
                                <br>
                                <i>This report displays the percent of each response, with a focus on surveyors who submit a large percentage of Yes responses.</i>
                            </div>
                            <div class="card-body">
                                <table class="table table-condensed table-hover">
                                    <thead>
                                    <tr>

                                        <td><b>Completed By</b></td>
                                        <td><b>Surveys</b></td>
                                        <td><b>Responses</b></td>
                                        <td><b>Yes</b></td>
                                        <td><b>Yes %</b></td>
                                        <td><b>No</b></td>
                                        <td><b>No %</b></td>
                                        <td><b>N/A</b></td>
                                        <td><b>N/A %</b></td>


                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($handwashing_responses as $response)
                                        <tr @if($response->yes/$response->TotalResponses >= 1) class="warning" @endif>

                                            <td>{{$response->completed_by}}</td>
                                            <td>{{$response->TotalResponses/7}}</td>
                                            <td>{{$response->TotalResponses}}</td>
                                            <td>{{$response->yes}}</td>
                                            <td>{{number_format(($response->yes/$response->TotalResponses)*100,2)}}%</td>
                                            <td>{{$response->no}}</td>
                                            <td>{{number_format(($response->no/$response->TotalResponses)*100,2)}}%</td>
                                            <td>{{$response->na}}</td>
                                            <td>{{number_format(($response->na/$response->TotalResponses)*100,2)}}%</td>


                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>




@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="application/javascript">

    </script>

@endsection
@endif