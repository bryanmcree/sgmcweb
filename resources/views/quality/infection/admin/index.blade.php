{{--New file Template--}}

{{--Add Security for this page below--}}


    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="card-deck">
        <div class="card col-sm-3 mb-4 text-white bg-dark border-info">
            <div class="card-body">
                <form method="post" action="/handwashing/admin/report">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="report_cc_id">Select Cost Center</label>
                        <select class="form-control input-sm" name="report_cc" id="report_cc_id">
                            <option value="ALL">[All Cost Centers]</option>
                            @foreach($report_cc as $cc)
                                <option value="{{$cc->cost_center_number}}">{{$cc->cost_center_number}} - {{$cc->cost_center}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="report_month_id">Report Month</label>
                        <select class="form-control input-sm" name="report_month" id="report_month_id" required>
                            <option value="">[Select Month]</option>
                            @foreach($report_month as $month)
                                <option value="{{$month->month}}">{{$month->month}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="report_year_id">Report Year</label>
                        <select class="form-control input-sm" name="report_year" id="report_year_id" required>
                            <option value="">[Select Year]</option>
                            @foreach($report_year as $year)
                                <option value="{{$year->year}}">{{$year->year}}</option>
                            @endforeach
                        </select>
                    </div>
                    <input type="submit" class="btn btn-primary btn-block btn-sm" value="Run Report">
                </form>
            </div>
        </div>
        <div class="card col-sm-3 mb-4 text-white bg-dark border-info">
            <div class="card-header">
                Surveys
            </div>
            <div class="card-body">
                <table class="table table-condensed">
                    <tr>
                        <td><b>Total</b></td>
                        <td align="right">{{$cost_centers->sum('surveys')}}</td>
                    </tr>
                    <tr>
                        <td><b>Completed</b></td>
                        <td align="right">{{$completed->count()}}</td>
                    </tr>
                    <tr>
                        <td><b>Remaining</b></td>
                        <td align="right">{{$cost_centers->sum('surveys') - $completed->count()}}</td>
                    </tr>
                    <tr>
                        <td><b>Percent</b></td>
                        <td align="right">{{number_format((float)($completed->count()/$cost_centers->sum('surveys'))*100,2,'.','') }}%</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="card col-sm-3 mb-4 text-white bg-dark border-info">
            <div class="card-header">
                Answers
            </div>
            <div class="card-body">
                <table class="table table-condensed">
                    <tr>
                        <td><b>Total</b></td>
                        <td align="center">{{$monthly_questions}}</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><b># of "Yes"</b></td>
                        <td align="center">{{$monthly_yes}}</td>
                        <td align="right">@if(is_null($monthly_yes)) 0 @else {{number_format((float)($monthly_yes/$monthly_questions)*100,2,'.','') }}% @endif</td>
                    </tr>
                    <tr>
                        <td><b># of "No"</b></td>
                        <td align="center">{{$monthly_no}}</td>
                        <td align="right">@if(is_null($monthly_no)) 0 @else {{number_format((float)($monthly_no/$monthly_questions)*100,2,'.','') }}% @endif</td>
                    </tr>
                    <tr>
                        <td><b># of "N/A"</b></td>
                        <td align="center">{{$monthly_na}}</td>
                        <td align="right">@if(is_null($monthly_na)) 0 @else {{number_format((float)($monthly_na/$monthly_questions)*100,2,'.','') }}% @endif</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="card col-sm-3 mb-4 text-white bg-dark border-info">
            <div class="card-header">
                Charts
            </div>
            <div class="card-body">

            </div>
        </div>
    </div>
    <div class="card-deck">
        <div class="card col-sm-6 mb-4 text-white bg-dark border-info" style="height:500px; overflow-y: auto;">
            <div class="card-header">
                Survey by Cost Center
            </div>
            <div class="card-body">
                <table class="table table-condensed">
                    <thead>
                    <tr>
                        <th><b>Cost Center</b></th>
                        <th align="center"><b># to do</b></th>
                        <th align="center"><b># done</b></th>
                        <th align="center"><b># left</b></th>
                        <th align="right"><b>Responsible Party</b></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($cost_centers as $costcenter)
                        @php
                            if(is_null($costcenter->surveyCount)){
                                $recordCount = 0;
                            }else{
                                $recordCount = $costcenter->surveyCount->completedtotal;
                            }
                        @endphp
                        <tr @if($costcenter->surveys <= $recordCount) class="bg-success" @else class="bg-danger" @endif>
                            <td>{{$costcenter->cost_center_name}} ({{$costcenter->cost_center}})</td>
                            <td align="center">{{$costcenter->surveys}}</td>
                            <td align="center">{{$recordCount}}</td>
                            <td align="center">{{$costcenter->surveys - $recordCount}}</td>
                            <td align="right">@if(!is_null($costcenter->manager)){{$costcenter->manager->first_name}} {{$costcenter->manager->last_name}} @else NA @endif</td>
                            <td><a href="#" class="btn btn-primary btm-sm edit-costcenter"
                                    data-name = "{{ $costcenter->cost_center_name }}"
                                    data-number = "{{ $costcenter->cost_center }}"
                                    data-manager = "{{ $costcenter->employee_number }}"
                                >Edit</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="card mb-4 text-white bg-dark border-info" style="height:500px; overflow-y: auto;">
            <div class="card-header">
                Monthly Completed Surveys
            </div>
            @if($completed->isEmpty())
                <div class="card-body">
                    <div class="alert alert-info">There are no completed surveys for this month.</div>
                </div>
            @else
                <div class="card-body">
                    <table class="table text-white bg-dark" id="completed">
                        <thead>
                        <tr>
                            <td><b>Completed By</b></td>
                            <td><b>Date</b></td>
                            <td><b>Cost Center</b></td>
                            <td><b>License Type</b></td>
                            <td><b>Shift</b></td>
                            <td align="center"><b>Comments?</b></td>
                            <td></td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($completed as $done)
                            <tr>
                                <td>{{$done->completed_by}}</td>
                                <td>{{$done->created_at}}</td>
                                <td>{{ $done->cost_center }}</td>
                                <td>{{$done->practitioner}}</td>
                                <td>{{$done->shift}}</td>
                                <td align="center">@if(!empty($done->comments)) <i class="fa fa-commenting-o bg-success" aria-hidden="true"></i> @else <i class="fa fa-ban bg-danger" aria-hidden="true"></i> @endif</td>
                                <td align="right"><a class="btn btn-primary btn-xs" data-toggle="tooltip" title="View Completed Survey" target="_blank" href="/handwashing/admin/view/{{$done->id}}"><i class="fa fa-eye"></i> View</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @endif
        </div>
    </div>


@include('quality.infection.admin.edit')

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">

$(document)
.ready(function () {
    $('#completed').dataTable({
        "order": [[ 1, "desc" ]],
        "pageLength": 100
    });
});

$(document).on("click", ".edit-costcenter", function () {
    $('.cc-name').val($(this).attr("data-name"));
    $('.cc-number').val($(this).attr("data-number"));
    $('.cc-manager').val($(this).attr("data-manager"));
    $('#editCC').modal('show');
});


</script>

@endsection
