<div class="modal fade" id="editCC" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document" style="max-width: 45%">
        <div class="modal-content text-white bg-dark">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Cost Center</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form method="POST" action="/handwashing/update">
                <input type="hidden" name="id" class="cc-number">
                {{ csrf_field() }}

                <div class="modal-body">

                    <div class="form-row">
                        <div class="form-group col-6">
                            <label>Cost Center Name</label>
                            <input type="text" class="form-control cc-name" name="ccName" placeholder="Name Of Cost Center" required>
                        </div>
                        <div class="form-group col-6">
                            <label>Cost Center Number</label>
                            <input type="text" class="form-control cc-number" name="ccNumber" placeholder="Number Of Cost Center" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Responsible Party <small><i>(employee number)</i></small></label>
                        <input type="text" class="form-control cc-manager" name="ccManager" required>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Update">
                </div>

            </form>
        </div>
    </div>
</div>