{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Quality') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="card bg-dark mb-3 text-white">
            <div class="card card-header">
                <h2><b>Imaging Quality Control</b></h2>
            </div>
            <div class="card-body">
                <h3>There are <b>{{(\Carbon\Carbon::now()->lastOfMonth()->day - \Carbon\Carbon::now()->day)+1}}</b> days remaining in this month.</h3>
            </div>
        </div>
    </div>
    {!! Form::open(array('action' => ['QualityController@notCompleted'], 'class' => 'form_control')) !!}
    <div class="col-lg-3">

        <div class="card bg-dark mb-3 text-white">
            <div class="card card-header">
                Select Date Range
            </div>
            <div class="card-body">
                <div class="form-group">
                    {!! Form::label('month', 'Month:') !!}
                    <div class="input-group">
                        <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                        {{ Form::date('start_date', null, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('year', 'Year:') !!}
                    <div class="input-group">
                        <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                        {!! Form::date('end_date', null, ['class' => 'form-control', 'id'=>'year', 'required']) !!}
                    </div>
                </div>
                {!! Form::submit('Run Report', array('class' => 'btn form-control btn-block btn-primary')) !!}
            </div>
        </div>
    </div>

    {!! Form::close() !!}



@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
@endif