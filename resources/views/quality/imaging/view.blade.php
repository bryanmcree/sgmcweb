{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Quality') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="card bg-dark mb-3 text-white">
            <div class="card card-header">
                <h2><b>Imaging Quality Control</b></h2>
            </div>
            <div class="card-body">
                <h3>There are <b>{{(\Carbon\Carbon::now()->lastOfMonth()->day - \Carbon\Carbon::now()->day)+1}}</b> days remaining in this month.</h3>
            </div>
        </div>
    </div>

    {!! Form::open(array('action' => ['QualityController@notCompleted'], 'class' => 'form_control')) !!}
    <div class="col-lg-3">

        <div class="card bg-dark mb-3 text-white">
            <div class="card card-header">
                Select Date Range
            </div>
            <div class="card-body">
                <div class="form-group">
                    {!! Form::label('month', 'Month:') !!}
                    <div class="input-group">
                        <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                        {{ Form::selectMonth('month', 7, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('year', 'Year:') !!}
                    <div class="input-group">
                        <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                        {!! Form::text('year', null, ['class' => 'form-control', 'id'=>'year','placeholder'=>'Year', 'required']) !!}
                    </div>
                </div>
                {!! Form::submit('Run Report', array('class' => 'btn form-control btn-block btn-primary')) !!}
            </div>
            </div>
        </div>

    {!! Form::close() !!}

    <div class="col-lg-3">
        <div class="card bg-dark mb-3 text-white">
            <div class="card card-header">
                Report
            </div>
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <td><b>Measure</b></td>
                        <td><b>CC</b></td>
                        <td><b>Completed</b></td>
                        <td><b>Required</b></td>
                        <td><b>Difference</b></td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($monthly as $costCenters)
                    <tr @if($costCenters->qty <0) class="bg-danger" @else class = "bg-success" @endif>
                        <td>{{$costCenters->measure}}</td>
                        <td>{{$costCenters->cost_center}}</td>
                        <td></td>
                        <td>{{$costCenters->qty}}</td>
                        <td>{{ $costCenters->qty}}</td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="card bg-dark mb-3 text-white">
            <div class="card card-header">
                MRI Quality Measures
            </div>
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <td><b>Question</b></td>
                        <td><b>Yes</b></td>
                        <td><b>No</b></td>
                        <td><b>N/A</b></td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Is this the correct OP order scanned into PACS?</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Is Contrast Consent Form signed, completed, and scanned?</td>
                        @foreach($MRI as $MRI_two)
                        <td>{{$MRI_two->totals}}</td>
                        @endforeach
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>



@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
@endif