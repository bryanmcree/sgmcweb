<div class="modal fade Ultrasound_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><b>Ultrasound Quality Measures</b></h4>
            </div>
            <div class="modal-body bg-default">
                {!! Form::open(array('action' => ['QualityController@newMeasure'], 'class' => 'form_control')) !!}
                {!! Form::hidden('measure', 'Ultrasound') !!}
                {!! Form::hidden('completed_by', Auth::user()->employee_number) !!}
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::label('accession_number', 'Accession Number:') !!}
                            <div class="input-group">
                                <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                {!! Form::text('accession_number', null, ['class' => 'form-control', 'id'=>'sys_name']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::label('date_of_service', 'Date of Service:') !!}
                            <div class="input-group">
                                <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                {!!Form::input('date', 'date_of_service', null, ['class' => 'form-control', 'required']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::label('cost_center', 'Ultrasound Cost Center:') !!}
                            <div class="input-group">
                                <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                {!! Form::select('cost_center', [
                                                    '' => '[Select Cost Center]',
                                                    '7044' => '7044',
                                                    '7087' => '7087',
                                                    '7815' => '7815',
                                                    '7400' => '7400',
                                                    '6524' => '6524',
                                                     ], null, ['class' => 'form-control', 'Required']) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <hr>
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <h5><b>Is the correct OP order scanned into PACS?</b></h5>
                    </div>
                    <div class="col-md-4" >
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                {!! Form::select('one', [
                                                    '' => '[Select]',
                                                    'Yes' => 'Yes',
                                                    'No' => 'No',
                                                    'N/A' => 'N/A',
                                                     ], null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <h5><b>On applicable exams, was a BETA HCG performed?</b></h5>
                    </div>
                    <div class="col-md-4" >
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                {!! Form::select('two', [
                                                    '' => '[Select]',
                                                    'Yes' => 'Yes',
                                                    'No' => 'No',
                                                    'N/A' => 'N/A',
                                                     ], null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <h5><b>Are the US images labeled appropriately?</b></h5>
                    </div>
                    <div class="col-md-4" >
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                                {!! Form::select('three', [
                                                    '' => '[Select]',
                                                    'Yes' => 'Yes',
                                                    'No' => 'No',
                                                    'N/A' => 'N/A',
                                                     ], null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                </div>


                <div class="modal-footer bg-default">
                    <div class="RightLeft">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        {!! Form::submit('Add Measure', ['class'=>'btn btn-sgmc', 'id'=>'updateButton']) !!}
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>