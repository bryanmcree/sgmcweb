{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Quality') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="card bg-dark mb-3 text-white">
            <div class="card card-header">
                <h2><b>Imaging Quality Control</b></h2>
            </div>
            <div class="card-body">
                <h3>There are <b>{{(\Carbon\Carbon::now()->lastOfMonth()->day - \Carbon\Carbon::now()->day)+1}}</b> days remaining in this month.</h3>
            </div>
        </div>

        <div class="col-md-2">
            <div class="card bg-dark mb-3 text-white border-success">
                <div class="card card-header">
                    <b>MRI</b>
                </div>
                <div class="card-body">
                    <a href="#" class="btn btn-primary btn-block" data-toggle="modal" data-target=".MRI_modal">
                        Add MRI Measure
                    </a>
                    @if(($monthlyMRI->isEmpty()))

                        <div class="alert " role="alert">You have no measures.</div>
                    @else
                    <table class="table">
                        <thead>
                            <tr>
                                <td><b>Cost Center</b></td>
                                <td align="right"><b>Total</b></td>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($monthlyMRI as $mri)
                            <tr>
                                <td>{{$mri->cost_center}}</td>
                                <td align="right">@if($mri->perMonth->qty - $mri->total > 0){{$mri->total}}({{$mri->perMonth->qty - $mri->total}})@else <i class="fa fa-check-square-o" aria-hidden="true"></i> @endif</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                        @endif
                </div>
            </div>
        </div>

        <div class="col-md-2">
            <div class="card bg-dark mb-3 text-white border-success">
                <div class="card card-header">
                    <b>CT</b>
                </div>
                <div class="card-body">
                    <a href="#" class="btn btn-primary btn-block" data-toggle="modal" data-target=".CT_modal">
                        Add CT Measure
                    </a>
                    @if(($monthlyCT->isEmpty()))

                        <div class="alert " role="alert">You have no measures.</div>
                    @else
                        <table class="table">
                            <thead>
                            <tr>
                                <td><b>Cost Center</b></td>
                                <td align="right"><b>Total</b></td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($monthlyCT as $ct)
                                <tr>
                                    <td>{{$ct->cost_center}}</td>
                                    <td align="right">@if($ct->perMonth->qty - $ct->total > 0){{$ct->total}}({{$ct->perMonth->qty - $ct->total}})@else <i class="fa fa-check-square-o" aria-hidden="true"></i> @endif</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-md-2">
            <div class="card bg-dark mb-3 text-white border-success">
                <div class="card card-header">
                    <b>IR</b>
                </div>
                <div class="card-body">
                    <a href="#" class="btn btn-primary btn-block" data-toggle="modal" data-target=".IR_modal">
                        Add IR Measure
                    </a>
                    @if(($monthlyIR->isEmpty()))

                        <div class="alert " role="alert">You have no measures.</div>
                    @else
                        <table class="table">
                            <thead>
                            <tr>
                                <td><b>Cost Center</b></td>
                                <td align="right"><b>Total</b></td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($monthlyIR as $ir)
                                <tr>
                                    <td>{{$ir->cost_center}}</td>
                                    <td align="right">@if($ir->perMonth->qty - $ir->total > 0){{$ir->total}}({{$ir->perMonth->qty - $ir->total}})@else <i class="fa fa-check-square-o" aria-hidden="true"></i> @endif</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-md-2">
            <div class="card bg-dark mb-3 text-white border-success">
                <div class="card card-header">
                    <b>Nuc Med</b>
                </div>
                <div class="card-body">
                    <a href="#" class="btn btn-primary btn-block" data-toggle="modal" data-target=".Nuc_Med_modal">
                        Add Nuc Med Measure
                    </a>
                    @if(($monthlyNuc->isEmpty()))

                        <div class="alert " role="alert">You have no measures.</div>
                    @else
                        <table class="table">
                            <thead>
                            <tr>
                                <td><b>Cost Center</b></td>
                                <td align="right"><b>Total</b></td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($monthlyNuc as $nu)
                                <tr>
                                    <td>{{$nu->cost_center}}</td>
                                    <td align="right">@if($nu->perMonth->qty - $nu->total > 0){{$nu->total}}({{$nu->perMonth->qty - $nu->total}})@else <i class="fa fa-check-square-o" aria-hidden="true"></i> @endif</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-md-2">
            <div class="card bg-dark mb-3 text-white border-success">
                <div class="card card-header">
                    <b>Ultrasound</b>
                </div>
                <div class="card-body">
                    <a href="#" class="btn btn-primary btn-block" data-toggle="modal" data-target=".Ultrasound_modal">
                        Add Ultrasound Measure
                    </a>
                    @if(($monthlyUltra->isEmpty()))

                        <div class="alert " role="alert">You have no measures.</div>
                    @else
                        <table class="table">
                            <thead>
                            <tr>
                                <td><b>Cost Center</b></td>
                                <td align="right"><b>Total</b></td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($monthlyUltra as $ut)
                                <tr>
                                    <td>{{$ut->cost_center}}</td>
                                    <td align="right">@if($ut->perMonth->qty - $ut->total > 0){{$ut->total}}({{$ut->perMonth->qty - $ut->total}})@else <i class="fa fa-check-square-o" aria-hidden="true"></i> @endif</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-md-2">
            <div class="card bg-dark mb-3 text-white border-success">
                <div class="card card-header">
                    <b>X-Ray</b>
                </div>
                <div class="card-body">
                    <a href="#" class="btn btn-primary btn-block" data-toggle="modal" data-target=".Xray_modal">
                        Add X-Ray Measure
                    </a>
                    @if(($monthlyXray->isEmpty()))

                        <div class="alert " role="alert">You have no measures.</div>
                    @else
                        <table class="table">
                            <thead>
                            <tr>
                                <td><b>Cost Center</b></td>
                                <td align="right"><b>Total</b></td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($monthlyXray as $xr)
                                <tr>
                                    <td>{{$xr->cost_center}}</td>
                                    <td align="right">@if($xr->perMonth->qty - $xr->total > 0){{$xr->total}}({{$xr->perMonth->qty - $xr->total}})@else <i class="fa fa-check-square-o" aria-hidden="true"></i> @endif</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>
    </div>

@include('quality.imaging.model.mri')
@include('quality.imaging.model.ct')
@include('quality.imaging.model.ir')
@include('quality.imaging.model.nucmed')
@include('quality.imaging.model.ultrasound')
@include('quality.imaging.model.xray')
@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
@endif