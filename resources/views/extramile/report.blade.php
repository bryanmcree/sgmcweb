
@extends('layouts.app')

@section('content')

<div class="card mb-3 border-info text-white bg-dark">
    <div class="card-body" style="height:90%; overflow-y: auto;">

        <div class="jumbotron bg-secondary">
            <h2 style="display:inline;">Awards Given Within Date Range: <span class="text-warning"> {{$reportData->count()}} </span> </h2>
            <h2 class="float-right" style="display:inline;"><a href="/extramile/report/export/{{ $startDate }}/{{ $endDate }}" class="btn btn-success">Export Report</a></h2>
        </div>

        <table class="table mt-3" id="emReport" width="100%">
            <thead>
                <tr>
                    <td><b>Presenter</b></td>
                    <td><b>Receiver</b></td>
                    <td align="center"><b>Meal Ticket #</b></td>
                    <td><b>Reason</b></td>
                    <td><b>Issued</b></td>
                </tr>
            </thead>
            <tbody>
            @foreach($reportData as $em)
                <tr>
                    <td>{{$em->presenterInfo->name}}</td>
                    <td>{{$em->receiverInfo->name}}</td>
                    <td align="center">{{$em->mtn}}</td>
                    <td>{{ $em->events }}</td>
                    <td>{{$em->created_at->format('m-d-Y')}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection

@section('scripts')

<script>

    $(document).ready(function() {
            $('#emReport').DataTable( {
                "pageLength": 30,
                "order": [],
                "columnDefs": [ {
                    "targets"  : 'no-sort',
                    "orderable": false,
                }]
            } );
        });

</script>

@endsection