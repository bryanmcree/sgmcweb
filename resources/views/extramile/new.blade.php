@extends('layouts.app')



@section('content')

    <div class="card mb-4 border-info text-white bg-dark">
        <div class="card-header">
            Extra Mile Award
        </div>
        <div class="card-body">
            {!! Form::open(array('action' => ['ExtraMileController@store'], 'class' => 'form_control')) !!}
            {!! Form::hidden('receiver_id', $user->employee_number) !!}
            {!! Form::hidden('presenter_id', Auth::user()->employee_number) !!}
            <div class="row">
                <div class="col-md-12">
                    <h4>Managers can issue an "Extra Mile" Award to an employee in recognition of service above and beyond expectations in the job description.  Examples include:</h4>

                    <ul>
                        <li>Customer Satisfaction as providing excellent customer service.</li>
                        <li>Handling a difficult situation involving a patient or family.</li>
                        <li>Demonstrating commitment to the SGHS Facility Mission and/or Values Statements</li>
                        <li>Performance above and beyond expectations in the job description</li>
                        <li>Teamwork</li>
                        <li>Any other instance of exemplary service to the SGHS Facility and its customers, employees or patients.</li>
                    </ul>
                    <h4>Recipents of an "Extra Mile" Award are presented with a certificate, recognition and a free meal ticket.</h4>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <h4><b>Presented to:</b> {{$user->first_name}} {{$user->last_name}}</h4>
                    <h5><b>Title:</b> {{$user->title}}</h5>
                    <h5><b>Cost Center:</b> {{$user->unit_code_description}}</h5>
                    <h5><b>Campus: </b> {{$user->location}}</h5>
                    <h5><b>Employee #: </b> {{$user->employee_number}}</h5>
                </div>
                <div class="col-md-6" align="right">
                    <h4><b>Presented by:</b> {{\Auth::user()->first_name}} {{\Auth::user()->last_name}}</h4>
                    <h5><b>Employee #:</b> {{\Auth::user()->employee_number}}</h5>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-lg-offset-3">
                    <div class="form-group">
                        {!! Form::label('events', 'What events lead up to recipient receiving award?',['class'=>'ldac']) !!}
                        {!! Form::textarea('events', null, ['class' => 'form-control events', 'id'=>'events', 'rows' => 4, 'required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('ldac', 'What characteristics did recipient demonstrate? (Select all that apply):',['class'=>'ldac required']) !!}<br>
                        {!! Form::checkbox('customer', 'YES', null, ['class' => 'field', 'id'=>'customer'])  !!}{!! Form::label('customer', ' - Customer satisfaction (named by patient, visitor, or physician)') !!}<br>
                        {!! Form::checkbox('handling', 'YES', null, ['class' => 'field', 'id'=>'handling'])  !!}{!! Form::label('handling', ' - Handling a difficult situation involving a patient or family') !!}<br>
                        {!! Form::checkbox('demonstrating', 'YES', null, ['class' => 'field', 'id'=>'demonstrating'])  !!}{!! Form::label('demonstrating', ' - Demonstrating commitment to Mission and/or Values Statements') !!}<br>
                        {!! Form::checkbox('performance', 'YES', null, ['class' => 'field', 'id'=>'performance'])  !!}{!! Form::label('performance', ' - Performance above and beyond expectations as outlined in the job description') !!}<br>
                        {!! Form::checkbox('teamwork', 'YES', null, ['class' => 'field', 'id'=>'teamwork'])  !!}{!! Form::label('teamwork', ' - Teamwork') !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('mtn', 'Meal Ticket Number:') !!} <i>(Meal ticket numbers come printed on the cards you get from Human Resources.) </i>
                        <div class="input-group">
                            <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                            {!! Form::text('mtn', null, ['class' => 'form-control mtn', 'id'=>'mtn']) !!}
                        </div>
                    </div>
                </div>

            </div>
            <div class="RightLeft">
                <button type="button" class="btn btn-default" data-dismiss="modal">Clear</button>
                {!! Form::submit('Add Extra Mile', ['class'=>'btn btn-sgmc', 'id'=>'updateButton']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>

@endsection

@section('scripts')

    <script type="text/javascript">


    </script>
@endsection