@extends('layouts.app')



@section('content')
    @if(Session::has('AddedExtraMile'))
        <meta http-equiv="refresh" content="5;url={{ Session::get('AddedExtraMile') }}">
    @endif
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel panel-heading-custom">
                <h2><b>Extra Mile Award</b></h2>
                <div class="panel-sub-title">
                    <i>System Policy 7.005</i>
                </div>
            </div>

            <div class="panel-body">
                <h4>Managers can issue an "Extra Mile" Award to an employee in recognition of service above and beyond expectations in the job description.  Examples include:</h4>

                <ul>
                    <li>Customer Satisfaction as providing excellent customer service.</li>
                    <li>Handling a difficult situation involving a patient or family.</li>
                    <li>Demonstrating commitment to the SGHS Facility Mission and/or Values Statements</li>
                    <li>Performance above and beyond expectations in the job description</li>
                    <li>Teamwork</li>
                    <li>Any other instance of exemplary service to the SGHS Facility and its customers, employees or patients.</li>
                </ul>
            <h4>Recipents of an "Extra Mile" Award are presented with a certificate, recognition and a free meal ticket.</h4>
            <br>
            <br>
            <h4>To recommend an employee for a "Extra Mile" Award simply locate them in the list below.</h4>
            <br>
                <div class="input-group">
                    <span class="input-group-addon">
                <i class="fa fa-trophy" aria-hidden="true"></i>
                </span>
                    {!! Form::text('search', null,array('class'=>'form-control','id'=>'myInputTextField','placeholder'=>'Search Extra Mile Award Winner...', 'autofocus'=>'autofocus')) !!}
                    <span class="input-group-addon">
                <i class="fa fa-search" aria-hidden="true"></i>
                </span>
                </div>
            <br>
            <table class="table table-stripped table-hover" id="users">
                <thead>
                <tr>
                    <td><b>Photo</b></td>
                    <td><b>Name</b></td>
                    <td><b>Title</b></td>
                    <td><b>Cost Center</b></td>
                    <td><b>Campus</b></td>
                    <td><b>Supervisor</b></td>
                    <td class='no-sort'></TD>
                </tr>
                </thead>
                <tbody>
                @foreach ($users as $user)
                <tr>
                    <td>@if($user->photo != '')<img src="data:image/jpeg;base64,{{str_replace('</photo>','',str_replace('<photo>','',$user->photo))}}" height="60" width="60"/>@else {{ Html::image('img/nopic.png', 'alt', array( 'width' => 60, 'height' => 60 )) }} @endif</td>
                    <td>{{$user->last_name}}, {{$user->first_name}}</td>
                    <td>{{$user->title}}</td>
                    <td>{{$user->unit_code_description}}</td>
                    <td>{{$user->location}}</td>
                    <td>{{$user->managername}}</td>
                    <td align="right"><a href="/extramile/new/{{$user->id}}" role="button"  ><span class="fa fa-pulse fa-star" aria-hidden="true"></span> Select</a></td>
                </tr>
                @endforeach
                </tbody>
            </table>




            </div>
        </div>
    </div>

@endsection

@section('scripts')

    <script type="text/javascript">



        $(document).ready(function() {
            oTable = $('#users').DataTable(
                    {
                        "info":     false,
                        "pageLength": 5,
                        "lengthChange": false,
                        "order": [],
                        "columnDefs": [
                            { targets: 'no-sort', orderable: false }
                        ],
                        "dom": '<l<t>ip>'

                    }
            );
            $('#myInputTextField').keyup(function(){
                oTable.search($(this).val()).draw() ;
            })


        });
    </script>
@endsection