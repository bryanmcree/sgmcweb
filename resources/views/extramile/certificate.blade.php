<table border="1" width="99%" align="center">
    <tr>
        <td height="99%">
            <table width="99%" align="center">
                <tr>
                    <td colspan="2">
                        <br>
                        <div align="center">{{ Html::image('img/rsz_sgmc_master_logo.png', 'alt', array( 'width' => 400, 'height' => 129 )) }}</div>
                        <div align="center"><h1>"Extra Mile" Certificate</h1></div>
                        <div align="center"><b><i>Presented to</i></b></div>
                        <div align="center"><h1><b><u>{{$data->receiverInfo->first_name}} {{$data->receiverInfo->last_name}}</u></b></h1></div>
                        <div align="center"><b><i>For</i></b><br><br>
                            @if($data->customer =='YES')
                            Customer satisfaction (named by patient, visitor or physician)<br>
                            @endif
                            @if($data->handling =='YES')
                            Handling a difficult situation involving a patient or family<br>
                            @endif
                            @if($data->demonstrating =='YES')
                            Demonstrating commitment to Mission and Values Statements<br>
                            @endif
                            @if($data->performance =='YES')
                            Performance above and beyond expectations as outlined in the job description<br>
                            @endif
                            @if($data->teamwork =='YES')
                            Teamwork
                            @endif
                        </div>

                    </td>
                </tr>
                <tr>
                    <td colspan="2"><hr><br></td>
                </tr>
                <tr>
                    <td width="50%">___________________________________________<BR>
                        <b>{{$data->presenterInfo->first_name}} {{$data->presenterInfo->last_name}}</b><br>
                        <i>{{$data->presenterInfo->title}}</i><br>
                        <i>{{$data->created_at}}</i></td>
                    <td align="right">{{ Html::image('img/certificateseal.png', 'alt', array( 'width' => 150, 'height' => 111 )) }}</td>
                </tr>
            </table>
        </td>
    </tr>


</table>