@extends('layouts.app')

@section('content')

<div class="card bg-dark text-white mb-3">
    <div class="card-body">
        <table class="table" width="100%">
            <thead>
                <tr>
                    <td><b>Presenter</b></td>
                    <td><b>Receiver</b></td>
                    <td align="center"><b>Meal Ticket #</b></td>
                    <td align="center"><b>Customer</b></td>
                    <td align="center"><b>Handling</b></td>
                    <td align="center"><b>Demonstrating</b></td>
                    <td align="center"><b>Performance</b></td>
                    <td align="center"><b>Teamwork</b></td>
                    <td><b>Issued</b></td>
                    <td align="right"><b>Reprint</b></td>
                </tr>
            </thead>
            <tbody>
            @foreach($allMiles as $em)
                <tr>
                    <td>@if(!is_null($em->presenterInfo)){{$em->presenterInfo->name}} @else - @endif</td>
                    <td>@if(!is_null($em->receiverInfo)){{$em->receiverInfo->name}} @else - @endif</td>
                    <td align="center">@if(!is_null($em->mtn)){{$em->mtn}} @else - @endif</td>
                    <td align="center">@if(!is_null($em->customer)) <i class="far fa-check-circle" style="color: green;"></i> @else - @endif</td>
                    <td align="center">@if(!is_null($em->handling)) <i class="far fa-check-circle" style="color: green;"></i> @else - @endif</td>
                    <td align="center">@if(!is_null($em->demonstrating)) <i class="far fa-check-circle" style="color: green;"></i> @else - @endif</td>
                    <td align="center">@if(!is_null($em->performance)) <i class="far fa-check-circle" style="color: green;"></i> @else - @endif</td>
                    <td align="center">@if(!is_null($em->teamwork)) <i class="far fa-check-circle" style="color: green;"></i> @else - @endif</td>
                    <td>{{$em->created_at->format('m-d-Y')}}</td>
                    <td align="right"><a href="extramile/download/{{$em->id}}"><i class="fas fa-print"></i></a> </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection

@section('scripts')

    <script>
        $(document).ready(function() {
                $('#allem').DataTable( {
                    "pageLength": 20,
                    "order": [],
                    "columnDefs": [ {
                        "targets"  : 'no-sort',
                        "orderable": false,
                    }]
                } );
            });
    </script>

@endsection