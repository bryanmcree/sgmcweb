{{--New file Template--}}

{{--Add Security for this page below--}}
@if( Auth::user()->hasRole('MOR_Access') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')


    <div class="card mb-4 text-white bg-secondary">
        <div class="card-header">
            MOR Report
        </div>
        <div class="card-body">
            
            <div class="table-responsive">
                <table class="table table-dark table-hover table-bordered text-center">
                    
                    <thead>
                        <th style="vertical-align: middle;">Month Actual</th>
                        <th style="vertical-align: middle;">Month Budget</th>
                        <th style="vertical-align: middle;">$ Variance</th>
                        <th style="vertical-align: middle;">% Variance</th>
                        <th style="vertical-align: middle;">Prior Year Same Month</th>
                        <th style="vertical-align: middle;">$ Variance</th>
                        <th style="vertical-align: middle;">% Variance</th>
                        <th colspan="2" class="bg-secondary"> <img src="/img/sgmc_logo.png" alt=""> </th>
                        <th style="vertical-align: middle;">YTD Actual</th>
                        <th style="vertical-align: middle;">YTD Budget</th>
                        <th style="vertical-align: middle;">$ Variance</th>
                        <th style="vertical-align: middle;">% Variance</th>
                        <th style="vertical-align: middle;">YTD Prior Yr</th>
                        <th style="vertical-align: middle;">$ Variance</th>
                        <th style="vertical-align: middle;">% Variance</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td><small class="text-primary"><i>2019-2020</i></small></td>
                            <td><small class="text-primary"><i>2019-2020</i></small></td>
                            <td><small class="text-primary"><i>2019-2020</i></small></td>
                            <td><small class="text-primary"><i>2019-2020</i></small></td>
                            <td><small class="text-info"><i>2018-2019</i></small></td>
                            <td><small class="text-info"><i>2018-2019</i></small></td>
                            <td><small class="text-info"><i>2018-2019</i></small></td>
                            <td colspan="2">Volume Indicators</td>
                            <td><small class="text-primary"><i>2019-2020</i></small></td>
                            <td><small class="text-primary"><i>2019-2020</i></small></td>
                            <td><small class="text-primary"><i>2019-2020</i></small></td>
                            <td><small class="text-primary"><i>2019-2020</i></small></td>
                            <td><small class="text-info"><i>2018-2019</i></small></td>
                            <td><small class="text-info"><i>2018-2019</i></small></td>
                            <td><small class="text-info"><i>2018-2019</i></small></td>
                        </tr>
                        @foreach($mors as $mor)
                            <tr>
                                <td>${{ $mor->month_actual }}</td>
                                <td>${{ $mor->month_budget }}</td>
                                <td>${{ $mor->month_variance }}</td>
                                <td>{{ $mor->month_variance_percent }}%</td>
                                <td>${{ $mor->month_prior_actual }}</td>
                                <td>${{ $mor->month_prior_variance }}</td>
                                <td>{{ $mor->month_prior_variance_percent }}%</td>
                                <td class="bg-danger"></td>
                                <td>{{ $mor->mor_description }}</td>
                                <td>${{ $mor->ytd_actual }}</td>
                                <td>${{ $mor->ytd_budget }}</td>
                                <td>${{ $mor->ytd_variance }}</td>
                                <td>{{ $mor->ytd_variance_percent }}%</td>
                                <td>${{ $mor->ytd_prior }}</td>
                                <td>${{ $mor->ytd_prior_variance }}</td>
                                <td>{{ $mor->ytd_prior_variance_percent }}%</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>


@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection

@endif