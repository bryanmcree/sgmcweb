@extends('layouts.app')

<style>
    
</style>

@if( Auth::user()->hasRole('SGMC Properties') == TRUE)

@section('content')

    <div class="card bg-dark text-white mb-3">

        <div class="card-header">
            
            <h5>
                SGMC Properties & Holdings
                <span class="float-right">
                    <img src="https://maps.gstatic.com/mapfiles/ms2/micons/blue-dot.png" alt=""> = Building & Land - Med Office
                    ||
                    <img src="https://maps.gstatic.com/mapfiles/ms2/micons/yellow-dot.png" alt=""> = Lease - Med Office
                    ||
                    <img src="https://maps.gstatic.com/mapfiles/ms2/micons/purple-dot.png" alt=""> = EMS
                    ||
                    <img src="https://maps.gstatic.com/mapfiles/ms2/micons/pink-dot.png" alt=""> = Buildings & Land - Non Med Office
                    ||
                    <img src="https://maps.gstatic.com/mapfiles/ms2/micons/red-dot.png" alt=""> = Land
                    ||
                    <img src="https://maps.gstatic.com/mapfiles/ms2/micons/green-dot.png" alt=""> = Leases - Misc
                </span>
            </h5>
            
        </div>
        
        <div class="card-body">

            <div id="map" style="height:100%;"></div>
        
            <script>
        
                var data = <?= json_encode($properties); ?>;
        
                function initMap() {
        
                    var map = new google.maps.Map(document.getElementById('map'), {
                        zoom: 10,
                        center: {lat: 30.9804658, lng: -83.2286798}
                    });

                    for (var i = 0; i < data.length; i++)
                    {
                        if(data[i].legend == 'Building & Land - Med Office')
                        {
                            var marker = new google.maps.MarkerImage("https://maps.gstatic.com/mapfiles/ms2/micons/blue-dot.png");
                        }
                        else if(data[i].legend == 'Lease - Med Office')
                        {
                            var marker = new google.maps.MarkerImage("https://maps.gstatic.com/mapfiles/ms2/micons/yellow-dot.png");
                        }
                        else if(data[i].legend == 'EMS')
                        {
                            var marker = new google.maps.MarkerImage("https://maps.gstatic.com/mapfiles/ms2/micons/purple-dot.png");
                        }
                        else if(data[i].legend == 'Buildings & Land - Non Med Office')
                        {
                            var marker = new google.maps.MarkerImage("https://maps.gstatic.com/mapfiles/ms2/micons/pink-dot.png");
                        }
                        else if(data[i].legend == 'Land')
                        {
                            var marker = new google.maps.MarkerImage("https://maps.gstatic.com/mapfiles/ms2/micons/red-dot.png");
                        }
                        else if(data[i].legend == 'Leases - Misc')
                        {
                            var marker = new google.maps.MarkerImage("https://maps.gstatic.com/mapfiles/ms2/micons/green-dot.png");
                        }

                        var marker = new google.maps.Marker({
                            position: {lat: data[i].lat, lng: data[i].long},
                            map: map,
                            icon: marker,
                            title: data[i].location + '\n' + 'Price: ' + data[i].price + '\n' + 'Description: ' + data[i].notes + '\n' + 'Legend: ' + data[i].legend
                        });
                    }
        
                }
        
            </script>
            

        </div>

    </div>


    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDDqCLn5-u1VpkILKXPs71_u-jBaLRGJy4&callback=initMap">
    </script>
    

    
@endsection

@endif