<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SGMC - Web</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">

    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
    {!! Html::style('css/navbar.css') !!}
    {!! Html::style('css/sweetalert.css') !!}
    {!! Html::style('css/TimeCircles.css') !!}
    <link rel="stylesheet" href="{!!  URL::asset('css/custom.css') !!}">


    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>

<body id="app-layout">
<div class="container-fluid">
    <div class="row">
        @if (session('msg'))
            <div class="alert alert-success">
                <ul>
                    <li><b>--> {!! session('msg') !!}</b></li>
                </ul>
            </div>
        @endif
        <div class="container-fluid">
            <div class="row">
                @yield('content')
            </div>
        </div>
    </div>
</div>
<!-- JavaScripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
<script   src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"   integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="   crossorigin="anonymous"></script>
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
{!! Html::script('js/sweetalert.min.js') !!}
{!! Html::script('js/jquery.autocomplete.js') !!}
{!! Html::script('js/jquery.searchable.js') !!}
{!! Html::script('js/TimeCircles.js') !!}
<script src="https://d3js.org/d3.v4.min.js"></script>
@include('sweet::alert')
@yield('scripts')

</body>
</html>
<script type="text/javascript">


    $(function() {
        $('#View').on("show.bs.modal", function (e) {
            $("#first_name").html($(e.relatedTarget).data('first_name'));
            $("#last_name").html($(e.relatedTarget).data('last_name'));
            $("#address").html($(e.relatedTarget).data('address'));
            $("#city").html($(e.relatedTarget).data('city'));
            $("#state").html($(e.relatedTarget).data('state'));
            $("#zip").html($(e.relatedTarget).data('zip'));
            $("#phone").html($(e.relatedTarget).data('phone'));
            $("#email").html($(e.relatedTarget).data('email'));
            $("#unit_code").html($(e.relatedTarget).data('unit_code'));
            $("#title").html($(e.relatedTarget).data('title'));
            $("#unit_description").html($(e.relatedTarget).data('unit_description'));
            $("#hire_date").html($(e.relatedTarget).data('hire_date'));
            $("#gender").html($(e.relatedTarget).data('gender'));
            $("#location").html($(e.relatedTarget).data('location'));
            $("#status").html($(e.relatedTarget).data('status'));
            $("#termination_date").html($(e.relatedTarget).data('termination_date'));
        });
    });
</script>

<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });
</script>
@if (Auth::user())
    @include("help.modal.help")
@endif

