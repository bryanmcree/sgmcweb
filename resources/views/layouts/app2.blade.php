<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<?php
$moose_talk = array('meooow','bggrrwww','rawwww','Purrrrrr','MEOW!','Buuuggguu','miaow','mrruh','prrrup','mrow','yowl','mrrrrrr','groooour');
$randomNumber = rand(0, (count($moose_talk) - 1));
?>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#288a6c">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#288a6c">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#288a6c">

    <title>SGMC - Web</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/fixedcolumns/3.2.6/css/fixedColumns.dataTables.min.css">
    <link href="{{ asset('css/dashboard.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{!!  URL::asset('css/dashboard_print.css') !!}">
    <link rel="stylesheet" href="js/minified/themes/default.min.css" id="theme-style" />


    <style>
        body {
            font-family: 'Lato';
            background-color: #F3FDFA;
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>

@include('navbars.navbar')

<div class="container-fluid">
    <div class="row">
        @if (session('msg'))
            <div class="alert alert-success">
                <ul>
                    <li><b>--> {!! session('msg') !!}</b></li>
                </ul>
            </div>
        @endif
        <div class="container-fluid">
            <div class="row">
                @yield('content')
            </div>
        </div>
    </div>
</div>
<!-- JavaScripts -->
<script
        src="https://code.jquery.com/jquery-3.3.1.js"
        integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
        crossorigin="anonymous"></script>

<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.js"></script>
<script src='https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js'></script>
<script src='https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js'></script>
{!! Html::script('js/sweetalert.min.js') !!}
{!! Html::script('js/jquery.autocomplete.js') !!}
{!! Html::script('js/jquery.searchable.js') !!}
{!! Html::script('js/TimeCircles.js') !!}
{!! Html::script('js/progressbar.js') !!}
<script src="https://d3js.org/d3.v4.min.js"></script>

<script src="js/minified/sceditor.min.js"></script>
<script src="js/minified/icons/monocons.js"></script>
<script src="js/minified/formats/bbcode.js"></script>


@include('sweet::alert')

@yield('scripts')



</html>

<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });
</script>
@if (Auth::user())
    @include("help.modal.help")
    @include("help.modal.airwatch")
@endif




