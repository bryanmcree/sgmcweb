<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<?php
$bg = array('45034226_xxl.jpg', '54495388_xxl.jpg','12751957_xxl.jpg','17185960_xxl.jpg','32597944_xxl.jpg',
    '12889008_xxl.jpg', '22175957_xxl.jpg','35802432_xxl.jpg','42068805_xxl.jpg','44701995_xxl.jpg',
    '61547667_xxl.jpg');
?>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#288a6c">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#288a6c">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#288a6c">

    <link rel="shortcut icon" href="/img/sgmc_logo_title.png"/>
    <title>SGMC - Web</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">

    <!-- Menu -->
    <link rel="stylesheet" href="/css/multi.min.css">
    <link rel="stylesheet" href="/css/carousel.css">

    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
    {!! Html::style('css/navbar.css') !!}
    {!! Html::style('css/sweetalert.css') !!}
    {!! Html::style('css/TimeCircles.css') !!}
    {{-- <link rel="stylesheet" href="{!!  URL::asset('css/custom.css') !!}"> --}}
    <link rel="stylesheet" href="/css/custom.css">

    <style>
        body {
            background-color: black;
            font-family: 'Lato';
            background-image: url('/img/background_5_test.jpg');
        }

        footer {
            background-color: black;
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>

</head>


<br>

<div class="container-fluid">
    @yield('content')
</div>

<!-- JavaScripts -->


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
<script type="text/javascript" src="//code.jquery.com/ui/1.12.1/jquery-ui.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<!-- Menu -->
<script src="/js/multi.min.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js"></script>
<!-- jQuery UI -->

{!! Html::script('js/sweetalert.min.js') !!}
{!! Html::script('js/jquery.autocomplete.js') !!}
{!! Html::script('js/jquery.searchable.js') !!}
{!! Html::script('js/TimeCircles.js') !!}
{!! Html::script('js/progressbar.js') !!}
<script src="https://d3js.org/d3.v4.min.js"></script>

@include('sweet::alert')

@yield('scripts')



</html>
<script type="text/javascript">

    //ToolTip Script
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });

    var input1 = document.getElementById('office_phone');
    input1.onkeypress = function(){
        input1.value = input1.value.replace(/[^0-9+]/g, '');
    };

    var input2 = document.getElementById('mobile_phone');
    input2.onkeypress = function(){
        input2.value = input2.value.replace(/[^0-9+]/g, '');
    };

    $(function() {
        $('#View').on("show.bs.modal", function (e) {
            $("#first_name").html($(e.relatedTarget).data('first_name'));
            $("#last_name").html($(e.relatedTarget).data('last_name'));
            $("#address").html($(e.relatedTarget).data('address'));
            $("#city").html($(e.relatedTarget).data('city'));
            $("#state").html($(e.relatedTarget).data('state'));
            $("#zip").html($(e.relatedTarget).data('zip'));
            $("#phone").html($(e.relatedTarget).data('phone'));
            $("#email").html($(e.relatedTarget).data('email'));
            $("#unit_code").html($(e.relatedTarget).data('unit_code'));
            $("#title").html($(e.relatedTarget).data('title'));
            $("#unit_description").html($(e.relatedTarget).data('unit_description'));
            $("#hire_date").html($(e.relatedTarget).data('hire_date'));
            $("#gender").html($(e.relatedTarget).data('gender'));
            $("#location").html($(e.relatedTarget).data('location'));
            $("#status").html($(e.relatedTarget).data('status'));
            $("#termination_date").html($(e.relatedTarget).data('termination_date'));
        });
    });
</script>

<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });
</script>
@if (Auth::user())
    @include("help.modal.help")
    @include("help.modal.airwatch")
@endif




