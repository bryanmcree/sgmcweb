{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('MGMA') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-8">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <b>MGMA Data</b>
            </div>
            <div class="panel-body">
                <div id="chart_div" style="width: 1100px; height: 700px;"></div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="panel panel-default">
            <div class="panel panel-heading">
                <b>Select Specialty</b>
            </div>
            <div class="panel-body">
                <form method="post" action="/mgma/specialty">
                    {{ csrf_field() }}
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                        <select name="specialty" class="form-control">
                            <option value=""></option>
                            @foreach($dropdown as $list)
                                <option value="{{ $list->Specialty }}"> {{$list->Specialty}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                {!! Form::submit('Load Specialty', ['class'=>'btn btn-sgmc btn-block', 'id'=>'AddButton']) !!}
                </form>
                <i>*Specialties with no values are removed from list.</i>
            </div>
        </div>
    </div>

    <div class="col-lg-4">
        <div class="panel panel-primary">
            <div class="panel panel-heading">
                <b>Formula</b>
            </div>
            <div class="panel-body">
                <form method="post" action="/mgma/formula">
                    <input type="hidden" name="specialty" value="{{$data->Specialty}}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        {!! Form::textarea('formula', $data->formula, ['class' => 'form-control', 'id'=>'formula', 'rows' => 5]) !!}
                    </div>
                    {!! Form::submit('Update Formula', ['class'=>'btn btn-sgmc btn-block', 'id'=>'AddButton']) !!}
                </form>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        @if (session('status'))
            <div class="alert alert-success">
                <b>{{ session('status') }}</b>
            </div>
        @endif
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">

        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Cost', 'Percent'],
                [ 10,      {{$data->TEN}}],
                [ 25,      {{$data->TWENTY}}],
                [ 50,      {{$data->FIFTY}}],
                [ 75,      {{$data->SEVENTY}}],
                [ 90,      {{$data->NINETY}}],

            ]);

            var options = {
                title: '{{$data->Specialty}}',
                vAxis: {title: 'Cost', minValue: 0, maxValue: 15},
                hAxis: {title: 'Percent', minValue: 0, maxValue: 15},
                //legend: 'bottom',
                crosshair: { trigger: "both", orientation: "both" },
                trendlines: {
                    0: {
                        type: 'polynomial',
                        degree: 4,
                        visibleInLegend: true,
                        showR2: true,
                        color: 'green',
                        opacity: 0.3
                    }
                }
            };

            var chart = new google.visualization.ScatterChart(document.getElementById('chart_div'));

            chart.draw(data, options);
        }
    </script>

@endsection
@endif