{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Security') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1

    --}}
@section('content')

    <form method="post" action="/security/assign">
        {{ csrf_field() }}
        <div class="mb-4 border-info text-white bg-dark">
            <div class="card-header">
                Blank Template
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="card-deck">
                        <div class="card text-white bg-dark">
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Select Cost Center</label>
                                <select class="form-control" name="cost_center" id="exampleFormControlSelect1">
                                    <option>[Select]</option>
                                    @foreach($costcenters as $cc)
                                        <option value="{{$cc->cost_center}}">{{$cc->style1}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="card text-white bg-dark">
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Select Role</label>
                                <select class="form-control" name="role" id="exampleFormControlSelect1">
                                    <option>[Select]</option>
                                    @foreach($roles as $role)
                                        <option value="{{$role->id}}">{{$role->name}} - {{$role->label}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="submit" value="Assign Role to All Employees in Cost Center" class="btn-primary btn-sm">
        </div>
    </form>



@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
@endif