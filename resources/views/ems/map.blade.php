{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('EMS') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}


<style>
    #map {
        height: 100%;
    }
    /* Optional: Makes the sample page fill the window. */
    html, body {
        height: 100%;
        margin: 0;
        padding: 0;
    }
    #floating-panel {
        position: absolute;
        top: 10px;
        left: 25%;
        z-index: 5;
        background-color: #fff;
        padding: 5px;
        border: 1px solid #999;
        text-align: center;
        font-family: 'Roboto','sans-serif';
        line-height: 30px;
        padding-left: 10px;
    }
</style>

{{-- geometry.bounds.na.l == lat /// geometry.bounds.ia.l == long --}}

@section('content')


    <div id="map"></div>

    <script>

        var coords = <?= json_encode($coords); ?>;

        function initMap() {

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 12,
                center: {lat: 30.831048, lng: -83.278934}
            });

            var pinImage = new google.maps.MarkerImage("https://storage.googleapis.com/support-kms-prod/SNP_2752125_en_v0",
                new google.maps.Size(14, 23),
                new google.maps.Point(0,0),
                new google.maps.Point(5, 17));

            for (var i = 0; i < coords.length; i++)
            {            
                var marker = new google.maps.Marker({
                    position: {lat: coords[i].lat, lng: coords[i].long},
                    map: map,
                    title: coords[i].incident_address,
                    icon: pinImage
                });
            }

        }

    </script>

    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDDqCLn5-u1VpkILKXPs71_u-jBaLRGJy4&callback=initMap">
    </script>

    
@endsection


@section('scripts')



@endsection
@endif
