{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('EMS') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="card bg-dark text-white mb-3">
            <div class="card-header">
                <h4><b>EMS Dashboard</b></h4>
            </div>
            <div class="card-body">

                <ul class="nav nav-tabs mb-3">
                    <li class="nav-item"><a data-toggle="pill" class="nav-link active" href="#home">Home</a></li>
                    <li class="nav-item"><a data-toggle="pill" class="nav-link" href="#menu1">Lowndes</a></li>
                    <li class="nav-item"><a data-toggle="pill" class="nav-link" href="#menu2">Lanier</a></li>
                    <li class="nav-item"><a data-toggle="pill" class="nav-link" href="#menu3">Echols</a></li>
                    <li class="nav-item"><a data-toggle="pill" class="nav-link" href="#menu4">All Counties</a></li>
                    {{-- <li class="nav-item"><a data-toggle="pill" class="nav-link" href="#menu6">Table View</a></li> --}}
                    <li class="nav-item"> 
                        <a class="nav-link" id="history-tab" data-toggle="modal" href="#emsReport" role="tab" aria-controls="history" aria-selected="false">Generate Report</a>
                    </li>
                    <li class="nav-item"> 
                        <a class="nav-link" id="map-tab" href="/ems/map" role="tab" aria-controls="map" aria-selected="false">Map View</a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div id="home" class="tab-pane in active">

                        <div class="card bg-secondary text-white mb-3">
                            <div class="card-header">
                                <b>Total Calls</b>
                            </div>
                            <div class="card-body">
                                <canvas id="chart_by_month" height="50"></canvas>
                            </div>
                        </div>

                        <div class="card bg-secondary text-white mb-3">
                            <div class="card-header">
                                Total Number of Times Level 0 was Reached
                            </div>
                            <div class="card-body">
                                <canvas id="zeroChart" height="50"></canvas>
                            </div>
                            <div class="card-footer">
                                <span class="float-right"><a href="/ems/zero/show" class="btn btn-primary btn-sm">Manage Level Zero Entry's</a></span>
                            </div>
                        </div>

                        <div class="card-deck mb-3">

                            <div class="card" style="background-color:#39cccc">
                                <div class="card-header">
                                    Total 911 EMS Responses
                                </div>
                                <div class="card-body">
                                    <table class="table table-dark table-hover table-bordered table-striped">
                                        <thead>
                                            <th>Date</th>
                                            <th class="text-center">Total</th>
                                            <th class="text-right">Avg</th>
                                        </thead>
                                        <tbody>
                                            @foreach($by_month as $total_monthly)
                                                <tr>
                                                    <td>{{ $total_monthly->call_month_name }}, {{ $total_monthly->call_year }} </td>
                                                    <td class="text-center">{{ $total_monthly->total }}</td>
                                                    <td class="text-right">{{ number_format($total_monthly->total / Carbon::parse('1 ' . $transported->first()->call_month_name)->daysInMonth, 2) }}/day</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            {{-- <div class="card" style="background-color:#39cccc">
                                <div class="card-header">
                                    Total 911 Patients Responded To
                                </div>
                                <div class="card-body">
                                    <table class="table table-dark table-hover table-bordered table-striped">
                                        <thead>
                                            <th>Month</th>
                                            <th class="text-center">Total</th>
                                            <th class="text-right">Avg</th>
                                        </thead>
                                        <tbody>
                                            @foreach($responded_to as $total_responded)
                                                <tr>
                                                    <td>{{ $total_responded->call_month_name }}</td>
                                                    <td class="text-center">{{ $total_responded->total }}</td>
                                                    <td class="text-right">{{ number_format($total_responded->total / Carbon::parse('1 ' . $transported->first()->call_month_name)->daysInMonth, 2) }}/day</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div> --}}

                            <div class="card" style="background-color:#39cccc">
                                <div class="card-header">
                                    Total 911 Patients Transported
                                </div>
                                <div class="card-body">
                                    <table class="table table-dark table-hover table-bordered table-striped">
                                        <thead>
                                            <th>Date</th>
                                            <th class="text-center">Total</th>
                                            <th class="text-right">Avg</th>
                                        </thead>
                                        <tbody>
                                            @foreach($transported as $total_transported)
                                                <tr>
                                                    <td>{{ $total_transported->call_month_name }}, {{ $total_transported->call_year }} </td>
                                                    <td class="text-center">{{ $total_transported->total }}</td>
                                                    <td class="text-right">{{ number_format($total_transported->total / Carbon::parse('1 ' . $transported->first()->call_month_name)->daysInMonth, 2) }}/day</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>

                        <div class="card bg-secondary text-white mb-3">
                            <div class="card-header">
                                NET Data 
                                <br>
                                <small class="text-warning"><i>last 3 months</i></small>
                            </div>
                            <div class="card-body">
                                
                                <div class="row">
                                    <div class="col-lg-5">

                                        <table class="table table-dark table-hover table-striped table-bordered text-center" id="lanier_NET">
                                            <thead>
                                                <tr>
                                                    <th colspan="3">6239 Lanier NET Transports</th>
                                                </tr>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Wheelchair</th>
                                                    <th>Stretcher</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @forelse($NETs as $NET) 
                                                    <tr>
                                                        <td>{{ Carbon::parse($NET->date)->format('m-d-Y') }}</td>
                                                        <td>{{ $NET->wheelchair }}</td>
                                                        <td>{{ $NET->stretcher }}</td>
                                                    </tr>
                                                @empty
                                                    <tr>
                                                        <td colspan="3">No Data Available</td>
                                                    </tr>
                                                @endforelse
                                            </tbody>
                                        </table>

                                    </div>

                                    <div class="col-lg-7">

                                        <table class="table table-dark table-hover table-striped table-bordered text-center" id="lowndes_NET">
                                            <thead>
                                                <tr>
                                                    <th colspan="5">6235 NET Transports</th>
                                                </tr>
                                                <tr>
                                                    <th>Under 50 miles</th>
                                                    <th>Hospice</th>
                                                    <th>Over 50 miles</th>
                                                    <th>Outsourced</th>
                                                    <th>Discharged to different SGMC Campus</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @forelse($NETs as $NET) 
                                                    <tr>
                                                        <td>{{ $NET->under_50 }}</td>
                                                        <td>{{ $NET->hospice }}</td>
                                                        <td>{{ $NET->over_50 }}</td>
                                                        <td>{{ $NET->punts }}</td>
                                                        <td>{{ $NET->diff_campus }}</td>
                                                    </tr>
                                                @empty
                                                    <tr>
                                                        <td colspan="5">No Data Available</td>
                                                    </tr>
                                                @endforelse
                                            </tbody>
                                        </table>

                                    </div>
                                </div>

                            </div>
                            <div class="card-footer">
                                <span class="float-right"><a href="/ems/NET/show" class="btn btn-primary btn-sm">Manage NET Entry's</a></span>
                            </div>
                        </div>


                        @if(!$not_classified->isEmpty())
                        <div class="col-md-12">
                            <div class="card bg-dark border-danger text-white mb-3">
                                <div class="card-header">
                                    <b>Unclassified Records</b> ({{$not_classified->count()}})
                                </div>
                                <div class="card-body">
                                    <table class="table table-hover table-condensed">
                                        <thead>
                                        <tr>
                                            <td><b>Incident #</b></td>
                                            <td><b>Incident Key</b></td>
                                            <td><b>Dispatch Time</b></td>
                                            <td><b>Disposition</b></td>
                                            <td><b>Call Sign</b></td>
                                            <td><b>Shift</b></td>
                                            <td><b>County</b></td>
                                            <td></td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($not_classified as $not_classified_var)
                                            <tr>
                                                <td style="vertical-align:top">{{$not_classified_var->incident_number}}</td>
                                                <td style="vertical-align:top">{{$not_classified_var->incident_key}}</td>
                                                <td style="vertical-align:top">{{$not_classified_var->dispatch_notified_time}}</td>
                                                <td style="vertical-align:top">{{$not_classified_var->patient_disposition}}</td>
                                                <td style="vertical-align:top">{{$not_classified_var->call_sign}}</td>
                                                <td style="vertical-align:top">{{$not_classified_var->shift_number}}</td>
                                                <td style="vertical-align:top">{{$not_classified_var->county}}</td>
                                                <td align="right"><a class="btn btn-default btn-xs EditUnclassified"

                                                                     href="#" data-toggle="modal"
                                                                     data-id="{{$not_classified_var->id}}"
                                                                     data-incident_number="{{$not_classified_var->incident_number}}"

                                                    >Fix</a></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>

                    <div id="menu1" class="tab-pane fade">
                        <br>
                        <div class="card bg-secondary text-white mb-3">
                            <div class="card-header">
                                <b>Lowndes County</b>
                            </div>
                            <div class="card-body">
                                @if($lowndes_zone1->isEmpty())
                                    <div class="alert alert-info">No data for last month.</div>
                                @Else

                                <div class="card bg-dark text-white mb-3">
                                    <div class="card-header">
                                        <b>Zone Totals for All Squads in Lowndes County</b>
                                    </div>
                                    <div class="card-body">
                                        <table class="table table-hover table-condensed">
                                            <thead>
                                                    <td><b></b></td>
                                                    <td align="center"><b>Zone 1</b></td>
                                                    <td align="center"><b>Zone 2</b></td>
                                                    <td align="center"><b>Zone 3</b></td>
                                                    <td align="center"><b>Zone 4</b></td>
                                                    <td align="center"><b>Zone 5</b></td>
                                                    <td align="center"><b>Zone 6</b></td>
                                                    <td align="center"><b>AVG/Total</b></td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td><b>Average Miles Out</b></td>
                                                    <td align="center">{{number_format($lowndes_zone_totals ->where('incident_zone','1')->first()->miles_out,1)}}</td>
                                                    <td align="center">{{number_format($lowndes_zone_totals ->where('incident_zone','2')->first()->miles_out,1)}}</td>
                                                    <td align="center">@if(!$lowndes_zone_totals ->where('incident_zone','3')->first()) 0 @else {{number_format($lowndes_zone_totals ->where('incident_zone','3')->first()->miles_out,1)}} @endif</td>
                                                    <td align="center">{{number_format($lowndes_zone_totals ->where('incident_zone','4')->first()->miles_out,1)}}</td>
                                                    <td align="center">@if(!$lowndes_zone_totals ->where('incident_zone','5')->first()) 0 @else {{number_format($lowndes_zone_totals ->where('incident_zone','5')->first()->miles_out,1)}} @endif</td>
                                                    <td align="center">@if(!$lowndes_zone_totals ->where('incident_zone','6')->first()) 0 @else {{number_format($lowndes_zone_totals ->where('incident_zone','6')->first()->miles_out,1)}} @endif</td>
                                                    <td align="center">{{number_format($lowndes_zone_totals ->avg('miles_out'),1)}}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Average Response Time</b></td>
                                                    <td align="center">{{$lowndes_zone_totals->where('incident_zone','1')->first()->response_time}}</td>
                                                    <td align="center">{{$lowndes_zone_totals ->where('incident_zone','2')->first()->response_time}}</td>
                                                    <td align="center">@if(!$lowndes_zone_totals ->where('incident_zone','3')->first()) 0 @else {{number_format($lowndes_zone_totals ->where('incident_zone','3')->first()->response_time,1)}} @endif</td>
                                                    <td align="center">{{$lowndes_zone_totals ->where('incident_zone','4')->first()->response_time}}</td>
                                                    <td align="center">@if(!$lowndes_zone_totals ->where('incident_zone','5')->first()) 0 @else {{$lowndes_zone_totals ->where('incident_zone','5')->first()->response_time}} @endif</td>
                                                    <td align="center">@if(!$lowndes_zone_totals ->where('incident_zone','6')->first()) 0 @else {{$lowndes_zone_totals ->where('incident_zone','6')->first()->response_time}} @endif</td>
                                                    <td align="center">{{number_format($lowndes_zone_totals ->avg('response_time'),1)}}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Average Time Out of Service</b></td>
                                                    <td align="center">{{$lowndes_zone_totals ->where('incident_zone','1')->first()->out_service}}</td>
                                                    <td align="center">{{$lowndes_zone_totals ->where('incident_zone','2')->first()->out_service}}</td>
                                                    <td align="center">@if(!$lowndes_zone_totals ->where('incident_zone','3')->first()) 0 @else {{number_format($lowndes_zone_totals ->where('incident_zone','3')->first()->out_service,1)}} @endif</td>
                                                    <td align="center">{{$lowndes_zone_totals ->where('incident_zone','4')->first()->out_service}}</td>
                                                    <td align="center">@if(!$lowndes_zone_totals ->where('incident_zone','5')->first()) 0 @else {{$lowndes_zone_totals ->where('incident_zone','5')->first()->out_service}} @endif</td>
                                                    <td align="center">@if(!$lowndes_zone_totals ->where('incident_zone','6')->first()) 0 @else {{$lowndes_zone_totals ->where('incident_zone','6')->first()->out_service}} @endif</td>
                                                    <td align="center">{{number_format($lowndes_zone_totals ->avg('out_service'),1)}}</td>
                                                </tr>
                                                <tr class="bg-success">
                                                    <td><b>Total # of Calls</b></td>
                                                    <td align="center">{{$lowndes_zone_totals ->where('incident_zone','1')->first()->total}}</td>
                                                    <td align="center">{{$lowndes_zone_totals ->where('incident_zone','2')->first()->total}}</td>
                                                    <td align="center">@if(!$lowndes_zone_totals ->where('incident_zone','3')->first()) 0 @else {{ $lowndes_squad_totals ->where('call_sign','SQUAD 3')->first()->total }} @endif</td>
                                                    <td align="center">{{$lowndes_zone_totals ->where('incident_zone','4')->first()->total}}</td>
                                                    <td align="center">{{$lowndes_zone_totals ->where('incident_zone','5')->first()->total}}</td>
                                                    <td align="center">{{$lowndes_zone_totals ->where('incident_zone','6')->first()->total}}</td>
                                                    <td align="center">{{$lowndes_zone_totals->sum('total')}}</td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                            <td colspan="7"><i>* Data calculated from previous month. ({{ Carbon::parse($most_recent->dispatch_notified_time)->format('m-Y') }})</i></td>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>

                                <div class="card bg-dark text-white mb-3">
                                    <div class="card-header">
                                        <b>Zone 1</b>
                                    </div>
                                    <div class="card-body">
                                        <table class="table table-hover table-condensed">
                                            <thead>
                                                <tr>
                                                    <td><b></b></td>
                                                    <td align="center"><b>Squad 1</b></td>
                                                    <td align="center"><b>Squad 2</b></td>
                                                    <td align="center"><b>Squad 3</b></td>
                                                    <td align="center"><b>Squad 4</b></td>
                                                    <td align="center"><b>Squad 5</b></td>
                                                    <td align="center"><b>Squad 6</b></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><b>Average Miles Out</b></td>
                                                    <td align="center">@if(!$lowndes_zone1 ->where('call_sign','SQUAD 1')->first()) 0 @else {{number_format($lowndes_zone1 ->where('call_sign','SQUAD 1')->first()->miles_out,1)}}@endif</td>
                                                    <td align="center">@if(!$lowndes_zone1 ->where('call_sign','SQUAD 2')->first()) 0 @else {{number_format($lowndes_zone1 ->where('call_sign','SQUAD 2')->first()->miles_out,1)}}@endif</td>
                                                    <td align="center">@if(!$lowndes_zone1 ->where('call_sign','SQUAD 3')->first()) 0 @else {{number_format($lowndes_zone1 ->where('call_sign','SQUAD 3')->first()->miles_out,1)}}@endif</td>
                                                    <td align="center">@if(!$lowndes_zone1 ->where('call_sign','SQUAD 4')->first()) 0 @else {{number_format($lowndes_zone1 ->where('call_sign','SQUAD 4')->first()->miles_out,1)}}@endif</td>
                                                    <td align="center">@if(!$lowndes_zone1 ->where('call_sign','SQUAD 5')->first()) 0 @else {{number_format($lowndes_zone1 ->where('call_sign','SQUAD 5')->first()->miles_out,1)}}@endif</td>
                                                    <td align="center">@if(!$lowndes_zone1 ->where('call_sign','SQUAD 6')->first()) 0 @else {{number_format($lowndes_zone1 ->where('call_sign','SQUAD 6')->first()->miles_out,1)}}@endif</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Average Response Time</b></td>
                                                    <td align="center">@if(!$lowndes_zone1 ->where('call_sign','SQUAD 1')->first()) 0 @else {{$lowndes_zone1 ->where('call_sign','SQUAD 1')->first()->response_time}}@endif</td>
                                                    <td align="center">@if(!$lowndes_zone1 ->where('call_sign','SQUAD 2')->first()) 0 @else {{$lowndes_zone1 ->where('call_sign','SQUAD 2')->first()->response_time}}@endif</td>
                                                    <td align="center">@if(!$lowndes_zone1 ->where('call_sign','SQUAD 3')->first()) 0 @else {{$lowndes_zone1 ->where('call_sign','SQUAD 3')->first()->response_time}}@endif</td>
                                                    <td align="center">@if(!$lowndes_zone1 ->where('call_sign','SQUAD 4')->first()) 0 @else {{$lowndes_zone1 ->where('call_sign','SQUAD 4')->first()->response_time}}@endif</td>
                                                    <td align="center">@if(!$lowndes_zone1 ->where('call_sign','SQUAD 5')->first()) 0 @else {{$lowndes_zone1 ->where('call_sign','SQUAD 5')->first()->response_time}}@endif</td>
                                                    <td align="center">@if(!$lowndes_zone1 ->where('call_sign','SQUAD 6')->first()) 0 @else {{$lowndes_zone1 ->where('call_sign','SQUAD 6')->first()->response_time}}@endif</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Average Time Out of Service</b></td>
                                                    <td align="center">@if(!$lowndes_zone1 ->where('call_sign','SQUAD 1')->first()) 0 @else {{$lowndes_zone1 ->where('call_sign','SQUAD 1')->first()->out_service}}@endif</td>
                                                    <td align="center">@if(!$lowndes_zone1 ->where('call_sign','SQUAD 2')->first()) 0 @else {{$lowndes_zone1 ->where('call_sign','SQUAD 2')->first()->out_service}}@endif</td>
                                                    <td align="center">@if(!$lowndes_zone1 ->where('call_sign','SQUAD 3')->first()) 0 @else {{$lowndes_zone1 ->where('call_sign','SQUAD 3')->first()->out_service}}@endif</td>
                                                    <td align="center">@if(!$lowndes_zone1 ->where('call_sign','SQUAD 4')->first()) 0 @else {{$lowndes_zone1 ->where('call_sign','SQUAD 4')->first()->out_service}}@endif</td>
                                                    <td align="center">@if(!$lowndes_zone1 ->where('call_sign','SQUAD 5')->first()) 0 @else {{$lowndes_zone1 ->where('call_sign','SQUAD 5')->first()->out_service}}@endif</td>
                                                    <td align="center">@if(!$lowndes_zone1 ->where('call_sign','SQUAD 6')->first()) 0 @else {{$lowndes_zone1 ->where('call_sign','SQUAD 6')->first()->out_service}}@endif</td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <td colspan="7"><i>* Data calculated from previous month. ({{ Carbon::parse($most_recent->dispatch_notified_time)->format('m-Y') }})</i></td>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <div class="card bg-dark text-white mb-3">
                                    <div class="card-header">
                                        <b>Zone 2</b>
                                    </div>
                                    <div class="card-body">
                                        <table class="table table-hover table-condensed">
                                            <thead>
                                                <tr>
                                                    <td><b></b></td>
                                                    <td align="center"><b>Squad 1</b></td>
                                                    <td align="center"><b>Squad 2</b></td>
                                                    <td align="center"><b>Squad 3</b></td>
                                                    <td align="center"><b>Squad 4</b></td>
                                                    <td align="center"><b>Squad 5</b></td>
                                                    <td align="center"><b>Squad 6</b></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><b>Average Miles Out</b></td>
                                                    <td align="center">@if(!$lowndes_zone2 ->where('call_sign','SQUAD 1')->first()) 0 @else {{number_format($lowndes_zone2 ->where('call_sign','SQUAD 1')->first()->miles_out,1)}} @endif</td>
                                                    <td align="center">@if(!$lowndes_zone2 ->where('call_sign','SQUAD 2')->first()) 0 @else {{number_format($lowndes_zone2 ->where('call_sign','SQUAD 2')->first()->miles_out,1)}} @endif</td>
                                                    <td align="center">@if(!$lowndes_zone2 ->where('call_sign','SQUAD 3')->first()) 0 @else {{number_format($lowndes_zone2 ->where('call_sign','SQUAD 3')->first()->miles_out,1)}} @endif</td>
                                                    <td align="center">@if(!$lowndes_zone2 ->where('call_sign','SQUAD 4')->first()) 0 @else {{number_format($lowndes_zone2 ->where('call_sign','SQUAD 4')->first()->miles_out,1)}} @endif</td>
                                                    <td align="center">@if(!$lowndes_zone2 ->where('call_sign','SQUAD 5')->first()) 0 @else {{number_format($lowndes_zone2 ->where('call_sign','SQUAD 5')->first()->miles_out,1)}} @endif</td>
                                                    <td align="center">@if(!$lowndes_zone2 ->where('call_sign','SQUAD 6')->first()) 0 @else {{number_format($lowndes_zone2 ->where('call_sign','SQUAD 6')->first()->miles_out,1)}} @endif</td>
                                                    </tr>
                                                <tr>
                                                    <td><b>Average Response Time</b></td>
                                                    <td align="center">@if(!$lowndes_zone2 ->where('call_sign','SQUAD 1')->first()) 0 @else {{$lowndes_zone2 ->where('call_sign','SQUAD 1')->first()->response_time}} @endif</td>
                                                    <td align="center">@if(!$lowndes_zone2 ->where('call_sign','SQUAD 2')->first()) 0 @else {{$lowndes_zone2 ->where('call_sign','SQUAD 2')->first()->response_time}} @endif</td>
                                                    <td align="center">@if(!$lowndes_zone2 ->where('call_sign','SQUAD 3')->first()) 0 @else {{$lowndes_zone2 ->where('call_sign','SQUAD 3')->first()->response_time}} @endif</td>
                                                    <td align="center">@if(!$lowndes_zone2 ->where('call_sign','SQUAD 4')->first()) 0 @else {{$lowndes_zone2 ->where('call_sign','SQUAD 4')->first()->response_time}} @endif</td>
                                                    <td align="center">@if(!$lowndes_zone2 ->where('call_sign','SQUAD 5')->first()) 0 @else {{$lowndes_zone2 ->where('call_sign','SQUAD 5')->first()->response_time}} @endif</td>
                                                    <td align="center">@if(!$lowndes_zone2 ->where('call_sign','SQUAD 6')->first()) 0 @else {{$lowndes_zone2 ->where('call_sign','SQUAD 6')->first()->response_time}} @endif</td>
                                                    </tr>
                                                <tr>
                                                    <td><b>Average Time Out of Service</b></td>
                                                    <td align="center">@if(!$lowndes_zone2 ->where('call_sign','SQUAD 1')->first()) 0 @else {{$lowndes_zone2 ->where('call_sign','SQUAD 1')->first()->out_service}} @endif</td>
                                                    <td align="center">@if(!$lowndes_zone2 ->where('call_sign','SQUAD 2')->first()) 0 @else {{$lowndes_zone2 ->where('call_sign','SQUAD 2')->first()->out_service}} @endif</td>
                                                    <td align="center">@if(!$lowndes_zone2 ->where('call_sign','SQUAD 3')->first()) 0 @else {{$lowndes_zone2 ->where('call_sign','SQUAD 3')->first()->out_service}} @endif</td>
                                                    <td align="center">@if(!$lowndes_zone2 ->where('call_sign','SQUAD 4')->first()) 0 @else {{$lowndes_zone2 ->where('call_sign','SQUAD 4')->first()->out_service}} @endif</td>
                                                    <td align="center">@if(!$lowndes_zone2 ->where('call_sign','SQUAD 5')->first()) 0 @else {{$lowndes_zone2 ->where('call_sign','SQUAD 5')->first()->out_service}} @endif</td>
                                                    <td align="center">@if(!$lowndes_zone2 ->where('call_sign','SQUAD 6')->first()) 0 @else {{$lowndes_zone2 ->where('call_sign','SQUAD 6')->first()->out_service}} @endif</td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                            <td colspan="7"><i>* Data calculated from previous month. ({{ Carbon::parse($most_recent->dispatch_notified_time)->format('m-Y') }})</i></td>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <div class="card bg-dark text-white mb-3">
                                    <div class="card-header">
                                        <b>Zone 3</b>
                                    </div>
                                    <div class="card-body">
                                        <table class="table table-hover table-condensed">
                                            <thead>
                                            <tr>
                                                <td><b></b></td>
                                                <td align="center"><b>Squad 1</b></td>
                                                <td align="center"><b>Squad 2</b></td>
                                                <td align="center"><b>Squad 3</b></td>
                                                <td align="center"><b>Squad 4</b></td>
                                                <td align="center"><b>Squad 5</b></td>
                                                <td align="center"><b>Squad 6</b></td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td><b>Average Miles Out</b></td>
                                                <td align="center">@if(!$lowndes_zone3 ->where('call_sign','SQUAD 1')->first()) 0 @else {{number_format($lowndes_zone3 ->where('call_sign','SQUAD 1')->first()->miles_out,1)}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone3 ->where('call_sign','SQUAD 2')->first()) 0 @else {{number_format($lowndes_zone3 ->where('call_sign','SQUAD 2')->first()->miles_out,1)}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone3 ->where('call_sign','SQUAD 3')->first()) 0 @else {{number_format($lowndes_zone3 ->where('call_sign','SQUAD 3')->first()->miles_out,1)}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone3 ->where('call_sign','SQUAD 4')->first()) 0 @else {{number_format($lowndes_zone3 ->where('call_sign','SQUAD 4')->first()->miles_out,1)}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone3 ->where('call_sign','SQUAD 5')->first()) 0 @else {{number_format($lowndes_zone3 ->where('call_sign','SQUAD 5')->first()->miles_out,1)}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone3 ->where('call_sign','SQUAD 6')->first()) 0 @else {{number_format($lowndes_zone3 ->where('call_sign','SQUAD 6')->first()->miles_out,1)}} @endif</td>
                                               </tr>
                                            <tr>
                                                <td><b>Average Response Time</b></td>
                                                <td align="center">@if(!$lowndes_zone3 ->where('call_sign','SQUAD 1')->first()) 0 @else {{$lowndes_zone3 ->where('call_sign','SQUAD 1')->first()->response_time}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone3 ->where('call_sign','SQUAD 2')->first()) 0 @else {{$lowndes_zone3 ->where('call_sign','SQUAD 2')->first()->response_time}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone3 ->where('call_sign','SQUAD 3')->first()) 0 @else {{$lowndes_zone3 ->where('call_sign','SQUAD 3')->first()->response_time}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone3 ->where('call_sign','SQUAD 4')->first()) 0 @else {{$lowndes_zone3 ->where('call_sign','SQUAD 4')->first()->response_time}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone3 ->where('call_sign','SQUAD 5')->first()) 0 @else {{$lowndes_zone3 ->where('call_sign','SQUAD 5')->first()->response_time}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone3 ->where('call_sign','SQUAD 6')->first()) 0 @else {{$lowndes_zone3 ->where('call_sign','SQUAD 6')->first()->response_time}} @endif</td>
                                               </tr>
                                            <tr>
                                                <td><b>Average Time Out of Service</b></td>
                                                <td align="center">@if(!$lowndes_zone3 ->where('call_sign','SQUAD 1')->first()) 0 @else {{$lowndes_zone3 ->where('call_sign','SQUAD 1')->first()->out_service}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone3 ->where('call_sign','SQUAD 2')->first()) 0 @else {{$lowndes_zone3 ->where('call_sign','SQUAD 2')->first()->out_service}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone3 ->where('call_sign','SQUAD 3')->first()) 0 @else {{$lowndes_zone3 ->where('call_sign','SQUAD 3')->first()->out_service}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone3 ->where('call_sign','SQUAD 4')->first()) 0 @else {{$lowndes_zone3 ->where('call_sign','SQUAD 4')->first()->out_service}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone3 ->where('call_sign','SQUAD 5')->first()) 0 @else {{$lowndes_zone3 ->where('call_sign','SQUAD 5')->first()->out_service}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone3 ->where('call_sign','SQUAD 6')->first()) 0 @else {{$lowndes_zone3 ->where('call_sign','SQUAD 6')->first()->out_service}} @endif</td>
                                            </tr>
                                            </tbody>
                                            <tfoot>
                                            <td colspan="7"><i>* Data calculated from previous month. ({{ Carbon::parse($most_recent->dispatch_notified_time)->format('m-Y') }})</i></td>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <div class="card bg-dark text-white mb-3">
                                    <div class="card-header">
                                        <b>Zone 4</b>
                                    </div>
                                    <div class="card-body">
                                        <table class="table table-hover table-condensed">
                                            <thead>
                                            <tr>
                                                <td><b></b></td>
                                                <td align="center"><b>Squad 1</b></td>
                                                <td align="center"><b>Squad 2</b></td>
                                                <td align="center"><b>Squad 3</b></td>
                                                <td align="center"><b>Squad 4</b></td>
                                                <td align="center"><b>Squad 5</b></td>
                                                <td align="center"><b>Squad 6</b></td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td><b>Average Miles Out</b></td>
                                                <td align="center">@if(!$lowndes_zone4 ->where('call_sign','SQUAD 1')->first()) 0 @else {{number_format($lowndes_zone4 ->where('call_sign','SQUAD 1')->first()->miles_out,1)}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone4 ->where('call_sign','SQUAD 2')->first()) 0 @else {{number_format($lowndes_zone4 ->where('call_sign','SQUAD 2')->first()->miles_out,1)}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone4 ->where('call_sign','SQUAD 3')->first()) 0 @else {{number_format($lowndes_zone4 ->where('call_sign','SQUAD 3')->first()->miles_out,1)}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone4 ->where('call_sign','SQUAD 4')->first()) 0 @else {{number_format($lowndes_zone4 ->where('call_sign','SQUAD 4')->first()->miles_out,1)}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone4 ->where('call_sign','SQUAD 5')->first()) 0 @else {{number_format($lowndes_zone4 ->where('call_sign','SQUAD 5')->first()->miles_out,1)}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone4 ->where('call_sign','SQUAD 6')->first()) 0 @else {{number_format($lowndes_zone4 ->where('call_sign','SQUAD 6')->first()->miles_out,1)}} @endif</td>
                                                </tr>
                                            <tr>
                                                <td><b>Average Response Time</b></td>
                                                <td align="center">@if(!$lowndes_zone4 ->where('call_sign','SQUAD 1')->first()) 0 @else {{$lowndes_zone4 ->where('call_sign','SQUAD 1')->first()->response_time}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone4 ->where('call_sign','SQUAD 2')->first()) 0 @else {{$lowndes_zone4 ->where('call_sign','SQUAD 2')->first()->response_time}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone4 ->where('call_sign','SQUAD 3')->first()) 0 @else {{number_format($lowndes_zone4 ->where('call_sign','SQUAD 3')->first()->response_time,1)}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone4 ->where('call_sign','SQUAD 4')->first()) 0 @else {{$lowndes_zone4 ->where('call_sign','SQUAD 4')->first()->response_time}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone4 ->where('call_sign','SQUAD 5')->first()) 0 @else {{$lowndes_zone4 ->where('call_sign','SQUAD 5')->first()->response_time}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone4 ->where('call_sign','SQUAD 6')->first()) 0 @else {{$lowndes_zone4 ->where('call_sign','SQUAD 6')->first()->response_time}} @endif</td>
                                                </tr>
                                            <tr>
                                                <td><b>Average Time Out of Service</b></td>
                                                <td align="center">@if(!$lowndes_zone4 ->where('call_sign','SQUAD 1')->first()) 0 @else {{$lowndes_zone4 ->where('call_sign','SQUAD 1')->first()->out_service}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone4 ->where('call_sign','SQUAD 2')->first()) 0 @else {{$lowndes_zone4 ->where('call_sign','SQUAD 2')->first()->out_service}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone4 ->where('call_sign','SQUAD 3')->first()) 0 @else {{number_format($lowndes_zone4 ->where('call_sign','SQUAD 3')->first()->out_service,1)}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone4 ->where('call_sign','SQUAD 4')->first()) 0 @else {{$lowndes_zone4 ->where('call_sign','SQUAD 4')->first()->out_service}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone4 ->where('call_sign','SQUAD 5')->first()) 0 @else {{$lowndes_zone4 ->where('call_sign','SQUAD 5')->first()->out_service}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone4 ->where('call_sign','SQUAD 6')->first()) 0 @else {{$lowndes_zone4 ->where('call_sign','SQUAD 6')->first()->out_service}} @endif</td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                            <td colspan="7"><i>* Data calculated from previous month. ({{ Carbon::parse($most_recent->dispatch_notified_time)->format('m-Y') }})</i></td>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <div class="card bg-dark text-white mb-3">
                                    <div class="card-header">
                                        <b>Zone 5</b>
                                    </div>
                                    <div class="card-body">
                                        <table class="table table-hover table-condensed">
                                            <thead>
                                            <tr>
                                                <td><b></b></td>
                                                <td align="center"><b>Squad 1</b></td>
                                                <td align="center"><b>Squad 2</b></td>
                                                <td align="center"><b>Squad 3</b></td>
                                                <td align="center"><b>Squad 4</b></td>
                                                <td align="center"><b>Squad 5</b></td>
                                                <td align="center"><b>Squad 6</b></td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td><b>Average Miles Out</b></td>
                                                <td align="center">@if(!$lowndes_zone5 ->where('call_sign','SQUAD 1')->first()) 0 @else {{number_format($lowndes_zone5 ->where('call_sign','SQUAD 1')->first()->miles_out,1)}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone5 ->where('call_sign','SQUAD 2')->first()) 0 @else {{number_format($lowndes_zone5 ->where('call_sign','SQUAD 2')->first()->miles_out,1)}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone5 ->where('call_sign','SQUAD 3')->first()) 0 @else {{number_format($lowndes_zone5 ->where('call_sign','SQUAD 3')->first()->miles_out,1)}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone5 ->where('call_sign','SQUAD 4')->first()) 0 @else {{number_format($lowndes_zone5 ->where('call_sign','SQUAD 4')->first()->miles_out,1)}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone5 ->where('call_sign','SQUAD 5')->first()) 0 @else {{number_format($lowndes_zone5 ->where('call_sign','SQUAD 5')->first()->miles_out,1)}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone5 ->where('call_sign','SQUAD 6')->first()) 0 @else {{number_format($lowndes_zone5 ->where('call_sign','SQUAD 6')->first()->miles_out,1)}} @endif</td>
                                                </tr>
                                            <tr>
                                                <td><b>Average Response Time</b></td>
                                                <td align="center">@if(!$lowndes_zone5 ->where('call_sign','SQUAD 1')->first()) 0 @else {{$lowndes_zone5 ->where('call_sign','SQUAD 1')->first()->response_time}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone5 ->where('call_sign','SQUAD 2')->first()) 0 @else {{$lowndes_zone5 ->where('call_sign','SQUAD 2')->first()->response_time}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone5 ->where('call_sign','SQUAD 3')->first()) 0 @else {{number_format($lowndes_zone5 ->where('call_sign','SQUAD 3')->first()->response_time,1)}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone5 ->where('call_sign','SQUAD 4')->first()) 0 @else {{$lowndes_zone5 ->where('call_sign','SQUAD 4')->first()->response_time}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone5 ->where('call_sign','SQUAD 5')->first()) 0 @else {{$lowndes_zone5 ->where('call_sign','SQUAD 5')->first()->response_time}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone5 ->where('call_sign','SQUAD 6')->first()) 0 @else {{$lowndes_zone5 ->where('call_sign','SQUAD 6')->first()->response_time}} @endif</td>
                                                </tr>
                                            <tr>
                                                <td><b>Average Time Out of Service</b></td>
                                                <td align="center">@if(!$lowndes_zone5 ->where('call_sign','SQUAD 1')->first()) 0 @else {{$lowndes_zone5 ->where('call_sign','SQUAD 1')->first()->out_service}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone5 ->where('call_sign','SQUAD 2')->first()) 0 @else {{$lowndes_zone5 ->where('call_sign','SQUAD 2')->first()->out_service}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone5 ->where('call_sign','SQUAD 3')->first()) 0 @else {{number_format($lowndes_zone5 ->where('call_sign','SQUAD 3')->first()->out_service,1)}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone5 ->where('call_sign','SQUAD 4')->first()) 0 @else {{$lowndes_zone5 ->where('call_sign','SQUAD 4')->first()->out_service}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone5 ->where('call_sign','SQUAD 5')->first()) 0 @else {{$lowndes_zone5 ->where('call_sign','SQUAD 5')->first()->out_service}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone5 ->where('call_sign','SQUAD 6')->first()) 0 @else {{$lowndes_zone5 ->where('call_sign','SQUAD 6')->first()->out_service}} @endif</td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                            <td colspan="7"><i>* Data calculated from previous month. ({{ Carbon::parse($most_recent->dispatch_notified_time)->format('m-Y') }})</i></td>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <div class="card bg-dark text-white mb-3">
                                    <div class="card-header">
                                        <b>Zone 6</b>
                                    </div>
                                    <div class="card-body">
                                        <table class="table table-hover table-condensed">
                                            <thead>
                                            <tr>
                                                <td><b></b></td>
                                                <td align="center"><b>Squad 1</b></td>
                                                <td align="center"><b>Squad 2</b></td>
                                                <td align="center"><b>Squad 3</b></td>
                                                <td align="center"><b>Squad 4</b></td>
                                                <td align="center"><b>Squad 5</b></td>
                                                <td align="center"><b>Squad 6</b></td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td><b>Average Miles Out</b></td>
                                                <td align="center">@if(!$lowndes_zone6 ->where('call_sign','SQUAD 1')->first()) 0 @else {{number_format($lowndes_zone6 ->where('call_sign','SQUAD 1')->first()->miles_out,1)}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone6 ->where('call_sign','SQUAD 2')->first()) 0 @else {{number_format($lowndes_zone6 ->where('call_sign','SQUAD 2')->first()->miles_out,1)}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone6 ->where('call_sign','SQUAD 3')->first()) 0 @else {{number_format($lowndes_zone6 ->where('call_sign','SQUAD 3')->first()->miles_out,1)}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone6 ->where('call_sign','SQUAD 4')->first()) 0 @else {{number_format($lowndes_zone6 ->where('call_sign','SQUAD 4')->first()->miles_out,1)}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone6 ->where('call_sign','SQUAD 5')->first()) 0 @else {{number_format($lowndes_zone6 ->where('call_sign','SQUAD 5')->first()->miles_out,1)}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone6 ->where('call_sign','SQUAD 6')->first()) 0 @else {{number_format($lowndes_zone6 ->where('call_sign','SQUAD 6')->first()->miles_out,1)}} @endif</td>
                                                </tr>
                                            <tr>
                                                <td><b>Average Response Time</b></td>
                                                <td align="center">@if(!$lowndes_zone6 ->where('call_sign','SQUAD 1')->first()) 0 @else {{$lowndes_zone6 ->where('call_sign','SQUAD 1')->first()->response_time}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone6 ->where('call_sign','SQUAD 2')->first()) 0 @else {{$lowndes_zone6 ->where('call_sign','SQUAD 2')->first()->response_time}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone6 ->where('call_sign','SQUAD 3')->first()) 0 @else {{number_format($lowndes_zone6 ->where('call_sign','SQUAD 3')->first()->response_time,1)}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone6 ->where('call_sign','SQUAD 4')->first()) 0 @else {{$lowndes_zone6 ->where('call_sign','SQUAD 4')->first()->response_time}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone6 ->where('call_sign','SQUAD 5')->first()) 0 @else {{$lowndes_zone6 ->where('call_sign','SQUAD 5')->first()->response_time}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone6 ->where('call_sign','SQUAD 6')->first()) 0 @else {{$lowndes_zone6 ->where('call_sign','SQUAD 6')->first()->response_time}} @endif</td>
                                                </tr>
                                            <tr>
                                                <td><b>Average Time Out of Service</b></td>
                                                <td align="center">@if(!$lowndes_zone6 ->where('call_sign','SQUAD 1')->first()) 0 @else {{$lowndes_zone6 ->where('call_sign','SQUAD 1')->first()->out_service}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone6 ->where('call_sign','SQUAD 2')->first()) 0 @else {{$lowndes_zone6 ->where('call_sign','SQUAD 2')->first()->out_service}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone6 ->where('call_sign','SQUAD 3')->first()) 0 @else {{number_format($lowndes_zone6 ->where('call_sign','SQUAD 3')->first()->out_service,1)}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone6 ->where('call_sign','SQUAD 4')->first()) 0 @else {{$lowndes_zone6 ->where('call_sign','SQUAD 4')->first()->out_service}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone6 ->where('call_sign','SQUAD 5')->first()) 0 @else {{$lowndes_zone6 ->where('call_sign','SQUAD 5')->first()->out_service}} @endif</td>
                                                <td align="center">@if(!$lowndes_zone6 ->where('call_sign','SQUAD 6')->first()) 0 @else {{$lowndes_zone6 ->where('call_sign','SQUAD 6')->first()->out_service}} @endif</td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                            <td colspan="7"><i>* Data calculated from previous month. ({{ Carbon::parse($most_recent->dispatch_notified_time)->format('m-Y') }})</i></td>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <div class="card bg-dark text-white mb-3">
                                    <div class="card-header">
                                        <b>Squad Totals for All Zones in Lowndes County</b>
                                    </div>
                                    <div class="card-body">
                                        <table class="table table-hover table-condensed">
                                            <thead>
                                            <tr>
                                                <td><b></b></td>
                                                <td align="center"><b>Squad 1</b></td>
                                                <td align="center"><b>Squad 2</b></td>
                                                <td align="center"><b>Squad 3</b></td>
                                                <td align="center"><b>Squad 4</b></td>
                                                <td align="center"><b>Squad 5</b></td>
                                                <td align="center"><b>Squad 6</b></td>
                                                <td align="center"><b>AVG Total</b></td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td><b>Average Miles Out</b></td>
                                                <td align="center">{{number_format($lowndes_squad_totals ->where('call_sign','SQUAD 1')->first()->miles_out,1)}}</td>
                                                <td align="center">{{number_format($lowndes_squad_totals ->where('call_sign','SQUAD 2')->first()->miles_out,1)}}</td>
                                                <td align="center">@if(!$lowndes_squad_totals ->where('call_sign','SQUAD 3')->first()) 0 @else {{number_format($lowndes_squad_totals ->where('call_sign','SQUAD 3')->first()->miles_out,1)}} @endif</td>
                                                <td align="center">{{number_format($lowndes_squad_totals ->where('call_sign','SQUAD 4')->first()->miles_out,1)}}</td>
                                                <td align="center">{{number_format($lowndes_squad_totals ->where('call_sign','SQUAD 5')->first()->miles_out,1)}}</td>
                                                <td align="center">@if(!$lowndes_squad_totals ->where('call_sign','SQUAD 6')->first()) 0 @else {{number_format($lowndes_squad_totals ->where('call_sign','SQUAD 6')->first()->miles_out,1)}} @endif</td>
                                                <td align="center">{{number_format($lowndes_squad_totals ->avg('miles_out'),1)}}</td>
                                            </tr>
                                            <tr>
                                                <td><b>Average Response Time</b></td>
                                                <td align="center">{{$lowndes_squad_totals ->where('call_sign','SQUAD 1')->first()->response_time}}</td>
                                                <td align="center">{{$lowndes_squad_totals ->where('call_sign','SQUAD 2')->first()->response_time}}</td>
                                                <td align="center">@if(!$lowndes_squad_totals ->where('call_sign','SQUAD 3')->first()) 0 @else {{number_format($lowndes_squad_totals ->where('call_sign','SQUAD 3')->first()->response_time,1)}} @endif</td>
                                                <td align="center">{{$lowndes_squad_totals ->where('call_sign','SQUAD 4')->first()->response_time}}</td>
                                                <td align="center">{{$lowndes_squad_totals ->where('call_sign','SQUAD 5')->first()->response_time}}</td>
                                                <td align="center">@if(!$lowndes_squad_totals ->where('call_sign','SQUAD 6')->first()) 0 @else {{number_format($lowndes_squad_totals ->where('call_sign','SQUAD 6')->first()->response_time,1)}} @endif </td>
                                                <td align="center">{{number_format($lowndes_squad_totals ->avg('response_time'),1)}}</td>
                                            </tr>
                                            <tr>
                                                <td><b>Average Time Out of Service</b></td>
                                                <td align="center">{{$lowndes_squad_totals ->where('call_sign','SQUAD 1')->first()->out_service}}</td>
                                                <td align="center">{{$lowndes_squad_totals ->where('call_sign','SQUAD 2')->first()->out_service}}</td>
                                                <td align="center">@if(!$lowndes_squad_totals ->where('call_sign','SQUAD 3')->first()) 0 @else {{number_format($lowndes_squad_totals ->where('call_sign','SQUAD 3')->first()->out_service,1)}} @endif</td>
                                                <td align="center">{{$lowndes_squad_totals ->where('call_sign','SQUAD 4')->first()->out_service}}</td>
                                                <td align="center">{{$lowndes_squad_totals ->where('call_sign','SQUAD 5')->first()->out_service}}</td>
                                                <td align="center">@if(!$lowndes_squad_totals ->where('call_sign','SQUAD 6')->first()) 0 @else {{number_format($lowndes_squad_totals ->where('call_sign','SQUAD 6')->first()->out_service,1)}} @endif</td>
                                                <td align="center">{{number_format($lowndes_squad_totals ->avg('out_service'),1)}}</td>
                                            </tr>
                                            </tbody>
                                            <tfoot>
                                            <td colspan="7"><i>* Data calculated from previous month. ({{ Carbon::parse($most_recent->dispatch_notified_time)->format('m-Y') }})</i></td>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>

                                
                                @endif
                            </div>
                        </div>
                    </div>
                    <div id="menu2" class="tab-pane fade">

                        <br>
                        <div class="card bg-secondary text-white mb-3">
                            <div class="card-header">
                                <b>Lanier County</b>
                            </div>
                            <div class="card-body">
                                @if($lanier_zone1->isEmpty())
                                    <div class="alert alert-info">No data for last month.</div>
                                @Else

                                <div class="card bg-dark text-white mb-3">
                                        <div class="card-header">
                                            <b>Zone Totals for All Squads in Lanier County</b>
                                        </div>
                                        <div class="card-body">
                                            <table class="table table-hover table-condensed">
                                                <thead>
                                                    <tr>
                                                        <td><b></b></td>
                                                        <td align="center"><b>Zone 1</b></td>
                                                        <td align="center"><b>9 - 1  Lakeland</b></td>
                                                        <td align="center"><b>9 - 2 Teeterville</b></td>
                                                        <td align="center"><b>9 - 3 Mud Creek</b></td>
                                                        <td align="center"><b>9 - 4 West Side</b></td>
                                                        <td align="center"><b>9 - 5 Stockton</b></td>
                                                        <td align="center"><b>AVG/Total</b></td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td><b>Average Miles Out</b></td>
                                                        <td align="center">@if(!$lanier_zone_totals ->where('incident_zone','1')->first()) 0 @else {{number_format($lanier_zone_totals ->where('incident_zone','1')->first()->miles_out,1)}} @endif</td>
                                                        <td align="center">@if(!$lanier_zone_totals ->where('incident_zone','9                - 1  Lakeland')->first()) 0 @else {{number_format($lanier_zone_totals ->where('incident_zone','9                - 1  Lakeland')->first()->miles_out,1)}} @endif</td>
                                                        <td align="center">@if(!$lanier_zone_totals ->where('incident_zone','9 - 2 Teeterville')->first()) 0 @else {{number_format($lanier_zone_totals ->where('incident_zone','9 - 2 Teeterville')->first()->miles_out,1)}} @endif</td>
                                                        <td align="center">@if(!$lanier_zone_totals ->where('incident_zone','9 - 3 Mud Creek')->first()) 0 @else {{number_format($lanier_zone_totals ->where('incident_zone','9 - 3 Mud Creek')->first()->miles_out,1)}} @endif</td>
                                                        <td align="center">@if(!$lanier_zone_totals ->where('incident_zone','9 - 4 West Side')->first()) 0 @else {{number_format($lanier_zone_totals ->where('incident_zone','9 - 4 West Side')->first()->miles_out,1)}} @endif</td>
                                                        <td align="center">@if(!$lanier_zone_totals ->where('incident_zone','9 - 5 Stockton')->first()) 0 @else {{number_format($lanier_zone_totals ->where('incident_zone','9 - 5 Stockton')->first()->miles_out,1)}} @endif</td>
                                                        <td align="center">{{number_format($lanier_zone_totals ->avg('miles_out'),1)}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>Average Response Time</b></td>
                                                        <td align="center">@if(!$lanier_zone_totals ->where('incident_zone','1')->first()) 0 @else {{$lanier_zone_totals ->where('incident_zone','1')->first()->response_time}} @endif</td>
                                                        <td align="center">@if(!$lanier_zone_totals ->where('incident_zone','9                - 1  Lakeland')->first()) 0 @else {{$lanier_zone_totals ->where('incident_zone','9                - 1  Lakeland')->first()->response_time}} @endif</td>
                                                        <td align="center">@if(!$lanier_zone_totals ->where('incident_zone','9 - 2 Teeterville')->first()) 0 @else {{$lanier_zone_totals ->where('incident_zone','9 - 2 Teeterville')->first()->response_time}} @endif</td>
                                                        <td align="center">@if(!$lanier_zone_totals ->where('incident_zone','9 - 3 Mud Creek')->first()) 0 @else {{$lanier_zone_totals ->where('incident_zone','9 - 3 Mud Creek')->first()->response_time}} @endif</td>
                                                        <td align="center">@if(!$lanier_zone_totals ->where('incident_zone','9 - 4 West Side')->first()) 0 @else {{$lanier_zone_totals ->where('incident_zone','9 - 4 West Side')->first()->response_time}} @endif</td>
                                                        <td align="center">@if(!$lanier_zone_totals ->where('incident_zone','9 - 5 Stockton')->first()) 0 @else {{$lanier_zone_totals ->where('incident_zone','9 - 5 Stockton')->first()->response_time}} @endif</td>
                                                        <td align="center">{{number_format($lanier_zone_totals ->avg('response_time'),1)}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>Average Time Out of Service</b></td>
                                                        <td align="center">@if(!$lanier_zone_totals ->where('incident_zone','1')->first()) 0 @else {{$lanier_zone_totals ->where('incident_zone','1')->first()->out_service}} @endif</td>
                                                        <td align="center">@if(!$lanier_zone_totals ->where('incident_zone','9                - 1  Lakeland')->first()) 0 @else {{$lanier_zone_totals ->where('incident_zone','9                - 1  Lakeland')->first()->out_service}} @endif</td>
                                                        <td align="center">@if(!$lanier_zone_totals ->where('incident_zone','9 - 2 Teeterville')->first()) 0 @else {{$lanier_zone_totals ->where('incident_zone','9 - 2 Teeterville')->first()->out_service}} @endif</td>
                                                        <td align="center">@if(!$lanier_zone_totals ->where('incident_zone','9 - 3 Mud Creek')->first()) 0 @else {{$lanier_zone_totals ->where('incident_zone','9 - 3 Mud Creek')->first()->out_service}} @endif</td>
                                                        <td align="center">@if(!$lanier_zone_totals ->where('incident_zone','9 - 4 West Side')->first()) 0 @else {{$lanier_zone_totals ->where('incident_zone','9 - 4 West Side')->first()->out_service}} @endif</td>
                                                        <td align="center">@if(!$lanier_zone_totals ->where('incident_zone','9 - 5 Stockton')->first()) 0 @else {{$lanier_zone_totals ->where('incident_zone','9 - 5 Stockton')->first()->out_service}} @endif</td>
                                                        <td align="center">{{number_format($lanier_zone_totals ->avg('out_service'),1)}}</td>
                                                    </tr>
                                                    <tr class="bg-success">
                                                        <td><b>Total # of Calls</b></td>
                                                        <td align="center">@if(!$lanier_zone_totals ->where('incident_zone','1')->first()) 0 @else {{$lanier_zone_totals ->where('incident_zone','1')->first()->total}} @endif</td>
                                                        <td align="center">@if(!$lanier_zone_totals ->where('incident_zone','9                - 1  Lakeland')->first()) 0 @else {{$lanier_zone_totals ->where('incident_zone','9                - 1  Lakeland')->first()->total}} @endif</td>
                                                        <td align="center">@if(!$lanier_zone_totals ->where('incident_zone','9 - 2 Teeterville')->first()) 0 @else {{$lanier_zone_totals ->where('incident_zone','9 - 2 Teeterville')->first()->total}} @endif</td>
                                                        <td align="center">@if(!$lanier_zone_totals ->where('incident_zone','9 - 3 Mud Creek')->first()) 0 @else {{$lanier_zone_totals ->where('incident_zone','9 - 3 Mud Creek')->first()->total}} @endif</td>
                                                        <td align="center">@if(!$lanier_zone_totals ->where('incident_zone','9 - 4 West Side')->first()) 0 @else {{$lanier_zone_totals ->where('incident_zone','9 - 4 West Side')->first()->total}} @endif</td>
                                                        <td align="center">@if(!$lanier_zone_totals ->where('incident_zone','9 - 5 Stockton')->first()) 0 @else {{$lanier_zone_totals ->where('incident_zone','9 - 5 Stockton')->first()->total}} @endif</td>
                                                        <td align="center">{{ $lanier_zone_totals->sum('total') }}</td>
                                                    </tr>
                                                </tbody>
                                                <tfoot>
                                                <td colspan="8"><i>* Data calculated from previous month. ({{ Carbon::parse($most_recent->dispatch_notified_time)->format('m-Y') }})</i></td>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="card bg-dark text-white mb-3">
                                        <div class="card-header">
                                            <b>9 - 1 Lakeland</b>
                                        </div>
                                        <div class="card-body">
                                            <table class="table table-hover table-condensed">
                                                <thead>
                                                <tr>
                                                    <td><b></b></td>
                                                    <td align="center"><b>Lanier County - M1</b></td>
                                                    <td align="center"><b>Lanier County - M2</b></td>
                                                    <td align="center"><b>Lanier County - M3</b></td>
                                                    <td align="center"><b>AVG Total</b></td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td><b>Average Miles Out</b></td>
                                                    <td align="center">@if(!$lanier_zone1 ->where('call_sign','LANIER COUNTY - M1')->first()) 0 @else {{number_format($lanier_zone1 ->where('call_sign','LANIER COUNTY - M1')->first()->miles_out,1)}} @endif</td>
                                                    <td align="center">@if(!$lanier_zone1 ->where('call_sign','LANIER COUNTY - M2')->first()) 0 @else {{number_format($lanier_zone1 ->where('call_sign','LANIER COUNTY - M2')->first()->miles_out,1)}} @endif</td>
                                                    <td align="center">@if(!$lanier_zone1 ->where('call_sign','LANIER COUNTY - M3')->first()) 0 @else {{number_format($lanier_zone1 ->where('call_sign','LANIER COUNTY - M3')->first()->miles_out,1)}} @endif</td>
                                                    <td align="center">{{number_format($lanier_zone1 ->avg('miles_out'),1)}}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Average Response Time</b></td>
                                                    <td align="center">@if(!$lanier_zone1 ->where('call_sign','LANIER COUNTY - M1')->first()) 0 @else {{$lanier_zone1 ->where('call_sign','LANIER COUNTY - M1')->first()->response_time}} @endif</td>
                                                    <td align="center">@if(!$lanier_zone1 ->where('call_sign','LANIER COUNTY - M2')->first()) 0 @else {{$lanier_zone1 ->where('call_sign','LANIER COUNTY - M2')->first()->response_time}} @endif</td>
                                                    <td align="center">@if(!$lanier_zone1 ->where('call_sign','LANIER COUNTY - M3')->first()) 0 @else {{$lanier_zone1 ->where('call_sign','LANIER COUNTY - M3')->first()->response_time}} @endif</td>
                                                    <td align="center">{{number_format($lowndes_zone1 ->avg('response_time'),1)}}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Average Time Out of Service</b></td>
                                                    <td align="center">@if(!$lanier_zone1 ->where('call_sign','LANIER COUNTY - M1')->first()) 0 @else {{$lanier_zone1 ->where('call_sign','LANIER COUNTY - M1')->first()->out_service}} @endif</td>
                                                    <td align="center">@if(!$lanier_zone1 ->where('call_sign','LANIER COUNTY - M2')->first()) 0 @else {{$lanier_zone1 ->where('call_sign','LANIER COUNTY - M2')->first()->out_service}} @endif</td>
                                                    <td align="center">@if(!$lanier_zone1 ->where('call_sign','LANIER COUNTY - M3')->first()) 0 @else {{$lanier_zone1 ->where('call_sign','LANIER COUNTY - M3')->first()->out_service}} @endif</td>
                                                    <td align="center">{{number_format($lowndes_zone1 ->avg('out_service'),1)}}</td>
                                                </tr>
                                                </tbody>
                                                <tfoot>
                                                <td colspan="7"><i>* Data calculated from previous month.</i></td>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="card bg-dark text-white mb-3">
                                        <div class="card-header">
                                            <b>9 - 2 Teeterville</b>
                                        </div>
                                        <div class="card-body">
                                            <table class="table table-hover table-condensed">
                                                <thead>
                                                <tr>
                                                    <td><b></b></td>
                                                    <td align="center"><b>Lanier County - M1</b></td>
                                                    <td align="center"><b>Lanier County - M2</b></td>
                                                    <td align="center"><b>Lanier County - M3</b></td>
                                                    <td align="center"><b>AVG Total</b></td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td><b>Average Miles Out</b></td>
                                                    <td align="center">@if(!$lanier_zone2 ->where('call_sign','LANIER COUNTY - M1')->first()) 0 @else {{number_format($lanier_zone2 ->where('call_sign','LANIER COUNTY - M1')->first()->miles_out,1)}} @endif</td>
                                                    <td align="center">@if(!$lanier_zone2 ->where('call_sign','LANIER COUNTY - M2')->first()) 0 @else {{number_format($lanier_zone2 ->where('call_sign','LANIER COUNTY - M2')->first()->miles_out,1)}} @endif</td>
                                                    <td align="center">@if(!$lanier_zone2 ->where('call_sign','LANIER COUNTY - M3')->first()) 0 @else {{number_format($lanier_zone2 ->where('call_sign','LANIER COUNTY - M3')->first()->miles_out,1)}} @endif</td>
                                                    <td align="center">{{number_format($lanier_zone2 ->avg('miles_out'),1)}}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Average Response Time</b></td>
                                                    <td align="center">@if(!$lanier_zone2 ->where('call_sign','LANIER COUNTY - M1')->first()) 0 @else {{$lanier_zone2 ->where('call_sign','LANIER COUNTY - M1')->first()->response_time}} @endif</td>
                                                    <td align="center">@if(!$lanier_zone2 ->where('call_sign','LANIER COUNTY - M2')->first()) 0 @else {{$lanier_zone2 ->where('call_sign','LANIER COUNTY - M2')->first()->response_time}} @endif</td>
                                                    <td align="center">@if(!$lanier_zone2 ->where('call_sign','LANIER COUNTY - M3')->first()) 0 @else {{$lanier_zone2 ->where('call_sign','LANIER COUNTY - M3')->first()->response_time}} @endif</td>
                                                    <td align="center">{{number_format($lanier_zone2 ->avg('response_time'),1)}}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Average Time Out of Service</b></td>
                                                    <td align="center">@if(!$lanier_zone2 ->where('call_sign','LANIER COUNTY - M1')->first() or is_null($lanier_zone2 ->where('call_sign','LANIER COUNTY - M1')->first())) 0 @else {{$lanier_zone2 ->where('call_sign','LANIER COUNTY - M1')->first()->out_service}} @endif</td>
                                                    <td align="center">@if(!$lanier_zone2 ->where('call_sign','LANIER COUNTY - M2')->first()) 0 @else {{$lanier_zone2 ->where('call_sign','LANIER COUNTY - M2')->first()->out_service}} @endif</td>
                                                    <td align="center">@if(!$lanier_zone2 ->where('call_sign','LANIER COUNTY - M3')->first() or is_null($lanier_zone2 ->where('call_sign','LANIER COUNTY - M3')->first())) 0 @else {{$lanier_zone2 ->where('call_sign','LANIER COUNTY - M3')->first()->out_service}} @endif</td>
                                                    <td align="center">{{number_format($lanier_zone2 ->avg('out_service'),1)}}</td>
                                                </tr>
                                                </tbody>
                                                <tfoot>
                                                <td colspan="7"><i>* Data calculated from previous month.</i></td>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="card bg-dark text-white mb-3">
                                        <div class="card-header">
                                            <b>9 - 3 Mud Creek</b>
                                        </div>
                                        <div class="card-body">
                                            <table class="table table-hover table-condensed">
                                                <thead>
                                                <tr>
                                                    <td><b></b></td>
                                                    <td align="center"><b>Lanier County - M1</b></td>
                                                    <td align="center"><b>Lanier County - M2</b></td>
                                                    <td align="center"><b>Lanier County - M3</b></td>
                                                    <td align="center"><b>AVG Total</b></td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td><b>Average Miles Out</b></td>
                                                    <td align="center">@if(!$lanier_zone3 ->where('call_sign','LANIER COUNTY - M1')->first()) 0 @else {{number_format($lanier_zone3 ->where('call_sign','LANIER COUNTY - M1')->first()->miles_out,1)}} @endif</td>
                                                    <td align="center">@if(!$lanier_zone3 ->where('call_sign','LANIER COUNTY - M2')->first()) 0 @else {{number_format($lanier_zone3 ->where('call_sign','LANIER COUNTY - M2')->first()->miles_out,1)}} @endif</td>
                                                    <td align="center">@if(!$lanier_zone3 ->where('call_sign','LANIER COUNTY - M3')->first()) 0 @else {{number_format($lanier_zone3 ->where('call_sign','LANIER COUNTY - M3')->first()->miles_out,1)}} @endif</td>
                                                    <td align="center">{{number_format($lanier_zone3 ->avg('miles_out'),1)}}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Average Response Time</b></td>
                                                    <td align="center">@if(!$lanier_zone3 ->where('call_sign','LANIER COUNTY - M1')->first()) 0 @else {{$lanier_zone3 ->where('call_sign','LANIER COUNTY - M1')->first()->response_time}} @endif</td>
                                                    <td align="center">@if(!$lanier_zone3 ->where('call_sign','LANIER COUNTY - M2')->first()) 0 @else {{$lanier_zone3 ->where('call_sign','LANIER COUNTY - M2')->first()->response_time}} @endif</td>
                                                    <td align="center">@if(!$lanier_zone3 ->where('call_sign','LANIER COUNTY - M3')->first()) 0 @else {{$lanier_zone3 ->where('call_sign','LANIER COUNTY - M3')->first()->response_time}} @endif</td>
                                                    <td align="center">{{number_format($lanier_zone3 ->avg('response_time'),1)}}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Average Time Out of Service</b></td>
                                                    <td align="center">@if(!$lanier_zone3 ->where('call_sign','LANIER COUNTY - M1')->first()) 0 @else {{$lanier_zone3 ->where('call_sign','LANIER COUNTY - M1')->first()->out_service}} @endif</td>
                                                    <td align="center">@if(!$lanier_zone3 ->where('call_sign','LANIER COUNTY - M2')->first()) 0 @else {{$lanier_zone3 ->where('call_sign','LANIER COUNTY - M2')->first()->out_service}} @endif</td>
                                                    <td align="center">@if(!$lanier_zone3 ->where('call_sign','LANIER COUNTY - M3')->first()) 0 @else {{$lanier_zone3 ->where('call_sign','LANIER COUNTY - M3')->first()->out_service}} @endif</td>
                                                    <td align="center">{{number_format($lanier_zone3 ->avg('out_service'),1)}}</td>
                                                </tr>
                                                </tbody>
                                                <tfoot>
                                                <td colspan="7"><i>* Data calculated from previous month.</i></td>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="card bg-dark text-white mb-3">
                                        <div class="card-header">
                                            <b>9 - 4 West Side</b>
                                        </div>
                                        <div class="card-body">
                                            <table class="table table-hover table-condensed">
                                                <thead>
                                                <tr>
                                                    <td><b></b></td>
                                                    <td align="center"><b>Lanier County - M1</b></td>
                                                    <td align="center"><b>Lanier County - M2</b></td>
                                                    <td align="center"><b>Lanier County - M3</b></td>
                                                    <td align="center"><b>AVG Total</b></td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td><b>Average Miles Out</b></td>
                                                    <td align="center">@if(!$lanier_zone4 ->where('call_sign','LANIER COUNTY - M1')->first()) 0 @else {{number_format($lanier_zone4 ->where('call_sign','LANIER COUNTY - M1')->first()->miles_out,1)}} @endif</td>
                                                    <td align="center">@if(!$lanier_zone4 ->where('call_sign','LANIER COUNTY - M2')->first()) 0 @else {{number_format($lanier_zone4 ->where('call_sign','LANIER COUNTY - M2')->first()->miles_out,1)}} @endif</td>
                                                    <td align="center">@if(!$lanier_zone4 ->where('call_sign','LANIER COUNTY - M3')->first()) 0 @else {{number_format($lanier_zone4 ->where('call_sign','LANIER COUNTY - M3')->first()->miles_out,1)}} @endif</td>
                                                    <td align="center">{{number_format($lanier_zone4 ->avg('miles_out'),1)}}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Average Response Time</b></td>
                                                    <td align="center">@if(!$lanier_zone4 ->where('call_sign','LANIER COUNTY - M1')->first()) 0 @else {{$lanier_zone4 ->where('call_sign','LANIER COUNTY - M1')->first()->response_time}} @endif</td>
                                                    <td align="center">@if(!$lanier_zone4 ->where('call_sign','LANIER COUNTY - M2')->first()) 0 @else {{$lanier_zone4 ->where('call_sign','LANIER COUNTY - M2')->first()->response_time}} @endif</td>
                                                    <td align="center">@if(!$lanier_zone4 ->where('call_sign','LANIER COUNTY - M3')->first()) 0 @else {{$lanier_zone4 ->where('call_sign','LANIER COUNTY - M3')->first()->response_time}} @endif</td>
                                                    <td align="center">{{number_format($lanier_zone4 ->avg('response_time'),1)}}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Average Time Out of Service</b></td>
                                                    <td align="center">@if(!$lanier_zone4 ->where('call_sign','LANIER COUNTY - M1')->first()) 0 @else {{$lanier_zone4 ->where('call_sign','LANIER COUNTY - M1')->first()->out_service}} @endif</td>
                                                    <td align="center">@if(!$lanier_zone4 ->where('call_sign','LANIER COUNTY - M2')->first()) 0 @else {{$lanier_zone4 ->where('call_sign','LANIER COUNTY - M2')->first()->out_service}} @endif</td>
                                                    <td align="center">@if(!$lanier_zone4 ->where('call_sign','LANIER COUNTY - M3')->first()) 0 @else {{$lanier_zone4 ->where('call_sign','LANIER COUNTY - M3')->first()->out_service}} @endif</td>
                                                    <td align="center">{{number_format($lanier_zone4 ->avg('out_service'),1)}}</td>
                                                </tr>
                                                </tbody>
                                                <tfoot>
                                                <td colspan="7"><i>* Data calculated from previous month.</i></td>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="card bg-dark text-white mb-3">
                                        <div class="card-header">
                                            <b>9 - 5 Stockton</b>
                                        </div>
                                        <div class="card-body">
                                            <table class="table table-hover table-condensed">
                                                <thead>
                                                <tr>
                                                    <td><b></b></td>
                                                    <td align="center"><b>Lanier County - M1</b></td>
                                                    <td align="center"><b>Lanier County - M2</b></td>
                                                    <td align="center"><b>Lanier County - M3</b></td>
                                                    <td align="center"><b>AVG Total</b></td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td><b>Average Miles Out</b></td>
                                                    <td align="center">@if(!$lanier_zone5 ->where('call_sign','LANIER COUNTY - M1')->first()) 0 @else {{number_format($lanier_zone5 ->where('call_sign','LANIER COUNTY - M1')->first()->miles_out,1)}} @endif</td>
                                                    <td align="center">@if(!$lanier_zone5 ->where('call_sign','LANIER COUNTY - M2')->first()) 0 @else {{number_format($lanier_zone5 ->where('call_sign','LANIER COUNTY - M2')->first()->miles_out,1)}} @endif</td>
                                                    <td align="center">@if(!$lanier_zone5 ->where('call_sign','LANIER COUNTY - M3')->first()) 0 @else {{number_format($lanier_zone5 ->where('call_sign','LANIER COUNTY - M3')->first()->miles_out,1)}} @endif</td>
                                                    <td align="center">@if(!$lanier_zone5) 0 @else {{ number_format($lanier_zone5 ->avg('miles_out'),1)}} @endif</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Average Response Time</b></td>
                                                    <td align="center">@if(!$lanier_zone5 ->where('call_sign','LANIER COUNTY - M1')->first()) 0 @else {{$lanier_zone5 ->where('call_sign','LANIER COUNTY - M1')->first()->response_time}} @endif</td>
                                                    <td align="center">@if(!$lanier_zone5 ->where('call_sign','LANIER COUNTY - M2')->first()) 0 @else {{$lanier_zone5 ->where('call_sign','LANIER COUNTY - M2')->first()->response_time}} @endif</td>
                                                    <td align="center">@if(!$lanier_zone5 ->where('call_sign','LANIER COUNTY - M3')->first()) 0 @else {{$lanier_zone5 ->where('call_sign','LANIER COUNTY - M3')->first()->response_time}} @endif</td>
                                                    <td align="center">{{number_format($lanier_zone5 ->avg('response_time'),1)}}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Average Time Out of Service</b></td>
                                                    <td align="center">@if(!$lanier_zone5 ->where('call_sign','LANIER COUNTY - M1')->first()) 0 @else {{$lanier_zone5 ->where('call_sign','LANIER COUNTY - M1')->first()->out_service}} @endif</td>
                                                    <td align="center">@if(!$lanier_zone5 ->where('call_sign','LANIER COUNTY - M2')->first()) 0 @else {{$lanier_zone5 ->where('call_sign','LANIER COUNTY - M2')->first()->out_service}} @endif</td>
                                                    <td align="center">@if(!$lanier_zone5 ->where('call_sign','LANIER COUNTY - M3')->first()) 0 @else {{$lanier_zone5 ->where('call_sign','LANIER COUNTY - M3')->first()->out_service}} @endif</td>
                                                    <td align="center">{{number_format($lanier_zone5 ->avg('out_service'),1)}}</td>
                                                </tr>
                                                </tbody>
                                                <tfoot>
                                                <td colspan="7"><i>* Data calculated from previous month.</i></td>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="card bg-dark text-white mb-3">
                                        <div class="card-header">
                                            <b>Totals for Lanier County</b>
                                        </div>
                                        <div class="card-body">
                                            <table class="table table-hover table-condensed">
                                                <thead>
                                                <tr>
                                                    <td><b></b></td>
                                                    <td align="center"><b>Lanier County - M1</b></td>
                                                    <td align="center"><b>Lanier County - M2</b></td>
                                                    <td align="center"><b>Lanier County - M3</b></td>
                                                    <td align="center"><b>AVG Total</b></td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td><b>Average Miles Out</b></td>
                                                    <td align="center">@if(!$lanier_zone6->where('call_sign','LANIER COUNTY - M1')->first()) 0 @else {{number_format($lanier_zone6 ->where('call_sign','LANIER COUNTY - M1')->first()->miles_out,1)}} @endif</td>
                                                    <td align="center">@if(!$lanier_zone6->where('call_sign','LANIER COUNTY - M2')->first()) 0 @else {{number_format($lanier_zone6 ->where('call_sign','LANIER COUNTY - M2')->first()->miles_out,1)}} @endif</td>
                                                    <td align="center">@if(!$lanier_zone6->where('call_sign','LANIER COUNTY - M3')->first()) 0 @else {{number_format($lanier_zone6 ->where('call_sign','LANIER COUNTY - M3')->first()->miles_out,1)}} @endif</td>
                                                    <td align="center">{{number_format($lanier_zone6 ->avg('miles_out'),1)}}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Average Response Time</b></td>
                                                    <td align="center">@if(!$lanier_zone6->where('call_sign','LANIER COUNTY - M1')->first()) 0 @else {{$lanier_zone6 ->where('call_sign','LANIER COUNTY - M1')->first()->response_time}} @endif</td>
                                                    <td align="center">@if(!$lanier_zone6->where('call_sign','LANIER COUNTY - M2')->first()) 0 @else {{$lanier_zone6 ->where('call_sign','LANIER COUNTY - M2')->first()->response_time}} @endif</td>
                                                    <td align="center">@if(!$lanier_zone6->where('call_sign','LANIER COUNTY - M3')->first()) 0 @else {{$lanier_zone6 ->where('call_sign','LANIER COUNTY - M3')->first()->response_time}} @endif</td>
                                                    <td align="center">{{number_format($lanier_zone6 ->avg('response_time'),1)}}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Average Time Out of Service</b></td>
                                                    <td align="center">@if(!$lanier_zone6->where('call_sign','LANIER COUNTY - M1')->first()) 0 @else {{$lanier_zone6 ->where('call_sign','LANIER COUNTY - M1')->first()->out_service}} @endif</td>
                                                    <td align="center">@if(!$lanier_zone6->where('call_sign','LANIER COUNTY - M2')->first()) 0 @else {{$lanier_zone6 ->where('call_sign','LANIER COUNTY - M2')->first()->out_service}} @endif</td>
                                                    <td align="center">@if(!$lanier_zone6->where('call_sign','LANIER COUNTY - M3')->first()) 0 @else {{$lanier_zone6 ->where('call_sign','LANIER COUNTY - M3')->first()->out_service}} @endif</td>
                                                    <td align="center">{{number_format($lanier_zone6 ->avg('out_service'),1)}}</td>
                                                </tr>
                                                </tbody>
                                                <tfoot>
                                                <td colspan="7"><i>* Data calculated from previous month.</i></td>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div id="menu3" class="tab-pane fade">
                        <div class="card-header">
                            <h3>8 - Echols County</h3>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-hover table-condensed table-dark table-bordered">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th nowrap>LANIER COUNTY - M1</th>
                                            <th nowrap>LANIER COUNTY - M2</th>
                                            <th nowrap>LANIER COUNTY - M3</th>
                                            <th nowrap>SQ1</th>
                                            <th nowrap>SQ2</th>
                                            <th nowrap>SQ3</th>
                                            <th nowrap>SQ4</th>
                                            <th nowrap>SQ5</th>
                                            <th nowrap>SQUAD 1</th>
                                            <th nowrap>SQUAD 2</th>
                                            <th nowrap>SQUAD 3</th>
                                            <th nowrap>SQUAD 4</th>
                                            <th nowrap>SQUAD 5</th>
                                            <th nowrap>SQUAD 6</th>
                                            <th nowrap>SQUAD 7</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td nowrap><b>Average Miles Out</b></td>
                                            <td align="center">@if(!$echols->where('call_sign','LANIER COUNTY - M1')->first()) 0 @else {{number_format($echols->where('call_sign','LANIER COUNTY - M1')->first()->miles_out,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','LANIER COUNTY - M2')->first()) 0 @else {{number_format($echols->where('call_sign','LANIER COUNTY - M2')->first()->miles_out,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','LANIER COUNTY - M3')->first()) 0 @else {{number_format($echols->where('call_sign','LANIER COUNTY - M3')->first()->miles_out,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','SQ1')->first()) 0 @else {{number_format($echols->where('call_sign','SQ1')->first()->miles_out,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','SQ2')->first()) 0 @else {{number_format($echols->where('call_sign','SQ2')->first()->miles_out,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','SQ3')->first()) 0 @else {{number_format($echols->where('call_sign','SQ3')->first()->miles_out,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','SQ4')->first()) 0 @else {{number_format($echols->where('call_sign','SQ4')->first()->miles_out,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','SQ5')->first()) 0 @else {{number_format($echols->where('call_sign','SQ5')->first()->miles_out,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','SQUAD 1')->first()) 0 @else {{number_format($echols->where('call_sign','SQUAD 1')->first()->miles_out,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','SQUAD 2')->first()) 0 @else {{number_format($echols->where('call_sign','SQUAD 2')->first()->miles_out,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','SQUAD 3')->first()) 0 @else {{number_format($echols->where('call_sign','SQUAD 3')->first()->miles_out,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','SQUAD 4')->first()) 0 @else {{number_format($echols->where('call_sign','SQUAD 4')->first()->miles_out,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','SQUAD 5')->first()) 0 @else {{number_format($echols->where('call_sign','SQUAD 5')->first()->miles_out,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','SQUAD 6')->first()) 0 @else {{number_format($echols->where('call_sign','SQUAD 6')->first()->miles_out,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','SQUAD 7')->first()) 0 @else {{number_format($echols->where('call_sign','SQUAD 7')->first()->miles_out,1)}} @endif</td>
                                        </tr>
                                        <tr>
                                            <td nowrap><b>Average Response Time</b></td>
                                            <td align="center">@if(!$echols->where('call_sign','LANIER COUNTY - M1')->first()) 0 @else {{number_format($echols->where('call_sign','LANIER COUNTY - M1')->first()->response_time,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','LANIER COUNTY - M2')->first()) 0 @else {{number_format($echols->where('call_sign','LANIER COUNTY - M2')->first()->response_time,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','LANIER COUNTY - M3')->first()) 0 @else {{number_format($echols->where('call_sign','LANIER COUNTY - M3')->first()->response_time,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','SQ1')->first()) 0 @else {{number_format($echols->where('call_sign','SQ1')->first()->response_time,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','SQ2')->first()) 0 @else {{number_format($echols->where('call_sign','SQ2')->first()->response_time,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','SQ3')->first()) 0 @else {{number_format($echols->where('call_sign','SQ3')->first()->response_time,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','SQ4')->first()) 0 @else {{number_format($echols->where('call_sign','SQ4')->first()->response_time,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','SQ5')->first()) 0 @else {{number_format($echols->where('call_sign','SQ5')->first()->response_time,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','SQUAD 1')->first()) 0 @else {{number_format($echols->where('call_sign','SQUAD 1')->first()->response_time,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','SQUAD 2')->first()) 0 @else {{number_format($echols->where('call_sign','SQUAD 2')->first()->response_time,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','SQUAD 3')->first()) 0 @else {{number_format($echols->where('call_sign','SQUAD 3')->first()->response_time,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','SQUAD 4')->first()) 0 @else {{number_format($echols->where('call_sign','SQUAD 4')->first()->response_time,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','SQUAD 5')->first()) 0 @else {{number_format($echols->where('call_sign','SQUAD 5')->first()->response_time,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','SQUAD 6')->first()) 0 @else {{number_format($echols->where('call_sign','SQUAD 6')->first()->response_time,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','SQUAD 7')->first()) 0 @else {{number_format($echols->where('call_sign','SQUAD 7')->first()->response_time,1)}} @endif</td>
                                        </tr>
                                        <tr>
                                            <td nowrap><b>Average Time Out of Service</b></td>
                                            <td align="center">@if(!$echols->where('call_sign','LANIER COUNTY - M1')->first()) 0 @else {{number_format($echols->where('call_sign','LANIER COUNTY - M1')->first()->out_service,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','LANIER COUNTY - M2')->first()) 0 @else {{number_format($echols->where('call_sign','LANIER COUNTY - M2')->first()->out_service,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','LANIER COUNTY - M3')->first()) 0 @else {{number_format($echols->where('call_sign','LANIER COUNTY - M3')->first()->out_service,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','SQ1')->first()) 0 @else {{number_format($echols->where('call_sign','SQ1')->first()->out_service,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','SQ2')->first()) 0 @else {{number_format($echols->where('call_sign','SQ2')->first()->out_service,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','SQ3')->first()) 0 @else {{number_format($echols->where('call_sign','SQ3')->first()->out_service,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','SQ4')->first()) 0 @else {{number_format($echols->where('call_sign','SQ4')->first()->out_service,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','SQ5')->first()) 0 @else {{number_format($echols->where('call_sign','SQ5')->first()->out_service,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','SQUAD 1')->first()) 0 @else {{number_format($echols->where('call_sign','SQUAD 1')->first()->out_service,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','SQUAD 2')->first()) 0 @else {{number_format($echols->where('call_sign','SQUAD 2')->first()->out_service,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','SQUAD 3')->first()) 0 @else {{number_format($echols->where('call_sign','SQUAD 3')->first()->out_service,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','SQUAD 4')->first()) 0 @else {{number_format($echols->where('call_sign','SQUAD 4')->first()->out_service,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','SQUAD 5')->first()) 0 @else {{number_format($echols->where('call_sign','SQUAD 5')->first()->out_service,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','SQUAD 6')->first()) 0 @else {{number_format($echols->where('call_sign','SQUAD 6')->first()->out_service,1)}} @endif</td>
                                            <td align="center">@if(!$echols->where('call_sign','SQUAD 7')->first()) 0 @else {{number_format($echols->where('call_sign','SQUAD 7')->first()->out_service,1)}} @endif</td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <td colspan="16"><i>* Data calculated from previous month. ({{ Carbon::parse($most_recent->dispatch_notified_time)->format('m-Y') }})</i></td>
                                    </tfoot>
                                </table>
                            </div>

                        </div>
                    </div>

                    <div id="menu4" class="tab-pane fade">
                        <div class="card-header">
                            <h3>All Counties, Zones, and Units Averages</h3>
                        </div>
                        <div class="card-body">
                            <table class="table table-dark table-bordered">
                                <thead>
                                    <th>Average Miles Out</th>
                                    <th>Average Response Time</th>
                                    <th>Average Time Out of Service</th>
                                </thead>
                                <tbody>
                                    <td>{{$allStats->miles_out}}</td>
                                    <td>{{$allStats->response_time}}</td>
                                    <td>{{$allStats->out_service}}</td>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    {{-- <div id="menu6" class="tab-pane fade">
                        <div class="card bg-dark text-white mb-3">
                            <div class="card-header">
                                <b>All Calls</b>
                            </div>
                            <div class="card-body">
                                <table width="99%" class="table" id="tableview">
                                    <thead>
                                        <tr>
                                            <td><b>Incident #</b></td>
                                            <td><b>Shift</b></td>
                                            <td><b>Call Sign</b></td>
                                            <td><b>Zone</b></td>
                                            <td><b>County</b></td>
                                            <td><b>Status</b></td>
                                            <td><b>Disposition</b></td>
                                            <td><b>Time</b></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($call_count as $calls)
                                        <tr>
                                            <td>{{$calls->incident_number}}</td>
                                            <td>{{$calls->shift_number}}</td>
                                            <td>{{$calls->call_sign}}</td>
                                            <td>{{$calls->incident_zone}}</td>
                                            <td>{{$calls->county}}</td>
                                            <td>{{$calls->incident_status}}</td>
                                            <td>{{$calls->patient_disposition}}</td>
                                            <td>{{$calls->total_time_on_call}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div> --}}

                </div>


            </div>
        </div>
    </div>

@include('ems.modal.editUnclassified')
@include('ems.modal.zero')
@include('ems.modal.report')

@endsection
{{--END of Content and START of Scripts--}}


@section('scripts')

<script type="application/javascript">

new Chart(document.getElementById("zeroChart"), {
        type: 'bar',
        data: {
            labels: [
                @foreach($zeros as $zero)
                    '{{$zero->date->format('m - Y')}}',
                @endforeach

            ],
            datasets: [
                {
                    backgroundColor: "#fc283a",
                    data: [
                        @foreach($zeros as $zero)
                            {{$zero->total_level_zero}},
                        @endforeach
                    ]
                }
            ]
        },
        options: {
            legend: { display: false },
            title: {
                display: true,
                text: 'Number of Level Zeros by Month'
            },
            scales: {
                yAxes: [{
                    stacked:false,
                ticks: {
                    beginAtZero: true
                    }
                }]
            }
        }
    });

</script>


<script type="application/ecmascript">
    Chart.defaults.global.defaultFontColor = '#fff';
    $(document).on("click", ".EditUnclassified", function () {
        //alert($(this).attr("data-id"));

        $('.id').val($(this).attr("data-id"));
        $('.incident_number').val($(this).attr("data-incident_number"));
        $('#EditUnclassified_modal').modal('show');
        //alert($(this).attr("data-first_name"));
    });

    $(document).ready(function() {
        $('#tableview').DataTable( {
            "pageLength": 25,
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );

    $(function(){
        $.getJSON("ems/charts/by_month", function (result) {
            var labels = [],data=[];
            for (var i = 0; i < result.length; i++) {
                labels.push(result[i].call_month_name);
                data.push(result[i].total);
            }
            var buyerData = {
                labels : labels,
                datasets: [
                    {
                        label: 'Calls for current year by month.',
                        data: data,
                        backgroundColor: ['#1d95f7'],
                        borderColor: "black",
                        borderWidth:1,
                    }
                ],


            };
            var buyers = document.getElementById('chart_by_month').getContext('2d');
            var chartInstance = new Chart(buyers, {
                type: 'line',

                options: {
                    legend: { display: false },
                    title: {
                        display: true,
                        text: 'Calls for last 12 months by month.'
                    },
                    responsiveAnimationDuration: 6000,
                    animation: {
                        duration: 200,
                        onComplete: function () {
                            // render the value of the chart above the bar
                            var ctx = this.chart.ctx;
                            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
                            ctx.fillStyle = this.chart.config.options.defaultFontColor;
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'bottom';
                            this.data.datasets.forEach(function (dataset) {
                                for (var i = 0; i < dataset.data.length; i++) {
                                    var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                                    ctx.fillText(dataset.data[i], model.x, model.y - 5);
                                }
                            });
                        }}
                },
                data: buyerData,


            });
        });

    });

</script>

<script type="application/javascript">
    $(document).ready(function() {
        $('#lanier_NET').DataTable( {
            "pageLength": 20,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>

<script type="application/javascript">
    $(document).ready(function() {
        $('#lowndes_NET').DataTable( {
            "pageLength": 20,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>

@endsection
@endif