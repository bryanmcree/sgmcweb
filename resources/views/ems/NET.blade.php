{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('EMS') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

<div class="card-deck mb-2">

    <div class="card bg-dark text-white mb-3 col-lg-3">
        <div class="card-header">
            New NET Entry
            <span class="float-right"><a href="/ems" class="btn btn-sm btn-primary">Return to Dashboard</a></span>
        </div>
        <form method="POST" action="/ems/NET/store">
            {{ csrf_field() }}

            <div class="card-body">

                <div class="form-group">
                    <label>Date</label>
                    <input type="date" name="date" class="form-control" required>
                </div>

                <div class="form-row">
                    <div class="form-group col-lg-6">
                        <label>Wheelchair</label>
                        <input type="number" class="form-control" name="wheelchair" placeholder="0" required>
                    </div>

                    <div class="form-group col-lg-6">
                        <label>Stetcher</label>
                        <input type="number" class="form-control" name="stretcher" placeholder="0" required>
                    </div>
                </div>

                <div class="form-group">
                    <label>Lowndes NET transports under 50 miles</label>
                    <input type="number" class="form-control" name="under_50" placeholder="0" required>
                </div>

                <div class="form-row">
                    <div class="form-group col-lg-6">
                        <label>Hospice</label>
                        <input type="number" class="form-control" name="hospice" placeholder="0" required>
                    </div>

                    <div class="form-group col-lg-6">
                        <label>Over 50 miles</label>
                        <input type="number" class="form-control" name="over_50" placeholder="0" required>
                    </div>
                </div>

                <div class="form-group">
                    <label>Outsourced</label>
                    <input type="number" class="form-control" name="Punts" placeholder="0" required>
                </div>

                <div class="form-group">
                    <label>Discharged to different SGMC Campus</label>
                    <input type="number" class="form-control" name="diff_campus" placeholder="0" required>
                </div>
                
            </div>

            <div class="card-footer">
                <input type="submit" class="btn btn-primary btn-block" value="Submit">
            </div>

        </form>
    </div>

    <div class="card bg-dark text-white mb-3 col-lg-9">
        <div class="card-header">
            NET Entries Totals
            <span class="float-right"><a href="/ems" class="btn btn-sm btn-primary">Return to Dashboard</a></span>
        </div>
        <div class="card-body">

            <table class="table table-dark table-hover table-striped table-bordered text-center" id="NET_totals">
                <thead>
                    <th>Month-Year</th>
                    <th>Wheelchair</th>
                    <th>Stretcher</th>
                    <th>Lowndes NET under 50 miles</th>
                    <th>Hospice</th>
                    <th>Over 50 miles</th>
                    <th>Outsourced</th>
                    <th>Discharge to diff SGMC Campus</th>
                </thead>
                <tbody>
                    @forelse($totalNET as $totals)
                        <tr>
                            <td>{{ $totals->month }}-{{ $totals->year }}</td>
                            <td>{{ $totals->wheelchair }}</td>
                            <td>{{ $totals->stretcher }}</td>
                            <td>{{ $totals->under_50 }}</td>
                            <td>{{ $totals->hospice }}</td>
                            <td>{{ $totals->over_50 }}</td>
                            <td>{{ $totals->punts }}</td>
                            <td>{{ $totals->diff_campus }}</td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="9">No Data Available</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>

        </div>
    </div>

</div>

<div class="card bg-dark text-white mb-3">
    <div class="card-header">
        Detailed NET Entries
        <span class="float-right"><a href="/ems" class="btn btn-sm btn-primary">Return to Dashboard</a></span>
    </div>
    <div class="card-body">

        <table class="table table-dark table-hover table-striped table-bordered text-center" id="NET_table">
            <thead>
                <th>Date</th>
                <th>Wheelchair</th>
                <th>Stretcher</th>
                <th>Lowndes NET under 50 miles</th>
                <th>Hospice</th>
                <th>Over 50 miles</th>
                <th>Outsourced</th>
                <th>Discharge to diff SGMC Campus</th>
                <th>Delete</th>
            </thead>
            <tbody>
                @forelse($NETs as $NET)
                    <tr>
                        <td>{{ Carbon::parse($NET->date)->format('m-d-Y') }}</td>
                        <td>{{ $NET->wheelchair }}</td>
                        <td>{{ $NET->stretcher }}</td>
                        <td>{{ $NET->under_50 }}</td>
                        <td>{{ $NET->hospice }}</td>
                        <td>{{ $NET->over_50 }}</td>
                        <td>{{ $NET->punts }}</td>
                        <td>{{ $NET->diff_campus }}</td>
                        <td><a href="#" class="btn btn-sm btn-danger NETDelete" NET_id="{{ $NET->id }}">Delete</a></td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="9">No Data Available</td>
                    </tr>
                @endforelse
            </tbody>
        </table>

    </div>
</div>

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">

    $(document).on("click", ".NETDelete", function () {
        //alert($(this).attr("data-cost_center"));
        var NET_id = $(this).attr("NET_id");
        DeleteZero(NET_id);
    });

    function DeleteZero(NET_id) {
        swal({
            title: "Delete NET Entry?",
            text: "Deleting this Entry cannot be undone.",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            confirmButtonText: "Yes, delete it!",
            confirmButtonColor: "#ec6c62"
        }, function() {
            $.ajax({
                url: "/ems/NET/destroy/" + NET_id,
                type: "GET"
            })
                .done(function(data) {
                    swal({
                            title: "Deleted",
                            text: "Entry Deleted",
                            type: "success",
                            timer: 1800,
                            showConfirmButton: false

                        },function() {
                            location.reload();
                        });
                })
                .error(function(data) {
                    swal("Oops", "We couldn't connect to the server!", "error");
                });
        });
    }

</script>

<script type="application/javascript">
    $(document).ready(function() {
        $('#NET_table').DataTable( {
            "pageLength": 10,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>

<script type="application/javascript">
    $(document).ready(function() {
        $('#NET_totals').DataTable( {
            "pageLength": 10,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>

@endsection

@endif