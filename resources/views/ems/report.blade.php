{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('EMS') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}

@section('content')

    <div class="card bg-dark text-white mb-3">
        <div class="card-header">
            Report for {{ $county }} County from Date Range: {{ Carbon::parse($startDate)->format('m-d-Y') }} - {{ Carbon::parse($endDate)->format('m-d-Y') }}
            <span class="float-right"><a href="/ems" class="btn btn-secondary btn-sm">Return To Dashboard</a></span>
        </div>
        <div class="card-body">

            <table class="table table-dark table-bordered">
                <thead>
                    <th>Average Miles Out</th>
                    <th>Average Response Time</th>
                    <th>Average Time Out of Service</th>
                </thead>
                <tbody>
                    <td>{{$report->miles_out}}</td>
                    <td>{{$report->response_time}}</td>
                    <td>{{$report->out_service}}</td>
                </tbody>
            </table>

        </div>
    </div>

@endsection

@endif