{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('EMS') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

<div class="card-deck">
    <div class="card bg-dark text-white mb-3">
        <div class="card-header">
            New Level Zero Entry
            <span class="float-right"><a href="/ems" class="btn btn-sm btn-primary">Return to Dashboard</a></span>
        </div>
        <form method="POST" action="/ems/zero">
            {{ csrf_field() }}

            <div class="card-body">

                <div class="form-group">
                    <label>Date</label>
                    <input type="date" name="date" class="form-control" required>
                </div>

                <div class="form-group">
                    <label>Total Number of Times Level Zero was Reached</label>
                    <input type="number" class="form-control" name="total_level_zero" required>
                </div>
                
            </div>

            <div class="card-footer">
                <input type="submit" class="btn btn-primary btn-block" value="Submit">
            </div>

        </form>
    </div>

    <div class="card bg-dark text-white mb-3">
        <div class="card-header">
            Level Zero Entries
            <span class="float-right"><a href="/ems" class="btn btn-sm btn-primary">Return to Dashboard</a></span>
        </div>
        <div class="card-body" style="height:790px; overflow-y: auto;">

            <table class="table table-dark table-hover">
                <thead>
                    <th>Date</th>
                    <th>Total</th>
                    <th>Delete</th>
                </thead>
                <tbody>
                    @forelse($zeros as $zero)
                        <tr>
                            <td> {{ Carbon::parse($zero->date)->format('m-Y') }} </td>
                            <td> {{ $zero->total_level_zero }} </td>
                            <td> <a href="#" zero_id="{{$zero->id}}" class="btn btn-danger btn-block btn-sm zeroDelete">Delete</a> </td>
                        </tr>
                    @empty 
                        <tr class="text-danger text-center">
                            <td colspan="4">NO DATA IN TABLE</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>

        </div>
    </div>
</div>

@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="application/javascript">

        $(document).on("click", ".zeroDelete", function () {
            //alert($(this).attr("data-cost_center"));
            var zero_id = $(this).attr("zero_id");
            DeleteZero(zero_id);
        });

        function DeleteZero(zero_id) {
            swal({
                title: "Delete Level Zero Entry?",
                text: "Deleting this Entry cannot be undone.",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                confirmButtonText: "Yes, delete it!",
                confirmButtonColor: "#ec6c62"
            }, function() {
                $.ajax({
                    url: "/ems/zero/destroy/" + zero_id,
                    type: "GET"
                })
                    .done(function(data) {
                        swal({
                                title: "Deleted",
                                text: "Entry Deleted",
                                type: "success",
                                timer: 1800,
                                showConfirmButton: false

                            },function() {
                                location.reload();
                            });
                    })
                    .error(function(data) {
                        swal("Oops", "We couldn't connect to the server!", "error");
                    });
            });
        }

    </script>

@endsection

@endif