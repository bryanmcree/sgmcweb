{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('EMS') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}

    {{-- <meta http-equiv="refresh" content="1"> --}}


<style>
    #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #floating-panel {
        position: absolute;
        top: 10px;
        left: 25%;
        z-index: 5;
        background-color: #fff;
        padding: 5px;
        border: 1px solid #999;
        text-align: center;
        font-family: 'Roboto','sans-serif';
        line-height: 30px;
        padding-left: 10px;
      }
</style>

@section('content')


    <div id="map"></div>

    {{-- Old Script --}}
    <script>

        var addMark = <?= json_encode($addresses); ?>;

        function initMap() {
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 9,
                center: {lat: 30.831048, lng: -83.278934}
            });
            var geocoder = new google.maps.Geocoder();

            geocodeAddress(geocoder, map);
        }

        function geocodeAddress(geocoder, resultsMap)
        {
            var address = addMark[0].incident_address;
            var addID = addMark[0].id;

            console.log(address, addID);

            geocoder.geocode({'address': address + 'Georgia'}, function(results, status)
            {
                if (status === 'OK')
                {
                    var marker = new google.maps.Marker({
                        map: resultsMap,
                        position: results[0].geometry.location
                    });

                    console.log('SUCCESS ' + status + results[0].geometry.location);

                    var coordinates = results[0].geometry.location

                    $.ajax({
                        url:'/ems/scripting/' + addID + '/' + coordinates,
                        type: 'get'
                    });

                    // console.log(results[0].geometry.bounds, results[0].geometry.bounds, results[0].geometry.location, results[0].geometry.location);
                    // if(results[0].geometry.bounds != undefined)
                    // {
                    //     console.log('SUCCESS ' + status + results[0].geometry.location);

                    //     var latitude = results[0].geometry.bounds.na.l
                    //     var longitude = results[0].geometry.bounds.ia.l

                    //     $.ajax({
                    //         url:'/ems/test/' + addID + '/' + latitude + '/' + longitude,
                    //         type: 'get'
                    //     });
                    // }
                    // else
                    // {
                    //     console.log('FAILED ' + status + results[0].geometry.location);

                    //     var latitude = 'NA'
                    //     var longitude = 'NA'

                    //     $.ajax({
                    //         url:'/ems/test/' + addID + '/' + latitude + '/' + longitude,
                    //         type: 'get'
                    //     });
                    // }
                    
                }
                else
                {
                    console.log('FAILED ' + status);

                    var coordinates = 'NA'

                    $.ajax({
                        url:'/ems/scripting/' + addID + '/' + coordinates,
                        type: 'get'
                    });
                }

            });
         

        }
    </script>

     <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDDqCLn5-u1VpkILKXPs71_u-jBaLRGJy4&callback=initMap">
    </script>

    
@endsection


@section('scripts')



@endsection
@endif

// geometry.bounds.na.l == lat || geometry.bounds.ia.l == long