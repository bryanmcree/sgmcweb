<div class="modal fade" id="EditUnclassified_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header modal-header-custom">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><b>Fix Unclassified Record</b></h4>
            </div>
            <div class="modal-body bg-default">
                <form method="post" action="/ems/editUnclassified">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" class="id">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Incident Number</label>
                        <input type="text" name="incident_number" class="form-control incident_number" id="exampleInputPassword1" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Incident Key</label>
                        <select class="form-control" name="incident_key" id="exampleFormControlSelect1" required>
                            <option value="">[Select Incident Key]</option>
                            <option value="Lanier 911 Transport">Lanier 911 Transport</option>
                            <option value="Lowndes 911 Transport">Lowndes 911 Transport</option>
                            <option value="Lowndes NET Transport">Lowndes NET Transport</option>
                            <option value="Lanier NET Transport">Lanier NET Transport</option>
                        </select>
                    </div>
                    <input type="Submit" class="btn btn-sm btn btn-default" value="Fix Record">
                </form>
            </div>
        </div>
    </div>
</div>