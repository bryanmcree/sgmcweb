<div class="modal fade" id="levelZero" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document" style="max-width: 30%">
        <div class="modal-content text-white bg-dark">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Total at Level Zero</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form method="POST" action="/ems/zero">
                {{ csrf_field() }}

                <div class="modal-body">

                    <div class="form-group">
                        <label>Month</label>
                        <select class="form-control" name="month" required>
                            <option selected value=''>[Select Month]</option>
                            <option value='Janaury'>Janaury</option>
                            <option value='February'>February</option>
                            <option value='March'>March</option>
                            <option value='April'>April</option>
                            <option value='May'>May</option>
                            <option value='June'>June</option>
                            <option value='July'>July</option>
                            <option value='August'>August</option>
                            <option value='September'>September</option>
                            <option value='October'>October</option>
                            <option value='November'>November</option>
                            <option value='December'>December</option>
                        </select> 
                    </div>

                    <div class="form-group">
                        <label>Year</label>
                        <input type="text" name="year" class="form-control" placeholder="Ex: 2019" required>
                    </div>

                    <div class="form-group">
                        <label>Total Number of Times Level Zero was Reached</label>
                        <input type="number" class="form-control" name="total_level_zero" required>
                    </div>
                   
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Submit">
                </div>

            </form>
        </div>
    </div>
</div>