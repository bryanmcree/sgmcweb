<h1>Don't forget!</h1>

<P>Completion of imaging surveys are due by the end of the month.  You have <b>{{(\Carbon\Carbon::now()->lastOfMonth()->day - \Carbon\Carbon::now()->day)+1}}</b> days left!</P>

<P>To complete a survey visit <a href="http://web.sgmc.org/imaging">http://web.sgmc.org/imaging</a> </P>

<P>View your progress by visiting <a href="http://web.sgmc.org/imaging/admin">http://web.sgmc.org/imaging/admin</a> </P>

<p>Works best in Google Chrome!</p>