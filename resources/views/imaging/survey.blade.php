{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Imaging-Admin') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="card bg-dark mb-3 text-white">
            <div class="card card-header">
                <h2>Imaging Survey</h2>
            </div>
            <div class="card-body">
                {!! Form::open(array('action' => ['ImagingController@submit_survey'], 'class' => 'form_control')) !!}
                {!! Form::hidden('completed_by', Auth::user()->first_name .' ' .Auth::user()->last_name) !!}
                {!! Form::hidden('cost_center', $area) !!}
                <table class="table table-bordered">

                    <tbody>
                    <tr>
                        <td  colspan="2">Survey completed by</td>
                        <td colspan="3" align="right">{{Auth::user()->first_name}} {{Auth::user()->last_name}}</td>
                    </tr>
                    <tr>
                        <td colspan="2">Current Date</td>
                        <td colspan="3" align="right">{{\Carbon\Carbon::now()->format('m-d-Y')}}</td>
                    </tr>
                    <tr>
                        <td colspan="2">Cost Center</td>
                        <td colspan="3" align="right">
                            {{$area}} - {{$cost_center_name}}
                        </td>
                    </tr>
                    @if($cost_center_name =='IR')
                    <tr>
                        <td colspan="2">Dr Name</td>
                        <td colspan="3" align="right">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text bg-danger" id="basic-addon1"><i class="fa fa-exclamation-triangle"></i></span>
                                </div>
                                {!! Form::text('dr_name', null, ['class' => 'form-control', 'id'=>'accession_number', 'required']) !!}
                            </div>
                        </td>
                    </tr>
                    @endif
                    <tr>
                        <td colspan="2">Accession Number</td>
                        <td colspan="3" align="right">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-danger" id="basic-addon1"><i class="fa fa-exclamation-triangle"></i></span>
                                    </div>
                                    {!! Form::text('accession_number', null, ['class' => 'form-control', 'id'=>'accession_number', 'required']) !!}
                                </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">Date of Service</td>
                        <td colspan="3" align="right">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text bg-danger" id="basic-addon1"><i class="fa fa-exclamation-triangle"></i></span>
                                </div>
                                {!! Form::input('date','date_of_service', '', ['class' => 'form-control','required']) !!}
                            </div>
                        </td>
                    </tr>

                    <tr bgcolor="#d3d3d3">
                        <td></td>
                        <td><b>Survey Questions</b></td>
                        <td align="center"><b>Yes</b></td>
                        <td align="center"><b>No</b></td>
                        <td align="center"><b>N/A</b></td>
                    </tr>
                    <?php $rowcount = 0 ?>
                    @foreach ($questions as $question)
                        <?php $rowcount = $rowcount + 1?>
                        <tr>
                            <td><b>{{$rowcount}})</b></td>
                            <td>{{$question->question}}</td>
                            <td align="center"><input type="radio" name="response[{{$question->id}}][answer]" value="Yes"></td>
                            <td align="center"><input type="radio" name="response[{{$question->id}}][answer]" value="No"></td>
                            <td align="center"><input type="radio" name="response[{{$question->id}}][answer]" value="NA"></td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="2">Submit another survey?</td>
                        <td colspan="3" align="center">
                            <input type="checkbox" name="another_survey" value="YES" checked> YES
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                {!! Form::submit('Submit Survey', ['class'=>'btn btn-sgmc', 'id'=>'updateButton']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
@endif