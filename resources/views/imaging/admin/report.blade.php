{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Imaging-Admin') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="card bg-dark mb-3 text-white">
            <div class="card card-header">
                <b>Monthly Dashboard</b> <a href="/imaging" class="btn btn-primary btn-sm pull-right">Submit Survey</a>
            </div>
            <div class="card-body">
                <div class="col-lg-4">
                    <div class="card-body">
                        <div class="form-horizontal">
                            {!! Form::open(array('url' => '/hr/exit/report', 'method' => 'POST', 'files' => true,'class' => 'form_control')) !!}
                            {!! Form::label('start_date', 'Start Date:') !!}
                            {!! Form::input('date','start_date', '', ['class' => 'form-control','required']) !!}
                            <BR>
                            {!! Form::label('end_date', 'End Date:') !!}
                            {!! Form::input('date','end_date', '', ['class' => 'form-control','required']) !!}
                            <BR>
                            <div class="RightLeft">
                                {!! Form::reset('Clear Form', array('class' => 'btn btn-default')) !!}
                                {!! Form::submit('Run Report', array('class' => 'btn btn-sgmc')) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

                <div class="card-deck">
                    <div class="card bg-dark mb-3 text-white border-info">
                        <div class="card-header">
                            <b>Total Surveys</b>
                        </div>
                        <div class="card-body">
                            <table class="table table-condensed">
                                <tr>
                                    <td><b>Total</b></td>
                                    <td align="right">{{$cost_centers->sum('surveys')}}</td>
                                </tr>
                                <tr>
                                    <td><b>Completed</b></td>
                                    <td align="right">{{$completed->count()}}</td>
                                </tr>
                                <tr>
                                    <td><b>Remaining</b></td>
                                    <td align="right">{{$cost_centers->sum('surveys') - $completed->count()}}</td>
                                </tr>
                                <tr>
                                    <td><b>Percent</b></td>
                                    <td align="right">{{number_format((float)($completed->count()/$cost_centers->sum('surveys'))*100,2,'.','') }}%</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="card bg-dark mb-3 text-white border-info">
                        <div class="card-header">
                            <b>Question Breakdown</b>
                        </div>
                        <div class="card-body">
                            <table class="table table-condensed">
                                <tr>
                                    <td><b>Total</b></td>
                                    <td align="center">{{$monthly_questions}}</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td><b># of "Yes"</b></td>
                                    <td align="center">{{$monthly_yes}}</td>
                                    <td align="right">@if(is_null($monthly_yes)) 0 @else {{number_format((float)($monthly_yes/$monthly_questions)*100,2,'.','') }}% @endif</td>
                                </tr>
                                <tr>
                                    <td><b># of "No"</b></td>
                                    <td align="center">{{$monthly_no}}</td>
                                    <td align="right">@if(is_null($monthly_no)) 0 @else {{number_format((float)($monthly_no/$monthly_questions)*100,2,'.','') }}% @endif</td>
                                </tr>
                                <tr>
                                    <td><b># of "N/A"</b></td>
                                    <td align="center">{{$monthly_na}}</td>
                                    <td align="right">@if(is_null($monthly_na)) 0 @else {{number_format((float)($monthly_na/$monthly_questions)*100,2,'.','') }}% @endif</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="card bg-dark mb-3 text-white border-info">
                        <div class="card-header">
                            <b>IR Procedure Success</b>
                        </div>
                        <div class="card-body" style="height:175px;">
                            <div id="ir"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>



    <div class="col-lg-8 m-auto">
        <div class="card bg-dark mb-3 text-white">
            <div class="card-header">
                <b>Monthly Stats</b>
            </div>

            <div class="card-body" style="height:400px; overflow-y: auto;">
                <table class="table table-condensed">
                    <thead>
                    <tr>
                        <td><b>Cost Center</b></td>
                        <td align="center"><b># to do</b></td>
                        <td align="center"><b># done</b></td>
                        <td align="center"><b># left</b></td>
                        <td align="right"><b>Responsible Party</b></td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($cost_center_list as $costcenter)
                        <tr>
                            <td>{{$costcenter->cost_center_name}} ({{$costcenter->cost_center}})</td>
                            <td align="center">{{$costcenter->surveys}}</td>
                            <td align="center">{{$data->where('cost_center',$costcenter->cost_center)}}</td>
                            <td align="center"></td>
                            <td align="right">{{$costcenter->manager->first_name}} {{$costcenter->manager->last_name}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="col-lg-8 m-auto">
        <div class="card bg-dark mb-3 text-white">
            <div class="card-header">
                <b>Completed Surveys</b>
            </div>
            <div class="card-body" style="height:400px; overflow-y: auto;">
                @if($completed->isEmpty())
                    <div class="alert alert-info">There are no completed surveys for this month.</div>
                @else


                    <table class="table table-hover table-condensed">
                        <thead>
                        <tr>
                            <td><b>Completed By</b></td>
                            <td><b>Date</b></td>
                            <td><b>Cost Center</b></td>
                            <td><b>Accession #</b></td>
                            <td><b>Date of Service</b></td>
                            <td></td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($completed as $done)
                            <tr>
                                <td>{{$done->completed_by}}</td>
                                <td>{{$done->created_at}}</td>
                                <td>{{$done->cost_centers->cost_center_name}} ({{$done->cost_centers->cost_center}})</td>
                                <td>{{$done->accession_number}}</td>
                                <td>{{$done->date_of_service}}</td>
                                <td align="right">[Button]</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>


@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="application/javascript">

        //IR value
        var bar = new ProgressBar.SemiCircle(ir, {
                strokeWidth: 6,
                color: '#FFEA82',
                trailColor: '#eee',
                trailWidth: 1,
                easing: 'easeInOut',
                duration: 1400,
                svgStyle: null,
                text: {
                    value: '',
                    alignToBottom: false
                },
                from: {color: '#f44242'},
                to: {color: '#41f453'},
                // Set default step function for all animate calls
                step: (state, bar) => {
                bar.path.setAttribute('stroke', state.color);
        var value = Math.round( @if(is_null($ir_no)) 0 @else  {{100-(number_format((float)($ir_no/($ir_procedures))*100,2,'.','')) }} @endif);
        if (value === 0) {
            bar.setText('');
        } else {
            bar.setText( @if(is_null($ir_no)) 0 @else  {{100-(number_format((float)($ir_no/($ir_procedures))*100,2,'.','')) }} +'%' @endif);
        }

        bar.text.style.color = state.color;
        }
        });
        bar.text.style.fontFamily = '"Raleway", Helvetica, sans-serif';
        bar.text.style.fontSize = '2rem';

        bar.animate( @if(is_null($ir_no)) 0 @else {{(100-(number_format((float)($ir_no/($ir_procedures))*100,2,'.','')))/100 }} @endif);  // Number from 0.0 to 1.0

    </script>

@endsection
@endif