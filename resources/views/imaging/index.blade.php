{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Imaging-Admin') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="card bg-dark mb-3 text-white">
            <div class="card card-header">
                <b>Select Cost Center</b>
            </div>
            <div class="card-body">

                {!! Form::open(array('action' => ['ImagingController@survey'], 'class' => 'form_control')) !!}
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text bg-danger" id="basic-addon1"><i class="fa fa-exclamation-triangle"></i></span>
                    </div>
                    <select name="area" class="form-control" required>
                        <option value="">[Select Area]</option>
                        @foreach($areas as $costcenter)
                            <option value="{{ $costcenter->cost_center }}">{{ $costcenter->cost_center }} - {{$costcenter->cost_center_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="card-footer">
                {!! Form::submit('Select Area', ['class'=>'btn btn-sgmc', 'id'=>'updateButton']) !!}
                <a href="imaging/admin" class="btn btn-primary">Skip to Admin card</a>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection
@endif