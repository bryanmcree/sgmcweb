{{--New file Template--}}

{{--Add Security for this page below--}}
@if( Auth::user()->hasRole('Readmission') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

<ul class="nav nav-tabs text-white bg-dark" id="myTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Main</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Index</a>
    </li>
    <li class="nav-item">
        <a class="nav-link"  href="/readmissions" >Back</a>
    </li>
</ul>
<div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
        <div class="card mb-4 text-white bg-dark">
            <div class="card-header">
                <b>Readmission Tracking Entry</b>
            </div>
            <div class="card-body">
                <div class="card-deck">
                    <div class="card mb-4 text-white bg-dark">
                        <div class="card-header">
                            <b>Patient Name</b>
                        </div>
                        <div class="card-body">
                            {{$patient->patient_name}} ({{$patient->patient_mrn}})
                        </div>
                    </div>
                    <div class="card mb-4 text-white bg-dark">
                        <div class="card-header">
                            <b>Readmit Disposition</b>
                        </div>
                        <div class="card-body">
                            {{$patient->readmit_disposition}}
                        </div>
                    </div>
                </div>
                <div class="card-deck">

                    <div class="card mb-4 text-white bg-dark">
                        <div class="card-header">
                            <b>PD Date</b>
                        </div>
                        <div class="card-body">
                            {{$patient->index_disch_date->format('m-d-Y')}}
                        </div>
                    </div>
                    <div class="card mb-4 text-white bg-dark">
                        <div class="card-header">
                            <b>Discharge Provider</b>
                        </div>
                        <div class="card-body">
                            {{$patient->index_disch_attending}}
                        </div>
                    </div>
                    <div class="card mb-4 text-white bg-dark">
                        <div class="card-header">
                            <b>Readmit Date</b>
                        </div>
                        <div class="card-body">
                            {{$patient->readmit_adm_date->format('m-d-Y')}}
                        </div>
                    </div>
                    <div class="card mb-4 text-white bg-dark">
                        <div class="card-header">
                            <b>Readmit Provider</b>
                        </div>
                        <div class="card-body">
                            {{$patient->readmit_attending}}
                        </div>
                    </div>
                    <div class="card mb-4 text-white bg-dark">
                        <div class="card-header">
                            <b>Readmit LOS</b>
                        </div>
                        <div class="card-body">
                            {{$patient->readmit_los}}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
        <div class="card mb-4 text-white bg-dark">
            <div class="card-header">
                <b>Readmission Tracking Entry</b>
            </div>
            <div class="card-body">
                <div class="card-deck">
                    <div class="card mb-4 text-white bg-dark">
                        <div class="card-header">
                            <b>Index Final DRG</b>
                        </div>
                        <div class="card-body">
                            {{$patient->index_final_drg_id}} - {{$patient->index_final_drg}}
                        </div>
                    </div>
                    <div class="card mb-4 text-white bg-dark">
                        <div class="card-header">
                            <b>Index Disposition</b>
                        </div>
                        <div class="card-body">
                            {{$patient->index_disposition}}
                        </div>
                    </div>
                </div>
                <div class="card-deck">

                    <div class="card mb-4 text-white bg-dark">
                        <div class="card-header">
                            <b>Index LOS</b>
                        </div>
                        <div class="card-body">
                            {{$patient->index_los}}
                        </div>
                    </div>
                    <div class="card mb-4 text-white bg-dark">
                        <div class="card-header">
                            <b>Patient Age</b>
                        </div>
                        <div class="card-body">
                            {{$patient->patient_age}}
                        </div>
                    </div>
                    <div class="card mb-4 text-white bg-dark">
                        <div class="card-header">
                            <b>Index Dish Dept</b>
                        </div>
                        <div class="card-body">
                            {{$patient->index_disch_dept}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">...</div>
</div>


<div class="card mb-4 text-white bg-secondary">
    <div class="card-header  bg-primary">
        <b>Accurate PCP information in Epic?</b>
    </div>
    <div class="card-body">
        <div class="card-deck">
            <div class="card mb-4 text-white bg-dark">
                <div class="card-header">
                    <b>Last Discharge (Y/N)</b>
                </div>
                <div class="card-body">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                        <label class="form-check-label" for="exampleRadios1">
                            Yes
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                        <label class="form-check-label" for="exampleRadios2">
                            No
                        </label>
                    </div>
                </div>
            </div>
            <div class="card mb-4 text-white bg-dark">
                <div class="card-header">
                    <b>This hospital stay (Y/N)</b>
                </div>
                <div class="card-body">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                        <label class="form-check-label" for="exampleRadios1">
                            Yes
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                        <label class="form-check-label" for="exampleRadios2">
                            No
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card mb-4 text-white bg-secondary">
    <div class="card-header bg-primary">
        <b>Preventable</b>
    </div>
    <div class="card-body">
        <div class="card-deck">
            <div class="card mb-4 text-white bg-dark">
                <div class="card-header">
                    <b>Was this preventable? (Y/N)</b>
                </div>
                <div class="card-body">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                        <label class="form-check-label" for="exampleRadios1">
                            Yes
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                        <label class="form-check-label" for="exampleRadios2">
                            No
                        </label>
                    </div>
                </div>
            </div>
            <div class="card mb-4 text-white bg-dark">
                <div class="card-header">
                    <b>Discharged too soon? (Y/N)</b>
                </div>
                <div class="card-body">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                        <label class="form-check-label" for="exampleRadios1">
                            Yes
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                        <label class="form-check-label" for="exampleRadios2">
                            No
                        </label>
                    </div>
                </div>
            </div>
            <div class="card mb-4 text-white bg-dark">
                <div class="card-header">
                    <b>Discharge medication list error? (Y/N)</b>
                </div>
                <div class="card-body">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                        <label class="form-check-label" for="exampleRadios1">
                            Yes
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                        <label class="form-check-label" for="exampleRadios2">
                            No
                        </label>
                    </div>
                </div>
            </div>
            <div class="card mb-4 text-white bg-dark">
                <div class="card-header">
                    <b>Discharge / Prevention Notes</b>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card mb-4 text-white bg-secondary">
    <div class="card-header bg-primary">
        <b>Discharge Events</b>
    </div>
    <div class="card-body">
        <div class="card-deck">
            <div class="card mb-4 text-white bg-dark">
                <div class="card-header">
                    <b>Improper disposition? (Y/N)</b>
                </div>
                <div class="card-body">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                        <label class="form-check-label" for="exampleRadios1">
                            Yes
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                        <label class="form-check-label" for="exampleRadios2">
                            No
                        </label>
                    </div>
                </div>
            </div>
            <div class="card mb-4 text-white bg-dark">
                <div class="card-header">
                    <b>AMA last time? (Y/N)</b>
                </div>
                <div class="card-body">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                        <label class="form-check-label" for="exampleRadios1">
                            Yes
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                        <label class="form-check-label" for="exampleRadios2">
                            No
                        </label>
                    </div>
                </div>
            </div>
            <div class="card mb-4 text-white bg-dark">
                <div class="card-header">
                    <b>Physician recommendation? (Y/N)</b>
                </div>
                <div class="card-body">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                        <label class="form-check-label" for="exampleRadios1">
                            Yes
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                        <label class="form-check-label" for="exampleRadios2">
                            No
                        </label>
                    </div>
                </div>
            </div>
            <div class="card mb-4 text-white bg-dark">
                <div class="card-header">
                    <b>Discharge Event Notes</b>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card mb-4 text-white bg-secondary">
    <div class="card-header bg-primary">
        <b>Patient Follow Up</b>
    </div>
    <div class="card-body">
        <div class="card-deck">
            <div class="card mb-4 text-white bg-dark">
                <div class="card-header">
                    <b>Did patient follow up? (Y/N)</b>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <select class="form-control" id="exampleFormControlSelect1">
                            <option>Yes - with PCP</option>
                            <option>Yes - With Consultant</option>
                            <option>NO</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="card mb-4 text-white bg-dark">
                <div class="card-header">
                    <b>AMA last time? (Y/N)</b>
                </div>
                <div class="card-body">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                        <label class="form-check-label" for="exampleRadios1">
                            Yes
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                        <label class="form-check-label" for="exampleRadios2">
                            No
                        </label>
                    </div>
                </div>
            </div>
            <div class="card mb-4 text-white bg-dark">
                <div class="card-header">
                    <b>Physician recommendation? (Y/N)</b>
                </div>
                <div class="card-body">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                        <label class="form-check-label" for="exampleRadios1">
                            Yes
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                        <label class="form-check-label" for="exampleRadios2">
                            No
                        </label>
                    </div>
                </div>
            </div>
            <div class="card mb-4 text-white bg-dark">
                <div class="card-header">
                    <b>Patient Follow Up Notes</b>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-deck">
            <div class="card mb-4 text-white bg-dark">
                <div class="card-header">
                    <b>Patient had PCP at the time of admission? (Y/N)</b>
                </div>
                <div class="card-body">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                        <label class="form-check-label" for="exampleRadios1">
                            Yes
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                        <label class="form-check-label" for="exampleRadios2">
                            No
                        </label>
                    </div>
                </div>
            </div>
            <div class="card mb-4 text-white bg-dark">
                <div class="card-header">
                    <b>Appointment was mad before DC? (Y/N)</b>
                </div>
                <div class="card-body">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                        <label class="form-check-label" for="exampleRadios1">
                            Yes
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                        <label class="form-check-label" for="exampleRadios2">
                            No
                        </label>
                    </div>
                </div>
            </div>
            <div class="card mb-4 text-white bg-dark">
                <div class="card-header">
                    <b>New PCP (other than partneship for health)  was established before DC? (Y/N)</b>
                </div>
                <div class="card-body">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                        <label class="form-check-label" for="exampleRadios1">
                            Yes
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                        <label class="form-check-label" for="exampleRadios2">
                            No
                        </label>
                    </div>
                </div>
            </div>
            <div class="card mb-4 text-white bg-dark">
                <div class="card-header">
                    <b>Partnership referral completed by now? (Y/N) </b>
                </div>
                <div class="card-body">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                        <label class="form-check-label" for="exampleRadios1">
                            Yes
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                        <label class="form-check-label" for="exampleRadios2">
                            No
                        </label>
                    </div>
                </div>
            </div>
            <div class="card mb-4 text-white bg-dark">
                <div class="card-header">
                    <b>New PCP estiblished before DC? (Y/N) </b>
                </div>
                <div class="card-body">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                        <label class="form-check-label" for="exampleRadios1">
                            Yes
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                        <label class="form-check-label" for="exampleRadios2">
                            No
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card mb-4 text-white bg-secondary">
    <div class="card-header bg-primary">
        <b>Prescription Elements</b>
    </div>
    <div class="card-body">
        <div class="card-deck">
            <div class="card mb-4 text-white bg-dark">
                <div class="card-header">
                    <b>Prescriptions filled?</b>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <select class="form-control" id="exampleFormControlSelect1">
                            <option>Yes</option>
                            <option>NO - Lack of funds</option>
                            <option>NO - Did not go to pharmacy</option>
                            <option>NO - Other</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="card mb-4 text-white bg-dark">
                <div class="card-header">
                    <b>Social Issue? (Y/N)</b>
                </div>
                <div class="card-body">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                        <label class="form-check-label" for="exampleRadios1">
                            Yes
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                        <label class="form-check-label" for="exampleRadios2">
                            No
                        </label>
                    </div>
                </div>
            </div>
            <div class="card mb-4 text-white bg-dark">
                <div class="card-header">
                    <b>Poor education on discharge? (Y/N)</b>
                </div>
                <div class="card-body">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                        <label class="form-check-label" for="exampleRadios1">
                            Yes
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                        <label class="form-check-label" for="exampleRadios2">
                            No
                        </label>
                    </div>
                </div>
            </div>
            <div class="card mb-4 text-white bg-dark">
                <div class="card-header">
                    <b>Patient non compliant? (Y/N)</b>
                </div>
                <div class="card-body">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                        <label class="form-check-label" for="exampleRadios1">
                            Yes
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                        <label class="form-check-label" for="exampleRadios2">
                            No
                        </label>
                    </div>
                </div>
            </div>
            <div class="card mb-4 text-white bg-dark">
                <div class="card-header">
                    <b>Prescription Notes</b>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card mb-4 text-white bg-secondary">
    <div class="card-header bg-primary">
        <b>Hospice Eligibility</b>
    </div>
    <div class="card-body">
        <div class="card-deck">
            <div class="card mb-4 text-white bg-dark">
                <div class="card-header">
                    <b>Hospice canidate (Y/N)?</b>
                </div>
                <div class="card-body">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                        <label class="form-check-label" for="exampleRadios1">
                            Yes
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                        <label class="form-check-label" for="exampleRadios2">
                            No
                        </label>
                    </div>
                </div>
            </div>
            <div class="card mb-4 text-white bg-dark">
                <div class="card-header">
                    <b>Hospice offered by MD? (Y/N)</b>
                </div>
                <div class="card-body">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                        <label class="form-check-label" for="exampleRadios1">
                            Yes
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                        <label class="form-check-label" for="exampleRadios2">
                            No
                        </label>
                    </div>
                </div>
            </div>
            <div class="card mb-4 text-white bg-dark">
                <div class="card-header">
                    <b>Palliative team consult completed? (Y/N)</b>
                </div>
                <div class="card-body">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                        <label class="form-check-label" for="exampleRadios1">
                            Yes
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                        <label class="form-check-label" for="exampleRadios2">
                            No
                        </label>
                    </div>
                </div>
            </div>
            <div class="card mb-4 text-white bg-dark">
                <div class="card-header ">
                    <b>Discharged with Hospice? (Y/N)</b>
                </div>
                <div class="card-body">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                        <label class="form-check-label" for="exampleRadios1">
                            Yes
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                        <label class="form-check-label" for="exampleRadios2">
                            No
                        </label>
                    </div>
                </div>
            </div>
            <div class="card mb-4 text-white bg-dark">
                <div class="card-header ">
                    <b>Hospice Notes</b>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')
    <script type="application/javascript">
        $(document).ready(function() {
            $('#10days').DataTable( {
                "pageLength": 20,
                "order": [],
                "columnDefs": [ {
                    "targets"  : 'no-sort',
                    "orderable": false,
                }]
            } );
        } );
    </script>


@endsection

@endif