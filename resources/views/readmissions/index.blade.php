{{--New file Template--}}

{{--Add Security for this page below--}}
@if( Auth::user()->hasRole('Readmission') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    <nav class="navbar navbar-expand-lg navbar-dark bg-primary mb-4 test-white">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Reports</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Graphs
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" href="#">Disabled</a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="card-deck mb-4">
        <div class="card text-white bg-dark">
            <div class="card-header bg-secondary">
                <b>Since GoLive</b>
            </div>
            <div class="card-body">
                <div class="card-deck">
                    <div class="card text-white bg-dark">
                        <div class="card-header text-center">
                            <b>Readmits</b>
                        </div>
                        <div class="card-body text-center">
                            {{$total_golive->count()}}
                        </div>
                    </div>
                    <div class="card text-white bg-dark">
                        <div class="card-header text-center">
                            <b>Adv Org LOS</b>
                        </div>
                        <div class="card-body text-center">
                            {{number_format($total_golive->avg('index_los'),2)}}
                        </div>
                    </div>
                    <div class="card text-white bg-dark">
                        <div class="card-header text-center">
                            <b>Adv ReAdm LOS</b>
                        </div>
                        <div class="card-body text-center">
                            {{number_format($total_golive->avg('readmit_los'),2)}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card text-white bg-dark">
            <div class="card-header bg-warning">
                <b>Last Year Stats</b>
            </div>
            <div class="card-body">
                <div class="card-deck">
                    <div class="card text-white bg-dark">
                        <div class="card-header text-center">
                            <b>Readmits</b>
                        </div>
                        <div class="card-body text-center">
                            {{$total_last_year->count()}}
                        </div>
                    </div>
                    <div class="card text-white bg-dark">
                        <div class="card-header text-center">
                            <b>Adv Org LOS</b>
                        </div>
                        <div class="card-body text-center">
                            {{number_format($total_last_year->avg('index_los'),2)}}
                        </div>
                    </div>
                    <div class="card text-white bg-dark">
                        <div class="card-header text-center">
                            <b>Adv ReAdm LOS</b>
                        </div>
                        <div class="card-body text-center">
                            {{number_format($total_last_year->avg('readmit_los'),2)}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card text-white bg-dark">
            <div class="card-header bg-success">
                <b>Current Year Stats</b>
            </div>
            <div class="card-body">
                <div class="card-deck">
                    <div class="card text-white bg-dark">
                        <div class="card-header text-center">
                            <b>Readmits</b>
                        </div>
                        <div class="card-body text-center">
                            {{$total_year->count()}}
                        </div>
                    </div>
                    <div class="card text-white bg-dark">
                        <div class="card-header text-center">
                            <b>Adv Org LOS</b>
                        </div>
                        <div class="card-body text-center">
                            {{number_format($total_year->avg('index_los'),2)}}
                        </div>
                    </div>
                    <div class="card text-white bg-dark">
                        <div class="card-header text-center">
                            <b>Adv ReAdm LOS</b>
                        </div>
                        <div class="card-body text-center">
                            {{number_format($total_year->avg('readmit_los'),2)}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card mb-4 text-white bg-dark">
        <div class="card-header bg-danger">
            <b>Readmission's for the Last 10 Days</b> ({{$patients->count()}})
        </div>
        <div class="card-body" style="height:450px; overflow-y: auto;">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <td><b>Patient Name</b></td>
                        <td align="center"><b>Same DX</b></td>
                        <td><b>Date of Prev Disc</b></td>
                        <td><b>Disc Prov</b></td>
                        <td><b>Days</b></td>
                        <td><b>Readmit Date</b></td>
                        <td><b>Readmit Prov</b></td>
                        <td><b>Readmit Disp</b></td>
                        <TD></TD>
                    </tr>
                </thead>
                <tbody>
                @foreach($patients as $patient)
                    <tr>
                        <td>{{$patient->patient_name}} ({{$patient->patient_mrn}})</td>
                        <td align="center">@if($patient->same_dx == 1) Yes @else No @endif</td>
                        <td>{{$patient->index_disch_date->format('m-d-Y')}}</td>
                        <td>{{$patient->index_disch_attending}}</td>
                        <td>{{$patient->readmit_adm_date->diffForHumans()}}</td>
                        <td>{{$patient->readmit_adm_date->format('m-d-Y')}}</td>
                        <td>{{$patient->readmit_attending}}</td>
                        <td>{{$patient->readmit_disposition}}</td>
                        <td align="right"><a href="/readmissions/survey/{{$patient->id}}"><i class="fas fa-poll-h"></i></a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>


@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')
    <script type="application/javascript">
        $(document).ready(function() {
            $('#10days').DataTable( {
                "pageLength": 20,
                "order": [],
                "columnDefs": [ {
                    "targets"  : 'no-sort',
                    "orderable": false,
                }]
            } );
        } );
    </script>


@endsection

@endif