<!-- Modal -->
<div class="modal fade" id="censusReport" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content text-white bg-dark">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Census Report</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="/census/report">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Select Unit</label>
                        <select class="form-control" name="unit" id="exampleFormControlSelect1" required>
                            <option value="" selected>[Select Unit]</option>
                            @foreach($all_units as $unit)
                                <option value="{{$unit->unit}}">{{$unit->unit}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Start Date / Time</label>
                        <input type="datetime-local" name="startdate" class="form-control" id="exampleFormControlInput1">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1">End Date / Time</label>
                        <input type="datetime-local" name="enddate" class="form-control" id="exampleFormControlInput1">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Run Report">
                </div>
            </form>
        </div>
    </div>
</div>