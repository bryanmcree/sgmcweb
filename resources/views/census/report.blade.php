{{--New file Template--}}


    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1

    --}}
@section('content')


    <div class="mb-4 border-info text-white bg-dark">
        <div class="card-header">
            {{$current_census->first()->unit}} ({{$floor_ratio->ratio}} to 1) <i>Between {{$startDate}} and {{$endDate}}</i>
        </div>
        <div class="card-body">
            <a href="#" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#censusReport">New Report</a> <a href="/census" class="btn btn-sm btn-primary">Return to Census</a>
            <canvas id="unit_chart" height="100"></canvas>
        </div>
    </div>

    @include('census.modal.report')
@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">
    Chart.defaults.global.defaultFontColor = '#fff';
    new Chart(document.getElementById('unit_chart'), {
        type: 'bar',
        data: {
            labels: [
                @foreach($current_census as $time)
                    "{{$time->created_at->format('m-d-Y H:i')}}",
                @endforeach
            ],
            datasets: [
                {
                    label: "Nurse Ratio",
                    type: "line",
                    borderColor: "#ff0000",
                    data: [
                        @foreach($current_census as $emp)
                                @if(($emp->total_rn+$emp->total_lpn)-1 > 0) {{number_format($emp->total_patients/(($emp->total_rn+$emp->total_lpn)-1),2)}} @else 0 @endif,
                        @endforeach
                    ],
                    fill: false,
                    borderWidth: 1,
                    pointRadius: 0,
                    //lable:'line'
                },
                {
                    label: "Floor Ratio",
                    type: "line",
                    borderColor: "#00ff00",
                    data: [
                        @foreach($current_census as $emp)
                        {{$floor_ratio->ratio}},
                        @endforeach
                    ],
                    fill: false,
                    pointRadius: 0,
                    borderWidth: 1,
                    //lable:'line'

                },
                {
                label: "{{$current_census->first()->unit}}",
                type: "line",
                borderColor: "#cca43d",
                data: [
                    @foreach($current_census as $census)
                    {{$census->total_patients}},
                    @endforeach
                ],
                fill: false,

                },
                {
                    label: "RN",
                    type: "bar",
                    backgroundColor: "#8e5ea2",
                    stack: 'Stack 0',
                    data: [
                        @foreach($current_census as $emp)
                        {{$emp->total_rn - $emp->total_traveler}},
                        @endforeach
                    ],
                },
                {
                    label: "Traveler",
                    type: "bar",
                    backgroundColor: "#c45850",
                    stack: 'Stack 0',
                    data: [
                        @foreach($current_census as $emp)
                        {{$emp->total_traveler}},
                        @endforeach
                    ],
                },
                {
                    label: "LPN",
                    type: "bar",
                    backgroundColor: "#3cba9f",
                    stack: 'Stack 0',
                    data: [
                        @foreach($current_census as $emp)
                        {{$emp->total_lpn}},
                        @endforeach
                    ],
                },
                {
                    label: "Support Staff",
                    type: "bar",
                    backgroundColor: "#e8c3b9",
                    stack: 'Stack 0',
                    data: [
                        @foreach($current_census as $emp)
                        {{$emp->total_support_staff}},
                        @endforeach
                    ],
                },
            ]
        },
        options: {
            responsive: true,
            title: {
                display: false,
                text: 'Population growth (millions): Europe & Africa'
            },
            legend: { display: true },
            scales:{
                xAxes: [{
                    stacked: true,
                    display:false
                }
                ],
            }
        }
    });
</script>

@endsection
