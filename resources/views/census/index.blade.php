{{--New file Template--}}

{{--Add Security for this page below--}}


    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1

    --}}
@section('content')

    <div class="mb-4 border-info text-white bg-dark">
        <div class="card-header">
            SGMC Census
        </div>
        <div class="card-body">

            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">DASHBOARD</a>
                </li>
                @foreach($distinct_unit as $unit)
                <li class="nav-item">
                    <a class="nav-link" id="{{$unit->unit}}-tab" data-toggle="tab" href="#{{$unit->unit}}" role="tab" aria-controls="{{$unit->unit}}" aria-selected="false">{{$unit->unit}} @if(!empty($current_census->where('unit',$unit->unit)->first()->total_traveler)) @if($current_census->where('unit',$unit->unit)->first()->total_traveler > 0  ) <i class="fas fa-plane" style="color:red;"></i> @endif @endif</a>
                </li>
                @endforeach
                <li class="nav-item">
                    <a class="nav-link" id="notes-tab" data-toggle="tab" href="#notes" role="tab" aria-controls="notes" aria-selected="false">NOTES</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="history-tab" data-toggle="tab" href="#history" role="tab" aria-controls="history" aria-selected="false">HISTORY</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="history-tab" data-toggle="modal" data-target="#censusReport" role="tab" aria-controls="history" aria-selected="false">REPORT</a>
                </li>


            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <br>
                    <div class="card-deck">
                        <div class="card  mb-4 border-success text-white bg-dark">
                            <div class="card-header text-center">
                                <b>Total Census</b>
                            </div>
                            <div class="card-body text-center">
                                <div class="row">
                                    <div class="col-6">
                                        <div style="font-weight: bold; font-size: 50px;">{{$total_census->sum('total_patients')}}</div>
                                    </div>
                                    <div class="col-6">
                                        <canvas class="" id="total_patient_trend" title="12 Hour Trend"></canvas>
                                    </div>
                                </div>
                                <div class="row tm-2">
                                    <div class="col-4">
                                        Low
                                    </div>
                                    <div class="col-4">
                                        Adv
                                    </div>
                                    <div class="col-4">
                                        High
                                    </div>
                                </div>
                                <div class="row tm-2">
                                    <div class="col-4">
                                        {{$all_total_census->min('total_patients')}}
                                    </div>
                                    <div class="col-4">
                                        {{round($all_total_census->avg('total_patients'))}}
                                    </div>
                                    <div class="col-4">
                                        {{$all_total_census->max('total_patients')}}
                                    </div>
                                </div>
                                <div class="row tm-2">
                                    <div class="col-12">
                                        <canvas class="" id="total_patient_trend_weekly" height="60" title="3 Day Trend"></canvas>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <i>Total bedded patients plus ER patients</i>
                            </div>
                        </div>
                        <div class="card  mb-4 border-warning text-white bg-dark">
                            <div class="card-header text-center">
                                <b>ED Census</b>
                            </div>
                            <div class="card-body text-center">
                                <div class="row">
                                    <div class="col-6">
                                        <div style="font-weight: bold; font-size: 50px;">{{$total_census_ed->total_patients}}</div>
                                    </div>
                                    <div class="col-6">
                                        <canvas class="" id="total_ed_trend" title="12 Hour Trend"></canvas>
                                    </div>
                                </div>
                                <div class="row tm-2">
                                    <div class="col-4">
                                        Low
                                    </div>
                                    <div class="col-4">
                                        Adv
                                    </div>
                                    <div class="col-4">
                                        High
                                    </div>
                                </div>
                                <div class="row tm-2">
                                    <div class="col-4">
                                        {{$all_total_census->min('total_ed_patients')}}
                                    </div>
                                    <div class="col-4">
                                        {{round($all_total_census->avg('total_ed_patients'))}}
                                    </div>
                                    <div class="col-4">
                                        {{$all_total_census->max('total_ed_patients')}}
                                    </div>
                                </div>
                                <div class="row tm-2">
                                    <div class="col-12">
                                        <canvas class="" id="total_ed_trend_weekly" height="60" title="3 Day Trend"></canvas>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <i>Total ED patients</i>
                            </div>
                        </div>
                        <div class="card  mb-4 border-success text-white bg-dark">
                            <div class="card-header text-center">
                                <b>Occupancy</b>
                            </div>
                            <div class="card-body text-center">
                                <div class="row">
                                    <div class="col-6 text-center">
                                        <div style="font-weight: bold; font-size: 50px;">{{number_format(($total_census->sum('total_patients')/364)*100,0)}}%</div>
                                    </div>
                                    <div class="col-6">
                                        <canvas class="" id="total_occ_trend" title="12 Hour Trend"></canvas>
                                    </div>
                                </div>
                                <div class="row tm-2">
                                    <div class="col-4">
                                        Low
                                    </div>
                                    <div class="col-4">
                                        Adv
                                    </div>
                                    <div class="col-4">
                                        High
                                    </div>
                                </div>
                                <div class="row tm-2">
                                    <div class="col-4">
                                        {{round($all_total_census->min('occ_precent'))}}%
                                    </div>
                                    <div class="col-4">
                                        {{round($all_total_census->avg('occ_precent'))}}%
                                    </div>
                                    <div class="col-4">
                                        {{round($all_total_census->max('occ_precent'))}}%
                                    </div>
                                </div>
                                <div class="row tm-2">
                                    <div class="col-12">
                                        <canvas class="" id="total_occ_trend_weekly" height="60" title="3 Day Trend"></canvas>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <i>Total bedded patients</i>
                            </div>
                        </div>
                        <div class="card  mb-4 border-success text-white bg-dark">
                            <div class="card-header text-center">
                                <b>Total Clocked in Employees</b>
                            </div>
                            <div class="card-body text-center">
                                <div class="row">
                                    <div class="col-6 text-center">
                                        <div style="font-weight: bold; font-size: 50px;">{{$current_census->sum('total_staff')}}</div>
                                    </div>
                                    <div class="col-6">
                                        <canvas class="" id="total_clock_trend" title="12 Hour Trend"></canvas>
                                    </div>
                                </div>
                                <div class="row tm-2">
                                    <div class="col-4">
                                        Low
                                    </div>
                                    <div class="col-4">
                                        Adv
                                    </div>
                                    <div class="col-4">
                                        High
                                    </div>
                                </div>
                                <div class="row tm-2">
                                    <div class="col-4">
                                        {{round($all_total_census->min('total_staff'))}}
                                    </div>
                                    <div class="col-4">
                                        {{round($all_total_census->avg('total_staff'))}}
                                    </div>
                                    <div class="col-4">
                                        {{round($all_total_census->max('total_staff'))}}
                                    </div>
                                </div>
                                <div class="row tm-2">
                                    <div class="col-12">
                                        <canvas class="" id="total_clock_trend_weekly" height="60" title="3 Day Trend"></canvas>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <i>Total employees clocked in</i>
                            </div>
                        </div>
                        <div class="card  mb-4 border-danger text-white bg-dark">
                            <div class="card-header text-center">
                                <b>Travelers</b>
                            </div>
                            <div class="card-body text-center">
                                <div class="row">
                                    <div class="col-6 text-center">
                                        <div style="font-weight: bold; font-size: 50px;">{{$current_census->sum('total_traveler')}}</div>
                                    </div>
                                    <div class="col-6">
                                        <canvas class="" id="total_travelers_trend" title="12 Hour Trend"></canvas>
                                    </div>
                                </div>
                                <div class="row tm-2">
                                    <div class="col-4">
                                        Low
                                    </div>
                                    <div class="col-4">
                                        Adv
                                    </div>
                                    <div class="col-4">
                                        High
                                    </div>
                                </div>
                                <div class="row tm-2">
                                    <div class="col-4">
                                        {{round($all_total_census->min('total_travelers'))}}
                                    </div>
                                    <div class="col-4">
                                        {{round($all_total_census->avg('total_travelers'))}}
                                    </div>
                                    <div class="col-4">
                                        {{round($all_total_census->max('total_travelers'))}}
                                    </div>
                                </div>
                                <div class="row tm-2">
                                    <div class="col-12">
                                        <canvas class="" id="total_travelers_trend_weekly" height="60" title="3 Day Trend"></canvas>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <i>Total number travelers clocked in</i>
                            </div>
                        </div>
                    </div>
                    <div class="card-deck">
                        <div class="card mb-4 border-info text-white bg-dark">
                            <div class="card-header text-center">
                                <b>Page Refreshed</b>
                            </div>
                            <div class="card-body text-center">
                                <div style="font-weight: bold; font-size: 50px;">{{Carbon\Carbon::now()->format('H:i')}}</div>
                            </div>
                        </div>
                        <div class="card mb-4 border-info text-white bg-dark">
                            <div class="card-header text-center">
                                <b>Data Refreshed</b>
                            </div>
                            <div class="card-body text-center">
                                <div style="font-weight: bold; font-size: 50px;">{{$total_census->first()->created_at->format('H:i')}}</div>
                            </div>
                        </div>
                    </div>
                </div>
                @foreach($distinct_unit as $unit)
                <div class="tab-pane fade" id="{{$unit->unit}}" role="tabpanel" aria-labelledby="{{$unit->unit}}-tab">
                    <div class="card mb-4 mt-2 border-primary text-white bg-dark">
                        <div class="card-body">
                            <canvas id="{{$unit->unit}}_chart" height="50"></canvas>
                        </div>
                    </div>
                    <div class="card mb-4 border-success text-white bg-dark">
                        <div class="card-header">
                            <b>Patient to Nurse Ratio @if(empty($floor_ratio->where('unit',$unit->unit)->first())) N/A @else {{$floor_ratio->where('unit',$unit->unit)->first()->ratio}} - 1 @endif</b>
                        </div>
                        <div class="card-body" style="overflow-x: auto;">
                            <table class="table table-condensed table-bordered">
                                <tr>
                                    @foreach($daily_census->where('unit',$unit->unit) as $time)
                                        <td>{{$time->created_at->format('H:i')}}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    @foreach($daily_census->where('unit',$unit->unit) as $emp)
                                        <td>@if(($emp->total_rn+$emp->total_lpn)-1 > 0) {{number_format($emp->total_patients/(($emp->total_rn+$emp->total_lpn)-1),2)}} @else 0 @endif</td>
                                    @endforeach
                                </tr>
                            </table>
                        </div>
                        <div class="card-footer">
                            <i>Note that there is a global assumption that a PCC is working on each floor and not taking patients.  The ratio above will reflect minus one nurse to account for this.</i>
                        </div>
                    </div>

                    <div class="card mb-4 border-info text-white bg-dark">
                        <div class="card-header">
                            <b>Patient to Support Staff Ratio</b>
                        </div>
                        <div class="card-body" style="overflow-x: auto;">
                            <table class="table table-condensed table-bordered">
                                <tr>
                                    @foreach($daily_census->where('unit',$unit->unit) as $time)
                                        <td>{{$time->created_at->format('H:i')}}</td>
                                    @endforeach
                                </tr>
                                <tr>
                                    @foreach($daily_census->where('unit',$unit->unit) as $emp)
                                        <td>
                                            @if($unit->unit == 'SGA2T' OR $unit->unit == 'SGA4T' OR $unit->unit == 'SGA3T')
                                                @if(($emp->total_support_staff) > 1) {{number_format($emp->total_patients/($emp->total_support_staff-1 ),2)}} @else 0 @endif
                                            @else

                                                @if(($emp->total_support_staff) > 0) {{number_format($emp->total_patients/($emp->total_support_staff),2)}} @else 0 @endif

                                            @endif

                                        </td>
                                    @endforeach
                                </tr>
                            </table>
                        </div>
                        <div class="card-footer">
                            <i>Support staff includes anyone who's job code is NOT a registered nurse, senior staff nurse or patient care coordinator. @if($unit->unit == 'SGA2T' OR $unit->unit == 'SGA4T' OR $unit->unit == 'SGA3T') -1 support person because floor does not use huc's or monitor techs and uses PCT III to fill these roles. @endif</i>
                        </div>
                    </div>

                    <div class="card mb-4 border-warning text-white bg-dark">
                        <div class="card-header">
                            <b>Clocked in Employees ({{$clockedin->where('cost_center', $unit->cost_center)->count()}})</b>
                        </div>
                        <div class="card-body">
                            <table class="table table-condensed table-bordered">
                                <thead>
                                    <tr>
                                        <td><b>Pic</b></td>
                                        <td><b>Emp #</b></td>
                                        <td><b>Name</b></td>
                                        <td><b>Job Code</b></td>
                                        <td><b>Title</b></td>
                                        <td align="right"><b>Updated</b></td>

                                    </tr>
                                </thead>
                                @foreach($clockedin->where('cost_center', $unit->cost_center) as $clocked_emp)
                                <tr>
                                    <td>@if($clocked_emp->empoyee != '')<a href="data:image/jpeg;base64,{{str_replace('</photo>','',str_replace('<photo>','',$clocked_emp->empoyee->photo))}}" target="_blank"><img src="data:image/jpeg;base64,{{str_replace('</photo>','',str_replace('<photo>','',$clocked_emp->empoyee->photo))}}" height="60" width="60"/></a>@else {{ Html::image('img/nopic.png', 'alt', array( 'width' => 60, 'height' => 60 )) }} @endif</td>
                                    @if(!empty($clocked_emp->empoyee->employee_number)) <td style="vertical-align: middle;">{{$clocked_emp->employee_number}}</td> @endif
                                    @if(!empty($clocked_emp->empoyee->name)) <td style="vertical-align: middle;">{{$clocked_emp->empoyee->name}}</td> @endif
                                    @if(!empty($clocked_emp->empoyee->job_code)) <td style="vertical-align: middle;">{{$clocked_emp->empoyee->job_code}}</td> @endif
                                    @if(!empty($clocked_emp->empoyee->title))<td style="vertical-align: middle;">{{$clocked_emp->empoyee->title}}</td> @endif
                                    <td align="right" style="vertical-align: middle;">{{$clocked_emp->created_at}}</td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>

                    <div class="card mb-4 border-danger text-white bg-dark">
                        <div class="card-header">
                            <b>Patient Count ({{$current_patients->where('unit', $unit->unit)->count()}})</b>
                        </div>
                        <div class="card-body">
                            <table class="table table-condensed table-bordered">
                                <thead>
                                <tr>
                                    <td><b>Unit</b></td>
                                    <td><b>Bed</b></td>
                                    <td><b>Bed Status</b></td>
                                    <td><b>Accom Code</b></td>
                                    <td align="right"><b>Updated</b></td>

                                </tr>
                                </thead>
                                @foreach($current_patients->where('unit', $unit->unit) as $patients)
                                    <tr>
                                        <td>{{$patients->unit}}</td>
                                        <td style="vertical-align: middle;">{{$patients->bed}}</td>
                                        <td style="vertical-align: middle;">{{$patients->bed_status}}</td>
                                        <td style="vertical-align: middle;">{{$patients->accom_code}}</td>
                                        <td align="right" style="vertical-align: middle;">{{$patients->created_at}}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
                    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.js"></script>
                <script type="application/javascript">
                    Chart.defaults.global.defaultFontColor = '#fff';
                    new Chart(document.getElementById('{{$unit->unit}}_chart'), {
                        type: 'bar',
                        data: {
                            labels: [
                                @foreach($daily_census->where('unit',$unit->unit ) as $time)
                                "{{$time->created_at->format('H:i')}}",
                                @endforeach
                            ],
                            datasets: [{
                                label: "{{$unit->unit}}",
                                type: "line",
                                borderColor: "#cca43d",
                                data: [
                                    @foreach($daily_census->where('unit',$unit->unit ) as $census)
                                    {{$census->total_patients}},
                                    @endforeach
                                ],
                                fill: false,

                            },
                                {
                                    label: "Nurse Ratio",
                                    type: "line",
                                    borderColor: "#ff0000",
                                    data: [
                                        @foreach($daily_census->where('unit',$unit->unit ) as $emp)
                                                @if(($emp->total_rn+$emp->total_lpn)-1 > 0) {{number_format($emp->total_patients/(($emp->total_rn+$emp->total_lpn)-1),2)}} @else 0 @endif,
                                        @endforeach
                                    ],
                                    fill: false,
                                    borderWidth: 1,
                                    pointRadius: 0,
                                    //lable:'line'
                                },
                                {
                                    label: "Floor Ratio",
                                    type: "line",
                                    borderColor: "#00ff00",
                                    data: [
                                        @foreach($daily_census->where('unit',$unit->unit ) as $emp)
                                            @if(empty($floor_ratio->where('unit',$unit->unit)->first()))
                                                'NA',
                                            @else
                                                {{$floor_ratio->where('unit',$unit->unit)->first()->ratio}},
                                            @endif
                                        @endforeach
                                    ],
                                    fill: false,
                                    pointRadius: 0,
                                    borderWidth: 1,
                                    //lable:'line'

                                },

                                {
                                    label: "RN",
                                    type: "bar",
                                    backgroundColor: "#8e5ea2",
                                    stack: 'Stack 0',
                                    data: [
                                        @foreach($daily_census->where('unit',$unit->unit) as $emp)
                                        {{$emp->total_rn - $emp->total_traveler}},
                                        @endforeach
                                    ],
                                },
                                {
                                    label: "Traveler",
                                    type: "bar",
                                    backgroundColor: "#c45850",
                                    stack: 'Stack 0',
                                    data: [
                                        @foreach($daily_census->where('unit',$unit->unit) as $emp)
                                        {{$emp->total_traveler}},
                                        @endforeach
                                    ],
                                },
                                {
                                    label: "LPN",
                                    type: "bar",
                                    backgroundColor: "#3cba9f",
                                    stack: 'Stack 0',
                                    data: [
                                        @foreach($daily_census->where('unit',$unit->unit) as $emp)
                                        {{$emp->total_lpn}},
                                        @endforeach
                                    ],
                                },
                                {
                                    label: "Support Staff",
                                    type: "bar",
                                    backgroundColor: "#e8c3b9",
                                    stack: 'Stack 0',
                                    data: [
                                        @foreach($daily_census->where('unit',$unit->unit) as $emp)
                                        {{$emp->total_support_staff}},
                                        @endforeach
                                    ],
                                },

                            ]
                        },
                        options: {
                            title: {
                                display: false,
                                text: 'Population growth (millions): Europe & Africa'
                            },
                            legend: { display: true },
                            scales:{
                                xAxes: [{
                                    stacked: true,
                                    display:false
                                }]
                            }
                        }
                    });
                </script>

                @endforeach
                <div class="tab-pane fade" id="notes" role="tabpanel" aria-labelledby="history-notes">
                    <div class="card mt-2 text-white bg-dark">
                        <div class="card-header">
                            <b>Notes</b>
                        </div>
                        <div class="card-body">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item bg-dark">RN totals include job code 639 Patient Care Coordinator, 640 Registered Nurse, 701 Senior Staff Nurse, 521 Weekend PCC, Weekend RN 700, 702 Weekend RN Level 2.
                                <li class="list-group-item bg-dark">LPN totals include job code 740 Licensed Practical Nurse.</li>
                                <li class="list-group-item bg-dark">Traveler totals are identified by a 5 digit number starting with 8 and are always assumed to be RN's.</li>
                                <li class="list-group-item bg-dark">Support staff are all employees left after filtering out RN's and LPN's and excluding 987 HUC I, 986 HCC 2, 988 Monitor Tech 1 and 808 Monitor Techs 2.
                                <li class="list-group-item bg-dark">Census data includes observation and is listed as total bedded patients with the exception of ER.</li>
                                <li class="list-group-item bg-dark">Page refreshed is the time when the user loaded the census page.</li>
                                <li class="list-group-item bg-dark">Data refreshed is the time when new census and API data is loaded.</li>
                                <li class="list-group-item bg-dark">Clocked in employees are those who are physically clocked into API at the last data update.  These do not include those who are in orientation, education or epic education.</li>
                                <li class="list-group-item bg-dark">Nurse to patient ratio is does not consider patient acuity and is ([Total Patients]/(([Total Nurses]+[Total LPN])-[1 PCC])).  Assumes that each floor has one PCC who does not take care of patients and is non-productive.</li>
                                <li class="list-group-item bg-dark"><i class="fas fa-plane" style="color:red;"></i> Indicates travelers are clocked into that unit.</li>
                                <li class="list-group-item bg-dark">Employees who fail to clock in/out, or are not clocked into the appropriate cost center will affect ratios.</li>
                                <li class="list-group-item bg-dark">Patients who leave to go to the OR remain in the census totals as they will be returning to their room.</li>
                                <li class="list-group-item bg-dark">Census totals do not include outpatient. </li>
                                <li class="list-group-item bg-dark">All data is for a 12 hour period. </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="history" role="tabpanel" aria-labelledby="history-tab">
                    <div class="card-deck mt-2">
                        <div class="card mb-4 text-white bg-dark border-primary">
                            <div class="card-header">
                                <b>Census View History by User</b>
                            </div>
                            <div class="card-body" style="height:580px; overflow-y: auto;">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <td><b>Name</b></td>
                                        <td><b>Emp#</b></td>
                                        <td><b>Title</b></td>
                                        <td align="right"><b>Views</b></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($census_history_group as $census_history_user)
                                        <tr>
                                            <td>{{$census_history_user->user_id->name}}</td>
                                            <td>{{$census_history_user->user_id->employee_number}}</td>
                                            <td>{{$census_history_user->user_id->title}}</td>
                                            <td align="right">{{$census_history_user->user_total}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card mb-4 text-white bg-dark border-primary">
                            <div class="card-header">
                                <b>Census View History</b> ({{$census_history->count()}})
                            </div>
                            <div class="card-body" style="height:580px; overflow-y: auto;">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <td><b>Name</b></td>
                                        <td><b>Emp#</b></td>
                                        <td><b>IP Address</b></td>
                                        <td align="right"><b>Date</b></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($census_history as $census_history)
                                        <tr>
                                            <td>{{$census_history->user_id->name}}</td>
                                            <td>{{$census_history->user_id->employee_number}}</td>
                                            <td>{{$census_history->user_ip}}</td>
                                            <td align="right">{{$census_history->created_at}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@include('census.modal.report')
@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">

    //Total Patients Trend
    new Chart(document.getElementById("total_patient_trend"), {
        type: 'line',
        data: {
            labels: [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050],
            datasets: [{
                data: [
                    @foreach($trends as $total_patient_trend)
                    {{$total_patient_trend->total_patients}},
                    @endforeach
                ],
                backgroundColor: "rgba(47, 152, 208, 0.1)",
                borderColor: "#3e95cd",
                borderWidth: 2,
                fill: true,
            }
            ]
        },
        options: {
            title: {
                display: false,
                text: 'World population per region (in millions)'
            },
            legend: {
                display: false
            },
            tooltips: {
                enabled: false
            },
            scales: {
                xAxes: [{
                    ticks: {
                        display: false
                    }
                }],
                yAxes: [{
                    ticks: {
                        display: false
                    }
                }]
            },
            elements: {
                point:{
                    radius: 0
                }
            }
        }
    });


    //Total Patients WEEKLY Trend
    new Chart(document.getElementById("total_patient_trend_weekly"), {
        type: 'line',
        data: {
            labels: [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050],
            datasets: [{
                data: [
                    @foreach($trends_weekly as $total_patient_trend)
                    {{$total_patient_trend->total_patients}},
                    @endforeach
                ],
                backgroundColor: "rgba(47, 152, 208, 0.1)",
                borderColor: "#3e95cd",
                borderWidth: 2,
                fill: true,
            }
            ]
        },
        options: {
            title: {
                display: false,
                text: 'World population per region (in millions)'
            },
            legend: {
                display: false
            },
            tooltips: {
                enabled: false
            },
            scales: {
                xAxes: [{
                    ticks: {
                        display: false
                    }
                }],
                yAxes: [{
                    ticks: {
                        display: false
                    }
                }]
            },
            elements: {
                point:{
                    radius: 0
                }
            }
        }
    });




    //Total ED Patients Trend
    new Chart(document.getElementById("total_ed_trend"), {
        type: 'line',
        data: {
            labels: [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050],
            datasets: [{
                data: [
                    @foreach($trends as $total_patient_trend)
                    {{$total_patient_trend->total_ed_patients}},
                    @endforeach
                ],
                backgroundColor: "rgba(47, 152, 208, 0.1)",
                borderColor: "#3e95cd",
                borderWidth: 2,
                fill: true,
            }
            ]
        },
        options: {
            title: {
                display: false,
                text: 'World population per region (in millions)'
            },
            legend: {
                display: false
            },
            tooltips: {
                enabled: false
            },
            scales: {
                xAxes: [{
                    ticks: {
                        display: false
                    }
                }],
                yAxes: [{
                    ticks: {
                        display: false
                    }
                }]
            },
            elements: {
                point:{
                    radius: 0
                }
            }
        }
    });


    //Total ED Patients WEEKLY Trend
    new Chart(document.getElementById("total_ed_trend_weekly"), {
        type: 'line',
        data: {
            labels: [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050],
            datasets: [{
                data: [
                    @foreach($trends_weekly as $total_patient_trend)
                    {{$total_patient_trend->total_ed_patients}},
                    @endforeach
                ],
                backgroundColor: "rgba(47, 152, 208, 0.1)",
                borderColor: "#3e95cd",
                borderWidth: 2,
                fill: true,
            }
            ]
        },
        options: {
            title: {
                display: false,
                text: 'World population per region (in millions)'
            },
            legend: {
                display: false
            },
            tooltips: {
                enabled: false
            },
            scales: {
                xAxes: [{
                    ticks: {
                        display: false
                    }
                }],
                yAxes: [{
                    ticks: {
                        display: false
                    }
                }]
            },
            elements: {
                point:{
                    radius: 0
                }
            }
        }
    });

    //Total occ Trend
    new Chart(document.getElementById("total_occ_trend"), {
        type: 'line',
        data: {
            labels: [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050],
            datasets: [{
                data: [
                    @foreach($trends as $total_patient_trend)
                    {{$total_patient_trend->occ_precent}},
                    @endforeach
                ],
                backgroundColor: "rgba(47, 152, 208, 0.1)",
                borderColor: "#3e95cd",
                borderWidth: 2,
                fill: true
            }
            ]
        },
        options: {
            title: {
                display: false,
                text: 'World population per region (in millions)'
            },
            legend: {
                display: false
            },
            tooltips: {
                enabled: false
            },
            scales: {
                xAxes: [{
                    ticks: {
                        display: false
                    }
                }],
                yAxes: [{
                    ticks: {
                        display: false
                    }
                }]
            },
            elements: {
                point:{
                    radius: 0
                }
            }
        }
    });


    //Total occ Trend
    new Chart(document.getElementById("total_occ_trend_weekly"), {
        type: 'line',
        data: {
            labels: [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050],
            datasets: [{
                data: [
                    @foreach($trends_weekly as $total_patient_trend)
                    {{$total_patient_trend->occ_precent}},
                    @endforeach
                ],
                backgroundColor: "rgba(47, 152, 208, 0.1)",
                borderColor: "#3e95cd",
                borderWidth: 2,
                fill: true
            }
            ]
        },
        options: {
            title: {
                display: false,
                text: 'World population per region (in millions)'
            },
            legend: {
                display: false
            },
            tooltips: {
                enabled: false
            },
            scales: {
                xAxes: [{
                    ticks: {
                        display: false
                    }
                }],
                yAxes: [{
                    ticks: {
                        display: false
                    }
                }]
            },
            elements: {
                point:{
                    radius: 0
                }
            }
        }
    });

    //Total clocked in Trend
    new Chart(document.getElementById("total_clock_trend"), {
        type: 'line',
        data: {
            labels: [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050],
            datasets: [{
                data: [
                    @foreach($trends as $total_patient_trend)
                    {{$total_patient_trend->total_staff}},
                    @endforeach
                ],
                backgroundColor: "rgba(47, 152, 208, 0.1)",
                borderColor: "#3e95cd",
                borderWidth: 2,
                fill: true
            }
            ]
        },
        options: {
            title: {
                display: false,
                text: 'World population per region (in millions)'
            },
            legend: {
                display: false
            },
            tooltips: {
                enabled: false
            },
            scales: {
                xAxes: [{
                    ticks: {
                        display: false
                    }
                }],
                yAxes: [{
                    ticks: {
                        display: false
                    }
                }]
            },
            elements: {
                point:{
                    radius: 0
                }
            }
        }
    });

    //Total clocked in Trend
    new Chart(document.getElementById("total_clock_trend_weekly"), {
        type: 'line',
        data: {
            labels: [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050],
            datasets: [{
                data: [
                    @foreach($trends_weekly as $total_patient_trend)
                    {{$total_patient_trend->total_staff}},
                    @endforeach
                ],
                backgroundColor: "rgba(47, 152, 208, 0.1)",
                borderColor: "#3e95cd",
                borderWidth: 2,
                fill: true
            }
            ]
        },
        options: {
            title: {
                display: false,
                text: 'World population per region (in millions)'
            },
            legend: {
                display: false
            },
            tooltips: {
                enabled: false
            },
            scales: {
                xAxes: [{
                    ticks: {
                        display: false
                    }
                }],
                yAxes: [{
                    ticks: {
                        display: false
                    }
                }]
            },
            elements: {
                point:{
                    radius: 0
                }
            }
        }
    });

    //Total travelers in Trend
    new Chart(document.getElementById("total_travelers_trend"), {
        type: 'line',
        data: {
            labels: [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050],
            datasets: [{
                data: [
                    @foreach($trends as $total_patient_trend)
                    {{$total_patient_trend->total_travelers}},
                    @endforeach
                ],
                backgroundColor: "rgba(245, 18, 7, 0.1)",
                borderColor: "#F51207",
                borderWidth: 2,
                fill: true
            }
            ]
        },
        options: {
            title: {
                display: false,
                text: 'World population per region (in millions)'
            },
            legend: {
                display: false
            },
            tooltips: {
                enabled: false
            },
            scales: {
                xAxes: [{
                    ticks: {
                        display: false
                    }
                }],
                yAxes: [{
                    ticks: {
                        display: false
                    }
                }]
            },
            elements: {
                point:{
                    radius: 0
                }
            }
        }
    });

    //Total travelers in Trend
    new Chart(document.getElementById("total_travelers_trend_weekly"), {
        type: 'line',
        data: {
            labels: [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050],
            datasets: [{
                data: [
                    @foreach($trends_weekly as $total_patient_trend)
                    {{$total_patient_trend->total_travelers}},
                    @endforeach
                ],
                backgroundColor: "rgba(245, 18, 7, 0.1)",
                borderColor: "#F51207",
                borderWidth: 2,
                fill: true
            }
            ]
        },
        options: {
            title: {
                display: false,
                text: 'World population per region (in millions)'
            },
            legend: {
                display: false
            },
            tooltips: {
                enabled: false
            },
            scales: {
                xAxes: [{
                    ticks: {
                        display: false
                    }
                }],
                yAxes: [{
                    ticks: {
                        display: false
                    }
                }]
            },
            elements: {
                point:{
                    radius: 0
                }
            }
        }
    });


    //Return user to proper tab
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var hash = $(e.target).attr('href');
        if (history.pushState) {
            history.pushState(null, null, hash);
        } else {
            location.hash = hash;
        }
    });

    var hash = window.location.hash;
    if (hash) {
        $('.nav-link[href="' + hash + '"]').tab('show');
    }
</script>

@endsection
