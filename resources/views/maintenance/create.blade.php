{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.app')
{{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')


<div class="card mb-4 text-white bg-dark">
    <div class="card-header">
        Create New Item
        <span class="float-right">
            <a href="/maintenance" class="btn btn-sm btn-primary">Return Home</a>
        </span>
    </div>
    <div class="card-body">

        <form action="/maintenance/store" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="created_by" value="{{ Auth::user()->employee_number }}">

            <div class="form-row">

                <div class="form-group col-lg-4">
                    <label for="">Item Name</label>
                    <input type="text" name="item" class="form-control">
                </div>

                <div class="form-group col-lg-4">
                    <label for="">Site</label>
                    <select name="site" class="form-control">
                        <option value="">[Choose Site]</option>
                        <option value="Main Campus">Main Campus</option>
                        <option value="Lanier">Lanier</option>
                        <option value="SNH">SNH</option>
                        <option value="Berrien">Berrien</option>
                        <option value="Villa">Villa</option>
                    </select>
                </div>

                <div class="form-group col-lg-4">
                    <label for="">Building</label>
                    <select name="building" class="form-control">
                        <option value="">[Choose Building]</option>
                        <option value="Main Campus">Main Campus</option>
                        <option value="Professional Building">Professional Building</option>
                        <option value="Dasher Heart Center & Patient Tower">Dasher Heart Center & Patient Tower</option>
                        <option value="Administrative Services Building">Administrative Services Building</option>
                        <option value="Surgery Center">Surgery Center</option>
                        <option value="Patient Finance">Patient Finance</option>
                    </select>
                </div>

            </div>

            <div class="form-row">

                <div class="form-group col-lg-3">
                    <label for="">Location</label>
                    <input type="text" name="location" class="form-control">
                </div>

                <div class="form-group col-lg-3">
                    <label for="">Asset #</label>
                    <input type="text" name="asset_num" class="form-control">
                </div>

                <div class="form-group col-lg-3">
                    <label for="">Serial #</label>
                    <input type="text" name="serial_num" class="form-control">
                </div>

                <div class="form-group col-lg-3">
                    <label for="">Type</label>
                    <select name="type" class="form-control">
                        <option value="">[Choose Type]</option>
                        <option value="20 min Fire Door">20 min Fire Door</option>
                        <option value="45 min Fire Door">45 min Fire Door</option>
                        <option value="1 hr Fire Door">1 hr Fire Door</option>
                        <option value="1.5 hr Fire Door">1.5 hr Fire Door</option>
                        <option value="Fire Extinguisher">Fire Extinguisher</option>
                    </select>
                </div>

            </div>

            <input type="submit" class="btn btn-primary btn-block">

        </form>

    </div>
</div>


@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

<script>
$(document).ready(function() {
    $('#competenciesTable').DataTable( {
        "pageLength": 25,
        "ordering": false,
        "columnDefs": [ {
            "targets"  : 'no-sort',
            "orderable": false,
        }]
    } );
});
</script>

@endsection
