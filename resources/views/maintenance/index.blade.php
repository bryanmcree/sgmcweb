{{--New file Template--}}

{{--Add Security for this page below--}}


    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')


    <div class="card mb-4 text-white bg-dark">
        <div class="card-header">
            All Items
            <span class="float-right">
                <a href="/maintenance/create" class="btn btn-sm btn-primary">Create New Item</a>
            </span>
        </div>
        <div class="card-body">

            <div class="table-responsive">
            
                <table class="table table-dark table-hover table-bordered" id="competenciesTable">
                    <thead>
                        <th>Title</th>
                        <th>Site</th>
                        <th>Location</th>
                        <th>Asset #</th>
                        <th>Serial #</th>
                        <th>Type</th>
                        <th>Start</th>
                    </thead>
                    <tbody>
                        @foreach ($items as $item)
                            <tr>
                                <td>{{ $item->item }}</td>
                                <td>{{ $item->site }}</td>
                                <td>{{ $item->location }}</td>
                                <td>{{ $item->asset_num }}</td>
                                <td>{{ $item->serial_num }}</td>
                                <td>{{ $item->type }}</td>
                                <td> <a href="/maintenance/complete/{{ $item->id }}" class="btn btn-info btn-sm btn-block">Start</a> </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            
            </div>

        </div>
    </div>


@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

<script>
    $(document).ready(function() {
        $('#competenciesTable').DataTable( {
            "pageLength": 25,
            "ordering": false,
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    });
</script>

@endsection
