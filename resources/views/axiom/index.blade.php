{{--New file Template--}}

{{--Add Security for this page below--}}
@if( Auth::user()->hasRole('Axiom Admin') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')


    <div class="card mb-4 text-white bg-dark">
        <div class="card-header">
            Axiom Resources
        </div>
        <div class="card-body">
            <div class="list-group">
                <a href="/axiom/paycodes" class="list-group-item list-group-item-action">HRP / API Pay Codes</a>
                <a href="/axiom/jobcodes" class="list-group-item list-group-item-action">HRP / API Job Codes</a>
            </div>
        </div>
    </div>


@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')



@endsection

@endif