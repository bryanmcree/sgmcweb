{{--New file Template--}}

{{--Add Security for this page below--}}
@if( Auth::user()->hasRole('Axiom Admin') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')


    <div class="card mb-4 text-white bg-dark">
        <div class="card-header">
            HRP / API Pay Codes and GL Codes
        </div>
        <div class="card-body">
            <table class="table table-hover" id="paycode">
                <thead>
                    <tr>
                        <td><b>Code</b></td>
                        <td><b>Description</b></td>
                        <td><b>Productive Type</b></td>
                        <td><b>GL Account</b></td>
                    </tr>
                </thead>
                <tbody>
                @foreach($pay_codes as $paycode)
                    <tr>
                        <td>{{$paycode->Code}}</td>
                        <td>{{$paycode->Description}}</td>
                        <td>{{$paycode->ProductiveType}}</td>
                        <td>{{$paycode->ChargeNumber}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>


@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="application/javascript">
        $(document).ready(function() {
            $('#paycode').DataTable( {
                "pageLength": 20,
                "order": [],
                "columnDefs": [ {
                    "targets"  : 'no-sort',
                    "orderable": false,
                }]
            } );
        } );
    </script>

@endsection

@endif