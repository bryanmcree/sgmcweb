{{--New file Template--}}

{{--Add Security for this page below--}}


@extends('layouts.phs')
    {{--Page Design Goes Below--}}
@section('content')
<br>

    <div class="col-md-8 col-lg-offset-2">
        <div class="panel panel-default">
            <div class="panel panel-heading" style="font-size: 20px;">
                Welcome to Web.SGMC.org.  Before you begin on your adventure we need some additional information.
            </div>
            <div class="panel-body">
                <b>Please select your immediate supervisor.</b>
                <ul>
                    <li>Use the search box below to find your supervisor by name.</li>
                    <li>Once you have found their name "click" on their name.</li>
                    <li>Then "click" the blue "Assign" button to the right of the search box.</li>
                </ul>
                <form method="post" action="/manager/update">
                    {{ csrf_field() }}
                    <input type="hidden" name="director" id="director">
                    <input type="hidden" name="redirect" value="{{ app('request')->input('redirect') }}">
                    <div class="input-group">
                        <input type="search" id="autocomplete" class="form-control input-lg" placeholder="Search">
                        <div class="input-group-btn">
                            <button class="btn btn-primary btn-lg" type="submit">
                                Assign
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">
    $('#autocomplete').autocomplete({
        serviceUrl: '/scorecard/employeesearch',
        dataType: 'json',
        type:'GET',
        width: 418,
        minChars:2,
        onSelect: function(suggestion) {
            //alert(suggestion.data);
            $("#director").val(suggestion.data);
        }


    });
</script>

@endsection
