{{--New file Template--}}

{{--Add Security for this page below--}}
@if( Auth::user()->hasRole('Daily Scorecard') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}

@section('content')

    <style>

        @keyframes down
        {
            0% {transform: translateY(-3%); opacity: 1;}
            100% { transform: translateY(3%); opacity: 0.2;}
        }

        #move-down{
            -webkit-animation:linear infinite alternate;
            -webkit-animation-name: down;
            -webkit-animation-duration: 1.5s;
        }

        @keyframes up
        {
            from {transform: translateY(10%);}
            to { transform: translateY(-10%); }
        }

        #move-up{
            -webkit-animation:linear infinite alternate;
            -webkit-animation-name: up;
            -webkit-animation-duration: 1.2s;
        }

        @keyframes left
        {
            from {transform: translateX(3%); opacity: 1;}
            to { transform: translateX(-3%); opacity: 0.2;}
        }

        #move-left{
            -webkit-animation:linear infinite alternate;
            -webkit-animation-name: left;
            -webkit-animation-duration: 1.5s;
        }

        @keyframes right
        {
            from {transform: translateX(-3%); opacity: 1;}
            to { transform: translateX(3%); opacity: 0.2;}
        }

        #move-right{
            -webkit-animation:linear infinite alternate;
            -webkit-animation-name: right;
            -webkit-animation-duration: 1.5s;
        }

        #border-bottom{
            border-bottom:solid 1px rgba(74, 74, 74, .3);
        }

    </style>

    @if(is_null($data))
        <div class="alert alert-warning">New data will not be available until after 0630.  Please try back after then.</div>

        @else
    <div class="card mb-4 text-white bg-dark">
        <div class="card-header ">
            <h2>Daily Stats for Yesterday


                <div class="btn-group float-right" role="group" aria-label="Basic example">
                    <a href="/daily/download/overtime" class="btn btn-warning float-right ml-1" title="Download in Excel all of the employees who got overtime yesterday."><i class="fad fa-download"></i> Download Overtime Report</a>
                    <a href="/daily/download/epic_stats" class="btn btn-primary float-right" title="Download in Excel all of the Epic stats for yesterday."><i class="fad fa-download"></i> Download Epic Stats</a>
                    <a href="/daily/download/history" class="btn btn-warning float-right" title="Download in Excel all the previous days."><i class="fad fa-download"></i> Download Historical</a>
                </div>

            </h2>
        </div>
        <div class="card-body">
            <div class="card-deck">
                <div class="card text-white bg-dark">
                    <div class="card-header" style="font-weight: bold; text-decoration: underline">Worked Hours <a href="#" data-toggle="modal" data-target="#worked_hours" class="float-right"><i class="fad fa-question-circle"></i></a> </div>
                    <div class="card-body">
                        <div class="row justify-content-end h-100">
                            <div class="col my-auto">
                                @if($isYesterday)
                                    @if($data->worked_hours > $yesterday->worked_hours)
                                        <div class="float-left my-auto"><i class="fad fa-arrow-square-up fa-4x" style="--fa-primary-color: red;" title="From Yesterday"></i></div>
                                    @else
                                        <div class="float-left my-auto"><i class="fad fa-arrow-square-down fa-4x" style="--fa-primary-color: green;" title="From Yesterday"></i></div>
                                    @endif
                                @endif
                            </div>
                            <div class="col" style="font-size: 45px; float:right">
                                @if(is_null($data->worked_hours))
                                    --
                                @else
                                    {{number_format($data->worked_hours,2)}}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card text-white bg-dark">
                    <div class="card-header" style="font-weight: bold; text-decoration: underline">Paid Hours <a href="#" data-toggle="modal" data-target="#paid_hours" class="float-right"><i class="fad fa-question-circle"></i></a></div>
                    <div class="card-body">
                        <div class="row justify-content-end h-100">
                            <div class="col my-auto">
                                @if($isYesterday)
                                    @if($data->paid_hours > $yesterday->paid_hours)
                                        <div class="float-left my-auto"><i class="fad fa-arrow-square-up fa-4x" style="--fa-primary-color: red;" title="From Yesterday"></i></div>
                                    @else
                                        <div class="float-left my-auto"><i class="fad fa-arrow-square-down fa-4x" style="--fa-primary-color: green;" title="From Yesterday"></i></div>
                                    @endif
                                @endif
                            </div>
                            <div class="col" style="font-size: 45px; float:right">
                                @if(is_null($data->paid_hours))
                                    --
                                @else
                                    {{number_format($data->paid_hours,2)}}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card text-white bg-dark">
                    <div class="card-header" style="font-weight: bold; text-decoration: underline"><a href="#" data-toggle="modal" data-target="#overtime_hours_graph" class="">Overtime Hours </a> <a href="#" data-toggle="modal" data-target="#overtime_hours" class="float-right"><i class="fad fa-question-circle"></i></a></div>
                    <div class="card-body">
                        <div class="row justify-content-end h-100">
                            <div class="col my-auto">
                                @if($isYesterday)
                                    @if($data->overtime_hours > $yesterday->overtime_hours)
                                        <div class="float-left my-auto"><i class="fad fa-arrow-square-up fa-4x" style="--fa-primary-color: red;" title="From Yesterday"></i></div>
                                    @else
                                        <div class="float-left my-auto"><i class="fad fa-arrow-square-down fa-4x" style="--fa-primary-color: green;" title="From Yesterday"></i></div>
                                    @endif
                                @endif
                            </div>
                            <div class="col" style="font-size: 45px; float:right">
                                @if(is_null($data->overtime_hours))
                                    --
                                @else
                                    {{number_format($data->overtime_hours,2)}}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card text-white bg-dark">
                    <div class="card-header" style="font-weight: bold; text-decoration: underline"><a href="#" data-toggle="modal" data-target="#discharges_graph" class="">Discharges</a> <a href="#" data-toggle="modal" data-target="#discharges" class="float-right"><i class="fad fa-question-circle"></i></a></div>
                    <div class="card-body">
                        <div class="row justify-content-end h-100">
                            <div class="col my-auto">
                                @if($isYesterday)
                                    @if($data->discharges > $yesterday->discharges)
                                        <div class="float-left my-auto"><i class="fad fa-arrow-square-up fa-4x" style="--fa-primary-color: red;" title="From Yesterday"></i></div>
                                    @else
                                        <div class="float-left my-auto"><i class="fad fa-arrow-square-down fa-4x" style="--fa-primary-color: green;" title="From Yesterday"></i></div>
                                    @endif
                                @endif
                            </div>
                            <div class="col" style="font-size: 45px; float:right">
                                @if(is_null($data->discharges))
                                    --
                                @else
                                    {{number_format($data->discharges,2)}}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card text-white @if($data->overtime_percent < 2) bg-success @else bg-danger @endif">
                    <div class="card-header" style="font-weight: bold; text-decoration: underline"><a href="#" data-toggle="modal" data-target="#overtime_percent_graph" class="">Overtime Percent</a> <a href="#" data-toggle="modal" data-target="#overtime_percent" class="float-right"><i class="fad fa-question-circle"></i></a></div>
                    <div class="card-body ">
                        <div class="row justify-content-end h-100">
                            <div class="col my-auto">
                                @if($isYesterday)
                                    @if($data->overtime_percent > $yesterday->overtime_percent)
                                        <div class="float-left my-auto"><i id="move-up" class="fad fa-arrow-square-up fa-4x" style="--fa-primary-color: red;" title="From Yesterday"></i></div>
                                    @else
                                        <div class="float-left my-auto"><i id="move-down" class="fad fa-arrow-square-down fa-4x" style="--fa-primary-color: green;" title="From Yesterday"></i></div>
                                    @endif
                                @endif
                            </div>
                            <div class="col" style="font-size: 45px; float:right">
                                @if(is_null($data->overtime_percent))
                                    --
                                @else
                                    {{number_format($data->overtime_percent,2)}}%
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @if( Auth::user()->hasRole('Daily Scorecard Advanced') == TRUE)

            <div class="card-deck mt-3">
                <div class="card text-white bg-dark">
                    <div class="card-header" style="font-weight: bold; text-decoration: underline"><a href="#" data-toggle="modal" data-target="#admissions_graph" class="">Admissions</a> <a href="#" data-toggle="modal" data-target="#admissions" class="float-right"><i class="fad fa-question-circle"></i></a></div>
                    <div class="card-body">
                        <div class="row justify-content-end h-100">
                            <div class="col my-auto">
                                @if($isYesterday)
                                    @if($data->equivalent_admissions > $yesterday->equivalent_admissions)
                                        <div class="float-left my-auto"><i class="fad fa-arrow-square-up fa-4x" style="--fa-primary-color: green;" title="From Yesterday"></i></div>
                                    @else
                                        <div class="float-left my-auto"><i class="fad fa-arrow-square-down fa-4x" style="--fa-primary-color: red;" title="From Yesterday"></i></div>
                                    @endif
                                @endif
                            </div>
                            <div class="col text-right" style="font-size: 45px;">
                                @if(is_null($data->equivalent_admissions))
                                    --
                                @else
                                    {{number_format($data->equivalent_admissions)}}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card text-white bg-dark">
                    <div class="card-header" style="font-weight: bold; text-decoration: underline"><a href="#" data-toggle="modal" data-target="#acute_admissions_graph" class="">Acute Admissions (IP)</a> <a href="#" data-toggle="modal" data-target="#acute_admissions" class="float-right"><i class="fad fa-question-circle"></i></a></div>
                    <div class="card-body">
                        <div class="row justify-content-end h-100">
                            <div class="col my-auto">
                                @if($isYesterday)
                                    @if($data->acute_admissions > $yesterday->acute_admissions)
                                        <div class="float-left my-auto"><i class="fad fa-arrow-square-up fa-4x" style="--fa-primary-color: green;" title="From Yesterday"></i></div>
                                    @else
                                        <div class="float-left my-auto"><i class="fad fa-arrow-square-down fa-4x" style="--fa-primary-color: red;" title="From Yesterday"></i></div>
                                    @endif
                                @endif
                            </div>
                            <div class="col text-right" style="font-size: 45px;">
                                @if(is_null($data->acute_admissions))
                                    --
                                @else
                                    {{number_format($data->acute_admissions)}}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card text-white bg-dark">
                    <div class="card-header" style="font-weight: bold; text-decoration: underline"><a href="#" data-toggle="modal" data-target="#observation_patients_graph" class="">Observation Patients</a> <a href="#" data-toggle="modal" data-target="#observation_patients" class="float-right"><i class="fad fa-question-circle"></i></a></div>
                    <div class="card-body">
                        <div class="row justify-content-end h-100">
                            <div class="col my-auto">
                                @if($isYesterday)
                                    @if($data->observation_patients > $yesterday->observation_patients)
                                        <div class="float-left my-auto"><i class="fad fa-arrow-square-up fa-4x" style="--fa-primary-color: green;" title="From Yesterday"></i></div>
                                    @else
                                        <div class="float-left my-auto"><i class="fad fa-arrow-square-down fa-4x" style="--fa-primary-color: red;" title="From Yesterday"></i></div>
                                    @endif
                                @endif
                            </div>
                            <div class="col text-right" style="font-size: 45px;">
                                @if(is_null($data->observation_patients))
                                    --
                                @else
                                    {{number_format($data->observation_patients)}}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card text-white bg-dark">
                    <div class="card-header" style="font-weight: bold; text-decoration: underline"><a href="#" data-toggle="modal" data-target="#ed_visits_graph" class="">ED Visits</a> <a href="#" data-toggle="modal" data-target="#ed_visits" class="float-right"><i class="fad fa-question-circle"></i></a></div>
                    <div class="card-body">
                        <div class="row justify-content-end h-100">
                            <div class="col my-auto">
                                @if($isYesterday)
                                    @if($data->ed_visits > $yesterday->ed_visits)
                                        <div class="float-left my-auto"><i class="fad fa-arrow-square-up fa-4x" style="--fa-primary-color: green;" title="From Yesterday"></i></div>
                                    @else
                                        <div class="float-left my-auto"><i class="fad fa-arrow-square-down fa-4x" style="--fa-primary-color: red;" title="From Yesterday"></i></div>
                                    @endif
                                @endif
                            </div>
                            <div class="col text-right" style="font-size: 45px;">
                                @if(is_null($data->ed_visits))
                                    --
                                @else
                                    {{number_format($data->ed_visits)}}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card text-white bg-dark">
                    <div class="card-header" style="font-weight: bold; text-decoration: underline"><a href="#" data-toggle="modal" data-target="#clinic_visits_graph" class="">Clinic Visits</a> <a href="#" data-toggle="modal" data-target="#clinic_visits" class="float-right"><i class="fad fa-question-circle"></i></a></div>
                    <div class="card-body">
                        <div class="row justify-content-end h-100">
                            <div class="col my-auto">
                                @if($isYesterday)
                                    @if($data->clinic_visits > $yesterday->clinic_visits)
                                        <div class="float-left my-auto"><i class="fad fa-arrow-square-up fa-4x" style="--fa-primary-color: green;" title="From Yesterday"></i></div>
                                    @else
                                        <div class="float-left my-auto"><i class="fad fa-arrow-square-down fa-4x" style="--fa-primary-color: red;" title="From Yesterday"></i></div>
                                    @endif
                                @endif
                            </div>
                            <div class="col text-right" style="font-size: 45px;">
                                @if(is_null($data->clinic_visits))
                                    --
                                @else
                                    {{number_format($data->clinic_visits)}}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card text-white bg-dark">
                    <div class="card-header" style="font-weight: bold; text-decoration: underline"><a href="#" data-toggle="modal" data-target="#surgery_cases_graph" class="">Surgery Cases</a> <a href="#" data-toggle="modal" data-target="#surgery_cases" class="float-right"><i class="fad fa-question-circle"></i></a></div>
                    <div class="card-body">
                        <div class="row justify-content-end h-100">
                            <div class="col my-auto">
                                @if($isYesterday)
                                    @if($data->surgery_cases > $yesterday->surgery_cases)
                                        <div class="float-left my-auto"><i class="fad fa-arrow-square-up fa-4x" style="--fa-primary-color: green;" title="From Yesterday"></i></div>
                                    @else
                                        <div class="float-left my-auto"><i class="fad fa-arrow-square-down fa-4x" style="--fa-primary-color: red;" title="From Yesterday"></i></div>
                                    @endif
                                @endif
                            </div>
                            <div class="col text-right" style="font-size: 45px;">
                                @if(is_null($data->surgery_cases))
                                    --
                                @else
                                    {{number_format($data->surgery_cases)}}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        @endif
        <div class="card-footer">
            <i class="text-muted">Updated at {{$data->created_at}}</i>
        </div>
    </div>




    <!-- Modal -->
    <div class="modal fade" id="worked_hours" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Worked Hours</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Worked hours include all pay codes EXCEPT PTO of any kind, Orientation, EDUCATION and meeting times.</p>
                    <p>Pay codes included are: 'DAY', 'DRMDHY', 'DRMDJS', 'DTD',
                        'DTE', 'DTN', 'ESHFT REG', 'ESHFTREG', 'EVE', 'HNB', 'HNBU', 'NGT', 'NIGHT', 'SAL', 'SALEXEC', 'SALPPD', 'SALPPD-40', 'BOD', 'BOE', 'BON', 'CALLBACK', 'CBD', 'CBE',
                        'CBN', 'DAYOVT', 'DBLTIME', 'DOT', 'EDDOUTSOVT', 'EOT', 'EPIC EDU OVT', 'EVEOVT', 'INHSEDDOVT', 'MEETING OVT', 'MOD', 'MOE', 'NIGHTOVT', 'NOT', 'ORTOVT', 'SALNONEXMTP'</p>
                    <p><i><b>REMEMBER:</b>This is a snap shot in time.  As mis-clocks and other adjustments are made this number could change.  Please refer to API for real time information</i></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="paid_hours" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Paid Hours</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Worked hours are any pay code except PTO payout that may appear on an employees check..</p>
                    <p>Pay codes included are: 'CALLBACK', 'CALLBACKMIM', 'CBD', 'CBDM', 'CBE', 'CBEM', 'CBN', 'CONT', 'DAY', 'DAYOVT', 'DBLTIME', 'DOT', 'DRMDJS', 'DTD', 'DTE', 'DTN', 'EOT', 'ESHFT REG', 'ESHFTREG', 'EVE',
                        'EVEOVT', 'HNB', 'HNBU', 'NGT', 'NIGHT', 'NIGHTOVT', 'NOT', 'PAGC', 'SAL', 'SALEXEC', 'SALNONEXMTP', 'SALPPD', 'SALPPD-40', 'BED', 'BEE', 'BEN', 'BOD', 'BOE', 'BON', 'BPTOTAKEN', 'CME',
                        'COM', 'DRPDO', 'EDD', 'EDDINH', 'EDDOUTS', 'EDDOUTSOVT', 'EDE', 'EOD', 'EPIC EDD', 'EPIC EDU', 'EPIC EDU OVT', 'FMLAFB', 'FMLAFP', 'FMLAFS', 'FMLAIFB', 'FMLAIFP', 'FMLAIFS', 'FMLAIPB',
                        'FMLAIPP', 'FMLAIPS', 'FMLAPB', 'FMLAPP', 'FMLAPS', 'HPTO', 'INHSEDDOVT', 'JUR', 'LCP', 'MED', 'MEE', 'MEETING', 'MEETING OVT', 'MEN', 'MIL', 'MOD', 'MOE', 'OED', 'OEE', 'OEN', 'OOD',
                        'OOE', 'OON', 'ORIENTATION', 'ORTOVT', 'PDLEAVE', 'PRN PAY PTO', 'PRN PTO RATE', 'PSUSPEND', 'PTO', 'PTOCASH', 'PTOU', 'REA', 'RET', 'SIC', 'SNOR', 'SUP', 'TERM BPTO', 'TERM PTO', 'TPB',
                        'TPV', 'TRANSRADIO', 'VS1', 'VS2', 'VS3', 'VS4', 'VS5', 'VTO', 'WCI', 'WIT'</p>
                    <p><i><b>REMEMBER:</b>This is a snap shot in time.  As mis-clocks and other adjustments are made this number could change.  Please refer to API for real time information</i></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="overtime_hours" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Overtime Hours</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Overtime hours are any specalized code where the employee gets over their 40 weekly hours or call back.</p>
                    <p>Pay codes included are: 'BOD', 'BOE', 'BON', 'CALLBACK', 'CBD', 'CBE', 'CBN', 'DAYOVT', 'DBLTIME', 'DOT', 'EDDOUTSOVT',
                        'EOT', 'EPIC EDU OVT', 'EVEOVT', 'INHSEDDOVT', 'MEETING OVT', 'MOD', 'MOE', 'NIGHTOVT', 'NOT', 'ORTOVT', 'SALNONEXMTP'</p>
                    <p><i><b>REMEMBER:</b>This is a snap shot in time.  As mis-clocks and other adjustments are made this number could change.  Please refer to API for real time information</i></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="discharges" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Discharges</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Total number of all discharges from all campuses.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="overtime_percent" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Overtime Percent</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Overtime percent is calculated by dividing the number of worked hours into overtime hours.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="admissions" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Admissions</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>This number includes all acute, GPU, and swing admissions for all campuses</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="acute_admissions" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Acute Admissions</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>This number includes acute admissions from SGMC.  No other campus has acute admissions.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="observation_patients" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Observation Patients</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>This number represents total number of patients placed in observation status at all campuses.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ed_visits" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">ED Visits</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>This number represents total number of patients registered in the ED from <b>all campuses</b> EXCLUDING LBBS.  Charts that were still open at 12 am will be counted on that day.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="clinic_visits" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Clinic Visits</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>This number represents total number of clinic visits from all campuses.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="surgery_cases" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Surgery Cases</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>This number represents total number of surgery cases.  This data is pulled from surgery logs.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="admissions_graph" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Admissions</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <canvas id="admissions-chart" width="800" height="450"></canvas>
                    <p>{{$all->count()}} Day Average {{number_format($all->avg('equivalent_admissions'),2)}}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="acute_admissions_graph" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Observation Patients</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <canvas id="acute-admissions-chart" width="800" height="450"></canvas>
                    <p>{{$all->count()}} Day Average {{number_format($all->avg('acute_admissions'),2)}}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="observation_patients_graph" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Observation Patients</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <canvas id="observation-patients-chart" width="800" height="450"></canvas>
                    <p>{{$all->count()}} Day Average {{number_format($all->avg('observation_patients'),2)}}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ed_visits_graph" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Clinic Visits</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <canvas id="ed-visits-chart" width="800" height="450"></canvas>
                    <p>{{$all->count()}} Day Average {{number_format($all->avg('ed_visits'),2)}}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="clinic_visits_graph" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Clinic Visits</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <canvas id="clinic-visits-chart" width="800" height="450"></canvas>
                    <p>{{$all->count()}} Day Average {{number_format($all->avg('clinic_visits'),2)}}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="surgery_cases_graph" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Surgery Cases</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <canvas id="sugery-cases-chart" width="800" height="450"></canvas>
                    <p>{{$all->count()}} Day Average {{number_format($all->avg('surgery_cases'),2)}}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="overtime_percent_graph" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Overtime Percent</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <canvas id="overtime-percent-chart" width="800" height="450"></canvas>
                    <p>{{$all->count()}} Day Average {{number_format($all->avg('overtime_percent'),2)}}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="discharges_graph" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Discharges</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <canvas id="discharges-chart" width="800" height="450"></canvas>
                    <p>{{$all->count()}} Day Average {{number_format($all->avg('discharges'),2)}}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="overtime_hours_graph" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Discharges</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <canvas id="overtime-hours-chart" width="800" height="450"></canvas>
                    <p>{{$all->count()}} Day Average {{number_format($all->avg('overtime_hours'),2)}}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    @endif
@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">

    // Admissions
    new Chart(document.getElementById("admissions-chart"), {
        type: 'bar',
        data: {
            labels: [

                @foreach($all_days as $all)
                    "{{Carbon\Carbon::parse($all->report_date)->subDays(1)->format('m-d (D)')}}",
                @endforeach

            ],
            datasets: [
                {
                    label: "Admissions",
                    backgroundColor: 'rgba(54, 162, 235, 0.2)',
                    borderColor: 'rgba(54, 162, 235, 1)',
                    borderWidth: 1,
                    data: [

                        @foreach($all_days as $all)
                        {{$all->equivalent_admissions}},
                        @endforeach

                    ],
                    trendlineLinear: {
                        style: "rgba(0 ,0 ,0, 1)",
                        lineStyle: "solid",
                        width: 1
                    }
                }
            ]
        },
        options: {
            legend: { display: false },
            title: {
                display: true,
                text: 'Daily Admissions (Report Date)'
            }
        }
    });

    // Acute Admissions
    new Chart(document.getElementById("acute-admissions-chart"), {
        type: 'bar',
        data: {
            labels: [

                @foreach($all_days as $all)
                    "{{Carbon\Carbon::parse($all->report_date)->subDays(1)->format('m-d (D)')}}",
                @endforeach

            ],
            datasets: [
                {
                    label: "Acute Admissions (IP)",
                    backgroundColor: 'rgba(54, 162, 235, 0.2)',
                    borderColor: 'rgba(54, 162, 235, 1)',
                    borderWidth: 1,
                    data: [

                        @foreach($all_days as $all)
                        {{$all->acute_admissions}},
                        @endforeach

                    ],
                    trendlineLinear: {
                        style: "rgba(0 ,0 ,0, 1)",
                        lineStyle: "solid",
                        width: 1
                    }
                }
            ]
        },
        options: {
            legend: { display: false },
            title: {
                display: true,
                text: 'Daily Acute Admissions (Report Date)'
            }
        }
    });

    // Observation Patients
    new Chart(document.getElementById("observation-patients-chart"), {
        type: 'bar',
        data: {
            labels: [

                @foreach($all_days as $all)
                    "{{Carbon\Carbon::parse($all->report_date)->subDays(1)->format('m-d (D)')}}",
                @endforeach

            ],
            datasets: [
                {
                    label: "Observation Patients",
                    backgroundColor: 'rgba(54, 162, 235, 0.2)',
                    borderColor: 'rgba(54, 162, 235, 1)',
                    borderWidth: 1,
                    data: [

                        @foreach($all_days as $all)
                        {{$all->observation_patients}},
                        @endforeach

                    ],
                    trendlineLinear: {
                        style: "rgba(0 ,0 ,0, 1)",
                        lineStyle: "solid",
                        width: 1
                    }
                }
            ]
        },
        options: {
            legend: { display: false },
            title: {
                display: true,
                text: 'Daily Observation Patients (Report Date)'
            }
        }
    });

    // ED Visits
    new Chart(document.getElementById("ed-visits-chart"), {
        type: 'bar',
        data: {
            labels: [

                @foreach($all_days as $all)
                    "{{Carbon\Carbon::parse($all->report_date)->subDays(1)->format('m-d (D)')}}",
                @endforeach

            ],
            datasets: [
                {
                    label: "ED Visits",
                    backgroundColor: 'rgba(54, 162, 235, 0.2)',
                    borderColor: 'rgba(54, 162, 235, 1)',
                    borderWidth: 1,
                    data: [

                        @foreach($all_days as $all)
                        {{$all->ed_visits}},
                        @endforeach

                    ],
                    trendlineLinear: {
                        style: "rgba(0 ,0 ,0, 1)",
                        lineStyle: "solid",
                        width: 1
                    }
                }
            ]
        },
        options: {
            legend: { display: false },
            title: {
                display: true,
                text: 'Daily ED Visits (Report Date)'
            }
        }
    });

    // Clinic Visits
    new Chart(document.getElementById("clinic-visits-chart"), {
        type: 'bar',
        data: {
            labels: [

                @foreach($all_days as $all)
                    "{{Carbon\Carbon::parse($all->report_date)->subDays(1)->format('m-d (D)')}}",
                @endforeach

            ],
            datasets: [
                {
                    label: "Clinic Visits",
                    backgroundColor: 'rgba(54, 162, 235, 0.2)',
                    borderColor: 'rgba(54, 162, 235, 1)',
                    borderWidth: 1,
                    data: [

                        @foreach($all_days as $all)
                        {{$all->clinic_visits}},
                        @endforeach

                    ],
                    trendlineLinear: {
                        style: "rgba(0 ,0 ,0, 1)",
                        lineStyle: "solid",
                        width: 1
                    }
                }
            ]
        },
        options: {
            legend: { display: false },
            title: {
                display: true,
                text: 'Daily Clinic Visits (Report Date)'
            }
        }
    });



    // Surgery Cases
    new Chart(document.getElementById("sugery-cases-chart"), {
        type: 'bar',
        data: {
            labels: [

                @foreach($all_days as $all)
                "{{Carbon\Carbon::parse($all->report_date)->subDays(1)->format('m-d (D)')}}",
                @endforeach

            ],
            datasets: [
                {
                    label: "Surgery Cases",
                    backgroundColor: 'rgba(54, 162, 235, 0.2)',
                    borderColor: 'rgba(54, 162, 235, 1)',
                    borderWidth: 1,
                    data: [

                        @foreach($all_days as $all)
                        {{$all->surgery_cases}},
                        @endforeach

                    ],
                    trendlineLinear: {
                        style: "rgba(0 ,0 ,0, 1)",
                        lineStyle: "solid",
                        width: 1
                    }
                }
            ]
        },
        options: {
            legend: { display: false },
            title: {
                display: true,
                text: 'Daily Surgery Cases (Report Date)'
            }
        }
    });


    // Overtime Percent
    new Chart(document.getElementById("overtime-percent-chart"), {
        type: 'bar',
        data: {
            labels: [

                @foreach($all_days as $all)
                    "{{Carbon\Carbon::parse($all->report_date)->subDays(1)->format('m-d (D)')}}",
                @endforeach

            ],
            datasets: [
                {
                    label: "Overtime Percent",
                    backgroundColor: 'rgba(54, 162, 235, 0.2)',
                    borderColor: 'rgba(54, 162, 235, 1)',
                    borderWidth: 1,
                    data: [

                        @foreach($all_days as $all)
                        {{$all->overtime_percent}},
                        @endforeach

                    ],
                    trendlineLinear: {
                        style: "rgba(0 ,0 ,0, 1)",
                        lineStyle: "solid",
                        width: 1
                    }
                }
            ]
        },
        options: {
            legend: { display: false },
            title: {
                display: true,
                text: 'Overtime Percent (Report Date)'
            }
        }
    });


    // Discharges
    new Chart(document.getElementById("discharges-chart"), {
        type: 'bar',
        data: {
            labels: [

                @foreach($all_days as $all)
                    "{{Carbon\Carbon::parse($all->report_date)->subDays(1)->format('m-d (D)')}}",
                @endforeach

            ],
            datasets: [
                {
                    label: "Discharges",
                    backgroundColor: 'rgba(54, 162, 235, 0.2)',
                    borderColor: 'rgba(54, 162, 235, 1)',
                    borderWidth: 1,
                    data: [

                        @foreach($all_days as $all)
                        {{$all->discharges}},
                        @endforeach

                    ],
                    trendlineLinear: {
                        style: "rgba(0 ,0 ,0, 1)",
                        lineStyle: "solid",
                        width: 1
                    }
                }
            ]
        },
        options: {
            legend: { display: false },
            title: {
                display: true,
                text: 'Discharges (Report Date)'
            }
        }
    });


    // Overtime Hours
    new Chart(document.getElementById("overtime-hours-chart"), {
        type: 'bar',
        data: {
            labels: [

                @foreach($all_days as $all)
                    "{{Carbon\Carbon::parse($all->report_date)->subDays(1)->format('m-d (D)')}}",
                @endforeach

            ],
            datasets: [
                {
                    label: "Overtime Hours",
                    backgroundColor: 'rgba(54, 162, 235, 0.2)',
                    borderColor: 'rgba(54, 162, 235, 1)',
                    borderWidth: 1,
                    data: [

                        @foreach($all_days as $all)
                        {{$all->overtime_hours}},
                        @endforeach

                    ],
                    trendlineLinear: {
                        style: "rgba(0 ,0 ,0, 1)",
                        lineStyle: "solid",
                        width: 1
                    }
                }
            ]
        },
        options: {
            legend: { display: false },
            title: {
                display: true,
                text: 'Overtime Hours (Report Date)'
            }
        }
    });

</script>

@endsection

@endif