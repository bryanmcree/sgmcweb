
<div class="card-deck">
    <div class="card border-danger text-white bg-dark mb-2">
        <div class="card-body">
            @if($user->photo != '')
                <img src="data:image/jpeg;base64,{{str_replace('</photo>','',str_replace('<photo>','',$user->photo))}}" height="200" width="200"/>
            @else
                {{ Html::image('img/nopic120.png', 'alt', array( 'width' => 200, 'height' => 200 )) }}
            @endif
        </div>
    </div>
    <div class="card border-danger text-white bg-dark mb-2">
        <div class="card-body">
            <table class="table table-hover text-white">
                <tbody>
                    <tr>
                        <td><B>DOB:</B></td>
                        <td>@if(!empty($user->dob)){{$user->dob->format('m-d-Y')}} / {{\Carbon\Carbon::createFromTimeStamp(strtotime($user->dob))->diffForHumans()}}@else Not entered @endif</td>
                    </tr>
                    <tr>
                        <td><B>Employee ID:</B></td>
                        <td>{{$user->employee_number}}</td>
                    </tr>
                    <tr>
                        <td><B>Cost Center:</B></td>
                        <td>{{$user->unit_code}} / {{$user->unit_code_description}}</td>
                    </tr>
                    <tr>
                        <td><B>Hire Date:</B></td>
                        <td>@if(!empty($user->hire_date)){{$user->hire_date->format('m-d-Y')}} / {{\Carbon\Carbon::createFromTimeStamp(strtotime($user->hire_date))->diffForHumans()}} @else Not entered @endif</td>
                    </tr>
                    <tr>
                        <td><B>Email Address:</B></td>
                        <td>{{ Html::mailto($user->mail) }}</td>
                    </tr>
                    <tr>
                        <td><B>Gender:</B></td>
                        <td>{{$user->gender}}</td>
                    </tr>
                    <tr>
                        <td><B>Race:</B></td>
                        <td>{{$user->race}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>


<nav>
    <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Login Details</a>
        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Employment Status</a>
        <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Employment Details</a>
        <a class="nav-item nav-link" id="nav-security-tab" data-toggle="tab" href="#nav-security" role="tab" aria-controls="nav-security" aria-selected="false">Security Roles</a>
    </div>
</nav>
<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
        <div class="card border-danger text-white bg-dark mt-3">
            <div class="card-body">
                <B>Created At:</B> {{$user->created_at}} / {{\Carbon\Carbon::createFromTimeStamp(strtotime($user->created_at))->diffForHumans()}}
                <br>
                <B>Updated At:</B> @if($user->updated_at !='') {{$user->updated_at}} / {{\Carbon\Carbon::createFromTimeStamp(strtotime($user->updated_at))->diffForHumans()}} @else No Data @endif
                <BR>
                <B>Deleted At:</B> @if($user->deleted_at !='') {{$user->deleted_at}} / {{\Carbon\Carbon::createFromTimeStamp(strtotime($user->deleted_at))->diffForHumans()}} @else No Data @endif
                <BR>
                <B>Last Login:</B> @if($user->last_login !='') {{$user->last_login}} / {{\Carbon\Carbon::createFromTimeStamp(strtotime($user->last_login))->diffForHumans()}} @else No Data @endif
                <BR>
                <B>Last Known IP:</B> @if($user->last_login_ip !='') {{$user->last_login_ip}} @else No Data @endif
                <BR>
                <B>Web.SGMC ID#:</B> {{$user->id}}
                <BR>

                <BR>
            </div>
        </div>

    </div>
    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
        <div class="card border-danger text-white bg-dark mt-3">
            <div class="card-body">
                <B>Work Status:</B> {{$user->workstatus}}
                <br>
                <B>Shift:</B> {{$user->shift}}
                <BR>
                <B>Federal Classification:</B> {{$user->federal_cat}}
                <BR>
                <B>Personal Type:</B> {{$user->personneltype}}
                <BR>
                <B>Rate:</B> ${{number_format($user->rate,2)}} / ${{number_format(($user->rate*40)*52,2)}}
                <BR>
                <B>Pay Grade:</B> {{$user->pay_grade}}
                <BR>
                <B>Min:</B> ${{number_format($user->pay_grade_min,2)}} / ${{number_format(($user->pay_grade_min*40)*52,2)}}
                <BR>
                <B>Mid:</B> ${{number_format($user->pay_grade_mid,2)}} / ${{number_format(($user->pay_grade_mid*40)*52,2)}}
                <BR>
                <B>Max:</B> ${{number_format($user->pay_grade_max,2)}} / ${{number_format(($user->pay_grade_max*40)*52,2)}}
                <BR>
                <B>Manager:</B> @if($user->managername !='') {{$user->managername}} / {{ Html::mailto($user->manageremail) }}@else No Data @endif
                <BR>
                <B>Employment Status:</B> {{$user->emp_status}}
                <BR>
                <B>Location:</B> {{$user->location}}
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
        <div class="card border-danger text-white bg-dark mt-3">
            <div class="card-body">
                
                <form action="/user/details/update/{{$user->id}}" method="POST">
                    {{csrf_field()}}

                    <div class="form-row">
                        <div class="form-group col-lg-4">
                            <label for="date">Username</label>
                            <input type="text" name="username" class="form-control" value={{$user->username}}>
                        </div>
                        <div class="form-group col-lg-4">
                            <label for="status">Employee Status</label>
                            <select name="emp_status" class="form-control">
                                <option value="{{$user->emp_status}}" selected>{{$user->emp_status}}</option>
                                <option value="Active">Active</option>
                                <option value="Terminated">Terminated</option>
                            </select>
                        </div>
                        <div class="form-group col-lg-4">
                            <label for="access">Access Level</label>
                            <select name="access" class="form-control">
                                <option value="{{$user->access}}" selected>{{$user->access}}</option>
                                <option value="Admin">Admin</option>
                                <option value="SGMC">SGMC</option>
                                <option value="Active">Active</option>
                                <option value="Medco">Medco</option>
                                <option value="Inactive">Inactive</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-lg-3">
                            <label for="title">Title</label>
                            <input type="text" name="title" class="form-control" value="{{$user->title}}">
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="empNum">Emp Number</label>
                            <input type="text" name="employee_number" class="form-control" value="{{$user->employee_number}}">
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="job">Job Code</label>
                            <input type="text" name="job_code" class="form-control" value="{{$user->job_code}}">
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="unit">Unit Code</label>
                            <input type="text" name="unit_code" class="form-control" value="{{$user->unit_code}}">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-lg-3">
                            <label for="address">Address</label>
                            <input type="text" name="address" class="form-control" value="{{$user->address}}">
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="city">City</label>
                            <input type="text" name="city" class="form-control" value="{{$user->city}}">
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="state">State</label>
                            <input type="text" name="state" class="form-control" value="{{$user->state}}">
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="zip">ZIP Code</label>
                            <input type="text" name="zip" class="form-control" value="{{$user->zip}}">
                        </div>
                    </div>

                    <input type="submit" class="btn btn-success btn-block" value="Update User">

                </form>

            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="nav-security" role="tabpanel" aria-labelledby="nav-security-tab">
        <div class="card border-danger text-white bg-dark mt-3">
            <div class="card-body">
                <nav>
                    <div class="nav nav-tabs" id="security-subtab" role="tablist">
                        <a class="nav-item nav-link active" id="security-assigned-tab" data-toggle="tab" href="#security-assigned" role="tab" aria-controls="security-unassigned" aria-selected="true">Assigned Roles</a>
                        <a class="nav-item nav-link" id="security-unassigned-tab" data-toggle="tab" href="#security-unassigned" role="tab" aria-controls="security-unassigned" aria-selected="false">Avaliable Roles</a>
                    </div>
                </nav>
                <div class="tab-content" id="security-sub">
                    <div class="tab-pane fade show active" id="security-assigned" role="tabpanel" aria-labelledby="security-assigned">
                        <form method="POST" action="/user_roles/remove" class="mt-4">
                            {{ csrf_field() }}
                            <input type="hidden" value="{{$user->id}}" name="user_id">
                            
                            <div style="height:450px; overflow-y: auto;">
                                <table class="table table-hover mb-4 border-info text-white bg-dark" style="width: 100%;" id="rolesYes">
                                    <thead>
                                    <tr>
                                        <td><B>Role Name</B></td>
                                        <td><b>Description</b></td>
                                        <td></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($assignedNew as $role)
                                        <tr>
                                            <td nowrap="">{{$role->name}}</td>
                                            <td>{{$role->label}}</td>
                                            <td align="center"><input type="checkbox" name="role_id[]" value="{{$role->id}}" style="width: 30px; height: 30px;"></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <input type="submit" class="btn btn-sgmc" value="Remove Roles">
                        </form>

                        {{-- {!! Form::open(array('action' => ['UserController@removeRoles'], 'class' => 'form_control')) !!}
                        {!! Form::hidden('user_id', $user->id,['class'=>'user_id']) !!}
                        
                        {!! Form::submit('Remove Roles', ['class'=>'btn btn-sgmc', 'id'=>'AddButton']) !!}
                        {!! Form::close() !!} --}}
                    </div>

                    <div class="tab-pane fade" id="security-unassigned" role="tabpanel" aria-labelledby="security-unassigned">
                        {!! Form::open(array('action' => ['UserController@newRoles'], 'class' => 'form_control mt-4')) !!}
                        {!! Form::hidden('user_id', $user->id,['class'=>'user_id']) !!}

                        <div style="height:450px; overflow-y: auto;">
                            <table class="table table-hover mb-4 border-info text-white bg-dark" style="width: 100%;" id="rolesNo">
                                <thead>
                                <tr>
                                    <td><B>Role Name</B></td>
                                    <td><b>Description</b></td>
                                    <td></td>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($roles as $role)
                                    <tr>
                                        <td nowrap="">{{$role->name}}</td>
                                        <td>{{$role->label}}</td>
                                        <td align="center"><input type="checkbox" name="{{ $role->id }}[answer]" value="{{$role->id}}" style="width: 30px; height: 30px;"></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        {!! Form::submit('Add Roles', ['class'=>'btn btn-sgmc', 'id'=>'AddButton']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


{{-- <script type="application/javascript">
    $(document).ready(function() {
        $('#rolesYes').DataTable( {
            "pageLength": 10,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script>

<script type="application/javascript">
    $(document).ready(function() {
        $('#rolesNo').DataTable( {
            "pageLength": 10,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }]
        } );
    } );
</script> --}}