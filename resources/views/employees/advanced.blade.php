{{--New file Template--}}

{{--Add Security for this page below--}}
@if( Auth::user()->hasRole('Business Intelligence') == FALSE)
    @include('layouts.unauthorized')
@Else

    @extends('layouts.app')
    {{--Updated 12/6/2018 for Bootstrap 4.1--}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

@section('content')


    <div class="card col-4 mb-4 text-white bg-dark">
        <div class="card-header">
            <b>Advanced Employee Search</b>
        </div>
        <div class="card-body">
            <div class="form-group">
                <label for="exampleFormControlSelect1">Cost Center</label>
                <select name="cost_center" class="form-control basic" required>
                    <option value="">[Choose Your Cost Center]</option>
                    @foreach($costcenters as $cc)
                        <option value="{{$cc->style1}}">{{$cc->style1}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="exampleFormControlSelect1">Cost Center</label>
                <select name="cost_center" class="form-control basic" required>
                    <option value="">[Select Job Code]</option>
                    @foreach($job_codes as $jc)
                        <option value="{{$jc->job_code}}">{{$jc->job_code}} - {{$jc->title}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-check">
                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                <label class="form-check-label" for="exampleCheck1">Include Terminated Employees</label>
            </div>
        </div>
    </div>


@endsection

{{--END of Content and START of Scripts--}}
@section('scripts')


    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    <script type="text/javascript">

        $(".basic").select2({
            allowClear: false
        });

    </script>

@endsection

@endif