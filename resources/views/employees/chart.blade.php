{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('Widget - SGMCBoard') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="col-md-12">
        <div class="card bg-dark text-white mb-3 border-info">
            <div class="card card-header">
                <b>SGMC Employee Trend</b>
            </div>
            <div class="card-body">
                <canvas id="emp_count" width="100%" height="30"></canvas>
                <div class="card-deck">
                    <div class="card bg-dark text-white mb-3 border-info">
                        <div class="card-header">
                            <b>2019 Minimum</b>
                        </div>
                        <div class="card-body">
                            {{$employeesMin->emp_value}}
                        </div>
                    </div>
                    <div class="card bg-dark text-white mb-3 border-info">
                        <div class="card-header">
                            <b>2019 Maximum</b>
                        </div>
                        <div class="card-body">
                            {{$employeesMax->emp_value}}
                        </div>
                    </div>
                    <div class="card bg-dark text-white mb-3 border-info">
                        <div class="card-header">
                            <b>2019 Current</b>
                        </div>
                        <div class="card-body">
                            {{$employeesCurrent->emp_value}}
                        </div>
                    </div>
                    <div class="card bg-dark text-white mb-3 border-info">
                        <div class="card-header">
                            <b>2019 Variance</b>
                        </div>
                        <div class="card-body">
                            {{$employeesMax->emp_value - $employeesCurrent->emp_value}}
                        </div>
                    </div>
                    <div class="card bg-dark text-white mb-3 border-info">
                        <div class="card-header">
                            <b>Estimated Savings / Cost Per PP</b>
                        </div>
                        <div class="card-body">
                            ${{number_format((($employeesMax->emp_value - $employeesCurrent->emp_value)*$avg_pay)*80,2)}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="card bg-dark text-white mb-3 border-info">
            <div class="card card-header">
                <b>SGMC PRN Employee Trend</b>
            </div>
            <div class="card-body">
                <canvas id="prn_count" width="100%" height="20"></canvas>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="card bg-dark text-white mb-3 border-info">
            <div class="card card-header">
                <b>SGMC RN Employee Trend</b>
            </div>
            <div class="card-body">
                <canvas id="rn_count" width="100%" height="20"></canvas>
            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

    <script type="text/javascript">
        Chart.defaults.global.defaultFontColor = '#fff';
        //Number of EMployees
        new Chart(document.getElementById("emp_count"), {
            type: 'line',
            data: {
                labels: [
                    @foreach($emp_count_chart_current_year as $emp_count)
                    '{{$emp_count->created_at->format('m-d')}}',
                    @endforeach
                ],
                datasets: [{
                    data: [

                        @foreach($emp_count_chart_last_year as $emp_count)
                        {{$emp_count->emp_value}},
                        @endforeach

                    ],
                    label: "{{\Carbon\Carbon::now()->subyear(1)->year}}",
                    borderColor: "#0066ff",
                    backgroundColor: 'rgba(0, 102, 255,  0.2)',
                    fill: true
                }, {
                    data: [
                        @foreach($emp_count_chart_current_year as $emp_count)
                        {{$emp_count->emp_value}},
                        @endforeach
                    ],
                    label: "{{\Carbon\Carbon::now()->year}}",
                    borderColor: "#9900ff",
                    backgroundColor: 'rgba(153, 0, 255,  0.2)',
                    fill: true
                }
                ]
            },
            options: {
                title: {
                    display: true,
                    text: 'Employee Count Daily by Year Comparison'
                },

            }
        });


        //Number of prn
        $(function(){
            $.getJSON("/employee/chart/prn", function (result) {
                var labels = [],data=[];
                for (var i = 0; i < result.length; i++) {
                    labels.push(result[i].created_at);
                    data.push(result[i].emp_value);
                }
                var buyerData = {
                    labels : labels,
                    datasets : [
                        {
                            backgroundColor: ['rgba(59,235,3,0.4)'],
                            borderColor: "orange",
                            borderWidth:1,
                            pointBackgroundColor: "black",
                            label: 'Number of PRN Employees',
                            data : data
                        }
                    ]
                };
                var buyers = document.getElementById('prn_count').getContext('2d');
                var chartInstance = new Chart(buyers, {
                    type: 'line',
                    data: buyerData,
                    options: {
                        elements: {
                            point:{
                                radius: 2
                            }
                        }
                    }
                });
            });
        });

        //Number of RN
        $(function(){
            $.getJSON("/employee/chart/rn", function (result) {
                var labels = [],data=[];
                for (var i = 0; i < result.length; i++) {
                    labels.push(result[i].created_at);
                    data.push(result[i].emp_value);
                }
                var buyerData = {
                    labels : labels,
                    datasets : [
                        {
                            backgroundColor: ['rgba(63, 77, 238, 0.13)'],
                            borderColor: "orange",
                            borderWidth:1,
                            pointBackgroundColor: "black",
                            label: 'Number of RNs',
                            data : data
                        }
                    ]
                };
                var buyers = document.getElementById('rn_count').getContext('2d');
                var chartInstance = new Chart(buyers, {
                    type: 'line',
                    data: buyerData,
                    options: {
                        elements: {
                            point:{
                                radius: 2
                            }
                        }
                    }
                });
            });
        });

    </script>

@endsection
@endif