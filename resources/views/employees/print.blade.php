<?PHP ini_set('max_execution_time', 150); ?>


    <div class="col-md-12">
        <div class="panel panel-success">
            <div class="panel-heading"><b>Current Employees ({{$tests->count()}})</b></div>
            <div class="panel-body">

                    <table border="1" cellspacing="0" cellpadding="2">

                        <thead>
                        <tr class="bg-success">
                            <td class=""><b>Name</b></td>
                            <td class=""><b>Title</b></td>
                            <td class="hidden-xs hidden-sm "><b>Department</b></td>
                            <td class="hidden-xs"><b>Facility</b></td>
                        </tr>
                        </thead>

                        @foreach ($tests as $test)
                            <tbody>
                            <tr>
                                <td class="">{{$test->last_name}}, {{$test->first_name}}</td>
                                <td class="">{{$test->title}}</td>
                                <td class="hidden-xs hidden-sm ">{{$test->unit_description}}</td>
                                <td class="hidden-xs">{{$test->location}}</td>
                            </tr>
                            </tbody>
                        @endforeach
                    </table>
            </div>
        </div>
    </div>
