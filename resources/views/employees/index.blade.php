@extends('layouts.app')

@section('content')

@if(Session::has('AddedExtraMile'))
    <meta http-equiv="refresh" content="5;url={{ Session::get('AddedExtraMile') }}">
@endif


    @if ($tests->isEmpty())
        <div class="alert alert-danger" role="alert"><b>No employees found. Try just using a first name or last name (Doe or John NOT JOHN DOE).  Employee numbers work great, or cost center numbers.</b> </div>
    @else
        <table class="table table-condensed table-hover table-dark mx-auto mt-3" id="employees11" style="width:96%">
            <thead>
            <tr>
                <td></td>
                <td class=""><b>Name</b></td>
                <td class="hidden-xs hidden-sm hidden-md"><b>Emp #</b></td>
                @if (Auth::user()->access == 'Admin')
                    <td class="hidden-xs hidden-sm hidden-md"><b>RN</b></td>
                    <td class="hidden-xs hidden-sm hidden-md"><b>DC</b></td>
                    <td class="hidden-xs hidden-sm hidden-md"><b>Status</b></td>
                    <td class="hidden-xs hidden-sm hidden-md"><b>Hire Date</b></td>
                    <td class="hidden-xs hidden-sm "><b>Phone</b></td>
                @endif
                <td class="hidden-xs hidden-sm "><b>Department</b></td>
                <td class="hidden-xs"><b>Facility</b></td>
                @if (Auth::user()->access == 'Admin')
                    <td class="" align="right" style="vertical-align:middle"><b>Detail</b></td>
                @endif
            </tr>
            </thead>
            @foreach ($tests as $test)
                <tbody>
                <tr @if($test->emp_status == 'Terminated')class="bg-danger"@endif
                    @if($test->emp_status == 'Contractor')class="bg-info"@endif
                    @if($test->emp_status == 'Active')class="bg-success"@endif
                    @if($test->emp_status == 'Contract')class="bg-warning"@endif
                >
                    <td style="vertical-align:middle; text-align:center;">@if($test->photo != '')<a href="data:image/jpeg;base64,{{str_replace('</photo>','',str_replace('<photo>','',$test->photo))}}" target="_blank"><img src="data:image/jpeg;base64,{{str_replace('</photo>','',str_replace('<photo>','',$test->photo))}}" height="60" width="60"/></a>@else {{ Html::image('img/nopic.png', 'alt', array( 'width' => 60, 'height' => 60 )) }} @endif</td>
                    <td style="vertical-align:middle;" class=""><b>{{$test->last_name}}, {{$test->first_name}}</b> <br> {{$test->title}}</td>
                    <td style="vertical-align:middle;" class="hidden-xs hidden-sm hidden-md">{{$test->employee_number}}</td>
                    @if (Auth::user()->access == 'Admin')
                        <td style="vertical-align:middle;" class="hidden-xs hidden-sm hidden-md">@if($test->RN == 'Yes')<i class="fa fa-user-md text-success" aria-hidden="true"></i>@else <i class="fa fa-user-md text-danger" aria-hidden="true"></i> @endif</td>
                        <td style="vertical-align:middle;" class="hidden-xs hidden-sm hidden-md">@if($test->DC == 'Yes')<i class="fas fa-check-square text-success" aria-hidden="true"></i>@else <i class="fas fa-check-square text-danger" aria-hidden="true"></i> @endif</td>
                        <td style="vertical-align:middle;" class="hidden-xs hidden-sm hidden-md">{{$test->emp_status}}</td>
                        <td style="vertical-align:middle;" class="hidden-xs hidden-sm hidden-md">@if($test->hire_date != NULL){{$test->hire_date->format('m-d-Y')}} <br> ({{\Carbon\Carbon::createFromTimeStamp(strtotime($test->hire_date))->diffForHumans()}}) @endif</td>
                        <td style="vertical-align:middle;" class="hidden-xs">{{PhoneNumbers::formatNumber($test->phone)}}</td>
                    @endif
                    <td style="vertical-align:middle;" class="hidden-xs hidden-sm ">{{$test->unit_code}}-{{$test->unit_code_description}}</td>
                    <td style="vertical-align:middle;" class="hidden-xs">{{$test->location}}</td>
                    <td style="vertical-align:middle;" align="right" style="vertical-align:middle">
                        <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                            @if ( Auth::user()->hasRole('ExtraMile') == TRUE)
                                <a href="extramile/new/{{$test->id}}"  role="button" class="btn btn-primary" title="Extra Mile"><span class="fa fa-btn fa-trophy" aria-hidden="true"></span></a>
                            @endif
                            @if (Auth::user()->access == 'Admin')
                                <a href="#" data-toggle="modal" data-id="{{$test->id}}" role="button" class="btn btn-dark DetailView2" title="Details"><span class="fa fa-btn fa-info-circle" aria-hidden="true"></span></a>
                            @endif
                            @if (Auth::user()->hasRole('Employee Health') == TRUE)
                                    <a href="employee_health/{{$test->employee_number}}"  role="button" class="btn btn-primary" title="Employee Health"><i class="fas fa-notes-medical"></i></a>
                            @endif
                        </div>
                    </td>
                </tr>
                </tbody>
            @endforeach
        </table>
        <span class="float-right"> @include('pagination', ['paginator' => $tests]) </span>

    @endif


@endsection



<div class="modal" tabindex="-1" role="dialog" id="dataModal">
    <div class="modal-dialog" role="document" style="max-width: 70%">
        <div class="modal-content text-white bg-dark">
            <div class="modal-header">
                <h5 class="modal-title"><i class="fas fa-user-secret"></i> Employee Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="employee_detail">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>




@include("employees.modal.edit")

@section('scripts')

    <script type="text/javascript">

        $(document).ready(function() {
            oTable = $('#employees').DataTable(
                    {
                        "info":     false,
                        "pageLength": 20,
                        "lengthChange": false,
                        "order": [],
                        "columnDefs": [
                            { targets: 'no-sort', orderable: false }
                        ],
                        "dom": '<l<t>ip>'

                    }
            );
            $('#search').keyup(function(){
                oTable.search($(this).val()).draw() ;
            })
        });



        $('.DetailView2').click(function(){
            var id = $(this).attr('data-id');
            //alert($(this).attr('data-id'));
            $.ajax({
                url:"user/detail/"+id,
                method:"GET",
                cache:false,
                success:function(result){
                $("#employee_detail").html(result);
                $('#dataModal').modal("show");
            }});
        });


        $(document).on("click", ".EditUser", function () {
            //alert($(this).attr("data-id"));

            $('.first_name').val($(this).attr("data-first_name"));
            $('.last_name').val($(this).attr("data-last_name"));
            $('.contact_type').val($(this).attr("data-contact_type"));
            $('.office_phone').val($(this).attr("data-office_phone"));
            $('.mobile_phone').val($(this).attr("data-mobile_phone"));
            $('.email_address').val($(this).attr("data-email_address"));
            $('.title').val($(this).attr("data-title"));
            $('.department').val($(this).attr("data-department"));
            $('.Pri').val($(this).attr("data-Pri"));
            $('.systems_id').val($(this).attr("data-systems_id"));
            $('.contacts_id').val($(this).attr("data-contacts_id"));
            $('#EditUser_modal').modal('show');
            //alert($(this).attr("data-first_name"));
        });

    </script>
@endsection

