{{--New file Template--}}

{{--Add Security for this page below--}}
@if ( Auth::user()->hasRole('SSIS') == FALSE)
    @include('layouts.unauthorized')
    @Else

    @extends('layouts.app')
    {{--Page Design Goes Below--}}
@section('content')

    <div class="card-deck">
        <div class="card bg-dark border-info col-lg-6 text-white mb-3">
            <div class="card-header">
                <b>Daily SSIS Report</b>
            </div>
            <div class="card-body" style="height:850px; overflow-y: auto;">
                <div class="row mb-2">
                    <div class="card bg-dark border-success col-md-6 text-white mb-3">
                        <div class="card-header border-success">
                            <b>Success</b>
                        </div>
                        <div class="card-body">
                            <p align="center">{{$daily->where('run_status',1)->count()}}</p>
                        </div>
                    </div>
                    <div class="card bg-dark border-danger col-md-6 text-white mb-3">
                        <div class="card-header border-danger">
                            <b>Failure</b>
                        </div>
                        <div class="card-body">
                            <p align="center">{{$daily->where('run_status',0)->count()}}</p>
                        </div>
                    </div>
                </div>

                <table class="table table-hover table-condensed" id="daily">
                    <thead>
                    <tr>
                        <td><b>Job Name</b></td>
                        <td align="center"><b>Run Date/Time</b></td>
                        <td align="center"><b>Duration</b></td>
                        <td align="center"><b>Status</b></td>
                        <td class="no-sort"></td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($daily as $history)
                        <tr>
                            <td>{{$history->job_name}}</td>
                            <td align="center">{{$history->run_datetime}}</td>
                            <td align="center">{{$history->run_duration}}</td>
                            <td align="center">@if($history->run_status == 1) Success @else FAILED @endif</td>
                            <td align="center">@if($history->run_status == 1) <i class="fa fa-check text-success" aria-hidden="true"></i> @else <i class="fa fa-times-circle-o text-danger" aria-hidden="true"></i> @endif</i></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="card bg-dark text-white border-info mb-3">
            <div class="card-header">
                <b>Monthly SSIS Report</b>
            </div>
            <div class="card-body" style="height:850px; overflow-y: auto;">

                <div class="row mb-2">
                    <div class="card bg-dark text-white col-lg-6 border-success mb-3">
                        <div class="card-header border-success">
                            <b>Success</b>
                        </div>
                        <div class="card-body">
                            <p align="center">{{$monthly->where('run_status',1)->count()}}</p>
                        </div>
                    </div>
                    <div class="card bg-dark text-white col-lg-6 border-danger mb-3">
                        <div class="card-header border-danger">
                            <b>Failure</b>
                        </div>
                        <div class="card-body">
                            <p align="center">{{$monthly->where('run_status',0)->count()}}</p>
                        </div>
                    </div>
                </div>

                <table class="table table-hover table-condensed" id="monthly">
                    <thead>
                    <tr>
                        <td><b>Job Name</b></td>
                        <td align="center"><b>Run Date/Time</b></td>
                        <td align="center"><b>Duration</b></td>
                        <td align="center"><b>Status</b></td>
                        <td class="no-sort"></td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($monthly as $history)
                        <tr>
                            <td>{{$history->job_name}}</td>
                            <td align="center">{{$history->run_datetime}}</td>
                            <td align="center">{{$history->run_duration}}</td>
                            <td align="center">@if($history->run_status == 1) Success @else FAILED @endif</td>
                            <td align="center">@if($history->run_status == 1) <i class="fa fa-check text-success" aria-hidden="true"></i> @else <i class="fa fa-times-circle-o text-danger" aria-hidden="true"></i> @endif</i></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
{{--END of Content and START of Scripts--}}
@section('scripts')

<script type="application/javascript">

    $(document).ready(function() {
        $('#monthly').DataTable({
            "autoWidth": true,
            "columnDefs": [
                { targets: 'no-sort', orderable: false }
            ],
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                if ( aData[3] == "FAILED" )
                {
                    $('td', nRow).css('background-color', '#b53838');
                }
            }
        });
    } );

    $(document).ready(function() {
        $('#daily').DataTable({
            "autoWidth": true,
            "columnDefs": [
                { targets: 'no-sort', orderable: false }
            ],
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                if ( aData[3] == "FAILED" )
                {
                    $('td', nRow).css('background-color', '#b53838');
                }
            }
        });
    } );

</script>

@endsection
@endif