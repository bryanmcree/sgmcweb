<h1>Financial Planning</h1>
<div><b><u>
            <table style="width: 239pt; border-collapse: collapse; margin-left: -0.75pt;" cellspacing="0" cellpadding="0" width="319" border="0">
                <tbody>
                <tr style="height: 15pt;">
                    <td style="height: 15pt; width: 176pt; background: #1f497d; white-space: nowrap; padding: 0in 5.4pt;" valign="bottom">
                        <p><span style="color: white;">Contact Information:</span></p>
                    </td>
                    <td style="height: 15pt; width: 63pt; background: #1f497d; white-space: nowrap; padding: 0in 5.4pt;" valign="bottom">
                        <p><span style="color: white;">Extension:</span></p>
                    </td>
                </tr>
                <tr style="height: 15pt;">
                    <td style="height: 15pt; width: 176pt; background: #c5d9f1; white-space: nowrap; padding: 0in 5.4pt;" valign="bottom">
                        <p>Andrea Mott, Director</p>
                    </td>
                    <td style="height: 15pt; width: 63pt; background: #c5d9f1; white-space: nowrap; padding: 0in 5.4pt;" valign="bottom">
                        <p>4039</p>
                    </td>
                </tr>
                <tr style="height: 15pt;">
                    <td style="height: 15pt; width: 176pt; white-space: nowrap; padding: 0in 5.4pt;" valign="bottom">
                        <p>Email:&nbsp;&nbsp;<a href="mailto:andrea.mott@SGMC.ORG">andrea.mott@sgmc.org</a></p>
                    </td>
                    <td style="height: 15pt; width: 63pt; white-space: nowrap; padding: 0in 5.4pt;" valign="bottom"></td>
                </tr>
                <tr style="height: 15pt;">
                    <td style="height: 15pt; width: 176pt; white-space: nowrap; padding: 0in 5.4pt;" valign="bottom"></td>
                    <td style="height: 15pt; width: 63pt; white-space: nowrap; padding: 0in 5.4pt;" valign="bottom"></td>
                </tr>
                <tr style="height: 15pt;">
                    <td style="height: 15pt; width: 176pt; background: #c5d9f1; white-space: nowrap; padding: 0in 5.4pt;" valign="bottom">
                        <p>Kelly Colwell, Finance Manager</p>
                    </td>
                    <td style="height: 15pt; width: 63pt; background: #c5d9f1; white-space: nowrap; padding: 0in 5.4pt;" valign="bottom">
                        <p>4144</p>
                    </td>
                </tr>
                <tr style="height: 15pt;">
                    <td style="height: 15pt; width: 176pt; white-space: nowrap; padding: 0in 5.4pt;" valign="bottom">
                        <p>Email:&nbsp; <a href="mailto:kelly.colwell@sgmc.org">kelly.colwell@sgmc.org</a></p>
                    </td>
                    <td style="height: 15pt; width: 63pt; white-space: nowrap; padding: 0in 5.4pt;" valign="bottom"></td>
                </tr>
                <tr style="height: 15pt;">
                    <td style="height: 15pt; width: 176pt; white-space: nowrap; padding: 0in 5.4pt;" valign="bottom"></td>
                    <td style="height: 15pt; width: 63pt; white-space: nowrap; padding: 0in 5.4pt;" valign="bottom"></td>
                </tr>
                <tr style="height: 15pt;">
                    <td style="height: 15pt; width: 176pt; background: #c5d9f1; white-space: nowrap; padding: 0in 5.4pt;" valign="bottom">
                        <p>Bryan McRee, Manager Business Intelligence</p>
                    </td>
                    <td style="height: 15pt; width: 63pt; background: #c5d9f1; white-space: nowrap; padding: 0in 5.4pt;" valign="bottom">
                        <p>4144</p>
                    </td>
                </tr>
                <tr style="height: 15pt;">
                    <td style="height: 15pt; width: 176pt; white-space: nowrap; padding: 0in 5.4pt;" valign="bottom">
                        <p>Email:&nbsp; <a href="mailto:bryan.mcree@sgmc.org">bryan.mcree@sgmc.org</a></p>
                    </td>
                    <td style="height: 15pt; width: 63pt; white-space: nowrap; padding: 0in 5.4pt;" valign="bottom"></td>
                </tr>
                <tr style="height: 15pt;">
                    <td style="height: 15pt; width: 176pt; white-space: nowrap; padding: 0in 5.4pt;" valign="bottom"></td>
                    <td style="height: 15pt; width: 63pt; white-space: nowrap; padding: 0in 5.4pt;" valign="bottom"></td>
                </tr>
                <tr style="height: 15pt;">
                    <td style="height: 15pt; width: 176pt; background: #c5d9f1; white-space: nowrap; padding: 0in 5.4pt;" valign="bottom">
                        <p>Jonathan Pritchard, Analyst I</p>
                    </td>
                    <td style="height: 15pt; width: 63pt; background: #c5d9f1; white-space: nowrap; padding: 0in 5.4pt;" valign="bottom">
                        <p>4156</p>
                    </td>
                </tr>
                <tr style="height: 15pt;">
                    <td style="height: 15pt; width: 176pt; white-space: nowrap; padding: 0in 5.4pt;" valign="bottom">
                        <p>Email:&nbsp; <a href="mailto:jonathan.pritchard@sgmc.org">jonathan.pritchard@sgmc.org</a></p>
                    </td>
                    <td style="height: 15pt; width: 63pt; white-space: nowrap; padding: 0in 5.4pt;" valign="bottom"></td>
                </tr>
                <tr style="height: 15pt;">
                    <td style="height: 15pt; width: 176pt; white-space: nowrap; padding: 0in 5.4pt;" valign="bottom"></td>
                    <td style="height: 15pt; width: 63pt; white-space: nowrap; padding: 0in 5.4pt;" valign="bottom"></td>
                </tr>
                <tr style="height: 15pt;">
                    <td style="height: 15pt; width: 176pt; background: #c5d9f1; white-space: nowrap; padding: 0in 5.4pt;" valign="bottom">
                        <p>Greg Rothfuss, Systems Analyst</p>
                    </td>
                    <td style="height: 15pt; width: 63pt; background: #c5d9f1; white-space: nowrap; padding: 0in 5.4pt;" valign="bottom">
                        <p>4011</p>
                    </td>
                </tr>
                <tr style="height: 15pt;">
                    <td style="height: 15pt; width: 176pt; white-space: nowrap; padding: 0in 5.4pt;" valign="bottom">
                        <p>Email:&nbsp; <a href="mailto:greg.rothfuss@sgmc.org">greg.rothfuss@sgmc.org</a></p>
                    </td>
                    <td style="height: 15pt; width: 63pt; white-space: nowrap; padding: 0in 5.4pt;" valign="bottom"><br />
                    </td>
                </tr>
                </tbody>
            </table>
            <br />
        </u></b></div>
<br />
<ul></ul>
<style id="telerik-reTable-1" type="text/css">
    .telerik-reTable-1 { border-width: 0px; border-style: none; border-collapse: collapse; font-family: Tahoma; } .telerik-reTable-1 tr.telerik-reTableHeaderRow-1 { margin: 10px; padding: 10px; color: #3F4D6B; background: #D6E8FF; text-align: left; font-size: 10pt; font-style: normal; font-family: Tahoma; text-transform: capitalize; font-weight: bold; border-spacing: 10px; line-height: 14pt; vertical-align: top; } .telerik-reTable-1 td.telerik-reTableHeaderFirstCol-1 { padding: 0in 5.4pt 0in 5.4pt; color: #3a4663; line-height: 14pt; } .telerik-reTable-1 td.telerik-reTableHeaderLastCol-1 { padding: 0in 5.4pt 0in 5.4pt; color: #3a4663; line-height: 14pt; } .telerik-reTable-1 td.telerik-reTableHeaderOddCol-1 { padding:0in 5.4pt 0in 5.4pt; color: #3a4663; line-height: 14pt; } .telerik-reTable-1 td.telerik-reTableHeaderEvenCol-1 { padding:0in 5.4pt 0in 5.4pt; color: #3a4663; line-height: 14pt; } .telerik-reTable-1 tr.telerik-reTableOddRow-1 { color: #666666; background-color: #F2F3F4; font-size: 10pt; vertical-align: top; } .telerik-reTable-1 tr.telerik-reTableEvenRow-1 { color: #666666; background-color: #E7EBF7; font-size: 10pt; vertical-align: top; } .telerik-reTable-1 td.telerik-reTableFirstCol-1 { padding: 0in 5.4pt 0in 5.4pt; } .telerik-reTable-1 td.telerik-reTableLastCol-1 {padding:0in 5.4pt 0in 5.4pt;} .telerik-reTable-1 td.telerik-reTableOddCol-1 { padding: 0in 5.4pt 0in 5.4pt; } .telerik-reTable-1 td.telerik-reTableEvenCol-1 { padding:0in 5.4pt 0in 5.4pt; } .telerik-reTable-1 tr.telerik-reTableFooterRow-1 { background-color: #D6E8FF; color: #4A5A80; font-weight: 500; font-size: 10pt; font-family: Tahoma; line-height: 11pt; } .telerik-reTable-1 td.telerik-reTableFooterFirstCol-1 { padding: 0in 5.4pt 0in 5.4pt; border-top: solid gray 1.0pt; text-align: left; } .telerik-reTable-1 td.telerik-reTableFooterLastCol-1 { padding:0in 5.4pt 0in 5.4pt; border-top:solid gray 1.0pt; text-align:left; } .telerik-reTable-1 td.telerik-reTableFooterOddCol-1 { padding: 0in 5.4pt 0in 5.4pt; text-align: left; border-top: solid gray 1.0pt; } .telerik-reTable-1 td.telerik-reTableFooterEvenCol-1 { padding: 0in 5.4pt 0in 5.4pt; text-align: left; border-top: solid gray 1.0pt; }
</style>